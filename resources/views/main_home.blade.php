@extends('layouts.user_layout')

@section('content')
<?php if (isset($_GET['page_id'])){
		$next = $_GET['page_id']+1;
		$prev = $_GET['page_id']-1;
	}
	else {
		$prev = '0';
		$next = '1';
	}

?>
<main id="col-main">	
<?php $featureds = session()->get('featureds');

											
											
									?>		
	<div class="flexslider progression-studios-dashboard-slider">
		<ul class="slides">
			@foreach ($featureds as $featured)
				{{-- expr --}}

			<li class="progression_studios_animate_left">
				<div class="progression-studios-slider-dashboard-image-background" style="background-image:url(https://image.tmdb.org/t/p/w500{{$featured->poster_path}});">
					<div class="progression-studios-slider-display-table">
						<div class="progression-studios-slider-vertical-align">							
							<div class="container">
									
										<div
										class="circle-rating-pro"
										data-value="0.86"
										data-animation-start-value="0.86"
										data-size="70"
										data-thickness="6"
										data-fill="{
										&quot;color&quot;: &quot;#42b740&quot;
									}"
									data-empty-fill="#def6de"
									data-reverse="true"
									><span style="color:#42b740;">8.6</span></div>
									
									<div class="progression-studios-slider-dashboard-caption-width">
										<div class="progression-studios-slider-caption-align">
											<h6>{{$featured->type}}</h6>
											<ul class="progression-studios-slider-rating">
												<li>PG-13</li><li>HD</li>
											</ul>
											<h2><a href="dashboard-movie-profile">{{$featured->name}}</a></h2>
											<ul class="progression-studios-slider-meta">
												<li>{{Carbon\Carbon::parse($featured->release_date)->format('d M Y')}} (UK)</li>
												<li>{{$featured->runtime}} min.</li>
												{{-- <li>Documentary</li> --}}
											</ul>
											<p class="progression-studios-slider-description">{{$featured->overview}} 
											</p>

											<a class="btn btn-green-pro btn-slider-pro btn-shadow-pro afterglow" href="<?php if ($featured->type == 'Series'){?>{{'tv'}}<?php } else echo 'movie';?>_detail/{{$featured->movie_id}}"><i class="fas fa-play"></i> Watch Trailer</a>
											{{-- <video class="vid" autoplay loop>
											    <source src="video.mp4" type='video/mp4' />
											    <img id="alternative" src="alternative.jpg" />
											</video>
											<iframe class="vid" id="yt" src="https://www.youtube.com/embed/qObSFfdfe7I" frameborder="0" allowfullscreen></iframe>
											<div id="content">
											    <p>Title</p>
											    <center>
											        <button class="btn btn-green-pro btn-slider-pro btn-shadow-pro afterglow">Click</button>
											    </center>
											</div> --}}


											<div class="progression-studios-slider-more-options">
												<ul>
													@if ($featured->favourite)
													<li><a href="#">Added to Favorites</a></li>
													@else
														<li><a href="/favourite/{{$featured->movie_id}}">Add to Favorites</a></li>
													@endif
													@if ($featured->watchlist)
													<li><a href="#">Added to Watchlist</a></li>
													@else
														<li><a href="/watchlist/{{$featured->movie_id}}">Add to Watchlist</a></li>
													@endif
													@if ($featured->playlist)
													<li><a href="#">Added to Playlist</a></li>
													@else
														<li><a href="/playlist/{{$featured->movie_id}}">Add to Playlist</a></li>
													@endif
													<li><a href="#!">Share...</a></li>
													<li><a href="#!">Write A Review</a></li>
												</ul>
												<i class="fas fa-ellipsis-h"></i>
											</div>
											<div class="clearfix"></div>      

											@if ($featured->crews)
											<h5>Starring</h5>
											<ul class="progression-studios-staring-slider">
													{{-- expr --}}
												@foreach ($featured->crews as $element)
													<li><a href="#!"><img src="https://image.tmdb.org/t/p/w300{{$element->profile_path}}" alt="{{$element->name}}"></a></li>
												@endforeach
																									
											</ul>
												@endif

										</div><!-- close .progression-studios-slider-caption-align -->
									</div><!-- close .progression-studios-slider-caption-width -->
									
								</div><!-- close .container -->
								
							</div><!-- close .progression-studios-slider-vertical-align -->
						</div><!-- close .progression-studios-slider-display-table -->
						
						<div class="progression-studios-slider-mobile-background-cover"></div>
					</div><!-- close .progression-studios-slider-image-background -->
				</li>
			@endforeach				
				</ul>
			</div><!-- close .progression-studios-slider - See /js/script.js file for options -->

			<ul class="dashboard-genres-pro">
				<li >
					<a href="/genre/drama">
						<img src="/final/images/genres/drama.png" alt="Drama">
						<h6>Drama</h6>
					</a>
				</li>
				<li >
					<a href="/genre/comedy">
						<img src="/final/images/genres/comedy.png" alt="Comedy">
						<h6>Comedy</h6>
					</a>
				</li>
				<li  >
					<a href="/genre/action">
						<img src="/final/images/genres/action.png" alt="Action">
						<h6>Action</h6>
					</a>
				</li>
				<li  >
					<a href="/genre/romance">
						<img src="/final/images/genres/romance.png" alt="Romance">
						<h6>Romance</h6>
					</a>
				</li>
				<li  >
					<a href="/genre/horror">
						<img src="/final/images/genres/horror.png" alt="Horror">
						<h6>Horror</h6>
					</a>
				</li>
				<li  >
					<a href="/genre/fantasy">
						<img src="/final/images/genres/fantasy.png" alt="Fantasy">
						<h6>Fantasy</h6>
					</a>
				</li>
				<li  >
					<a href="/genre/science_fiction">
						<img src="/final/images/genres/sci-fi.png" alt="Sci-Fi">
						<h6>Sci-Fi</h6>
					</a>
				</li>
				<li  >
					<a href="/genre/thriller">
						<img src="/final/images/genres/thriller.png" alt="Thriller">
						<h6>Thriller</h6>
					</a>
				</li>
				<li  >
					<a href="/genre/western">
						<img src="/final/images/genres/western.png" alt="Western">
						<h6>Western</h6>
					</a>
				</li>
				<li  >
					<a href="/genre/adventure">
						<img src="/final/images/genres/adventure.png" alt="Adventure">
						<h6>Adventure</h6>
					</a>
				</li>
				<li  >
					<a href="/genre/animation">
						<img src="/final/images/genres/animation.png" alt="Animation">
						<h6>Animation</h6>
					</a>
				</li>
				<li  >
					<a href="/genre/documentary">
						<img src="/final/images/genres/documentary.png" alt="Documentary">
						<h6>Documentary</h6>
					</a>
				</li>
			</ul>
			
			<div class="clearfix"></div>
			
			<div class="dashboard-container">
				
				<h4 class="heading-extra-margin-bottom">Movies</h4>
				<div class="row">
					@if (isset($contents->results))
					@foreach ($contents->results as $element)

					<div class="col-12 col-md-6 col-lg-4 col-xl-3">
						<div class="item-listing-container-skrn">
							<a href="/movie_detail/{{$element->id}}"><img src="https://image.tmdb.org/t/p/w300{{$element->poster_path}}" alt="Listing"></a>
							<div class="item-listing-text-skrn">
								<div class="item-listing-text-skrn-vertical-align"><h6><a href="/movie_detail/{{$element->id}}">{{$element->title}}</a></h6>
									<div
									class="circle-rating-pro"
									data-value="{{$element->vote_average/10}}"
									data-animation-start-value="{{$element->vote_average/10}}"
									data-size="32"
									data-thickness="3"
									data-fill="{
									&quot;color&quot;: &quot;#42b740&quot;
								}"
								data-empty-fill="#def6de"
								data-reverse="true"
								><span style="color:#42b740;">{{$element->vote_average}}</span></div>
							</div><!-- close .item-listing-text-skrn-vertical-align -->
						</div><!-- close .item-listing-text-skrn -->
					</div><!-- close .item-listing-container-skrn -->
				</div><!-- close .col -->
			@endforeach
			@endif
</div><!-- close .row -->


<ul class="page-numbers">
	<li><a class="previous page-numbers" href="/page_id=<?php echo $prev; ?>"><i class="fas fa-chevron-left"></i></a></li>
	<li><span class="page-numbers current">1</span></li>
	<li><a class="page-numbers" href="/?page_id=2">2</a></li>
	<li><a class="page-numbers" href="/?page_id=3">3</a></li>
	<li><a class="page-numbers" href="/?page_id=4">4</a></li>
	<li><a class="next page-numbers" href="/?page_id=<?php echo $next; ?>"><i class="fas fa-chevron-right"></i></a></li>
</ul>


</div><!-- close .dashboard-container -->
</main>


@endsection
