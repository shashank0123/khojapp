@php $count_m = $episodestotal @endphp
@foreach($episodes as $episode)
     <div class="col-sm-12" style="border-bottom: 1px solid rgba(50,50,50,0.1); padding: 15px 0 5px">
      <div class="row" style=" cursor: pointer;" onclick="showEpisodes({{$episode->content_id}})">
        <div class="col-sm-11 col-xs-11">
          <h5 style="font-size: 15px; font-weight: normal;">
            {{$season_one}} 
            <span style="color: #9ba09b">
              @if (!empty($episode->episode_number))-{{'E'.$episode->episode_number}}@else {{ "E".$count_m}}@endif 
              @if(!empty($episode->original_title))
              -{{$episode->original_title}}
              @elseif(!empty($episode->title))
              -{{$episode->title}}
              @endif
            </span>
          </h5>
        </div>
        <div class="col-xs-1 col-sm-1" style="text-align: right;">
          <i class="fa fa-angle-down" style="font-size: 20px"></i>&nbsp;&nbsp;&nbsp;
        </div>
      </div>
    </div>
     @php $count_m-- @endphp
    <div class="col-sm-12" id="episode{{$episode->content_id}}" style="display: none; border: 1px solid #fff; border-top: 1px solid rgba(50,50,50,0.1); border-radius: 5px">
      @if(!empty($episode->short_description ))
      <div class="content" style="padding: 2px 10px">
        <p style="font-size: 14px">{{$episode->short_description ?? ''}}</p>
      </div>
      @endif

      <div class="row ipad-width2 app-icons" style="margin-top: 5px; padding:  10px">
       <div class="col-md-12 col-sm-12 col-xs-12" >
        <h4>
        Watch Now</h4>
        <div class="app-get">
         <div class="row">
          <?php
          $flatrate = array ( 'cinema','flatrate','free','ads');
          $streams = DB::table('content_providers')
          ->where('content_id',$episode->content_id)
          ->whereIn('monetization_type',$flatrate)
          ->select('content_id','provider_id','video_quality','monetization_type','retail_price','currency','web_url')
          ->orderBy('provider_id', 'DESC')
          ->distinct('provider_id')
          ->get();

                      // $providers->stream = $streams;
          $buy = array ( 'cinema','buy','rent');

          $buys = DB::table('content_providers')
          ->where('content_id',$episode->content_id)
          ->select('content_id','provider_id','video_quality','monetization_type','retail_price','currency','web_url')
          ->where('retail_price','>',0)
          ->whereIn('monetization_type',$buy)
          ->orderBy('provider_id', 'DESC')
          ->distinct('provider_id')
          ->get();

          ?>

          <table class="table table-bordered">
            <tbody>
              @php $countpro = 0 @endphp
              @if(!empty($streams) && count($streams)>0)
              <tr>
                <th style="width: 20px" id="movie-providers"><h4>Stream<br><br></h4></th>
                <td colspan="14">
                  <?php $i = 0; $prev = ""; $qlty = "";?>
                  @foreach($streams as $con)
                  <?php
                  $countpro++;
                  $providers1 = DB::table('providers')->where('provider_id',$con->provider_id)->first(); 
                  if(!empty($providers1)){

                    if ($con->retail_price <= 0 || $con->monetization_type == 'flatrate' || $con->monetization_type == 'Flatrate' ||  $con->monetization_type == 'free'){ 
                      if($prev == $con->provider_id ){
                        continue;
                      }
                      ?>
                      @if ($con->retail_price < 1 && $i == 0)
                      <?php $i = 1; ?>

                      @endif
                      <div class="col-sm-1 col-xs-2" id="setproviders">
                        <a href="{{$con->web_url}}" target="blank">
                         <img src="{{asset('/asset'.$providers1->icon_url)}}" alt="{{$provider1->title ?? ''}}" class="app-img">
                         <p>@if ($con->retail_price > 0) {{$con->retail_price}}@elseif($con->monetization_type == 'flatrate' || $con->monetization_type == 'Flatrate'){{'Subs'}}@else{{ucfirst($con->monetization_type)}}@endif <span style="color: yellow"> {{strtoupper($con->video_quality)}} </span>
                         </p>
                       </a>
                     </div>
                     <?php 
                     $qlty = $con->video_quality;
                     $prev = $con->provider_id;
                   }
                 } ?>
                 @endforeach

               </td>
             </tr>
             @endif


             @if(!empty($buys) && count($buys)>0)
             <tr>
              <th id="movie-providers"><h4>Buy/Rent<br><br></h4></th>
              <td colspan="14">

                <?php $i = 0; $prev = ""; $qlty = "";?>

                @foreach($buys as $con)
                <?php
                $countpro++;
                $providers1 = DB::table('providers')->where('provider_id',$con->provider_id)->first(); 
                if(!empty($providers1)){
                  $i++;
                  if($prev == $con->provider_id ){
                    continue;
                  }
                  ?>

                  <div class="col-sm-1 col-xs-2" id="setproviders">
                   <a href="{{$con->web_url}}" target="blank">
                    <img src="{{asset('/asset'.$providers1->icon_url)}}" alt="{{$provider1->title ?? ''}}" class="app-img">
                    <p>@if ($con->retail_price > 0) {{$con->retail_price}}@elseif($con->monetization_type == 'flatrate' || $con->monetization_type == 'Flatrate'){{'Subs'}}@else{{ucfirst($con->monetization_type)}}@endif <span style="color: yellow"> {{strtoupper($con->video_quality)}} </span>
                    </p>
                  </a>
                </div>
                <?php 
                $qlty = $con->video_quality;
                $prev = $con->provider_id;

              } ?>
              @endforeach

            </td>
          </tr>
          @endif
        </tbody>
      </table> 

      @if($countpro == 0)
      <p>No source available right now to watch this.</p>
      @endif

    </div>
  </div>
</div>
</div>

</div>
@endforeach

<div style="text-align: center;" id="downarrow2" onclick="showAllEpisodes({{$movie->content_id}})">
    <i class="fa fa-angle-double-down"></i>
  </div>
  <div style="text-align: center;" id="uparrow2" onclick="showSelectedEpisodes({{$movie->content_id}})">
    <i class="fa fa-angle-double-up"></i>
  </div>