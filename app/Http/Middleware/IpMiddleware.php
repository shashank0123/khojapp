<?php

namespace App\Http\Middleware;

use Closure;

class IpMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

    public $restrictIps = [
        '64.145.*.*', 
        '192.200.*.*', 
        '209.107.*.*', 
        '173.239.*.*', 
        '173.245.*.*', 
        '23.250.*.*', 
        '205.185.*.*',
        '193.37.*.*',
        '196.52.*.*',
        '209.197.*.*',
        '199.116.*.*',
        '173.244.*.*',
        '185.245.*.*',
        '45.72.*.*',
        '46.229.*.*',
        // '110.172.*.*',
        '216.151.*.*'];
    public function handle($request, Closure $next)
    {
        // echo \Request::ip();
        // die();
        $ip = \Request::ip();
        $ip = explode('.', $ip);
        $newip = $ip[0].".".$ip[1].".*.*";
        if (in_array($newip, $this->restrictIps)) {
    
            return response()->json(["you don't have permission to access this application."]);
        }
    
        return $next($request);
    }
}
