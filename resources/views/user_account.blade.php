@extends('layouts.user_layout')
<meta name="csrf-token" content="{{ csrf_token() }}">

@section('content')

<main id="col-main">
			
			<div class="dashboard-container">
				
				<ul class="dashboard-sub-menu">
					<li class="current"><a href="dashboard-account">Account Settings</a></li>
					{{-- <li><a href="dashboard-account-payment">Payment Info</a></li> --}}
				</ul><!-- close .dashboard-sub-menu -->
				
				<div class="container-fluid">
					<div class="row">
						<?php if (isset($profile->profile_pic))
							 $profile_pic = '/images/user/'.$profile->profile_pic;
							else {
								$profile_pic = '/final/images/demo/account-profile.jpg';
							}
						?>

					
						<div class="col-3  col-lg-3">
							<div id="account-edit-photo">
								<div><img src="{{ $profile_pic }}" id="display_pic" alt="{{auth()->user()->name}}"></div>
								<p><input type="file" id="profile_pic" name="profile_pic" style="display: none" ><button class="btn btn-green-pro" id="OpenImgUpload">Change Profile Picture</button></p>
								<p><a href="#!" class="btn">Delete Photo</a></p>
							</div>
						</div><!-- close .col -->


						<div class="col">
						<form id="form" method="POST" class="account-settings-form" action="/account" enctype="multipart/form-data">
							@csrf
						
						
							<h5>General Information</h5>
							<p class="small-paragraph-spacing">By letting us know your name, we can make our support experience much more personal.</p>
							<div class="row">
								<div class="col-sm">
	   	  						 <div class="form-group">
	   								 <label for="first-name" class="col-form-label">Name:</label>
	   	  							 <input type="text" name="name" class="form-control" id="first-name" value="{{$user->name}}">
	   	  						 </div>
								</div><!-- close .col -->
{{-- 								<div class="col-sm">
    	  	  						 <div class="form-group">
    	  								 <label for="last-name" class="col-form-label">Last Name:</label>
    	  	  							 <input type="text" name="lastname" class="form-control" id="last-name" value="Smith">
    	  	  						 </div>
								</div><!-- close .col --> --}}
								<div class="col-sm">
    	  	  						 <div class="form-group">
    	  								 <label for="last-name" class="col-form-label">Country:</label>
		 								<select name="country" class="custom-select">
		 								  <option @if (isset($profile->country) && $profile->country=='0')
		 								  	{{ 'selected' }}
		 								  @endif value="0">All Countries</option>
		 								  <option @if (isset($profile->country) && $profile->country=='1')
		 								  	{{ 'selected' }}
		 								  @endif value="1">Argentina</option>
		 								  <option @if (isset($profile->country) && $profile->country=='2')
		 								  	{{ 'selected' }}
		 								  @endif value="2">Australia</option>
		 								  <option @if (isset($profile->country) && $profile->country=='3')
		 								  	{{ 'selected' }}
		 								  @endif value="3">Bahamas</option>
		 								  <option @if (isset($profile->country) && $profile->country=='4')
		 								  	{{ 'selected' }}
		 								  @endif value="4">Belgium</option>
		 								  <option @if (isset($profile->country) && $profile->country=='5')
		 								  	{{ 'selected' }}
		 								  @endif value="5">Brazil</option>
		 								  <option @if (isset($profile->country) && $profile->country=='6')
		 								  	{{ 'selected' }}
		 								  @endif value="6">Canada</option>
		 								  <option @if (isset($profile->country) && $profile->country=='7')
		 								  	{{ 'selected' }}
		 								  @endif value="7">Chile</option>
		 								  <option @if (isset($profile->country) && $profile->country=='8')
		 								  	{{ 'selected' }}
		 								  @endif value="8">China</option>
		 								  <option @if (isset($profile->country) && $profile->country=='9')
		 								  	{{ 'selected' }}
		 								  @endif value="9">Denmark</option>
		 								  <option @if (isset($profile->country) && $profile->country=='10')
		 								  	{{ 'selected' }}
		 								  @endif value="10">Ecuador</option>
		 								  <option @if (isset($profile->country) && $profile->country=='11')
		 								  	{{ 'selected' }}
		 								  @endif value="11">France</option>
		 								  <option @if (isset($profile->country) && $profile->country=='12')
		 								  	{{ 'selected' }}
		 								  @endif value="12">Germany</option>
		 								  <option @if (isset($profile->country) && $profile->country=='13')
		 								  	{{ 'selected' }}
		 								  @endif value="13">Greece</option>
		 								  <option @if (isset($profile->country) && $profile->country=='14')
		 								  	{{ 'selected' }}
		 								  @endif value="14">Guatemala</option>
		 								  <option @if (isset($profile->country) && $profile->country=='15')
		 								  	{{ 'selected' }}
		 								  @endif value="15">Italy</option>
		 								  <option @if (isset($profile->country) && $profile->country=='16')
		 								  	{{ 'selected' }}
		 								  @endif value="16">Japan</option>
		 								  <option @if (isset($profile->country) && $profile->country=='17')
		 								  	{{ 'selected' }}
		 								  @endif value="17">asdfasdf</option>
		 								  <option @if (isset($profile->country) && $profile->country=='18')
		 								  	{{ 'selected' }}
		 								  @endif value="18">Korea</option>
		 								  <option @if (isset($profile->country) && $profile->country=='19')
		 								  	{{ 'selected' }}
		 								  @endif value="19">Malaysia</option>
		 								  <option @if (isset($profile->country) && $profile->country=='20')
		 								  	{{ 'selected' }}
		 								  @endif value="20">Monaco</option>
		 								  <option @if (isset($profile->country) && $profile->country=='21')
		 								  	{{ 'selected' }}
		 								  @endif value="21">Morocco</option>
		 								  <option @if (isset($profile->country) && $profile->country=='22')
		 								  	{{ 'selected' }}
		 								  @endif value="22">New Zealand</option>
		 								  <option @if (isset($profile->country) && $profile->country=='23')
		 								  	{{ 'selected' }}
		 								  @endif value="23">Panama</option>
		 								  <option @if (isset($profile->country) && $profile->country=='24')
		 								  	{{ 'selected' }}
		 								  @endif value="24">Portugal</option>
		 								  <option @if (isset($profile->country) && $profile->country=='25')
		 								  	{{ 'selected' }}
		 								  @endif value="25">Russia</option>
		 								  <option @if (isset($profile->country) && $profile->country=='26')
		 								  	{{ 'selected' }}
		 								  @endif value="26">United Kingdom</option>
		 								  <option @if (isset($profile->country) && $profile->country=='26')
		 								  	{{ 'selected' }}
		 								  @endif value="27">India</option>
		 								</select>
    	  	  						 </div>
								</div><!-- close .col -->
							</div><!-- close .row -->
							<hr>
						
							<h5>Account Information</h5>
							<p class="small-paragraph-spacing">You can change the email address you use here.</p>
							
							<div class="row">
								<div class="col-sm">
	   	  						 <div class="form-group">
	   								 <label for="e-mail" class="col-form-label">E-mail</label>
	   	  							 <input type="text" class="form-control" id="e-mail" value="{{ $user->email}}">
	   	  						 </div>
								</div><!-- close .col -->
								{{-- <div class="col-sm">
    	  	  						 <div class="form-group">
    	  								 <div><label for="button-change" class="col-form-label">&nbsp; &nbsp;</label></div>
    	  	  							 <a href="#!" class="btn btn-form">Change E-mail</a>
    	  	  						 </div>
								</div><!-- close .col --> --}}
								
							</div><!-- close .row -->
							
							<hr>
							<h5>Change Password</h5>
							<p class="small-paragraph-spacing">You can change the password you use for your account here.</p>
							<div class="row">
								<div class="col-sm">
	   	  						 <div class="form-group">
	   								 <label for="password" class="col-form-label">Current Password:</label>
	   	  							 <input type="password" name="old_password" class="form-control" id="old-password" placeholder="&middot;&middot;&middot;&middot;&middot;&middot;&middot;&middot;&middot;&middot;&middot;&middot;">
	   	  						 </div>
								</div><!-- close .col -->
								<div class="col-sm">
	   	  						 <div class="form-group">
	   								 <label for="new-password" class="col-form-label">New Password:</label>
	   	  							 <input type="password" name="password" class="form-control" id="new-password" placeholder="Minimum of 6 characters">
	   	  						 </div>
								</div><!-- close .col -->
								<div class="col-sm">
	   	  						 <div class="form-group">
	   								 <div><label for="confirm-password" class="col-form-label">&nbsp; &nbsp;</label></div>
	   	  							 <input type="password" name="password_confirmation" class="form-control" id="confirm-password" placeholder="Confirm New Password">
	   	  						 </div>
								</div><!-- close .col -->
							</div><!-- close .row -->
							
							<hr>
							<h5>Preferred Genres</h5>
							<p class="small-paragraph-spacing">Pick your favorite genres for content.</p>
							
								<div class="registration-genres-step">
									<ul id="favourite-genre" class="registration-genres-choice">
										<li data-option="Drama" class="active">
											<i class="fas fa-check-circle"></i>
											<img src="final/images/genres/drama.png" alt="Drama">
											<h6>Drama</h6>
										</li>
										<li data-option="Comedy">
											<i class="fas fa-check-circle"></i>
											<img src="final/images/genres/comedy.png" alt="Comedy">
											<h6>Comedy</h6>
										</li>
										<li data-option="Action">
											<i class="fas fa-check-circle"></i>
											<img src="final/images/genres/action.png" alt="Action">
											<h6>Action</h6>
										</li>
										<li data-option="Romance">
											<i class="fas fa-check-circle"></i>
											<img src="final/images/genres/romance.png" alt="Romance">
											<h6>Romance</h6>
										</li>
										<li data-option="Horror" class="active">
											<i class="fas fa-check-circle"></i>
											<img src="final/images/genres/horror.png" alt="Horror">
											<h6>Horror</h6>
										</li>
										<li data-option="Fantasy" class="active">
											<i class="fas fa-check-circle"></i>
											<img src="final/images/genres/fantasy.png" alt="Fantasy">
											<h6>Fantasy</h6>
										</li>
										<li data-option="Sci">
											<i class="fas fa-check-circle"></i>
											<img src="final/images/genres/sci-fi.png" alt="Sci-Fi">
											<h6>Sci-Fi</h6>
										</li>
										<li data-option="Thriller">
											<i class="fas fa-check-circle"></i>
											<img src="final/images/genres/thriller.png" alt="Thriller">
											<h6>Thriller</h6>
										</li>
										<li data-option="Western">
											<i class="fas fa-check-circle"></i>
											<img src="final/images/genres/western.png" alt="Western">
											<h6>Western</h6>
										</li>
										<li data-option="Adventure">
											<i class="fas fa-check-circle"></i>
											<img src="final/images/genres/adventure.png" alt="Adventure">
											<h6>Adventure</h6>
										</li>
										<li data-option="Animation">
											<i class="fas fa-check-circle"></i>
											<img src="final/images/genres/animation.png" alt="Animation">
											<h6>Animation</h6>
										</li>
										<li data-option="Documentary">
											<i class="fas fa-check-circle"></i>
											<img src="final/images/genres/documentary.png" alt="Documentary">
											<h6>Documentary</h6>
										</li>
									</ul>
									<div class="clearfix"></div>
								</div><!-- close .registration-genres-step -->
								<div class="clearfix"></div>
							<hr>
							<p><button id="savechanges" class="btn btn-green-pro">Save Changes</button></p>
							<br>
							</form>
						
						</div><!-- close .col -->
					
					</div><!-- close .row -->
				</div><!-- close .container-fluid -->
						
			</div><!-- close .dashboard-container -->
		</main>	
@endsection