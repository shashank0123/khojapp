<?php
$count_search = 0;
?>

<style>
.dynamic-li h6 { font-size: 15px }
.ul-head { margin-top: 10px }
h6 { color: #fff; }
.dynamic-li h6, .dynamic-li p { color: #fff; }
.dynamic-li { margin: 10px 0px 3px; border-bottom: 1px solid #0a0a0a }
ul{ margin: 5% }
#search-mob { display: none; }

@media screen and (max-width: 768px){
	#search-web { display: none; }
	#search-mob { display: block; }
}
</style>
<div class="row">
	<div class="col-sm-8">
		
		<h6 class="ul-head">&nbsp;&nbsp;&nbsp;Movies & Tv Series</h6>
		<ul><?php $space = 0;?>
			@if(!empty($contents))
			@foreach($contents as $element)	

			<?php 
			$parent_title = "";

			$flag = 1; 
			$space++;
			//if($element->parent_id>0){
			//	$parent_title = DB::table('contents')->where('content_id',$element->parent_id)->first();
			//}
			?>		

			<li class="dynamic-li" style="height: auto;">
				<div class="row">
					<div class="col-sm-3 col-xs-3" style="text-align: center">
						@if(isset($element->poster) && strpos($element->poster, 'poster/') == false )

						<img src="/poster/{{$element->poster}}" alt="{{$element->title ?? ''}}" style="width: 35px; height: auto;padding-bottom: 5px">

						@else
						<img src="/images/default.png" alt="{{$element->title ?? ''}}"  style="width: 35px; height: auto; padding-bottom: 5px"/>
						@endif
					</div>
					<div class="col-sm-9 col-xs-9">
						<?php $urlen = preg_replace('/[^a-zA-Z0-9_.]/', '_', (strtolower($element->title))).'-'.$element->content_id; ?>
						<a  href="/detail/{{$element->content_type}}/{{$urlen}}">
							
							<h6><?php if(isset($element->title))
							echo substr($element->title,0,50); 
							elseif(isset($element->original_title))
								echo substr($element->original_title,0,50); 
							if(isset($parent_title) && isset($parent_title->title))
								echo " (".substr($parent_title->title,0,50).") ";
							elseif(isset($parent_title) && isset($parent_title->original_title))
								echo " (".substr($parent_title->original_title,0,50).") ";
							?></h6>
							<p>@if($element->content_type){{$element->content_type}}@endif, @if($element->original_release_year){{$element->original_release_year}}@endif</p>

						</a>
					</div>
				</div>
			</li>
			@endforeach
			@endif

		</ul>
	</div>
	<div class="col-sm-4">
		
		<h6 class="ul-head">People</h6>
		<ul>
			@if(!empty($casts))
			@foreach($casts as $cast)


<?php $urlen = preg_replace('/[^a-zA-Z0-9_.]/', '_', (strtolower($cast->name))).'-'.$cast->person_id; ?>
								

			<li class="dynamic-li"  style="height: 55px; border-bottom: 1px solid #0a0a0a">
				<a href="{{url('casts/'.$urlen)}}">{{$cast->name}}</a>
			</li>

			@endforeach
			@endif

		</ul>
	</div>
</div>


<div class="row" style="background-color: #006699;text-align: center; cursor: pointer;" onclick="getSearchPage()" id="search-web">
	<p style="padding:10px; font-size: 16px; color: #fff ; ">See all results for <i>"{{$keyword}}"</i></p>
</div>

<div class="row" style="background-color: #006699;text-align: center; cursor: pointer;" onclick="getSearchPage2()" id="search-mob">
	<p style="padding:10px; font-size: 16px; color: #fff ; ">See all results for <i>"{{$keyword}}"</i></p>
</div>
