<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddImdbRatingToMovies extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('movies', function (Blueprint $table) {
            $table->string('full_path')->nullable();
            $table->string('original_title')->nullable();
            $table->string('localized_release_date')->nullable();
            $table->string('original_language')->nullable();
            $table->string('cinema_release_date')->nullable();
            $table->string('cinema_release_week')->nullable();
            $table->string('is_nationwide_cinema_release_date')->nullable();
            $table->string('age_certification')->nullable();
            $table->string('imdb_score')->nullable();
            $table->string('tmdb_score')->nullable();
            $table->string('tmdb_popularity')->nullable();
            $table->string('date_modified')->nullable();
            $table->string('show_id')->nullable();
            $table->string('season_number')->nullable();
            $table->string('total_seasons')->nullable();
            $table->string('season_id')->nullable();
            $table->string('episode_number')->nullable();
            $table->string('total_episodes')->nullable();
            $table->string('score_updated')->nullable();
            $table->string('content_source')->nullable();
            $table->string('content_loaded')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('movies', function (Blueprint $table) {
            //
        });
    }
}
