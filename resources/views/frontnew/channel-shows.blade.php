@extends('layouts.new_front')


@section('content')


<div class="movie-items  full-width">
	<div class="row">
		<div class="col-md-12">
			<div class="title-hd">
				<h2>STAR PLUS</h2>
				{{-- <a href="#" class="nowshow">Now Showing <i class="ion-ios-arrow-right"></i></a>
				<a href="#" class="viewall">View all <i class="ion-ios-arrow-right"></i></a> --}}

			</div>
			
				<ul class="tab-links">
					{{-- <li class="subtitle">Showing all results for keyword</li> --}}
					                      
				</ul>        
			            
			            	<div class="row">
			            		<div class="col-sm-3 search-response">
									<div class="movie-item channel-img">
										<a href="channel-shows">
				            			<div class="mv-img">            				
				            				<img src="/channels/1.webp" alt="">
				            			</div>
				            			</a>
				            			{{-- <div class="hvr-inner">
				            				<a  href="moviesingle.html">
				            					<i class="fa fa-play" aria-hidden="true"></i> </a>
				            			</div> --}}
				            			{{-- <div class="title-in">
				            				<h6><a href="#">The revenant</a></h6>
				            				<p><i class="ion-android-star"></i><span>7.4</span> /10</p>
				            			</div> --}}
				            		</div>
								</div>
								<div class="col-sm-3 search-response">
									<div class="movie-item channel-img">
										<a href="channel-shows">
				            			<div class="mv-img">
				            				<img src="/channels/2.webp" alt="">
				            			</div>
				            		</a>
				            			{{-- <div class="hvr-inner">
				            				<a  href="moviesingle.html">
				            					<i class="fa fa-play" aria-hidden="true"></i> </a>
				            			</div>
				            			<div class="title-in">
				            				<h6><a href="#">The revenant</a></h6>
				            				<p><i class="ion-android-star"></i><span>7.4</span> /10</p>
				            			</div> --}}
				            		</div>
								</div>
			            		<div class="col-sm-3 search-response">
			            			<div class="movie-item channel-img">
										<a href="channel-shows">
				            			<div class="mv-img">
				            				<img src="/channels/3.webp" alt="">
				            			</div>
										</a>
				            			{{-- <div class="hvr-inner">
				            				<a  href="moviesingle.html">
				            					<i class="fa fa-play" aria-hidden="true"></i> </a>
				            			</div>
				            			<div class="title-in">
				            				<h6><a href="#">Die hard</a></h6>
				            				<p><i class="ion-android-star"></i><span>7.4</span> /10</p>
				            			</div> --}}
				            		</div>
			            		</div>
			            		<div class="col-sm-3 search-response">
			            			<div class="movie-item channel-img">
										<a href="channel-shows">
				            			<div class="mv-img">
				            				<img src="/channels/4.webp" alt="">
				            			</div>
				            		</a>
				            			{{-- <div class="hvr-inner">
				            				<a  href="moviesingle.html">
				            					<i class="fa fa-play" aria-hidden="true"></i> </a>
				            			</div>
				            			<div class="title-in">
				            				<h6><a href="#">The walk</a></h6>
				            				<p><i class="ion-android-star"></i><span>7.4</span> /10</p>
				            			</div> --}}
				            		</div>
			            		</div>
			            		<div class="col-sm-3 search-response">
			            			<div class="movie-item channel-img">
										<a href="channel-shows">
				            			<div class="mv-img">
				            				<img src="/channels/5.webp" alt="">
				            			</div>
				            		</a>
				            			{{-- <div class="hvr-inner">
				            				<a  href="moviesingle.html">
				            					<i class="fa fa-play" aria-hidden="true"></i> </a>
				            			</div>
				            			<div class="title-in">
				            				<h6><a href="#">Die hard</a></h6>
				            				<p><i class="ion-android-star"></i><span>7.4</span> /10</p>
				            			</div> --}}
				            		</div>
			            		</div>
			            		<div class="col-sm-3 search-response">
			            			<div class="movie-item channel-img">
										<a href="channel-shows">
				            			<div class="mv-img">
				            				<img src="/channels/6.webp" alt="">
				            			</div>
										</a>
				            			{{-- <div class="hvr-inner">
				            				<a  href="moviesingle.html">
				            					<i class="fa fa-play" aria-hidden="true"></i> </a>
				            			</div>
				            			<div class="title-in">
				            				<h6><a href="#">Interstellar</a></h6>
				            				<p><i class="ion-android-star"></i><span>7.4</span> /10</p>
				            			</div> --}}
				            		</div>
			            		</div>
			            		<div class="col-sm-3 search-response">
			            			<div class="movie-item channel-img">
										<a href="channel-shows">
				            			<div class="mv-img">
				            				<img src="/channels/7.webp" alt="">
				            			</div>
				            		</a>
				            			{{-- <div class="hvr-inner">
				            				<a  href="moviesingle.html">
				            					<i class="fa fa-play" aria-hidden="true"></i> </a>
				            			</div>
				            			<div class="title-in">
				            				<h6><a href="#">Die hard</a></h6>
				            				<p><i class="ion-android-star"></i><span>7.4</span> /10</p>
				            			</div> --}}
				            		</div>
			            		</div>
			            		<div class="col-sm-3 search-response">
			            			<div class="movie-item channel-img">
										<a href="channel-shows">
				            			<div class="mv-img">
				            				<img src="/channels/8.webp" alt="">
				            			</div>
				            			</a>
				            			{{-- <div class="hvr-inner">
				            				<a  href="moviesingle.html">
				            					<i class="fa fa-play" aria-hidden="true"></i> </a>
				            			</div>
				            			<div class="title-in">
				            				<h6><a href="#">Die hard</a></h6>
				            				<p><i class="ion-android-star"></i><span>7.4</span> /10</p>
				            			</div> --}}
				            		</div>
			            		</div>


			            		<div class="col-sm-3 search-response">
									<div class="movie-item channel-img">
										<a href="channel-shows">
				            			<div class="mv-img">
				            				<img src="/channels/1.webp" alt="">
				            			</div>
				            		</a>
				            			{{-- <div class="hvr-inner">
				            				<a  href="moviesingle.html">
				            					<i class="fa fa-play" aria-hidden="true"></i> </a>
				            			</div> --}}
				            			{{-- <div class="title-in">
				            				<h6><a href="#">The revenant</a></h6>
				            				<p><i class="ion-android-star"></i><span>7.4</span> /10</p>
				            			</div> --}}
				            		</div>
								</div>
								<div class="col-sm-3 search-response">
									<div class="movie-item channel-img">
										<a href="channel-shows">
				            			<div class="mv-img">
				            				<img src="/channels/2.webp" alt="">
				            			</div>
										</a>
				            			{{-- <div class="hvr-inner">
				            				<a  href="moviesingle.html">
				            					<i class="fa fa-play" aria-hidden="true"></i> </a>
				            			</div>
				            			<div class="title-in">
				            				<h6><a href="#">The revenant</a></h6>
				            				<p><i class="ion-android-star"></i><span>7.4</span> /10</p>
				            			</div> --}}
				            		</div>
								</div>
			            		<div class="col-sm-3 search-response">
			            			<div class="movie-item channel-img">
										<a href="channel-shows">
				            			<div class="mv-img">
				            				<img src="/channels/3.webp" alt="">
				            			</div>
				            		</a>
				            			{{-- <div class="hvr-inner">
				            				<a  href="moviesingle.html">
				            					<i class="fa fa-play" aria-hidden="true"></i> </a>
				            			</div>
				            			<div class="title-in">
				            				<h6><a href="#">Die hard</a></h6>
				            				<p><i class="ion-android-star"></i><span>7.4</span> /10</p>
				            			</div> --}}
				            		</div>
			            		</div>
			            		<div class="col-sm-3 search-response">
			            			<div class="movie-item channel-img">
										<a href="channel-shows">
				            			<div class="mv-img">
				            				<img src="/channels/4.webp" alt="">
				            			</div>
				            			</a>
				            			{{-- <div class="hvr-inner">
				            				<a  href="moviesingle.html">
				            					<i class="fa fa-play" aria-hidden="true"></i> </a>
				            			</div>
				            			<div class="title-in">
				            				<h6><a href="#">The walk</a></h6>
				            				<p><i class="ion-android-star"></i><span>7.4</span> /10</p>
				            			</div> --}}
				            		</div>
			            		</div>
			            		<div class="col-sm-3 search-response">
			            			<div class="movie-item channel-img">
										<a href="channel-shows">
				            			<div class="mv-img">
				            				<img src="/channels/5.webp" alt="">
				            			</div>
				            		</a>
				            			{{-- <div class="hvr-inner">
				            				<a  href="moviesingle.html">
				            					<i class="fa fa-play" aria-hidden="true"></i> </a>
				            			</div>
				            			<div class="title-in">
				            				<h6><a href="#">Die hard</a></h6>
				            				<p><i class="ion-android-star"></i><span>7.4</span> /10</p>
				            			</div> --}}
				            		</div>
			            		</div>
			            		<div class="col-sm-3 search-response">
			            			<div class="movie-item channel-img">
										<a href="channel-shows">
				            			<div class="mv-img">
				            				<img src="/channels/6.webp" alt="">
				            			</div>
				            			</a>
				            			{{-- <div class="hvr-inner">
				            				<a  href="moviesingle.html">
				            					<i class="fa fa-play" aria-hidden="true"></i> </a>
				            			</div>
				            			<div class="title-in">
				            				<h6><a href="#">Interstellar</a></h6>
				            				<p><i class="ion-android-star"></i><span>7.4</span> /10</p>
				            			</div> --}}
				            		</div>
			            		</div>
			            		<div class="col-sm-3 search-response">
			            			<div class="movie-item channel-img">
										<a href="channel-shows">
				            			<div class="mv-img">
				            				<img src="/channels/7.webp" alt="">
				            			</div>
				            		</a>
				            			{{-- <div class="hvr-inner">
				            				<a  href="moviesingle.html">
				            					<i class="fa fa-play" aria-hidden="true"></i> </a>
				            			</div>
				            			<div class="title-in">
				            				<h6><a href="#">Die hard</a></h6>
				            				<p><i class="ion-android-star"></i><span>7.4</span> /10</p>
				            			</div> --}}
				            		</div>
			            		</div>
			            		<div class="col-sm-3 search-response">
			            			<div class="movie-item channel-img">
										<a href="channel-shows">
				            			<div class="mv-img">
				            				<img src="/channels/8.webp" alt="">
				            			</div>
				            		</a>
				            			{{-- <div class="hvr-inner">
				            				<a  href="moviesingle.html">
				            					<i class="fa fa-play" aria-hidden="true"></i> </a>
				            			</div>
				            			<div class="title-in">
				            				<h6><a href="#">Die hard</a></h6>
				            				<p><i class="ion-android-star"></i><span>7.4</span> /10</p>
				            			</div> --}}
				            		</div>
			            		</div>
			            		

			            		
			            	</div>
			           
			        
			        
			          
		       	 	
			   
			    <hr class="hrstyle">
			

			
			
			
		</div>
	</div>
	
</div>


@endsection 