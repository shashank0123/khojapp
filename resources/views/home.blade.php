@extends('layouts.static_layout')

@section('content')
<section class="banner">
        <div class="col-md-3 head_text" style="color: white;">
            <h1>Welcome To <br> SKRN.</h1>
            <p>Watch the largest collection of Movies and TV series anytime anywhere !</p>
            <br>
            <a href="" class="btn btn-success">Start your Free Trial</a>
                    
        </div>
    </section>
    <section class="watch">
        <div class="container">
            <div class="col-md-6 watch_content">
                <img src="img/mobile.png" style="width: 100%;  " alt="">
            </div>
            <div class="col-md-6 watch_content2">
                <h1>Watch In Any Devices</h1>
                <br>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Pariatur labore, commodi perspiciatis. Id minus reprehenderit ad ipsum, earum qui quam ea at! Non praesentium sint perferendis, voluptas velit magni nobis.</p>
                <br>
                <a href="" class="btn btn-success">Learn More</a>
            </div>
        </div>
    </section>

    <section class="playlist">
        <div class="container">
            <div class="col-md-6 playlist_content2">
                <h1>Watch In Any Devices</h1>
                <br>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Pariatur labore, commodi perspiciatis. Id minus reprehenderit ad ipsum, earum qui quam ea at! Non praesentium sint perferendis, voluptas velit magni nobis.</p>
                <br>
                <a href="" class="btn btn-success">Learn More</a>
            </div>
            <div class="col-md-6 playlist_content">
                <img src="img/mobile 1.png" style="width: 100%;  " alt="">
            </div>
        </div>
    </section>
    <section class="ultra">
        <div class="container">
            <div class="col-md-6 ultra_content">
                <img src="img/mobile 3.png" style="width: 100%;  " alt="">
            </div>
            <div class="col-md-6 ultra_content2">
                <h1>Watch In Any Devices</h1>
                <br>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Pariatur labore, commodi perspiciatis. Id minus reprehenderit ad ipsum, earum qui quam ea at! Non praesentium sint perferendis, voluptas velit magni nobis.</p>
                <br>
                <a href="" class="btn btn-success">Learn More</a>
            </div>
        </div>
    </section>
    <hr>

    <section class="services">
        <div class="container">
            <div class="service_head">
                <h1>Our plan &amp; Pricing</h1>
                <hr style="width: 2%; border-radius: 3px; border: 3px solid green;">
                <div class="col-md-4"><i class="fa fa-check s_icon"></i> 1 Month Unlimited Access !</div>
                <div class="col-md-4"><i class="fa fa-check s_icon"></i> Stream On Your Phone , Laptop , Tablet and TV.</div>
                <div class="col-md-4"><i class="fa fa-check s_icon"></i> 1 Month Unlimited Access</div>
                <div class="col-md-4"><i class="fa fa-check s_icon"></i> Thousands of TV shows , movies &amp; more. </div>
                <div class="col-md-4"><i class="fa fa-check s_icon"></i> You can even download and watch online.</div>
                <div class="col-md-4"><i class="fa fa-check s_icon"></i> Thousands of TV shows , movies &amp; more. </div>
            </div>
            <br>
            <br>
            <div class="plan">
                <div>                   
                    <div class="col-md-4 pricing">
                        <div class="p_head">Free Trial</div>
                        <h2>Free</h2>
                        <p>Ultra HD Available</p>
                        <p>Watch on any device</p>
                        <p>20 Movies and shows</p>
                        <p><a href="" class="btn btn-lg btn-default"> Choose Plan</a></p>
                    </div>
                </div>
                <div>
                    <div class="col-md-4 pricing">
                        <div class="p_head">Starter</div>
                        <div class="p_title">
                            <span class="cd-currency">$</span>
                            <span class="cd-value">10</span>
                            <span class="cd-duration">/&nbsp;Month</span>
                        </div>
                        <p>Ultra HD Available</p>
                        <p>Watch on any device</p>
                        <p>70 Movies and shows</p>
                        <p><a href="" class="btn btn-lg btn-default"> Choose Plan</a></p>
                    </div>
                </div>
                <div>
                    <div class="col-md-4 pricing">
                        <div class="p_head">Premium</div>
                        <div class="p_title">
                            <span class="cd-currency">$</span>
                            <span class="cd-value">14</span>
                            <span class="cd-duration">/&nbsp;Month</span>
                        </div>
                        <p>Ultra HD Available</p>
                        <p>Watch on any device</p>
                        <p>Unlimited Movies and shows</p>
                        <p><a href="" class="btn btn-default btn-lg"> Choose Plan</a></p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <footer>
        <div class="container">
            <div class="col-md-6 pull-left">
                <p><i class="fa fa-copyright"></i> &nbsp; Copyright 2017 SKRN. All Rights Reserved</p>
            </div>
            <div class="col-md-6 ">
                <div class="social pull-right">
                    <ul>
                        <li><a href=""><i class="fa fa-facebook"></i></a></li>
                        <li><a href=""><i class="fa fa-twitter"></i></a></li>
                        <li><a href=""><i class="fa fa-instagram"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>
@endsection
