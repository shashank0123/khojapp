@extends('layouts.admin')

@section('content')
<h4 class="header-title m-t-0 m-b-30">Default Example</h4>
<h4 class="header-title m-t-0 m-b-30">SEOs</h4>
@if(Session::get('success'))
<div class="alert alert-success"> {{Session::get('success')}} </div>
@endif

@if (count($errors) > 0)
<div class="alert alert-danger">
	<ul>
		@foreach ($errors->all() as $error)
		<li>{{ $error }}</li>
		@endforeach
	</ul>
</div>
@endif

<form method="POST" action="{{url('admin/seo')}}" enctype="multipart/form-data">
	@csrf
	
	<div class="row input-control">
		<label class="col-sm-3">Tags/Keywords</label>
		<div class="col-sm-6">
			
			<input type="text" name="page_tags" id="page_tags" value="{{ $seo->page_tags ?? '' }}" class="form-control">
			<p>Use comma (,) as a separator</p>

		</div>
	</div>

	<div class="row input-control">
		<label class="col-sm-3">Page Title</label>
		<div class="col-sm-6">
			
			<input type="text" name="page_title" id="page_title" value="{{ $seo->page_title ?? '' }}" class="form-control">

		</div>
	</div>

	<div class="row input-control">
		<label class="col-sm-3">Page Descriptions</label>
		<div class="col-sm-6">
			
			<textarea name="page_description" id="page_description" class="form-control">{{ $seo->page_description ?? '' }}</textarea>
		</div>
	</div>

	<div class="row form-group">
		<div class="col-sm-6 col-sm-offset-5">
			<button type="submit" name="save" class="btn btn-primary">Save</button> 
		</div>
	</div>

</form>

@endsection