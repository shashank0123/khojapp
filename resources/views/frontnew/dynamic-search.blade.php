<?php
$count_search = 0;
?>

<style>
	.dynamic-li h6 { font-size: 15px }
	.ul-head { margin-top: 10px }
 	h6 { color: #fff; }
	.dynamic-li h6, .dynamic-li p { color: #fff; }
	.dynamic-li { margin: 10px 0px 3px; border-bottom: 1px solid #0a0a0a }
	ul{ margin: 5% }
	#search-mob { display: none; }
	
	@media screen and (max-width: 768px){
		#search-web { display: none; }
	#search-mob { display: block; }
	}
</style>
<div class="row">
	<div class="col-sm-8">
		
	<h6 class="ul-head">&nbsp;&nbsp;&nbsp;Movies & Tv Series</h6>
<ul>
	@if(!empty($contents->results))
	@foreach($contents->results as $element)
	@if($count_search>=5)
	<?php
	break;
	?>
	@else
	<?php $count_search++; ?>
	<li class="dynamic-li" style="height: 55px;">
		<div class="row">
			<div class="col-sm-3 col-xs-3" style="text-align: center">
				@if(isset($element->poster_path))
				<img src="https://image.tmdb.org/t/p/w500{{$element->poster_path}}" style="width: 35px; height: auto;">
				@else
				<img src="/images/default.png" alt="Bootstrap Touch Slider"  style="width: 35px; height: auto"/>
				@endif
			</div>
			<div class="col-sm-9 col-xs-9">
				@if($type == 'TV Serials')							
							<a  href="/tvseries/{{$element->id}}">
								@else
							<a  href="/movie/{{$element->id}}">
								@endif
				@if(!empty($element->title))
				<h6>{{$element->title}}</h6>
				<p>{{$element->release_date}}</p>
				@else
				<h6>{{$element->original_name}}</h6>
				<p>{{$element->first_air_date}}</p>
				@endif
			</a>
			</div>
		</div>
	</li>
	@endif
	@endforeach
	@endif

</ul>
	</div>
	<div class="col-sm-4">
		
	<h6 class="ul-head">People</h6>
<ul>
	@if(!empty($check_cast))
	@foreach($check_cast as $cast)
	
	
	<li class="dynamic-li"  style="height: 55px; border-bottom: 1px solid #0a0a0a">
		<a onclick="viewInSearch({{$cast->id}})" id="cast{{$cast->id}}">{{$cast->name}}</a>
	</li>
	
	@endforeach
	@endif

</ul>
	</div>
</div>


<div class="row" style="background-color: #006699;text-align: center; cursor: pointer;" onclick="getSearchPage()" id="search-web">
	<p style="padding:10px; font-size: 16px; color: #fff ; ">See all results for <i>"{{$keyword}}"</i></p>
</div>

<div class="row" style="background-color: #006699;text-align: center; cursor: pointer;" onclick="getSearchPage2()" id="search-mob">
	<p style="padding:10px; font-size: 16px; color: #fff ; ">See all results for <i>"{{$keyword}}"</i></p>
</div>