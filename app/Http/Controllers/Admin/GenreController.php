<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Genre;

class GenreController extends Controller
{
    public function showgenres(Request $request)
    {
        $genres = Genre::all();
        return view('admin.genrelist', compact('genres'));
    }
}
