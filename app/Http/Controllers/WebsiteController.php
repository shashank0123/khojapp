<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Crew;
use App\Movie;
use App\Cast;
use App\WatchList;
use App\Playlist;
use App\Favourite;
use App\Genre;
use App\Rating;
use DB;


class WebsiteController extends Controller
{
	public function getHomepage()
	{
		$data = [];
		return view('demo.index', compact('data'));
	}

	public function getList()
	{
		$data = [];
		return view('demo.index', compact('data'));
	}

	public function getMovieDetails()
	{
		$data = [];
		return view('demo.index', compact('data'));
	}

	public function getShowDetails()
	{
		$data = [];
		return view('demo.index', compact('data'));
	}

	public function getPlaylists()
	{
		$data = [];
		return view('demo.index', compact('data'));
	}

	public function getPlaylistDetails()
	{
		$data = [];
		return view('demo.index', compact('data'));
	}

	
}