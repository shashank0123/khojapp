@extends('layouts.admin')


@section('content')
<h4 class="header-title m-t-0 m-b-30">Default Example</h4>
<h4 class="header-title m-t-0 m-b-30">Edit Slider Detail</h4>
@if(Session::get('success'))
<div class="alert alert-success"> {{Session::get('success')}} </div>
@endif
@if (count($errors) > 0)
<div class="alert alert-danger">
	<ul>
		@foreach ($errors->all() as $error)
		<li>{{ $error }}</li>
		@endforeach
	</ul>
</div>
@endif

<form method="POST" action="{{url('admin/edit_slider/'.$slider->id)}}" enctype="multipart/form-data">
	@csrf
	
	<div class="row input-control">
		<div class="col-sm-12" style="text-align: center;">		
			
			<img src="{{asset($slider->poster)}}" style="height: 200px">

		</div>
	</div>

	<div class="row input-control">
		<label class="col-sm-3">Page Name</label>

		<div class="col-sm-6">
			<select name='page_title' id="page_title" class="form-control" required="required">
				<option value=""> -- SELECT PAGE NAME -- </option>	

				<option value="default" @if($slider->page_title == 'default'){{'selected'}}@endif>Default Image</option>	
				<option value="Home" @if($slider->page_title == 'Home'){{'selected'}}@endif>Home</option>		
				<option value="Trending" @if($slider->page_title == 'Trending'){{'selected'}}@endif>Trending</option>	
				<option value="Upcoming" @if($slider->page_title == 'Upcoming'){{'selected'}}@endif>Upcoming</option>	
				<option value="New" @if($slider->page_title == 'New'){{'new'}}@endif>New Release</option>	
				<option value="Free" @if($slider->page_title == 'Free'){{'selected'}}@endif>Free</option>	
				<option value="Viral" @if($slider->page_title == 'Viral'){{'selected'}}@endif>Viral Videos</option>	
				<option @if($slider->page_title == 'Recent'){{'selected'}}@endif value="Recent">Recently Added</option>	
				<option @if($slider->page_title == 'homeside'){{'selected'}}@endif value="homeside">Home Sidebar</option>	
				<option @if($slider->page_title == 'movielist'){{'selected'}}@endif value="movielist">Home Movielist</option>	
				<option @if($slider->page_title == 'moviedetailstitle'){{'selected'}}@endif value="moviedetailstitle">Side of Title Movie Details</option>	
				<option @if($slider->page_title == 'moviedetailscast'){{'selected'}}@endif value="moviedetailscast">Below Cast Movie Details</option>
				<option @if($slider->page_title == 'roadblock'){{'selected'}}@endif value="roadblock">Road Block</option>	
			</select>
		</div>
	</div>

	<div class="row input-control">
		<label class="col-sm-3">Banner Title</label>
		<div class="col-sm-6">
			
			<input type="text" name="image_title" id="image_title" class="form-control" value="{{ $slider->image_title ?? '' }}">

		</div>
	</div>

	<div class="row input-control">
		<label class="col-sm-3">Banner Image</label>
		<div class="col-sm-6">
			
			<input type="file" name="poster" id="poster" class="form-control">
			<h6>Banner size should be 1920*500 </h6>
		</div>
	</div>

	<div class="row input-control">
		<label class="col-sm-3">Redirect Url</label>
		<div class="col-sm-6">
			
			<input type="text" name="redirect_url" id="redirect_url" value="{{$slider->redirect_url ?? ''}}" class="form-control">

		</div>
	</div>

	<div class="row input-control">
		<label class="col-sm-3">Priorty</label>
		<div class="col-sm-6">
			
			<input type="text" name="display_priority" id="display_priority" value="{{$slider->display_priority ?? ''}}" class="form-control">

		</div>
	</div>

	<div class="row input-control">
		<label class="col-sm-3">Start Date</label>
		<div class="col-sm-6">
			
			<input type="date" name="start_date" id="start_date" value="{{$slider->start_date ?? ''}}" class="form-control" required="required">

		</div>
	</div><div class="row input-control">
		<label class="col-sm-3">End Date</label>
		<div class="col-sm-6">
			
			<input type="date" name="end_date" id="end_date" value="{{$slider->end_date ?? ''}}" class="form-control" required="required">

		</div>
	</div>

	<div class="row input-control">
		<label class="col-sm-3">Status</label>

		<div class="col-sm-6">
			
			<select name='status' id="status" class="form-control">
				<option value="Active" @if($slider->status == 'Active') {{'selected'}} @endif>Active</option>		
				<option value="Inactive" @if($slider->status == 'Inactive') {{'selected'}} @endif>Inactive</option>	
			</select>

		</div>
	</div>

	<div class="row form-group">
		<div class="col-sm-6 col-sm-offset-5">
			<button type="submit" name="save" class="btn btn-primary">Save</button> 
		</div>
	</div>



</form>


@endsection
