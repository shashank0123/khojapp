<head>
  <meta charset="UTF-8">
  <title>Skrn</title>
  
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<!-- <link rel="stylesheet" type="text/css" href="css/bootstrap-material-design.min.css"> -->
	<!-- <link rel="stylesheet" type="text/css" href="css/ripples.min.css"> -->
	<!-- <link rel="stylesheet" href="css/custom.css"> -->
	<link rel="stylesheet" href="css/style.css">
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome.min.css">
	<link rel="stylesheet" href="slick/slick.css">
	<link rel="stylesheet" href="slick/slick-theme.css">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  
     <link rel="stylesheet" href="css/test.css">

  
</head>
<nav>
	<div class="container">
		<div class="col-md-6">
			<a href="#" class="logo pull-left" style="position: relative;"><img src="img/SKRN logo.png" width="80" height="20" alt=""></a>
		</div>
		<div class="col-md-6 ">
			<a href="#" class="pull-right btn btn-default btn-sm" style="padding: 5px 15px 5px 15px; position: relative;top: 20px;">Sign in</a>
		</div>
	</div>
</nav>
<div class="col-md-12">
<div class="col-md-2"></div>
<div class="col-md-8">
	<div class="signup_head" style="text-align: center; margin-bottom: 50px;">
		<p>For One Low Monthly Price</p>
		<h1>Instantly  Watch TV Show &amp; Movies</h1>
	</div>
<!-- multistep form -->
<form id="msform">
	<!-- progressbar -->
	<ul id="progressbar" class="" style="width: 100%;">
		<li class="active">
			<div class="counter">
				
				<span>Step 1</span><br>
				<span>Choose Your Plan</span>
			</div>
		</li>
		<li>
			<div class="counter">
				<span>Step 2</span><br>
				<span>Create Your Account</span>
			</div>
		</li>
		<li>
			<div class="counter">
				<span>Step 3</span><br>
				<span>Billing Information</span>
			</div>
		</li>
		<li>
			<div class="counter">
				<span>Step 4</span><br>
				<span>Welcome To SKRN.</span>
			</div>
		</li>
	</ul>
	<!-- fieldsets -->
	<fieldset>
		{{-- <h2 class="fs-title">Create your account</h2>
		<h3 class="fs-subtitle">This is step 1</h3>
		<input type="text" name="email" placeholder="Email" />
		<input type="password" name="pass" placeholder="Password" />
		<input type="password" name="cpass" placeholder="Confirm Password" />
		<input type="button" name="next" class="next action-button" value="Next" /> --}}
		{{-- <div class="sign_plan" style="height: 200px;"> --}}
			<div class="plan_head">
				<h1 style="text-align: center;">Choose Your Plan</h1>
				<hr style="width: 20%;">
			</div>
                    <div class="col-md-4 col-sm-4 col-xs-4 pricing">
                        <div class="p_head">Free Trial</div>
                        <h2>Free</h2>
                        <p>Ultra HD Available</p>
                        <p>Watch on any device</p>
                        <p>20 Movies and shows</p>
                        <input type="button" name="next" class="next action-button" value="Next" /> 
                        {{-- <p><a href="" class="btn btn-lg btn-default next action-button"> Choose Plan</a></p> --}}
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-4 pricing">
                        <div class="p_head">Starter</div>
                        <div class="p_title">
                            <span class="cd-currency">$</span>
                            <span class="cd-value">10</span>
                            <span class="cd-duration">/&nbsp;Month</span>
                        </div>
                        <p>Ultra HD Available</p>
                        <p>Watch on any device</p>
                        <p>70 Movies and shows</p>
                        <input type="button" name="next" class="next action-button" value="Next" /> 
                        {{-- <p><a href="" class="btn btn-lg btn-default next action-button"> Choose Plan</a></p> --}}
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-4 pricing">
                        <div class="p_head">Premium</div>
                        <div class="p_title">
                            <span class="cd-currency">$</span>
                            <span class="cd-value">14</span>
                            <span class="cd-duration">/&nbsp;Month</span>
                        </div>
                        <p>Ultra HD Available</p>
                        <p>Watch on any device</p>
                        <p>Unlimited Movies and shows</p>
                        <input type="button" name="next" class="next action-button" value="Next" /> 
                        {{-- <p><a href="" class="btn btn-default btn-lg next action-button"> Choose Plan</a></p> --}}
                    </div>
            {{-- </div> --}}
	</fieldset>
	<fieldset>
		<div class="sign_plan col-md-12">
			<div class="col-md-8">
				<label for=""> Full Name :</label>
				<input type="text" name="name" placeholder="Enter Your Name" />
				<label for=""> Email :</label>
				<input type="email" name="email" placeholder="Enter Your Mail" />
				<label for=""> Password :</label>
				<input type="password" name="pass"  placeholder="Password" />
				<input type="password" name="c_pass"  placeholder="Confirm Password" />
				<label for=""> Country :</label>
				<select name="country" id="" class="form-control">
					<option value="">India</option>
					<option value="">India</option>
					<option value="">India</option>
					<option value="">India</option>
				</select>
				<input type="button" name="previous" class="previous action-button" value="Previous" />
				<input type="button" name="next" class="next action-button" value="Next" />
				
			</div>
		</div>
	</fieldset>
	<fieldset>
		<h2 class="fs-title">Personal Details</h2>
		<h3 class="fs-subtitle">We will never sell it</h3>
		<input type="text" name="fname" placeholder="First Name" />
		<input type="text" name="lname" placeholder="Last Name" />
		<input type="text" name="phone" placeholder="Phone" />
		<textarea name="address" placeholder="Address"></textarea>
		<input type="button" name="previous" class="previous action-button" value="Previous" />
		<input type="submit" name="submit" class="submit action-button" value="Submit" />
	</fieldset>
	<fieldset>
		<h1>hii</h1>
	</fieldset>
</form>
</div>
<div class="col-md-2"></div>
	
</div>

  <script src='js/jquery.js'></script>
  <script type="text/javascript" src="js/bootstrap.min.js" ></script> 
	<script src="js/ripples.min.js"></script>
	<!-- <script src="js/material.min.js"></script> -->
    <script src="slick/slick.min.js"></script>
    <link href='https://fonts.googleapis.com/css?family=Ubuntu:400,700' rel='stylesheet' type='text/css'>
{{-- <script src='js/fsvc.js'></script> --}}
 <script src="js/slider.js" type="text/javascript"></script>
<script type="text/javascript">jssor_1_slider_init();</script>

<!-- jQuery -->
<script src="http://thecodeplayer.com/uploads/js/jquery-1.9.1.min.js" type="text/javascript"></script>
<!-- jQuery easing plugin -->
<script src="http://thecodeplayer.com/uploads/js/jquery.easing.min.js" type="text/javascript"></script>
<script src='js/custom.js'></script>