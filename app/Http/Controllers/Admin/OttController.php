<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class OttController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page = 0;
        if(isset($_GET['page'])){
            $page = $_GET['page']-1;
        }
        $otts = DB::table('providers')->orderBy('display_priority','ASC')->paginate(15);

        return view('admin.ott.list',compact('otts','page'));

    }

    public function getSearchedResult(Request $request)
    {
        $page = 0;
        $keyword = $request->keyword;
        if(isset($_GET['page'])){
            $page = $_GET['page']-1;
        }
        $otts = DB::table('providers')->where('title','LIKE','%'.$keyword.'%')->orWhere('technical_name','LIKE','%'.$keyword.'%')->orderBy('display_priority','ASC')->paginate(15);

        return view('admin.ott.list',compact('otts','page','keyword'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $data = $request->all();

        $provider = DB::table('providers')->where('provider_id',$data['provider_id'])->update(
            array(
                'display_priority' => $data['display_priority'],
            )
        );

        return redirect()->back()->with('message','Data successfully updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
