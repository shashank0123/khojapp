@extends('layouts.admin')


@section('content')

<h4 class="header-title m-t-0 m-b-30">Languages</h4>
<div class="row">
    <div class="col-sm-6">
        <h4 class="header-title m-t-0 m-b-30">Languages</h4>
    </div>
    <div class="col-sm-6">
        <form action="{{url('admin/search_language')}}" method="GET">
            <div class="row">
                <div class="col-sm-8">
                    <input type="search" name="keyword" value="@if(!empty($keyword)){{'keyword'}}@endif" class="form-control"/>
                </div>
                <div class="col-sm-4">
                    <button type="submit" class="btn btn-primary">Search</button>
                </div>
            </div>
        </form>
    </div>
</div>
<h4 class="header-title m-t-0 m-b-30"></h4>
@if(Session::has('message'))
<div class="alert alert-success">{{ Session::get('message') }}</div>
@endif

<br><br>
<table id="datatable" class="table table-striped table-bordered">
    <thead>
        <tr>
            <th>id</th>
            <th>English Name</th>
            <th>Code</th>           
            <th>Priority</th>            
            <th>Status</th>            
            <th>Change Status</th>            
            <th>Action</th>
        </tr>
    </thead>

    <tbody>
        @php $i=1 @endphp
        @if(!empty($languages))
        @foreach ($languages as $language)
        
        <tr>
            <td>{{ $i + ($page*15) }}</td>
            <td>{{ $language->english_name ?? ''}}</td>
            <td>{{ $language->code ?? '' }}</td>
         <td>
            <input type="number" class="form-control" id="language{{$language->id}}" value="{{$language->display_priority ?? '0'}}" style="width: 50%">
         </td>   
         <td>{{$language->status ?? ''}}</td>         
         <td>
            @if($language->status=='Active')
            <a href="{{url('admin/change_status/Inactive/'.$language->id)}}" class="btn btn-danger">Inactive</a>
            @else
            <a href="{{url('admin/change_status/Active/'.$language->id)}}" class="btn btn-success">Active</a>
            @endif
        </td>         
         <td>
             <button class="btn btn-primary" onclick="getLanguagePriority({{$language->id}})">Save</button>
         </td>

     </tr>
     @php $i++ @endphp
     @endforeach
     @endif

 </tbody>
</table> 

@if($i==1)
<div class="row" style="text-align: center; padding: 50px">No Record Found</div>
@endif
<div class="row">
    {{$languages->links()}}
</div>










@endsection

