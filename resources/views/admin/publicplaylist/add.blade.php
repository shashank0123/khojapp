@extends('layouts.admin')


@section('content')
<h4 class="header-title m-t-0 m-b-30">Default Example</h4>
<h4 class="header-title m-t-0 m-b-30">@if(!empty($type)){{$type}}@else{{'Add'}}@endif  Playlist</h4>
@if(Session::get('success'))
<div class="alert alert-success"> {{Session::get('success')}} </div>
@endif
@if (count($errors) > 0)
<div class="alert alert-danger">
	<ul>
		@foreach ($errors->all() as $error)
		<li>{{ $error }}</li>
		@endforeach
	</ul>
</div>
@endif

<div class="row">
	<div class="col-sm-9">
		@if(!empty($type))
<form method="POST" action="{{url('admin/edit-playlist/'.$playlist->id)}}" enctype="multipart/form-data">
	@else
<form method="POST" action="{{url('admin/public_playlist')}}" enctype="multipart/form-data">

	@endif


	@csrf

	
	<div class="row input-control">
		<label class="col-sm-4">Title</label>
		<div class="col-sm-8">
			<input type="text" name="title" id="title" required="required" class="form-control" value="@if(!empty($playlist)){{ $playlist->playlist_name ?? '' }}@endif">
		</div>
	</div>

	<div class="row input-control">
		<label class="col-sm-4">Order</label>
		<div class="col-sm-8">
			<input type="text" name="order" id="order"  class="form-control" value="@if(!empty($playlist)){{ $playlist->playlist_order ?? '' }}@endif">
		</div>
	</div>

	

	<div class="row input-control">
		<label class="col-sm-4">List of movie id(comma separated)</label>
		<div class="col-sm-8">
			<textarea name="movie_id" id="movie_id" class="form-control" rows='5'>@if(!empty($playlist)){{ $playlist->playlist_content ?? '' }}@endif</textarea>
		</div>
	</div>

	<div class="row input-control">
		<label class="col-sm-4">Description of playlist</label>
		<div class="col-sm-8">
			<textarea name="description" id="description" class="form-control" rows='5'>@if(!empty($playlist)){{ $playlist->description ?? '' }}@endif</textarea>
		</div>
	</div>
	<div class="row input-control">
		<label class="col-sm-4">Poster</label>
		<div class="col-sm-8">
			<input type="file" name="poster" id="poster" class="form-control">
		</div>
	</div>
	


	

	<div class="form-group">
		<div class="col-sm-6 col-sm-offset-5">
			<button type="submit" name="save" class="btn btn-primary">Save</button> 
		</div>
	</div>


	</form>

	</div>
	<div class="col-sm-3">
		@if(!empty($playlist) && !empty($playlist->poster))
		<img src="{{ asset('poster/'.$playlist->poster) }}" style="width: 100%; height: auto">
		@endif
	</div>
</div>


	@endsection
@section('script')

<script>

  $(document).ready(function(){
    document.getElementById('output').innerHTML = location.search;
    $(".chosen-select").chosen()   
});

function selectDisable(){
  if (document.getElementById('all_users').checked){
    $('#users').prop('required',false);
  }
  else{
    $('#users').attr('disabled',true);
      }
}

</script>
@endsection()
