<?php 

if (isset(auth()->user()->profile_pic))
	$profile_pic = '/images/user/'.auth()->user()->profile_pic;
else {
	$profile_pic = '/images/default_user.png';
}


$username = explode(' ',Auth::user()->name);
$word = 0;
$count =0;

?>


@extends('layouts.khoj_new')

@section('content')
<style>
#clickBro { display: none; }
#file { display: none; }
.click-watch ul li {display: inline-block;padding-right: 10px; margin-bottom: 0}
.click-watch ul li img{width: 40px; height: 40px}
.click-watch ul li p{font-size: 10px}
.search-response { margin-bottom: 40px }
.breadcumb li { display: inline-block !important; }

@media screen and (min-width: 1300px){

	.movie-items .movie-item .mv-img { height: 190px; }
}

</style>

<div class="hero user-hero">
	@if($errors->any())
	<div class="alert alert-danger" style="position: absolute; top: 2px">
		@foreach($errors->all() as $error)
		<li style="color: #fff">{{ $error }}</li>
		@endforeach
	</div>
	@endif

	@if($message = Session::get('message'))
	<div class="btn btn-success" style="width: 100%; position: absolute; top: 2px">
		<p style="color: #fff">{{ $message }}</p>
	</div>
	@endif
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="hero-ct">
					<h1>{{$key}}</h1>
					<ul class="breadcumb">
						<li class="active"><a href="/">Home</a></li>
						<li> <span class="ion-ios-arrow-right"></span>{{$keyword}}</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="page-single">
	<div class="container">
		<div class="row ipad-width">
			<div class="col-md-3 col-sm-12 col-xs-12">
				<div class="user-information">
					<div class="user-img">

						<form method="post" action="/upload-image" enctype="multipart/form-data" id="upload_image">
							@csrf
							<input type="hidden" name="member_id" id="uid" value="{{auth()->user()->id}}">
							<a href="#"><img src="{{$profile_pic}}" alt="" style="width: 80%; margin: auto"><br></a>
							{{-- @if(!empty($profile->profimage))
								<span id="uploaded_image">
									<img src="/assetsss/images/AdminProduct/{{$profile->profimage}}" width="150px" height="150px">
								</span>
								@else
								<span id="uploaded_image"></span>
								@endif --}}
								<br>
								{{-- <img src="{{ asset('images/product/fashion-12.jpg') }}" style="width:100px;height:100px;border-radius:50%;margin: auto;display: block; margin-bottom:15px"> --}}
								<h6 for="file">Change Pic</h6>
								<input type="file" name="file" id="file" onchange='submitMe()'/>
								<input type="submit" name="submit" id="clickBro" />
							</form>

							
						</div>
						<div class="user-fav">
							<p onclick="showdropdown1()" style="cursor: pointer;">Account Details&nbsp;&nbsp;<i class="fa fa-angle-down"></i></p>
							<ul style="display: none;" id="dropdown1">
								<li><a href="/favourites/{{auth()->user()->id}}">&nbsp;&nbsp;&nbsp;Favourite movies</a></li>
								{{-- <li><a href="/playlists/{{auth()->user()->id}}">&nbsp;&nbsp;&nbsp;My Playlist</a></li> --}}
								<li><a href="/watchlists/{{auth()->user()->id}}">&nbsp;&nbsp;&nbsp;My WatchList</a></li>
							</ul>
						</div> 
						<div class="user-fav">
							<p onclick="showdropdown2()" style="cursor: pointer;">Settings&nbsp;&nbsp;<i class="fa fa-angle-down"></i></p>
							<ul style="display: none;" id="dropdown2">
								<li><a href="/profile">&nbsp;&nbsp;&nbsp;Profile</a></li>
								<li><a href="/reset-password">&nbsp;&nbsp;&nbsp;Change password</a></li>

							</ul>
						</div>

						<div class="user-fav">
							{{-- <p>Settings</p> --}}
							<ul>
								<li><a href="{{ route('logout') }}"
									onclick="event.preventDefault();
									document.getElementById('logout-form').submit();">Log out</a>
									<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
										{{ csrf_field() }}
									</form>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="col-md-9 col-sm-12 col-xs-12">
					<div class="form-style-1 user-pro" action="#">



						<div class="row" id="user-profile">
							@if (isset($contents))
							@foreach ($contents as $element)

							<?php
							$count =1;
							if($keyword == 'Favourite'){
								$word = $element->favourite_id;
							}
							if($keyword == 'PlayList'){
								$word = $element->playlist_id;
							}
							if($keyword == 'WatchList'){
								$word = $element->watchlist_id;
							}
							?>
							<div class="row" id="item{{$word}}">
								<div style="border: 1px solid #404040; background-color: #010c17; width: 98%; height: 250px; margin: 1% 1% 0 ; padding: 20px 10px 10px">
									<div class="col-sm-3 col-xs-4 search-response">

										<div class="movie-item">
											<div class="mv-img">
												@if(isset($element->poster))
												<img src="{{asset('poster/'.$element->poster)}}" alt=""  style="width: 200px; height: auto">
												@else
												<img src="/images/default.png" alt="" style="width: 200px; height: auto">
												@endif									

											</div>							
										</div>
									</div>
									<?php
									$genres = DB::table('content_genres')
									->leftJoin('genres','genres.genre_id','content_genres.genre_id')
									->where('content_genres.content_id',$element->content_id)
									->select('genres.genre_id','genres.title')
									->distinct('genre_id')
									->get();

									$providers = DB::table('content_providers')
									->where('content_id',$element->content_id)
									->get();

									$urlen = preg_replace('/[^a-zA-Z0-9_.]/', '_', (strtolower($element->title))).'-'.$element->content_id;

									?>
									<div class="col-sm-9 col-xs-8">
										<div style="position: absolute; top: 5px; right: 25px; padding: 5px 10px; border: 2px solid #fff; border-radius: 50%;  cursor: pointer; " onclick="deleteItem({{$word}},'{{$keyword}}')">
											<i class="fa fa-times" style="color: #fff"></i>
										</div>
										<div class="hvr-inner">
											<h5><a  href="/detail/{{$element->content_type}}/{{$urlen}}">{{$element->title}}</a></h5>
										</div>
										<div class="title-in">
											
											
											<p><br>{{$element->overview}}</p>
											<p><i class="ion-android-star"></i><span>&nbsp;&nbsp;&nbsp;@if(isset($element->tmdb_score)){{$element->tmdb_score}}@else{{'N/A'}}@endif</span> /10</p>
										</div>

										@if(!empty($providers))
										<div class="row ipad-width2">
											<br>
											<h6>Watch Now At</h6>
											<br>
											<div class="col-md-10 col-sm-10 col-xs-10">	
												<div class="click-watch">
													<ul>
														<?php $prev = ""?>
														@foreach($providers as $con)

														<?php
														if($con->provider_id == $prev){}
															else{ $providers = DB::table('providers')->where('provider_id',$con->provider_id)->first(); 
													}
													?>			
													@if($con->provider_id != $prev)
													<li>
														<a href="{{$con->web_url}}" target="blank">					
															<img src="{{asset('/asset'.$providers->icon_url)}}" class="image-responsive">
															<p style="margin-bottom: 0px">
																@if($con->monetization_type == 'flatrate' || $con->monetization_type == 'Flatrate' ){{'Subs'}}@else{{ucfirst($con->monetization_type)}}@endif <span style="color: yellow"> {{strtoupper($con->video_quality)}} </span></p>
															<p> {{$con->currency." ".$con->retail_price}}</p>
														</a>
													</li>
													@endif

													<?php $prev = $con->provider_id; ?>

													@endforeach
												</ul>
											</div>
										</div>
										<div class="col-md-1 col-sm-1 col-xs-1"></div>				
									</div>
									@endif

								</div>
							</div>
						</div>
						@endforeach
						@endif							

						@if($count == 0 )
						<div style="text-align: center;margin-top: 20%;
						margin-bottom: 20%; color: #fff; font-size: 32px">No Result Found</div>
						@endif
					</div>
				</div>

			</div>
		</div>
	</div>
</div>
</div>
<!-- footer section-->

<script>
	function submitMe() {
		$('#file').click();
		$("#clickBro").click();
	}

	function deleteItem(id , key){
		var CSRF_Token = $('meta[name="csrf-token"]').attr('content');

		$.ajax({
			type: "POST",
			url: "/delete-item",
			data:{ _token: CSRF_Token, id: id , key: key},
			success:function(data){
				$('#item'+id).hide();   
			}
		});
	}

	function showdropdown1() {
		$('#dropdown1').toggle();

	}

	function showdropdown2() {
		$('#dropdown2').toggle();

	}
</script>

@endsection