@extends('layouts.admin')


@section('content')

<h4 class="header-title m-t-0 m-b-30">Default Example</h4>
<h4 class="header-title m-t-0 m-b-30">PlayList</h4>
<a href="{{url('admin/public_playlist/add')}}" class="btn btn-primary">Add Playlist</a>
@if(Session::has('message'))
   <div class="alert alert-success">{{ Session::get('message') }}</div>
@endif
<table id="datatable" class="table table-striped table-bordered">
    <thead>
        <tr>
            <th>id</th>
            <th>Title</th>
            <th>Order</th>
            <th>Movies</th>
            <th>Action</th>
            
           
        </tr>
    </thead>

    <tbody>
    	@foreach ($playlists as $user)
        <tr>
            <td>{{ $user->id }}</td>
            <td>{{ $user->playlist_name ?? ''}}</td>
            <td>{{ $user->playlist_order ?? ''}}</td>
            <td>{{ $user->playlist_content ?? ''}}</td>
             <td>
           <a href="{{'/admin/edit-playlist/'.$user->id}}" class="btn btn-primary" ><i class="fa fa-edit"></i></a>
           <a href="{{'/admin/delete-playlist/'.$user->id}}" class="btn btn-danger" ><i class="fa fa-trash"></i></a>
           
           
       </td>
        </tr>
        @endforeach
       
    </tbody>
</table> 
                        
                        


                        


                        

                    
@endsection

