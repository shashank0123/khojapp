@extends('layouts.khoj_new')
@php $miny=1900; $maxy=date('Y'); $minr=0; $maxr=10; @endphp
@section('content')
<section id="page-bg-color">
	<div class="container">
		<div class="row">
			<div class="faq-sec1">
				<h2 class="head-title">CONTACT</h2>
				<h4>Contact Information</h4>

<p>E-399,<br>
Swami Vivekanand Marg,<br>
East Babarpur,<br>
New Delhi-110032</p>
<h4>Contact</h4>
<p>Mail: pankaj@digichoice.in<br>
Phone:  (+91) 9322854558</p>

<h4>Responsible Disclosure Policy</h4>
<p>We are dedicated to maintaining the security and privacy of the Khoj platform. We welcome security researchers from the community who want to help us improve our services.
If you discover a security vulnerability, please give us the chance to fix it by emailing us at example@khoj.com. Publicly disclosing a security vulnerability without informing us first puts the rest of the community at risk. When you notify us of a potential problem, we will work with you to make sure we understand the scope and cause of the issue.
Thank you for your work and interest in making our service safer and more secure!</p>
 <h4>Content & Copyright</h4>
<p>Khoj is using the following sources:<br><br>

This product uses the TMDb API but is not endorsed or certified by TMDb.Rating Icons made by Baianat from www.flaticon.com</p>

			</div>
		</div>
	</div>
</section>


<script src="//code.jquery.com/jquery.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
	
@endsection