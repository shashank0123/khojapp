@extends('layouts.admin')


@section('content')
<h4 class="header-title m-t-0 m-b-30">Default Example</h4>
<h4 class="header-title m-t-0 m-b-30">Add Movie/Show</h4>
@if(Session::get('success'))
<div class="alert alert-success"> {{Session::get('success')}} </div>
@endif
@if (count($errors) > 0)
<div class="alert alert-danger">
	<ul>
		@foreach ($errors->all() as $error)
		<li>{{ $error }}</li>
		@endforeach
	</ul>
</div>
@endif

@if(!empty(session()->get('search')))
<div class="row">
		<div class="col-sm-3"></div>
		 <div class="col-sm-6" style="text-align: right;">
    <a href="{{url('admin/global_search')}}" class="btn btn-primary">Back To previous page</a>
  </div>
	</div>
	@endif

<form method="POST" action="{{url('admin/edit-content/'.$movie->content_id)}}" enctype="multipart/form-data">
	@csrf

	<div class="row input-control">
		<label class="col-sm-3">Content Type</label>
		<div class="col-sm-6">
			<select name="content_type" id="content_type" required="required" class="form-control">

				<option value="movie" @if($movie->content_type == 'movie'){{'selected'}}@endif>{{'Movie'}}</option>
				<option value="tv-show" @if($movie->content_type == 'tv-show'){{'selected'}}@endif>{{'TV Show'}}</option>
				
			</select>
		</div>
	</div>

	<div class="row input-control">
		<label class="col-sm-3">Title</label>
		<div class="col-sm-6">
			<input type="text" name="title" id="title" required="required" class="form-control"  value="{{$movie->title ?? ''}}">
		</div>
	</div>

	<div class="row input-control">
		<label class="col-sm-3">Release Date/Year</label>
		<div class="col-sm-6">
			<input type="date" name="cinema_release_date" id="cinema_release_date"  class="form-control" value="{{$movie->cinema_release_date ?? ''}}">
		</div>
	</div>

	<div class="row input-control">
		<label class="col-sm-3">Runtime ( in minutes )</label>
		<div class="col-sm-6">
			<input type="text" name="runtime" id="runtime"  class="form-control" value="{{$movie->runtime ?? ''}}">
		</div>
	</div>

	<div class="row input-control">
		<label class="col-sm-3">Popularity</label>
		<div class="col-sm-6">
			<input type="text" name="popularity" id="popularity"  class="form-control" value="{{$movie->tmdb_popularity ?? '0'}}">
		</div>
	</div>

	<div class="row input-control">
		<label class="col-sm-3">IMDB Score</label>
		<div class="col-sm-6">
			<input type="text" name="imdb_score" id="imdb_score" required="required" class="form-control" value="{{$movie->imdb_score ?? '0'}}">
		</div>
	</div>

	<div class="row input-control">
		<label class="col-sm-3">TMDB Score</label>
		<div class="col-sm-6">
			<input type="text" name="tmdb_score" id="tmdb_score" required="required" class="form-control" value="{{$movie->tmdb_score ?? '0'}}">
		</div>
	</div>

<div class="row input-control">
		<label class="col-sm-3">Certification</label>
		<div class="col-sm-6">
			<select name="age_certification" id="age_certification" required="required" class="form-control">

				<option value=""> - Select Age Certifictaion - </option>
				<option value="U" @if($movie->age_certification == 'U'){{'selected'}}@endif>U</option>
				<option value="A" @if($movie->age_certification == 'A'){{'selected'}}@endif>A</option>
				<option value="UA" @if($movie->age_certification == 'UA'){{'selected'}}@endif>UA</option>
				
			</select>
		</div>
	</div>

	<div class="row input-control">
		<label class="col-sm-3">Overview</label>
		<div class="col-sm-6">
			<textarea name="short_description" id="short_description" class="form-control" rows='5'>{{$movie->short_description ?? ''}}</textarea>
		</div>
	</div>

	<div class="row input-control">
		<label class="col-sm-3">Tags</label>
		<div class="col-sm-6">
			<textarea name="tags" id="tags" class="form-control" rows='3' >{{$movie->tags ?? ''}}</textarea>
			<h6>Use comma (,) for separator</h6>
		</div>
	</div>


	<div class="row input-control">
		<label class="col-sm-3">Page Title</label>
		<div class="col-sm-6">
			<textarea name="meta_title" id="meta_title" class="form-control" rows='2'>{{$movie->meta_title ?? ''}}</textarea>
			</div>
	</div>

	<div class="row input-control">
		<label class="col-sm-3">Page Description</label>
		<div class="col-sm-6">
			<textarea name="meta_description" id="meta_description" class="form-control" rows='4'>{{$movie->meta_description ?? ''}}</textarea>
			</div>
	</div>





	<div class="row input-control">
		<label class="col-sm-3">Poster</label>
		<div class="col-sm-6">
			<input type="file" name="poster" id="poster" class="form-control">
		</div>
	</div>

	<div class="form-group">
		<div class="col-sm-6 col-sm-offset-5">
			<button type="submit" name="save" class="btn btn-primary">Save</button> 
		</div>
	</div>


	</form>


	@endsection
@section('script')

<script>

  $(document).ready(function(){
    document.getElementById('output').innerHTML = location.search;
    $(".chosen-select").chosen()   
});

function selectDisable(){
  if (document.getElementById('all_users').checked){
    $('#users').prop('required',false);
  }
  else{
    $('#users').attr('disabled',true);
      }
}

</script>
@endsection()
