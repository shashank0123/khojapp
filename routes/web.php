<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//New Routes
Route::get('/updateIt', 'KhojNewController@updateReleaseYear' );
Route::get('/', 'KhojNewController@getHome' );
Route::get('/home', 'KhojNewController@getHome' )->name('home');
Route::get('/react2', 'KhojVodiController@getVodi' )->name('home');
Route::get('/react2/genre/{title}', 'KhojVodiController@getVodiList' )->name('home');
Route::get('/react2/episode', 'KhojVodiController@getVodiDetail' )->name('home');
Route::get('/react2/movies/category/drama', 'KhojVodiController@getVodiCategory' )->name('home');
Route::get('/react2/category/movie', 'KhojVodiController@getVodiList' )->name('home');
Route::get('/react2/{type}/{title}/', 'KhojVodiController@getVodiDetail' )->name('home');
Route::get('/react2/{type}/{title}/{id}', 'KhojVodiController@getVodiDetail' )->name('home');
Route::get('/react2/movies', 'KhojVodiController@getVodiList' )->name('home');
Route::get('/react2/movies/title', 'KhojVodiController@getVodiList' )->name('home');
Route::get('/trending', 'KhojNewController@getTrendingMovies');
Route::get('/new-release', 'KhojNewController@getNewMovies');
Route::get('/newest', 'KhojNewController@getLatestMovies');
Route::get('/recently', 'KhojNewController@recentlyAdded');
Route::get('/upcoming', 'KhojNewController@getUpcomingMovies');
Route::get('/playlists', 'KhojNewController@playlists');
Route::get('/playlists/details/{title}', 'KhojNewController@playlistDetails');
Route::get('/viral', 'KhojNewController@getViralMovies');
Route::get('/free', 'KhojNewController@getFreeMovies');
Route::get('top-10/movies/{score}', 'KhojNewController@getTopMovies');
Route::get('top-10/tv-shows/{score}', 'KhojNewController@getTopShows');
Route::get('top-10/viral-videos/{score}', 'KhojNewController@getTopVideos');
Route::get('/free', 'KhojNewController@getFreeMovies');
Route::get('/free', 'KhojNewController@getFreeMovies');
Route::get('/detail/{type}/{id}', 'KhojNewController@getMovie' );
Route::get('/languages', 'KhojNewController@languages');
Route::get('/search', 'KhojNewController@getSearch');
Route::post('/dynamic-search', 'KhojNewController@getDynamicSearch');
Route::get('/home/language/{data}', 'KhojNewController@getLanguageSearch');
Route::post('forgot_password', 'Auth\ResetPasswordController@password');

Route::get('reset_password/{email}/{token}', 'Auth\ResetPasswordController@getResetPage');

Route::get('/updateCertification', 'KhojNewController@updateCertification');
Route::get('/getmxplayerdata', 'JustWatchCrawlerController@getmxplayerdata');
Route::get('/testreleasedateupdate', 'JustWatchCrawlerController@testreleasedateupdate');
Route::get('/getairteldata', 'JustWatchCrawlerController@getairteldata');
Route::get('/getvodafonedata', 'JustWatchCrawlerController@getvodafonedata');
Route::get('/updatevodafoneposter', 'JustWatchCrawlerController@updatevodafoneposter');
Route::get('/findduplicatedata', 'JustWatchCrawlerController@findduplicatedata');
Route::get('/getulludata', 'JustWatchCrawlerController@getulludata');
Route::get('/gettmdbdata', 'JustWatchCrawlerController@gettmdbdata');
Route::get('/updatetmdbid', 'JustWatchCrawlerController@updatetmdbid');
Route::get('/updatetmdbmovie', 'JustWatchCrawlerController@updatetmdbmovie');
Route::get('/updatetmdbtv', 'JustWatchCrawlerController@updatetmdbtv');
Route::get('/updatetmdbmoviecast', 'JustWatchCrawlerController@updatetmdbmoviecast');
Route::get('/addmxprovider', 'JustWatchCrawlerController@addmxprovider');
Route::get('/updatecastid', 'JustWatchCrawlerController@updatecastid');
Route::get('/updatedata', 'JustWatchCrawlerController@updatedata');
Route::get('/updatecast', 'JustWatchCrawlerController@updatecast');
Route::get('/changemoviestatus', 'JustWatchCrawlerController@changeMovieStatus');
Route::get('/feedback', 'KhojNewController@submitFeedback');


Route::get('/casts/{url}', 'KhojNewController@getCastDetail');

Route::get('/redirect/{facebook}', 'SocialController@redirect');
Route::get('/callback/{facebook}', 'SocialController@callback');

Route::get('/redirect/{google}', 'SocialController@redirect');
Route::get('/callback/{google}', 'SocialController@callback');

Route::post('show-episodes', 'KhojNewController@showEpisodes');


Route::get('/countContent', 'KhojNewController@getCountContent' );




Route::post('checkEmail', 'KhojNewController@checkEmail');
Route::post('checkMobile', 'KhojNewController@checkMobile');

// Route::get('/test', function(){
//     return view('test');
// });




//Old Routes
// Route::get('/', 'KhojController@getHome' );
// Route::get('/trending', 'KhojController@getTrendingMovies');
// Route::get('/new-release', 'KhojController@getNewMovies');
// Route::get('/newest', 'KhojController@getLatestMovies');
// Route::get('/upcoming', 'KhojController@getUpcomingMovies');
// Route::get('/free', 'KhojController@getFreeMovies');
// Route::get('/{type}/{id}', 'KhojController@getMovie' );
// Route::get('/search', 'KhojController@getSearch');
// Route::post('/dynamic-search', 'KhojController@getDynamicSearch');






Route::get('auth/{provider}', 'Auth\LoginController@redirectToProvider');
Route::get('auth/{provider}/callback', 'Auth\LoginController@handleProviderCallback');
Route::get('/verifyemail/{token}', 'Auth\RegisterController@verify');


Route::get('/khojle', 'JustWatchCrawlerController@crawl' );
Route::get('/selectmovie', 'KhojNewController@selectbasicmovie' );
Route::get('/selectott', 'KhojNewController@selectbasicott' );
Route::get('/updateuserpreference', 'KhojNewController@updateuserpreference' );
Route::get('/khojledetails', 'JustWatchCrawlerController@getmoviedetails' );
Route::get('/getdatasorted', 'JustWatchCrawlerController@getdatasorted' );
Route::get('/slugname', 'JustWatchCrawlerController@slugname' );
Route::get('/about-us', function(){
    $type = 'movie';
    return view('frontnew.about',compact('type'));
} );
Route::get('/contact-us', function(){
    $type = 'movie';
    return view('frontnew.contact',compact('type'));
} );
Route::get('/frequently-asked-questions', function(){
    $type = 'movie';
    return view('frontnew.faq',compact('type'));
} );
Route::get('/privacy-policy', function(){
    $type = 'movie';
    return view('frontnew.privacy',compact('type'));
} );







Route::post('/series', 'KhojController@getTvSeries');

Route::post('/search-filter', 'KhojController@getSearchFilter');

Route::get('/tvseries/{id}', 'KhojController@getSingleTseries');
Route::post('/submit-newsletter', 'KhojController@submitNewsletter');

Route::get('/forgot-password', 'UserController@getForgetPassword');
Route::post('/send-reset-password-link', 'UserController@sendResetPasswordLink');

Route::post('/check-user', 'UserController@checkUser');






Route::get('/new', function(){ return view('auth/reg');} );


// Route::get('/', 'ViewController@getmain_home');
// Route::get('/genre/{genre}', 'ViewController@getgenre');
// Route::get('/movies/popular', 'ViewController@getmain_home');
// Route::get('/movies/newest', 'ViewController@getnew_home');
// Route::get('/movies/upcoming', 'ViewController@getcoming_home');
// Route::get('/tvseries', 'ViewController@getmain_hometv');
// Route::get('movie_detail/{id}', 'ViewController@getmovie_detail');
// Route::get('tv_detail/{id}', 'ViewController@gettv_detail');
// Route::get('signupform', function () {  return view('signupform');});
// Route::get('newhome', function () {  return view('new-home');});
// Route::get('user-profile', function () {  return view('user-profile');});
// Route::get('new-home', function () {  return view('new-home');});
// Route::get('playlists', 'ViewController@playlists');
Route::get('savegenre', 'ViewController@savegenre');

// Route::get('/', 'FrontViewController@get_home');
// Route::get('/search', 'FrontViewController@getSearch');
// Route::get('/movies-list', 'FrontViewController@getAllMovies');
// Route::get('/movies-list/latest', 'FrontViewController@getLatestMovies');
// Route::get('/movies-list/upcoming', 'FrontViewController@getUpcomingMovies');
// Route::get('/movies-list/popular', 'FrontViewController@getPopularMovies');
// Route::get('/movies-list/trending', 'FrontViewController@getTrendingMovies');
// Route::get('/movie-detail/{id}', 'FrontViewController@getSingleMovie');
// Route::get('/shows-list', 'FrontViewController@getAllShows');
// Route::get('/show-detail', 'FrontViewController@getSingleShow');
// Route::get('/genres/{genre}', 'FrontViewController@getAllGenres');
// Route::get('/genre-detail', 'FrontViewController@getSingleGenre');
// Route::get('/t-series', 'FrontViewController@getTseries');
// Route::get('/all-channels', 'FrontViewController@getAllChannels');
// Route::get('/channel-shows', 'FrontViewController@getChannelShows');
// Route::get('/tseries-detail/{id}', 'FrontViewController@getSingleTseries');




// Route::get('/', 'ViewController@getmain_home');
// Route::get('/genre/{genre}', 'ViewController@getgenre');
// Route::get('/movies/popular', 'ViewController@getmain_home');
// Route::get('/movies/newest', 'ViewController@getnew_home');
// Route::get('/movies/upcoming', 'ViewController@getcoming_home');
// Route::get('/tvseries', 'ViewController@getmain_hometv');
// Route::get('movie_detail/{id}', 'ViewController@getmovie_detail');
// Route::get('tv_detail/{id}', 'ViewController@gettv_detail');
// Route::get('signupform', function () {  return view('signupform');});
// Route::get('newhome', function () {  return view('new-home');});
// Route::get('user-profile', function () {  return view('user-profile');});
// Route::get('new-home', function () {  return view('new-home');});
// Route::get('playlists', 'ViewController@playlists');
// Route::get('savegenre', 'ViewController@savegenre');

Route::post('/upload-image', 'UserController@uplaodProfileImage');
Route::post('/delete-item', 'UserController@deleteFavourite');

Auth::routes();
// Route::get('/', 'FrontViewController@get_home');


// Route::get('/', 'HomeController@index')->name('home');
Route::get('/profile', 'UserController@profile')->name('profile')->middleware('auth');
Route::get('/reset-password', 'UserController@getResetPassword')->name('profile')->middleware('auth');
Route::get('/profile/{$tab}', 'UserController@profileview')->name('profileview')->middleware('auth');
Route::get('/favourites/{id}', 'UserController@favourite')->name('favourite')->middleware('auth');
Route::get('/watchlists/{id}', 'UserController@watchlist')->name('watchlist')->middleware('auth');



Route::get('/playlists/{id}', 'UserController@playlist')->name('playlist')->middleware('auth');


Route::get('/account', 'UserController@account')->name('account')->middleware('auth');
Route::post('/account', 'UserController@saveaccount')->middleware('auth');
Route::get('/favourite/{type}/{id}', 'UserController@savefavourite')->middleware('auth');
Route::get('/watchlist/{type}/{id}', 'UserController@savewatchlist')->middleware('auth');
Route::get('/playlist/{type}/{id}', 'UserController@saveplaylist')->middleware('auth');
Route::post('/save-rating', 'UserController@saveRating')->middleware('auth');
Route::get('/passwordUpdate/{id}', 'UserController@passwordUpdate')->middleware('auth');
Route::get('/update-profile', 'UserController@updateProfile')->middleware('auth');

// Route::get('search', 'ViewController@search');

Route::get('final-home', function () {  return view('finaldesign/index-2');});
Route::get('dashboard-home', function () {  return view('finaldesign/dashboard-home');});
Route::get('dashboard-movie-profile', function () {  return view('finaldesign/dashboard-movie-profile');});
Route::get('dashboard-account-payment', function () {  return view('finaldesign/dashboard-account-payment');});
Route::get('dashboard-account', function () {  return view('finaldesign/dashboard-account');});
Route::get('dashboard-coming-soon', function () {  return view('finaldesign/dashboard-coming-soon');});
Route::get('dashboard-favorites', function () {  return view('finaldesign/dashboard-favorites');});
Route::get('dashboard-movies', function () {  return view('finaldesign/dashboard-movies');});
Route::get('dashboard-playlists', function () {  return view('finaldesign/dashboard-playlists');});
Route::get('dashboard-new-arrivals', function () {  return view('finaldesign/dashboard-new-arrivals');});
Route::get('dashboard-profile', function () {  return view('finaldesign/dashboard-profile');});
Route::get('faqs', function () {  return view('finaldesign/faqs');});
Route::get('index-2', function () {  return view('finaldesign/index-2');});
Route::get('signup-welcome', function () {  return view('finaldesign/signup-welcome');});

Route::get('admin_area', ['middleware' => 'admin', function () {
    //
}]);


Route::prefix('admin')->middleware('admin')->group(function () {
    Route::get('dashboard', 'Admin\UserController@showhome');
    Route::get('users', 'Admin\UserController@showusers');

    Route::get('seo', 'Admin\UserController@getSeo');
    Route::post('seo', 'Admin\UserController@postSeo');
    
    Route::get('genres', 'Admin\GenreController@showgenres');
    Route::get('movie_feature/{feature}/{movie_id}', 'Admin\MovieController@changefeature');
    Route::resource('country', 'Admin\adminCountryController');

    Route::get('public_playlist', 'Admin\publicPlaylistController@playlist');
    Route::get('public_playlist/add', 'Admin\publicPlaylistController@addplaylist');
    Route::post('public_playlist', 'Admin\publicPlaylistController@saveplaylist');

    Route::get('edit-playlist/{id}', 'Admin\publicPlaylistController@editplaylist');
    Route::post('edit-playlist/{id}', 'Admin\publicPlaylistController@updateplaylist');
    Route::get('delete-playlist/{id}', 'Admin\publicPlaylistController@deleteplaylist');
    Route::get('movies', 'Admin\MovieController@getMovies');
    Route::get('tv-shows', 'Admin\MovieController@getShows');
    Route::get('viral-videos', 'Admin\MovieController@getViralVideos');
    Route::get('tv-seasons/{show_id}', 'Admin\MovieController@getSeasons');
    Route::get('tv-episodes/{season_id}', 'Admin\MovieController@getEpisodes');
    Route::get('add_content/{type}', 'Admin\MovieController@addContent');
    Route::post('store_content', 'Admin\MovieController@storeContent');
    Route::get('edit-content/{id}', 'Admin\MovieController@editContent');
    Route::post('edit-content/{id}', 'Admin\MovieController@updateContent');
    Route::get('delete-content/{id}', 'Admin\MovieController@deleteContent');
    Route::get('favourite/{id}', 'Admin\MovieController@setfavouriteContent');

    Route::get('add_content/{type}/{id}', 'Admin\MovieController@addShowType');

    Route::post('store_season/{type}/{show_id}', 'Admin\MovieController@storeShow');
    Route::post('store_episode/{type}/{season_id}', 'Admin\MovieController@storeShow');

    Route::get('content_genre/{content_id}', 'Admin\MovieController@addContentGenre');
    Route::post('store_genre/{content_id}', 'Admin\MovieController@storeContentGenre');
    Route::get('delete_genre/{content_id}/{genre_id}', 'Admin\MovieController@deleteContentGenre');

    Route::get('content_casts/{content_id}', 'Admin\MovieController@addContentCast');
    Route::post('store_cast/{content_id}', 'Admin\MovieController@storeContentCast');
    Route::get('delete_cast/{content_id}/{person_id}', 'Admin\MovieController@deleteContentCast');

    Route::get('content_clips/{content_id}', 'Admin\MovieController@addContentClip');
    Route::post('store_clip/{content_id}', 'Admin\MovieController@storeContentClip');
    Route::get('delete_clip/{content_id}/{clip_id}', 'Admin\MovieController@deleteContentClip');

    Route::get('content_banners/{content_id}', 'Admin\MovieController@addContentBanner');
    Route::post('store_banner/{content_id}', 'Admin\MovieController@storeContentBanner');
    Route::get('delete_banner/{content_id}', 'Admin\MovieController@deleteContentBanner');

    Route::get('content_providers/{content_id}', 'Admin\MovieController@addContentProvider');
    Route::post('store_provider/{content_id}', 'Admin\MovieController@storeContentProvider');
    // Route::get('delete_provider/{content_id}/{provider_id}', 'Admin\MovieController@deleteContentProvider');



    Route::get('edit_provider/{content_id}/{provider_id}/{video_quality}', 'Admin\MovieController@editContentProvider');

    Route::post('edit_provider/{content_id}/{provider_id}/{video_quality}', 'Admin\MovieController@updateContentProvider');

    Route::get('delete_provider/{content_id}/{provider_id}/{video_quality}', 'Admin\MovieController@deleteContentProvider');

    Route::get('status/{content_id}/{provider_id}/{status}', 'Admin\MovieController@updateStatusContentProvider');

    //Add Free Movies
    Route::get('free-movies', 'Admin\MovieController@getFreeMovies');
    Route::get('movie/free', 'Admin\MovieController@createFreeMovie');
    Route::post('free', 'Admin\MovieController@storeFreeMovie');

    Route::get('sliders','Admin\SliderController@index');
    Route::get('add_slider','Admin\SliderController@create');
    Route::post('add_slider','Admin\SliderController@store');
    Route::get('edit_slider/{id}','Admin\SliderController@edit');
    Route::post('edit_slider/{id}','Admin\SliderController@update');
    Route::get('delete_slider/{id}','Admin\SliderController@destroy');

        //For OTT
    Route::get('ott','Admin\OttController@index');
    Route::post('set_priority','Admin\OttController@update');
    Route::get('search_ott','Admin\OttController@getSearchedResult');

    // For Language
    Route::get('languages','Admin\LanguageController@index');
    Route::post('language_priority','Admin\LanguageController@update');
    Route::get('search_language','Admin\LanguageController@getSearchedResult');
    Route::get('change_status/{status}/{id}','Admin\LanguageController@changeStatus');


    Route::get('global_search','Admin\MovieController@globalSearch');
    Route::get('search_result','Admin\MovieController@getSearchedResult');

    // For Company's Detail
    Route::get('setting/website','Admin\SettingController@getCompanyDetail');
    Route::post('setting/website','Admin\SettingController@storeCompanyDetail');

    // For fb detail
    Route::get('setting/facebook','Admin\SettingController@getFacebookDetail');
    Route::post('setting/facebook','Admin\SettingController@storeFacebookDetail');

    // For google detail
    Route::get('setting/google','Admin\SettingController@getGoogleDetail');
    Route::post('setting/google','Admin\SettingController@storeGoogleDetail');
    Route::get('export', 'BulkUploadController@export')->name('export');
    Route::get('importExportView', 'BulkUploadController@importExportView');
    Route::post('import', 'BulkUploadController@import')->name('import');
});
Route::get('/clear-cache', function() {
    Artisan::call('cache:clear');
    return "Cache is cleared";
});




Route::get('demo','WebsiteController@getHomepage');
Route::get('demo/list/{type}','WebsiteController@getList');
Route::get('demo/movies/{slug}','WebsiteController@getMovieDetails');
Route::get('demo/tv-shows/{slug}','WebsiteController@getShowDetails');
Route::get('demo/playlists','WebsiteController@getPlaylists');
Route::get('demo/playlists/{slug}','WebsiteController@getPlaylistDetails');

