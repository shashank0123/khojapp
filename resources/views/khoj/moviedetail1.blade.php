@extends('layouts.khoj_new')
@section('tags'){{$movie->tags ?? ''}}@endsection
@section('title'){{$movie->meta_title ?? ''}}@endsection
@section('description'){{$movie->meta_description ?? ''}}@endsection
<style type="text/css"></style>
@section('banner')
<div class="row ipad-width2">
   <div class="col-md-12 col-sm-12 col-xs-12" id="banner-section">
      @if($errors->any())
      <div class="alert alert-danger" style="position: absolute; top: 2px">
         @foreach($errors->all() as $error)
         <li style="color: #fff">{{ $error }}</li>
         @endforeach
      </div>
      @endif
      @if($message = Session::get('message'))
      <div class="btn btn-success" style="width: 100%; position: absolute; top: 2px">
         <p style="color: #fff">{{ $message }}</p>
      </div>
      @endif
      @if(!empty($clips[0]))
      <div class="movie-img ">
         <iframe width="100%" height="350px" src="@if(!empty($clips[0]))https://www.youtube.com/embed/{{$clips[0]->external_id}}@endif" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
      </div>
      @else
      @section('banner')
      @if(!empty($banners) && count($banners)>0)
      <div id="bootstrap-touch-slider" class="carousel bs-slider fade  control-round indicators-line" data-ride="carousel" data-pause="hover" data-interval="false" >
         <!-- Indicators -->
         <ol class="carousel-indicators">
            <?php $i=0; ?>
            @foreach($banners as $banner)
            <li data-target="#bootstrap-touch-slider" data-slide-to="{{$i}}" class="@if($i==0){{'active'}}@endif"></li>
            <?php $i++; ?>
            @endforeach
         </ol>
         <!-- Wrapper For Slides -->
         <div class="carousel-inner" role="listbox" style="margin-top: 0; max-height: 350px; width: auto;">
            <?php $i=0; ?>
            @foreach ($banners as $banner)
            <!-- Third Slide -->
            <div class="item @if($i==0){{'active'}}@endif" style="max-height: 350px; width: auto;">
               <!-- Slide Background -->
               @if(!empty($banner->banner_url))
               <img src="{{asset('banners/'.$banner->banner_url)}}" alt=""  class="slide-image" style="max-height: 350px; width: auto;"/>
               @else
               <img src="/images/default.png" alt=""  class="slide-image"/>
               @endif
               <div class="bs-slider-overlay"></div>
            </div>
            <!-- End of Slide -->
            <?php $i++; ?>
            @endforeach
         </div>
         <!-- <a class="left carousel-control" href="#bootstrap-touch-slider" role="button" data-slide="prev">
            <span class="fa fa-angle-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
            </a> -->
         <!-- Right Control -->
         <!-- <a class="right carousel-control" href="#bootstrap-touch-slider" role="button" data-slide="next">
            <span class="fa fa-angle-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
            </a> -->
      </div>
      @endif
      @endsection
      @endif
   </div>
</div>
@endsection
@section('content')
<div class="page-single movie-single movie_single">
   <div class="container">
      <div class="row">
         <div class="col-sm-12" style="text-align: left;">
            <?php 
               $get11 = ""; $get22 = "" ; $get33 = "";
               $name1 = ""; $name2 = "" ; $name3 = "";
               $link = "<a href='/'> HOME </a>";
               
               $get1 = DB::table('contents')->where('content_id',$movie->parent_id)->first();
               
               if(!empty($get1)){
               	$urlen = preg_replace('/[^a-zA-Z0-9_.]/', '_', (strtolower($get1->title))).'-'.$get1->content_id;
               	$get11 = "/detail/".$get1->content_type."/".$urlen;
               	$name1 = $get1->title ?? '';
               
               
               	$get2 = DB::table('contents')->where('content_id',$get1->parent_id)->first();
               
               	if(!empty($get2)){
               		$urlen = preg_replace('/[^a-zA-Z0-9_.]/', '_', (strtolower($get2->title))).'-'.$get2->content_id;
               		$get22 = "/detail/".$get2->content_type."/".$urlen;
               		$name2 = $get2->title ?? '';
               
               		$get3 = DB::table('contents')->where('content_id',$get2->parent_id)->first();
               
               		if(!empty($get3)){
               			$urlen = preg_replace('/[^a-zA-Z0-9_.]/', '_', (strtolower($get3->title))).'-'.$get3->content_id;
               			$get33 = "/detail/".$get3->content_type."/".$urlen;
               			$name3 = $get3->title ?? '';
               		}
               	}
               }
               
               ?>
            <h5 style="font-size: 14px;">
               <a href="{{url('/')}}">&nbsp;&nbsp;&nbsp;&nbsp; HOME</a> @if($get33!="") > <a href="{{$get33}}">{{$name3}}</a>@endif @if($get22!="") > <a href="{{$get22}}">{{$name2}}</a>@endif @if($get11!="") > <a href="{{$get11}}">{{$name1}}</a>@endif > <a href="{{url()->current()}}"><span style="color: #ffffff">{{$movie->title ?? ''}}</span></a>
            </h5>
            <br>
         </div>
      </div>
   </div>
   <div class="container">
      @php $miny=1900; $maxy=date('Y'); $minr=0; $maxr=10; @endphp
      <div class="row ">
         <div class="col-md-12 col-sm-12 col-xs-12 ">
            <div class="container row"  id="movie-detail">
               <div class="col-sm-4" style="text-align: center;">
                  @if(!empty($movie->poster) && strpos($movie->poster, 'poster/') == false)
                  <img src="/poster/{{$movie->poster}}" alt="" class="poster-image web-image">
                  @else
                  <img src="/images/default.png" alt="" class="web-image">
                  @endif
                  <div class="row">
                     <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="synopsys" onclick="getSynopsys()">
                           <h4>Synopsys <i class="fa fa-angle-down"></i></h4>
                           <div class="synopsys-list" style="cursor: pointer; max-height: 220px; overflow: auto; color: white;">
                              {{$movie->short_description ?? 'No Description Available'}}
                           </div>
                        </div>
                        <div class="cast" onclick="getCast()">
                           <h4><br>Cast <i class="fa fa-angle-down"></i></h4>
                           <div class="cast-list" style="cursor: pointer; max-height: 300px; overflow-y: auto">
                              <div class="row">
                                 @if(!empty($casts))
                                 @php $ci=0 @endphp 
                                 @foreach($casts as $cast)
                                 <div style="min-height: 110px;" class="col-sm-4 col-xs-4 cast-info" >
                                    <?php $urlen = preg_replace('/[^a-zA-Z0-9_.]/', '_', (strtolower($cast->name))).'-'.$cast->person_id; ?>
                                    <a href="{{url('casts/'.$urlen)}}">
                                    @if(!empty($cast->poster))
                                    <img src="{{$cast->poster}}" style="width: 60%; height: 67px; margin:0 10%; border-radius: 50%;">
                                    @else
                                    <img src="{{asset('images/default_user.png')}}" style="width: 60%; height: auto; margin:0 10%">
                                    @endif
                                    {{substr($cast->name ?? '',0,14)}}  </a>
                                    <!-- ({{$cast->role ?? ''}})&nbsp;&nbsp;&nbsp;   -->
                                 </div>
                                 @php $ci++ @endphp
                                 @endforeach
                                 @if($ci==0)
                                 {{'No Cast Available'}}
                                 @endif
                                 @endif
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-sm-8">
                  <div class="movie-detail">
                     @if(!empty($movie->title))
                     <h1 style="margin-bottom: 2px" class="bd-hd">{{$movie->title}} </h1>
                     @elseif(!empty($movie->original_title))
                     <h1 style="margin-bottom: 2px" class="bd-hd">{{$movie->original_title}} </h1>
                     @endif
                     <div style="font-size: 14; color: #9ba09b">
                         @if(!empty($movie->cinema_release_date) && $movie->cinema_release_date != '0000-00-00')
                         <i class="ion-calculator" style="font-size: 14px;"></i> {{explode('-',$movie->cinema_release_date)[0] }}

                        @elseif(!empty($movie->localized_release_date)  && $movie->localized_release_date != '0000-00-00')
                        <i class="ion-calculator" style="font-size: 14px; "></i> {{explode('-',$movie->localized_release_date)[0]}}

                        @elseif($movie->original_release_year) 
                        <i class="ion-calculator" style="font-size: 14px; "></i> {{$movie->original_release_year}} 

                        @endif &nbsp;&nbsp;&nbsp;


                        <?php
                           if(!empty($movie->runtime)){
                           	$hours = floor($movie->runtime / 60);
                           	$min = $movie->runtime - ($hours * 60);
                              ?>

                              <i class="ion-clock" style="font-size: 14px; "></i> 
   
                              <?php
                           	echo $hours." hrs ".$min." mins";
                           }
                           
                           ?>
                     </div>
                     <div class="rating">
                        <ul>
                           @if($movie->content_type)
                           <li style="font-size: 12px; color: #9ba09b;">  {{strtoupper($movie->content_type)}}</li>
                           @endif

                           @if($movie->imdb_score)
                           <li style="font-size: 12px; color: #9ba09b;">  IMDB&nbsp;-   &nbsp;   {{$movie->imdb_score}}</li>
                           @endif

                           @if($movie->tmdb_score)
                           <li style="font-size: 12px; color: #9ba09b;">  RATING&nbsp;-&nbsp;&nbsp;{{$movie->tmdb_score}}</li>
                           @endif 

                           <li style="font-size: 12px; color: #9ba09b">@if(!empty($movie->age_certification))
                              <span style=" color: #9ba09b">{{$movie->age_certification}}</span>
                              @else
                              <span style=" color: #9ba09b">{{'U/A'}}</span>
                              @endif
                           </li>
                        </ul>
                     </div>
                     <div class="movie-genre">
                        <ul>
                           @if(!empty($genres))
                           @foreach($genres as $genre)
                           @if(!empty($genre->name))
                           <li>{{$genre->name}}</li>
                           @endif
                           @endforeach
                           @endif
                        </ul>
                     </div>
                     <div class="social-btn" id="hide-icon">
                        <div class="container row" style="padding-left: 0px;">
                           <div class="col-sm-9">
                              @if (!empty($favourite))
                              <a href="#" class="parent-btn yellow" title="Already in your favourite list"><i class="ion-heart yellow-icon"></i></a>
                              @else
                              <a href="/favourite/{{'Movie'}}/{{$movie->content_id}}" class="parent-btn" title="Add to favourite"><i class="ion-heart"></i></a>
                              @endif
                              @if (!empty($watchlist))						
                              <a href="#" class="parent-btn yellow" title="Already in your watchlist"><i class="ion-plus yellow-icon"></i></a>
                              @else
                              <a href="/watchlist/{{'Movie'}}/{{$movie->content_id}}" class="parent-btn" title="Add to watchlist"><i class="ion-plus"></i></a>
                              @endif
                              <?php $urlen = preg_replace('/[^a-zA-Z0-9_.]/', '_', (strtolower($movie->title))).'-'.$movie->content_id; ?>
                              <a href="https://www.facebook.com/sharer/sharer.php?u={{url('movie')}}/{{$urlen}}"><i class="ion-social-facebook" style=" background-color: #006699; color: #fff; padding: 8px 14px; border-radius: 50%; font-size: 14px; margin-right: 9px"></i></a>&nbsp;
                              <a href="http://www.twitter.com/intent/tweet?url={{url('movie')}}/{{$urlen}}"><i class="ion-social-twitter" style=" background-color: #00ACED; color: #fff; padding: 8px 10px; border-radius: 50%; font-size: 14px"></i></a>&nbsp;
                           </div>
                        </div>
                     </div>
                     @if(!empty($providers) && count($providers)>0)
                     <div class="row ipad-width2 app-icons">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                           <h4>
                           Watch Now</h5>
                           <div class="app-get">
                              <div class="row">
                                 <div class=""></div>
                                 <?php $i = 0; $prev = ""; $qlty = "";?>
                                 <h4>Stream</h4>
                                 @foreach($providers as $con)
                                 <?php
                                    $providers1 = DB::table('providers')->where('provider_id',$con->provider_id)->first(); 
                                    if(!empty($providers1)){
                                    	
                                    	if ($con->retail_price <= 0 || $con->monetization_type == 'flatrate' || $con->monetization_type == 'Flatrate' ||  $con->monetization_type == 'free'){ 
                                    		if($prev == $con->provider_id ){
                                    			continue;
                                    		}
                                    		?>
                                 @if ($con->retail_price < 1 && $i == 0)
                                 <?php $i = 1; ?>
                                 <br>
                                 @endif
                                 <div class="col-sm-1 col-xs-1 col-md-1" style="background-color: #555; width: 9%; margin-left: 2%; margin-bottom: 5px">
                                    <a href="{{$con->web_url}}" target="blank">
                                       <img src="{{asset('/asset'.$providers1->icon_url)}}" class="app-img">
                                       <p>@if ($con->retail_price > 0) {{$con->retail_price}}@elseif($con->monetization_type == 'flatrate' || $con->monetization_type == 'Flatrate'){{'Subs'}}@else{{ucfirst($con->monetization_type)}}@endif <span style="color: yellow"> {{strtoupper($con->video_quality)}} </span>
                                       </p>
                                    </a>
                                 </div>
                                 <?php 
                                    $qlty = $con->video_quality;
                                    $prev = $con->provider_id;
                                    }
                                    } ?>
                                 @endforeach
                                 @if($i==0)
                                 <h6 style="margin-top: 10px">No source available right now to watch </h6>
                                 @endif
                                 <?php $i = 0; $prev = ""; $qlty = "";?>
                                 <h4><br><br>Buy<br></h4>
                                 @foreach($providers as $con)
                                 <?php
                                    $providers1 = DB::table('providers')->where('provider_id',$con->provider_id)->first(); 
                                    if(!empty($providers1)){
                                    	if ($con->retail_price > 0 ){ 
                                    		
                                    		?>
                                 @if ($con->retail_price >1 )
                                 <?php $i = 1; ?>
                                 @endif
                                 <div class="col-sm-1 col-xs-1 col-md-1" style="background-color: #555; width: 9%; margin-left: 2%; margin-bottom: 5px">
                                    <a href="{{$con->web_url}}" target="blank">
                                       <img src="{{asset('/asset'.$providers1->icon_url)}}" class="app-img">
                                       <p>@if ($con->retail_price > 0) {{$con->retail_price}}@elseif($con->monetization_type == 'flatrate' || $con->monetization_type == 'Flatrate'){{'Subs'}}@else{{ucfirst($con->monetization_type)}}@endif <span style="color: yellow"> {{strtoupper($con->video_quality)}} </span>
                                       </p>
                                    </a>
                                 </div>
                                 <?php 
                                    $qlty = $con->video_quality;
                                    $prev = $con->provider_id;
                                    }
                                    } ?>
                                 @endforeach
                                 @if($i==0)
                                 <h6 style="margin-top: 10px">No source available right now to watch </h6>
                                 @endif
                              </div>
                           </div>
                        </div>
                     </div>
                     @endif
                     @if($movie->content_type  == 'show')
                     <!-- For Seasons -->
                     <div class="row ipad-width2 trailors">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                           <h3>Seasons</h3>
                           <div class="row">
                              <div class="slick-multiItem">
                                 <?php $count_m = 0; ?>
                                 @if(!empty($seasons))
                                 @foreach($seasons as $season)
                                 <?php
                                    $release_date="";
                                    if(!empty($season->cinema_release_date))
                                    	$release_date = explode('-',$season->cinema_release_date);
                                    $count_m=1;
                                    ?>
                                 <?php $urlen = preg_replace('/[^a-zA-Z0-9_.]/', '_', (strtolower($season->title))).'-'.$season->content_id; ?>
                                 <div class="slide-it">
                                    <!-- <a onclick="showEpisodes({{$season->content_id}})"> -->
                                    <a href="/detail/{{$season->content_type}}/{{$urlen}}">
                                       <div class="movie-item">
                                          <div class="mv-img">
                                             @if(!empty($season->poster) && strpos($season->poster, 'poster/') == false)
                                             <img src="/poster/{{$season->poster}}" alt="" style="width: auto; height: 250px">
                                             @else
                                             <img src="/images/default.png" alt="" style="width: auto; height: 250px">
                                             @endif
                                          </div>
                                          <div class="hvr-inner">
                                          </div>
                                          <div class="title-in">
                                             <p><i class="ion-android-star"></i><span>@if($season->tmdb_score){{$season->tmdb_score}}@else{{'0'}}@endif</span> /10</p>
                                          </div>
                                       </div>
                                       <div class="row">
                                          <h6>
                                             <!-- <a onclick="showEpisodes({{$season->content_id}})"> -->
                                    <a href="/detail/{{$season->content_type}}/{{$urlen}}">
                                    <br>
                                    &nbsp;&nbsp;@if(!empty($season->original_title)){{substr($season->original_title,0,30)}}@elseif(!empty($season->title)){{substr($season->title,0,30)}}@endif</a></h6>
                                    <div class="col-sm-4">IMDB - @if($season->tmdb_score){{$season->tmdb_score}}@else{{'0'}}@endif</div>
                                    <div class="col-sm-4">RT</div>
                                    <div class="col-sm-4">@if(!empty($season->age_certification)){{$season->age_certification}}@else{{'U/A'}}@endif</div>
                                    </div>
                                    </a>
                                 </div>
                                 @endforeach
                                 @endif
                              </div>
                           </div>
                           @if($count_m == 0)
                           <div style="text-align: left;margin: 20px; color: #ffffff">No Seasons Available</div>
                           @endif
                        </div>
                     </div>
                     @endif
                     @if($movie->content_type == 'tv-season' )
                     <div class="row ipad-width2 trailors">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                           <h4>{{$season_one}}</h4>
                           <div class="row">
                              <div class="slick-multiItem">
                                 <?php $count_m = 0; ?>
                                 @if(!empty($episodes))
                                 @foreach($episodes as $episode)
                                 <div class="slide-it">
                                    <a onclick="({{$episode->content_id}})">
                                       <div class="row episode-details"  >
                                          <span>
                                    <a href="/detail/{{$episode->content_type}}/{{$urlen}}">
                                    {{$season_one}} 
                                    @if (!empty($episode->episode_number)){{'E'.$episode->episode_number}}@endif 
                                    @if(!empty($episode->original_title))
                                    {{substr($episode->original_title,0,20)}}...
                                    @elseif(!empty($episode->title))
                                    {{substr($episode->title,0,20)}}...
                                    @endif
                                    </a>
                                    </span>
                                    <br>
                                    <a onclick="watchStream({{$episode->content_id}})">
                                    <span>You can watch this on  &nbsp;&nbsp;<i class="fa fa-angle-right"></i></span>
                                    </a>
                                    </div>
                                    <div class="show_stream" id="stream{{$episode->content_id}}" style="display: none;">
                                       <?php
                                          $episode_providers = DB::table('content_providers')
                                          ->where('content_id',$episode->content_id)
                                          ->get();
                                          ?>
                                       @if(!empty($episode_providers) && $movie->content_type == 'movie')
                                       <div class="row ipad-width2 app-icons">
                                          <div class="col-md-12 col-sm-12 col-xs-12">
                                             <div class="app-get">
                                                <div class="row">
                                                   <?php $prev = ""?>
                                                   @foreach($episode_providers as $con)
                                                   <?php
                                                      $providers1 = DB::table('providers')->where('provider_id',$con->provider_id)->first();
                                                      ?>
                                                   <div class="col-sm-12">
                                                      <a href="{{$con->web_url}}" target="blank">
                                                         <img src="{{asset('/asset'.$providers1->icon_url)}}" class="app-img">
                                                         <p>{{ucfirst($con->monetization_type)}} <span style="color: yellow"> {{strtoupper($con->video_quality)}} </span><br>@if($con->monetization_type == 'flatrate'){{'Subscsription'}}@else {{$con->currency." ".$con->retail_price}}@endif</p>
                                                      </a>
                                                   </div>
                                                   <?php $prev = $con->provider_id; ?>
                                                   @endforeach
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       @endif
                                    </div>
                                 </div>
                                 </a>
                                 @endforeach
                                 @endif
                              </div>
                              @if($count_m == 0)
                              <div style="text-align: left;margin: 20px; color: #ffffff">No Episode Available</div>
                              @endif
                           </div>
                        </div>
                     </div>
                     @endif
                     @if($movie->content_type == 'show' )
                     <div class="row ipad-width2 trailors">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                           <h4>{{$season_one}}</h4>
                           <div class="row">
                              <div class="slick-multiItem">
                                 <?php $count_m = 0; ?>
                                 @if(!empty($episodes))
                                 @foreach($episodes as $episode)
                                 <div class="slide-it">
                                    <a onclick="({{$episode->content_id}})">
                                       <div class="row episode-details"  >
                                          <span >
                                    <a href="/detail/{{$episode->content_type}}/{{$urlen}}">
                                    {{$season_one}} 
                                    @if (!empty($episode->episode_number)){{'E'.$episode->episode_number}}@endif 
                                    @if(!empty($episode->original_title))
                                    {{substr($episode->original_title,0,20)}}...
                                    @elseif(!empty($episode->title))
                                    {{substr($episode->title,0,20)}}...
                                    @endif
                                    </a>
                                    </span>
                                    <br>
                                    <a onclick="watchStream({{$episode->content_id}})">
                                    <span>You can watch this on  &nbsp;&nbsp;<i class="fa fa-angle-right"></i></span>
                                    </a>
                                    </div>
                                    <div class="show_stream" id="stream{{$episode->content_id}}" style="display: none;">
                                       <?php
                                          $episode_providers = DB::table('content_providers')
                                          ->where('content_id',$episode->content_id)
                                          ->get();
                                          ?>
                                       @if(!empty($episode_providers) && $movie->content_type == 'movie')
                                       <div class="row ipad-width2 app-icons">
                                          <div class="col-md-12 col-sm-12 col-xs-12">
                                             <div class="app-get">
                                                <div class="row">
                                                   <?php $prev = ""?>
                                                   @foreach($episode_providers as $con)
                                                   <?php
                                                      $providers1 = DB::table('providers')->where('provider_id',$con->provider_id)->first();
                                                      ?>
                                                   <div class="col-sm-12">
                                                      <a href="{{$con->web_url}}" target="blank">
                                                         <img src="{{asset('/asset'.$providers1->icon_url)}}" class="app-img">
                                                         <p>{{ucfirst($con->monetization_type)}} <span style="color: yellow"> {{strtoupper($con->video_quality)}} </span><br>@if($con->monetization_type == 'flatrate' && $con->retail_price == '0'){{'Subs'}}@else {{$con->currency." ".$con->retail_price}}@endif</p>
                                                      </a>
                                                   </div>
                                                   <?php $prev = $con->provider_id; ?>
                                                   @endforeach
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       @endif
                                    </div>
                                 </div>
                                 @endforeach
                                 @endif
                              </div>
                              @if($count_m == 0)
                              <div style="text-align: left;margin: 20px; color: #ffffff">No Episode Available</div>
                              @endif
                           </div>
                        </div>
                        @endif
                        <?php $videoc = 0; ?>
                        @if(!empty($clips))
                        <div class="row ipad-width2 trailors">
                           <div class="col-md-12 col-sm-12 col-xs-12">
                              <h4>
                                 <br><br>
                                 VIDEOS: TRAILERS, TEASERS, FEATURETTES<br><br>
                              </h4>
                              <div class="row">
                                 @foreach($clips as $clip)
                                 <?php $videoc++; ?>
                                 <div class="col-sm-6"><iframe src="https://www.youtube.com/embed/@if(!empty($clip->external_id)){{$clip->external_id}}@endif" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen style="border: 5px solid rgba(20,20,20,0.5); width: 100%; height: auto"></iframe></div>
                                 @endforeach
                              </div>
                              @if($videoc == 0)
                              <div style="text-align: left;margin: 20px; color: #ffffff">No Video Available</div>
                              @endif
                           </div>
                        </div>
                        @endif
                        <div class="row ipad-width2 trailors">
                           <div class="col-md-12 col-sm-12 col-xs-12">
                              <h4 style="margin-top: 35px">People also liked<br><br></h4>
                              <div class="row">
                                 <div class="slick-multiItem">
                                    <?php $count_m = 0; ?>
                                    @if(!empty($related_movies))
                                    @foreach($related_movies as $movie)
                                    <?php
                                       $release_date="";
                                       if(!empty($movie->cinema_release_date))
                                       	$release_date = explode('-',$movie->cinema_release_date);
                                       $count_m=1;
                                       ?>
                                    <?php $urlen = preg_replace('/[^a-zA-Z0-9_.]/', '_', (strtolower($movie->title))).'-'.$movie->content_id; ?>
                                    <div class="slide-it">
                                       <a  href="/detail/{{$movie->content_type}}/{{$urlen}}">
                                          <div class="movie-item">
                                             <div class="mv-img" style="padding-left: 10%;
                                                padding-right: 10%;" >
                                                @if(!empty($movie->poster) && strpos($movie->poster, 'poster/') == false)
                                                <img src="/poster/{{$movie->poster}}" alt="" class="web-image">
                                                @else
                                                <img src="/images/default.png" alt="" class="web-image">
                                                @endif
                                             </div>
                                             <div class="hvr-inner">
                                             </div>
                                             <div class="title-in">
                                                <p><i class="ion-android-star"></i><span>@if($movie->tmdb_score){{$movie->tmdb_score}}@else{{'0'}}@endif</span> /10</p>
                                             </div>
                                          </div>
                                          <div class="">
                                             <h6>
                                                &nbsp;&nbsp;@if(!empty($movie->original_title)){{substr($movie->original_title,0,30)}}@elseif(!empty($movie->title)){{substr($movie->title,0,30)}}@endif
                                             </h6>
                                             <div class="col-sm-4">IMDB - @if($movie->tmdb_score){{$movie->tmdb_score}}@else{{'0'}}@endif</div>
                                             <div class="col-sm-4">RT</div>
                                             <div class="col-sm-4">@if(!empty($movie->age_certification)){{$movie->age_certification}}@else{{'U/A'}}@endif</div>
                                          </div>
                                       </a>
                                    </div>
                                    @endforeach
                                    @endif
                                 </div>
                              </div>
                              @if($count_m == 0)
                              <div style="text-align: left;margin: 0px 20px; color: #ffffff">No movie/show Available
                              </div>
                              @endif
                              <div class="clearfix"></div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <!-- Related Movies & Shows -->
      </div>
   </div>
</div>
<script type="text/javascript">
   function getSynopsys(){
   	$('.synopsys-list').toggle();
   }
   
   function getCast(){
   	// $('.cast-list').toggle();
   }
   
   function getCrew(){
   	$('.crew-list').toggle();
   }
   
   function getReviews(){
   	$('.reviews-list').toggle();
   }
   
   function watchStream(id){
   	$('#show_stream'+id).toggle();
   }
   
   
</script>
@endsection