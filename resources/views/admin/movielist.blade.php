@extends('layouts.admin')


@section('content')

<h4 class="header-title m-t-0 m-b-30">Default Example</h4>
<div class="row">
    <div class="col-sm-6">
        <h4 class="header-title m-t-0 m-b-30">{{$type}}</h4>
    </div>
    <!-- <div class="col-sm-6">
        <form action="{{url('admin/search_result')}}" method="GET">
            <div class="row">
                <div class="col-sm-8">
                    <input type="hidden" name="content_type" value="@if(!empty($content_type)){{$content_type}}@else{{'movie'}}@endif" class="form-control"/>
                    <input type="search" name="keyword" class="form-control"/>
                </div>
                <div class="col-sm-4">
                    <button type="submit" class="btn btn-primary">Search</button>
                </div>
            </div>
        </form>
    </div> -->
</div>

@if(Session::has('message'))
<div class="alert alert-success">{{ Session::get('message') }}</div>
@endif
@if(!empty($show_id))
<a href="{{url('admin/add_content/tv-season/'.$show_id)}}" class="btn btn-primary">Add Season</a>
@endif
@if(!empty($season_id))
<a href="{{url('admin/add_content/tv-episode/'.$season_id)}}" class="btn btn-primary">Add Episode</a>
@endif

<br><br>
<table id="datatable" class="table table-striped table-bordered">
    <thead>
        <tr>
            <th>id</th>
            <th>Name</th>
            <th>Release Year</th>

            @if(!empty($content_type) && $content_type == 'tv-show')
            <th>No of Seasons</th>
            @endif

            @if(!empty($content_type) && $content_type == 'tv-season')
            <th>No of Episodes</th>
            @endif
            <th>Poster</th>
            
            <th>Add</th>
            <th>Action</th>
            

        </tr>
    </thead>

    <tbody>
        @php $i=1 @endphp
        @if(!empty($movies))
        @foreach ($movies as $movie)
        @php $i++ @endphp
        <tr>
            <td>{{ $movie->content_id ?? ''}}</td>
            <td>{{ $movie->original_title ?? ''}}</td>
            <td>{{ $movie->original_release_year ?? ''}}</td>
            @if(!empty($content_type) && $content_type == 'tv-show')
            <td>
                @php 
                echo $count = DB::table('contents')->where('parent_id',$movie->content_id)->count();
                @endphp
                {{ $movie->total_seasons ?? ''}}

            </td>
            @endif

            @if(!empty($content_type) && $content_type == 'tv-season')
            <td>
                @php 
                echo $count = DB::table('contents')->where('parent_id',$movie->content_id)->count();
                @endphp
                {{ $movie->total_seasons ?? ''}}

            </td>
            @endif
            <td>
             @if(!empty($movie->poster) && strpos($movie->poster, 'poster/') == false )
             <img src="/poster/{{$movie->poster}}" style="height: 100px; width: auto;"/>
             @else
             <img src="/images/default.png" style="height: 150px; width: auto;"/>
             @endif
         </td>
         <td>
            <a href="{{'/admin/content_genre/'.$movie->content_id}}" class="btn btn-primary" style="width: 100px; margin-bottom: 5px">Genres</a> <br>
            <a href="{{'/admin/content_casts/'.$movie->content_id}}" class="btn btn-danger" style="width: 100px; margin-bottom: 5px">Casts</a> <br>
            <a href="{{'/admin/content_clips/'.$movie->content_id}}" class="btn btn-success" style="width: 100px; margin-bottom: 5px">Clips</a> <br>
            <a href="{{'/admin/content_banners/'.$movie->content_id}}" class="btn btn-primary" style="width: 100px; margin-bottom: 5px">Banners</a> <br>
            <a href="{{'/admin/content_providers/'.$movie->content_id}}" class="btn btn-danger" style="width: 100px; margin-bottom: 5px">Providers</a>

            @if(!empty($movie->content_type) && ($movie->content_type == 'tv-show' || $movie->content_type == 'show' || $movie->content_type == 'shows' ) )
            <br>
            <a href="{{'/admin/tv-seasons/'.$movie->content_id}}" class="btn btn-success" style="width: 100px; margin-bottom: 5px">Seasons</a>
            @endif
            
            @if( !empty($movie->content_type) && ($movie->content_type == 'tv-season' || $movie->content_type == 'season' ) )
            <br>
            <a href="{{'/admin/tv-episodes/'.$movie->content_id}}" class="btn btn-success" style="width: 100px; margin-bottom: 5px">Episodes</a>
            @endif


        </td>
        <td>
           <a href="{{'/admin/edit-content/'.$movie->content_id}}" class="btn btn-primary" ><i class="fa fa-edit"></i></a>
           <a href="{{'/admin/delete-content/'.$movie->content_id}}" class="btn btn-danger" ><i class="fa fa-trash"></i></a>
           <form action="{{'/admin/favourite/'.$movie->content_id}}">
               <input type="text" value="{{$movie->featured}}" name="priority">
           <button href="{{'/admin/favourite/'.$movie->content_id}}" class="btn btn-success" ><i class="fa fa-star"></i></button>
           </form>
           
       </td>

   </tr>
   @endforeach
   @endif

</tbody>
</table> 

@if($i==1)
<div class="row" style="text-align: center; padding: 50px">No Record Found</div>
@endif
<div class="row">
    {{$movies->links()}}
</div>










@endsection

