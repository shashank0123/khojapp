<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTvShowEpisodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tv_show_episodes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('episode_name')->nullable();
            $table->string('episode_number')->nullable();
            $table->string('short_desc')->nullable();
            $table->text('description')->nullable();
            $table->string('show_id')->nullable();
            $table->string('season_number')->nullable();
            $table->string('season_id')->nullable();
            $table->string('poster')->nullable();
            $table->string('backdrop_path')->nullable();
            $table->string('release_date')->nullable();
            $table->string('status')->default('1');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tv_show_episodes');
    }
}
