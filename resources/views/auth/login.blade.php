@extends('registernew')
<style>
    #login-blade { box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);  margin-bottom: 50px}
    #login-card { padding: 30px; }
    #login-card .card-header { color: #000; font-size: 28px ; font-weight: bold; margin-bottom: 30px}
    .social-icon-login a { color: #fff }
    .social-icon-login i { left:30px!important; }
    #content-pro { background-color: #ffffff!important  }

</style>
@section('content')

<div id="content-pro">                   
    @if($message = Session::get('credentials'))
    <div class="btn btn-danger" style="width: 100%;  position: absolute; top: 102px">
        <p style="color: #fff; font-size: 20px">{{ $message }}</p>
    </div>
    @endif
    <div id="pricing-plans-background-image">
        <div class="container">
            <div class="row justify-content-center" style="margin-top: 90px">
                <div class="col-md-3"></div>
                <div class="col-md-6" id="login-blade">
                    <div class="card" id="login-card">
                        <div class="card-header">{{ __('Login') }}</div>

                        <div class="card-body">
                            <form method="POST" action="{{ route('login') }}">
                                @csrf

                                <div class="form-group row">
                                    <label for="email" class="col-sm-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                                    <div class="col-md-6">
                                        <input id="email_id" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>
                                        <h6 id="username-result"></h6>

                                        @if ($errors->has('email'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                                    <div class="col-md-6">
                                        <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
                                        <h6 id="password-result"></h6>

                                        @if ($errors->has('password'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-md-6 offset-md-4">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> {{ __('Remember Me') }}
                                            </label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row mb-0">
                                    <div class="col-md-8 offset-md-4">
                                        <button type="button" class="btn btn-primary" onclick="checkLogin()">
                                            {{ __('Login') }}
                                        </button>

                                        <button type="submit" id="login-submit2" class="btn btn-primary" style="display: none;">
                                            {{ __('Login') }}
                                        </button>

                                        <a class="btn btn-link" href="{{ route('password.request') }}">
                                            {{ __('Forgot Your Password?') }}
                                        </a>
                                    </div>
                                </div>
                            </form>
                            <div style="text-align: center;">OR</div>
                            <div class="modal-body-pro social-login-modal-body-pro">
                               <div class="registration-social-login-options">
                                <h6>Sign in with your social account</h6>
                                <div class="row">
                                    <div class="col-sm-6 col-xs-12">
                                        <div class="social-icon-login facebook-color"><a href="{{ url('redirect/facebook') }}" style="color: #fff"><i class="fab fa-facebook-f"></i> Facebook</a>
                                        </div>
                                    </div>
                                   {{--  <div class="col-sm-4 col-xs-12">
                                        <div class="social-icon-login twitter-color"><a href="#"><i class="fab fa-twitter"></i> Twitter</a>
                                        </div>
                                    </div> --}}
                                    <div class="col-sm-6 col-xs-12">
                                        <div class="social-icon-login google-color"><a href="{{ url('redirect/google') }}" style="color: #fff"><i class="fab fa-google-plus-g"></i> Google</a>
                                        </div>
                                    </div>
                                </div>
                                
                                
                                
                            </div><!-- close .registration-social-login-options -->
                            </
                        </div>
                    </div>
                </div>
                <div class="col-md-2"></div>
            </div>
        </div>
    </div>
    </div>
</div>
</div>
@endsection

@section('script')
<script>
    function checkLogin(){
        // alert('yes');
        var uname = $('#email_id').val();
        var pass = $('#password').val();

        
        if(uname == ''){
            // alert('uname empty')
          $('#username-result').text('*First Enter user email');
      }
      else{
          $('#username-result').empty();

          if(pass == ''){
            // alert('pass empty')
              $('#password-result').text('*Enter your password');
          }
          else{
              $('#password-result').empty();
              $('#login-submit2').click();
          }
      }
      

  }
</script>

@endsection
