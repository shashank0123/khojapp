@extends('registernew')
<style>
    #login-blade { box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);  margin-bottom: 50px}
    #login-card { padding: 30px; }
    #login-card .card-header { color: #000; font-size: 28px ; font-weight: bold; margin-bottom: 30px}
    .social-icon-login a { color: #fff }
    .social-icon-login i { left:30px!important; }

</style>
@section('content')
<div id="content-pro">                   
    <div id="pricing-plans-background-image">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-3"></div>
                <div class="col-md-6" id="login-blade">
                    <div class="card" id="login-card">
                        <div class="card-header">Forgot Password</div>

                        <div class="card-body">
                            <form method="POST" action="/forgot-password">
                                @csrf

                                <div class="form-group row">
                                    <label for="email" class="col-sm-4 col-form-label text-md-right">&nbsp;&nbsp;&nbsp;&nbsp;{{ __('Registered E-Mail') }}</label>

                                    <div class="col-md-6">
                                        <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>

                                        @if ($errors->has('email'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div id="email-result" style="text-align: center; color: #ff0000"></div>
                                <br><br>



                              {{--   <div class="form-group row">
                                    <div class="col-md-6 offset-md-4">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> {{ __('Remember Me') }}
                                            </label>
                                        </div>
                                    </div>
                                </div>
 --}}
                                <div class="form-group row mb-0">
                                    <div class="col-md-8 offset-md-4">
                                        &nbsp;&nbsp;&nbsp;&nbsp;<button type="button" class="btn btn-primary" onclick="checkEmail()">
                                            {{ __('Request Reset Password') }}
                                        </button>
<br><br>
                                        &nbsp;&nbsp;&nbsp;&nbsp;<a href="/login"><u>Back to Login</u></a>
                                    </div>
                                </div>
                            </form>
                            
                        </div>
                    </div>
                </div>
                <div class="col-md-2"></div>
            </div>
        </div>
    </div>
</div>
</div>


@endsection

@section('script')

<script>
    function checkEmail(){
         var CSRF_Token = $('meta[name="csrf-token"]').attr('content');
        var email = $('#email').val();
        // alert(email);
           
    $.ajax({              
          url: '/send-reset-password-link',
          type: 'POST',             
          data: { _token: CSRF_Token, email : email},
          success: function (data) { 
                $('#email-result').text(data.message);
          },
         
        });
    }
</script>

@endsection