@extends('layouts.admin')


@section('content')
<h4 class="header-title m-t-0 m-b-30">Default Example</h4>
<h4 class="header-title m-t-0 m-b-30">
Edit Provider</h4>
@if(Session::get('success'))
<div class="alert alert-success"> {{Session::get('success')}} </div>
@endif

@if(Session::get('fail'))
<div class="alert alert-danger"> {{Session::get('fail')}} </div>
@endif
@if (count($errors) > 0)
<div class="alert alert-danger">
	<ul>
		@foreach ($errors->all() as $error)
		<li>{{ $error }}</li>
		@endforeach
	</ul>
</div>
@endif

<form method="POST" action="" enctype="multipart/form-data">
	@csrf
	
	<!-- Provider -->
	<div class="row input-control">
		<label class="col-sm-3">Select Provider</label>

		<div class="col-sm-6">
			
			<select name='provider_id' id="provider_id" class="form-control">

				@if(!empty($providers))
				@foreach($providers as $provider)
				<option value="{{$provider->provider_id}}" @if($getProvider->provider_id == $provider->provider_id){{'selected'}}@endif>{{ucfirst($provider->title)}}</option>
				@endforeach
				@endif			
				
			</select>
		</div>
	</div>

	<!-- //Quality -->
	<div class="row input-control">
		<label class="col-sm-3">Select Video Quality</label>

		<div class="col-sm-6">
			
			<select name='video_quality' id="video_quality" class="form-control">

				<option value="hd" @if($getProvider->video_quality == 'hd'){{'selected'}}@endif>HD</option>
				<option value="sd" @if($getProvider->video_quality == 'sd'){{'selected'}}@endif>SD</option>
				<option value="4k" @if($getProvider->video_quality == '4k'){{'selected'}}@endif>4k</option>
				
			</select>
		</div>
	</div>

	<div class="row input-control">
		<label class="col-sm-3">Select Monetization Type</label>

		<div class="col-sm-6">
			
			<select name='monetization_type' id="monetization_type" class="form-control">

				<option value="flatrate" @if($getProvider->monetization_type == 'flatrate'){{'selected'}}@endif>Subscription</option>
				<option value="buy" @if($getProvider->monetization_type == 'buy'){{'selected'}}@endif>Buy</option>
				<option value="rent" @if($getProvider->monetization_type == 'rent'){{'selected'}}@endif>Rent</option>			
				<option value="ads" @if($getProvider->monetization_type == 'ads'){{'selected'}}@endif>Ads</option>			
				<option value="flatrate" >Flatrate</option>			
				<option value="free" @if($getProvider->monetization_type == 'free'){{'selected'}}@endif>Free</option>			
				
			</select>
		</div>
	</div>

	<div class="row input-control">
		<label class="col-sm-3">Retail Price</label>
		<div class="col-sm-6">			
			<input type="text" name="retail_price" id="retail_price" class="form-control" value="{{ $getProvider->retail_price ?? '' }}">
		</div>
	</div>

	<div class="row input-control">
		<label class="col-sm-3">Currency</label>
		<div class="col-sm-6">			
			<input type="text" name="currency" id="currency" class="form-control" value="{{ $getProvider->currency ?? '' }}">
		</div>
	</div>

	<div class="row input-control">
		<label class="col-sm-3">Video Link (Web URL)</label>
		<div class="col-sm-6">			
			<input type="text" name="web_url" id="web_url" class="form-control"  value="{{ $getProvider->web_url ?? '' }}">
		</div>
	</div>

	<div class="row input-control" style="display: none;">
		<label class="col-sm-3">IsLive</label>

		<div class="col-sm-6">
			
			<select name='isLive' id="isLive" class="form-control">

				<option value="1">1</option>
				
			</select>
		</div>
	</div>

	<div class="row form-group">
		<div class="col-sm-6 col-sm-offset-5">
			<button type="submit" name="save" class="btn btn-primary">Save</button> 
		</div>
	</div>

<br><br>
</form>


@endsection
