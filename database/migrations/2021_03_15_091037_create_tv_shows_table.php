<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTvShowsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tv_shows', function (Blueprint $table) {
            $table->increments('id');
            $table->string('show_name');
            $table->string('short_desc')->nullable();
            $table->text('description')->nullable();
            $table->string('show_type')->nullable();
            $table->string('poster')->nullable();
            $table->string('backdrop_path')->nullable();
            $table->string('tmdb_id')->nullable();
            $table->string('release_date')->nullable();
            $table->string('number_of_episodes')->nullable();
            $table->string('number_of_seasons')->nullable();
            $table->string('last_episode')->nullable();
            $table->string('current_status')->nullable();
            $table->string('status')->default('1');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tv_shows');
    }
}
