@extends('layouts.admin')


@section('content')

<h4 class="header-title m-t-0 m-b-30">OTTs</h4>
<div class="row">
    <div class="col-sm-6">
        <h4 class="header-title m-t-0 m-b-30">OTTs</h4>
    </div>
    <div class="col-sm-6">
        <form action="{{url('admin/search_ott')}}" method="GET">
            <div class="row">
                <div class="col-sm-8">
                    <input type="search" name="keyword" value="@if(!empty($keyword)){{'keyword'}}@endif" class="form-control"/>
                </div>
                <div class="col-sm-4">
                    <button type="submit" class="btn btn-primary">Search</button>
                </div>
            </div>
        </form>
    </div>
</div>
<h4 class="header-title m-t-0 m-b-30"></h4>
@if(Session::has('message'))
<div class="alert alert-success">{{ Session::get('message') }}</div>
@endif

<br><br>
<table id="datatable" class="table table-striped table-bordered">
    <thead>
        <tr>
            <th>id</th>
            <th>Name</th>
            <th>Icon</th>           
            <th>Priority</th>            
            <th>Action</th>
        </tr>
    </thead>

    <tbody>
        @php $i=1 @endphp
        @if(!empty($otts))
        @foreach ($otts as $ott)
        
        <tr>
            <td>{{ $i + ($page*15) }}</td>
            <td>{{ $ott->title ?? ''}}</td>
            <td><img src="{{ asset('asset/'.$ott->icon_url ?? '') }}" alt="{{$ott->technical_name ?? ''}}" style="height: 50px; width: auto;"></td>

         <td>
            <input type="number" class="form-control" id="priority{{$ott->provider_id}}" value="{{$ott->display_priority ?? '0'}}" style="width: 50%">
         </td>            
         <td>
             <button class="btn btn-primary" onclick="getPriority({{$ott->provider_id}})">Save</button>
         </td>

     </tr>
     @php $i++ @endphp
     @endforeach
     @endif

 </tbody>
</table> 

@if($i==1)
<div class="row" style="text-align: center; padding: 50px">No Record Found</div>
@endif
<div class="row">
    {{$otts->links()}}
</div>










@endsection

