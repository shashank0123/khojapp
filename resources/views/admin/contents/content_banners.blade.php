@extends('layouts.admin')


@section('content')
<h4 class="header-title m-t-0 m-b-30">Default Example</h4>
<h4 class="header-title m-t-0 m-b-30">All Banners</h4>
@if(Session::get('success'))
<div class="alert alert-success"> {{Session::get('success')}} </div>
@endif
@if (count($errors) > 0)
<div class="alert alert-danger">
	<ul>
		@foreach ($errors->all() as $error)
		<li>{{ $error }}</li>
		@endforeach
	</ul>
</div>
@endif

<form method="POST" action="{{url('admin/store_banner/'.$id)}}" enctype="multipart/form-data">
	@csrf
	
	
	<div class="row input-control">
		<label class="col-sm-3">Image</label>
		<div class="col-sm-6">			
			<input type="file" name="banner_url" id="banner_url" class="form-control">
		</div>
	</div>

	<div class="row input-control">
		<label class="col-sm-3">Is Live</label>
		<div class="col-sm-6">
			<select name="isLive" id="isLive" class="form-control">
				<option value="1">Yes</option>
				<option value="0">No</option>
			</select>

		</div>
	</div>


	<div class="row form-group">
		<div class="col-sm-6 col-sm-offset-5">
			<button type="submit" name="save" class="btn btn-primary">Save</button> 
		</div>
	</div>

		<h4> Banners of <i>'{{ucfirst($content->title)}}'</i>: </h4>
<div class="row">

	<br><br>

<table class="table">

			<thead>
				<th>ID</th>
				<th>Image</th>
				<th>Is Live</th>
				<th>Action</th>
			</thead>

			<tbody>

				@if(!empty($content_banners))
			@foreach($content_banners as $banner)
				<tr>
					<th>{{$banner->banner_id ?? ''}}</th>
					<th>
						@if(!empty($banner->banner_url))
						<img src="{{asset('banners/'.$banner->banner_url)}}" style="height: 100px; width: auto;">
						@else
						<img src="/images/default.png" style="height: 120px; width: auto;">
						@endif


					</th>
					<th>@if($banner->isLive=='1'){{'Yes'}}@else{{'No'}}@endif</th>
					<th>
						<a href="{{url('admin/delete_banner/'.$banner->banner_id)}}" class="btn btn-danger"> <i class="fa fa-trash" style="cursor: pointer;" ></i> </a>
					</th>

				</tr>

				@endforeach
				@endif

			</tbody>
		</table>





	
		
	</div>

</form>


@endsection
