<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('fonts/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{ asset('slick/slick.css') }}">
    <link rel="stylesheet" href="{{ asset('slick/slick-theme.css')}}">
    <script type="text/javascript" src="{{ asset('js/jquery-3.1.1.js') }}" ></script> 
    <!-- <script type="text/javascript" src="//code.jquery.com/jquery-1.11.0.min.js"></script> -->
    <script type="text/javascript" src="{{ asset('js/bootstrap.min.js')}}" ></script> 
    <script src="{{ asset('js/ripples.min.js')}}"></script>
    <!-- <script src="js/material.min.js"></script> -->
    <script src="{{ asset('slick/slick.min.js')}}"></script>
    <link href='https://fonts.googleapis.com/css?family=Ubuntu:400,700' rel='stylesheet' type='text/css'>



    {{-- <link href="{{ asset('css/app.css') }}" rel="stylesheet"> --}}
</head>
<body>
    <div id="app">
        <nav class="navbar">
        <div class="container mainnav" id="">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#example-navbar-collapse" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{ url('/') }}">
                    <img src="{{ url('/img/SKRN logo.png') }}" style="width: 170px; height:40px;" alt="{{ config('app.name', 'SKRN') }}"> 
                </a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="example-navbar-collapse">
                <ul class="nav navbar-nav m_nav" style="">
                    <!-- <li class="active"><a href="#">Home</a></li> -->
                    <li><a href="movies/newest">NEW RELEASE</a></li>
                    <li><a href="#">PRICING PLANS</a></li>
                    <li><a href="#">FAQ</a></li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    
                    <li><a href="#" class="btn btn-default btn-sm" style="margin: 7px;padding: 5px 15px 5px 15px;">Sign in</a></li>
                </ul>
            </div>
        </div>
    </nav>
        

        @yield('content')
    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <script>
    if (screen.width < 980) { 
    $('.plan').slick({
          slidesToShow: 3,
          slidesToScroll: 1,
          dots : false,
          autoplay: true,
          autoplaySpeed: 2000,
          responsive: [
             {
               breakpoint: 1024,
               settings: {
                 slidesToShow: 3,
                 slidesToScroll: 3,
                 infinite: true,
                 dots: true
               }
             },
             {
               breakpoint: 600,
               settings: {
                 slidesToShow: 2,
                 slidesToScroll: 2
               }
             },
             {
               breakpoint: 480,
               settings: {
                 slidesToShow: 1,
                 slidesToScroll: 1
               }
             }
             // You can unslick at a given breakpoint now by adding:
             // settings: "unslick"
             // instead of a settings object
           ]
        });
}

</script>
</body>
</html>
