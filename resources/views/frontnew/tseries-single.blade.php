<?php 
//$rate = floor($movie->vote_average);
$rate = $movie->vote_average;
if(!empty($movie->genre_type)){
	$genre_type = explode(',',$movie->genre_type);
	$genre_type = array_filter($genre_type);
}

?>
@extends('layouts.khoj_new')


<?php if (isset($_GET['page_id'])){
	$next = $_GET['page_id']+1;
	$prev = $_GET['page_id']-1;
}
else {
	$prev = '0';
	$next = '1';
}

$featureds = session()->get('featureds');
?>

@section('banner')

<div class="row ipad-width2">
	<div class="col-md-12 col-sm-12 col-xs-12">
		@if($errors->any())
		<div class="alert alert-danger" style="position: absolute; top: 2px">
			@foreach($errors->all() as $error)
			<li style="color: #fff">{{ $error }}</li>
			@endforeach
		</div>
		@endif

		@if($message = Session::get('message'))
		<div class="btn btn-success" style="width: 100%; position: absolute; top: 2px">
			<p style="color: #fff">{{ $message }}</p>
		</div>
		@endif
		@if(!empty($trailorslist[0]->key))
		<div class="movie-img ">
			<iframe width="100%" height="315" src="https://www.youtube.com/embed/@if(!empty($trailorslist[0])){{$trailorslist[0]->key}}@endif" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
		</div>
		@endif
	</div>
</div>

@endsection

@section('content')


<div class="page-single movie-single movie_single">
	<div class="container-fluid">
		

		<div class="row ipad-width2 single-main">
			<div class="col-md-12 col-sm-12 col-xs-12 ">				
				<div class="movie-img single-detail" >
					@if(isset($movie->poster_path))
					<img src="https://image.tmdb.org/t/p/w300{{$movie->poster_path}}" alt="" class="banner-image">
					@else
					<img src="/images/default.png" alt="" class="banner-image">
					@endif

					<div class="image-left">
						<div class="row">
							<div class="col-sm-3 col-xs-12">
								@if(isset($movie->poster_path))
								<img src="https://image.tmdb.org/t/p/w300{{$movie->poster_path}}" alt="" class="poster-image web-image">
								@else
								<img src="/images/default.png" alt="" class="web-image">
								@endif
							</div>
							<div class="col-sm-7 col-xs-12 image-mid">
								<div style="padding-left: 30px;">
								<h1 class="bd-hd">{{$movie->name}} </h1>
								<!-- <p><span>UA</span> HINDI, TELUGU, TAMIL</p> -->
								<ul>
									@if(!empty($genre_type))
									@foreach($genre_type as $genre)
									<li>{{$genre}}</li>
									@endforeach
									@endif
								</ul>
								<i class="ion-calculator" style="color:#fff; font-size: 14px; "></i> {{$movie->first_air_date }} &nbsp;&nbsp;&nbsp;
								<i class="ion-clock" style="color:#fff; font-size: 14px; "></i> 
								<?php
								$hours = floor($movie->runtime / 60);
								$min = $movie->runtime - ($hours * 60);
								echo $hours." hrs ".$min." mins";
								?>
								<div class="social-btn" id="hide-icon">
									@if ($favourite)
									<a href="#" class="parent-btn yellow" title="Already in your favourite list"><i class="ion-heart yellow-icon"></i> Add to Favorite</a>
									@else
									<a href="/favourite/{{'Tv'}}/{{$movie->movie_id}}" class="parent-btn"><i class="ion-heart"></i> Add to Favorite</a>
									@endif

									@if ($watchlist)						
									<a href="#" class="parent-btn yellow" title="Already in your watchlist"><i class="ion-eye yellow-icon"></i> Add to WatchList</a>
									@else
									<a href="/watchlist/{{'Tv'}}/{{$movie->movie_id}}" class="parent-btn"><i class="ion-eye"></i> Add to WatchList</a>
									@endif

									{{-- @if ($playlist)						
									<a href="#" class="parent-btn yellow" title="Already in your playlist"><i class="ion-play yellow-icon"></i> Add to PlayList</a>
									@else
									<a href="/playlist/{{'Tv'}}/{{$movie->movie_id}}" class="parent-btn"><i class="ion-play"></i> Add to PlayList</a>
									@endif --}}

							{{-- <div class="hover-bnt">
								<a href="#" class="parent-btn"><i class="ion-android-share-alt"></i>Share</a>
								<div class="hvr-item">
									<a href="#" class="hvr-grow"><i class="ion-social-facebook"></i></a>
									<a href="#" class="hvr-grow"><i class="ion-social-twitter"></i></a>
									<a href="#" class="hvr-grow"><i class="ion-social-googleplus"></i></a>
									<a href="#" class="hvr-grow"><i class="ion-social-youtube"></i></a>
								</div>
							</div>	 --}}	
						</div>
						</div>
					</div>
					<div class="col-sm-2 col-xs-12 image-right">
						<a href="https://www.facebook.com/sharer/sharer.php?u={{url('movie')}}/{{$movie->movie_id}}"><i class="ion-social-facebook" style=" background-color: #006699; color: #fff; padding: 5px 12px; border-radius: 50%; font-size: 14px"></i></a>&nbsp;
					<a href="http://www.twitter.com/intent/tweet?url={{url('movie')}}/{{$movie->movie_id}}"><i class="ion-social-twitter" style=" background-color: #00ACED; color: #fff; padding: 5px 8px; border-radius: 50%; font-size: 14px"></i></a>&nbsp;
					</div>
				</div>						
			</div>

			<div class="image-bottom">
				<div style="width: 200px; height: auto; background-color: #000000">
					&nbsp;&nbsp;&nbsp;<span style="font-weight: bold;">IMDB</span>&nbsp; N/A &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<span style="font-weight: bold;">RT</span>&nbsp; {{$rate}}
				</div>
			</div>

		</div>
	</div>
</div>


<div class="row ipad-width2 app-icons response-socio" id="response-space">
	<div class="col-md-1 col-sm-1 col-xs-1"></div>				
	<div class="col-md-10 col-sm-10 col-xs-10">					
		<div class="social-btn2">
			<ul>
				<li>
					@if ($favourite)
					<a href="#" class="parent-btn yellow" title="Already in your favourite list"><i class="ion-heart yellow-icon"></i></a>
					@else
					<a href="/favourite/{{'Tv'}}/{{$movie->movie_id}}" class="parent-btn" title="Add to Favourite"><i class="ion-heart"></i></a>
					@endif
				</li>
				<li>
					@if ($watchlist)						
					<a href="#" class="parent-btn yellow" title="Already in your watchlist"><i class="ion-eye yellow-icon"></i></a>
					@else
					<a href="/watchlist/{{'Tv'}}/{{$movie->movie_id}}" class="parent-btn" title="Add to Wishlist"><i class="ion-eye"></i></a>
					@endif
				</li>
				<li>
					@if ($playlist)						
					<a href="#" class="parent-btn yellow" title="Already in your playlist"><i class="ion-play yellow-icon"></i></a>
					@else
					<a href="/playlist/{{'Tv'}}/{{$movie->movie_id}}" class="parent-btn" title="Add to Playlist"><i class="ion-play"></i></a>
					@endif
				</li>
			</ul>

		</div>
	</div>
	<div class="col-md-1 col-sm-1 col-xs-1"></div>				
</div>

@if(!empty($content_provide))
<div class="row ipad-width2 app-icons">
	<div class="col-md-1 col-sm-1 col-xs-1"></div>				
	<div class="col-md-10 col-sm-10 col-xs-10">					
		You can watch this on
		<div class="app-get">
			<ul>
@foreach($content_provide as $con)

<?php
	if($con->provider_id == $prev){}
else{ $providers = DB::table('providers')->where('provider_id',$con->provider_id)->first(); 
}
?>			
@if($con->provider_id != $prev)
				<li>
				<a href="{{$con->web_url}}">					
					<img src="{{asset('/asset'.$providers->icon_url)}}" class="app-img">
					<p>{{ucfirst($con->monetization_type)}} <span style="color: yellow"> {{strtoupper($con->video_quality)}} </span></p>
					<p> {{$con->currency." ".$con->retail_price}}</p>
				</a>
				</li>
				@endif
				
				<?php $prev = $con->provider_id; ?>

@endforeach
			</ul>
		</div>
	</div>
	<div class="col-md-1 col-sm-1 col-xs-1"></div>				
</div>
@endif

<div class="row ipad-width2 app-icons response-socio">
	<div class="col-md-1 col-sm-1 col-xs-1"></div>				
	<div class="col-md-10 col-sm-10 col-xs-10" id="get-socio">					
		<div class="social-btn2">	

			<ul>
				<li><a href="https://www.facebook.com/sharer/sharer.php?u={{url('movie')}}/{{$movie->movie_id}}"><i class="ion-social-facebook" style=" background-color: #006699; color: #fff; padding: 5px 12px; border-radius: 50%; font-size: 14px"></i></a>&nbsp;</li>
				<li><a href="http://www.twitter.com/intent/tweet?url={{url('movie')}}/{{$movie->movie_id}}"><i class="ion-social-twitter" style=" background-color: #00ACED; color: #fff; padding: 5px 8px; border-radius: 50%; font-size: 14px"></i></a>&nbsp;</li>
			</ul>	

		</div>
	</div>
	<div class="col-md-1 col-sm-1 col-xs-1"></div>				
</div>

<div class="row ipad-width2 app-icons">
	<div class="col-md-1 col-sm-1 col-xs-1"></div>	
	<div class="col-md-10 col-sm-10 col-xs-10">				
		<div class="synopsys" onclick="getSynopsys()">
			<h4>Synopsys <i class="fa fa-angle-down"></i></h4>
			<div class="synopsys-list">
				{{$movie->overview}}
			</div>
		</div>

		<div class="cast" onclick="getCast()">
			<h4>Cast <i class="fa fa-angle-down"></i></h4>
			<div class="cast-list">
				@if(isset($casts))
				@foreach($casts as $cast)
				{{$cast->name}}&nbsp;&nbsp;&nbsp;  
				@endforeach
				@endif
			</div>
		</div>

		<div class="crew" onclick="getCrew()">
			<h4>Crew <i class="fa fa-angle-down"></i></h4>
			<div class="crew-list">
				@if(isset($crews))
				@foreach($crews as $crew)
				{{$crew->name}}&nbsp;&nbsp;&nbsp;  
				@endforeach
				@endif
			</div>
		</div>

		<div class="review" onclick="getReviews()">
			<h4>Reviews <i class="fa fa-angle-down"></i></h4>
			<div class="reviews-list">
				<?php $count = 0;?>
				@if(isset($reviewslist))
				@foreach($reviewslist as $review)
				<?php
				$count++;
				?>
				<div class="row">							
					<div class="col-sm-12 col-xs-12">
						<h3>{{$count}}. {{ ucfirst($review->author)}}</h3>
						<p class="review-end">{{ substr($review->content,0,500)}}</p>
					</div>							
				</div>

				@endforeach
				@endif
				@if($count == 0)
				<h5 >No Reviews Yet</h5>
				@endif
			</div>
		</div>
	</div>
	<div class="col-md-1 col-sm-1 col-xs-1"></div>	
</div>

<?php $videoc = 0; ?>
@if(isset($seasons))
<div class="row ipad-width2 trailors">
	<div class="col-md-1 col-sm-1 col-xs-1"></div>
	<div class="col-md-10 col-sm-10 col-xs-10">				
		<h4>{{count($seasons)}} Seasons</h4>
		<div class="row">


			<div class="slick-multiItem">
				<?php $count_m = 0; ?>
				@foreach($seasons as $season)
				<?php $videoc++; ?>



				<div class="slide-it">
					<div class="movie-item">
						<div class="mv-img">
							@if(isset($season->poster_path))
							<img src="https://image.tmdb.org/t/p/w300{{$season->poster_path}}" alt="" style="width: 185px; height: 270px">
							@else
							<img src="/images/default.png" alt="" style="width: 185px; height: 270px">
							@endif
						</div>

						<div class="title-in">
							<h6><a href="/tvseries/{{$season->id}}">{{$season->name}}</a></h6>
							<p>Episodes : {{$season->episode_count}}</p>
						</div>
					</div>
				</div>
				@endforeach
			</div>
		</div>
		@if($videoc == 0)
	<div style="text-align: left;margin: 20px; color: #ffffff">No Video Available</div>
@endif
	</div>
	<div class="col-md-1 col-sm-1 col-xs-1"></div>

</div>
<br><br>
@endif

<div class="row ipad-width2 trailors">
	<div class="col-md-1 col-sm-1 col-xs-1"></div>			
	<div class="col-md-10 col-sm-10 col-xs-10">				
		<h4>People also liked</h4>
		<div class="row">


			<div class="slick-multiItem">
				<?php $count_m = 0; ?>
				@if(isset($related_movies))
				@foreach($related_movies as $movie)
				<?php
				$first_air_date = explode('-',$movie->first_air_date);
				$count_m++;
				?>

				
				<div class="slide-it">
							<a  href="/tvseries/{{$movie->id}}">      					
					<div class="movie-item">
						<div class="mv-img">
							@if(isset($movie->poster_path))
							<img src="https://image.tmdb.org/t/p/w300{{$movie->poster_path}}" alt="" style="width: 185px; height: 270px">
							@else
							<img src="/images/default.png" alt="" style="width: 185px; height: 270px">
							@endif
						</div>
						<div class="hvr-inner">
						</div>
						<div class="title-in">
							<h6><a href="/tvseries/{{$movie->id}}">{{$movie->name}}</a></h6>
							<p><i class="ion-android-star"></i><span>{{$movie->vote_average}}</span> /10</p>
						</div>
					</div>
							</a>
				</div>
				
				@endforeach
				@endif


			</div>
			@if($count_m == 0)
	<div style="text-align: left;margin: 20px; color: #ffffff">No Video Available</div>
@endif
		</div>
	</div>
	<div class="col-md-1 col-sm-1 col-xs-1"></div>			
</div>

</div>
</div>

<script type="text/javascript">
	function getSynopsys(){
		$('.synopsys-list').toggle();
	}

	function getCast(){
		$('.cast-list').toggle();
	}

	function getCrew(){
		$('.crew-list').toggle();
	}

	function getReviews(){
		$('.reviews-list').toggle();
	}
</script>

@endsection 