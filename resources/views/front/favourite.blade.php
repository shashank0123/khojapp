<?php if (isset(auth()->user()->profile_pic))
$profile_pic = '/images/user/'.auth()->user()->profile_pic;
else {
	$profile_pic = '/final/images/demo/account-profile.jpg';
}

$username = explode(' ',Auth::user()->name);
$word = 0;
$count =0;

?>


@extends('layouts.new_front')

@section('content')
<style>
	#clickBro { display: none; }
	#file { display: none; }
</style>

<div class="hero user-hero">
	@if($errors->any())
	<div class="alert alert-danger" style="position: absolute; top: 2px">
		@foreach($errors->all() as $error)
		<li style="color: #fff">{{ $error }}</li>
		@endforeach
	</div>
	@endif

	@if($message = Session::get('message'))
	<div class="btn btn-success" style="width: 100%; position: absolute; top: 2px">
		<p style="color: #fff">{{ $message }}</p>
	</div>
	@endif
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="hero-ct">
					<h1>{{$key}}</h1>
					<ul class="breadcumb">
						<li class="active"><a href="/">Home</a></li>
						<li> <span class="ion-ios-arrow-right"></span>{{$keyword}}</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="page-single">
	<div class="container">
		<div class="row ipad-width">
			<div class="col-md-3 col-sm-12 col-xs-12">
				<div class="user-information">
					<div class="user-img">

						<form method="post" action="/upload-image" enctype="multipart/form-data" id="upload_image">
							@csrf
							<input type="hidden" name="member_id" id="uid" value="{{auth()->user()->id}}">
							<a href="#"><img src="{{$profile_pic}}" alt="" style="width: 80%; margin: auto"><br></a>
							
							<br>
							
							<label for="file"  class="redbtn">Change Pic</label>
							<input type="file" name="file" id="file" onchange='submitMe()'/>
							<input type="submit" name="submit" id="clickBro" />
						</form>
						
						
					</div>
					<div class="user-fav">
						<p>Account Details</p>
						<ul>
							<li><a href="/profile">Profile</a></li>
							<li  class="@if($keyword == 'Favourite'){{'active'}}@endif"><a href="/favourites/{{auth()->user()->id}}">Favourite movies</a></li>
							<li  class="@if($keyword == 'PlayList'){{'active'}}@endif"><a href="/playlists/{{auth()->user()->id}}">My Playlist</a></li>
							<li class="@if($keyword == 'WatchList'){{'active'}}@endif"><a href="/watchlists/{{auth()->user()->id}}">My WatchList</a></li>
						</ul>
					</div>
					<div class="user-fav">
						<p>Others</p>
						<ul>
							<li><a href="#">Change password</a></li>
							<li><a href="{{ route('logout') }}"
								onclick="event.preventDefault();
								document.getElementById('logout-form').submit();">Log out</a>
								<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
									{{ csrf_field() }}
								</form>
							</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="col-md-9 col-sm-12 col-xs-12">
				<div class="form-style-1 user-pro" action="#">
					


					<div class="row" id="user-profile">
						@if (isset($contents))
						@foreach ($contents as $element)

						<?php
						$count =1;
						if($keyword == 'Favourite'){
							$word = $element->favourite_id;
						}
						if($keyword == 'PlayList'){
							$word = $element->playlist_id;
						}
						if($keyword == 'WatchList'){
							$word = $element->watchlist_id;
						}
						?>

						<div class="col-sm-3 search-response" id="item{{$word}}">


							<div class="movie-item">
								<div class="mv-img">
									@if(isset($element->poster_path))
									<img src="https://image.tmdb.org/t/p/w300{{$element->poster_path}}" alt=""  style="width: 240px; height: 320px">
									@else
									<img src="/images/default.png" alt="">
									@endif
									<div style="position: absolute; top: 5px; right: 25px; padding: 5px 10px; border: 2px solid #fff; border-radius: 50%; background-color: #fff; cursor: pointer;" onclick="deleteItem({{$word}},'{{$keyword}}')">
										<i class="fa fa-times" style="color: #000"></i>
									</div>
								</div>
								<div class="hvr-inner">
									<a  href="/tseries-detail/{{$element->id}}">
										{{-- <i class="fa fa-play" aria-hidden="true"></i> </a> --}}
									</div>
									<div class="title-in">
										<br>
										<h6><a href="/tseries-detail/{{$element->id}}">{{$element->name}}</a></h6>
										<p><i class="ion-android-star"></i><span>{{$element->vote_average}}</span> /10</p>
									</div>
								</div>
							</div>
							@endforeach
							@endif							


							@if($count == 0 )
							<div style="text-align: center;margin-top: 20%;
							margin-bottom: 20%; color: #fff; font-size: 32px">No Result Found</div>
							@endif
						</div>









					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- footer section-->

	<script>
		function submitMe() {
			$('#file').click();
			$("#clickBro").click();
		}
		
		function deleteItem(id , key){
			var CSRF_Token = $('meta[name="csrf-token"]').attr('content');

			$.ajax({
				type: "POST",
				url: "/delete-item",
				data:{ _token: CSRF_Token, id: id , key: key},
				success:function(data){
					$('#item'+id).hide();   
				}
			});
		}
	</script>

	@endsection