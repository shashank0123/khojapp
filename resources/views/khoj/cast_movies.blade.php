@extends('layouts.khoj_new')

@section('content')
<div class="col-sm-1"></div>
<div class=" col-sm-12 col-lg-10 col-xs-12 right-filter">

  <div class="movie-content" id="dynamic-search" style="height: 100%">
<br>
    <div class="row">
    <div class="col-sm-6">
     @if(isset($cast))
    <h4>Result for "<i>{{$cast->name}}</i>"&nbsp;&nbsp;
      <a href="{{url('/')}}"><u>RESET</u></a>
    </h4>
    @endif
  </div>
  <div class="col-sm-6" style="text-align: right;">
    <a href="{{url()->previous()}}" class="btn btn-primary">Back To previous page</a>
  </div>
  </div>

<br>
   

    @if (isset($contents))
    <?php $space = 0; $rat=0;?>
    @foreach ($contents as $element)
    <?php 
    $parent_title = "";

    $flag = 1; 
    $space++;
    if($element->parent_id>0){
      $parent_title = DB::table('contents')->where('content_id',$element->parent_id)->first();
    }
    ?>
    <div class="col-sm-3 col-lg-2 col-md-3 col-xs-6 search-response dynamic" style="padding-left: 1%; padding-right: 1%">

       <?php $urlen = preg_replace('/[^a-zA-Z0-9_.]/', '-', (strtolower($element->title))).'-'.$element->content_id; ?>  

       <a  href="/detail/{{$element->content_type}}/{{$urlen}}">



        <div class="movie-item">
         <div class="mv-img">
          @if(!empty($element->poster) && strpos($element->poster, 'poster/') == false)
          <img src="/poster/{{$element->poster}}" alt="{{$element->title ?? ''}}" class="poster-image web-image">
          @else
          <img src="/images/default.png" alt="{{$element->title ?? ''}}" class="web-image">
          @endif
        </div>
        <div class="hvr-inner">
         {{-- <i class="fa fa-play" aria-hidden="true"></i>  --}}
       </div>
       <div class="title-in">

        <p>
         IMDB: <span style="color: yellow; font-size: 13px;">@if(isset($element->imdb_score)){{$element->imdb_score}}@else{{'N/A'}}@endif</span>
       </p>
     </div>
     <div class="imdb">
      <div class="row" style="text-align: left;">
        <?php $urlen = preg_replace('/[^a-zA-Z0-9_.]/', '-', (strtolower($element->title))).'-'.$element->content_id; ?>
        <span><a href="/detail/{{$element->content_type}}/{{$urlen}}" style="font-size: 14px"> 
          <?php
          if(isset($element->title))
            echo substr(explode(':',$element->title)[0],0,18); 
          elseif(isset($element->original_title))
            echo substr($element->original_title,0,25); 
          if(isset($parent_title) && isset($parent_title->title))
            echo " (".substr($parent_title->title,0,25).") ";
          elseif(isset($parent_title) && isset($parent_title->original_title))
            echo " (".substr($parent_title->original_title,0,25).") ";
          ?></a></span>

        </div>
        <div class="row rate-row" style="font-size: smaller;">
          <?php if ($element->original_language == 'hi') $element->original_language = "Hindi";?>
          <?php if ($element->original_language == 'en') $element->original_language = "English";?>
          <?php if ($element->original_language == 'te') $element->original_language = "Telugu";?>
          <?php if ($element->original_language == 'bn') $element->original_language = "Bengali";?>
          <?php if ($element->original_language == 'kn') $element->original_language = "Kannada";?>
          <?php if ($element->original_language == 'es') $element->original_language = "Spanish";?>
          <?php if ($element->original_language == 'ru') $element->original_language = "Russian";?>
          <?php if ($element->original_language == 'gu') $element->original_language = "Gujrati";?>
          <?php if ($element->original_language == 'ml') $element->original_language = "Malyalam";?>
          <?php if ($element->original_language == 'pa') $element->original_language = "Punjabi";?>

          <div class="col-sm-4 col-xs-4" >{{ucfirst($element->content_type)}}</div>
          <div class="col-sm-4 col-xs-4" style="text-align: center">{{$element->original_language}} </div>
          <div class="col-sm-3 col-xs-3 pull-right" style="text-align: right;">@if(isset($element->age_certification)){{$element->age_certification}}@else{{'U/A'}}@endif</div>
        </div>
      </div>
    </div>
  </a>
</div>
<?php $rat++; ?>
@endforeach

@endif

@if($flag == 0)
<h4 class="result-sec">No result found</h4>
@endif

</div>
</div>

@endsection 

@section('paginate')

<div class="container" style="text-align: center;">
  <div class="row">
    {{$contents->links()}}
  </div>
</div>


@endsection

