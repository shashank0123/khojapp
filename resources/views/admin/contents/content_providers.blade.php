@extends('layouts.admin')


@section('content')
<h4 class="header-title m-t-0 m-b-30">Default Example</h4>
<h4 class="header-title m-t-0 m-b-30">All Providers</h4>
@if(Session::get('success'))
<div class="alert alert-success"> {{Session::get('success')}} </div>
@endif

@if(Session::get('fail'))
<div class="alert alert-danger"> {{Session::get('fail')}} </div>
@endif
@if (count($errors) > 0)
<div class="alert alert-danger">
	<ul>
		@foreach ($errors->all() as $error)
		<li>{{ $error }}</li>
		@endforeach
	</ul>
</div>
@endif

<form method="POST" action="{{url('admin/store_provider/'.$id)}}" enctype="multipart/form-data">
	@csrf
	
	<!-- Provider -->
	<div class="row input-control">
		<label class="col-sm-3">Select Provider</label>

		<div class="col-sm-6">
			
			<select name='provider_id' id="provider_id" class="form-control">

				@if(!empty($providers))
				@foreach($providers as $provider)
				<option value="{{$provider->provider_id}}">{{ucfirst($provider->title)}}</option>
				@endforeach
				@endif			
				
			</select>
		</div>
	</div>

	<!-- //Quality -->
	<div class="row input-control">
		<label class="col-sm-3">Select Video Quality</label>

		<div class="col-sm-6">
			
			<select name='video_quality' id="video_quality" class="form-control">

				<option value="hd">HD</option>
				<option value="sd">SD</option>
				<option value="4k">4k</option>
				
			</select>
		</div>
	</div>

	<div class="row input-control">
		<label class="col-sm-3">Select Monetization Type</label>

		<div class="col-sm-6">
			
			<select name='monetization_type' id="monetization_type" class="form-control">

				<option value="flatrate">Subscription</option>
				<option value="buy">Buy</option>
				<option value="rent">Rent</option>			
				<option value="ads">Ads</option>			
				<option value="flatrate">Flatrate</option>			
				<option value="free">Free</option>			
				
			</select>
		</div>
	</div>

	<div class="row input-control">
		<label class="col-sm-3">Retail Price</label>
		<div class="col-sm-6">			
			<input type="text" name="retail_price" id="retail_price" class="form-control">
		</div>
	</div>

	<div class="row input-control">
		<label class="col-sm-3">Currency</label>
		<div class="col-sm-6">			
			<input type="text" name="currency" id="currency" class="form-control">
		</div>
	</div>

	<div class="row input-control">
		<label class="col-sm-3">Video Link (Web URL)</label>
		<div class="col-sm-6">			
			<input type="text" name="web_url" id="web_url" class="form-control">
		</div>
	</div>

	<div class="row input-control" style="display: none;">
		<label class="col-sm-3">IsLive</label>

		<div class="col-sm-6">
			
			<select name='isLive' id="isLive" class="form-control">

				<option value="1">1</option>
				
			</select>
		</div>
	</div>

	<div class="row form-group">
		<div class="col-sm-6 col-sm-offset-5">
			<button type="submit" name="save" class="btn btn-primary">Save</button> 
		</div>
	</div>

	<h4> Providers of <i>'{{ucfirst($content->title)}}'</i>: </h4>
	<div class="row">
		<br><br>
		<table class="table">

			<thead>
				<th>ID</th>
				<th>Provider</th>
				<th>Quality</th>
				<th>Monetization Type</th>
				<th>Retail Price</th>
				<th>Currency</th>
				<!-- <th>Is Live</th> -->
				<th>Action</th>
			</thead>

			<tbody>

				@if(!empty($content_providers))
				@foreach($content_providers as $provider)

				<tr>
					<th>{{$provider->provider_id ?? ''}}</th>
					<th>{{$provider->title ?? ''}}</th>
					<th>{{$provider->video_quality ?? ''}}</th>
					<th>{{$provider->monetization_type ?? ''}}</th>
					<th>{{$provider->retail_price ?? ''}}</th>
					<th>{{$provider->currency ?? ''}}</th>				
					<!-- <th>{{$provider->isLive ?? ''}}</th>					 -->
					<th>
						<!-- @if($provider->isLive =='1')
						<a href="{{url('admin/status/'.$provider->content_id.'/'.$provider->provider_id.'/0')}}" class="btn btn-danger">No</a>
						@else
						<a href="{{url('admin/status/'.$provider->content_id.'/'.$provider->provider_id.'/1')}}" class="btn btn-sucess">Yes</a>
						@endif -->
						<a href="{{url('admin/edit_provider/'.$provider->content_id.'/'.$provider->provider_id.'/'.$provider->video_quality)}}" class="btn btn-primary"> <i class="fa fa-edit" style="cursor: pointer;" ></i> </a>
						<a href="{{url('admin/delete_provider/'.$provider->content_id.'/'.$provider->provider_id.'/'.$provider->video_quality)}}" class="btn btn-danger"> <i class="fa fa-trash" style="cursor: pointer;" ></i> </a>
					</th>

				</tr>

				@endforeach
				@endif

			</tbody>
		</table>
	</div>

<br><br>
</form>


@endsection
