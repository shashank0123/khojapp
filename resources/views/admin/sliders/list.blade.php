@extends('layouts.admin')


@section('content')
<h4 class="header-title m-t-0 m-b-30">Default Example</h4>
<h4 class="header-title m-t-0 m-b-30">All Sliders</h4>
@if(Session::get('success'))
<div class="alert alert-success"> {{Session::get('success')}} </div>
@endif
@if (count($errors) > 0)
<div class="alert alert-danger">
	<ul>
		@foreach ($errors->all() as $error)
		<li>{{ $error }}</li>
		@endforeach
	</ul>
</div>
@endif


<h4> Slider Images </h4>
<div class="row">

	<br><br>

	<table class="table">

		<thead>
			<th>#</th>
			<th>Page Title</th>
			<th>Title</th>
			<th>Image</th>
			<th>Priority</th>
			<th>Start Date</th>
			<th>Expiry</th>
			<th>Status</th>
			<th>Action</th>
		</thead>

		<tbody>

			@if(!empty($sliders))
			@php $i=1 @endphp
			@foreach($sliders as $slider)
			<tr>
				<th>{{$i++}}</th>
				<th>{{ucfirst($slider->page_title ?? '')}}</th>
				<th>{{$slider->image_title ?? ''}}</th>
				<th>
					@if(!empty($slider->poster))
					<img src="{{asset($slider->poster)}}" style="height: 75px; width: auto;">
					@else
					<img src="/images/default.png" style="height: 75px; width: auto;">
					@endif
				</th>
				<th>{{$slider->display_priority ?? ''}}</th>
				<th>{{date('d-m-Y',strtotime($slider->start_date ?? ''))}}</th>
				<th>
					@if(date('Y-m-d') < $slider->end_date)
					<span style="color: green; font-weight: bold;">{{'NO'}}</span>
					@else
					<span style="color: red; font-weight: bold;">{{'Expired'}}</span>
					@endif
				</th>
				<th>{{$slider->status ?? ''}}</th>
				<th>
					<a href="{{url('admin/edit_slider/'.$slider->id)}}" class="btn btn-danger"> <i class="fa fa-edit" style="cursor: pointer;" ></i> </a>
					<a href="{{url('admin/delete_slider/'.$slider->id)}}" class="btn btn-danger"> <i class="fa fa-trash" style="cursor: pointer;" ></i> </a>
				</th>
				

			</tr>

			@endforeach
			@endif

		</tbody>
	</table>





	

</div>



@endsection
