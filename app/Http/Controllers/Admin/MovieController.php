<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Movie;
use App\Genre;
use DB;
// use App\Content;


class MovieController extends Controller
{
    public function getMovies()
    {
        $type = 'Movies List';
        $page = 0;
        if (isset($_GET['page'])) {
            $page = $_GET['page'] - 1;
        }
        $movies = DB::table('contents')->where('content_type', 'movie')->orderBy('content_id', 'DESC')->paginate(50);
        return view('admin.movielist', compact('movies', 'type', 'page'));
    }

    public function getViralVideos()
    {
        $type = 'Viral Videos';
        $page = 0;
        if (isset($_GET['page'])) {
            $page = $_GET['page'] - 1;
        }
        $movies = DB::table('contents')->where('content_type', 'viral')->orderBy('content_id', 'DESC')->paginate(50);
        return view('admin.movielist', compact('movies', 'type', 'page'));
    }

    public function addShowType($type, $id)
    {

        if ($type == 'tv-season' || $type == 'season') {
            $show = DB::table('contents')->where('content_id', $id)->first();
            return view('admin.contents.add_season', compact('show', 'type'));
        }
        if ($type == 'tv-episode' || $type == 'episode') {
            $show = DB::table('contents')->where('content_id', $id)->first();
            return view('admin.contents.add_episode', compact('show', 'type'));
        }
    }

    public function getShows()
    {
        $content_type = 'tv-show';
        $page = 0;
        if (isset($_GET['page'])) {
            $page = $_GET['page'] - 1;
        }
        $type = 'TV Shows';
        $movies = DB::table('contents')->where('content_type', 'tv-show')->orWhere('content_type', 'show')->orWhere('content_type', 'shows')->where('parent_id', 0)->orderBy('content_id', 'DESC')->paginate(50);
        return view('admin.movielist', compact('movies', 'type', 'content_type', 'page'));
    }

    public function getSeasons($show_id)
    {
        $content_type = 'tv-season';
        $page = 0;
        if (isset($_GET['page'])) {
            $page = $_GET['page'] - 1;
        }
        $show = DB::table('contents')->where('content_id', $show_id)->first();
        $type = "Seasons of Show '" . $show->title . "'";
        $movies = DB::table('contents')->where('parent_id', $show_id)->paginate(50);
        return view('admin.movielist', compact('movies', 'type', 'content_type', 'show_id', 'page'));
    }

    public function getEpisodes($season_id)
    {
        $page = 0;
        if (isset($_GET['page'])) {
            $page = $_GET['page'] - 1;
        }
        $season = DB::table('contents')->where('content_id', $season_id)->first();
        $type = "Episodes of Show '" . $season->title . "'";
        $movies = DB::table('contents')->where('parent_id', $season_id)->paginate(50);
        return view('admin.movielist', compact('movies', 'type', 'season_id', 'page'));
    }

    public function changefeature($feature, $movie_id)
    {
        $movies = Movie::find($movie_id);
        // dd($movies);
        if ($feature == 'yes') {

            $movies->featured          = "no";
        } else {
            $movies->featured          = "yes";
        }

        $movies->update();
        return \Redirect::back()->with('message', "Updated successfully");
    }

    public function addContent($type)
    {
        return view('admin.contents.add_content', compact('type'));
    }

    // For Movie & Show
    public function storeContent(Request $request)
    {
        $data = $request->all();
        $poster = " ";
        $message = 'Something went wrong';

        $min = DB::table('contents')->where('content_source', 'admin')->orderBy('created_at', 'DESC')->first();
        $min_id = 0;
        if ($min) {
            $min_id =  rand(20, 999999);
        } else {
            $min_id = 1;
        }

        if ($request->hasFile('poster')) {
            $file = $request->poster;
            $file->move(public_path() . '/poster/', time() . $file->getClientOriginalName());
            // $file->move(public_path(). '/thumbnail/', time().$file->getClientOriginalName());
            $poster = time() . $file->getClientOriginalName();
        }

        $release_year = explode('-', $data['cinema_release_date'])[0];

        $newData = DB::table('contents')->insert(array(
            'title' => $data['title'] ?? '',

            'original_title' => $data['title'] ?? '',
            'content_type' => $data['content_type'],
            'poster' => $poster,
            'runtime' => $data['runtime'] ?? '',
            'content_source' => 'admin',
            'short_description' => $data['short_description'] ?? '',
            'content_source_id' => $min_id,
            'tmdb_popularity' => $data['popularity'] ?? '',
            'age_certification' => $data['age_certification'] ?? '',
            'original_release_year' => $release_year,
            'cinema_release_date' => $data['cinema_release_date'] ?? '',
            'tmdb_score' => $data['tmdb_score'] ?? '',
            'imdb_score' => $data['imdb_score'] ?? '',
            'tags' => $data['tags'] ?? '',
            'meta_title' => $data['meta_title'] ?? '',
            'meta_description' => $data['meta_description'] ?? '',
            'parent_id' => '0'
        ));
        $message = 'Content successfully added.';
        return redirect()->back()->with('success', $message);
    }

    //For Season & Episode
    public function storeShow(Request $request, $type, $parent_id)
    {
        $data = $request->all();
        $poster = "";
        $message = 'Something went wrong';

        $min = DB::table('contents')->where('content_source', 'admin')->orderBy('content_source_id', 'DESC')->first();
        $min_id = 0;
        if ($min) {
            $min_id = rand(20, 999999);
            // $min_id = $min->content_source_id + 1;
        } else {
            $min_id = 1;
        }

        if ($request->hasFile('poster')) {
            $file = $request->poster;
            $file->move(public_path() . '/poster/', time() . $file->getClientOriginalName());
            // $file->move(public_path(). '/thumbnail/', time().$file->getClientOriginalName());
            $poster = time() . $file->getClientOriginalName();
        }


        $release_year = explode('-', $data['cinema_release_date'])[0];

        $newData = DB::table('contents')->insert(array(
            'title' => $data['title'] ?? '',
            'parent_id' => $parent_id,
            'original_title' => $data['title'] ?? '',
            'content_type' => $type,
            'poster' => $poster,
            'runtime' => $data['runtime'] ?? '',
            'content_source' => 'admin',
            'short_description' => $data['short_description'] ?? '',
            'content_source_id' => $min_id,
            'tmdb_popularity' => $data['popularity'],
            'age_certification' => $data['age_certification'] ?? '',
            'original_release_year' => $release_year,
            'cinema_release_date' => $data['cinema_release_date'] ?? '',
            'tmdb_score' => $data['tmdb_score'] ?? '',
            'imdb_score' => $data['imdb_score'] ?? '',
            'tags' => $data['tags'] ?? '',
            'meta_title' => $data['meta_title'] ?? '',
            'meta_description' => $data['meta_description'] ?? '',

        ));

        $message = 'Content successfully added.';
        return redirect()->back()->with('success', $message);
    }


    //For Adding Content's  Genre

    public function addContentGenre($id, Request $request)
    {
        $content_genres = DB::table('content_genres')->leftJoin('genres', 'genres.genre_id', 'content_genres.genre_id')->where('content_genres.content_id', $id)->select('genres.*', 'content_genres.content_id')->get();

        $content = DB::table('contents')->where('content_id', $id)->first();
        $genres = Genre::select('genre_id', 'title')->distinct('genre_id')->get();
        return view('admin.contents.content_genres', compact('content_genres', 'id', 'genres', 'content'));
    }

    public function storeContentGenre($id, Request $request)
    {
        $data = $request->all();
        $message = 'Something went wrong';

        $check = DB::table('content_genres')->where('content_id', $id)->where('genre_id', $data['genre_id'])->first();
        if ($check) {
            $message = 'Already added';
        } else {
            $newData = DB::table('content_genres')->insert(array(
                'content_id' => $id,
                'genre_id' => $data['genre_id'],
            ));
            $message = 'Data successfully added';
        }
        return redirect()->back()->with('success', $message);
    }

    public function deleteContentGenre($content_id, $genre_id)
    {
        $check = DB::table('content_genres')->where('content_id', $content_id)->where('genre_id', $genre_id)->delete();
        return redirect()->back()->with('success', 'Data successfully deleted');
    }



    //For Adding content's casts
    public function addContentCast($id, Request $request)
    {
        $content_casts = DB::table('content_casting')->where('content_id', $id)->get();
        $content = DB::table('contents')->where('content_id', $id)->first();
        
        return view('admin.contents.content_casting', compact('content_casts', 'id', 'content'));
    }
    public function storeContentCast($id, Request $request)
    {
        $data = $request->all();
        $message = 'Something went wrong';



        $poster = " ";
        if ($request->hasFile('poster')) {
            $file = $request->poster;
            $file->move(public_path() . '/poster/', time() . $file->getClientOriginalName());
            $poster = time() . $file->getClientOriginalName();
        }


        $person = DB::table('content_casting')->where('person_id', $data['person_id'])->first();
        $check = DB::table('content_casting')->where('content_id', $id)->where('person_id', $data['person_id'])->first();
        if ($check) {
            $message = 'Already added';
        } else {
            $newData = DB::table('content_casting')->insert(array(
                'content_id' => $id,
                'person_id' => $person->person_id ?? '',
                'name' => $person->name ?? '',
                'poster' => $poster,
                'character_name' => $person->character_name ?? '',
                'role' => $person->role ?? '',
            ));
            $message = 'Data successfully added';
        }
        return redirect()->back()->with('success', $message);
    }

    public function deleteContentCast($content_id, $person_id)
    {
        $check = DB::table('content_casting')->where('content_id', $content_id)->where('person_id', $person_id)->delete();
        return redirect()->back()->with('success', 'Data successfully deleted');
    }




    //For Adding content's banners
    public function addContentBanner($id, Request $request)
    {
        $content_banners = DB::table('content_banners')->where('content_id', $id)->get();

        $content = DB::table('contents')->where('content_id', $id)->first();

        return view('admin.contents.content_banners', compact('content_banners', 'id', 'content'));
    }
    public function storeContentBanner($id, Request $request)
    {
        $data = $request->all();
        $message = 'Something went wrong';

        $poster = " ";
        if ($request->hasFile('banner_url')) {
            $file = $request->banner_url;
            $file->move(public_path() . '/banners/', time() . $file->getClientOriginalName());
            $poster = time() . $file->getClientOriginalName();
        }
        $priority = 1;
        $banner = DB::table('content_banners')->where('content_id', $id)->orderBy('display_priority')->first();
        if (!empty($banner)) {
            $priority = $banner->display_priority + 1;
        }

        $source_url = 'admin-' . $poster;
        $newData = DB::table('content_banners')->insert(array(
            'content_id' => $id,
            'banner_url' => $poster,
            'display_priority' => $priority,
            'isLive' => $data['isLive'],
            'content_source_url' => $source_url,
        ));
        $message = 'Data successfully added';

        return redirect()->back()->with('success', $message);
    }

    public function deleteContentBanner($banner_id)
    {
        $check = DB::table('content_banners')->where('banner_id', $banner_id)->delete();
        return redirect()->back()->with('success', 'Data successfully deleted');
    }



    // For adding content's clips

    public function addContentClip($id, Request $request)
    {
        $content_clips = DB::table('content_clips')->where('content_id', $id)->get();

        $content = DB::table('contents')->where('content_id', $id)->first();

        return view('admin.contents.content_clips', compact('id', 'content', 'content_clips'));
    }
    public function storeContentClip($id, Request $request)
    {
        $data = $request->all();
        $message = 'Something went wrong';
        $url1 = "";

        $url = explode('https://www.youtube.com/watch?v=', $data['url']);
        $url2 = explode('&', $url[1]);
        $url1 = $url2[0];

        $data['external_id'] = $url1;
        $data['provider'] = 'youtube';

        $check = DB::table('content_clips')->where('content_id', $id)->where('external_id', $data['external_id'])->first();
        if ($check) {
            $message = 'Already added';
        } else {
            $newData = DB::table('content_clips')->insert(array(
                'content_id' => $id,
                'title' => $data['title'] ?? '',
                'display_priority' => '1',
                'external_id' => $data['external_id'],
                'type' => $data['type'],
                'provider' => $data['provider'],

            ));
            $message = 'Data successfully added';
        }
        return redirect()->back()->with('success', $message);
    }

    public function deleteContentClip($content_id, $clip_id)
    {
        $check = DB::table('content_clips')->where('content_id', $content_id)->where('clip_id', $clip_id)->delete();
        return redirect()->back()->with('success', 'Data successfully deleted');
    }


    // For adding content's providers

    public function addContentProvider($id, Request $request)
    {
        $content_providers = DB::table('content_providers')->leftJoin('providers', 'providers.provider_id', 'content_providers.provider_id')->where('content_providers.content_id', $id)->select('providers.title', 'content_providers.*')->get();

        $content = DB::table('contents')->where('content_id', $id)->first();

        $providers = DB::table('providers')->select('provider_id', 'title')->orderBY('title', 'ASC')->get();
        // var_dump($content_providers);die;

        return view('admin.contents.content_providers', compact('id', 'content', 'content_providers', 'providers'));
    }

    public function storeContentProvider($id, Request $request)
    {
        $data = $request->all();
        $message = 'Something went wrong';
        $field = 'success';


        $findRow = DB::table('content_providers')
            ->where('content_id', $id)
            ->where('provider_id', $data['provider_id'])
            ->where('video_quality', $data['video_quality'])
            ->first();

        if (!empty($findRow)) {
            $message = "You can't add this ott again as it already stored.";
            $field = 'fail';
        } else {

            $newData = DB::table('content_providers')->insert(array(
                'content_id' => $id,
                'provider_id' => $data['provider_id'] ?? '',
                'video_quality' => $data['video_quality'] ?? '',
                'retail_price' => $data['retail_price'] ?? '',
                'monetization_type' => $data['monetization_type'] ?? '',
                'currency' => $data['currency'] ?? '',
                'web_url' => $data['web_url'] ?? '',
                'added_by' => 'admin',

                'isLive' => 1,
            ));
            $message = 'Data successfully added';
        }
        return redirect()->back()->with($field, $message);
    }

    public function editContentProvider($content_id, $provider_id, $video)
    {

        $getProvider = DB::table('content_providers')->where('content_id', $content_id)->where('provider_id', $provider_id)->where('video_quality', $video)->first();

        $providers = DB::table('providers')->select('provider_id', 'title')->orderBY('title', 'ASC')->get();

        return view('admin.contents.edit_provider', compact('getProvider', 'providers'));
    }

    public function updateContentProvider($content_id, $provider_id, $video, Request $request)
    {
        $data = $request->all();
        $message = 'Something went wrong';
        $field = 'success';


        $findRow = DB::table('content_providers')
            ->where('content_id', $content_id)
            ->where('provider_id', $provider_id)
            ->where('video_quality', $video)
            ->update(array(

                'provider_id' => $data['provider_id'] ?? '',
                'video_quality' => $data['video_quality'] ?? '',
                'retail_price' => $data['retail_price'] ?? '',
                'monetization_type' => $data['monetization_type'] ?? '',
                'currency' => $data['currency'] ?? '',
                'web_url' => $data['web_url'] ?? '',
                'isLive' => 1

            ));




        $message = 'Data successfully updated';

        return redirect()->back()->with($field, $message);
    }


    public function deleteContentProvider($content_id, $provider_id, $video)
    {

        $check = DB::table('content_providers')->where('content_id', $content_id)->where('provider_id', $provider_id)->where('video_quality', $video)->delete();
        return redirect()->back()->with('success', 'Data successfully deleted');
    }

    public function updateStatusContentProvider($content_id, $provider_id, $status)
    {

        $check = DB::table('content_providers')->where('content_id', $content_id)->where('provider_id', $provider_id)->update(array(
            'isLive' => $status,
        ));

        return redirect()->back()->with('success', 'Status successfully updated');
    }



    public function editContent($id)
    {
        $movie = DB::table('contents')->where('content_id', $id)->first();

        return view('admin.contents.edit_content', compact('movie'));
    }


    //For Season & Episode
    public function updateContent(Request $request, $id)
    {
        $data = $request->all();
        $message = 'Something went wrong';
        $movie = DB::table('contents')->where('content_id', $id)->first();
        if ($movie) {
            $poster = $movie->poster;
            if ($request->hasFile('poster')) {
                $file = $request->poster;
                $file->move(public_path() . '/poster/', time() . $file->getClientOriginalName());
                // $file->move(public_path(). '/thumbnail/', time().$file->getClientOriginalName());
                $poster = time() . $file->getClientOriginalName();
            } 
            

                    $release_year = explode('-', $data['cinema_release_date'])[0];

            $newData = DB::table('contents')->where('content_id', $id)->update(array(
                'title' => $data['title'] ?? '',
                'original_title' => $data['title'] ?? '',
                'poster' => $poster,
                'runtime' => $data['runtime'] ?? '',
                'short_description' => $data['short_description'] ?? '',
                'tmdb_popularity' => $data['popularity'] ?? '',
                'age_certification' => $data['age_certification'] ?? '',
                'original_release_year' => $release_year,
                'cinema_release_date' => $data['cinema_release_date'] ?? '',
                'tmdb_score' => $data['tmdb_score'] ?? '',
                'imdb_score' => $data['imdb_score'] ?? '',
                'tags' => $data['tags'] ?? '',
                'meta_title' => $data['meta_title'] ?? '',
                'meta_description' => $data['meta_description'] ?? '',

            ));

            $message = 'Content successfully updated.';
            return redirect()->back()->with('success', $message);
        } else {
            $message = 'Content not found.';
            return redirect()->back()->with('failure', $message);
        }
    }



    public function getFreeMovies()
    {
        $type = "Free Movies";
        $page = 0;
        if (isset($_GET['page'])) {
            $page = $_GET['page'] - 1;
        }
        $movies = DB::table('contents')
            ->leftJoin('content_providers', 'contents.content_id', 'content_providers.content_id')
            ->where('contents.view_type', 'free')
            ->orWhere('content_providers.retail_price', '0.00')
            ->where('content_providers.monetization_type', "!=", 'flatrate')
            // ->whereNotNull('contents.content_id')
            ->orderBy('contents.content_id', 'DESC')
            ->select('contents.*')
            ->distinct('contents.content_id')
            ->paginate(50);

        return view('admin.movielist', compact('movies', 'page', 'type'));
    }

    public function createFreeMovie()
    {
        $type = "movie";
        return view('admin.free.add', compact('type'));
    }

    public function storeFreeMovie(Request $request)
    {
        $data = $request->all();
        $poster = " ";
        $message = 'Something went wrong';

        $min = DB::table('contents')->where('content_source', 'admin')->orderBy('content_source_id', 'DESC')->first();
        $min_id = 0;
        if ($min) {
            $min_id = rand(20, 999999);
            // $min_id = $min->content_source_id + 1;
        } else {
            $min_id = 1;
        }

        if ($request->hasFile('poster')) {
            $file = $request->poster;
            $file->move(public_path() . '/poster/', time() . $file->getClientOriginalName());
            // $file->move(public_path(). '/thumbnail/', time().$file->getClientOriginalName());
            $poster = time() . $file->getClientOriginalName();
        }

        $release_year = explode('-', $data['cinema_release_date'])[0];

        $newData = DB::table('contents')->insert(array(
            'title' => $data['title'] ?? '',

            'original_title' => $data['title'] ?? '',
            'content_type' => $data['content_type'],
            'poster' => $poster,
            'runtime' => $data['runtime'] ?? '',
            'content_source' => 'admin',
            'short_description' => $data['short_description'] ?? '',
            'content_source_id' => $min_id,
            'tmdb_popularity' => $data['popularity'] ?? '',
            'age_certification' => $data['age_certification'] ?? '',
            'original_release_year' => $release_year,
            'cinema_release_date' => $data['cinema_release_date'] ?? '',
            'tmdb_score' => $data['tmdb_score'] ?? '',
            'imdb_score' => $data['imdb_score'] ?? '',
            'tags' => $data['tags'] ?? '',
            'meta_title' => $data['meta_title'] ?? '',
            'meta_description' => $data['meta_description'] ?? '',
            'parent_id' => '0',
            'view_type' => 'free'
        ));
        $message = 'Content successfully added.';
        return redirect()->back()->with('success', $message);
    }

    public function deleteContent($id)
    {
        $content = DB::table('contents')->where('content_id', $id)->delete();
        $banners = DB::table('content_banners')->where('content_id', $id)->delete();
        $clips = DB::table('content_clips')->where('content_id', $id)->delete();
        $casting = DB::table('content_casting')->where('content_id', $id)->delete();
        $provider = DB::table('content_providers')->where('content_id', $id)->delete();
        $provider = DB::table('content_genres')->where('content_id', $id)->delete();


        $seasons = DB::table('contents')->where('parent_id', $id)->get();

        if (count($seasons) > 0) {
            foreach ($seasons as $season) {
                $episodes = DB::table('contents')->where('parent_id', $season->content_id)->get();
                foreach ($episodes as $episode) {
                    $banners = DB::table('content_banners')->where('content_id', $episode->content_id)->delete();
                    $clips = DB::table('content_clips')->where('content_id', $episode->content_id)->delete();
                    $casting = DB::table('content_casting')->where('content_id', $episode->content_id)->delete();
                    $provider = DB::table('content_providers')->where('content_id', $episode->content_id)->delete();
                    $provider = DB::table('content_genres')->where('content_id', $episode->content_id)->delete();
                }

                $episodes = DB::table('contents')->where('parent_id', $season->content_id)->delete();

                $banners = DB::table('content_banners')->where('content_id', $season->content_id)->delete();
                $clips = DB::table('content_clips')->where('content_id', $season->content_id)->delete();
                $casting = DB::table('content_casting')->where('content_id', $season->content_id)->delete();
                $provider = DB::table('content_providers')->where('content_id', $season->content_id)->delete();
                $provider = DB::table('content_genres')->where('content_id', $season->content_id)->delete();
            }
        }

        $seasons = DB::table('contents')->where('parent_id', $id)->delete();

        return redirect()->back()->with('message', 'Data successfully deleted');
    }


    public function setfavouriteContent($id, Request $request)
    {
        $content = DB::table('contents')->where('content_id', $id)->update(['featured' => $request->priority]);

        return redirect()->back()->with('message', 'Data successfully updated');
    }

    public function globalSearch(Request $request)
    {
        $page = 0;
        $keyword = $request->keyword;

        if (isset($_GET['page'])) {
            $page = $_GET['page'] - 1;
        }
        session()->put('search', $keyword);
        $movies = DB::table('contents')->where('content_id', $keyword)->orWhere('title', 'LIKE', $keyword . '%')->orwhere('original_title', 'LIKE', $keyword . '%')->orderBy('content_type', 'ASC')->orderBy('title', 'ASC')->orderBy('original_title', 'ASC')->paginate(50);

        return view('admin.contents.search_result', compact('page', 'movies', 'keyword'));
    }

    public function getSearchedResult(Request $request)
    {
        $page = 0;

        $keyword = $request->keyword;
        $content_type = $request->content_type;

        if (isset($_GET['page'])) {
            $page = $_GET['page'] - 1;
        }
        $movies = DB::table('contents')->where('content_type', $content_type)->where('title', 'LIKE', $keyword . '%')->orderBy('title', 'ASC')->orderBy('original_title', 'ASC')->paginate(50);

        return view('admin.contents.search_result', compact('page', 'movies', 'keyword', 'content_type'));
    }
}
