<?php
use App\Seo;
use App\Genre;
use App\FeaturedImage;
$flag = 0;
$type = "";

$seo = Seo::first();
?>

@extends('layouts.khoj_new') 

@section('tags')
{{$seo->page_tags ?? 'khojaap online movie search, watch online '}}
@endsection

@section('title')
{{$seo->page_title ?? 'Khojapp - Stream online movies on khoj'}}
@endsection

@section('description')
{{$seo->meta_description ?? 'We show you where you can legally watch movies and TV shows that you love. You are kept up to date with what is new on Netflix, Amazon Prime, iTunes and many other streaming platforms.'}}
@endsection

@section('banner')

<style>

.inner {
  padding: 6px;
}

.inner img {
  width: 100%;
  border-radius: 10px;
  height: auto;
}

.web-image{
  text-align: center;
  margin: auto;
  height: 300px; 
  width: auto;
}
img:hover {
  opacity: 0.9;
}

#playlist-content h3, #playlist-content span {
  /*margin-left: 35px;*/
}

.playlist_text{
  padding-left: 31px;
}

#playlist-img img {
  width: 100%; height: auto;
  margin-bottom: 25px;
}
</style>
@endsection
@section('content')
<div class="col-sm-1">
  </div>
  <div class="col-sm-10 col-lg-10 col-xs-12 right-filter">
    <div class="row" style="width: 100%; margin-top: 30px;padding-left: 3%;">
    <div class="col-sm-7 col-xs-12">
        
      <h2 style="margin-left: 35px;">{{ $playlist->playlist_name ?? '' }} &nbsp;&nbsp;
        
      </h2>
      <br><br>
      <p style="margin-left: 35px;">{{ $playlist->description ?? ''}}</p>
     
    </div>
      <div class="col-sm-3 col-xs-12">
        
      @if(!empty($playlist->poster) && $playlist->poster != '')
          <img src="/poster/{{$playlist->poster}}" alt="{{$playlist->playlist_name ?? ''}}" class="poster-image web-image">
          @else
          <img src="/images/default.png" alt="{{$playlist->playlist_name ?? ''}}" class="web-image">
          @endif
     
    </div>
      <div class="col-sm-1 col-xs-12"></div>

    
    </div>

    <div class="movie-content" id="dynamic-search" style="height: 100%; padding-top: 60px; margin-top: 30px;">
      <div class="row">
       <div class="col-sm-8">
        
      @if (!empty($playlists))
      <?php $space = 0; $rat=0;?>
      @foreach ($playlists as $element)
      <?php 
      $parent_title = "";

      $flag = 1; 
      $space++;
      
      ?>

     
<div class="row">
        <?php $urlen = preg_replace('/[^a-zA-Z0-9_.]/', '-', (strtolower($element->title))).'-'.$element->content_id; ?>
        <a href="/detail/{{$element->content_type}}/{{$urlen}}">
          <div class="col-md-1"></div>
        <div class="col-md-3" id="playlist-img"> @if(isset($element->poster) && strpos($element->poster, 'poster/') == false && file_exists(public_path().'/poster/'.$element->poster))
          <img src="/poster/{{$element->poster}}" alt="{{$first->element ?? ''}}" class="poster-image web-image">
          @else
          <img src="/images/default.png" alt="{{$first->element ?? ''}}" class="web-image">
          @endif
        </div>
        <div class="col-md-8 playlist_text" id="playlist-content">
          <h3>{{$element->title}}</h3>
          <br>
          <span>{{$element->short_description}}</span>
          
        </div>
        </a>
      </div>
<?php $flag++; ?>
@endforeach

@endif

    </div>
    <div class="col-sm-4"></div>
      </div>
@if($flag == 0)
<h4 class="result-sec">No result found</h4>
@endif

</div>
</div>

@endsection 

@section('paginate')


<div class="container" style="text-align: center;">
  <div class="row">
    @if (!is_array($playlists) && $tab != "Select Your favourite" && $playlists->links() != null)
    {{$playlists->links()}}
  @endif
    
  </div>
</div>


@endsection

@section('script')



@endsection
