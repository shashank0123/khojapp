<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use DB;
use App\ContentCasting;
use App\Content;
use App\ContentClip;
use App\ContentGenre;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function mergeContent($content_id)
    {
    	$checkname = \DB::table('contents')->where('content_id', $content_id)->first();
    	if ($checkname){
    		// do the merge check 
    	}
    	else{
    		// no movie found so return null 
    		return 0;
    	}
    }


    public function verifyfromjustwatch($movie)
    {
        $id = $movie->content_id;

        if ($movie->content_type == "tv-season") {
            $localtype = "show";
            $justwatch_urlnew = "https://apis.justwatch.com/content/titles/show_season/" . $movie->content_source_id . "/locale/en_IN?language=en";
        } else {
            $localtype = $movie->content_type;
            if ($localtype == 'shows') {
                $localtype = 'show';
            }

            // dd($movie);
            $justwatch_urlnew = "https://apis.justwatch.com/content/titles/" . $localtype . "/" . $movie->content_source_id . "/locale/en_IN?language=en";
        }
        $orig_title = $movie->title;
        // $justwatch_urlnew = "https://apis.justwatch.com/content/titles/show/11188/locale/en_IN";

        $genrerequest = curl_init();
        curl_setopt($genrerequest, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($genrerequest, CURLOPT_URL, $justwatch_urlnew);
        curl_setopt($genrerequest, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($genrerequest, CURLOPT_SSL_VERIFYPEER, false);
        $contents = curl_exec($genrerequest);

        $contents = json_decode($contents);
        // dd($contentFkes);
        $newdata = array();
        $newdata = $this->formatjustwatchcontent($movie, $contents, $newdata);
        if (isset($contents->episodes)) {
            foreach ($contents->episodes as $key79 => $episode) {

                $episodes = $this->processjustwatchepisode($episode, $movie, $orig_title);
            }
        }

        // dd($contents);
        $newdata1 = $this->storejustwatchimage($movie, $contents, $newdata);


        if (!empty($newdata)) {
            $newmovie = DB::table('contents')->where('content_id', $id)->update($newdata);
            $movie = Content::where('content_id', $id)->first();
        }
        // =================================justwatch==============================
        if (isset($contents->seasons)) {
            $totalseasons = DB::table('contents')->where('parent_id', $movie->content_id)->where('content_type', "tv-season")->get();
            if (count($contents->seasons) > 0) {
                foreach ($contents->seasons as $key23 => $value23) {
                    $check = DB::table('contents')->where('content_source_id', $value23->id)->where('content_type', "tv-season")->first();
                    $seasontitle = "";
                    if (isset($value23->title)){
                        $seasontitle = $value23->title;
                    }
                    else 
                        $seasontitle = "Season ".$value23->season_number;
                    if ($check) {
                        $db_data = $check;
                        $updateData = [];
                        $updateData = $this->storejustwatchimage($check, $contents, $updateData);
                        if ($check->parent_id == '0') {
                            $updateData['parent_id'] = $movie->content_id;
                            $updateData['content_type'] = 'tv-season';
                        }

                        if ($updateData) {
                            $db_data = DB::table('contents')->where('content_id', $check->content_id)->update($updateData);
                        }
                        $seasoncontentid = $check->content_id;
                    } else {
                        $db_data = DB::table('contents')->insert(
                            array(
                                'title' => $seasontitle,
                                'original_title' => $orig_title . " " . $seasontitle,
                                'content_type' => "tv-season",
                                'poster' => '',
                                'content_source' => 'justwatch',
                                'content_source_id' => $value23->id,
                                'full_path' => $value23->full_path ?? "",
                                'tmdb_popularity' => $tmdb_popularity ?? '',
                                'original_release_year' => $contents->original_release_year ?? '',
                                'cinema_release_date' => $contents->cinema_release_date ?? '',
                                'original_language' => $contents->original_language ?? "",
                                'tmdb_score' => $tmdb_score ?? "",
                                'imdb_score' => $imdb_score ?? '',
                                'parent_id' => $movie->content_id
                            )
                        );
                        $db_data = DB::table('contents')->where('title', $seasontitle)->where('content_type', "tv-season")->where('parent_id', $movie->content_id)->first();

                        $seasoncontentid = $db_data->content_id;
                    }
                    // ===================season episodce ============
                    $url = "https://apis.justwatch.com/content/titles/show_season/" . $value23->id . "/locale/en_IN?language=en";
                    $seasonrequest = curl_init();
                    curl_setopt($seasonrequest, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($seasonrequest, CURLOPT_URL, $url);
                    curl_setopt($seasonrequest, CURLOPT_SSL_VERIFYHOST, false);
                    curl_setopt($seasonrequest, CURLOPT_SSL_VERIFYPEER, false);
                    $seasonsdetails = curl_exec($seasonrequest);
                    $seasonsdetails = json_decode($seasonsdetails);
                    // dd($seasonsdetails);
                    if (isset($seasonsdetails->episodes)) {
                        foreach ($seasonsdetails->episodes as $key79 => $episode) {
                            // echo \gettype($episode);
                            // dd($episode);
                            $check1 = DB::table('contents')->where('content_source_id', $episode->id)->where('content_type', "tv-episode")->first();
                            if ($check1) {
                                $updateData = [];
                                $updateData = $this->storejustwatchimage($check1, $episode, $updateData);
                                if ($check1->parent_id == '0') {
                                    $updateData['parent_id'] = $seasoncontentid;
                                }
                                if ($updateData) {
                                    DB::table('contents')->where('content_source_id', $episode->id)->where('content_type', 'tv-episode')->update($updateData);
                                }
                            } else {
                                if (!isset($episode->title)) {
                                    $episode->title = "Episode " . $episode->episode_number;
                                }
                                $db_data1 = DB::table('contents')->insert(
                                    array(
                                        'title' => $episode->title,
                                        'original_title' => $orig_title . " " . $episode->title,
                                        'content_type' => "tv-episode",
                                        'poster' => '',
                                        'content_source' => 'justwatch',
                                        'content_source_id' => $episode->id,
                                        'full_path' => $episode->full_path ?? "",
                                        'tmdb_popularity' => $tmdb_popularity ?? '',
                                        'original_release_year' => $contents->original_release_year ?? '',
                                        'cinema_release_date' => $contents->cinema_release_date ?? '',
                                        'original_language' => $contents->original_language ?? "",
                                        'tmdb_score' => $tmdb_score ?? "",
                                        'imdb_score' => $imdb_score ?? '',
                                        'parent_id' => $seasoncontentid
                                    )
                                );
                                $check1 = DB::table('contents')->where('content_source_id', $episode->id)->where('content_type', "tv-episode")->first();
                            }

                            if (isset($episode->offers)) {
                                if (count($episode->offers) > 0) {
                                    foreach ($episode->offers as $key2 => $value2) {
                                        $deleteoldprovider = DB::table('content_providers')->where('content_id', $check1->content_id)->where('added_by', 'justwatch')->delete();
                                        $this->addjustwatchprovider($value2, $check1->content_id);
                                    }
                                }
                            }
                        }
                    }
                    // ===================season episodce ============
                }
            }
        }
        $casts = DB::table('content_casting')
            ->where('content_id', $id)
            ->get();
        // die();
        // if (count($casts)<1 && isset($contents->credits)){
        if (isset($contents->credits)) {
            // dd($contents->credits);

            // if (count($casts)!= count($contents->credits)){
            foreach ($contents->credits as $key => $value) {
                $castingcheck = ContentCasting::where('person_id', $value->person_id)->where('content_id', $id)->where('role', $value->role)->first();
                $casting = array();
                if (!$castingcheck) {
                    $casting['priority'] = $key;
                    if (isset($value->role)) {
                        $casting['role'] = $value->role;
                    }
                    $casting['content_id'] = $id;
                    if (isset($value->character_name)) {
                        $casting['character_name'] = $value->character_name;
                    }
                    if (isset($value->name)) {
                        $casting['name'] = $value->name;
                    }
                    if (isset($value->person_id)) {
                        $casting['person_id'] = $value->person_id;
                    }
                    $casting['priority'] = $key+1;
                    // dd($casting);
                    if ($casting['name']) {
                        $addcast = ContentCasting::insert($casting);
                    }

                    // else
                    //  $addcast = ContentCasting::where('content_id', $casting->content_id)->where('person_id', $casting->person_id)->update(['priority' => $key]);
                } else {
                    $casting['priority'] = $key+1;
                    // dd($casting);
                    $addcast = ContentCasting::where('content_id', $id)->where('person_id', $castingcheck->person_id)->where('role', $value->role)->update($casting);
                }
            }
            // }
        }
        // var_dump($value);

        $genres = DB::table('content_genres')
            ->leftJoin('genres', 'genres.genre_id', 'content_genres.genre_id')
            ->where('content_genres.content_id', $id)
            ->select('genres.genre_id', 'genres.title')
            ->distinct('genre_id')
            ->get();

        if (count($genres) < 1 && isset($contents->genre_ids)) {

            foreach ($contents->genre_ids as $key => $value) {
                $castingcheck = ContentGenre::where('content_id', $id)->first();
                if (!$castingcheck) {
                    $genres = array();
                    $genres['content_id'] = $id;

                    $genres['genre_id'] = $value;
                    $addcast = ContentGenre::insert($genres);
                }
            }
        }

        $banners = DB::table('content_banners')
            ->where('content_id', $id)
            ->get();
        $clips = DB::table('content_clips')
            ->where('content_id', $id)
            ->get();
            if (isset($contents->clips)) {
                foreach ($contents->clips as $key => $value) {
                    $castingcheck = ContentClip::where('content_id', $id)->where('external_id', $value->external_id)->first();
                    if (!$castingcheck) {
                        $genres = array();
                        $genres['content_id'] = $id;
                        $genres['type'] = $value->type;
                        $genres['provider'] = $value->provider;
                        $genres['external_id'] = $value->external_id;
                        $genres['title'] = $value->name ? $value->name :'';
                        $addcast = ContentClip::insert($genres);
                    }
                }
            }
            $clips = DB::table('content_clips')
                ->where('content_id', $id)
                ->get();
       
        $providers = DB::table('content_providers')
            ->where('content_id', $id)
            ->get();


        if (isset($contents->offers)) {


            // if (count($providers)!=count($contents->offers)){
            if (isset($contents->offers)) {
                $providerlist = $contents->offers;

                foreach ($providerlist as $key2 => $value2) {
                    // dd
                    $deleteoldprovider = DB::table('content_providers')->where('content_id', $id)->where('added_by', 'justwatch')->delete();
                    $this->addjustwatchprovider($value2, $id);
                }
            }

            // }
        }
    }

    public function verifyfromtmdb($movie)
    {
        $id = $movie->content_id;
        $orig_title = $movie->title;

        if ($movie->content_type == 'show') {
            $content_type = 'tv';
        } else {
            $content_type = $movie->content_type;
        }
        // $justwatch_urlnew = "https://apis.justwatch.com/content/titles/show/11188/locale/en_IN";
        $url = "https://api.themoviedb.org/3/" . $content_type . "/$movie->content_source_id?api_key=f0dc06cd59a97bbddcebd05e902559e0&language=en-US";

        $c = curl_init();
        curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($c, CURLOPT_URL, $url);
        curl_setopt($c, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($c, CURLOPT_SSL_VERIFYPEER, false);
        $content = curl_exec($c);
        $contents = json_decode($content);
        // return 0;

        $newdata = array();
        $this->formattmdbcontent($contents);
        // if (isset($contents->episodes)){
        //  foreach ($contents->episodes as $key79 => $episode) {
        //      $episodes = $this->processtmdbepisode($episode, $movie, $orig_title);
        //  }
        // }




        return 0;
        if (!empty($newdata)) {
            $newmovie = DB::table('contents')->where('content_id', $id)->update($newdata);
            $movie = Content::where('content_id', $id)->first();
        }
        // =================================tmdb==============================
        if (isset($contents->seasons)) {
            $totalseasons = DB::table('contents')->where('parent_id', $movie->content_id)->where('content_type', "tv-season")->get();
            if (count($contents->seasons) > 0) {
                foreach ($contents->seasons as $key23 => $value23) {
                    $check = DB::table('contents')->where('content_source_id', $value23->id)->where('content_type', "tv-season")->first();
                    if ($check) {
                        $db_data = $check;
                        $updateData = [];
                        $updateData = $this->storetmdbimage($check, $contents, $updateData);
                        if ($check->parent_id == '0') {
                            $updateData['parent_id'] = $movie->content_id;
                            $updateData['content_type'] = 'tv-season';
                        }

                        if ($updateData) {
                            $db_data = DB::table('contents')->where('content_id', $check->content_id)->update($updateData);
                        }
                        $seasoncontentid = $check->content_id;
                    } else {
                        $db_data = DB::table('contents')->insert(
                            array(
                                'title' => $value23->title,
                                'original_title' => $orig_title . " " . $value23->title,
                                'content_type' => "tv-season",
                                'poster' => '',
                                'content_source' => 'tmdb',
                                'content_source_id' => $value23->id,
                                'full_path' => $value23->full_path ?? "",
                                'tmdb_popularity' => $tmdb_popularity ?? '',
                                'original_release_year' => $contents->original_release_year ?? '',
                                'cinema_release_date' => $contents->cinema_release_date ?? '',
                                'original_language' => $contents->original_language ?? "",
                                'tmdb_score' => $tmdb_score ?? "",
                                'imdb_score' => $imdb_score ?? '',
                                'parent_id' => $movie->content_id
                            )
                        );
                        $db_data = DB::table('contents')->where('title', $value23->title)->where('content_type', "tv-season")->where('parent_id', $movie->content_id)->first();

                        $seasoncontentid = $db_data->content_id;
                    }
                    // ===================season episodce ============
                    $url = "https://apis.justwatch.com/content/titles/show_season/" . $value23->id . "/locale/en_IN?language=en";
                    $seasonrequest = curl_init();
                    curl_setopt($seasonrequest, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($seasonrequest, CURLOPT_URL, $url);
                    curl_setopt($seasonrequest, CURLOPT_SSL_VERIFYHOST, false);
                    curl_setopt($seasonrequest, CURLOPT_SSL_VERIFYPEER, false);
                    $seasonsdetails = curl_exec($seasonrequest);
                    $seasonsdetails = json_decode($seasonsdetails);
                    // dd($seasonsdetails);
                    if (isset($seasonsdetails->episodes)) {
                        foreach ($seasonsdetails->episodes as $key79 => $episode) {
                            // echo \gettype($episode);
                            // dd($episode);
                            $check1 = DB::table('contents')->where('content_source_id', $episode->id)->where('content_type', "tv-episode")->first();
                            if ($check1) {
                                $updateData = [];
                                $updateData = $this->storejustwatchimage($check1, $episode, $updateData);
                                if ($check1->parent_id == '0') {
                                    $updateData['parent_id'] = $seasoncontentid;
                                }
                                if ($updateData) {
                                    DB::table('contents')->where('content_source_id', $episode->id)->where('content_type', 'tv-episode')->update($updateData);
                                }
                            } else {
                                if (!isset($episode->title)) {
                                    $episode->title = "Episode " . $episode->episode_number;
                                }
                                $db_data1 = DB::table('contents')->insert(
                                    array(
                                        'title' => $episode->title,
                                        'original_title' => $orig_title . " " . $episode->title,
                                        'content_type' => "tv-episode",
                                        'poster' => '',
                                        'content_source' => 'justwatch',
                                        'content_source_id' => $episode->id,
                                        'full_path' => $episode->full_path ?? "",
                                        'tmdb_popularity' => $tmdb_popularity ?? '',
                                        'original_release_year' => $contents->original_release_year ?? '',
                                        'cinema_release_date' => $contents->cinema_release_date ?? '',
                                        'original_language' => $contents->original_language ?? "",
                                        'tmdb_score' => $tmdb_score ?? "",
                                        'imdb_score' => $imdb_score ?? '',
                                        'parent_id' => $seasoncontentid
                                    )
                                );
                                $check1 = DB::table('contents')->where('content_source_id', $episode->id)->where('content_type', "tv-episode")->first();
                            }

                            if (isset($episode->offers)) {
                                if (count($episode->offers) > 0) {
                                    foreach ($episode->offers as $key2 => $value2) {
                                        $deleteoldprovider = DB::table('content_providers')->where('content_id', $check1->content_id)->where('added_by', 'justwatch')->delete();
                                        $this->addjustwatchprovider($value2, $check1->content_id);
                                    }
                                }
                            }
                        }
                    }
                    // ===================season episodce ============
                }
            }
        }
        $casts = DB::table('content_casting')
            ->where('content_id', $id)
            ->get();
        // die();
        // if (count($casts)<1 && isset($contents->credits)){
        if (isset($contents->credits)) {
            // if (count($casts)!= count($contents->credits)){
            foreach ($contents->credits as $key => $value) {
                $castingcheck = ContentCasting::where('person_id', $value->person_id)->where('content_id', $id)->first();
                $casting = array();
                if (!$castingcheck) {
                    $casting['priority'] = $key;
                    if (isset($value->role)) {
                        $casting['role'] = $value->role;
                    }
                    $casting['content_id'] = $id;
                    if (isset($value->character_name)) {
                        $casting['character_name'] = $value->character_name;
                    }
                    if (isset($value->name)) {
                        $casting['name'] = $value->name;
                    }
                    if (isset($value->person_id)) {
                        $casting['person_id'] = $value->person_id;
                    }
                    if (!empty($casting)) {
                        $addcast = ContentCasting::insert($casting);
                    }
                    // else
                    //  $addcast = ContentCasting::where('content_id', $casting->content_id)->where('person_id', $casting->person_id)->update(['priority' => $key]);
                } else {
                    $casting['priority'] = $key;
                    // dd($casting);
                    $addcast = ContentCasting::where('content_id', $id)->where('person_id', $castingcheck->person_id)->update($casting);
                }
            }
            // }
        }
        // var_dump($value);

        $genres = DB::table('content_genres')
            ->leftJoin('genres', 'genres.genre_id', 'content_genres.genre_id')
            ->where('content_genres.content_id', $id)
            ->select('genres.genre_id', 'genres.title')
            ->distinct('genre_id')
            ->get();

        if (count($genres) < 1 && isset($contents->genre_ids)) {

            foreach ($contents->genre_ids as $key => $value) {
                $castingcheck = ContentGenre::where('content_id', $id)->first();
                if (!$castingcheck) {
                    $genres = array();
                    $genres['content_id'] = $id;

                    $genres['genre_id'] = $value;
                    $addcast = ContentGenre::insert($genres);
                }
            }
        }

        $banners = DB::table('content_banners')
            ->where('content_id', $id)
            ->get();
        $clips = DB::table('content_clips')
            ->where('content_id', $id)
            ->get();
        if (count($clips) < 1) {
            if (isset($contents->clips)) {
                foreach ($contents->clips as $key => $value) {
                    $castingcheck = ContentClip::where('content_id', $id)->first();
                    if (!$castingcheck) {
                        $genres = array();
                        $genres['content_id'] = $id;
                        $genres['type'] = $value->type;
                        $genres['provider'] = $value->provider;
                        $genres['external_id'] = $value->external_id;
                        $genres['title'] = $value->name;
                        $addcast = ContentClip::insert($genres);
                    }
                }
            }
            $clips = DB::table('content_clips')
                ->where('content_id', $id)
                ->get();
        }

        $providers = DB::table('content_providers')
            ->where('content_id', $id)
            ->get();


        if (isset($contents->offers)) {
            if (count($providers) != count($contents->offers)) {
                if (isset($contents->offers)) {
                    $providerlist = $contents->offers;

                    foreach ($providerlist as $key2 => $value2) {
                        $deleteoldprovider = DB::table('content_providers')->where('content_id', $id)->where('added_by', 'justwatch')->delete();
                        $this->addjustwatchprovider($value2, $id);
                    }
                }
            }
        }
    }

    public function addjustwatchprovider($value2, $id)
    {
        $checkprovider = DB::table('content_providers')->where('content_id', $id)->where('provider_id', $value2->provider_id)->where('video_quality', $value2->presentation_type)->first();

        // dd($checkprovider);
        // var_dump($checkprovider);
        // die();
        if (!$checkprovider) {
            $db_data = DB::table('content_providers')->insert(
                array(
                    'content_id' => $id,
                    'provider_id' => $value2->provider_id,
                    'video_quality' => $value2->presentation_type ?? '',
                    'monetization_type' => $value2->monetization_type ?? '',
                    'retail_price' => $value2->retail_price ?? '',
                    'web_url' => $value2->urls->standard_web ?? "",
                    'added_by' => 'justwatch',
                    'isLive' => 1
                )
            );
        }
        else{
            $db_data = DB::table('content_providers')->where('content_id', $id)->where('provider_id', $value2->provider_id)->update(
                array(
                    'added_by' => 'justwatch',
                    'isLive' => 1
                )
            );
        }
    }


    public function formatjustwatchcontent($movie, $contents, $newdata)
    {
        if (isset($contents->short_description) && $movie->short_description != $contents->short_description) {
            $newdata['short_description'] = $contents->short_description;
        }

        if (isset($contents->title) && $movie->title != $contents->title) {
            $newdata['title'] = $contents->title;
        }

        if (isset($contents->original_release_year) && $movie->original_release_year != $contents->original_release_year) {
            $newdata['original_release_year'] = $contents->original_release_year;
        }
        if (isset($contents->localized_release_date)) {
            $newdata['localized_release_date'] = $contents->localized_release_date;
        }

        if (isset($contents->first_air_date)) {
            $newdata['localized_release_date'] = $contents->first_air_date;
            $newdata['cinema_release_date'] = $contents->first_air_date;
        }

        if (isset($contents->cinema_release_date)) {
            $newdata['cinema_release_date'] = $contents->cinema_release_date;
        }
        if (isset($contents->title)) {
            $newdata['title'] = $contents->title;
            $orig_title = $newdata['title'];
        }

        if (isset($contents->original_title)) {
            $newdata['original_title'] = $contents->original_title;
            $orig_title = $newdata['original_title'];
        }

        if (isset($contents->tmdb_popularity)) {
            $newdata['tmdb_popularity'] = $contents->tmdb_popularity;
        }

        if (isset($contents->runtime)) {
            $newdata['runtime'] = $contents->runtime;
        }

        if (isset($contents->full_path)) {
            $newdata['full_path'] = $contents->full_path;
        }
        if (isset($contents->scoring)) {
            foreach ($contents->scoring as $key34 => $value34) {
                if ($value34->provider_type == "imdb:score") {
                    $newdata['imdb_score'] = $value34->value;
                }
            }
        }

        return $newdata;
    }

    public function storejustwatchimage($currentdata, $newdata, $updatedata)
    {
        if (!empty($newdata->poster)) {
            $key1 = explode('/', $newdata->poster)[2];
            $url1 = 'https://images.justwatch.com/poster/' . $key1 . '/s592';
            $url2 = 'https://images.justwatch.com/poster/' . $key1 . '/s166';
            $base_path = public_path();
            if ($currentdata->poster == "" || strpos($currentdata->poster, 'poster/') == true || !file_exists($base_path.'/poster/' .$currentdata->poster)) {
                
                $keyname = $key1 . ".jpg";
                $img1 = $base_path . '/poster/' . $keyname;
                $img2 = $base_path . '/thumbnail/' . $keyname;
                file_put_contents($img1, file_get_contents($url1));
                file_put_contents($img2, file_get_contents($url2));
                $updatedata['poster'] = $keyname;
                return $updatedata;
            }
        }
    }



    public function processjustwatchepisode($episode, $movie, $orig_title)
    {
        // dd($episode);
        $check1 = DB::table('contents')->where('content_source_id', $episode->id)->where('content_type', "tv-episode")->first();
        if ($check1) {
            $updateData = [];
            if (($check1->poster == '' || strpos($check1->poster, 'poster/') >= 0) && isset($contents->poster)) {
                $updateData = $this->storejustwatchimage($check1, $episode);
            }

            if ($check1->parent_id == '0' || $check1->parent_id == $movie->content_id) {
                $updateData['parent_id'] = $movie->content_id;
            }
            if (isset($episode->episode_number)) {
                $updateData['episode_number'] = $episode->episode_number;
            }
            if (isset($episode->runtime)) {
                $updateData['runtime'] = $episode->runtime;
            }
            if (isset($episode->season_number)) {
                $updateData['season_number'] = $episode->season_number;
            }
            if (isset($episode->short_description)) {
                $updateData['short_description'] = $episode->short_description;
            }
            if ($updateData) {
                DB::table('contents')->where('content_id', $movie->content_id)->where('content_type', 'tv-episode')->update($updateData);
            }
        } else {
            $db_data1 = DB::table('contents')->insert(
                array(
                    'title' => $episode->title,
                    'original_title' => $orig_title . " " . $episode->title,
                    'content_type' => "tv-episode",
                    'poster' => '',
                    'content_source' => 'justwatch',
                    'content_source_id' => $episode->id,
                    'full_path' => $episode->full_path ?? "",
                    'tmdb_popularity' => $tmdb_popularity ?? '',
                    'original_release_year' => $contents->original_release_year ?? '',
                    'cinema_release_date' => $contents->cinema_release_date ?? '',
                    'original_language' => $contents->original_language ?? "",
                    'tmdb_score' => $tmdb_score ?? "",
                    'imdb_score' => $imdb_score ?? '',
                    'parent_id' => $movie->content_id
                )
            );
        }
    }

    public function verifyfrommxplayer($movie)
    {
        $id = $movie->content_id;

        $checkoriginal = Content::where('title', $movie->title)
            ->where('content_type', $movie->content_type)
            ->where('original_release_year', $movie->original_release_year)
            ->where('content_source', '!=', 'mxplayer')
            ->first();
        if (!$checkoriginal){
            $checkoriginal = Content::where('title', $movie->title)
            ->where('content_type', $movie->content_type)
            ->where('original_release_year', $movie->original_release_year+1)
            ->where('content_source', '!=', 'mxplayer')
            ->first();
        }
        if (!$checkoriginal){
            $checkoriginal = Content::where('title', $movie->title)
            ->where('content_type', $movie->content_type)
            ->where('original_release_year', $movie->original_release_year-1)
            ->where('content_source', '!=', 'mxplayer')
            ->first();
        }
        
        if ($checkoriginal && ($checkoriginal->content_source == 'justwatch' || $checkoriginal->content_source == 'ullu'|| $checkoriginal->content_source == 'tmdb')) {
            // dd($checkoriginal);

            if ($movie->full_path != "") {

                $web_url = "http://mxplayer.in" . $movie->full_path;
            } else
                $web_url = "";
            $mxplayercheck = DB::table('content_providers')->where('content_id', $checkoriginal->content_id)->where('provider_id', 700)->first();
            if (!$mxplayercheck)
                $db_data = DB::table('content_providers')->where('content_id', $id)->where('provider_id', 700)->update(
                    array(
                        'content_id' => $checkoriginal->content_id,
                        'monetization_type' => 'free',
                        'web_url' => $web_url,
                        'video_quality' => 'sd',
                        'provider_id' => 700,
                        'isLive' => 1
                    )
                );
            else
                // $db_data = DB::table('content_providers')->where('content_id', $id)->delete();
                $check3 = DB::table('content_providers')->where('content_id', $checkoriginal->content_id)->get();
            $url = "detail/movie/" . $checkoriginal->title . "-" . $checkoriginal->content_id;
            $delete = DB::table('contents')->where('content_id', $id)->delete();
            return $checkoriginal->content_id;
        } else {
            if ($movie->content_type == 'show') {
                $localtype = 'tvshow';
                $utype = 'collection';
            } else {
                $utype = 'video';
                $localtype = $movie->content_type;
            }

            $url = "https://api.mxplay.com/v1/web/detail/" . $utype . "?type=" . $localtype . "&id=" . $movie->content_source_id . "&platform=com.mxplay.desktop&content-languages=hi,en";
            // die();
            $c = curl_init();
            curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($c, CURLOPT_URL, $url);
            curl_setopt($c, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($c, CURLOPT_SSL_VERIFYPEER, 0);
            $mxplayer = curl_exec($c);

            $mxplayer = json_decode($mxplayer);
            // dd($mxplayer);

            // dd($data);
            $newdata = array();

            if (isset($mxplayer->description) && $movie->short_description != $mxplayer->description) {
                $newdata['short_description'] = $mxplayer->description;
            }

            if (isset($mxplayer->releaseYear) && $movie->original_release_year != $mxplayer->releaseYear) {
                $newdata['original_release_year'] = $mxplayer->releaseYear;
            }

            if (isset($mxplayer->releaseDate)) {
                $newdata['localized_release_date'] = explode('T', $mxplayer->releaseDate)[0];
                $newdata['cinema_release_date'] = explode('T', $mxplayer->releaseDate)[0];
            }

            if (isset($mxplayer->duration)) {
                $newdata['runtime'] = $mxplayer->duration;
            }

            if (isset($mxplayer->languages)) {
                $newdata['original_language'] = implode(',', $mxplayer->languages);
            } else {
                $newdata['original_language'] = 'hi';
            }


            // dd($mxplayer);
            $newmovie = DB::table('contents')->where('content_id', $id)->update($newdata);
            if ($movie->full_path == "") {
                $web_url = "http://mxplayer.in" . $mxplayer->webUrl;
            } else
                $web_url = "http://mxplayer.in" . $mxplayer->webUrl;
            $db_data = DB::table('content_providers')->where('content_id', $movie->content_id)->where('provider_id', 700)->update(
                array(
                    'content_id' => $movie->content_id,
                    'monetization_type' => 'free',
                    'video_quality' => 'sd',
                    'web_url' => $web_url,
                    'isLive' => 1
                )
            );

            // ==================================cast section =========================


            if ($mxplayer->contributors) {
                $casts = $mxplayer->contributors;
                foreach ($casts as $key => $value) {
                    $castingcheck = ContentCasting::where('content_id', $id)->where('name', $value->name)->first();
                    if (!$castingcheck) {
                        $casting = array();
                        $casting['role'] = $value->type;
                        $casting['person_id'] = $value->id;
                        $casting['content_id'] = $id;
                        $casting['name'] = $value->name;
                        if (!empty($casting)) {
                            $addcast = ContentCasting::insert($casting);
                        }
                    }
                }
            }
            return $id;
        }
    }

    public function verifyfromullu($movie)
    {
        $id = $movie->content_id;
        $url = "https://ullu.app/ulluCore/api/v2/media/fetchMediaBySlug?titleYearSlug=" . $movie->full_path;
        $data = file_get_contents($url);
        $data = json_decode($data);
        // dd($data);
        $newdata = array();

        if (isset($data->description) && $movie->short_description != $data->description) {
            $newdata['short_description'] = $data->description;
        }

        if (isset($data->releaseYear) && $movie->original_release_year != $data->releaseYear) {
            $newdata['original_release_year'] = $data->releaseYear;
        }

        if (isset($data->releaseDate)) {
            $newdata['localized_release_date'] = $data->releaseDate;
            $newdata['cinema_release_date'] = $data->releaseDate;
        }

        if (isset($data->multiLanguage1)) {
            $newdata['original_language'] = implode(',', $data->multiLanguage);
        } else {
            if ($data->suffix == "ENGLISH")
                $data->suffix = 'en';
            if ($data->suffix == "HINDI")
                $data->suffix = 'hi';
            $newdata['original_language'] = $data->suffix;
        }

        if (isset($data->title)) {
            $newdata['title'] = $data->title;
            $orig_title = $newdata['title'];
        }

        if (isset($data->original_title)) {
            $newdata['original_title'] = $data->original_title;
            $orig_title = $newdata['original_title'];
        }

        if (isset($data->tmdb_popularity)) {
            $newdata['tmdb_popularity'] = $data->tmdb_popularity;
        }

        if (isset($data->durationMinutes)) {
            $newdata['runtime'] = $data->durationMinutes;
        }
        if (isset($data->freelyAvailable) && $data->freelyAvailable == false) {
            $monetization_type = 'Flatrate';
        } else
            $monetization_type = 'Free';

        $newmovie = DB::table('contents')->where('content_id', $id)->update($newdata);
        $dbcheck = DB::table('content_providers')->where('content_id', $id)->where('provider_id', '701')->first();
        if (!$dbcheck) {
            $dbcheck = DB::table('content_providers')->insert(
                array(
                    'content_id' => $dbcheck->content_id,
                    'provider_id' => 701,
                    'video_quality' => 'HD',
                    'currency' => 'INR',
                    'monetization_type' => $monetization_type,
                    'retail_price' => '0',
                    'isLive' => '1',
                    'web_url' => "http://ullu.app/#/media/" . $movie->full_path,
                )
            );
        } else {
            $dbcheck = DB::table('content_providers')->where('content_id', $id)->where('provider_id', '701')->update(['monetization_type' => $monetization_type, 'web_url' => "http://ullu.app/#/media/" . $movie->full_path]);
        }

        // ==================================cast section =========================
        if ($data->director) {
            $casting = array();
            $casting['role'] = 'director';
            $casting['content_id'] = $id;
            $casting['name'] = $data->director;
            if (!empty($casting)) {
                $castingcheck = ContentCasting::where('role', 'director')->where('content_id', $id)->first();
                if (!$castingcheck)
                    $addcast = ContentCasting::insert($casting);
            }
        }

        if ($data->cast) {
            $casts = explode(',', $data->cast);
            foreach ($casts as $key => $value) {
                $castingcheck = ContentCasting::where('content_id', $id)->where('name', trim($value))->first();
                if (!$castingcheck) {
                    $casting = array();
                    $casting['role'] = 'actor';
                    $casting['content_id'] = $id;
                    $casting['name'] = trim($value);
                    if (!empty($casting)) {
                        $addcast = ContentCasting::insert($casting);
                    }
                }
            }
        }
    }
}
