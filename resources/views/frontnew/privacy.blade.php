@extends('layouts.khoj_new')

@section('content')
<section id="page-bg-color">
	<div class="container">
		<div class="row">
			<div class="privacy-sec1">
				<h2 class="head-title">Privacy Policy</h2>
				<h4>Privacy Policy Information</h4>
<p>This Policy describes how we treat personal information on the websites where it is located.<br><br>

We collect information from and about you.</p>
<ul>
<li>Contact information. For example, we might collect your email address if you sign up for our emails.</li>
<li>Information you submit or post. For example, we collect the information you post in a public space on our website. We also collect information if you respond to a survey.</li>

<li>Demographic information. We may collect your zip code to help you find sales relevant to you.
Other information. If you use our website, we may collect information about your IP address and the browser you’re using. We might look at what site you came from, or what site you visit when you leave us. If you use our mobile app, we may collect your GPS location. We might look at how often you use the app and where you downloaded it.</li>

<li>We collect information from you passively. We use tracking tools like browser cookies and web beacons to collect information from you.</li>

<li>We get information about you from third parties. For example, our business partners may give us information about you. Social media platforms may also give us information about you.
We may share information with third parties.</li>

<li>We will share information with our business partners. For example, we may share information with third parties who operate our “find ads near me” feature. We may also share information with third parties who co-sponsor a promotion. We may also share information with our retail partners. This might include those whose circulars and ads appear on our platforms.</li>

<li>We will share information if we think we have to in order to comply with the law or to protect ourselvesFor example, we will share information to respond to a court order or subpoena. We may share it if a government agency or investigatory body requests. We might share information when we are investigating potential fraud.</li>
</ul>



<p>We may share information with any successor to all or part of our business.For example, if part of our business was sold we may give our customer list as part of that transaction.</p>

<h4>We may share information for other reasons we may describe to you</h4>

<p>You have certain choices about sharing and marketing practices.</p>
<p>You can opt out of receiving our marketing emails. To stop receiving our promotional emails, follow the instructions in any promotional message you get from us. Don’t worry - even if you opt out of getting marketing messages, we will still send you transactional messages. These include responses to your questions or information about your account. You can control cookies and tracking tools.</p>

<h4>We use standard security measures.</h4>
<p>The Internet is not 100% secure. We cannot promise that your use of our sites will be completely safe. We encourage you to use caution when using the Internet. This includes not sharing your passwords. We keep personal information as long as it is necessary or relevant for the practices described in this Policy. We also keep information as otherwise required by law.</p>
<ul>
<li>We may link to other sites or have third party services on our site we don’t control.
If you click on a link to a third party site, you will be taken to websites we do not control. This includes social media sites. This policy does not apply to the privacy practices of these websites. Read the privacy policy of other websites and insurance carriers carefully. We are not responsible for these third party practices.</li>

<li>Our site and apps may also link to and/or serve third party ads or other content that contains their own cookies or tracking technologies.</li>

<li>We use tracking technologies for many reasons.</li>
<li>To understand the activities and behaviors of our consumers and website visitors.</li>
<li>To recognize new visitors to our websites and apps.</li>
<li>To recognize past customers.</li>
<li>To serve you with customized or interest-based advertising. These ads may appear on our website or others you visit
So we can better understand our audience, our customers, our website visitors, and their respective interests.</li>
<li>We engage in interest-based advertising.</li>
<li>We may serve interest-based advertising using personal and other information gathered about you over time across multiple websites or other platforms. This might include apps. These ads are served on websites or apps. They might also be served in emails. Interest-based or “online behavioral advertising” includes ads that are served to you after you leave our website, encouraging you to return. They also include ads we think are relevant to you based on your shopping habits or online activities.</li>
</ul>

			</div>
		</div>
	</div>
</section>


<script src="//code.jquery.com/jquery.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
@endsection