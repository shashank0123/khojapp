@extends('layouts.admin')


@section('content')
<h4 class="header-title m-t-0 m-b-30">Default Example</h4>
<h4 class="header-title m-t-0 m-b-30">Genres</h4>
@if(Session::get('success'))
<div class="alert alert-success"> {{Session::get('success')}} </div>
@endif
@if (count($errors) > 0)
<div class="alert alert-danger">
	<ul>
		@foreach ($errors->all() as $error)
		<li>{{ $error }}</li>
		@endforeach
	</ul>
</div>
@endif

<form method="POST" action="{{url('admin/store_genre/'.$id)}}" enctype="multipart/form-data">
	@csrf
	
	<div class="row input-control">
		<label class="col-sm-3">Select Genres</label>

		<div class="col-sm-6">
			
			<select name='genre_id' id="genre_id" class="form-control">

				@if(!empty($genres))
				@foreach($genres as $genre)
				<option value="{{$genre->genre_id}}">{{ucfirst($genre->title)}}</option>
				@endforeach
				@endif			
				
			</select>
		</div>
	</div>


	<div class="row form-group">
		<div class="col-sm-6 col-sm-offset-5">
			<button type="submit" name="save" class="btn btn-primary">Save</button> 
		</div>
	</div>

		<h4> Genres for <i>'{{ucfirst($content->title)}}'</i>: </h4>
<div class="row">
		<br><br>
		<ul>
			@if(!empty($content_genres))
			@foreach($content_genres as $genre)
			<li>&nbsp;<a href="{{url('admin/delete_genre/'.$genre->content_id.'/'.$genre->genre_id)}}"> <i class="fa fa-trash" style="cursor: pointer;" ></i> </a>&nbsp;&nbsp;{{$genre->title}} </li> 
			@endforeach
			@endif
		</ul>
	</div>


</form>


@endsection
