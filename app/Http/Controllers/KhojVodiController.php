<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Content;
use App\ContentGenre;
use App\Genre;
ini_set('memory_limit', '-1');

class KhojVodiController extends Controller
{
    public function getVodi(Request $request)
    {
        return view('khoj.vodidesign');
    }

    public function getVodiList(Request $request)
    {
        return view('khoj.vodicategory');
    }

    public function getVodiCategory(Request $request)
    {
        return view('khoj.vodicategory2');
    }

    public function getVodiDetail($type = false, $title = false, $id = false, Request $request)
    {
        $movie = Content::where('content_id', $id)->first();
        if ($movie){
            return view('khoj.vodidesigndetails', compact('id', 'movie'));
        }
        else{
            return redirect('/');
        }
        
    }

    public function getSlider()
    {

        //inputs 
        $source = 'web';
        $date = date('Y-m-d');
        $page = 'home';
        $content_type = 'movie';

        // get data 
        $rawcontents = Content::where('content_type', $content_type)
                            ->orderBy('click_count', 'desc')
                            ->limit(8)
                            ->get();

        // process data 
        $contents = [];

        foreach ($rawcontents as $key => $value) {
            $newobj = new \StdClass;
            $newobj->title = $value->title;
            $newobj->content_id = $value->content_id;
            $newobj->poster = $this->getImage($value);
            $newobj->short_description = $value->short_description;
            $newobj->runtime = $this->formatRuntime($value);
            $newobj->content_source = $value->content_source;
            $newobj->genre = $this->getGenre($value->content_id);
            array_push($contents, $newobj);
        }



        // response of function 
        return response()->json($contents);
        
    }


    public function getMovies(Request $request)
    {
        //inputs 
        $source = 'web';
        $date = date('Y-m-d');
        $page = 'home';
        $content_type = 'movie';
        if ($request->limit){
            $limit = $request->limit;
        }
        else{
            $limit = 8;
        }
        if ($request->offset){
            $offset = $request->offset;
        }
        else{
            $offset = 0;
        }

        // get data 
        $rawcontents = Content::where('content_type', $content_type)
                            ->orderBy('created_at', 'desc')
                            ->offset($offset)
                            ->limit($limit)
                            ->get();

        // process data 
        $contents = [];

        foreach ($rawcontents as $key => $value) {
            $newobj = new \StdClass;
            $newobj->title = $value->title;
            $newobj->content_id = $value->content_id;
            $newobj->poster = $this->getImage($value);
            $newobj->short_description = $value->short_description;
            $newobj->release_year = $this->getReleaseYear($value);
            $newobj->runtime = $this->formatRuntime($value);
            $newobj->content_source = $value->content_source;
            $newobj->genre = $this->getGenre($value->content_id);
            array_push($contents, $newobj);
        }



        // response of function 
        return response()->json($contents);
        
    }


    public function getShows(Request $request)
    {
        //inputs 
        $source = 'web';
        $date = date('Y-m-d');
        $page = 'home';
        $content_type = 'show';
        if ($request->limit){
            $limit = $request->limit;
        }
        else{
            $limit = 8;
        }
        if ($request->offset){
            $offset = $request->offset;
        }
        else{
            $offset = 0;
        }

        // get data 
        $rawcontents = Content::where('content_type', $content_type)
                            ->orderBy('created_at', 'desc')
                            ->offset($offset)
                            ->limit($limit)
                            ->get();

        // process data 
        $contents = [];

        foreach ($rawcontents as $key => $value) {
            $newobj = new \StdClass;
            $newobj->title = $value->title;
            $newobj->content_id = $value->content_id;
            $newobj->poster = $this->getImage($value);
            $newobj->short_description = $value->short_description;
            $newobj->release_year = $this->getReleaseYear($value);
            $newobj->runtime = $this->formatRuntime($value);
            $newobj->content_source = $value->content_source;
            $newobj->genre = $this->getGenre($value->content_id);
            array_push($contents, $newobj);
        }



        // response of function 
        return response()->json($contents);
        
    }


    public function getMovieByGenre($genre = false, Request $request)
    {
        //inputs 
        $source = 'web';
        $date = date('Y-m-d');
        $page = 'home';
        $content_type = 'movie';
        $genrerow = Genre::where('slug', $genre)->first();

        if ($genrerow){
            $genre_id = $genrerow->genre_id;
        }
        else 
            return [];


        if ($request->limit){
            $limit = $request->limit;
        }
        else{
            $limit = 8;
        }
        if ($request->offset){
            $offset = $request->offset;
        }
        else{
            $offset = 0;
        }

        // get data 
        $rawcontents = Content::join('content_genres', 'content_genres.content_id', '=', 'contents.content_id')
                            ->where('contents.content_type', $content_type)
                            ->where('content_genres.genre_id', $genre_id)
                            ->orderBy('contents.created_at', 'desc')
                            ->offset($offset)
                            ->limit($limit)
                            ->get();

        // process data 
        $contents = [];

        foreach ($rawcontents as $key => $value) {
            $newobj = new \StdClass;
            $newobj->title = $value->title;
            $newobj->poster = $this->getImage($value);
            $newobj->short_description = $value->short_description;
            $newobj->release_year = $this->getReleaseYear($value);
            $newobj->runtime = $this->formatRuntime($value);
            $newobj->content_source = $value->content_source;
            $newobj->genre = $this->getGenre($value->content_id);
            array_push($contents, $newobj);
        }



        // response of function 
        return response()->json($contents);
        
    }


    public function getShowByGenre($genre = false, Request $request)
    {
        //inputs 
        $source = 'web';
        $date = date('Y-m-d');
        $page = 'home';
        $content_type = 'show';
        $genrerow = Genre::where('slug', $genre)->first();

        if ($genrerow){
            $genre_id = $genrerow->genre_id;
        }
        else 
            return [];

        
        if ($request->limit){
            $limit = $request->limit;
        }
        else{
            $limit = 8;
        }
        if ($request->offset){
            $offset = $request->offset;
        }
        else{
            $offset = 0;
        }

        // get data 
        $rawcontents = Content::join('content_genres', 'content_genres.content_id', '=', 'contents.content_id')
                            ->where('contents.content_type', $content_type)
                            ->where('content_genres.genre_id', $genre_id)
                            ->orderBy('contents.created_at', 'desc')
                            ->offset($offset)
                            ->limit($limit)
                            ->get();

        // process data 
        $contents = [];

        foreach ($rawcontents as $key => $value) {
            $newobj = new \StdClass;
            $newobj->title = $value->title;
            $newobj->content_id = $value->content_id;
            $newobj->poster = $this->getImage($value);
            $newobj->short_description = $value->short_description;
            $newobj->release_year = $this->getReleaseYear($value);
            $newobj->runtime = $this->formatRuntime($value);
            $newobj->content_source = $value->content_source;
            $newobj->genre = $this->getGenre($value->content_id);
            array_push($contents, $newobj);
        }



        // response of function 
        return response()->json($contents);
        
    }


    public function getMovieByFeatured(Request $request)
    {
        //inputs 
        $source = 'web';
        $date = date('Y-m-d');
        $page = 'home';
        $content_type = 'movie';
        if ($request->limit){
            $limit = $request->limit;
        }
        else{
            $limit = 8;
        }
        if ($request->offset){
            $offset = $request->offset;
        }
        else{
            $offset = 0;
        }

        // get data 
        $rawcontents = Content::where('content_type', $content_type)
                            ->orderBy('created_at', 'desc')
                            ->offset($offset)
                            ->limit($limit)
                            ->get();

        // process data 
        $contents = [];

        foreach ($rawcontents as $key => $value) {
            $newobj = new \StdClass;
            $newobj->title = $value->title;
            $newobj->content_id = $value->content_id;
            $newobj->poster = $this->getImage($value);
            $newobj->short_description = $value->short_description;
            $newobj->release_year = $this->getReleaseYear($value);
            $newobj->runtime = $this->formatRuntime($value);
            $newobj->content_source = $value->content_source;
            $newobj->genre = $this->getGenre($value->content_id);
            array_push($contents, $newobj);
        }



        // response of function 
        return response()->json($contents);
        
    }


    public function getShowByFeatured(Request $request)
    {
        //inputs 
        $source = 'web';
        $date = date('Y-m-d');
        $page = 'home';
        $content_type = 'show';
        if ($request->limit){
            $limit = $request->limit;
        }
        else{
            $limit = 8;
        }
        if ($request->offset){
            $offset = $request->offset;
        }
        else{
            $offset = 0;
        }

        // get data 
        $rawcontents = Content::where('content_type', $content_type)
                            ->orderBy('created_at', 'desc')
                            ->offset($offset)
                            ->limit($limit)
                            ->get();

        // process data 
        $contents = [];

        foreach ($rawcontents as $key => $value) {
            $newobj = new \StdClass;
            $newobj->title = $value->title;
            $newobj->content_id = $value->content_id;
            $newobj->poster = $this->getImage($value);
            $newobj->short_description = $value->short_description;
            $newobj->release_year = $this->getReleaseYear($value);
            $newobj->runtime = $this->formatRuntime($value);
            $newobj->content_source = $value->content_source;
            $newobj->genre = $this->getGenre($value->content_id);
            array_push($contents, $newobj);
        }



        // response of function 
        return response()->json($contents);
        
    }


    public function getGenreByContentId($content_id = false, Request $request)
    {
        $movie = Content::where('content_id', $content_id)->first();
        if ($movie){
            $genre = $this->getGenre($movie);

            return response()->json($genre);
        }
        else{
            return response()->json([]);
        }
        
    }


    public function getOTTByContentId($content_id = false, Request $request)
    {
        $movie = Content::where('content_id', $id)->first();
        if ($movie){
            return view('khoj.vodidesigndetails', compact('id', 'movie'));
        }
        else{
            return redirect('/');
        }
        
    }


    public function getTrailerByContentId($content_id = false, Request $request)
    {
        $movie = Content::where('content_id', $id)->first();
        if ($movie){
            return view('khoj.vodidesigndetails', compact('id', 'movie'));
        }
        else{
            return redirect('/');
        }
        
    }



    public function getGenre($content_id)
    {
        // check if the content id is correct 
        $content = Content::where('content_id', $content_id)->first();
        if ($content){
            // check if the genre exists for the content 
            $genres = ContentGenre::where('content_id', $content_id)->get();
            return $genres;
        }
        else{
            return [];
        }
    }

    public function getOTT($content_id)
    {
        // check if the content id is correct 
        $content = Content::where('content_id', $content_id)->first();
        if ($content){
            // check if the genre exists for the content 
            $genres = ContentGenre::where('content_id', $content_id)->get();
            return $genres;
        }
        else{
            return [];
        }
    }


    public function formatRuntime($content)
    {
        // first check if the runtime is there
        if ($content->runtime == null){
            return '0 min';
        }

        // check the source of the content 
        // justwatch = minute
        // tmdb = minute
        // mxplayer = seconds

        if ($content->content_source == 'mxplayer'){
            $minute = $content->runtime/60;
            $minute = round($minute);
        }
        else $minute = $content->runtime;

        return "$minute min";


    }

    public function getImage($content)
    {
        if ($content->poster == ''){
            return url('/images/default.png');
        }
        else
            return url('/poster/'.$content->poster);

        // phase 2 is to create multiple size of images for the movie
    }

    public function getReleaseYear($content)
    {
        if ($content->original_release_year > 1700){
            return $content->original_release_year;
        }
        elseif ($content->localized_release_date != null){
            return explode('-', $content->localized_release_date)[0];
        }
        elseif ($content->cinema_release_date != null){
            return explode('-', $content->cinema_release_date)[0];
        }
        else return 'NA';
    }

    
}
