<!doctype html>
<html lang="en">
<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

		<link rel="stylesheet" href="/final/css/bootstrap.min.css">
		<link rel="stylesheet" href="/final/style.css">
		<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Lato:400,700%7CMontserrat:300,400,600,700">
		
		<link rel="stylesheet" href="/final/icons/fontawesome/css/fontawesome-all.min.css"><!-- FontAwesome Icons -->
		<link rel="stylesheet" href="/final/icons/Iconsmind__Ultimate_Pack/Line%20icons/styles.min.css"><!-- iconsmind.com Icons -->
		<script src="/final/js/libs/jquery-3.3.1.min.js"></script><!-- jQuery -->
		
		
		<title>BestPlaceto.Stream - Media Database</title>
	</head>
	<body>
		<div id="sidebar-bg">
			
      <header id="videohead-pro" class="sticky-header">
			<div id="video-logo-background"><a href="dashboard-home"><img src="/final/images/logo-video-layout.png" alt="Logo"></a></div>
			<?php if (isset(auth()->user()->profile_pic))
							 $profile_pic = '/images/user/'.auth()->user()->profile_pic;
							else {
								$profile_pic = '/final/images/demo/account-profile.jpg';
							}
						?>
			
			<div id="video-search-header">
				<div id="search-icon-more"></div>
					<form name="search-form" method="GET" action="/search" id="video-search-header-filtering-padding">
				<input type="text" name="keyword" onkeydown="if (event.keyCode == 13) { this.form.submit(); return false; }" placeholder="Search for Movies or TV Series" aria-label="Search">
				<div id="video-search-header-filtering">
						<div class="row">
							<div class="col-sm extra-padding">
								<h5>Type:</h5>
								
								<div class="row">
									<div class="col-sm">
										<label class="checkbox-pro-container">Movies
										  <input type="checkbox" value="movies" name="type[]" checked="checked" id="movies-type">
										  <span class="checkmark-pro"></span>
										</label>
								
										<label class="checkbox-pro-container">TV Series
										  <input type="checkbox" value="series" name="type[]" id="tv-type">
										  <span class="checkmark-pro"></span>
										</label>
									</div><!-- close .col -->
									<div class="col">
										<label class="checkbox-pro-container">New Arrivals
										  <input type="checkbox" value="new" name="type[]" id="movie-type">
										  <span class="checkmark-pro"></span>
										</label>
								
										<label class="checkbox-pro-container">Documentary
										  <input type="checkbox" name="type" value="doc" id="documentary-type">
										  <span class="checkmark-pro"></span>
										</label>
									</div><!-- close .col -->
								</div><!-- close .row -->
								
								<div class="dotted-dividers-pro"></div>
							</div><!-- close .col -->
							<div class="col-sm extra-padding">
								<h5>Genres:</h5>
								<select class="custom-select" name="genre">
									@foreach (session()->get('genre') as $key => $value)
									  <option value="{{$value->genre_id}}">{{$value->name}}</option>
										{{-- expr --}}
									@endforeach
								  
								</select>
								<div class="dotted-dividers-pro"></div>
							</div><!-- close .col -->
							<div class="col-sm extra-padding">
								<h5>Country:</h5>
								<select class="custom-select" name="country">
								  <option value="ALL" selected>All Countries</option>
								  <option value="AU">Argentina</option>
								  <option value="AS">Australia</option>
								  <option value="BS">Bahamas</option>
								  <option value="BE">Belgium</option>
								  <option value="BR">Brazil</option>
								  <option value="CA">Canada</option>
								  <option value="CL">Chile</option>
								  <option value="CH">China</option>
								  <option value="DE">Denmark</option>
								  <option value="EU">Ecuador</option>
								  <option value="FR">France</option>
								  <option value="GE">Germany</option>
								  <option value="GR">Greece</option>
								  <option value="GT">Guatemala</option>
								  <option value="IT">Italy</option>
								  <option value="JA">Japan</option>
								  <option value="KO">Korea</option>
								  <option value="ML">Malaysia</option>
								  <option value="MA">Monaco</option>
								  <option value="MO">Morocco</option>
								  <option value="NZ">New Zealand</option>
								  <option value="PA">Panama</option>
								  <option value="PT">Portugal</option>
								  <option value="RU">Russia</option>
								  <option value="GB">United Kingdom</option>
								  <option value="US">United States</option>
								</select>
								<div class="dotted-dividers-pro"></div>
							</div><!-- close .col -->
							<div class="col-sm extra-padding extra-range-padding">
								<h5>Average Rating:</h5>
				            <input class="range-example-rating-input" name="rating" type="text" min="0" max="10" value="4,10" step="1" />
								<!-- JS is under /js/script.jss -->
							</div><!-- close .col -->
						</div><!-- close .row -->
						<div id="video-search-header-buttons">
							<button type="submit" class="btn btn-green-pro">Filter Search</button>
							<input type="reset" name="reset" class="btn" value="Reset" style="border: 1px solid #ccc; padding: 10px 20px; margin-top: 0; width: auto;">
						</div><!-- close #video-search-header-buttons -->
					</form><!-- #video-search-header-filtering-padding -->
				</div><!-- close #video-search-header-filtering -->
			</div><!-- close .video-search-header -->
			
			<div id="mobile-bars-icon-pro" class="noselect"><i class="fas fa-bars"></i></div>
			
			@guest
                            <a href="/register" class="btn btn-header-pro btn-green-pro noselect">Register</a>
                
			                <button class="btn btn-header-pro noselect" data-toggle="modal" data-target="#LoginModal" role="button">Sign In</button>
                        @else
                            
                            <div id="header-user-profile">
								<div id="header-user-profile-click" class="noselect">
									<img src="{{$profile_pic}}" alt="{{ Auth::user()->name }}">
									<div id="header-username">{{ Auth::user()->name }}</div><i class="fas fa-angle-down"></i>
								</div><!-- close #header-user-profile-click -->
								<div id="header-user-profile-menu">
									<ul>
										<li><a href="/profile"><span class="icon-User"></span>My Profile</a></li>
										<li><a href="/profile/favorites"><span class="icon-Favorite-Window"></span>My Favorites</a></li>
										<li><a href="/account"><span class="icon-Gears"></span>Account Details</a></li>
										<li><a href="help"><span class="icon-Life-Safer"></span>Help/Support</a></li>
										<li><a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form></li>
									</ul>
								</div><!-- close #header-user-profile-menu -->
							</div><!-- close #header-user-profile -->
							
							<div id="header-user-notification" style="margin-top: 15px;">
								<div id="header-user-notification-click" class="noselect">
									<i class="far fa-bell"></i>
									<span class="user-notification-count">0</span>
								</div><!-- close #header-user-profile-click -->
								<div id="header-user-notification-menu" style="margin-top: 15px;">
									<h3>Notifications</h3>
									<div id="header-notification-menu-padding">
											<ul id="header-user-notification-list">
												{{-- <li><a href="#!"><img src="/final/images/demo/user-profile-2.jpg" alt="Profile">Lorem ipsum dolor sit amet, consec tetur adipiscing elit. <div class="header-user-notify-time">21 hours ago</div></a></li>
												<li><a href="#!"><img src="/final/images/demo/user-profile-3.jpg" alt="Profile">Donec vitae lacus id arcu molestie mollis. <div class="header-user-notify-time">3 days ago</div></a></li>
												<li><a href="#!"><img src="/final/images/demo/user-profile-4.jpg" alt="Profile">Aenean vitae lectus non purus facilisis imperdiet. <div class="header-user-notify-time">5 days ago</div></a></li> --}}
											</ul>
											<div class="clearfix"></div>
										</div><!-- close #header-user-profile-menu -->
									</div>
							</div><!-- close #header-user-notification -->
                        @endguest
			
			
			
			
			
			<div class="clearfix"></div>
			
			<nav id="mobile-navigation-pro">
			
				<ul id="mobile-menu-pro">
	            <li>
	              <a href="/">
						<span class="icon-Reel"></span>
	                Movies
	              </a>
	            </li>
	            <li class="current-menu-item">
	              <a href="/tvseries">
						<span class="icon-Old-TV"></span>
	                TV Series
	              </a>
	            <li>
	            <li>
	              <a href="playlists">
						<span class="icon-Movie"></span>
	                Playlists
	              </a>
	            </li>
	            <li>
	              <a href="/movies/newest">
						<span class="icon-Movie-Ticket"></span>
	                New Arrivals
	              </a>
	            </li>
	            <li>
	              <a href="/movies/upcoming">
						<span class="icon-Clock"></span>
	                Coming Soon
	              </a>
	            </li>
	            <li>
	              <a href="#!">
						<i class="far fa-bell"></i>
						<span class="user-notification-count">3</span>
	                Notifications
	              </a>
	            </li>
				</ul>
				<div class="clearfix"></div>
				
				<div id="search-mobile-nav-pro">
					<input type="text" placeholder="Search for Movies or TV Series" aria-label="Search">
				</div>
				
			</nav>
			
      </header>
		
		
		
		<nav id="sidebar-nav"><!-- Add class="sticky-sidebar-js" for auto-height sidebar -->
            <ul id="vertical-sidebar-nav" class="sf-menu">
              <li class="normal-item-pro">
                <a href="/">
						<span class="icon-Reel"></span>
                  Movies
                </a>
              </li>
              <li class="normal-item-pro current-menu-item">
                <a href="/tvseries">
						<span class="icon-Old-TV"></span>
                  TV Series
                </a>
              </li>
              <li class="normal-item-pro">
                <a href="/playlists">
						<span class="icon-Movie"></span>
                  Playlists
                </a>
              </li>
              <li class="normal-item-pro">
                <a href="/movies/newest">
						<span class="icon-Movie-Ticket"></span>
                  New Arrivals
                </a>
              </li>
              <li class="normal-item-pro">
                <a href="/movies/upcoming">
						<span class="icon-Clock"></span>
                  Coming Soon
                </a>
              </li>

            </ul>
				<div class="clearfix"></div>
		</nav>

	


@yield('content')
<!-- Modal -->
        <div class="modal fade" id="LoginModal" tabindex="-1" role="dialog" aria-labelledby="LoginModal" aria-hidden="true">
             <button type="button" class="close float-close-pro" data-dismiss="modal" aria-label="Close">
                       <span aria-hidden="true">&times;</span>
            </button>
          <div class="modal-dialog modal-dialog-centered modal-md" role="document">
            <div class="modal-content">
                 <div class="modal-header-pro">
                     <h2>Welcome Back</h2>
                     <h6>Sign in to your account to continue using SKRN</h6>
                 </div>
                 <div class="modal-body-pro social-login-modal-body-pro">
                     
                     <div class="registration-social-login-container">
                         <form method="POST" action="{{ route('login') }}">
                            @csrf
                             <div class="form-group">
                                 <input type="text" name="email" class="form-control" id="username" placeholder="Username">
                             </div>
                             <div class="form-group">
                                 <input type="password" name="password" class="form-control" id="password" placeholder="Password">
                             </div>
                             <div class="form-group">
                             	<input type="submit" name="submit" value="Sign In" class="btn btn-green-pro btn-display-block">
                                 {{-- <button type="button" class="btn btn-green-pro btn-display-block">Sign In</button> --}}
                             </div>
                             <div class="container-fluid">
                                 <div class="row no-gutters">
                                    <div class="col checkbox-remember-pro"><input type="checkbox" id="checkbox-remember"><label for="checkbox-remember" class="col-form-label">Remember me</label></div>
                                    <div class="col forgot-your-password"><a href="#!">Forgot your password?</a></div>
                                </div>
                            </div><!-- close .container-fluid -->
                        
                         </form>
                     
                         <div class="registration-social-login-or">or</div>
                         
                     </div><!-- close .registration-social-login-container -->
                     
                     <div class="registration-social-login-options">
                        <h6>Sign in with your social account</h6>
                        <div class="social-icon-login facebook-color"><i class="fab fa-facebook-f"></i> Facebook</div>
                        <div class="social-icon-login twitter-color"><i class="fab fa-twitter"></i> Twitter</div>
                        <div class="social-icon-login google-color"><i class="fab fa-google-plus-g"></i> Google</div>
                     </div><!-- close .registration-social-login-options -->
                     
                     <div class="clearfix"></div>
                     

              </div><!-- close .modal-body -->
                
             <a class="not-a-member-pro" href="signup-step2">Not a member? <span>Join Today!</span></a>
            </div><!-- close .modal-content -->
          </div><!-- close .modal-dialog -->
        </div><!-- close .modal -->   


 </div><!-- close #sidebar-bg-->
		
		<!-- Required Framework JavaScript -->
		<script src="/final/js/libs/popper.min.js" defer></script><!-- Bootstrap Popper/Extras JS -->
		<script src="/final/js/libs/bootstrap.min.js" defer></script><!-- Bootstrap Main JS -->
		<!-- All JavaScript in Footer -->
		
		<!-- Additional Plugins and JavaScript -->
		<script src="/final/js/navigation.js" defer></script><!-- Header Navigation JS Plugin -->
		<script src="/final/js/jquery.flexslider-min.js" defer></script><!-- FlexSlider JS Plugin -->
		<script src="/final/js/jquery-asRange.min.js" defer></script><!-- Range Slider JS Plugin -->
		<script src="/final/js/circle-progress.min.js" defer></script><!-- Circle Progress JS Plugin -->
		<script src="/final/js/afterglow.min.js" defer></script><!-- Video Player JS Plugin -->
		<script src="/final/js/script.js" defer></script><!-- Custom Document Ready JS -->
		<script src="/final/js/script-dashboard.js" defer></script><!-- Custom Document Ready for Dashboard Only JS -->
		<script type="text/javascript">
			$(function() { // when page loads
			    $("#savechanges").on("click",function(e) { // when link clicked
			        e.preventDefault(); // stop the click from any further action
			        var fileInput = document.getElementById('profile_pic');
					var file = fileInput.files[0];
			        var formData = new FormData(document.querySelector('form#form'));
					formData.append('profile_pic', file);
			        var option = $("#favourite-genre li.active").map(function(i,e){return $(e).data("option")}); // get the selected option
			        option = $.makeArray(option).join();
					formData.append('genres', option);
			        

				    $.ajax({
				        url: '/account',
				        headers: {    'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')},
				        type: 'POST',
				        data: formData,
				        success: function (data) {
				            location.reload();

				        },
				        cache: false,
				        contentType: false,
				        processData: false
				    });
			    });
			    $('#OpenImgUpload').click(function(){ $('#profile_pic').trigger('click');
			     });
			    
			});
		</script>
		
	</body>

<!-- Mirrored from progression-studios.com/skrn/dashboard-home by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 15 Apr 2018 14:47:18 GMT -->
</html>