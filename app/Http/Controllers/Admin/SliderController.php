<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\FeaturedImage;

class SliderController extends Controller
{


public function index()
{
    $sliders = FeaturedImage::orderBy('page_title','ASC')->orderBy('display_priority','ASC')->get();
    return view('admin.sliders.list',compact('sliders'));
}


public function create()
{  
  return view('admin.sliders.add');
}


public function store(Request $request)
{
    $data = $request->all();
    $message = 'Something went wrong';

   $newData = new FeaturedImage;

    $poster = " ";
    if($request->hasFile('poster')) 
    {
       $file=$request->poster;
       $file->move(public_path(). '/featured/', time().$file->getClientOriginalName());    
       $poster = time().$file->getClientOriginalName();
       $newData->poster = '/featured/'.$poster;
   }


   $newData->image_title = $data['image_title'];  
   $newData->redirect_url = $data['redirect_url'];  
   $newData->display_priority = $data['display_priority'];  
   $newData->start_date = $data['start_date'];  
   $newData->end_date = $data['end_date'];  
   $newData->page_title = $data['page_title'];  
   $newData->status = $data['status'];

   $newData->save();

   $message = 'Data successfully added';
   return redirect()->back()->with('success',$message);
}

/**
 * Display the specified resource.
 *
 * @param  int  $id
 * @return \Illuminate\Http\Response
 */
public function show($id)
{
    //
}

/**
 * Show the form for editing the specified resource.
 *
 * @param  int  $id
 * @return \Illuminate\Http\Response
 */
public function edit($id)
{
    $slider = FeaturedImage::find($id);
    
    return view('admin.sliders.edit',compact('slider'));
}

/**
 * Update the specified resource in storage.
 *
 * @param  \Illuminate\Http\Request  $request
 * @param  int  $id
 * @return \Illuminate\Http\Response
 */
public function update(Request $request, $id)
{
    $updateData = FeaturedImage::find($id);
    $data = $request->all();
    $message = 'Something went wrong';

    $poster = " ";
    if($request->hasFile('poster'))
    {
       $file=$request->poster;
       $file->move(public_path(). '/featured/', time().$file->getClientOriginalName());    
       $poster = time().$file->getClientOriginalName();
       $updateData->poster = '/featured/'.$poster;
   }
   
   $updateData->image_title = $data['image_title'];
   $updateData->redirect_url = $data['redirect_url'];  
   $updateData->display_priority = $data['display_priority'];  
   $updateData->end_date = $data['end_date'];  
   $updateData->start_date = $data['start_date'];  
   $updateData->page_title = $data['page_title'];  
   $updateData->status = $data['status'];

   $updateData->update();

   $message = 'Data successfully updated';
   return redirect()->back()->with('success',$message);
}

/**
 * Remove the specified resource from storage.
 *
 * @param  int  $id
 * @return \Illuminate\Http\Response
 */
public function destroy($id)
{
    $check = FeaturedImage::where('id',$id)->delete();
    return redirect()->back()->with('success','Data successfully deleted');

}
}
