@extends('layouts.admin')


@section('content')
<h4 class="header-title m-t-0 m-b-30">Settings</h4>
<h4 class="header-title m-t-0 m-b-30">Google</h4>
@if(Session::get('success'))
<div class="alert alert-success"> {{Session::get('success')}} </div>
@endif
@if (count($errors) > 0)
<div class="alert alert-danger">
	<ul>
		@foreach ($errors->all() as $error)
		<li>{{ $error }}</li>
		@endforeach
	</ul>
</div>
@endif

<form method="POST" action="{{url('admin/setting/google')}}" enctype="multipart/form-data">
	@csrf
	
	
	<div class="row input-control">
		<label class="col-sm-3">Google Client ID</label>
		<div class="col-sm-6">			
			<input type="text" name="client_id" id="client_id" value="{{$data['client_id'] ?? ''}}" class="form-control">
		</div>
	</div>
	
	<div class="row input-control">
		<label class="col-sm-3">Google Client Secret</label>
		<div class="col-sm-6">			
			<input type="text" name="client_secret" id="client_secret" value="{{$data['client_secret'] ?? ''}}" class="form-control">
		</div>
	</div>

	<div class="row input-control">
		<label class="col-sm-3">Google Callback URL</label>
		<div class="col-sm-6">			
			<input type="text" name="callback_url" id="callback_url" value="{{$data['callback_url'] ?? ''}}" class="form-control">
		</div>
	</div>



	
	<div class="row form-group">
		<div class="col-sm-6 col-sm-offset-5">
			<button type="submit" name="save" class="btn btn-primary">Save</button> 
		</div>
	</div>

		
</form>


@endsection
