<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
    integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <!-- fonts -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
    integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  <!-- custom CSS -->
  <link rel="stylesheet" href="css/style.css">
  <script src="carousel.js"></script>


  <title>Khoj</title>
</head>

<body>
  <!-- Modal -->
    <!-- Modal -->
    <div class="modal fade5" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle"
    aria-hidden="true">
    <div class="modal-dialog5" role="document">
      <div class="modal-content5">
        <div class="modal-header5">
          <h1 class="modal-title" id="exampleModalLongTitle">Settings
            <button type="button" class="close pr-5" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </h1>
        </div>
        <div class="modal-body5">
          <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 pr-0">
            <h3>Account</h3>
            <div class="tab">
              <button class="tablinks" onclick="openCity(event, 'SignIn')" id="defaultOpen"> <i
                  class="fas fa-sign-in-alt pr-3"></i>Sign In<i class="fas fa-chevron-right"></i></button>
              <button class="tablinks" onclick="openCity(event, 'MyLanguage')"><i class="fas fa-language pr-3"></i>My
                Language<i class="fas fa-chevron-right"></i></button>
              <button class="tablinks" onclick="openCity(event, 'MyProviders')"><i
                  class="fas fa-satellite-dish pr-3"></i>My Providers<i class="fas fa-chevron-right"></i></button>
              <button class="tablinks" onclick="openCity(event, 'MyGenres')"><i class="fas fa-mars-double pr-3"></i>My
                Genres<i class="fas fa-chevron-right"></i></button>
              <button class="tablinks" onclick="openCity(event, 'MyActors')"><i class="fas fa-podcast pr-3"></i>My
                Actors<i class="fas fa-chevron-right"></i></button>
              <button class="tablinks" onclick="openCity(event, 'MyFilmmakers')"><i class="fas fa-tv pr-3"></i>My
                Filmmakers<i class="fas fa-chevron-right"></i></button>
            </div>
            <div id="SignIn" class="tabcontent">

              <img src="images/Khojlogo.png">
              <h2 class="pt-4">Sign in to synchronize your watchlist across all your devices</h2>
              <button type="button" class="btn btn-lg mt-4 conti" data-toggle="modal"
                data-target="#my-modal">Continue</button>

            </div>
            <div id="MyLanguage" class="tabcontent">
              <div class="row  pt-4">
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 ">
                  <h3 class="pt-2 pr-2">Select All &nbsp;
                    <label class="switch mr-3">
                      <input type="checkbox">
                      <span class="slider round"></span>
                    </label>
                  </h3>
                </div>
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 ">
                  <a href="#">
                    <h3 class="pt-2 pr-2 text-right"><i class="fas fa-undo"></i>&nbsp; Clear</h3>
                  </a>

                </div>


              </div>
              <div class="row">
                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 ">
                  <button type="button" class="btn btn-lg mt-4 my_lang">
                    <div class="row">
                      <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4 ">
                        <p class="alpha_bet ">E</p>
                      </div>
                      <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-8 ">
                        <p class="text-left pt-2">English</p>
                      </div>
                    </div>
                  </button>
                </div>

                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 ">
                  <button type="button" class="btn btn-lg mt-4 my_lang">
                    <div class="row">
                      <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4 ">
                        <p class="alpha_bet">हि</p>
                      </div>
                      <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-8 ">
                        <p class="text-left pt-2">Hindi</p>
                      </div>
                    </div>
                  </button>
                </div>

              </div>
              <div class="row">
                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 ">
                  <button type="button" class="btn btn-lg mt-4 my_lang">
                    <div class="row">
                      <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4 ">
                        <p class="alpha_bet">తె</p>
                      </div>
                      <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-8 ">
                        <p class="text-left pt-2">Telugu</p>
                      </div>
                    </div>
                  </button>
                </div>

                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 ">
                  <button type="button" class="btn btn-lg mt-4 my_lang">
                    <div class="row">
                      <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4 ">
                        <p class="alpha_bet">മ</p>
                      </div>
                      <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-8 ">
                        <p class="text-left pt-2">Malayalam</p>
                      </div>
                    </div>
                  </button>
                </div>

              </div>
              <div class="row">
                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 ">
                  <button type="button" class="btn btn-lg mt-4 my_lang">
                    <div class="row">
                      <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4 ">
                        <p class="alpha_bet">த</p>
                      </div>
                      <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-8 ">
                        <p class="text-left pt-2">Tamil</p>
                      </div>
                    </div>
                  </button>
                </div>

                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 ">
                  <button type="button" class="btn btn-lg mt-4 my_lang">
                    <div class="row">
                      <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4 ">
                        <p class="alpha_bet">म</p>
                      </div>
                      <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-8 ">
                        <p class="text-left pt-2">Marathi</p>
                      </div>
                    </div>
                  </button>
                </div>

              </div>

              <div class="row">
                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 ">
                  <button type="button" class="btn btn-lg mt-4 my_lang">
                    <div class="row">
                      <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4 ">
                        <p class="alpha_bet">ગુ</p>
                      </div>
                      <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-8 ">
                        <p class="text-left pt-2">Gujrati</p>
                      </div>
                    </div>
                  </button>
                </div>

                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 ">
                  <button type="button" class="btn btn-lg mt-4 my_lang">
                    <div class="row">
                      <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4 ">
                        <p class="alpha_bet">ਪੰ</p>
                      </div>
                      <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-8 ">
                        <p class="text-left pt-2">Punjabi</p>
                      </div>
                    </div>
                  </button>
                </div>

              </div>
              <div class="row">
                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 ">
                  <button type="button" class="btn btn-lg mt-4 my_lang">
                    <div class="row">
                      <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4 ">
                        <p class="alpha_bet">ଓ</p>
                      </div>
                      <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-8 ">
                        <p class="text-left pt-2">Oddisa</p>
                      </div>
                    </div>
                  </button>
                </div>
                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 ">

                </div>

              </div>
              <div class="row justify-content-center pt-4">
                <button type="button" class="btn-primary  rounded px-5 py-2">Lets Go</button>
              </div>

            </div>
            <div id="MyProviders" class="tabcontent">
              <div class="row  pt-4">
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 ">
                  <h3 class="pt-2 pr-2">Select All &nbsp;
                    <label class="switch mr-3">
                      <input type="checkbox">
                      <span class="slider round"></span>
                    </label>
                  </h3>
                </div>
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 ">
                  <a href="#">
                    <h3 class="pt-2 pr-2 text-right"><i class="fas fa-undo"></i>&nbsp; Clear</h3>
                  </a>

                </div>


              </div>
              <div class="row">
                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 ">
                  <button type="button" class="btn btn-lg mt-4 my_lang">
                    <div class="row">
                      <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4 ">
                        <img src="images/netflix-252.png " width="40" height="40">
                      </div>
                      <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-8 ">
                        <p class="text-left pt-3">Netflix</p>
                      </div>
                    </div>
                  </button>
                </div>

                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 ">
                  <button type="button" class="btn btn-lg mt-4 my_lang">
                    <div class="row">
                      <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4 ">
                        <img src="images/amazon-prime-video-436.png " width="40" height="40">
                      </div>
                      <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-8 ">
                        <p class="text-left pt-3">Prime Video</p>
                      </div>
                    </div>
                  </button>
                </div>

              </div>
              <div class="row">
                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 ">
                  <button type="button" class="btn btn-lg mt-4 my_lang">
                    <div class="row">
                      <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4 ">
                        <img src="images/disney-hotstar-323.png " width="40" height="40">
                      </div>
                      <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-8 ">
                        <p class="text-left pt-3">Disney+ Hotstar</p>
                      </div>
                    </div>
                  </button>
                </div>

                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 ">
                  <button type="button" class="btn btn-lg mt-4 my_lang">
                    <div class="row">
                      <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4 ">
                        <img src="images/zee5-945.png " width="40" height="40">
                      </div>
                      <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-8 ">
                        <p class="text-left pt-3">Zee5</p>
                      </div>
                    </div>
                  </button>
                </div>

              </div>
              <div class="row">
                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 ">
                  <button type="button" class="btn btn-lg mt-4 my_lang">
                    <div class="row">
                      <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4 ">
                        <img src="images/sony-liv-485.png " width="40" height="40">
                      </div>
                      <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-8 ">
                        <p class="text-left pt-3">SonyLIv</p>
                      </div>
                    </div>
                  </button>
                </div>

                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 ">
                  <button type="button" class="btn btn-lg mt-4 my_lang">
                    <div class="row">
                      <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4 ">
                        <img src="images/voot.jpg " width="40" height="40">
                      </div>
                      <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-8 ">
                        <p class="text-left pt-3">Voot</p>
                      </div>
                    </div>
                  </button>
                </div>

              </div>

              <div class="row">
                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 ">
                  <button type="button" class="btn btn-lg mt-4 my_lang">
                    <div class="row">
                      <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4 ">
                        <img src="images/mx-player-34.png " width="40" height="40">
                      </div>
                      <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-8 ">
                        <p class="text-left pt-3">Mx Player</p>
                      </div>
                    </div>
                  </button>
                </div>

                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 ">
                  <button type="button" class="btn btn-lg mt-4 my_lang">
                    <div class="row">
                      <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4 ">
                        <img src="images/jio-cinema-665.png " width="40" height="40">
                      </div>
                      <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-8 ">
                        <p class="text-left pt-3">Jio Cinema</p>
                      </div>
                    </div>
                  </button>
                </div>

              </div>
              <div class="row">
                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 ">
                  <button type="button" class="btn btn-lg mt-4 my_lang">
                    <div class="row">
                      <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4 ">
                        <img src="images/dicovery-538.png " width="40" height="40">
                      </div>
                      <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-8 ">
                        <p class="text-left pt-3">Discovery+</p>
                      </div>
                    </div>
                  </button>
                </div>
                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 ">
                  <button type="button" class="btn btn-lg mt-4 my_lang">
                    <div class="row">
                      <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4 ">
                        <img src="images/sunnxt-393.png " width="40" height="40">
                      </div>
                      <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-8 ">
                        <p class="text-left pt-3">Sun Nxt</p>
                      </div>
                    </div>
                  </button>
                </div>
              </div>
              <div class="row">
                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 ">
                  <button type="button" class="btn btn-lg mt-4 my_lang">
                    <div class="row">
                      <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4 ">
                        <img src="images/dicovery-538.png " width="40" height="40">
                      </div>
                      <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-8 ">
                        <p class="text-left pt-3">Discovery+</p>
                      </div>
                    </div>
                  </button>
                </div>
                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 ">
                  <button type="button" class="btn btn-lg mt-4 my_lang">
                    <div class="row">
                      <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4 ">
                        <img src="images/sunnxt-393.png " width="40" height="40">
                      </div>
                      <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-8 ">
                        <p class="text-left pt-3">Sun Nxt</p>
                      </div>
                    </div>
                  </button>
                </div>
              </div>

              <div class="row">
                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 ">
                  <button type="button" class="btn btn-lg mt-4 my_lang">
                    <div class="row">
                      <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4 ">
                        <img src="images/dicovery-538.png " width="40" height="40">
                      </div>
                      <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-8 ">
                        <p class="text-left pt-3">Discovery+</p>
                      </div>
                    </div>
                  </button>
                </div>
                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 ">
                  <button type="button" class="btn btn-lg mt-4 my_lang">
                    <div class="row">
                      <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4 ">
                        <img src="images/sunnxt-393.png " width="40" height="40">
                      </div>
                      <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-8 ">
                        <p class="text-left pt-3">Sun Nxt</p>
                      </div>
                    </div>
                  </button>
                </div>
              </div>

              <div class="row">
                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 ">
                  <button type="button" class="btn btn-lg mt-4 my_lang">
                    <div class="row">
                      <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4 ">
                        <img src="images/dicovery-538.png " width="40" height="40">
                      </div>
                      <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-8 ">
                        <p class="text-left pt-3">Discovery+</p>
                      </div>
                    </div>
                  </button>
                </div>
                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 ">
                  <button type="button" class="btn btn-lg mt-4 my_lang">
                    <div class="row">
                      <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4 ">
                        <img src="images/sunnxt-393.png " width="40" height="40">
                      </div>
                      <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-8 ">
                        <p class="text-left pt-3">Sun Nxt</p>
                      </div>
                    </div>
                  </button>
                </div>
              </div>

              <div class="row">
                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 ">
                  <button type="button" class="btn btn-lg mt-4 my_lang">
                    <div class="row">
                      <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4 ">
                        <img src="images/dicovery-538.png " width="40" height="40">
                      </div>
                      <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-8 ">
                        <p class="text-left pt-3">Discovery+</p>
                      </div>
                    </div>
                  </button>
                </div>
                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 ">
                  <button type="button" class="btn btn-lg mt-4 my_lang">
                    <div class="row">
                      <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4 ">
                        <img src="images/sunnxt-393.png " width="40" height="40">
                      </div>
                      <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-8 ">
                        <p class="text-left pt-3">Sun Nxt</p>
                      </div>
                    </div>
                  </button>
                </div>
              </div>
              <div class="row justify-content-center pt-4">
                <button type="button" class="btn-primary rounded px-5 py-2">Lets Go</button>
              </div>
            </div>
            <div id="MyGenres" class="tabcontent">
              <div class="row  pt-4">
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 ">
                  <h3 class="pt-2 pr-2">Select All &nbsp;
                    <label class="switch mr-3">
                      <input type="checkbox">
                      <span class="slider round"></span>
                    </label>
                  </h3>
                </div>
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 ">
                  <a href="#">
                    <h3 class="pt-2 pr-2 text-right"><i class="fas fa-undo"></i>&nbsp; Clear</h3>
                  </a>
                </div>
              </div>
              <div class="row pt-3">
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                  <a href="#"><img src="images/adventure-849.png" class="filter_img" style="width: 100% ; "
                      alt="img-responsive" class="img-fluid ">
                    <p class="text-center">Adventure</p>
                  </a>
                </div>

                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                  <a href="#"><img src="images/drama-346.png" class="filter_img" style="width: 100% ; "
                      alt="img-responsive" class="img-fluid ">
                    <p class="text-center">Drama</p>
                  </a>
                </div>

                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                  <a href="#"><img src="images/war-314.png" class="filter_img" style="width: 100% ; "
                      alt="img-responsive" class="img-fluid ">
                    <p class="text-center">War</p>
                  </a>
                </div>



              </div>
              <div class="row pt-3">
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                  <a href="#"><img src="images/horror-527.png" class="filter_img" style="width: 100% ; "
                      alt="img-responsive" class="img-fluid ">
                    <p class="text-center">Horror</p>
                  </a>
                </div>

                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                  <a href="#"> <img src="images/western-341.png" class="filter_img" style="width: 100% ; "
                      alt="img-responsive" class="img-fluid ">
                    <p class="text-center">Westeren</p>
                  </a>
                </div>

                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                  <a href="#"><img src="images/comedy-938.png" class="filter_img" style="width: 100% ; "
                      alt="img-responsive" class="img-fluid ">
                    <p class="text-center">Comedy</p>
                  </a>
                </div>
              </div>
              <div class="row pt-3">
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                  <a href="#"> <img src="images/adventure-849.png" class="filter_img" style="width: 100% ; "
                      alt="img-responsive" class="img-fluid ">
                    <p class="text-center">Adventure</p>
                  </a>
                </div>

                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                  <a href="#"> <img src="images/drama-346.png" class="filter_img" style="width: 100% ; "
                      alt="img-responsive" class="img-fluid ">
                    <p class="text-center">Drama</p>
                  </a>
                </div>

                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                  <a href="#"> <img src="images/war-314.png" class="filter_img" style="width: 100% ; "
                      alt="img-responsive" class="img-fluid ">
                    <p class="text-center">War</p>
                  </a>
                </div>



              </div>
              <div class="row pt-3">
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                  <a href="#"> <img src="images/horror-527.png" class="filter_img" style="width: 100% ; "
                      alt="img-responsive" class="img-fluid ">
                    <p class="text-center">Horror</p>
                  </a>
                </div>

                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                  <a href="#"> <img src="images/western-341.png" class="filter_img" style="width: 100% ; "
                      alt="img-responsive" class="img-fluid ">
                    <p class="text-center">Westeren</p>
                  </a>
                </div>

                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                  <a href="#"> <img src="images/comedy-938.png" class="filter_img" style="width: 100% ; "
                      alt="img-responsive" class="img-fluid ">
                    <p class="text-center">Comedy</p>
                  </a>
                </div>



              </div>
              <div class="row pt-3">
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                  <a href="#"> <img src="images/adventure-849.png" class="filter_img" style="width: 100% ; "
                      alt="img-responsive" class="img-fluid ">
                    <p class="text-center">Adventure</p>
                  </a>
                </div>

                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                  <a href="#"><img src="images/drama-346.png" class="filter_img" style="width: 100% ; "
                      alt="img-responsive" class="img-fluid ">
                    <p class="text-center">Drama</p>
                  </a>
                </div>

                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                  <a href="#"> <img src="images/war-314.png" class="filter_img" style="width: 100% ; "
                      alt="img-responsive" class="img-fluid ">
                    <p class="text-center">War</p>
                  </a>
                </div>



              </div>
              <div class="row pt-3">
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                  <a href="#"> <img src="images/horror-527.png" class="filter_img" style="width: 100% ; "
                      alt="img-responsive" class="img-fluid ">
                    <p class="text-center">Horror</p>
                  </a>
                </div>

                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                  <a href="#"> <img src="images/western-341.png" class="filter_img" style="width: 100% ; "
                      alt="img-responsive" class="img-fluid ">
                    <p class="text-center">Westeren</p>
                  </a>
                </div>

                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                  <a href="#"> <img src="images/comedy-938.png" class="filter_img" style="width: 100% ; "
                      alt="img-responsive" class="img-fluid ">
                    <p class="text-center">Comedy</p>
                  </a>
                </div>

              </div>
              <div class="row justify-content-center pt-4">
                <button type="button" class="btn-primary rounded px-5 py-2">Lets Go</button>
              </div>
            </div>
            <div id="MyActors" class="tabcontent">
              <div class="row  pt-4">
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 ">
                  <h3 class="pt-2 pr-2">Select All &nbsp;
                    <label class="switch mr-3">
                      <input type="checkbox">
                      <span class="slider round"></span>
                    </label>
                  </h3>
                </div>
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 ">
                  <a href="#">
                    <h3 class="pt-2 pr-2 text-right"><i class="fas fa-undo"></i>&nbsp; Clear</h3>
                  </a>

                </div>


              </div>
              <div class="row pt-3">
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                  <a href="#"><img src="images/ranbir.png" class="filter_img" style="width: 100% ; "
                      alt="img-responsive" class="img-fluid ">
                    <h4 class="text-center">Ranbir kapoor</h4>
                  </a>
                </div>

                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                  <a href="#"> <img src="images/alia.png" class="filter_img" style="width: 100% ; " alt="img-responsive"
                      class="img-fluid ">
                    <h4 class="text-center">Alia Bhatt</h4>
                  </a>
                </div>

                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                  <a href="#"> <img src="images/ranveer.png" class="filter_img" style="width: 100% ; "
                      alt="img-responsive" class="img-fluid ">
                    <h4 class="text-center">Ranveer singh</h4>
                  </a>
                </div>

              </div>
              <div class="row pt-3">
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                  <a href="#"> <img src="images/ranbir.png" class="filter_img" style="width: 100% ; "
                      alt="img-responsive" class="img-fluid ">
                    <h4 class="text-center">Ranbir kapoor</h4>
                  </a>
                </div>

                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                  <a href="#"><img src="images/alia.png" class="filter_img" style="width: 100% ; " alt="img-responsive"
                      class="img-fluid ">
                    <h4 class="text-center">Alia Bhatt</h4><a href="#"></a>
                </div>

                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                  <a href="#"><img src="images/ranveer.png" class="filter_img" style="width: 100% ; "
                      alt="img-responsive" class="img-fluid ">
                    <h4 class="text-center">Ranveer singh</h4>
                  </a>
                </div>

              </div>
              <div class="row pt-3">
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                  <a href="#"> <img src="images/ranbir.png" class="filter_img" style="width: 100% ; "
                      alt="img-responsive" class="img-fluid ">
                    <h4 class="text-center">Ranbir kapoor</h4>
                  </a>
                </div>

                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                  <a href="#"> <img src="images/alia.png" class="filter_img" style="width: 100% ; " alt="img-responsive"
                      class="img-fluid ">
                    <h4 class="text-center">Alia Bhatt</h4>
                  </a>
                </div>

                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                  <a href="#"> <img src="images/ranveer.png" class="filter_img" style="width: 100% ; "
                      alt="img-responsive" class="img-fluid ">
                    <h4 class="text-center">Ranveer singh</h4>
                  </a>
                </div>

              </div>
              <div class="row pt-3">
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                  <a href="#"> <img src="images/ranbir.png" class="filter_img" style="width: 100% ; "
                      alt="img-responsive" class="img-fluid ">
                    <h4 class="text-center">Ranbir kapoor</h4>
                  </a>
                </div>

                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                  <a href="#"> <img src="images/alia.png" class="filter_img" style="width: 100% ; " alt="img-responsive"
                      class="img-fluid ">
                    <h4 class="text-center">Alia Bhatt</h4>
                  </a>
                </div>

                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                  <a href="#"> <img src="images/ranveer.png" class="filter_img" style="width: 100% ; "
                      alt="img-responsive" class="img-fluid ">
                    <h4 class="text-center">Ranveer singh</h4>
                  </a>
                </div>

              </div>
              <div class="row justify-content-center pt-4">
                <button type="button" class="btn-primary rounded px-5 py-2">Lets Go</button>
              </div>
            </div>
            <div id="MyFilmmakers" class="tabcontent">
              <div class="row  pt-4">
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 ">
                  <h3 class="pt-2 pr-2">Select All &nbsp;
                    <label class="switch mr-3">
                      <input type="checkbox">
                      <span class="slider round"></span>
                    </label>
                  </h3>
                </div>
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 ">
                  <a href="#">
                    <h3 class="pt-2 pr-2 text-right"><i class="fas fa-undo"></i>&nbsp; Clear</h3>
                  </a>

                </div>


              </div>
              <div class="row pt-3">
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                  <a href="#"><img src="images/ranbir.png" class="filter_img" style="width: 100% ; "
                      alt="img-responsive" class="img-fluid ">
                    <h4 class="text-center">Ranbir kapoor</h4>
                  </a>
                </div>

                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                  <a href="#"> <img src="images/alia.png" class="filter_img" style="width: 100% ; " alt="img-responsive"
                      class="img-fluid ">
                    <h4 class="text-center">Alia Bhatt</h4>
                  </a>
                </div>

                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                  <a href="#"> <img src="images/ranveer.png" class="filter_img" style="width: 100% ; "
                      alt="img-responsive" class="img-fluid ">
                    <h4 class="text-center">Ranveer singh</h4>
                  </a>
                </div>

              </div>
              <div class="row pt-3">
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                  <a href="#"> <img src="images/ranbir.png" class="filter_img" style="width: 100% ; "
                      alt="img-responsive" class="img-fluid ">
                    <h4 class="text-center">Ranbir kapoor</h4>
                  </a>
                </div>

                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                  <a href="#"><img src="images/alia.png" class="filter_img" style="width: 100% ; " alt="img-responsive"
                      class="img-fluid ">
                    <h4 class="text-center">Alia Bhatt</h4><a href="#"></a>
                </div>

                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                  <a href="#"><img src="images/ranveer.png" class="filter_img" style="width: 100% ; "
                      alt="img-responsive" class="img-fluid ">
                    <h4 class="text-center">Ranveer singh</h4>
                  </a>
                </div>

              </div>
              <div class="row pt-3">
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                  <a href="#"> <img src="images/ranbir.png" class="filter_img" style="width: 100% ; "
                      alt="img-responsive" class="img-fluid ">
                    <h4 class="text-center">Ranbir kapoor</h4>
                  </a>
                </div>

                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                  <a href="#"> <img src="images/alia.png" class="filter_img" style="width: 100% ; " alt="img-responsive"
                      class="img-fluid ">
                    <h4 class="text-center">Alia Bhatt</h4>
                  </a>
                </div>

                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                  <a href="#"> <img src="images/ranveer.png" class="filter_img" style="width: 100% ; "
                      alt="img-responsive" class="img-fluid ">
                    <h4 class="text-center">Ranveer singh</h4>
                  </a>
                </div>

              </div>
              <div class="row pt-3">
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                  <a href="#"> <img src="images/ranbir.png" class="filter_img" style="width: 100% ; "
                      alt="img-responsive" class="img-fluid ">
                    <h4 class="text-center">Ranbir kapoor</h4>
                  </a>
                </div>

                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                  <a href="#"> <img src="images/alia.png" class="filter_img" style="width: 100% ; " alt="img-responsive"
                      class="img-fluid ">
                    <h4 class="text-center">Alia Bhatt</h4>
                  </a>
                </div>

                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                  <a href="#"> <img src="images/ranveer.png" class="filter_img" style="width: 100% ; "
                      alt="img-responsive" class="img-fluid ">
                    <h4 class="text-center">Ranveer singh</h4>
                  </a>
                </div>

              </div>
              <div class="row justify-content-center pt-4">
                <button type="button" class="btn-primary rounded px-5 py-2">Lets Go</button>
              </div>
            </div>

          </div>
        </div>
      </div>
    </div>
  </div>
  <section class="top_bar">
    <div class="container">
      <div class="row pt-2">
        <button type="button" class="btn leftpop" data-toggle="modal" data-target="#exampleModal">
          Feedback
        </button>
        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6">
          <div class="row">
            <a class="nav-link " href="#"> <img src="images/googleicon.png" width="20" alt=""></a>
            <a class="nav-link active  Text-white" href="#"><img src="images/apple.png" width="25" class="ml-4"
                alt=""></a>
          </div>
        </div>
        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 pr-1">
          <ul class="list-inline list-unstyled mb-0">
            <li class="list-inline-item">
              <button type="button" class="btn_fasco pt-1" data-toggle="modal" data-target="#exampleModalLong">
                <a class="nav-link" href="#"><i class="fas fa-cog"></i></a>
            </li>
            </button>
            <li class="list-inline-item">
              <div class="container d-flex justify-content-center registermodal"> <button class="btn p-0 login  mr-2"
                  data-toggle="modal" data-target="#my-modal">
                  <i class="fas fa-user fa-2x"></i> </button>
                <div id="my-modal" class="modal fade fix_modal" tabindex="-1" role="dialog" aria-hidden="true">
                  <div class="modal-dialog justify-content-center " role="document">
                    <div class="modal-content border-0 mx-3">
                      <div class="modal-body p-0">
                        <div class="row justify-content-center">
                          <div class="col-auto">
                            <div class="card">
                              <div class="card-header  pb-0 border-0">
                              </div>
                              <div class="card-body pt-0">
                                <div class="row justify-content-center text-center">
                                  <div class="col">
                                    <h2 class="mb-2 pt-1"><b>Welcome Back Login below.</b></h2>
                                  </div>
                                </div>
                                <div class="row justify-content-center my-1">
                                  <div class="col-12">
                                    <p class="text-center">We'll send you welcome emails and never post.</p>
                                  </div>
                                </div>
                                <div class="row justify-content-center pt-1">
                                  <a href="#"> <button type="button" class="btn btn-icon  text-left "><span><img
                                          src="https://img.icons8.com/color/48/000000/google-logo.png"
                                          class="img-fluid mr-1" width="35"></span> </button></a>
                                  <a href="#"> <button type="button" class="btn btn-icon  text-left "><span><img
                                          src="https://i.imgur.com/URmkevm.png" class="img-fluid mr-1"
                                          width="35"></span> </button> </a>
                                  <a href="#"> <button type="button" class="btn btn-icon  text-left "><span><img
                                          src="https://img.icons8.com/ios-filled/50/000000/mac-os.png"
                                          class="img-fluid mr-1" width="35"></span> </button></a>
                                </div>
                                <div class="row ">
                                  <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 pt-4">
                                    <ul class="nav nav-pills mb-1 justify-content-center registry " id="pills-tab"
                                      role="tablist">
                                      <li class="nav-item text-center">
                                        <a class="nav-link active" id="pills-home-tab" data-toggle="pill"
                                          href="#pills-homemy" role="tab" aria-controls="pills-home"
                                          aria-selected="true">Signup</a>
                                      </li>
                                      <li class="nav-item text-center">
                                        <a class="nav-link" id="pills-profile-tab" data-toggle="pill"
                                          href="#pills-profilemy" role="tab" aria-controls="pills-profile"
                                          aria-selected="false">Login</a>
                                      </li>
                                    </ul>
                                    <div class="tab-content signi" id="pills-tabContent">
                                      <div class="tab-pane fade show active" id="pills-homemy" role="tabpanel"
                                        aria-labelledby="pills-home-tab">
                                        <div class="container">
                                          <div class="row justify-content-center">
                                            <div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 col-12 ">
                                              <form>
                                                <div class="form-group">
                                                   
                                                  <div class="searchformfld">
                                                    <input type="text" class="candidateName" id="candidateName" name="candidateName" placeholder=" "/>
                                                    <label for="candidateName">Your name</label>
                                                </div>

                                                <div class="searchformfld formfld">
                                                  <input type="email" class="email"  placeholder=" "/>
                                                  <label for="candidateName">Email</label>
                                              </div>
                                              <div class="searchformfld formfld">
                                                <input type="password" class="password" placeholder=" "/>
                                                <label for="candidateName">Password</label>
                                            </div>
                                           
                                                  
                                                  <input type="checkbox" class="form-check-input" id="exampleCheck1">
                                                  <label class="form-check-label pl-4" for="exampleCheck1">Receive
                                                    welcome emails</label>
                                                </div>
                                              </form>
                                              <div class="row text-center">
                                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12  ">
                                                  <button type="button"
                                                    class="btn btn-primary btn-lg signbtn mt-1">Signup</button>
                                                </div>
                                              </div>


                                              <div class="row">

                                                <p class="text-center pt-1">By creating an account you consent to having
                                                  your data <br>collected to be used for generating recommendations,
                                                  market research,<br> and improving the service
                                                  <br> Learn more: Privacy | Terms of Service
                                                </p>

                                              </div>


                                            </div>
                                          </div>
                                        </div>

                                      </div>
                                      <div class="tab-pane fade" id="pills-profilemy" role="tabpanel"
                                        aria-labelledby="pills-profile-tab">
                                        <div class="row justify-content-center">
                                          <div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 col-12 ">

                                            <form>
                                              <div class="searchformfld formfld">
                                                <input type="email" class="email"  placeholder=" "/>
                                                <label for="candidateName">Email</label>
                                            </div>
                                            <div class="searchformfld formfld">
                                              <input type="password" class="password" placeholder=" "/>
                                              <label for="candidateName">Password</label>
                                          </div>
                                            </form>
                                            <div class="row text-center">
                                              <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12  ">
                                                <button type="button"
                                                  class="btn btn-primary btn-lg signbtn mt-3">Login</button>
                                              </div>
                                            </div>

                                            <div class="row justify-content-center pt-2">

                                              <a href="#">Forgot Password?</a>

                                            </div>

                                          </div>
                                        </div>

                                      </div>

                                    </div>
                                  </div>
                                </div>

                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

            </li>
            <li class="list-inline-item">
              <ul class="nav ">
                <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle bg-white" href="#" id="navbarDropdown" role="button"
                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Theme Option
                  </a>
                  <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="#">Dark</a>
                    <a class="dropdown-item" href="#">White</a>
                  </div>
                </li>
              </ul>
            </li>
          </ul>
        </div>
      </div>
    </div>

    <!-- feedback Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
      aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel"></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body modell">
            <div class="container">
              <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 pb-4 ">
                  <h2 class="input">We Want Your Input!</h2>
                  <form>
                    <div class="form-group">
                      <label for="exampleFormControlTextarea1"></label>
                      <textarea class="form-control " id="exampleFormControlTextarea1"
                        placeholder="Questions, bug reports, missing or wrong data, praise, feature requests — help us improve!"
                        rows="3"></textarea>
                    </div>
                    <div class="form-group">
                      <div class="md-form mb-0">
                        <label for="name" class="">Your name</label>
                        <input type="text" id="name" name="name" class="form-control">
                      </div>

                      <label for="exampleInputEmail1">Email address</label>
                      <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">

                    </div>

                  </form>
                  <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 pl-0 pt-3">


                    <p class="note">Note: Without giving us an email we can't reply to you!</p>
                  </div>
                  <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 text-right pt-3">
                    <button type="button" class="btn btn-secondary btn-lg sub">
                      submit
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- feedback Modal end -->

  </section>

  <section class="nav_bar bg-white">
    <div class="container">
      <nav class="navbar navbar-expand-lg navbar-light bg-white mb-0">
        <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
          <a href="index.html"> <img src="images/khojlogo.png" alt=""></a>
          <button class="navbar-toggler float-right" type="button" data-toggle="collapse"
            data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
            aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
        </div>
        <div class="col-xl-9 col-lg-9 col-md-9 col-sm-12 col-12 ">
          <div class="collapse navbar-collapse" id="navbarSupportedContent">

            <ul class="navbar-nav ml-auto mr-auto">
              <li class="nav-item  px-2 font-weight-bold">

                <a class="nav-link" href="index.html">Home <span class="sr-only">(current)</span></a>
              </li>

              <li class="nav-item  active dropdown font-weight-bold">
                <a class="nav-link dropdown-toggle" href="movies.html"  data-bs-toggle="dropdown">
                  Movies
                </a>
                <div class="dropdown-menu Drop_first" aria-labelledby="navbarDropdown">
                  <div class="container-fluid px-5">
                    <div class="row">
                      <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12 border-right">
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                          <h3 class="text-dark font-weight-bold py-3">Movie of the day</h3>
                          <img src="images/dropimage.jpg" class="img-fluid" width="200" alt="">
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                          <h5 class="text-dark mt-5 pt-5">2017,Adventure comedy,Roman..</h5>
                          <h4 class="text-dark font-weight-bold pt-4">The Big Sick</h4>
                          <h5 class="text-dark  pt-2"> Pakistan-born comedian Kumail Nanjiani and grad student Emily Gardner fall in love but
                            struggle as their cultures clash. When Emily contracts a mysterious illness,</h5>
                         <button class="btn btn-link font-weight-bold mt-4 pl-0"><h4>Watch Now</h4></button>
                         <button class="btn font-weight-bold bg-light mt-4 pl-0"><h4>+ PLaylist</h4></button>
                        </div>

                      </div>
                      <div class="col-xl-2 col-lg-2 col-md-6 col-sm-6 col-12">
                        <h3 class="text-dark font-weight-bold py-3">Movie Genre</h3>
                        <ul class="pl-0">
<li class="list-unstyled"><a href ="#" class="text-dark " >Action</a></li>
<li class="list-unstyled"><a href ="" class="text-dark">Adventure</a></li>
<li class="list-unstyled"><a href ="" class="text-dark">Comedy</a></li>
<li class="list-unstyled"><a href ="" class="text-dark">Drama</a></li>
<li class="list-unstyled"><a href ="" class="text-dark">Sci-Fy</a></li>
<li class="list-unstyled"><a href ="" class="text-dark">Thriller</a></li>
<li class="list-unstyled"><a href ="" class="text-dark">Biography</a></li>
<li class="list-unstyled"><a href ="" class="text-dark">Family</a></li>
<li class="list-unstyled"><a href ="" class="text-dark">Romance</a></li>

                        </ul>
                      </div>
                      <div class="col-xl-2 col-lg-2 col-md-6 col-sm-6 col-12 border-right">
                        <ul class="pt-5 pl-0 mt-3">
                          <li class="list-unstyled"><a href ="#" class="text-dark" >Animation</a></li>
                          <li class="list-unstyled"><a href ="" class="text-dark">adventure</a></li>
                          <li class="list-unstyled"><a href ="" class="text-dark">Comedy</a></li>
                          <li class="list-unstyled"><a href ="" class="text-dark">Drama</a></li>
                          <li class="list-unstyled"><a href ="" class="text-dark">Sci-Fy</a></li>
                          <li class="list-unstyled"><a href ="" class="text-dark">Thriller</a></li>
                         
                          
                                                  </ul>
                      </div>
                      <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12">
                        <h3 class="text-dark font-weight-bold py-3">Movie Genre</h3>
                        <div id="carouselExampleControlsdrop2" class="carousel slide " data-ride="carousel">
                          <div class="carousel-inner">
                            <div class="carousel-item active">
                              <img class="img-fluid" src="images/qqqq.jpg" alt="">
                            </div>
                            <div class="carousel-item">
                              <img class="img-fluid" src="images/qqqq2.jpg" alt="">
                            </div>
                            <div class="carousel-item">
                              <img class="img-fluid" src="images/qqqq3.jpg" alt="">
                            </div>
                          </div>
                          <a class="carousel-control-prev" href="#carouselExampleControlsdrop2" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                          </a>
                          <a class="carousel-control-next" href="#carouselExampleControlsdrop2" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                          </a>
                        </div>
                      </div>
                    </div>
                  </div>

                </div>
              </li>
              <li class="nav-item px-2  dropdown font-weight-bold">
                <a class="nav-link dropdown-toggle"  href="tvseries.html"  data-bs-toggle="dropdown">
                  Tv-series
                </a>
                <div class="dropdown-menu Second_drop" aria-labelledby="navbarDropdown22">
                  <div class="container-fluid px-5">
                    <div class="row">
                      <div class="col-xl-3 col-lg-3 col-md-12 col-sm-12 col-12 border-right">
                        <ul class="pl-0 pt-5 mt-2">
                          <li class="list-unstyled"><a href ="#" class="text-dark " >Trending Tv Shows</a></li>
                          <li class="list-unstyled"><a href ="" class="text-dark">Popular Now</a></li>
                          <li class="list-unstyled"><a href ="" class="text-dark">New This Month</a></li>
                          <li class="list-unstyled"><a href ="" class="text-dark">Khoj Exclusive</a></li>
                          <li class="list-unstyled"><a href ="" class="text-dark">Just For Kids</a></li>
                          <li class="list-unstyled"><a href ="" class="text-dark">Featured Shows</a></li>
                          <li class="list-unstyled"><a href ="" class="text-dark">Tv Shows Genre</a></li>
                          <li class="list-unstyled"><a href ="" class="text-dark">Tv Series:Genre</a></li>
                          <li class="list-unstyled"><a href ="" class="text-dark">Tv Series:Drama</a></li>
                          
                                                  </ul>

                      </div>

                      <div class="col-xl-9 col-lg-9 col-md-12 col-sm-12 col-12">
                        <h3 class="text-dark font-weight-bold py-3">Movie Genre</h3>
                        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                          <ol class="carousel-indicators">
                            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                           
                          </ol>
                          <div class="carousel-inner">
                            <div class="carousel-item active">
                            
                              <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-3">
                                <img class="img-fluid" src="images/qqqq.jpg" alt="">
                                <h4 class="pt-4">Action,Drama,2015-2016</h4>
                                <h4 class="font-weight-bold">Chicago med</h4>
                                </div>

                                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-3">
                                  <img class="img-fluid" src="images/qqqq2.jpg" alt="">
                                  <h4 class="pt-4">Action,Drama,2015-2016</h4>
                                  <h4 class="font-weight-bold">Chicago med</h4>
                                </div>
                                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-3">
                                  <img class="img-fluid" src="images/qqqq3.jpg" alt="">
                                  <h4 class="pt-4">Action,Drama,2015-2016</h4>
                                  <h4 class="font-weight-bold">Chicago med</h4>
                                </div>
                                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-3">
                                  <img class="img-fluid" src="images/qqqq.jpg" alt="">
                                  <h4 class="pt-4">Action,Drama,2015-2016</h4>
                                  <h4 class="font-weight-bold">Chicago med</h4>
                                </div>

                            </div>
                            <div class="carousel-item">
                              <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-3">
                                <img class="img-fluid" src="images/qqqq.jpg" alt="">
                                <h4 class="pt-4">Action,Drama,2015-2016</h4>
                                <h4 class="font-weight-bold">Chicago med</h4>
                                </div>

                                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-3">
                                  <img class="img-fluid" src="images/qqqq2.jpg" alt="">
                                  <h4 class="pt-4">Action,Drama,2015-2016</h4>
                                  <h4 class="font-weight-bold">Chicago med</h4>
                                </div>
                                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-3">
                                  <img class="img-fluid" src="images/qqqq3.jpg" alt="">
                                  <h4 class="pt-4">Action,Drama,2015-2016</h4>
                                  <h4 class="font-weight-bold">Chicago med</h4>
                                </div>
                                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-3">
                                  <img class="img-fluid" src="images/qqqq.jpg" alt="">
                                  <h4 class="pt-4">Action,Drama,2015-2016</h4>
                                  <h4 class="font-weight-bold">Chicago med</h4>
                                </div>
                            </div>
                           
                          </div>
                          <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                          </a>
                          <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                          </a>
                        </div>
                      </div>
                    </div>
                  </div>

                </div>
              </li>


              <li class="nav-item px-3 font-weight-bold">
                <a class="nav-link" href="free.html">Free</a>
              </li>
              <li class="nav-item px-3 font-weight-bold">
                <a class="nav-link" href="blog.html">Blog</a>
              </li>
            </ul>
            <form autocomplete="off" action="/action_page.php" style="max-width:450px;">
              <div class="input-icons autocomplete">
                <i class="fas fa-search icon">
                </i>
                <input id="myInput" class="pl-3" type="text" name="myCountry" placeholder="Search">
              </div>
            </form>
          </div>
        </div>
      </nav>

    </div>
  </section>

  <section class="first1">
    <div class="container">
      <h3 class="pt-4 pb-4">Home &nbsp;> Movie</h3>
    </div>
  </section>

  <section class="main">
    <div class="container">
      <div class="row star mb-4 mt-4">
        <div class="container ">
          <h2 class="pt-3 pb-3 font-weight-bold">Movie</h2>
        </div>
      </div>
      <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12 collap pt-3 pb-5 ">
<div class="row pb-4">
  <div class="col-8">
    <a href= "# " class="text-white">Select all  <label class="switch mx-3">
      <input type="checkbox">
      <span class="slider round"></span>
    </label></a>
  </div>
  <div class="col-4">
    <a href="#"> <h3 class="pt-2 pr-2 text-right"><i class="fas fa-undo"></i>&nbsp; Clear</h3> </a>
  </div>
</div>

        <button type="button" class="collapsible font-weight-bold "aria-expanded="false">Sources <i
            class="fas fa-angle-down float-right"></i>
          <div class="text-white count"> 1 </div>
        </button>
       
        <div class="content source">
          <div class="container">
            <div class="row source">
              <button type="button" class="btn btn-light btn-lg btn-block text-left">Hotstar </button>
            </div>
          </div>
          <div class="container">
            <div class="row source">
              <button type="button" class="btn btn-light btn-lg btn-block text-left">Eros Now</button>
            </div>
          </div>
          <div class="container">
            <div class="row source">
              <button type="button" class="btn btn-light btn-lg btn-block text-left">Amazon Prime</button>
            </div>
          </div>
          <div class="container">
            <div class="row source">
              <button type="button" class="btn btn-light btn-lg btn-block text-left">Netflix</button>
            </div>
          </div>
          <div class="container">
            <div class="row source">
              <button type="button" class="btn btn-light btn-lg btn-block text-left">Jio prime</button>
            </div>
          </div>
          <div class="container">
            <div class="row source">
              <button type="button" class="btn btn-light btn-lg btn-block text-left">Sony LIV</button>
            </div>
          </div>
          <div class="container">
            <div class="row source">
              <button type="button" class="btn btn-light btn-lg btn-block text-left">itunes</button>
            </div>
          </div>
          <div class="container">
            <div class="row source">
              <button type="button" class="btn btn-light btn-lg btn-block text-left">spull</button>
            </div>
          </div>
          <div class="container">
            <div class="row source">
              <button type="button" class="btn btn-light btn-lg btn-block text-left">viki</button>
            </div>
          </div>
          <div class="container">
            <div class="row source">
              <button type="button" class="btn btn-light btn-lg btn-block text-left">mx-player</button>
            </div>
          </div>
          <div class="container">
            <div class="row source">
              <button type="button" class="btn btn-light btn-lg btn-block text-left">Voot</button>
            </div>
          </div>
          <div class="container">
            <div class="row source">
              <button type="button" class="btn btn-light btn-lg btn-block text-left">Alt-balaji</button>
            </div>
          </div>


        </div>

        <button type="button" class="collapsible font-weight-bold mt-1" aria-expanded="false">Type <i
            class="fas fa-angle-down float-right"></i> </button>
        <div class="content">
          <div class="container">
            <div class="row source">
              <button type="button" class="btn btn-light btn-lg btn-block text-left">Romantic</button>
            </div>
          </div>
          <div class="container">
            <div class="row source">
              <button type="button" class="btn btn-light btn-lg btn-block text-left">Love</button>
            </div>
          </div>
          <div class="container">
            <div class="row source">
              <button type="button" class="btn btn-light btn-lg btn-block text-left">Action</button>
            </div>
          </div>
          <div class="container">
            <div class="row source">
              <button type="button" class="btn btn-light btn-lg btn-block text-left">Romantic</button>
            </div>
          </div>
          <div class="container">
            <div class="row source">
              <button type="button" class="btn btn-light btn-lg btn-block text-left">Love</button>
            </div>
          </div>
          <div class="container">
            <div class="row source">
              <button type="button" class="btn btn-light btn-lg btn-block text-left">Action</button>
            </div>
          </div>
          <div class="container">
            <div class="row source">
              <button type="button" class="btn btn-light btn-lg btn-block text-left">Romantic</button>
            </div>
          </div>
          <div class="container">
            <div class="row source">
              <button type="button" class="btn btn-light btn-lg btn-block text-left">Love</button>
            </div>
          </div>
          <div class="container">
            <div class="row source">
              <button type="button" class="btn btn-light btn-lg btn-block text-left">Action</button>
            </div>
          </div>


        </div>
        <button type="button" class="collapsible font-weight-bold mt-1" aria-expanded="false">Genre <i
            class="fas fa-angle-down float-right"></i></button>
        <div class="content">
          <div class="container">
            <div class="row source">
              <button type="button" class="btn btn-light btn-lg btn-block text-left">Horror</button>
            </div>
          </div>
          <div class="container">
            <div class="row source">
              <button type="button" class="btn btn-light btn-lg btn-block text-left">Action</button>
            </div>
          </div>
          <div class="container">
            <div class="row source">
              <button type="button" class="btn btn-light btn-lg btn-block text-left">Comedy</button>
            </div>
          </div>
          <div class="container">
            <div class="row source">
              <button type="button" class="btn btn-light btn-lg btn-block text-left">Romance</button>
            </div>
          </div>
          <div class="container">
            <div class="row source">
              <button type="button" class="btn btn-light btn-lg btn-block text-left">Adventure</button>
            </div>
          </div>
          <div class="container">
            <div class="row source">
              <button type="button" class="btn btn-light btn-lg btn-block text-left">Love</button>
            </div>
          </div>
          <div class="container">
            <div class="row source">
              <button type="button" class="btn btn-light btn-lg btn-block text-left">Romance</button>
            </div>
          </div>
          <div class="container">
            <div class="row source">
              <button type="button" class="btn btn-light btn-lg btn-block text-left">Adventure</button>
            </div>
          </div>
          <div class="container">
            <div class="row source">
              <button type="button" class="btn btn-light btn-lg btn-block text-left">Love</button>
            </div>
          </div>
        </div>
        <button type="button" class="collapsible font-weight-bold mt-1" aria-expanded="false">Language <i
            class="fas fa-angle-down float-right"></i></button>
        <div class="content">
          <div class="container">
            <div class="row source">
              <button type="button" class="btn btn-light btn-lg btn-block text-left">English</button>
            </div>
          </div>
          <div class="container">
            <div class="row source">
              <button type="button" class="btn btn-light btn-lg btn-block text-left">Hindi</button>
            </div>
          </div>
          <div class="container">
            <div class="row source">
              <button type="button" class="btn btn-light btn-lg btn-block text-left">Punjabi</button>
            </div>
          </div>
          <div class="container">
            <div class="row source">
              <button type="button" class="btn btn-light btn-lg btn-block text-left">Tammil</button>
            </div>
          </div>
          <div class="container">
            <div class="row source">
              <button type="button" class="btn btn-light btn-lg btn-block text-left">Marathi</button>
            </div>
          </div>
          <div class="container">
            <div class="row source">
              <button type="button" class="btn btn-light btn-lg btn-block text-left">Telugu</button>
            </div>
          </div>
          <div class="container">
            <div class="row source">
              <button type="button" class="btn btn-light btn-lg btn-block text-left">Malyalam</button>
            </div>
          </div>
        </div>
        <button type="button" class="collapsible font-weight-bold mt-1" aria-expanded="false">Streaming Type <i
            class="fas fa-angle-down float-right"></i></button>
        <div class="content">
          <div class="container">
            <div class="row source">
              <button type="button" class="btn btn-light btn-lg btn-block text-left">Netflix</button>
            </div>
          </div>
          <div class="container">
            <div class="row source">
              <button type="button" class="btn btn-light btn-lg btn-block text-left">Prime Video</button>
            </div>
          </div>
          <div class="container">
            <div class="row source">
              <button type="button" class="btn btn-light btn-lg btn-block text-left">Amazon</button>
            </div>
          </div>
          <div class="container">
            <div class="row source">
              <button type="button" class="btn btn-light btn-lg btn-block text-left">jio Prime</button>
            </div>
          </div>
          <div class="container">
            <div class="row source">
              <button type="button" class="btn btn-light btn-lg btn-block text-left">Voot</button>
            </div>
          </div>
        </div>
        <button type="button" class="collapsible font-weight-bold mt-1" aria-expanded="false">Device Type <i
            class="fas fa-angle-down float-right"></i></button>
        <div class="content">
          <div class="container">
            <div class="row source">
              <button type="button" class="btn btn-light btn-lg btn-block text-left"></button>
            </div>
          </div>
          <div class="container">
            <div class="row source">
              <button type="button" class="btn btn-light btn-lg btn-block text-left">Netflix</button>
            </div>
          </div>
          <div class="container">
            <div class="row source">
              <button type="button" class="btn btn-light btn-lg btn-block text-left">Netflix</button>
            </div>
          </div>
          <div class="container">
            <div class="row source">
              <button type="button" class="btn btn-light btn-lg btn-block text-left">Netflix</button>
            </div>
          </div>
        </div>
        <button type="button" class="collapsible font-weight-bold mt-1" aria-expanded="false">Content <i
            class="fas fa-angle-down float-right"></i></button>
        <div class="content">
          <div class="container">
            <div class="row source">
              <button type="button" class="btn btn-light btn-lg btn-block text-left">Netflix</button>
            </div>
          </div>
          <div class="container">
            <div class="row source">
              <button type="button" class="btn btn-light btn-lg btn-block text-left">Netflix</button>
            </div>
          </div>
          <div class="container">
            <div class="row source">
              <button type="button" class="btn btn-light btn-lg btn-block text-left">Netflix</button>
            </div>
          </div>
          <div class="container">
            <div class="row source">
              <button type="button" class="btn btn-light btn-lg btn-block text-left">Netflix</button>
            </div>
          </div>

        </div>
      </div>

      <div class="col-xl-8 col-lg-8 col-md-12 col-sm-12 col-12 pb-5 ">
        <section class="trailer2 ">
          <div class="container  pt-4 pb-5">

            <div class="container">
              <div class="row ">
                <div class="col-xl-8 col-lg-8 col-md-12 col-sm-12 col-12">


                </div>
                <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12">
                  <a class="carousel-control-prev " href="#carouselExampleControls20" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                  </a>
                  <a class="carousel-control-next " href="#carouselExampleControls20" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                  </a>
                </div>
              </div>
            </div>
            <div class="container">
              <div class="row pt-4 mt-4 popu">
                <div id="carouselExampleControls20" class="carousel slide" data-ride="carousel" data-interval="false">
                  <div class="carousel-inner">
                    <div class="carousel-item active">
                      <div class="row ">
                        <a href="movies2.html">
                          <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12  ksiwc hover5">
                            <a href="movies2.html"> <img src="images/img1.png" style="width: 100% ; " alt="..."></a>
                            <div class="position_effect">
                              <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                                  class="fas fa-check"></i></button>
                              <button type="button" class="btn btn-light css-ksiwc2 seentext">Seen it</button>
                            </div>


                            <div class="position_effect2">
                              <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                                  class="fas fa-plus"></i></button>
                              <button type="button" class="btn btn-light css-ksiwc2 seentext">+ Want to see</button>
                            </div>
                            <div class="position_effect3">
                              <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                                  class="fas fa-star"></i></button>
                              <button type="button" class="btn btn-light css-ksiwc2 seentext"><i
                                  class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i
                                  class="fas fa-star"></i><i class="fas fa-star"></i></button>
                            </div>
                            <h4 class="pt-3 text-white">2017,Hindi ,Action</h4>
                            <p class="font-weight-bold text-white">Gully Boy</p>
                            <a href="movies2.html">
                              <div class="overlay5">
                                <h1 class="pt-3 text font-weight-bold">Gully Boy</h1>
                                <h1 class="pt-3 text2 font-italic ">Khoj says</h1>
                                <img src="images/check.png" class="text3" alt="...">
                                <h1 class="pt-3 text4 ">Age 8+</h1><img src="images/rating.png" class="text5" alt="...">
                                <h1 class="pt-3 text6 ">Maidaan is an upcoming Indian Hindi-language biographical sports
                                </h1>
                                <h1 class="pt-3 text7 ">Full Expert views</h1>
                                <img src="images/play.png" class="text8" alt="...">
                                <h1 class="pt-3 text9 ">Video Reviews</h1>
                              </div>
                            </a>
                          </div>
                        </a>

                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-6 ksiwc none hover5">
                          <a href="movies2.html"> <img src="images/img2.png" style="width: 100%; " alt="..."></a>
                          <div class="position_effect">
                            <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                                class="fas fa-check"></i></button>
                            <button type="button" class="btn btn-light css-ksiwc2 seentext">Seen it</button>
                          </div>


                          <div class="position_effect2">
                            <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                                class="fas fa-plus"></i></button>
                            <button type="button" class="btn btn-light css-ksiwc2 seentext">+ Want to see</button>
                          </div>
                          <div class="position_effect3">
                            <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                                class="fas fa-star"></i></button>
                            <button type="button" class="btn btn-light css-ksiwc2 seentext"><i
                                class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i
                                class="fas fa-star"></i><i class="fas fa-star"></i></button>
                          </div>
                          <h4 class="pt-3 text-white">2017,Hindi ,Action</h4>
                          <p class="font-weight-bold text-white">Gully Boy</p>
                          <a href="movies2.html">
                            <div class="overlay5">
                              <h1 class="pt-3 text font-weight-bold">Gully Boy</h1>
                              <h1 class="pt-3 text2 font-italic ">Khoj says</h1>
                              <img src="images/check.png" class="text3" alt="...">
                              <h1 class="pt-3 text4 ">Age 8+</h1><img src="images/rating.png" class="text5" alt="...">
                              <h1 class="pt-3 text6 ">Maidaan is an upcoming Indian Hindi-language biographical sports
                              </h1>
                              <h1 class="pt-3 text7 ">Full Expert views</h1>
                              <img src="images/play.png" class="text8" alt="...">
                              <h1 class="pt-3 text9 ">Video Reviews</h1>
                            </div>
                          </a>
                        </div>
                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4 none dnone ksiwc hover5">
                          <a href="movies2.html"> <img src="images/img3.png" style="width: 100%; " alt="..."></a>
                          <div class="position_effect">
                            <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                                class="fas fa-check"></i></button>
                            <button type="button" class="btn btn-light css-ksiwc2 seentext">Seen it</button>
                          </div>


                          <div class="position_effect2">
                            <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                                class="fas fa-plus"></i></button>
                            <button type="button" class="btn btn-light css-ksiwc2 seentext">+ Want to see</button>
                          </div>
                          <div class="position_effect3">
                            <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                                class="fas fa-star"></i></button>
                            <button type="button" class="btn btn-light css-ksiwc2 seentext"><i
                                class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i
                                class="fas fa-star"></i><i class="fas fa-star"></i></button>
                          </div>
                          <h4 class="pt-3 text-white">2017,Hindi ,Action</h4>
                          <p class="font-weight-bold text-white">Gully Boy</p>
                          <a href="movies2.html">
                            <div class="overlay5">
                              <h1 class="pt-3 text font-weight-bold">Gully Boy</h1>
                              <h1 class="pt-3 text2 font-italic ">Khoj says</h1>
                              <img src="images/check.png" class="text3" alt="...">
                              <h1 class="pt-3 text4 ">Age 8+</h1><img src="images/rating.png" class="text5" alt="...">
                              <h1 class="pt-3 text6 ">Maidaan is an upcoming Indian Hindi-language biographical sports
                              </h1>
                              <h1 class="pt-3 text7 ">Full Expert views</h1>
                              <img src="images/play.png" class="text8" alt="...">
                              <h1 class="pt-3 text9 ">Video Reviews</h1>
                            </div>
                          </a>
                        </div>

                      </div>
                    </div>
                    <div class="carousel-item">
                      <div class="row ">
                        <a href="movies2.html">
                          <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12 none dnone ksiwc hover5">
                            <a href="movies2.html"> <img src="images/img1.png" style="width: 100% ; " alt="..."></a>
                            <div class="position_effect">
                              <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                                  class="fas fa-check"></i></button>
                              <button type="button" class="btn btn-light css-ksiwc2 seentext">Seen it</button>
                            </div>


                            <div class="position_effect2">
                              <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                                  class="fas fa-plus"></i></button>
                              <button type="button" class="btn btn-light css-ksiwc2 seentext">+ Want to see</button>
                            </div>
                            <div class="position_effect3">
                              <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                                  class="fas fa-star"></i></button>
                              <button type="button" class="btn btn-light css-ksiwc2 seentext"><i
                                  class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i
                                  class="fas fa-star"></i><i class="fas fa-star"></i></button>
                            </div>
                            <h4 class="pt-3 text-white">2017,Hindi ,Action</h4>
                            <p class="font-weight-bold text-white">Gully Boy</p>
                            <a href="movies2.html">
                              <div class="overlay5">
                                <h1 class="pt-3 text font-weight-bold">Gully Boy</h1>
                                <h1 class="pt-3 text2 font-italic ">Khoj says</h1>
                                <img src="images/check.png" class="text3" alt="...">
                                <h1 class="pt-3 text4 ">Age 8+</h1><img src="images/rating.png" class="text5" alt="...">
                                <h1 class="pt-3 text6 ">Maidaan is an upcoming Indian Hindi-language biographical sports
                                </h1>
                                <h1 class="pt-3 text7 ">Full Expert views</h1>
                                <img src="images/play.png" class="text8" alt="...">
                                <h1 class="pt-3 text9 ">Video Reviews</h1>
                              </div>
                            </a>
                          </div>
                        </a>

                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-6 none ksiwc hover5">
                          <a href="movies2.html"> <img src="images/img2.png" style="width: 100%; " alt="..."></a>
                          <div class="position_effect">
                            <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                                class="fas fa-check"></i></button>
                            <button type="button" class="btn btn-light css-ksiwc2 seentext">Seen it</button>
                          </div>


                          <div class="position_effect2">
                            <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                                class="fas fa-plus"></i></button>
                            <button type="button" class="btn btn-light css-ksiwc2 seentext">+ Want to see</button>
                          </div>
                          <div class="position_effect3">
                            <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                                class="fas fa-star"></i></button>
                            <button type="button" class="btn btn-light css-ksiwc2 seentext"><i
                                class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i
                                class="fas fa-star"></i><i class="fas fa-star"></i></button>
                          </div>
                          <h4 class="pt-3 text-white">2017,Hindi ,Action</h4>
                          <p class="font-weight-bold text-white">Gully Boy</p>
                          <a href="movies2.html">
                            <div class="overlay5">
                              <h1 class="pt-3 text font-weight-bold">Gully Boy</h1>
                              <h1 class="pt-3 text2 font-italic ">Khoj says</h1>
                              <img src="images/check.png" class="text3" alt="...">
                              <h1 class="pt-3 text4 ">Age 8+</h1><img src="images/rating.png" class="text5" alt="...">
                              <h1 class="pt-3 text6 ">Maidaan is an upcoming Indian Hindi-language biographical sports
                              </h1>
                              <h1 class="pt-3 text7 ">Full Expert views</h1>
                              <img src="images/play.png" class="text8" alt="...">
                              <h1 class="pt-3 text9 ">Video Reviews</h1>
                            </div>
                          </a>
                        </div>
                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12  ksiwc hover5">
                          <a href="movies2.html"> <img src="images/img5.png" style="width: 100%; " alt="..."></a>
                          <div class="position_effect">
                            <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                                class="fas fa-check"></i></button>
                            <button type="button" class="btn btn-light css-ksiwc2 seentext">Seen it</button>
                          </div>


                          <div class="position_effect2">
                            <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                                class="fas fa-plus"></i></button>
                            <button type="button" class="btn btn-light css-ksiwc2 seentext">+ Want to see</button>
                          </div>
                          <div class="position_effect3">
                            <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                                class="fas fa-star"></i></button>
                            <button type="button" class="btn btn-light css-ksiwc2 seentext"><i
                                class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i
                                class="fas fa-star"></i><i class="fas fa-star"></i></button>
                          </div>
                          <h4 class="pt-3 text-white">2017,Hindi ,Action</h4>
                          <p class="font-weight-bold text-white">Gully Boy</p>
                          <a href="movies2.html">
                            <div class="overlay5">
                              <h1 class="pt-3 text font-weight-bold">Gully Boy</h1>
                              <h1 class="pt-3 text2 font-italic ">Khoj says</h1>
                              <img src="images/check.png" class="text3" alt="...">
                              <h1 class="pt-3 text4 ">Age 8+</h1><img src="images/rating.png" class="text5" alt="...">
                              <h1 class="pt-3 text6 ">Maidaan is an upcoming Indian Hindi-language biographical sports
                              </h1>
                              <h1 class="pt-3 text7 ">Full Expert views</h1>
                              <img src="images/play.png" class="text8" alt="...">
                              <h1 class="pt-3 text9 ">Video Reviews</h1>
                            </div>
                          </a>
                        </div>

                      </div>
                    </div>
                  </div>

                </div>
              </div>
            </div>
          </div>

        </section>


        <section class="people2 ">
          <div class="container pt-5 pb-5">
            <div class="container">
              <div class="row ">
                <div class="col-xl-8 col-lg-8 col-md-12 col-sm-12 col-12">

                </div>
                <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12">
                  <a class="carousel-control-prev " href="#carouselExampleControls22" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                  </a>
                  <a class="carousel-control-next " href="#carouselExampleControls22" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                  </a>
                </div>
                </div>
            </div>
            <div class="container">
              <div class="row pt-4 mt-4 popu">
                <div id="carouselExampleControls22" class="carousel slide" data-ride="carousel" data-interval="false">
                  <div class="carousel-inner">
                    <div class="carousel-item active">
                      <div class="row ">
                        <a href="movies2.html">
                          <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12  ksiwc hover5">
                            <a href="movies2.html"> <img src="images/img1.png" style="width: 100% ; " alt="..."></a>
                            <div class="position_effect">
                              <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                                  class="fas fa-check"></i></button>
                              <button type="button" class="btn btn-light css-ksiwc2 seentext">Seen it</button>
                            </div>


                            <div class="position_effect2">
                              <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                                  class="fas fa-plus"></i></button>
                              <button type="button" class="btn btn-light css-ksiwc2 seentext">+ Want to see</button>
                            </div>
                            <div class="position_effect3">
                              <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                                  class="fas fa-star"></i></button>
                              <button type="button" class="btn btn-light css-ksiwc2 seentext"><i
                                  class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i
                                  class="fas fa-star"></i><i class="fas fa-star"></i></button>
                            </div>
                            <h4 class="pt-3 text-white">2017,Hindi ,Action</h4>
                            <p class="font-weight-bold text-white">Gully Boy</p>
                            <a href="movies2.html">
                              <div class="overlay5">
                                <h1 class="pt-3 text font-weight-bold">Gully Boy</h1>
                                <h1 class="pt-3 text2 font-italic ">Khoj says</h1>
                                <img src="images/check.png" class="text3" alt="...">
                                <h1 class="pt-3 text4 ">Age 8+</h1><img src="images/rating.png" class="text5" alt="...">
                                <h1 class="pt-3 text6 ">Maidaan is an upcoming Indian Hindi-language biographical sports
                                </h1>
                                <h1 class="pt-3 text7 ">Full Expert views</h1>
                                <img src="images/play.png" class="text8" alt="...">
                                <h1 class="pt-3 text9 ">Video Reviews</h1>
                              </div>
                            </a>
                          </div>
                        </a>

                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-6 none ksiwc hover5">
                          <a href="movies2.html"> <img src="images/img2.png" style="width: 100%; " alt="..."></a>
                          <div class="position_effect">
                            <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                                class="fas fa-check"></i></button>
                            <button type="button" class="btn btn-light css-ksiwc2 seentext">Seen it</button>
                          </div>


                          <div class="position_effect2">
                            <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                                class="fas fa-plus"></i></button>
                            <button type="button" class="btn btn-light css-ksiwc2 seentext">+ Want to see</button>
                          </div>
                          <div class="position_effect3">
                            <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                                class="fas fa-star"></i></button>
                            <button type="button" class="btn btn-light css-ksiwc2 seentext"><i
                                class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i
                                class="fas fa-star"></i><i class="fas fa-star"></i></button>
                          </div>
                          <h4 class="pt-3 text-white">2017,Hindi ,Action</h4>
                          <p class="font-weight-bold text-white">Gully Boy</p>
                          <a href="movies2.html">
                            <div class="overlay5">
                              <h1 class="pt-3 text font-weight-bold">Gully Boy</h1>
                              <h1 class="pt-3 text2 font-italic ">Khoj says</h1>
                              <img src="images/check.png" class="text3" alt="...">
                              <h1 class="pt-3 text4 ">Age 8+</h1><img src="images/rating.png" class="text5" alt="...">
                              <h1 class="pt-3 text6 ">Maidaan is an upcoming Indian Hindi-language biographical sports
                              </h1>
                              <h1 class="pt-3 text7 ">Full Expert views</h1>
                              <img src="images/play.png" class="text8" alt="...">
                              <h1 class="pt-3 text9 ">Video Reviews</h1>
                            </div>
                          </a>
                        </div>
                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4 none dnone ksiwc hover5">
                          <a href="movies2.html"> <img src="images/img3.png" style="width: 100%; " alt="..."></a>
                          <div class="position_effect">
                            <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                                class="fas fa-check"></i></button>
                            <button type="button" class="btn btn-light css-ksiwc2 seentext">Seen it</button>
                          </div>


                          <div class="position_effect2">
                            <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                                class="fas fa-plus"></i></button>
                            <button type="button" class="btn btn-light css-ksiwc2 seentext">+ Want to see</button>
                          </div>
                          <div class="position_effect3">
                            <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                                class="fas fa-star"></i></button>
                            <button type="button" class="btn btn-light css-ksiwc2 seentext"><i
                                class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i
                                class="fas fa-star"></i><i class="fas fa-star"></i></button>
                          </div>
                          <h4 class="pt-3 text-white">2017,Hindi ,Action</h4>
                          <p class="font-weight-bold text-white">Gully Boy</p>
                          <a href="movies2.html">
                            <div class="overlay5">
                              <h1 class="pt-3 text font-weight-bold">Gully Boy</h1>
                              <h1 class="pt-3 text2 font-italic ">Khoj says</h1>
                              <img src="images/check.png" class="text3" alt="...">
                              <h1 class="pt-3 text4 ">Age 8+</h1><img src="images/rating.png" class="text5" alt="...">
                              <h1 class="pt-3 text6 ">Maidaan is an upcoming Indian Hindi-language biographical sports
                              </h1>
                              <h1 class="pt-3 text7 ">Full Expert views</h1>
                              <img src="images/play.png" class="text8" alt="...">
                              <h1 class="pt-3 text9 ">Video Reviews</h1>
                            </div>
                          </a>
                        </div>

                      </div>
                    </div>
                    <div class="carousel-item">
                      <div class="row ">
                        <a href="movies2.html">
                          <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12 none dnone ksiwc hover5">
                            <a href="movies2.html"> <img src="images/img1.png" style="width: 100% ; " alt="..."></a>
                            <div class="position_effect">
                              <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                                  class="fas fa-check"></i></button>
                              <button type="button" class="btn btn-light css-ksiwc2 seentext">Seen it</button>
                            </div>


                            <div class="position_effect2">
                              <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                                  class="fas fa-plus"></i></button>
                              <button type="button" class="btn btn-light css-ksiwc2 seentext">+ Want to see</button>
                            </div>
                            <div class="position_effect3">
                              <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                                  class="fas fa-star"></i></button>
                              <button type="button" class="btn btn-light css-ksiwc2 seentext"><i
                                  class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i
                                  class="fas fa-star"></i><i class="fas fa-star"></i></button>
                            </div>
                            <h4 class="pt-3 text-white">2017,Hindi ,Action</h4>
                            <p class="font-weight-bold text-white">Gully Boy</p>
                            <a href="movies2.html">
                              <div class="overlay5">
                                <h1 class="pt-3 text font-weight-bold">Gully Boy</h1>
                                <h1 class="pt-3 text2 font-italic ">Khoj says</h1>
                                <img src="images/check.png" class="text3" alt="...">
                                <h1 class="pt-3 text4 ">Age 8+</h1><img src="images/rating.png" class="text5" alt="...">
                                <h1 class="pt-3 text6 ">Maidaan is an upcoming Indian Hindi-language biographical sports
                                </h1>
                                <h1 class="pt-3 text7 ">Full Expert views</h1>
                                <img src="images/play.png" class="text8" alt="...">
                                <h1 class="pt-3 text9 ">Video Reviews</h1>
                              </div>
                            </a>
                          </div>
                        </a>

                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-6 none ksiwc hover5">
                          <a href="movies2.html"> <img src="images/img2.png" style="width: 100%; " alt="..."></a>
                          <div class="position_effect">
                            <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                                class="fas fa-check"></i></button>
                            <button type="button" class="btn btn-light css-ksiwc2 seentext">Seen it</button>
                          </div>


                          <div class="position_effect2">
                            <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                                class="fas fa-plus"></i></button>
                            <button type="button" class="btn btn-light css-ksiwc2 seentext">+ Want to see</button>
                          </div>
                          <div class="position_effect3">
                            <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                                class="fas fa-star"></i></button>
                            <button type="button" class="btn btn-light css-ksiwc2 seentext"><i
                                class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i
                                class="fas fa-star"></i><i class="fas fa-star"></i></button>
                          </div>
                          <h4 class="pt-3 text-white">2017,Hindi ,Action</h4>
                          <p class="font-weight-bold text-white">Gully Boy</p>
                          <a href="movies2.html">
                            <div class="overlay5">
                              <h1 class="pt-3 text font-weight-bold">Gully Boy</h1>
                              <h1 class="pt-3 text2 font-italic ">Khoj says</h1>
                              <img src="images/check.png" class="text3" alt="...">
                              <h1 class="pt-3 text4 ">Age 8+</h1><img src="images/rating.png" class="text5" alt="...">
                              <h1 class="pt-3 text6 ">Maidaan is an upcoming Indian Hindi-language biographical sports
                              </h1>
                              <h1 class="pt-3 text7 ">Full Expert views</h1>
                              <img src="images/play.png" class="text8" alt="...">
                              <h1 class="pt-3 text9 ">Video Reviews</h1>
                            </div>
                          </a>
                        </div>
                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12  ksiwc hover5">
                          <a href="movies2.html"> <img src="images/img5.png" style="width: 100%; " alt="..."></a>
                          <div class="position_effect">
                            <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                                class="fas fa-check"></i></button>
                            <button type="button" class="btn btn-light css-ksiwc2 seentext">Seen it</button>
                          </div>


                          <div class="position_effect2">
                            <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                                class="fas fa-plus"></i></button>
                            <button type="button" class="btn btn-light css-ksiwc2 seentext">+ Want to see</button>
                          </div>
                          <div class="position_effect3">
                            <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                                class="fas fa-star"></i></button>
                            <button type="button" class="btn btn-light css-ksiwc2 seentext"><i
                                class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i
                                class="fas fa-star"></i><i class="fas fa-star"></i></button>
                          </div>
                          <h4 class="pt-3 text-white">2017,Hindi ,Action</h4>
                          <p class="font-weight-bold text-white">Gully Boy</p>
                          <a href="movies2.html">
                            <div class="overlay5">
                              <h1 class="pt-3 text font-weight-bold">Gully Boy</h1>
                              <h1 class="pt-3 text2 font-italic ">Khoj says</h1>
                              <img src="images/check.png" class="text3" alt="...">
                              <h1 class="pt-3 text4 ">Age 8+</h1><img src="images/rating.png" class="text5" alt="...">
                              <h1 class="pt-3 text6 ">Maidaan is an upcoming Indian Hindi-language biographical sports
                              </h1>
                              <h1 class="pt-3 text7 ">Full Expert views</h1>
                              <img src="images/play.png" class="text8" alt="...">
                              <h1 class="pt-3 text9 ">Video Reviews</h1>
                            </div>
                          </a>
                        </div>

                      </div>
                    </div>
                  </div>

                </div>
              </div>
            </div>

          </div>

        </section>

        <section class="ott2 ">
          <div class="container pt-5 pb-5">
            <div class="container">
              <div class="row ">
                <div class="col-xl-8 col-lg-8 col-md-12 col-sm-12 col-12">

                </div>
                <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12">
                  <a class="carousel-control-prev " href="#carouselExampleControls23" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                  </a>
                  <a class="carousel-control-next " href="#carouselExampleControls23" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                  </a>
                </div>



              </div>
            </div>
            <div class="container">
              <div class="row pt-4 mt-4 popu">
                <div id="carouselExampleControls23" class="carousel slide" data-ride="carousel" data-interval="false">
                  <div class="carousel-inner">
                    <div class="carousel-item active">
                      <div class="row ">
                        <a href="movies2.html">
                          <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12  ksiwc hover5">
                            <a href="movies2.html"> <img src="images/img1.png" style="width: 100% ; " alt="..."></a>
                            <div class="position_effect">
                              <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                                  class="fas fa-check"></i></button>
                              <button type="button" class="btn btn-light css-ksiwc2 seentext">Seen it</button>
                            </div>


                            <div class="position_effect2">
                              <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                                  class="fas fa-plus"></i></button>
                              <button type="button" class="btn btn-light css-ksiwc2 seentext">+ Want to see</button>
                            </div>
                            <div class="position_effect3">
                              <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                                  class="fas fa-star"></i></button>
                              <button type="button" class="btn btn-light css-ksiwc2 seentext"><i
                                  class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i
                                  class="fas fa-star"></i><i class="fas fa-star"></i></button>
                            </div>
                            <h4 class="pt-3 text-white">2017,Hindi ,Action</h4>
                            <p class="font-weight-bold text-white">Gully Boy</p>
                            <a href="movies2.html">
                              <div class="overlay5">
                                <h1 class="pt-3 text font-weight-bold">Gully Boy</h1>
                                <h1 class="pt-3 text2 font-italic ">Khoj says</h1>
                                <img src="images/check.png" class="text3" alt="...">
                                <h1 class="pt-3 text4 ">Age 8+</h1><img src="images/rating.png" class="text5" alt="...">
                                <h1 class="pt-3 text6 ">Maidaan is an upcoming Indian Hindi-language biographical sports
                                </h1>
                                <h1 class="pt-3 text7 ">Full Expert views</h1>
                                <img src="images/play.png" class="text8" alt="...">
                                <h1 class="pt-3 text9 ">Video Reviews</h1>
                              </div>
                            </a>
                          </div>
                        </a>

                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-6 none ksiwc hover5">
                          <a href="movies2.html"> <img src="images/img2.png" style="width: 100%; " alt="..."></a>
                          <div class="position_effect">
                            <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                                class="fas fa-check"></i></button>
                            <button type="button" class="btn btn-light css-ksiwc2 seentext">Seen it</button>
                          </div>


                          <div class="position_effect2">
                            <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                                class="fas fa-plus"></i></button>
                            <button type="button" class="btn btn-light css-ksiwc2 seentext">+ Want to see</button>
                          </div>
                          <div class="position_effect3">
                            <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                                class="fas fa-star"></i></button>
                            <button type="button" class="btn btn-light css-ksiwc2 seentext"><i
                                class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i
                                class="fas fa-star"></i><i class="fas fa-star"></i></button>
                          </div>
                          <h4 class="pt-3 text-white">2017,Hindi ,Action</h4>
                          <p class="font-weight-bold text-white">Gully Boy</p>
                          <a href="movies2.html">
                            <div class="overlay5">
                              <h1 class="pt-3 text font-weight-bold">Gully Boy</h1>
                              <h1 class="pt-3 text2 font-italic ">Khoj says</h1>
                              <img src="images/check.png" class="text3" alt="...">
                              <h1 class="pt-3 text4 ">Age 8+</h1><img src="images/rating.png" class="text5" alt="...">
                              <h1 class="pt-3 text6 ">Maidaan is an upcoming Indian Hindi-language biographical sports
                              </h1>
                              <h1 class="pt-3 text7 ">Full Expert views</h1>
                              <img src="images/play.png" class="text8" alt="...">
                              <h1 class="pt-3 text9 ">Video Reviews</h1>
                            </div>
                          </a>
                        </div>
                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4 none dnone ksiwc hover5">
                          <a href="movies2.html"> <img src="images/img3.png" style="width: 100%; " alt="..."></a>
                          <div class="position_effect">
                            <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                                class="fas fa-check"></i></button>
                            <button type="button" class="btn btn-light css-ksiwc2 seentext">Seen it</button>
                          </div>


                          <div class="position_effect2">
                            <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                                class="fas fa-plus"></i></button>
                            <button type="button" class="btn btn-light css-ksiwc2 seentext">+ Want to see</button>
                          </div>
                          <div class="position_effect3">
                            <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                                class="fas fa-star"></i></button>
                            <button type="button" class="btn btn-light css-ksiwc2 seentext"><i
                                class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i
                                class="fas fa-star"></i><i class="fas fa-star"></i></button>
                          </div>
                          <h4 class="pt-3 text-white">2017,Hindi ,Action</h4>
                          <p class="font-weight-bold text-white">Gully Boy</p>
                          <a href="movies2.html">
                            <div class="overlay5">
                              <h1 class="pt-3 text font-weight-bold">Gully Boy</h1>
                              <h1 class="pt-3 text2 font-italic ">Khoj says</h1>
                              <img src="images/check.png" class="text3" alt="...">
                              <h1 class="pt-3 text4 ">Age 8+</h1><img src="images/rating.png" class="text5" alt="...">
                              <h1 class="pt-3 text6 ">Maidaan is an upcoming Indian Hindi-language biographical sports
                              </h1>
                              <h1 class="pt-3 text7 ">Full Expert views</h1>
                              <img src="images/play.png" class="text8" alt="...">
                              <h1 class="pt-3 text9 ">Video Reviews</h1>
                            </div>
                          </a>
                        </div>

                      </div>
                    </div>
                    <div class="carousel-item">
                      <div class="row ">
                        <a href="movies2.html">
                          <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12 none dnone ksiwc hover5">
                            <a href="movies2.html"> <img src="images/img1.png" style="width: 100% ; " alt="..."></a>
                            <div class="position_effect">
                              <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                                  class="fas fa-check"></i></button>
                              <button type="button" class="btn btn-light css-ksiwc2 seentext">Seen it</button>
                            </div>


                            <div class="position_effect2">
                              <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                                  class="fas fa-plus"></i></button>
                              <button type="button" class="btn btn-light css-ksiwc2 seentext">+ Want to see</button>
                            </div>
                            <div class="position_effect3">
                              <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                                  class="fas fa-star"></i></button>
                              <button type="button" class="btn btn-light css-ksiwc2 seentext"><i
                                  class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i
                                  class="fas fa-star"></i><i class="fas fa-star"></i></button>
                            </div>
                            <h4 class="pt-3 text-white">2017,Hindi ,Action</h4>
                            <p class="font-weight-bold text-white">Gully Boy</p>
                            <a href="movies2.html">
                              <div class="overlay5">
                                <h1 class="pt-3 text font-weight-bold">Gully Boy</h1>
                                <h1 class="pt-3 text2 font-italic ">Khoj says</h1>
                                <img src="images/check.png" class="text3" alt="...">
                                <h1 class="pt-3 text4 ">Age 8+</h1><img src="images/rating.png" class="text5" alt="...">
                                <h1 class="pt-3 text6 ">Maidaan is an upcoming Indian Hindi-language biographical sports
                                </h1>
                                <h1 class="pt-3 text7 ">Full Expert views</h1>
                                <img src="images/play.png" class="text8" alt="...">
                                <h1 class="pt-3 text9 ">Video Reviews</h1>
                              </div>
                            </a>
                          </div>
                        </a>

                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-6 none ksiwc hover5">
                          <a href="movies2.html"> <img src="images/img2.png" style="width: 100%; " alt="..."></a>
                          <div class="position_effect">
                            <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                                class="fas fa-check"></i></button>
                            <button type="button" class="btn btn-light css-ksiwc2 seentext">Seen it</button>
                          </div>


                          <div class="position_effect2">
                            <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                                class="fas fa-plus"></i></button>
                            <button type="button" class="btn btn-light css-ksiwc2 seentext">+ Want to see</button>
                          </div>
                          <div class="position_effect3">
                            <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                                class="fas fa-star"></i></button>
                            <button type="button" class="btn btn-light css-ksiwc2 seentext"><i
                                class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i
                                class="fas fa-star"></i><i class="fas fa-star"></i></button>
                          </div>
                          <h4 class="pt-3 text-white">2017,Hindi ,Action</h4>
                          <p class="font-weight-bold text-white">Gully Boy</p>
                          <a href="movies2.html">
                            <div class="overlay5">
                              <h1 class="pt-3 text font-weight-bold">Gully Boy</h1>
                              <h1 class="pt-3 text2 font-italic ">Khoj says</h1>
                              <img src="images/check.png" class="text3" alt="...">
                              <h1 class="pt-3 text4 ">Age 8+</h1><img src="images/rating.png" class="text5" alt="...">
                              <h1 class="pt-3 text6 ">Maidaan is an upcoming Indian Hindi-language biographical sports
                              </h1>
                              <h1 class="pt-3 text7 ">Full Expert views</h1>
                              <img src="images/play.png" class="text8" alt="...">
                              <h1 class="pt-3 text9 ">Video Reviews</h1>
                            </div>
                          </a>
                        </div>
                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12  ksiwc hover5">
                          <a href="movies2.html"> <img src="images/img5.png" style="width: 100%; " alt="..."></a>
                          <div class="position_effect">
                            <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                                class="fas fa-check"></i></button>
                            <button type="button" class="btn btn-light css-ksiwc2 seentext">Seen it</button>
                          </div>


                          <div class="position_effect2">
                            <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                                class="fas fa-plus"></i></button>
                            <button type="button" class="btn btn-light css-ksiwc2 seentext">+ Want to see</button>
                          </div>
                          <div class="position_effect3">
                            <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                                class="fas fa-star"></i></button>
                            <button type="button" class="btn btn-light css-ksiwc2 seentext"><i
                                class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i
                                class="fas fa-star"></i><i class="fas fa-star"></i></button>
                          </div>
                          <h4 class="pt-3 text-white">2017,Hindi ,Action</h4>
                          <p class="font-weight-bold text-white">Gully Boy</p>
                          <a href="movies2.html">
                            <div class="overlay5">
                              <h1 class="pt-3 text font-weight-bold">Gully Boy</h1>
                              <h1 class="pt-3 text2 font-italic ">Khoj says</h1>
                              <img src="images/check.png" class="text3" alt="...">
                              <h1 class="pt-3 text4 ">Age 8+</h1><img src="images/rating.png" class="text5" alt="...">
                              <h1 class="pt-3 text6 ">Maidaan is an upcoming Indian Hindi-language biographical sports
                              </h1>
                              <h1 class="pt-3 text7 ">Full Expert views</h1>
                              <img src="images/play.png" class="text8" alt="...">
                              <h1 class="pt-3 text9 ">Video Reviews</h1>
                            </div>
                          </a>
                        </div>

                      </div>
                    </div>
                  </div>

                </div>
              </div>
            </div>

          </div>

        </section>

        <section class="ott3 ">
          <div class="container pt-5 pb-5">
            <div class="container">
              <div class="row ">
                <div class="col-xl-8 col-lg-8 col-md-12 col-sm-12 col-12">

                </div>
                <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12">
                  <a class="carousel-control-prev " href="#carouselExampleControls24" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                  </a>
                  <a class="carousel-control-next " href="#carouselExampleControls24" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                  </a>
                </div>
              </div>
            </div>
            <div class="container">
              <div class="row pt-4 mt-4 popu">
                <div id="carouselExampleControls24" class="carousel slide" data-ride="carousel" data-interval="false">
                  <div class="carousel-inner">
                    <div class="carousel-item active">
                      <div class="row ">
                        <a href="movies2.html">
                          <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12  ksiwc hover5">
                            <a href="movies2.html"> <img src="images/img1.png" style="width: 100% ; " alt="..."></a>
                            <div class="position_effect">
                              <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                                  class="fas fa-check"></i></button>
                              <button type="button" class="btn btn-light css-ksiwc2 seentext">Seen it</button>
                            </div>


                            <div class="position_effect2">
                              <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                                  class="fas fa-plus"></i></button>
                              <button type="button" class="btn btn-light css-ksiwc2 seentext">+ Want to see</button>
                            </div>
                            <div class="position_effect3">
                              <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                                  class="fas fa-star"></i></button>
                              <button type="button" class="btn btn-light css-ksiwc2 seentext"><i
                                  class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i
                                  class="fas fa-star"></i><i class="fas fa-star"></i></button>
                            </div>
                            <h4 class="pt-3 text-white">2017,Hindi ,Action</h4>
                            <p class="font-weight-bold text-white">Gully Boy</p>
                            <a href="movies2.html">
                              <div class="overlay5">
                                <h1 class="pt-3 text font-weight-bold">Gully Boy</h1>
                                <h1 class="pt-3 text2 font-italic ">Khoj says</h1>
                                <img src="images/check.png" class="text3" alt="...">
                                <h1 class="pt-3 text4 ">Age 8+</h1><img src="images/rating.png" class="text5" alt="...">
                                <h1 class="pt-3 text6 ">Maidaan is an upcoming Indian Hindi-language biographical sports
                                </h1>
                                <h1 class="pt-3 text7 ">Full Expert views</h1>
                                <img src="images/play.png" class="text8" alt="...">
                                <h1 class="pt-3 text9 ">Video Reviews</h1>
                              </div>
                            </a>
                          </div>
                        </a>

                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-6 none ksiwc hover5">
                          <a href="movies2.html"> <img src="images/img2.png" style="width: 100%; " alt="..."></a>
                          <div class="position_effect">
                            <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                                class="fas fa-check"></i></button>
                            <button type="button" class="btn btn-light css-ksiwc2 seentext">Seen it</button>
                          </div>


                          <div class="position_effect2">
                            <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                                class="fas fa-plus"></i></button>
                            <button type="button" class="btn btn-light css-ksiwc2 seentext">+ Want to see</button>
                          </div>
                          <div class="position_effect3">
                            <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                                class="fas fa-star"></i></button>
                            <button type="button" class="btn btn-light css-ksiwc2 seentext"><i
                                class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i
                                class="fas fa-star"></i><i class="fas fa-star"></i></button>
                          </div>
                          <h4 class="pt-3 text-white">2017,Hindi ,Action</h4>
                          <p class="font-weight-bold text-white">Gully Boy</p>
                          <a href="movies2.html">
                            <div class="overlay5">
                              <h1 class="pt-3 text font-weight-bold">Gully Boy</h1>
                              <h1 class="pt-3 text2 font-italic ">Khoj says</h1>
                              <img src="images/check.png" class="text3" alt="...">
                              <h1 class="pt-3 text4 ">Age 8+</h1><img src="images/rating.png" class="text5" alt="...">
                              <h1 class="pt-3 text6 ">Maidaan is an upcoming Indian Hindi-language biographical sports
                              </h1>
                              <h1 class="pt-3 text7 ">Full Expert views</h1>
                              <img src="images/play.png" class="text8" alt="...">
                              <h1 class="pt-3 text9 ">Video Reviews</h1>
                            </div>
                          </a>
                        </div>
                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4 none dnone ksiwc hover5">
                          <a href="movies2.html"> <img src="images/img3.png" style="width: 100%; " alt="..."></a>
                          <div class="position_effect">
                            <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                                class="fas fa-check"></i></button>
                            <button type="button" class="btn btn-light css-ksiwc2 seentext">Seen it</button>
                          </div>


                          <div class="position_effect2">
                            <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                                class="fas fa-plus"></i></button>
                            <button type="button" class="btn btn-light css-ksiwc2 seentext">+ Want to see</button>
                          </div>
                          <div class="position_effect3">
                            <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                                class="fas fa-star"></i></button>
                            <button type="button" class="btn btn-light css-ksiwc2 seentext"><i
                                class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i
                                class="fas fa-star"></i><i class="fas fa-star"></i></button>
                          </div>
                          <h4 class="pt-3 text-white">2017,Hindi ,Action</h4>
                          <p class="font-weight-bold text-white">Gully Boy</p>
                          <a href="movies2.html">
                            <div class="overlay5">
                              <h1 class="pt-3 text font-weight-bold">Gully Boy</h1>
                              <h1 class="pt-3 text2 font-italic ">Khoj says</h1>
                              <img src="images/check.png" class="text3" alt="...">
                              <h1 class="pt-3 text4 ">Age 8+</h1><img src="images/rating.png" class="text5" alt="...">
                              <h1 class="pt-3 text6 ">Maidaan is an upcoming Indian Hindi-language biographical sports
                              </h1>
                              <h1 class="pt-3 text7 ">Full Expert views</h1>
                              <img src="images/play.png" class="text8" alt="...">
                              <h1 class="pt-3 text9 ">Video Reviews</h1>
                            </div>
                          </a>
                        </div>

                      </div>
                    </div>
                    <div class="carousel-item">
                      <div class="row ">
                        <a href="movies2.html">
                          <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12  none dnone ksiwc hover5">
                            <a href="movies2.html"> <img src="images/img1.png" style="width: 100% ; " alt="..."></a>
                            <div class="position_effect">
                              <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                                  class="fas fa-check"></i></button>
                              <button type="button" class="btn btn-light css-ksiwc2 seentext">Seen it</button>
                            </div>


                            <div class="position_effect2">
                              <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                                  class="fas fa-plus"></i></button>
                              <button type="button" class="btn btn-light css-ksiwc2 seentext">+ Want to see</button>
                            </div>
                            <div class="position_effect3">
                              <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                                  class="fas fa-star"></i></button>
                              <button type="button" class="btn btn-light css-ksiwc2 seentext"><i
                                  class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i
                                  class="fas fa-star"></i><i class="fas fa-star"></i></button>
                            </div>
                            <h4 class="pt-3 text-white">2017,Hindi ,Action</h4>
                            <p class="font-weight-bold text-white">Gully Boy</p>
                            <a href="movies2.html">
                              <div class="overlay5">
                                <h1 class="pt-3 text font-weight-bold">Gully Boy</h1>
                                <h1 class="pt-3 text2 font-italic ">Khoj says</h1>
                                <img src="images/check.png" class="text3" alt="...">
                                <h1 class="pt-3 text4 ">Age 8+</h1><img src="images/rating.png" class="text5" alt="...">
                                <h1 class="pt-3 text6 ">Maidaan is an upcoming Indian Hindi-language biographical sports
                                </h1>
                                <h1 class="pt-3 text7 ">Full Expert views</h1>
                                <img src="images/play.png" class="text8" alt="...">
                                <h1 class="pt-3 text9 ">Video Reviews</h1>
                              </div>
                            </a>
                          </div>
                        </a>

                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-6 none ksiwc hover5">
                          <a href="movies2.html"> <img src="images/img2.png" style="width: 100%; " alt="..."></a>
                          <div class="position_effect">
                            <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                                class="fas fa-check"></i></button>
                            <button type="button" class="btn btn-light css-ksiwc2 seentext">Seen it</button>
                          </div>


                          <div class="position_effect2">
                            <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                                class="fas fa-plus"></i></button>
                            <button type="button" class="btn btn-light css-ksiwc2 seentext">+ Want to see</button>
                          </div>
                          <div class="position_effect3">
                            <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                                class="fas fa-star"></i></button>
                            <button type="button" class="btn btn-light css-ksiwc2 seentext"><i
                                class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i
                                class="fas fa-star"></i><i class="fas fa-star"></i></button>
                          </div>
                          <h4 class="pt-3 text-white">2017,Hindi ,Action</h4>
                          <p class="font-weight-bold text-white">Gully Boy</p>
                          <a href="movies2.html">
                            <div class="overlay5">
                              <h1 class="pt-3 text font-weight-bold">Gully Boy</h1>
                              <h1 class="pt-3 text2 font-italic ">Khoj says</h1>
                              <img src="images/check.png" class="text3" alt="...">
                              <h1 class="pt-3 text4 ">Age 8+</h1><img src="images/rating.png" class="text5" alt="...">
                              <h1 class="pt-3 text6 ">Maidaan is an upcoming Indian Hindi-language biographical sports
                              </h1>
                              <h1 class="pt-3 text7 ">Full Expert views</h1>
                              <img src="images/play.png" class="text8" alt="...">
                              <h1 class="pt-3 text9 ">Video Reviews</h1>
                            </div>
                          </a>
                        </div>
                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12  ksiwc hover5">
                          <a href="movies2.html"> <img src="images/img5.png" style="width: 100%; " alt="..."></a>
                          <div class="position_effect">
                            <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                                class="fas fa-check"></i></button>
                            <button type="button" class="btn btn-light css-ksiwc2 seentext">Seen it</button>
                          </div>


                          <div class="position_effect2">
                            <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                                class="fas fa-plus"></i></button>
                            <button type="button" class="btn btn-light css-ksiwc2 seentext">+ Want to see</button>
                          </div>
                          <div class="position_effect3">
                            <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                                class="fas fa-star"></i></button>
                            <button type="button" class="btn btn-light css-ksiwc2 seentext"><i
                                class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i
                                class="fas fa-star"></i><i class="fas fa-star"></i></button>
                          </div>
                          <h4 class="pt-3 text-white">2017,Hindi ,Action</h4>
                          <p class="font-weight-bold text-white">Gully Boy</p>
                          <a href="movies2.html">
                            <div class="overlay5">
                              <h1 class="pt-3 text font-weight-bold">Gully Boy</h1>
                              <h1 class="pt-3 text2 font-italic ">Khoj says</h1>
                              <img src="images/check.png" class="text3" alt="...">
                              <h1 class="pt-3 text4 ">Age 8+</h1><img src="images/rating.png" class="text5" alt="...">
                              <h1 class="pt-3 text6 ">Maidaan is an upcoming Indian Hindi-language biographical sports
                              </h1>
                              <h1 class="pt-3 text7 ">Full Expert views</h1>
                              <img src="images/play.png" class="text8" alt="...">
                              <h1 class="pt-3 text9 ">Video Reviews</h1>
                            </div>
                          </a>
                        </div>

                      </div>
                    </div>
                  </div>

                </div>
              </div>
            </div>

          </div>

        </section>

      </div>

    </div>
  </section>
  <footer class="footer ">
    <div class="container mt-5 pb-5 pt-5">
      <div class="row pt-5 first">
        <div class="container">
          <div class="col-xl-2 col-lg-2 col-md-12 col-sm-12 col-12 pb-5 ">
            <img src="images/khojlogo.png" alt="...">

          </div>
          <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12">
            &nbsp;
          </div>
          <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
            <div class="row">
              <a class="nav-link " href="#"> <i class="fab fa-facebook-f"></i> &nbsp; facebook</a>
              <a class="nav-link active  Text-white" href="#"><i class="fab fa-twitter"></i> &nbsp;
                Twitter</a>
              <a class="nav-link active  Text-white" href="#"><i class="fab fa-google-plus-g"></i> &nbsp;
                Google+</a>
              <a class="nav-link active  Text-white" href="#"><i class="fab fa-vimeo-v"></i> &nbsp; Vimeo</a>
              <a class="nav-link active  Text-white" href="#"><i class="fas fa-rss"></i> &nbsp; RSS</a>
            </div>
          </div>
        </div>
      </div>
      <div class="row pt-5">
        <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6 col-6">
          <h2 class="font-weight-bold pt-5">Movie Categories</h2>
          <div class="row pt-5">
            <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
              <ul class="pl-0 lists">
                <li> <a class="nav-link " href="#"> Action </a></li>
                <li><a class="nav-link " href="#"> Adventure </a></li>
                <li><a class="nav-link " href="#"> Animation </a></li>
                <li><a class="nav-link " href="#"> Comedy </a></li>
                <li><a class="nav-link " href="#"> Crime </a></li>
            </div>
            <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
              <ul class="pl-0 lists">
                <li> <a class="nav-link " href="#">Drama </a></li>
                <li><a class="nav-link " href="#"> Fantacy </a></li>
                <li><a class="nav-link " href="#"> Horror</a></li>
                <li><a class="nav-link " href="#"> Mystrey </a></li>
                <li><a class="nav-link " href="#"> Romance </a></li>
              </ul>
            </div>
          </div>
        </div>
        <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6 col-6">
          <h2 class="font-weight-bold pt-5">TV Series</h2>
          <div class="row pt-5">
            <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
              <ul class="pl-0 lists">
                <li><a class="nav-link " href="#">Valentine Day</a></li>
                <li><a class="nav-link " href="#"> Underrated Comedies</a></li>
                <li><a class="nav-link " href="#">Scary TV Series </a></li>
                <li><a class="nav-link " href="#"> Best 2018 Documentaries </a></li>
                <li><a class="nav-link " href="#"> Classic Shows </a></li>
              </ul>
            </div>
            <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
              <ul class="pl-0 lists">
                <li><a class="nav-link " href="#"> Big TV Premieres </a></li>
                <li><a class="nav-link " href="#"> Reality TV Shows </a></li>
                <li><a class="nav-link " href="#"> Original Shows</a></li>
                <li><a class="nav-link " href="#"> Suprise of the Year Shows </a></li>
              </ul>
            </div>
          </div>
        </div>
        <div class="col-xl-3 col-lg-3 col-md-12 col-sm-12 col-12 ml-auto">

          <div class="container">
            <div class="row  ">
              <h2 class="font-weight-bold  pt-5">Support</h2>
            </div>
            <div class="row pt-5 pl-0">
              <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 pl-0 ">
                <ul class="pl-0 lists">
                  <li> <a class="nav-link " href="#"> My Account</a></li>
                  <li><a class="nav-link " href="#"> FAQ</a></li>
                  <li><a class="nav-link " href="#">Watch on TV</a></li>
                  <li><a class="nav-link " href="#">Help Center</a></li>
                  <li><a class="nav-link " href="#"> Crime </a></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="container-fluid">
      <div class="container">
        <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
          <p class="text-white copy pt-5"> © 2021 KhojApp. All Rights Reserved.</p>
        </div>
        <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
          <p class="text-white copy text-right pt-5"> Privacy Policy</p>
        </div>
      </div>
    </div>
  </footer>





  <!-- Optional JavaScript -->
  <!-- jQuery first, then Popper.js, then Bootstrap JS -->

  <script>
    var coll = document.getElementsByClassName("collapsible");
    var i;

    for (i = 0; i < coll.length; i++) {
      coll[i].addEventListener("click", function () {
        this.classList.toggle("active2");
        var content = this.nextElementSibling;
        if (content.style.display === "block") {
          content.style.display = "none";
        } else {
          content.style.display = "block";
        }
      });
    }

 // When the user clicks on <div>, open the popup
  function myFunction() {
    var popup = document.getElementById("myPopup");
    popup.classList.toggle("show");
  }



  function openCity(evt, cityName) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " active";
}



function autocomplete(inp, arr) {
  /*the autocomplete function takes two arguments,
  the text field element and an array of possible autocompleted values:*/
  var currentFocus;
  /*execute a function when someone writes in the text field:*/
  inp.addEventListener("input", function(e) {
      var a, b, i, val = this.value;
      /*close any already open lists of autocompleted values*/
      closeAllLists();
      if (!val) { return false;}
      currentFocus = -1;
      /*create a DIV element that will contain the items (values):*/
      a = document.createElement("DIV");
      a.setAttribute("id", this.id + "autocomplete-list");
      a.setAttribute("class", "autocomplete-items");
      /*append the DIV element as a child of the autocomplete container:*/
      this.parentNode.appendChild(a);
      /*for each item in the array...*/
      for (i = 0; i < arr.length; i++) {
        /*check if the item starts with the same letters as the text field value:*/
        if (arr[i].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
          /*create a DIV element for each matching element:*/
          b = document.createElement("DIV");
          /*make the matching letters bold:*/
          b.innerHTML = "<strong>" + arr[i].substr(0, val.length) + "</strong>";
          b.innerHTML += arr[i].substr(val.length);
          /*insert a input field that will hold the current array item's value:*/
          b.innerHTML += "<input type='hidden' value='" + arr[i] + "'>";
          /*execute a function when someone clicks on the item value (DIV element):*/
          b.addEventListener("click", function(e) {
              /*insert the value for the autocomplete text field:*/
              inp.value = this.getElementsByTagName("input")[0].value;
              /*close the list of autocompleted values,
              (or any other open lists of autocompleted values:*/
              closeAllLists();
          });
          a.appendChild(b);
        }
      }
  });
  /*execute a function presses a key on the keyboard:*/
  inp.addEventListener("keydown", function(e) {
      var x = document.getElementById(this.id + "autocomplete-list");
      if (x) x = x.getElementsByTagName("div");
      if (e.keyCode == 40) {
        /*If the arrow DOWN key is pressed,
        increase the currentFocus variable:*/
        currentFocus++;
        /*and and make the current item more visible:*/
        addActive(x);
      } else if (e.keyCode == 38) { //up
        /*If the arrow UP key is pressed,
        decrease the currentFocus variable:*/
        currentFocus--;
        /*and and make the current item more visible:*/
        addActive(x);
      } else if (e.keyCode == 13) {
        /*If the ENTER key is pressed, prevent the form from being submitted,*/
        e.preventDefault();
        if (currentFocus > -1) {
          /*and simulate a click on the "active" item:*/
          if (x) x[currentFocus].click();
        }
      }
  });
  function addActive(x) {
    /*a function to classify an item as "active":*/
    if (!x) return false;
    /*start by removing the "active" class on all items:*/
    removeActive(x);
    if (currentFocus >= x.length) currentFocus = 0;
    if (currentFocus < 0) currentFocus = (x.length - 1);
    /*add class "autocomplete-active":*/
    x[currentFocus].classList.add("autocomplete-active");
  }
  function removeActive(x) {
    /*a function to remove the "active" class from all autocomplete items:*/
    for (var i = 0; i < x.length; i++) {
      x[i].classList.remove("autocomplete-active");
    }
  }
  function closeAllLists(elmnt) {
    /*close all autocomplete lists in the document,
    except the one passed as an argument:*/
    var x = document.getElementsByClassName("autocomplete-items");
    for (var i = 0; i < x.length; i++) {
      if (elmnt != x[i] && elmnt != inp) {
        x[i].parentNode.removeChild(x[i]);
      }
    }
  }
  /*execute a function when someone clicks in the document:*/
  document.addEventListener("click", function (e) {
      closeAllLists(e.target);
  });
}

/*An array containing all the country names in the world:*/
var countries = ["Afghanistan","Albania","Algeria","Andorra","Angola","Anguilla","Antigua & Barbuda","Argentina","Armenia","Aruba","Australia","Austria","Azerbaijan","Bahamas","Bahrain","Bangladesh","Barbados","Belarus","Belgium","Belize","Benin","Bermuda","Bhutan","Bolivia","Bosnia & Herzegovina","Botswana","Brazil","British Virgin Islands","Brunei","Bulgaria","Burkina Faso","Burundi","Cambodia","Cameroon","Canada","Cape Verde","Cayman Islands","Central Arfrican Republic","Chad","Chile","China","Colombia","Congo","Cook Islands","Costa Rica","Cote D Ivoire","Croatia","Cuba","Curacao","Cyprus","Czech Republic","Denmark","Djibouti","Dominica","Dominican Republic","Ecuador","Egypt","El Salvador","Equatorial Guinea","Eritrea","Estonia","Ethiopia","Falkland Islands","Faroe Islands","Fiji","Finland","France","French Polynesia","French West Indies","Gabon","Gambia","Georgia","Germany","Ghana","Gibraltar","Greece","Greenland","Grenada","Guam","Guatemala","Guernsey","Guinea","Guinea Bissau","Guyana","Haiti","Honduras","Hong Kong","Hungary","Iceland","India","Indonesia","Iran","Iraq","Ireland","Isle of Man","Israel","Italy","Jamaica","Japan","Jersey","Jordan","Kazakhstan","Kenya","Kiribati","Kosovo","Kuwait","Kyrgyzstan","Laos","Latvia","Lebanon","Lesotho","Liberia","Libya","Liechtenstein","Lithuania","Luxembourg","Macau","Macedonia","Madagascar","Malawi","Malaysia","Maldives","Mali","Malta","Marshall Islands","Mauritania","Mauritius","Mexico","Micronesia","Moldova","Monaco","Mongolia","Montenegro","Montserrat","Morocco","Mozambique","Myanmar","Namibia","Nauro","Nepal","Netherlands","Netherlands Antilles","New Caledonia","New Zealand","Nicaragua","Niger","Nigeria","North Korea","Norway","Oman","Pakistan","Palau","Palestine","Panama","Papua New Guinea","Paraguay","Peru","Philippines","Poland","Portugal","Puerto Rico","Qatar","Reunion","Romania","Russia","Rwanda","Saint Pierre & Miquelon","Samoa","San Marino","Sao Tome and Principe","Saudi Arabia","Senegal","Serbia","Seychelles","Sierra Leone","Singapore","Slovakia","Slovenia","Solomon Islands","Somalia","South Africa","South Korea","South Sudan","Spain","Sri Lanka","St Kitts & Nevis","St Lucia","St Vincent","Sudan","Suriname","Swaziland","Sweden","Switzerland","Syria","Taiwan","Tajikistan","Tanzania","Thailand","Timor L'Este","Togo","Tonga","Trinidad & Tobago","Tunisia","Turkey","Turkmenistan","Turks & Caicos","Tuvalu","Uganda","Ukraine","United Arab Emirates","United Kingdom","United States of America","Uruguay","Uzbekistan","Vanuatu","Vatican City","Venezuela","Vietnam","Virgin Islands (US)","Yemen","Zambia","Zimbabwe"];

/*initiate the autocomplete function on the "myInput" element, and pass along the countries array as possible autocomplete values:*/
autocomplete(document.getElementById("myInput"), countries);

// dropdown menu 

document.addEventListener("DOMContentLoaded", function(){
// make it as accordion for smaller screens
if (window.innerWidth > 992) {

	document.querySelectorAll('.navbar .nav-item').forEach(function(everyitem){

		everyitem.addEventListener('mouseover', function(e){

			let el_link = this.querySelector('a[data-bs-toggle]');

			if(el_link != null){
				let nextEl = el_link.nextElementSibling;
				el_link.classList.add('show');
				nextEl.classList.add('show');
			}

		});
		everyitem.addEventListener('mouseleave', function(e){
			let el_link = this.querySelector('a[data-bs-toggle]');

			if(el_link != null){
				let nextEl = el_link.nextElementSibling;
				el_link.classList.remove('show');
				nextEl.classList.remove('show');
			}


		})
	});

}
// end if innerWidth
}); 






  </script>

  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
    integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
    crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
    integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
    crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
    integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
    crossorigin="anonymous"></script>
</body>

</html>