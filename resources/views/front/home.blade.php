
@extends('layouts.new_front')

<?php if (isset($_GET['page_id'])){
		$next = $_GET['page_id']+1;
		$prev = $_GET['page_id']-1;
	}
	else {
		$prev = '0';
		$next = '1';
	}

$featureds = session()->get('featureds');
?>

@section('content')

<div id="bootstrap-touch-slider" class="carousel bs-slider fade  control-round indicators-line" data-ride="carousel" data-pause="hover" data-interval="false" >
   <!-- Indicators -->
   <ol class="carousel-indicators">
      <li data-target="#bootstrap-touch-slider" data-slide-to="0" class="active"></li>
      <li data-target="#bootstrap-touch-slider" data-slide-to="1"></li>
      <li data-target="#bootstrap-touch-slider" data-slide-to="2"></li>
   </ol>
   <!-- Wrapper For Slides -->
   <div class="carousel-inner" role="listbox">
      @if(!empty($featureds))
      <?php $i=0; ?>
      @foreach ($featureds as $featured)
      <!-- Third Slide -->
      <div class="item @if($i==0){{'active'}}@endif">
         <!-- Slide Background -->
         @if(isset($featured->poster_path))
         <img src="https://image.tmdb.org/t/p/w500{{$featured->poster_path}}" alt="Bootstrap Touch Slider"  class="slide-image"/>
         @else
         <img src="/images/default.png" alt="Bootstrap Touch Slider"  class="slide-image"/>
         @endif
         <div class="bs-slider-overlay"></div>
         <div class="container">
            <div class="row">
               <!-- Slide Text Layer -->
               <div class="slide-text slide_style_left headertxt">
                  <h4 data-animation="animated zoomInRight">{{$featured->name}}</h4>
                  <p data-animation="animated fadeInLeft">{{$featured->type}}</p>
                  <a href="<?php if ($featured->type == 'Series'){?>{{'tv'}}<?php } else echo 'movie';?>_detail/{{$featured->movie_id}}" target="_blank" class="btn btn-default" data-animation="animated fadeInLeft">Watch Now</a>
                  <div class="social-btn" style="margin-top: 8px;">
                     <a href="<?php if ($featured->type == 'Series'){?>{{'tv'}}<?php } else echo 'movie';?>_detail/{{$featured->movie_id}}" class="parent-btn" tabindex="0"><i class="ion-play"></i> Watch Trailer</a>
                     <a href="/favourite/{{$featured->movie_id}}" class="parent-btn" tabindex="0"><i class="ion-heart"></i> Add to Favorite</a>
                     <div class="hover-bnt">
                        <a href="#" class="parent-btn" tabindex="0"><i class="ion-android-share-alt"></i>share</a>
                        <div class="hvr-item">
                           <a href="#" class="hvr-grow" tabindex="0"><i class="ion-social-facebook"></i></a>
                           <a href="#" class="hvr-grow" tabindex="0"><i class="ion-social-twitter"></i></a>
                           <a href="#" class="hvr-grow" tabindex="0"><i class="ion-social-googleplus"></i></a>
                           <a href="#" class="hvr-grow" tabindex="0"><i class="ion-social-youtube"></i></a>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- End of Slide -->
      <?php $i++; ?>
      @endforeach
      @endif
      <!-- Second Slide -->
      
      <!-- End of Slide -->
      <!-- Third Slide -->
      
      <!-- End of Slide -->  </div>
   <!-- End of Wrapper For Slides -->
   <!-- Left Control -->
   <a class="left carousel-control" href="#bootstrap-touch-slider" role="button" data-slide="prev">
   <span class="fa fa-angle-left" aria-hidden="true"></span>
   <span class="sr-only">Previous</span>
   </a>
   <!-- Right Control -->
   <a class="right carousel-control" href="#bootstrap-touch-slider" role="button" data-slide="next">
   <span class="fa fa-angle-right" aria-hidden="true"></span>
   <span class="sr-only">Next</span>
   </a>
</div>

         <!-- End  bootstrap-touch-slider Slider -->
<div class="movie-items  full-width">
	<div class="row">
		<div class="col-md-12">
			<div class="title-hd">
				<h2>MOVIES</h2>
				<a href="#" class="nowshow">Now Showing <i class="ion-ios-arrow-right"></i></a>
				<a href="{{asset('/movies-list')}}" class="viewall">View all <i class="ion-ios-arrow-right"></i></a>

			</div>
			<div class="tabs">
				<ul class="tab-links">
					<li class="subtitle">Select The Movie You Want</li>
					                      
				</ul>
			    <div class="tab-content">
			        <div id="tab1-h2" class="tab active">
			            <div class="row">
			            	
			            	<div class="slick-multiItem">
			            		@if (isset($contents->results))
					@foreach ($contents->results as $element)
			            		<div class="slide-it">
									<div class="movie-item">
				            			<div class="mv-img">
				            				@if(isset($element->poster_path))
				            				<img src="https://image.tmdb.org/t/p/w300{{$element->poster_path}}" alt="" style="width: 120px; height: 180px">
				            				@else
				            				<img src="/images/default.png" alt="" style="width: 120px; height: 180px">
				            				@endif
				            			</div>
				            			<div class="hvr-inner">
				            				<a  href="/movie-detail/{{$element->id}}">
				            					<i class="fa fa-play" aria-hidden="true"></i> </a>
				            			</div>
				            			<div class="title-in">
				            				<h6><a href="/movie-detail/{{$element->id}}">{{$element->title}}</a></h6>
				            				<p><i class="ion-android-star"></i><span>{{$element->vote_average}}</span> /10</p>
				            			</div>
				            		</div>
								</div>
								
			            		@endforeach
			            	@endif
			            	</div>
			            	
			            </div>
			        </div>
			       
			    </div>
			    <hr class="hrstyle" />
			</div>

			
			
				{{-- <div class="title-hd">
				<h2>SHOWS</h2>
				<a href="#" class="nowshow">Now Showing <i class="ion-ios-arrow-right"></i></a>
				<a href="#" class="viewall">View all <i class="ion-ios-arrow-right"></i></a>

			</div> --}}
					{{-- 	<div class="tabs">
				<ul class="tab-links">
					<li class="subtitle">Select The Show You Want</li>
					                      
				</ul>
			    <div class="tab-content">
			        <div id="tab21-h2" class="tab active">
			            <div class="row">
			            	<div class="slick-multiItem2">
			            		<div class="slide-it">
			            			<div class="movie-item">
				            			<div class="mv-img">
				            				<img src="/asset/images/uploads/mv-it7.jpg" alt="">
				            			</div>
				            			<div class="hvr-inner">
				            				<a  href="moviesingle.html">
				            					<i class="fa fa-play" aria-hidden="true"></i> </a>
				            			</div>
				            			<div class="title-in">
				            				<h6><a href="#">Interstellar</a></h6>
				            				<p><i class="ion-android-star"></i><span>7.4</span> /10</p>
				            			</div>
				            		</div>
			            		</div>
								<div class="slide-it">
									<div class="movie-item">
				            			<div class="mv-img">
				            				<img src="/asset/images/uploads/mv-it8.jpg" alt="">
				            			</div>
				            			<div class="hvr-inner">
				            				<a  href="moviesingle.html">
				            					<i class="fa fa-play" aria-hidden="true"></i> </a>
				            			</div>
				            			<div class="title-in">
				            				<h6><a href="#">The revenant</a></h6>
				            				<p><i class="ion-android-star"></i><span>7.4</span> /10</p>
				            			</div>
				            		</div>
								</div>
			            		<div class="slide-it">
			            			<div class="movie-item">
				            			<div class="mv-img">
				            				<img src="/asset/images/uploads/mv-it9.jpg" alt="">
				            			</div>
				            			<div class="hvr-inner">
				            				<a  href="moviesingle.html">
				            					<i class="fa fa-play" aria-hidden="true"></i> </a>
				            			</div>
				            			<div class="title-in">
				            				<h6><a href="#">Die hard</a></h6>
				            				<p><i class="ion-android-star"></i><span>7.4</span> /10</p>
				            			</div>
				            		</div>
			            		</div>
			            		<div class="slide-it">
			            			<div class="movie-item">
				            			<div class="mv-img">
				            				<img src="/asset/images/uploads/mv-it4.jpg" alt="">
				            			</div>
				            			<div class="hvr-inner">
				            				<a  href="moviesingle.html">
				            					<i class="fa fa-play" aria-hidden="true"></i> </a>
				            			</div>
				            			<div class="title-in">
				            				<h6><a href="#">The walk</a></h6>
				            				<p><i class="ion-android-star"></i><span>7.4</span> /10</p>
				            			</div>
				            		</div>
			            		</div>
			            		<div class="slide-it">
			            			<div class="movie-item">
				            			<div class="mv-img">
				            				<img src="/asset/images/uploads/mv-it5.jpg" alt="">
				            			</div>
				            			<div class="hvr-inner">
				            				<a  href="moviesingle.html">
				            					<i class="fa fa-play" aria-hidden="true"></i> </a>
				            			</div>
				            			<div class="title-in">
				            				<h6><a href="#">Die hard</a></h6>
				            				<p><i class="ion-android-star"></i><span>7.4</span> /10</p>
				            			</div>
				            		</div>
			            		</div>
			            		<div class="slide-it">
			            			<div class="movie-item">
				            			<div class="mv-img">
				            				<img src="/asset/images/uploads/mv-it6.jpg" alt="">
				            			</div>
				            			<div class="hvr-inner">
				            				<a  href="moviesingle.html">
				            					<i class="fa fa-play" aria-hidden="true"></i> </a>
				            			</div>
				            			<div class="title-in">
				            				<h6><a href="#">Interstellar</a></h6>
				            				<p><i class="ion-android-star"></i><span>7.4</span> /10</p>
				            			</div>
				            		</div>
			            		</div>
			            		<div class="slide-it">
			            			<div class="movie-item">
				            			<div class="mv-img">
				            				<img src="/asset/images/uploads/mv-it7.jpg" alt="">
				            			</div>
				            			<div class="hvr-inner">
				            				<a  href="moviesingle.html">
				            					<i class="fa fa-play" aria-hidden="true"></i> </a>
				            			</div>
				            			<div class="title-in">
				            				<h6><a href="#">Die hard</a></h6>
				            				<p><i class="ion-android-star"></i><span>7.4</span> /10</p>
				            			</div>
				            		</div>
			            		</div>
			            		<div class="slide-it">
			            			<div class="movie-item">
				            			<div class="mv-img">
				            				<img src="/asset/images/uploads/mv-it8.jpg" alt="">
				            			</div>
				            			<div class="hvr-inner">
				            				<a  href="moviesingle.html">
				            					<i class="fa fa-play" aria-hidden="true"></i> </a>
				            			</div>
				            			<div class="title-in">
				            				<h6><a href="#">Die hard</a></h6>
				            				<p><i class="ion-android-star"></i><span>7.4</span> /10</p>
				            			</div>
				            		</div>
			            		</div>
			            	</div>
			            </div>
			        </div>
			        <div id="tab22-h2" class="tab">
			           <div class="row">
			            	<div class="slick-multiItem2">
			            		<div class="slide-it">
			            			<div class="movie-item">
				            			<div class="mv-img">
				            				<img src="/asset/images/uploads/mv-it4.jpg" alt="">
				            			</div>
				            			<div class="hvr-inner">
				            				<a  href="moviesingle.html">
				            					<i class="fa fa-play" aria-hidden="true"></i> </a>
				            			</div>
				            			<div class="title-in">
				            				<h6><a href="#">The walk</a></h6>
				            				<p><i class="ion-android-star"></i><span>7.4</span> /10</p>
				            			</div>
				            		</div>
			            		</div>
			            		<div class="slide-it">
			            			<div class="movie-item">
				            			<div class="mv-img">
				            				<img src="/asset/images/uploads/mv-it5.jpg" alt="">
				            			</div>
				            			<div class="hvr-inner">
				            				<a  href="moviesingle.html">
				            					<i class="fa fa-play" aria-hidden="true"></i> </a>
				            			</div>
				            			<div class="title-in">
				            				<h6><a href="#">Die hard</a></h6>
				            				<p><i class="ion-android-star"></i><span>7.4</span> /10</p>
				            			</div>
				            		</div>
			            		</div>
			            		<div class="slide-it">
			            			<div class="movie-item">
				            			<div class="mv-img">
				            				<img src="/asset/images/uploads/mv-it6.jpg" alt="">
				            			</div>
				            			<div class="hvr-inner">
				            				<a  href="moviesingle.html">
				            					<i class="fa fa-play" aria-hidden="true"></i> </a>
				            			</div>
				            			<div class="title-in">
				            				<h6><a href="#">Interstellar</a></h6>
				            				<p><i class="ion-android-star"></i><span>7.4</span> /10</p>
				            			</div>
				            		</div>
			            		</div>
			            		<div class="slide-it">
			            			<div class="movie-item">
				            			<div class="mv-img">
				            				<img src="/asset/images/uploads/mv-it7.jpg" alt="">
				            			</div>
				            			<div class="hvr-inner">
				            				<a  href="moviesingle.html">
				            					<i class="fa fa-play" aria-hidden="true"></i> </a>
				            			</div>
				            			<div class="title-in">
				            				<h6><a href="#">Die hard</a></h6>
				            				<p><i class="ion-android-star"></i><span>7.4</span> /10</p>
				            			</div>
				            		</div>
			            		</div>
			            		<div class="slide-it">
			            			<div class="movie-item">
				            			<div class="mv-img">
				            				<img src="/asset/images/uploads/mv-it8.jpg" alt="">
				            			</div>
				            			<div class="hvr-inner">
				            				<a  href="moviesingle.html">
				            					<i class="fa fa-play" aria-hidden="true"></i> </a>
				            			</div>
				            			<div class="title-in">
				            				<h6><a href="#">Die hard</a></h6>
				            				<p><i class="ion-android-star"></i><span>7.4</span> /10</p>
				            			</div>
				            		</div>
			            		</div>
			            	</div>
			            </div>
			        </div>
			        <div id="tab23-h2" class="tab">
			        	<div class="row">
			            	<div class="slick-multiItem2">
			            		<div class="movie-item">
			            			<div class="mv-img">
			            				<img src="/asset/images/uploads/mv-it1.jpg" alt="">
			            			</div>
			            			<div class="hvr-inner">
			            				<a  href="moviesingle.html"> Read more <i class="ion-android-arrow-dropright"></i> </a>
			            			</div>
			            			<div class="title-in">
			            				<h6><a href="#">Interstellar</a></h6>
			            				<p><i class="ion-android-star"></i><span>7.4</span> /10</p>
			            			</div>
			            		</div>
								<div class="movie-item">
			            			<div class="mv-img">
			            				<img src="/asset/images/uploads/mv-it2.jpg" alt="">
			            			</div>
			            			<div class="hvr-inner">
			            				<a  href="moviesingle.html"> Read more <i class="ion-android-arrow-dropright"></i> </a>
			            			</div>
			            			<div class="title-in">
			            				<h6><a href="#">The revenant</a></h6>
			            				<p><i class="ion-android-star"></i><span>7.4</span> /10</p>
			            			</div>
			            		</div>
			            		<div class="movie-item">
			            			<div class="mv-img">
			            				<img src="/asset/images/uploads/mv-it3.jpg" alt="">
			            			</div>
			            			<div class="hvr-inner">
			            				<a  href="moviesingle.html"> Read more <i class="ion-android-arrow-dropright"></i> </a>
			            			</div>
			            			<div class="title-in">
			            				<h6><a href="#">Die hard</a></h6>
			            				<p><i class="ion-android-star"></i><span>7.4</span> /10</p>
			            			</div>
			            		</div>
			            		<div class="movie-item">
			            			<div class="mv-img">
			            				<img src="/asset/images/uploads/mv-it4.jpg" alt="">
			            			</div>
			            			<div class="hvr-inner">
			            				<a  href="moviesingle.html"> Read more <i class="ion-android-arrow-dropright"></i> </a>
			            			</div>
			            			<div class="title-in">
			            				<h6><a href="#">The walk</a></h6>
			            				<p><i class="ion-android-star"></i><span>7.4</span> /10</p>
			            			</div>
			            		</div>
			            		<div class="movie-item">
			            			<div class="mv-img">
			            				<img src="/asset/images/uploads/mv-it3.jpg" alt="">
			            			</div>
			            			<div class="hvr-inner">
			            				<a  href="moviesingle.html"> Read more <i class="ion-android-arrow-dropright"></i> </a>
			            			</div>
			            			<div class="title-in">
			            				<h6><a href="#">Die hard</a></h6>
			            				<p><i class="ion-android-star"></i><span>7.4</span> /10</p>
			            			</div>
			            		</div>
			            	</div>
			            </div>
		       	 	</div>
		       	 	 <div id="tab24-h2" class="tab">
			        	<div class="row">
			            	<div class="slick-multiItem2">
			            		<div class="slide-it">
			            			<div class="movie-item">
				            			<div class="mv-img">
				            				<img src="/asset/images/uploads/mv-it4.jpg" alt="">
				            			</div>
				            			<div class="hvr-inner">
				            				<a  href="moviesingle.html">
				            					<i class="fa fa-play" aria-hidden="true"></i> </a>
				            			</div>
				            			<div class="title-in">
				            				<h6><a href="#">The walk</a></h6>
				            				<p><i class="ion-android-star"></i><span>7.4</span> /10</p>
				            			</div>
				            		</div>
			            		</div>
			            		<div class="slide-it">
			            			<div class="movie-item">
				            			<div class="mv-img">
				            				<img src="/asset/images/uploads/mv-it5.jpg" alt="">
				            			</div>
				            			<div class="hvr-inner">
				            				<a  href="moviesingle.html">
				            					<i class="fa fa-play" aria-hidden="true"></i> </a>
				            			</div>
				            			<div class="title-in">
				            				<h6><a href="#">Die hard</a></h6>
				            				<p><i class="ion-android-star"></i><span>7.4</span> /10</p>
				            			</div>
				            		</div>
			            		</div>
			            		<div class="slide-it">
			            			<div class="movie-item">
				            			<div class="mv-img">
				            				<img src="/asset/images/uploads/mv-it6.jpg" alt="">
				            			</div>
				            			<div class="hvr-inner">
				            				<a  href="moviesingle.html">
				            					<i class="fa fa-play" aria-hidden="true"></i> </a>
				            			</div>
				            			<div class="title-in">
				            				<h6><a href="#">Interstellar</a></h6>
				            				<p><i class="ion-android-star"></i><span>7.4</span> /10</p>
				            			</div>
				            		</div>
			            		</div>
			            		<div class="slide-it">
			            			<div class="movie-item">
				            			<div class="mv-img">
				            				<img src="/asset/images/uploads/mv-it7.jpg" alt="">
				            			</div>
				            			<div class="hvr-inner">
				            				<a  href="moviesingle.html">
				            					<i class="fa fa-play" aria-hidden="true"></i> </a>
				            			</div>
				            			<div class="title-in">
				            				<h6><a href="#">Die hard</a></h6>
				            				<p><i class="ion-android-star"></i><span>7.4</span> /10</p>
				            			</div>
				            		</div>
			            		</div>
			            		<div class="slide-it">
			            			<div class="movie-item">
				            			<div class="mv-img">
				            				<img src="/asset/images/uploads/mv-it8.jpg" alt="">
				            			</div>
				            			<div class="hvr-inner">
				            				<a  href="moviesingle.html">
				            					<i class="fa fa-play" aria-hidden="true"></i> </a>
				            			</div>
				            			<div class="title-in">
				            				<h6><a href="#">Die hard</a></h6>
				            				<p><i class="ion-android-star"></i><span>7.4</span> /10</p>
				            			</div>
				            		</div>
			            		</div>
			            	</div>
			            </div>
		       	 	</div>
			    </div>
			    <hr class="hrstyle">
			</div> --}}

{{-- 

							<div class="title-hd">
				<h2>GENRE</h2>
				<a href="#" class="nowshow">Now Showing <i class="ion-ios-arrow-right"></i></a>
				<a href="#" class="viewall">View all <i class="ion-ios-arrow-right"></i></a>

			</div> --}}
			{{-- <div class="tabs">
				<ul class="tab-links">
					<li class="subtitle">Select The Genre You Want</li>
					                      
				</ul>
			    <div class="tab-content">
			        <div id="tab21-h2" class="tab active">
			            <div class="row">
			            	<div class="slick-multiItem2">
			            		<div class="slide-it">
			            			<div class="movie-item">
				            			<div class="mv-img">
				            				<img src="/asset/images/uploads/mv-it7.jpg" alt="">
				            			</div>
				            			<div class="hvr-inner">
				            				<a  href="moviesingle.html">
				            					<i class="fa fa-play" aria-hidden="true"></i> </a>
				            			</div>
				            			<div class="title-in">
				            				<h6><a href="#">Interstellar</a></h6>
				            				<p><i class="ion-android-star"></i><span>7.4</span> /10</p>
				            			</div>
				            		</div>
			            		</div>
								<div class="slide-it">
									<div class="movie-item">
				            			<div class="mv-img">
				            				<img src="/asset/images/uploads/mv-it8.jpg" alt="">
				            			</div>
				            			<div class="hvr-inner">
				            				<a  href="moviesingle.html">
				            					<i class="fa fa-play" aria-hidden="true"></i> </a>
				            			</div>
				            			<div class="title-in">
				            				<h6><a href="#">The revenant</a></h6>
				            				<p><i class="ion-android-star"></i><span>7.4</span> /10</p>
				            			</div>
				            		</div>
								</div>
			            		<div class="slide-it">
			            			<div class="movie-item">
				            			<div class="mv-img">
				            				<img src="/asset/images/uploads/mv-it9.jpg" alt="">
				            			</div>
				            			<div class="hvr-inner">
				            				<a  href="moviesingle.html">
				            					<i class="fa fa-play" aria-hidden="true"></i> </a>
				            			</div>
				            			<div class="title-in">
				            				<h6><a href="#">Die hard</a></h6>
				            				<p><i class="ion-android-star"></i><span>7.4</span> /10</p>
				            			</div>
				            		</div>
			            		</div>
			            		<div class="slide-it">
			            			<div class="movie-item">
				            			<div class="mv-img">
				            				<img src="/asset/images/uploads/mv-it4.jpg" alt="">
				            			</div>
				            			<div class="hvr-inner">
				            				<a  href="moviesingle.html">
				            					<i class="fa fa-play" aria-hidden="true"></i> </a>
				            			</div>
				            			<div class="title-in">
				            				<h6><a href="#">The walk</a></h6>
				            				<p><i class="ion-android-star"></i><span>7.4</span> /10</p>
				            			</div>
				            		</div>
			            		</div>
			            		<div class="slide-it">
			            			<div class="movie-item">
				            			<div class="mv-img">
				            				<img src="/asset/images/uploads/mv-it5.jpg" alt="">
				            			</div>
				            			<div class="hvr-inner">
				            				<a  href="moviesingle.html">
				            					<i class="fa fa-play" aria-hidden="true"></i> </a>
				            			</div>
				            			<div class="title-in">
				            				<h6><a href="#">Die hard</a></h6>
				            				<p><i class="ion-android-star"></i><span>7.4</span> /10</p>
				            			</div>
				            		</div>
			            		</div>
			            		<div class="slide-it">
			            			<div class="movie-item">
				            			<div class="mv-img">
				            				<img src="/asset/images/uploads/mv-it6.jpg" alt="">
				            			</div>
				            			<div class="hvr-inner">
				            				<a  href="moviesingle.html">
				            					<i class="fa fa-play" aria-hidden="true"></i> </a>
				            			</div>
				            			<div class="title-in">
				            				<h6><a href="#">Interstellar</a></h6>
				            				<p><i class="ion-android-star"></i><span>7.4</span> /10</p>
				            			</div>
				            		</div>
			            		</div>
			            		<div class="slide-it">
			            			<div class="movie-item">
				            			<div class="mv-img">
				            				<img src="/asset/images/uploads/mv-it7.jpg" alt="">
				            			</div>
				            			<div class="hvr-inner">
				            				<a  href="moviesingle.html">
				            					<i class="fa fa-play" aria-hidden="true"></i> </a>
				            			</div>
				            			<div class="title-in">
				            				<h6><a href="#">Die hard</a></h6>
				            				<p><i class="ion-android-star"></i><span>7.4</span> /10</p>
				            			</div>
				            		</div>
			            		</div>
			            		<div class="slide-it">
			            			<div class="movie-item">
				            			<div class="mv-img">
				            				<img src="/asset/images/uploads/mv-it8.jpg" alt="">
				            			</div>
				            			<div class="hvr-inner">
				            				<a  href="moviesingle.html">
				            					<i class="fa fa-play" aria-hidden="true"></i> </a>
				            			</div>
				            			<div class="title-in">
				            				<h6><a href="#">Die hard</a></h6>
				            				<p><i class="ion-android-star"></i><span>7.4</span> /10</p>
				            			</div>
				            		</div>
			            		</div>
			            	</div>
			            </div>
			        </div>
			        <div id="tab22-h2" class="tab">
			           <div class="row">
			            	<div class="slick-multiItem2">
			            		<div class="slide-it">
			            			<div class="movie-item">
				            			<div class="mv-img">
				            				<img src="/asset/images/uploads/mv-it4.jpg" alt="">
				            			</div>
				            			<div class="hvr-inner">
				            				<a  href="moviesingle.html">
				            					<i class="fa fa-play" aria-hidden="true"></i> </a>
				            			</div>
				            			<div class="title-in">
				            				<h6><a href="#">The walk</a></h6>
				            				<p><i class="ion-android-star"></i><span>7.4</span> /10</p>
				            			</div>
				            		</div>
			            		</div>
			            		<div class="slide-it">
			            			<div class="movie-item">
				            			<div class="mv-img">
				            				<img src="/asset/images/uploads/mv-it5.jpg" alt="">
				            			</div>
				            			<div class="hvr-inner">
				            				<a  href="moviesingle.html">
				            					<i class="fa fa-play" aria-hidden="true"></i> </a>
				            			</div>
				            			<div class="title-in">
				            				<h6><a href="#">Die hard</a></h6>
				            				<p><i class="ion-android-star"></i><span>7.4</span> /10</p>
				            			</div>
				            		</div>
			            		</div>
			            		<div class="slide-it">
			            			<div class="movie-item">
				            			<div class="mv-img">
				            				<img src="/asset/images/uploads/mv-it6.jpg" alt="">
				            			</div>
				            			<div class="hvr-inner">
				            				<a  href="moviesingle.html">
				            					<i class="fa fa-play" aria-hidden="true"></i> </a>
				            			</div>
				            			<div class="title-in">
				            				<h6><a href="#">Interstellar</a></h6>
				            				<p><i class="ion-android-star"></i><span>7.4</span> /10</p>
				            			</div>
				            		</div>
			            		</div>
			            		<div class="slide-it">
			            			<div class="movie-item">
				            			<div class="mv-img">
				            				<img src="/asset/images/uploads/mv-it7.jpg" alt="">
				            			</div>
				            			<div class="hvr-inner">
				            				<a  href="moviesingle.html">
				            					<i class="fa fa-play" aria-hidden="true"></i> </a>
				            			</div>
				            			<div class="title-in">
				            				<h6><a href="#">Die hard</a></h6>
				            				<p><i class="ion-android-star"></i><span>7.4</span> /10</p>
				            			</div>
				            		</div>
			            		</div>
			            		<div class="slide-it">
			            			<div class="movie-item">
				            			<div class="mv-img">
				            				<img src="/asset/images/uploads/mv-it8.jpg" alt="">
				            			</div>
				            			<div class="hvr-inner">
				            				<a  href="moviesingle.html">
				            					<i class="fa fa-play" aria-hidden="true"></i> </a>
				            			</div>
				            			<div class="title-in">
				            				<h6><a href="#">Die hard</a></h6>
				            				<p><i class="ion-android-star"></i><span>7.4</span> /10</p>
				            			</div>
				            		</div>
			            		</div>
			            	</div>
			            </div>
			        </div>
			        <div id="tab23-h2" class="tab">
			        	<div class="row">
			            	<div class="slick-multiItem2">
			            		<div class="movie-item">
			            			<div class="mv-img">
			            				<img src="/asset/images/uploads/mv-it1.jpg" alt="">
			            			</div>
			            			<div class="hvr-inner">
			            				<a  href="moviesingle.html"> Read more <i class="ion-android-arrow-dropright"></i> </a>
			            			</div>
			            			<div class="title-in">
			            				<h6><a href="#">Interstellar</a></h6>
			            				<p><i class="ion-android-star"></i><span>7.4</span> /10</p>
			            			</div>
			            		</div>
								<div class="movie-item">
			            			<div class="mv-img">
			            				<img src="/asset/images/uploads/mv-it2.jpg" alt="">
			            			</div>
			            			<div class="hvr-inner">
			            				<a  href="moviesingle.html"> Read more <i class="ion-android-arrow-dropright"></i> </a>
			            			</div>
			            			<div class="title-in">
			            				<h6><a href="#">The revenant</a></h6>
			            				<p><i class="ion-android-star"></i><span>7.4</span> /10</p>
			            			</div>
			            		</div>
			            		<div class="movie-item">
			            			<div class="mv-img">
			            				<img src="/asset/images/uploads/mv-it3.jpg" alt="">
			            			</div>
			            			<div class="hvr-inner">
			            				<a  href="moviesingle.html"> Read more <i class="ion-android-arrow-dropright"></i> </a>
			            			</div>
			            			<div class="title-in">
			            				<h6><a href="#">Die hard</a></h6>
			            				<p><i class="ion-android-star"></i><span>7.4</span> /10</p>
			            			</div>
			            		</div>
			            		<div class="movie-item">
			            			<div class="mv-img">
			            				<img src="/asset/images/uploads/mv-it4.jpg" alt="">
			            			</div>
			            			<div class="hvr-inner">
			            				<a  href="moviesingle.html"> Read more <i class="ion-android-arrow-dropright"></i> </a>
			            			</div>
			            			<div class="title-in">
			            				<h6><a href="#">The walk</a></h6>
			            				<p><i class="ion-android-star"></i><span>7.4</span> /10</p>
			            			</div>
			            		</div>
			            		<div class="movie-item">
			            			<div class="mv-img">
			            				<img src="/asset/images/uploads/mv-it3.jpg" alt="">
			            			</div>
			            			<div class="hvr-inner">
			            				<a  href="moviesingle.html"> Read more <i class="ion-android-arrow-dropright"></i> </a>
			            			</div>
			            			<div class="title-in">
			            				<h6><a href="#">Die hard</a></h6>
			            				<p><i class="ion-android-star"></i><span>7.4</span> /10</p>
			            			</div>
			            		</div>
			            	</div>
			            </div>
		       	 	</div>
		       	 	 <div id="tab24-h2" class="tab">
			        	<div class="row">
			            	<div class="slick-multiItem2">
			            		<div class="slide-it">
			            			<div class="movie-item">
				            			<div class="mv-img">
				            				<img src="/asset/images/uploads/mv-it4.jpg" alt="">
				            			</div>
				            			<div class="hvr-inner">
				            				<a  href="moviesingle.html">
				            					<i class="fa fa-play" aria-hidden="true"></i> </a>
				            			</div>
				            			<div class="title-in">
				            				<h6><a href="#">The walk</a></h6>
				            				<p><i class="ion-android-star"></i><span>7.4</span> /10</p>
				            			</div>
				            		</div>
			            		</div>
			            		<div class="slide-it">
			            			<div class="movie-item">
				            			<div class="mv-img">
				            				<img src="/asset/images/uploads/mv-it5.jpg" alt="">
				            			</div>
				            			<div class="hvr-inner">
				            				<a  href="moviesingle.html">
				            					<i class="fa fa-play" aria-hidden="true"></i> </a>
				            			</div>
				            			<div class="title-in">
				            				<h6><a href="#">Die hard</a></h6>
				            				<p><i class="ion-android-star"></i><span>7.4</span> /10</p>
				            			</div>
				            		</div>
			            		</div>
			            		<div class="slide-it">
			            			<div class="movie-item">
				            			<div class="mv-img">
				            				<img src="/asset/images/uploads/mv-it6.jpg" alt="">
				            			</div>
				            			<div class="hvr-inner">
				            				<a  href="moviesingle.html">
				            					<i class="fa fa-play" aria-hidden="true"></i> </a>
				            			</div>
				            			<div class="title-in">
				            				<h6><a href="#">Interstellar</a></h6>
				            				<p><i class="ion-android-star"></i><span>7.4</span> /10</p>
				            			</div>
				            		</div>
			            		</div>
			            		<div class="slide-it">
			            			<div class="movie-item">
				            			<div class="mv-img">
				            				<img src="/asset/images/uploads/mv-it7.jpg" alt="">
				            			</div>
				            			<div class="hvr-inner">
				            				<a  href="moviesingle.html">
				            					<i class="fa fa-play" aria-hidden="true"></i> </a>
				            			</div>
				            			<div class="title-in">
				            				<h6><a href="#">Die hard</a></h6>
				            				<p><i class="ion-android-star"></i><span>7.4</span> /10</p>
				            			</div>
				            		</div>
			            		</div>
			            		<div class="slide-it">
			            			<div class="movie-item">
				            			<div class="mv-img">
				            				<img src="/asset/images/uploads/mv-it8.jpg" alt="">
				            			</div>
				            			<div class="hvr-inner">
				            				<a  href="moviesingle.html">
				            					<i class="fa fa-play" aria-hidden="true"></i> </a>
				            			</div>
				            			<div class="title-in">
				            				<h6><a href="#">Die hard</a></h6>
				            				<p><i class="ion-android-star"></i><span>7.4</span> /10</p>
				            			</div>
				            		</div>
			            		</div>
			            	</div>
			            </div>
		       	 	</div>
			    </div>
			    <hr class="hrstyle">
			</div>
 --}}




<div class="title-hd">
				<h2>T-SERIES</h2>
				<a href="#" class="nowshow">Now Showing <i class="ion-ios-arrow-right"></i></a>
				<a href="/t-series" class="viewall">View all <i class="ion-ios-arrow-right"></i></a>

			</div>
			<div class="tabs">
				<ul class="tab-links">
					<li class="subtitle">Select The T-Series You Want</li>
					                      
				</ul>
			    <div class="tab-content">
			        <div id="tab1-h2" class="tab active">
			            <div class="row">
			            	<div class="slick-multiItem">
			            		@if(!empty($tvseries))
			            		@foreach ($tvseries->results as $element)
			            		<div class="slide-it">
									<div class="movie-item">
				            			<div class="mv-img">
				            				@if(isset($element->poster_path))
				            				<img src="https://image.tmdb.org/t/p/w300{{$element->backdrop_path}}" alt="">
				            				@else
				            				<img src="/images/default.png" alt="" style="width: 240px; height: 340px">
				            				@endif
				            			</div>
				            			<div class="hvr-inner">
				            				<a  href="/tseries-detail/{{$element->id}}">
				            					<i class="fa fa-play" aria-hidden="true"></i> </a>
				            			</div>
				            			<div class="title-in">
				            				<h6><a href="/tseries-detail/{{$element->id}}">{{$element->name}}</a></h6>
				            				<p><i class="ion-android-star"></i><span>{{$element->vote_average}}</span> /10</p>
				            			</div>
				            		</div>
								</div>
								@endforeach
								@endif
								
			            	</div>
			            </div>
			        </div>
			        
			    </div>
			    <hr class="hrstyle">
			</div>

		</div>
	</div>
	
</div>

@endsection 