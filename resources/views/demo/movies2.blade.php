<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
        integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <!-- fonts -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <!-- custom CSS -->
    <link rel="stylesheet" href="css/style.css">
    <title>Khoj</title>
</head>

<body>
  <section class="social">
    <nav>
      <ul>
          <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
          <li><a href="#"><i class="fab fa-twitter"></i></a></li>
          <li><a href="#"><i class="fab fa-pinterest"></i></a></li>
          <li><a href="#"><i class="fas fa-envelope"></i></a></li>
          <li><a href="#"><i class="fas fa-print"></i></a></li>
         
      </ul>
  </nav>
  </section>
  <div class="modal fade5" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle"
  aria-hidden="true">
  <div class="modal-dialog5" role="document">
    <div class="modal-content5">
      <div class="modal-header5">
        <h1 class="modal-title" id="exampleModalLongTitle">Settings
          <button type="button" class="close pr-5" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </h1>
      </div>
      <div class="modal-body5">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 pr-0">
          <h3>Account</h3>
          <div class="tab">
            <button class="tablinks" onclick="openCity(event, 'SignIn')" id="defaultOpen"> <i
                class="fas fa-sign-in-alt pr-3"></i>Sign In<i class="fas fa-chevron-right"></i></button>
            <button class="tablinks" onclick="openCity(event, 'MyLanguage')"><i class="fas fa-language pr-3"></i>My
              Language<i class="fas fa-chevron-right"></i></button>
            <button class="tablinks" onclick="openCity(event, 'MyProviders')"><i
                class="fas fa-satellite-dish pr-3"></i>My Providers<i class="fas fa-chevron-right"></i></button>
            <button class="tablinks" onclick="openCity(event, 'MyGenres')"><i class="fas fa-mars-double pr-3"></i>My
              Genres<i class="fas fa-chevron-right"></i></button>
            <button class="tablinks" onclick="openCity(event, 'MyActors')"><i class="fas fa-podcast pr-3"></i>My
              Actors<i class="fas fa-chevron-right"></i></button>
            <button class="tablinks" onclick="openCity(event, 'MyFilmmakers')"><i class="fas fa-tv pr-3"></i>My
              Filmmakers<i class="fas fa-chevron-right"></i></button>
          </div>
          <div id="SignIn" class="tabcontent">

            <img src="images/Khojlogo.png">
            <h2 class="pt-4">Sign in to synchronize your watchlist across all your devices</h2>
            <button type="button" class="btn btn-lg mt-4 conti" data-toggle="modal"
              data-target="#my-modal">Continue</button>

          </div>
          <div id="MyLanguage" class="tabcontent">
            <div class="row  pt-4">
              <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 ">
                <h3 class="pt-2 pr-2">Select All &nbsp;
                  <label class="switch mr-3">
                    <input type="checkbox">
                    <span class="slider round"></span>
                  </label>
                </h3>
              </div>
              <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 ">
                <a href="#">
                  <h3 class="pt-2 pr-2 text-right"><i class="fas fa-undo"></i>&nbsp; Clear</h3>
                </a>

              </div>


            </div>
            <div class="row">
              <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 ">
                <button type="button" class="btn btn-lg mt-4 my_lang">
                  <div class="row">
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4 ">
                      <p class="alpha_bet ">E</p>
                    </div>
                    <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-8 ">
                      <p class="text-left pt-2">English</p>
                    </div>
                  </div>
                </button>
              </div>

              <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 ">
                <button type="button" class="btn btn-lg mt-4 my_lang">
                  <div class="row">
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4 ">
                      <p class="alpha_bet">हि</p>
                    </div>
                    <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-8 ">
                      <p class="text-left pt-2">Hindi</p>
                    </div>
                  </div>
                </button>
              </div>

            </div>
            <div class="row">
              <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 ">
                <button type="button" class="btn btn-lg mt-4 my_lang">
                  <div class="row">
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4 ">
                      <p class="alpha_bet">తె</p>
                    </div>
                    <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-8 ">
                      <p class="text-left pt-2">Telugu</p>
                    </div>
                  </div>
                </button>
              </div>

              <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 ">
                <button type="button" class="btn btn-lg mt-4 my_lang">
                  <div class="row">
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4 ">
                      <p class="alpha_bet">മ</p>
                    </div>
                    <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-8 ">
                      <p class="text-left pt-2">Malayalam</p>
                    </div>
                  </div>
                </button>
              </div>

            </div>
            <div class="row">
              <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 ">
                <button type="button" class="btn btn-lg mt-4 my_lang">
                  <div class="row">
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4 ">
                      <p class="alpha_bet">த</p>
                    </div>
                    <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-8 ">
                      <p class="text-left pt-2">Tamil</p>
                    </div>
                  </div>
                </button>
              </div>

              <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 ">
                <button type="button" class="btn btn-lg mt-4 my_lang">
                  <div class="row">
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4 ">
                      <p class="alpha_bet">म</p>
                    </div>
                    <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-8 ">
                      <p class="text-left pt-2">Marathi</p>
                    </div>
                  </div>
                </button>
              </div>

            </div>

            <div class="row">
              <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 ">
                <button type="button" class="btn btn-lg mt-4 my_lang">
                  <div class="row">
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4 ">
                      <p class="alpha_bet">ગુ</p>
                    </div>
                    <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-8 ">
                      <p class="text-left pt-2">Gujrati</p>
                    </div>
                  </div>
                </button>
              </div>

              <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 ">
                <button type="button" class="btn btn-lg mt-4 my_lang">
                  <div class="row">
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4 ">
                      <p class="alpha_bet">ਪੰ</p>
                    </div>
                    <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-8 ">
                      <p class="text-left pt-2">Punjabi</p>
                    </div>
                  </div>
                </button>
              </div>

            </div>
            <div class="row">
              <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 ">
                <button type="button" class="btn btn-lg mt-4 my_lang">
                  <div class="row">
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4 ">
                      <p class="alpha_bet">ଓ</p>
                    </div>
                    <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-8 ">
                      <p class="text-left pt-2">Oddisa</p>
                    </div>
                  </div>
                </button>
              </div>
              <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 ">

              </div>

            </div>
            <div class="row justify-content-center pt-4">
              <button type="button" class="btn-primary  rounded px-5 py-2">Lets Go</button>
            </div>

          </div>
          <div id="MyProviders" class="tabcontent">
            <div class="row  pt-4">
              <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 ">
                <h3 class="pt-2 pr-2">Select All &nbsp;
                  <label class="switch mr-3">
                    <input type="checkbox">
                    <span class="slider round"></span>
                  </label>
                </h3>
              </div>
              <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 ">
                <a href="#">
                  <h3 class="pt-2 pr-2 text-right"><i class="fas fa-undo"></i>&nbsp; Clear</h3>
                </a>

              </div>


            </div>
            <div class="row">
              <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 ">
                <button type="button" class="btn btn-lg mt-4 my_lang">
                  <div class="row">
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4 ">
                      <img src="images/netflix-252.png " width="40" height="40">
                    </div>
                    <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-8 ">
                      <p class="text-left pt-3">Netflix</p>
                    </div>
                  </div>
                </button>
              </div>

              <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 ">
                <button type="button" class="btn btn-lg mt-4 my_lang">
                  <div class="row">
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4 ">
                      <img src="images/amazon-prime-video-436.png " width="40" height="40">
                    </div>
                    <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-8 ">
                      <p class="text-left pt-3">Prime Video</p>
                    </div>
                  </div>
                </button>
              </div>

            </div>
            <div class="row">
              <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 ">
                <button type="button" class="btn btn-lg mt-4 my_lang">
                  <div class="row">
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4 ">
                      <img src="images/disney-hotstar-323.png " width="40" height="40">
                    </div>
                    <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-8 ">
                      <p class="text-left pt-3">Disney+ Hotstar</p>
                    </div>
                  </div>
                </button>
              </div>

              <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 ">
                <button type="button" class="btn btn-lg mt-4 my_lang">
                  <div class="row">
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4 ">
                      <img src="images/zee5-945.png " width="40" height="40">
                    </div>
                    <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-8 ">
                      <p class="text-left pt-3">Zee5</p>
                    </div>
                  </div>
                </button>
              </div>

            </div>
            <div class="row">
              <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 ">
                <button type="button" class="btn btn-lg mt-4 my_lang">
                  <div class="row">
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4 ">
                      <img src="images/sony-liv-485.png " width="40" height="40">
                    </div>
                    <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-8 ">
                      <p class="text-left pt-3">SonyLIv</p>
                    </div>
                  </div>
                </button>
              </div>

              <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 ">
                <button type="button" class="btn btn-lg mt-4 my_lang">
                  <div class="row">
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4 ">
                      <img src="images/voot.jpg " width="40" height="40">
                    </div>
                    <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-8 ">
                      <p class="text-left pt-3">Voot</p>
                    </div>
                  </div>
                </button>
              </div>

            </div>

            <div class="row">
              <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 ">
                <button type="button" class="btn btn-lg mt-4 my_lang">
                  <div class="row">
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4 ">
                      <img src="images/mx-player-34.png " width="40" height="40">
                    </div>
                    <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-8 ">
                      <p class="text-left pt-3">Mx Player</p>
                    </div>
                  </div>
                </button>
              </div>

              <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 ">
                <button type="button" class="btn btn-lg mt-4 my_lang">
                  <div class="row">
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4 ">
                      <img src="images/jio-cinema-665.png " width="40" height="40">
                    </div>
                    <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-8 ">
                      <p class="text-left pt-3">Jio Cinema</p>
                    </div>
                  </div>
                </button>
              </div>

            </div>
            <div class="row">
              <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 ">
                <button type="button" class="btn btn-lg mt-4 my_lang">
                  <div class="row">
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4 ">
                      <img src="images/dicovery-538.png " width="40" height="40">
                    </div>
                    <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-8 ">
                      <p class="text-left pt-3">Discovery+</p>
                    </div>
                  </div>
                </button>
              </div>
              <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 ">
                <button type="button" class="btn btn-lg mt-4 my_lang">
                  <div class="row">
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4 ">
                      <img src="images/sunnxt-393.png " width="40" height="40">
                    </div>
                    <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-8 ">
                      <p class="text-left pt-3">Sun Nxt</p>
                    </div>
                  </div>
                </button>
              </div>
            </div>
            <div class="row">
              <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 ">
                <button type="button" class="btn btn-lg mt-4 my_lang">
                  <div class="row">
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4 ">
                      <img src="images/dicovery-538.png " width="40" height="40">
                    </div>
                    <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-8 ">
                      <p class="text-left pt-3">Discovery+</p>
                    </div>
                  </div>
                </button>
              </div>
              <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 ">
                <button type="button" class="btn btn-lg mt-4 my_lang">
                  <div class="row">
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4 ">
                      <img src="images/sunnxt-393.png " width="40" height="40">
                    </div>
                    <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-8 ">
                      <p class="text-left pt-3">Sun Nxt</p>
                    </div>
                  </div>
                </button>
              </div>
            </div>

            <div class="row">
              <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 ">
                <button type="button" class="btn btn-lg mt-4 my_lang">
                  <div class="row">
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4 ">
                      <img src="images/dicovery-538.png " width="40" height="40">
                    </div>
                    <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-8 ">
                      <p class="text-left pt-3">Discovery+</p>
                    </div>
                  </div>
                </button>
              </div>
              <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 ">
                <button type="button" class="btn btn-lg mt-4 my_lang">
                  <div class="row">
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4 ">
                      <img src="images/sunnxt-393.png " width="40" height="40">
                    </div>
                    <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-8 ">
                      <p class="text-left pt-3">Sun Nxt</p>
                    </div>
                  </div>
                </button>
              </div>
            </div>

            <div class="row">
              <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 ">
                <button type="button" class="btn btn-lg mt-4 my_lang">
                  <div class="row">
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4 ">
                      <img src="images/dicovery-538.png " width="40" height="40">
                    </div>
                    <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-8 ">
                      <p class="text-left pt-3">Discovery+</p>
                    </div>
                  </div>
                </button>
              </div>
              <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 ">
                <button type="button" class="btn btn-lg mt-4 my_lang">
                  <div class="row">
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4 ">
                      <img src="images/sunnxt-393.png " width="40" height="40">
                    </div>
                    <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-8 ">
                      <p class="text-left pt-3">Sun Nxt</p>
                    </div>
                  </div>
                </button>
              </div>
            </div>

            <div class="row">
              <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 ">
                <button type="button" class="btn btn-lg mt-4 my_lang">
                  <div class="row">
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4 ">
                      <img src="images/dicovery-538.png " width="40" height="40">
                    </div>
                    <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-8 ">
                      <p class="text-left pt-3">Discovery+</p>
                    </div>
                  </div>
                </button>
              </div>
              <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 ">
                <button type="button" class="btn btn-lg mt-4 my_lang">
                  <div class="row">
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4 ">
                      <img src="images/sunnxt-393.png " width="40" height="40">
                    </div>
                    <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-8 ">
                      <p class="text-left pt-3">Sun Nxt</p>
                    </div>
                  </div>
                </button>
              </div>
            </div>
            <div class="row justify-content-center pt-4">
              <button type="button" class="btn-primary rounded px-5 py-2">Lets Go</button>
            </div>
          </div>
          <div id="MyGenres" class="tabcontent">
            <div class="row  pt-4">
              <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 ">
                <h3 class="pt-2 pr-2">Select All &nbsp;
                  <label class="switch mr-3">
                    <input type="checkbox">
                    <span class="slider round"></span>
                  </label>
                </h3>
              </div>
              <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 ">
                <a href="#">
                  <h3 class="pt-2 pr-2 text-right"><i class="fas fa-undo"></i>&nbsp; Clear</h3>
                </a>
              </div>
            </div>
            <div class="row pt-3">
              <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                <a href="#"><img src="images/adventure-849.png" class="filter_img" style="width: 100% ; "
                    alt="img-responsive" class="img-fluid ">
                  <p class="text-center">Adventure</p>
                </a>
              </div>

              <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                <a href="#"><img src="images/drama-346.png" class="filter_img" style="width: 100% ; "
                    alt="img-responsive" class="img-fluid ">
                  <p class="text-center">Drama</p>
                </a>
              </div>

              <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                <a href="#"><img src="images/war-314.png" class="filter_img" style="width: 100% ; "
                    alt="img-responsive" class="img-fluid ">
                  <p class="text-center">War</p>
                </a>
              </div>



            </div>
            <div class="row pt-3">
              <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                <a href="#"><img src="images/horror-527.png" class="filter_img" style="width: 100% ; "
                    alt="img-responsive" class="img-fluid ">
                  <p class="text-center">Horror</p>
                </a>
              </div>

              <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                <a href="#"> <img src="images/western-341.png" class="filter_img" style="width: 100% ; "
                    alt="img-responsive" class="img-fluid ">
                  <p class="text-center">Westeren</p>
                </a>
              </div>

              <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                <a href="#"><img src="images/comedy-938.png" class="filter_img" style="width: 100% ; "
                    alt="img-responsive" class="img-fluid ">
                  <p class="text-center">Comedy</p>
                </a>
              </div>
            </div>
            <div class="row pt-3">
              <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                <a href="#"> <img src="images/adventure-849.png" class="filter_img" style="width: 100% ; "
                    alt="img-responsive" class="img-fluid ">
                  <p class="text-center">Adventure</p>
                </a>
              </div>

              <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                <a href="#"> <img src="images/drama-346.png" class="filter_img" style="width: 100% ; "
                    alt="img-responsive" class="img-fluid ">
                  <p class="text-center">Drama</p>
                </a>
              </div>

              <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                <a href="#"> <img src="images/war-314.png" class="filter_img" style="width: 100% ; "
                    alt="img-responsive" class="img-fluid ">
                  <p class="text-center">War</p>
                </a>
              </div>



            </div>
            <div class="row pt-3">
              <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                <a href="#"> <img src="images/horror-527.png" class="filter_img" style="width: 100% ; "
                    alt="img-responsive" class="img-fluid ">
                  <p class="text-center">Horror</p>
                </a>
              </div>

              <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                <a href="#"> <img src="images/western-341.png" class="filter_img" style="width: 100% ; "
                    alt="img-responsive" class="img-fluid ">
                  <p class="text-center">Westeren</p>
                </a>
              </div>

              <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                <a href="#"> <img src="images/comedy-938.png" class="filter_img" style="width: 100% ; "
                    alt="img-responsive" class="img-fluid ">
                  <p class="text-center">Comedy</p>
                </a>
              </div>



            </div>
            <div class="row pt-3">
              <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                <a href="#"> <img src="images/adventure-849.png" class="filter_img" style="width: 100% ; "
                    alt="img-responsive" class="img-fluid ">
                  <p class="text-center">Adventure</p>
                </a>
              </div>

              <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                <a href="#"><img src="images/drama-346.png" class="filter_img" style="width: 100% ; "
                    alt="img-responsive" class="img-fluid ">
                  <p class="text-center">Drama</p>
                </a>
              </div>

              <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                <a href="#"> <img src="images/war-314.png" class="filter_img" style="width: 100% ; "
                    alt="img-responsive" class="img-fluid ">
                  <p class="text-center">War</p>
                </a>
              </div>



            </div>
            <div class="row pt-3">
              <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                <a href="#"> <img src="images/horror-527.png" class="filter_img" style="width: 100% ; "
                    alt="img-responsive" class="img-fluid ">
                  <p class="text-center">Horror</p>
                </a>
              </div>

              <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                <a href="#"> <img src="images/western-341.png" class="filter_img" style="width: 100% ; "
                    alt="img-responsive" class="img-fluid ">
                  <p class="text-center">Westeren</p>
                </a>
              </div>

              <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                <a href="#"> <img src="images/comedy-938.png" class="filter_img" style="width: 100% ; "
                    alt="img-responsive" class="img-fluid ">
                  <p class="text-center">Comedy</p>
                </a>
              </div>

            </div>
            <div class="row justify-content-center pt-4">
              <button type="button" class="btn-primary rounded px-5 py-2">Lets Go</button>
            </div>
          </div>
          <div id="MyActors" class="tabcontent">
            <div class="row  pt-4">
              <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 ">
                <h3 class="pt-2 pr-2">Select All &nbsp;
                  <label class="switch mr-3">
                    <input type="checkbox">
                    <span class="slider round"></span>
                  </label>
                </h3>
              </div>
              <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 ">
                <a href="#">
                  <h3 class="pt-2 pr-2 text-right"><i class="fas fa-undo"></i>&nbsp; Clear</h3>
                </a>

              </div>


            </div>
            <div class="row pt-3">
              <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                <a href="#"><img src="images/ranbir.png" class="filter_img" style="width: 100% ; "
                    alt="img-responsive" class="img-fluid ">
                  <h4 class="text-center">Ranbir kapoor</h4>
                </a>
              </div>

              <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                <a href="#"> <img src="images/alia.png" class="filter_img" style="width: 100% ; " alt="img-responsive"
                    class="img-fluid ">
                  <h4 class="text-center">Alia Bhatt</h4>
                </a>
              </div>

              <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                <a href="#"> <img src="images/ranveer.png" class="filter_img" style="width: 100% ; "
                    alt="img-responsive" class="img-fluid ">
                  <h4 class="text-center">Ranveer singh</h4>
                </a>
              </div>

            </div>
            <div class="row pt-3">
              <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                <a href="#"> <img src="images/ranbir.png" class="filter_img" style="width: 100% ; "
                    alt="img-responsive" class="img-fluid ">
                  <h4 class="text-center">Ranbir kapoor</h4>
                </a>
              </div>

              <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                <a href="#"><img src="images/alia.png" class="filter_img" style="width: 100% ; " alt="img-responsive"
                    class="img-fluid ">
                  <h4 class="text-center">Alia Bhatt</h4><a href="#"></a>
              </div>

              <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                <a href="#"><img src="images/ranveer.png" class="filter_img" style="width: 100% ; "
                    alt="img-responsive" class="img-fluid ">
                  <h4 class="text-center">Ranveer singh</h4>
                </a>
              </div>

            </div>
            <div class="row pt-3">
              <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                <a href="#"> <img src="images/ranbir.png" class="filter_img" style="width: 100% ; "
                    alt="img-responsive" class="img-fluid ">
                  <h4 class="text-center">Ranbir kapoor</h4>
                </a>
              </div>

              <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                <a href="#"> <img src="images/alia.png" class="filter_img" style="width: 100% ; " alt="img-responsive"
                    class="img-fluid ">
                  <h4 class="text-center">Alia Bhatt</h4>
                </a>
              </div>

              <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                <a href="#"> <img src="images/ranveer.png" class="filter_img" style="width: 100% ; "
                    alt="img-responsive" class="img-fluid ">
                  <h4 class="text-center">Ranveer singh</h4>
                </a>
              </div>

            </div>
            <div class="row pt-3">
              <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                <a href="#"> <img src="images/ranbir.png" class="filter_img" style="width: 100% ; "
                    alt="img-responsive" class="img-fluid ">
                  <h4 class="text-center">Ranbir kapoor</h4>
                </a>
              </div>

              <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                <a href="#"> <img src="images/alia.png" class="filter_img" style="width: 100% ; " alt="img-responsive"
                    class="img-fluid ">
                  <h4 class="text-center">Alia Bhatt</h4>
                </a>
              </div>

              <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                <a href="#"> <img src="images/ranveer.png" class="filter_img" style="width: 100% ; "
                    alt="img-responsive" class="img-fluid ">
                  <h4 class="text-center">Ranveer singh</h4>
                </a>
              </div>

            </div>
            <div class="row justify-content-center pt-4">
              <button type="button" class="btn-primary rounded px-5 py-2">Lets Go</button>
            </div>
          </div>
          <div id="MyFilmmakers" class="tabcontent">
            <div class="row  pt-4">
              <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 ">
                <h3 class="pt-2 pr-2">Select All &nbsp;
                  <label class="switch mr-3">
                    <input type="checkbox">
                    <span class="slider round"></span>
                  </label>
                </h3>
              </div>
              <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 ">
                <a href="#">
                  <h3 class="pt-2 pr-2 text-right"><i class="fas fa-undo"></i>&nbsp; Clear</h3>
                </a>

              </div>


            </div>
            <div class="row pt-3">
              <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                <a href="#"><img src="images/ranbir.png" class="filter_img" style="width: 100% ; "
                    alt="img-responsive" class="img-fluid ">
                  <h4 class="text-center">Ranbir kapoor</h4>
                </a>
              </div>

              <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                <a href="#"> <img src="images/alia.png" class="filter_img" style="width: 100% ; " alt="img-responsive"
                    class="img-fluid ">
                  <h4 class="text-center">Alia Bhatt</h4>
                </a>
              </div>

              <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                <a href="#"> <img src="images/ranveer.png" class="filter_img" style="width: 100% ; "
                    alt="img-responsive" class="img-fluid ">
                  <h4 class="text-center">Ranveer singh</h4>
                </a>
              </div>

            </div>
            <div class="row pt-3">
              <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                <a href="#"> <img src="images/ranbir.png" class="filter_img" style="width: 100% ; "
                    alt="img-responsive" class="img-fluid ">
                  <h4 class="text-center">Ranbir kapoor</h4>
                </a>
              </div>

              <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                <a href="#"><img src="images/alia.png" class="filter_img" style="width: 100% ; " alt="img-responsive"
                    class="img-fluid ">
                  <h4 class="text-center">Alia Bhatt</h4><a href="#"></a>
              </div>

              <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                <a href="#"><img src="images/ranveer.png" class="filter_img" style="width: 100% ; "
                    alt="img-responsive" class="img-fluid ">
                  <h4 class="text-center">Ranveer singh</h4>
                </a>
              </div>

            </div>
            <div class="row pt-3">
              <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                <a href="#"> <img src="images/ranbir.png" class="filter_img" style="width: 100% ; "
                    alt="img-responsive" class="img-fluid ">
                  <h4 class="text-center">Ranbir kapoor</h4>
                </a>
              </div>

              <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                <a href="#"> <img src="images/alia.png" class="filter_img" style="width: 100% ; " alt="img-responsive"
                    class="img-fluid ">
                  <h4 class="text-center">Alia Bhatt</h4>
                </a>
              </div>

              <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                <a href="#"> <img src="images/ranveer.png" class="filter_img" style="width: 100% ; "
                    alt="img-responsive" class="img-fluid ">
                  <h4 class="text-center">Ranveer singh</h4>
                </a>
              </div>

            </div>
            <div class="row pt-3">
              <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                <a href="#"> <img src="images/ranbir.png" class="filter_img" style="width: 100% ; "
                    alt="img-responsive" class="img-fluid ">
                  <h4 class="text-center">Ranbir kapoor</h4>
                </a>
              </div>

              <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                <a href="#"> <img src="images/alia.png" class="filter_img" style="width: 100% ; " alt="img-responsive"
                    class="img-fluid ">
                  <h4 class="text-center">Alia Bhatt</h4>
                </a>
              </div>

              <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                <a href="#"> <img src="images/ranveer.png" class="filter_img" style="width: 100% ; "
                    alt="img-responsive" class="img-fluid ">
                  <h4 class="text-center">Ranveer singh</h4>
                </a>
              </div>

            </div>
            <div class="row justify-content-center pt-4">
              <button type="button" class="btn-primary rounded px-5 py-2">Lets Go</button>
            </div>
          </div>

        </div>
      </div>
    </div>
  </div>
</div>
<section class="top_bar">
  <div class="container">
    <div class="row pt-2">
      <button type="button" class="btn leftpop" data-toggle="modal" data-target="#exampleModal">
        Feedback
      </button>
      <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6">
        <div class="row">
          <a class="nav-link " href="#"> <img src="images/googleicon.png" width="20" alt=""></a>
          <a class="nav-link active  Text-white" href="#"><img src="images/apple.png" width="25" class="ml-4"
              alt=""></a>
        </div>
      </div>
      <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 pr-1">
        <ul class="list-inline list-unstyled mb-0">
          <li class="list-inline-item">
            <button type="button" class="btn_fasco pt-1" data-toggle="modal" data-target="#exampleModalLong">
              <a class="nav-link" href="#"><i class="fas fa-cog"></i></a>
          </li>
          </button>
          <li class="list-inline-item">
            <div class="container d-flex justify-content-center registermodal"> <button class="btn p-0 login  mr-2"
                data-toggle="modal" data-target="#my-modal">
                <i class="fas fa-user fa-2x"></i> </button>
              <div id="my-modal" class="modal fade fix_modal" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog justify-content-center " role="document">
                  <div class="modal-content border-0 mx-3">
                    <div class="modal-body p-0">
                      <div class="row justify-content-center">
                        <div class="col-auto">
                          <div class="card">
                            <div class="card-header  pb-0 border-0">
                            </div>
                            <div class="card-body pt-0">
                              <div class="row justify-content-center text-center">
                                <div class="col">
                                  <h2 class="mb-2 pt-1"><b>Welcome Back Login below.</b></h2>
                                </div>
                              </div>
                              <div class="row justify-content-center my-1">
                                <div class="col-12">
                                  <p class="text-center">We'll send you welcome emails and never post.</p>
                                </div>
                              </div>
                              <div class="row justify-content-center pt-1">
                                <a href="#"> <button type="button" class="btn btn-icon  text-left "><span><img
                                        src="https://img.icons8.com/color/48/000000/google-logo.png"
                                        class="img-fluid mr-1" width="35"></span> </button></a>
                                <a href="#"> <button type="button" class="btn btn-icon  text-left "><span><img
                                        src="https://i.imgur.com/URmkevm.png" class="img-fluid mr-1"
                                        width="35"></span> </button> </a>
                                <a href="#"> <button type="button" class="btn btn-icon  text-left "><span><img
                                        src="https://img.icons8.com/ios-filled/50/000000/mac-os.png"
                                        class="img-fluid mr-1" width="35"></span> </button></a>
                              </div>
                              <div class="row ">
                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 pt-4">
                                  <ul class="nav nav-pills mb-1 justify-content-center registry " id="pills-tab"
                                    role="tablist">
                                    <li class="nav-item text-center">
                                      <a class="nav-link active" id="pills-home-tab" data-toggle="pill"
                                        href="#pills-homemy" role="tab" aria-controls="pills-home"
                                        aria-selected="true">Signup</a>
                                    </li>
                                    <li class="nav-item text-center">
                                      <a class="nav-link" id="pills-profile-tab" data-toggle="pill"
                                        href="#pills-profilemy" role="tab" aria-controls="pills-profile"
                                        aria-selected="false">Login</a>
                                    </li>
                                  </ul>
                                  <div class="tab-content signi" id="pills-tabContent">
                                    <div class="tab-pane fade show active" id="pills-homemy" role="tabpanel"
                                      aria-labelledby="pills-home-tab">
                                      <div class="container">
                                        <div class="row justify-content-center">
                                          <div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 col-12 ">
                                            <form>
                                              <div class="form-group">
                                                 
                                                <div class="searchformfld">
                                                  <input type="text" class="candidateName" id="candidateName" name="candidateName" placeholder=" "/>
                                                  <label for="candidateName">Your name</label>
                                              </div>

                                              <div class="searchformfld formfld">
                                                <input type="email" class="email"  placeholder=" "/>
                                                <label for="candidateName">Email</label>
                                            </div>
                                            <div class="searchformfld formfld">
                                              <input type="password" class="password" placeholder=" "/>
                                              <label for="candidateName">Password</label>
                                          </div>
                                         
                                                
                                                <input type="checkbox" class="form-check-input" id="exampleCheck1">
                                                <label class="form-check-label pl-4" for="exampleCheck1">Receive
                                                  welcome emails</label>
                                              </div>
                                            </form>
                                            <div class="row text-center">
                                              <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12  ">
                                                <button type="button"
                                                  class="btn btn-primary btn-lg signbtn mt-1">Signup</button>
                                              </div>
                                            </div>


                                            <div class="row">

                                              <p class="text-center pt-1">By creating an account you consent to having
                                                your data <br>collected to be used for generating recommendations,
                                                market research,<br> and improving the service
                                                <br> Learn more: Privacy | Terms of Service
                                              </p>

                                            </div>


                                          </div>
                                        </div>
                                      </div>

                                    </div>
                                    <div class="tab-pane fade" id="pills-profilemy" role="tabpanel"
                                      aria-labelledby="pills-profile-tab">
                                      <div class="row justify-content-center">
                                        <div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 col-12 ">

                                          <form>
                                            <div class="searchformfld formfld">
                                              <input type="email" class="email"  placeholder=" "/>
                                              <label for="candidateName">Email</label>
                                          </div>
                                          <div class="searchformfld formfld">
                                            <input type="password" class="password" placeholder=" "/>
                                            <label for="candidateName">Password</label>
                                        </div>
                                          </form>
                                          <div class="row text-center">
                                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12  ">
                                              <button type="button"
                                                class="btn btn-primary btn-lg signbtn mt-3">Login</button>
                                            </div>
                                          </div>

                                          <div class="row justify-content-center pt-2">

                                            <a href="#">Forgot Password?</a>

                                          </div>

                                        </div>
                                      </div>

                                    </div>

                                  </div>
                                </div>
                              </div>

                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

          </li>
          <li class="list-inline-item">
            <ul class="nav ">
              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle bg-white" href="#" id="navbarDropdown" role="button"
                  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  Theme Option
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                  <a class="dropdown-item" href="#">Dark</a>
                  <a class="dropdown-item" href="#">White</a>
                </div>
              </li>
            </ul>
          </li>
        </ul>
      </div>
    </div>
  </div>

  <!-- feedback Modal -->
  <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel"></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body modell">
          <div class="container">
            <div class="row">
              <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 pb-4 ">
                <h2 class="input">We Want Your Input!</h2>
                <form>
                  <div class="form-group">
                    <label for="exampleFormControlTextarea1"></label>
                    <textarea class="form-control " id="exampleFormControlTextarea1"
                      placeholder="Questions, bug reports, missing or wrong data, praise, feature requests — help us improve!"
                      rows="3"></textarea>
                  </div>
                  <div class="form-group">
                    <div class="md-form mb-0">
                      <label for="name" class="">Your name</label>
                      <input type="text" id="name" name="name" class="form-control">
                    </div>

                    <label for="exampleInputEmail1">Email address</label>
                    <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">

                  </div>

                </form>
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 pl-0 pt-3">


                  <p class="note">Note: Without giving us an email we can't reply to you!</p>
                </div>
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 text-right pt-3">
                  <button type="button" class="btn btn-secondary btn-lg sub">
                    submit
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- feedback Modal end -->

</section>

<section class="nav_bar bg-white">
  <div class="container">
    <nav class="navbar navbar-expand-lg navbar-light bg-white mb-0">
      <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
        <a href="index.html"> <img src="images/khojlogo.png" alt=""></a>
        <button class="navbar-toggler float-right" type="button" data-toggle="collapse"
          data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
          aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
      </div>
      <div class="col-xl-9 col-lg-9 col-md-9 col-sm-12 col-12 ">
        <div class="collapse navbar-collapse" id="navbarSupportedContent">

          <ul class="navbar-nav ml-auto mr-auto">
            <li class="nav-item  active px-2 font-weight-bold">

              <a class="nav-link" href="index.html">Home <span class="sr-only">(current)</span></a>
            </li>

            <li class="nav-item dropdown font-weight-bold">
              <a class="nav-link dropdown-toggle" href="movies.html"  data-bs-toggle="dropdown">
                Movies
              </a>
              <div class="dropdown-menu Drop_first" aria-labelledby="navbarDropdown">
                <div class="container-fluid px-5">
                  <div class="row">
                    <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12 border-right">
                      <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                        <h3 class="text-dark font-weight-bold py-3">Movie of the day</h3>
                        <img src="images/dropimage.jpg" class="img-fluid" width="200" alt="">
                      </div>
                      <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                        <h5 class="text-dark mt-5 pt-5">2017,Adventure comedy,Roman..</h5>
                        <h4 class="text-dark font-weight-bold pt-4">The Big Sick</h4>
                        <h5 class="text-dark  pt-2"> Pakistan-born comedian Kumail Nanjiani and grad student Emily Gardner fall in love but
                          struggle as their cultures clash. When Emily contracts a mysterious illness,</h5>
                       <button class="btn btn-link font-weight-bold mt-4 pl-0"><h4>Watch Now</h4></button>
                       <button class="btn font-weight-bold bg-light mt-4 pl-0"><h4>+ PLaylist</h4></button>
                      </div>

                    </div>
                    <div class="col-xl-2 col-lg-2 col-md-6 col-sm-6 col-12">
                      <h3 class="text-dark font-weight-bold py-3">Movie Genre</h3>
                      <ul class="pl-0">
<li class="list-unstyled"><a href ="#" class="text-dark " >Action</a></li>
<li class="list-unstyled"><a href ="" class="text-dark">Adventure</a></li>
<li class="list-unstyled"><a href ="" class="text-dark">Comedy</a></li>
<li class="list-unstyled"><a href ="" class="text-dark">Drama</a></li>
<li class="list-unstyled"><a href ="" class="text-dark">Sci-Fy</a></li>
<li class="list-unstyled"><a href ="" class="text-dark">Thriller</a></li>
<li class="list-unstyled"><a href ="" class="text-dark">Biography</a></li>
<li class="list-unstyled"><a href ="" class="text-dark">Family</a></li>
<li class="list-unstyled"><a href ="" class="text-dark">Romance</a></li>

                      </ul>
                    </div>
                    <div class="col-xl-2 col-lg-2 col-md-6 col-sm-6 col-12 border-right">
                      <ul class="pt-5 pl-0 mt-3">
                        <li class="list-unstyled"><a href ="#" class="text-dark" >Animation</a></li>
                        <li class="list-unstyled"><a href ="" class="text-dark">adventure</a></li>
                        <li class="list-unstyled"><a href ="" class="text-dark">Comedy</a></li>
                        <li class="list-unstyled"><a href ="" class="text-dark">Drama</a></li>
                        <li class="list-unstyled"><a href ="" class="text-dark">Sci-Fy</a></li>
                        <li class="list-unstyled"><a href ="" class="text-dark">Thriller</a></li>
                       
                        
                                                </ul>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12">
                      <h3 class="text-dark font-weight-bold py-3">Movie Genre</h3>
                      <div id="carouselExampleControlsdrop2" class="carousel slide " data-ride="carousel">
                        <div class="carousel-inner">
                          <div class="carousel-item active">
                            <img class="img-fluid" src="images/qqqq.jpg" alt="">
                          </div>
                          <div class="carousel-item">
                            <img class="img-fluid" src="images/qqqq2.jpg" alt="">
                          </div>
                          <div class="carousel-item">
                            <img class="img-fluid" src="images/qqqq3.jpg" alt="">
                          </div>
                        </div>
                        <a class="carousel-control-prev" href="#carouselExampleControlsdrop2" role="button" data-slide="prev">
                          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                          <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselExampleControlsdrop2" role="button" data-slide="next">
                          <span class="carousel-control-next-icon" aria-hidden="true"></span>
                          <span class="sr-only">Next</span>
                        </a>
                      </div>
                    </div>
                  </div>
                </div>

              </div>
            </li>
            <li class="nav-item px-2  dropdown font-weight-bold">
              <a class="nav-link dropdown-toggle"  href="tvseries.html"  data-bs-toggle="dropdown">
                Tv-series
              </a>
              <div class="dropdown-menu Second_drop" aria-labelledby="navbarDropdown22">
                <div class="container-fluid px-5">
                  <div class="row">
                    <div class="col-xl-3 col-lg-3 col-md-12 col-sm-12 col-12 border-right">
                      <ul class="pl-0 pt-5 mt-2">
                        <li class="list-unstyled"><a href ="#" class="text-dark " >Trending Tv Shows</a></li>
                        <li class="list-unstyled"><a href ="" class="text-dark">Popular Now</a></li>
                        <li class="list-unstyled"><a href ="" class="text-dark">New This Month</a></li>
                        <li class="list-unstyled"><a href ="" class="text-dark">Khoj Exclusive</a></li>
                        <li class="list-unstyled"><a href ="" class="text-dark">Just For Kids</a></li>
                        <li class="list-unstyled"><a href ="" class="text-dark">Featured Shows</a></li>
                        <li class="list-unstyled"><a href ="" class="text-dark">Tv Shows Genre</a></li>
                        <li class="list-unstyled"><a href ="" class="text-dark">Tv Series:Genre</a></li>
                        <li class="list-unstyled"><a href ="" class="text-dark">Tv Series:Drama</a></li>
                        
                                                </ul>

                    </div>

                    <div class="col-xl-9 col-lg-9 col-md-12 col-sm-12 col-12">
                      <h3 class="text-dark font-weight-bold py-3">Movie Genre</h3>
                      <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                        <ol class="carousel-indicators">
                          <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                          <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                         
                        </ol>
                        <div class="carousel-inner">
                          <div class="carousel-item active">
                          
                            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-3">
                              <img class="img-fluid" src="images/qqqq.jpg" alt="">
                              <h4 class="pt-4">Action,Drama,2015-2016</h4>
                              <h4 class="font-weight-bold">Chicago med</h4>
                              </div>

                              <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-3">
                                <img class="img-fluid" src="images/qqqq2.jpg" alt="">
                                <h4 class="pt-4">Action,Drama,2015-2016</h4>
                                <h4 class="font-weight-bold">Chicago med</h4>
                              </div>
                              <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-3">
                                <img class="img-fluid" src="images/qqqq3.jpg" alt="">
                                <h4 class="pt-4">Action,Drama,2015-2016</h4>
                                <h4 class="font-weight-bold">Chicago med</h4>
                              </div>
                              <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-3">
                                <img class="img-fluid" src="images/qqqq.jpg" alt="">
                                <h4 class="pt-4">Action,Drama,2015-2016</h4>
                                <h4 class="font-weight-bold">Chicago med</h4>
                              </div>

                          </div>
                          <div class="carousel-item">
                            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-3">
                              <img class="img-fluid" src="images/qqqq.jpg" alt="">
                              <h4 class="pt-4">Action,Drama,2015-2016</h4>
                              <h4 class="font-weight-bold">Chicago med</h4>
                              </div>

                              <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-3">
                                <img class="img-fluid" src="images/qqqq2.jpg" alt="">
                                <h4 class="pt-4">Action,Drama,2015-2016</h4>
                                <h4 class="font-weight-bold">Chicago med</h4>
                              </div>
                              <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-3">
                                <img class="img-fluid" src="images/qqqq3.jpg" alt="">
                                <h4 class="pt-4">Action,Drama,2015-2016</h4>
                                <h4 class="font-weight-bold">Chicago med</h4>
                              </div>
                              <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-3">
                                <img class="img-fluid" src="images/qqqq.jpg" alt="">
                                <h4 class="pt-4">Action,Drama,2015-2016</h4>
                                <h4 class="font-weight-bold">Chicago med</h4>
                              </div>
                          </div>
                         
                        </div>
                        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                          <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                          <span class="carousel-control-next-icon" aria-hidden="true"></span>
                          <span class="sr-only">Next</span>
                        </a>
                      </div>
                    </div>
                  </div>
                </div>

              </div>
            </li>


            <li class="nav-item px-3 font-weight-bold">
              <a class="nav-link" href="free.html">Free</a>
            </li>
            <li class="nav-item px-3 font-weight-bold">
              <a class="nav-link" href="blog.html">Blog</a>
            </li>
          </ul>
          <form autocomplete="off" action="/action_page.php" style="max-width:450px;">
            <div class="input-icons autocomplete">
              <i class="fas fa-search icon">
              </i>
              <input id="myInput" class="pl-3" type="text" name="myCountry" placeholder="Search">
            </div>
          </form>
        </div>
      </div>
    </nav>

  </div>
</section>
    <section class="first1">
        <div class="container">
            <h3 class="pt-4 pb-4">Home &nbsp;> Movies</h3>
        </div>
    </section>
    
    <section class="banner_movie">
        <div class="container-fluid">


            <div class="container ">
                <div class="row">
                    <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12 colum">
                        <img src="images/girl.jpg" style="width: 100% ; " alt="...">
                        <div class="row back mx-0 py-3 ">

                            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4 main">
                                <a class="nav-link " href="#"> <i class="fas fa-plus"></i> <br>Want to see</a>
                            </div>
                            <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-2 main ">
                                <a class="nav-link active  Text-white" href="#"><i class="fas fa-eye"></i><br> Seen</a>
                            </div>
                            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-3 main">
                                <a class="nav-link active  Text-white" href="#"><i class="far fa-thumbs-up"></i><br>
                                    Like</a>
                            </div>
                            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-3 main">
                                <a class="nav-link active  Text-white" href="#"><i
                                        class="fas fa-thumbs-down"></i><br>Dislike</a>
                            </div>
                        </div>
                        <div class="row  mx-0 my-4  back2">
                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                <a class="nav-link text-center py-3 " href="#"> Watch the Trailer</a>

                            </div>
                        </div>
                        <div class="row  mx-0 my-4  back2">
                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                <a class="nav-link text-center py-3 " href="#"> Sign in to sync Watchlist</a>

                            </div>
                        </div>
                        <div class="row  my-5">
                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                <img src="images/add3.jpg" style="width: 100% ; " alt="...">

                            </div>
                        </div>

                    </div>
                    <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1 col-1 ">
                        &nbsp;
                    </div>
                    <div class="col-xl-7 col-lg-7 col-md-12 col-sm-12 col-12 colum ">
                        <div class="container">
                            <div class="row">
                                <h1 class="font-weight-bold">Avengers</h1>
                            </div>
                            <div class="row pt-4">
                                <img src="images/khojlogo.png" class="pt-2" style="width: 15%; height: 100%; "
                                    alt="...">
                                &nbsp;&nbsp;&nbsp; &nbsp;
                                <img src="images/rating.png" class="pt-2" style="width: 18% ;height: 10%; " alt="...">
                                &nbsp;&nbsp;&nbsp; &nbsp;
                                <h3 class="font-weight-bold">5.0<br>8 vote</h3>
                                &nbsp;&nbsp;&nbsp; &nbsp;
                                
                            </div>
                            <div class="row pt-4">
                              <button type="button"
                              class="btn btn-primary btn-lg text-center blue1 mr-1 px-4">2004</button>
                          <button type="button"
                              class="btn btn-primary btn-lg text-center blue mr-1 px-4">English</button>
                          <button type="button" class="btn btn-primary btn-lg text-center  blue mr-1 px-4">1hr 30min</button>
                          <button type="button" class="btn btn-primary btn-lg text-center blue2 mr-1 px-5"><i
                                  class="fab fa-imdb fa-2x pt-2"></i> &nbsp;<p class="pt-3">8.0</p></button>

                            </div>
                            
                              <div id="module" class="container pt-4 pl-0">
                                <h2 class="font-weight-bold">Avengers</h2>
                                <p class="collapse pt-4" id="collapseExample" aria-expanded="false">
                                  Bacon ipsum dolor amet doner picanha tri-tip biltong leberkas salami meatball tongue filet mignon landjaeger tail. Kielbasa salami tenderloin picanha spare ribs, beef ribs strip steak jerky cow. Pork chop chicken ham hock beef ribs turkey jerky. Shoulder
                                  beef capicola doner, tongue tail sausage short ribs andouille. Rump frankfurter landjaeger t-bone, kielbasa doner ham hock shankle venison. Cupim capicola kielbasa t-bone, ball tip chicken andouille venison pork chop doner bacon beef ribs kevin shankle.
                                  Short loin leberkas tenderloin ground round shank, brisket strip steak ham hock ham.
                                </p>
                                <a role="button" class="collapsed pt-4" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample"></a>
                              </div>
                              
                              
                           
<div class="row pt-3">
    <h2 class="font-weight-bold">Genre :</h2>
    <a class="nav-link  " href="#"> Horror ,</a>
    <a class="nav-link  " href="#"> Romantic ,</a>
    <a class="nav-link  " href="#"> Action</a>
</div>
<div class="row pt-3">
    <h2 class="font-weight-bold">Tags</h2>
    </div>
    <div class="row ">
        <button type="button"
        class="btn btn-primary btn-lg text-center blu mr-2 mt-3 py-3 px-4">American Movies</button> 
        <button type="button"
                                    class="btn btn-primary btn-lg text-center py-3 blu mr-2 mt-3 px-4">Avenger 2012 Film</button>
                                    <button type="button"
                                    class="btn btn-primary btn-lg text-center py-3 blu mr-2 mt-3 px-4">English Language Films</button>
                                    <button type="button"
                                    class="btn btn-primary btn-lg text-center py-3 blu mr-2 mt-3 px-4">Film About Mind Controls</button>
    
</div>
<div class="row pt-4">
    <h2 class="font-weight-bold">Where to Watch Avengers</h2>
    <ul class="nav nav-pills watch  mb-3" id="pills-tab" role="tablist">
        <li class="nav-item mt-4">
          <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true"><strong>Buy</strong></a>
        </li>
        <li class="nav-item mt-4">
          <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false"><strong>Stream</strong></a>
        </li>
        <li class="nav-item mt-4">
          
            <a class="nav-link " id="pills-contact-tab" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-toggle="pill" href="#pills-contact" role="tab" aria-controls="pills-contact" aria-selected="false"><img src="images/america.jpg"  style="width: 100%; "
              alt="..."></a>
            <div class="dropdown-menu drop_country" aria-labelledby="dropdownMenuButton">
              <a class="dropdown-item" href="#"><img src="images/america.jpg"  style="width: 80%; "
                alt="..."></a>
              <a class="dropdown-item" href="#"><img src="images/canada.jpg"  style="width: 80%; "
                alt="..."></a>
              <a class="dropdown-item" href="#"><img src="images/2560px-Flag_of_Australia.svg.png"  style="width: 80%; "
                alt="..."></a>
                <a class="dropdown-item" href="#"><img src="images/download.png"  style="width: 80%; "
                  alt="..."></a>
                  <a class="dropdown-item" href="#"><img src="images/america.jpg"  style="width: 80%; "
                    alt="..."></a>
                  <a class="dropdown-item" href="#"><img src="images/canada.jpg"  style="width: 80%; "
                    alt="..."></a>
                  <a class="dropdown-item" href="#"><img src="images/2560px-Flag_of_Australia.svg.png"  style="width: 80%; "
                    alt="..."></a>
                    <a class="dropdown-item" href="#"><img src="images/download.png"  style="width: 80%; "
                      alt="..."></a>
                      <a class="dropdown-item" href="#"><img src="images/america.jpg"  style="width: 80%; "
                        alt="..."></a>
                      <a class="dropdown-item" href="#"><img src="images/canada.jpg"  style="width: 80%; "
                        alt="..."></a>
                      <a class="dropdown-item" href="#"><img src="images/2560px-Flag_of_Australia.svg.png"  style="width: 80%; "
                        alt="..."></a>
                        <a class="dropdown-item" href="#"><img src="images/download.png"  style="width: 80%; "
                          alt="..."></a>
                          <a class="dropdown-item" href="#"><img src="images/america.jpg"  style="width: 80%; "
                            alt="..."></a>
                          <a class="dropdown-item" href="#"><img src="images/canada.jpg"  style="width: 80%; "
                            alt="..."></a>
                          <a class="dropdown-item" href="#"><img src="images/2560px-Flag_of_Australia.svg.png"  style="width: 80%; "
                            alt="..."></a>
                            <a class="dropdown-item" href="#"><img src="images/download.png"  style="width: 80%; "
                              alt="..."></a>
            </div>
   
          
        </li>
        
      </ul></div>
      <div class="row ">
      <div class="tab-content" id="pills-tabContent">
        <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
<div class="row">
    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4 pt-4 ">
        <img src="images/primeicon.png"  style="width: 100%; "
        alt="...">
      <h4 class="text-white rupee pt-3"> <i class="fas fa-rupee-sign"></i> 129 per Month</h4>
      <a class="nav-link rupee2 " href="#"> Watch Now</a> 
            </div>
        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4 pt-4">
            <img src="images/alticon.png"  style="width: 100%; "
            alt="...">
            <h4 class="text-white rupee pt-3"> <i class="fas fa-rupee-sign"></i> 155 per Month</h4>
            <a class="nav-link rupee2 " href="#"> Watch Now</a> 
        </div>
        
</div>


        </div>
        <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
          <div class="row">
            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4 pt-4 ">
                <img src="images/ulluicon.png"  style="width: 100%; "
                alt="...">
              <h4 class="text-white rupee pt-3"> <i class="fas fa-rupee-sign"></i> 88 per Month</h4>
              <a class="nav-link rupee2 " href="#"> Watch Now</a> 
                    </div>
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4 pt-4">
                    <img src="images/vooticon.png"  style="width: 100%; "
                    alt="...">
                    <h4 class="text-white rupee pt-3"> <i class="fas fa-rupee-sign"></i> 59 per Month</h4>
                    <a class="nav-link rupee2 " href="#"> Watch Now</a> 
                </div>
                
        </div></div>
       
    
      </div></div>
   
          <div class="row pt-4">
                <h3 class="text-white avenger">
                Robert Downey Jr. , Chris Evans as Steve Rogers / Captain America , Mark Ruffalo as Bruce Banner / Hulk ,  Chris 
Hemsworth as Thor , Jeremy Renner as Clint Barton / Hawkeye</h3>
           
          </div>
          <div class="row mt-4 mb-5 pt-3 pb-3 lastline">
            <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12  ">
<h4 class="text-white font-weight-bold"> Avenger is streaming in the U.S.</h4>
<h4 class="text-white">Change region with ExpressVPN to watch.</h4>
</div>
<div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12  ">
<button type="button"
        class="btn btn-primary btn-lg text-center font-weight-bold unlock  py-3 px-5">Unlock With Express VPN</button> 
</div>
    </div>
    </div>

                        </div>
                    </div>
                </div>


            </div>
        </div>
    </section>

<section>
<div class="container px-5">
<div class="row pt-5">
  <h1 class="text-white font-weight-bold">Avengers</h1>
</div>

<div class="row pt-3">

<p class="text-white">Bacon ipsum dolor amet doner picanha tri-tip biltong leberkas salami meatball tongue filet mignon landjaeger tail. Kielbasa salami tenderloin picanha spare ribs, beef ribs strip steak jerky cow. Pork chop chicken ham hock beef ribs turkey jerky. Shoulder beef capicola doner, tongue tail sausage short ribs andouille. Rump frankfurter landjaeger t-bone, kielbasa doner ham hock shankle venison. Cupim capicola kielbasa t-bone, ball tip chicken andouille venison pork chop doner bacon beef ribs kevin shankle. Short loin leberkas tenderloin ground round shank, brisket strip steak ham hock ham.</p>
</div>
</div>

</section>


<section class="eight">

<!-- The Modal start -->
<div class="modal" id="myModal1">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
       
        <button type="button" class="close text-dark" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        
        <h3 class="modal-title text-dark pt-2">A+  <img src="images/balls.png" class="pl-3  mb-0" style="width: 35%;height: 100%; " alt="..."></h3>
    
<div class="row">

  <h2 class="text-dark pl-4 pt-4">Positive Messages</h2>

</div>
<div class="row">

  <p class="text-dark pl-4 pt-5">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged </p>

</div>



      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
      </div>

    </div>
  </div>
</div>


<div class="modal" id="myModal2">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
       
        <button type="button" class="close text-dark" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        
        <h3 class="modal-title text-dark pt-2">A+  <img src="images/balls.png" class="pl-3  mb-0" style="width: 35%;height: 100%; " alt="..."></h3>
    
<div class="row">

  <h2 class="text-dark pl-4 pt-4">Positive Role Models & Representations</h2>

</div>
<div class="row">

  <p class="text-dark pl-4 pt-5">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged </p>

</div>



      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
      </div>

    </div>
  </div>
</div>

<div class="modal" id="myModal3">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
       
        <button type="button" class="close text-dark" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        
        <h3 class="modal-title text-dark pt-2">A+  <img src="images/balls.png" class="pl-3  mb-0" style="width: 35%;height: 100%; " alt="..."></h3>
    
<div class="row">

  <h2 class="text-dark pl-4 pt-4">Ease of Play</h2>

</div>
<div class="row">

  <p class="text-dark pl-4 pt-5">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged </p>

</div>



      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
      </div>

    </div>
  </div>
</div>

<div class="modal" id="myModal4">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
       
        <button type="button" class="close text-dark" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        
        <h3 class="modal-title text-dark pt-2">A+  <img src="images/balls.png" class="pl-3  mb-0" style="width: 35%;height: 100%; " alt="..."></h3>
    
<div class="row">

  <h2 class="text-dark pl-4 pt-4">Violence</h2>

</div>
<div class="row">

  <p class="text-dark pl-4 pt-5">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged </p>

</div>



      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
      </div>

    </div>
  </div>
</div>

<div class="modal" id="myModal5">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
       
        <button type="button" class="close text-dark" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        
        <h3 class="modal-title text-dark pt-2">A+  <img src="images/balls.png" class="pl-3  mb-0" style="width: 35%;height: 100%; " alt="..."></h3>
    
<div class="row">

  <h2 class="text-dark pl-4 pt-4">Sex</h2>

</div>
<div class="row">

  <p class="text-dark pl-4 pt-5">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged </p>

</div>



      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
      </div>

    </div>
  </div>
</div>
<div class="modal" id="myModal6">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
       
        <button type="button" class="close text-dark" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        
        <h3 class="modal-title text-dark pt-2">A+  <img src="images/balls.png" class="pl-3  mb-0" style="width: 35%;height: 100%; " alt="..."></h3>
    
<div class="row">

  <h2 class="text-dark pl-4 pt-4">Language</h2>

</div>
<div class="row">

  <p class="text-dark pl-4 pt-5">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged </p>

</div>



      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
      </div>

    </div>
  </div>
</div>

<div class="modal" id="myModal7">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
       
        <button type="button" class="close text-dark" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        
        <h3 class="modal-title text-dark pt-2">A+  <img src="images/balls.png" class="pl-3  mb-0" style="width: 35%;height: 100%; " alt="..."></h3>
    
<div class="row">

  <h2 class="text-dark pl-4 pt-4">Consumerism</h2>

</div>
<div class="row">

  <p class="text-dark pl-4 pt-5">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged </p>

</div>



      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
      </div>

    </div>
  </div>
</div>
<div class="modal" id="myModal8">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
       
        <button type="button" class="close text-dark" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        
        <h3 class="modal-title text-dark pt-2">A+  <img src="images/balls.png" class="pl-3  mb-0" style="width: 35%;height: 100%; " alt="..."></h3>
    
<div class="row">

  <h2 class="text-dark pl-4 pt-4">Drinking,Drugs & Smoking</h2>

</div>
<div class="row">

  <p class="text-dark pl-4 pt-5">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged </p>

</div>



      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
      </div>

    </div>
  </div>
</div>
<!-- The Modal end -->


  <div class="container px-5">
  <div class="row">
    
  <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12 inner mt-4 mr-2 ">
    <button class=" btn btn-link w-100" type="button" data-toggle="modal" data-target="#myModal1">
    <div class="row">
    <div class="col-xl-10 col-lg-10 col-md-10 col-sm-10 col-10 ">
      <div class="row">
      <i class="fas fa-envelope pl-4 pt-4 text-dark fa-2x"></i>
      <p class="present pl-3 pt-3 mb-0 ">not present</p></div>
      <p class="text-white para pt-2 text-left">Positive Message</p>
</div>
<div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-2 pt-4">
  <a class=" Text-white anchr" href="#"> <i class="fas fa-chevron-right"></i></a>
</div>

</div></button>
</div>
<div class="col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12 inner mt-4 mr-2 ">
  <button class=" btn btn-link w-100" type="button" data-toggle="modal" data-target="#myModal2">
    <div class="row">
    <div class="col-xl-10 col-lg-10 col-md-10 col-sm-10 col-10 ">
      <div class="row">
        <i class="fas fa-user-tag pl-4 pt-4 text-dark fa-2x"></i>
        <img src="images/balls.png" class="pl-3 pt-4 mb-0" style="width: 35%;height: 100%; " alt="..."></div>
      <p class="text-white para pt-2 text-left">Positive Role Models & Representations</p>
</div>
<div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-2 pt-4">
  <a class=" Text-white anchr" href="#"> <i class="fas fa-chevron-right"></i></a>
</div>

</div></button>
 
</div>

<div class="col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12 inner mt-4 mr-2 ">
  <button class=" btn btn-link w-100" type="button" data-toggle="modal" data-target="#myModal3">
    <div class="row">
    <div class="col-xl-10 col-lg-10 col-md-10 col-sm-10 col-10 ">
      <div class="row">
        <i class="fab fa-playstation pl-4 pt-4 text-dark fa-2x"></i>
      
        <img src="images/balls.png" class="pl-3 pt-4 mb-0" style="width: 35%;height: 100%; " alt="..."></div>
      <p class="text-white para pt-2 text-left">Ease of play</p>
</div>
<div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-2 pt-4">
  <a class=" Text-white anchr" href="#"> <i class="fas fa-chevron-right"></i></a>
</div>

</div></button>
 

</div>

<div class="col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12 inner mt-4 mr-2 ">
  <button class=" btn btn-link w-100" type="button" data-toggle="modal" data-target="#myModal4">
    <div class="row">
    <div class="col-xl-10 col-lg-10 col-md-10 col-sm-10 col-10 ">
      <div class="row">
        <i class="fas fa-bomb pl-4 pt-4 text-dark fa-2x"></i>
        
        <img src="images/balls.png" class="pl-3 pt-4 mb-0" style="width: 35%;height: 100%; " alt="..."></div>
      <p class="text-white para pt-2 text-left">Violence</p>
</div>
<div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-2 pt-4">
  <a class=" Text-white anchr" href="#"> <i class="fas fa-chevron-right"></i></a>
</div>
</div></button>
</div>

<div class="col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12 inner mt-4 mr-2 ">
  <button class=" btn btn-link w-100" type="button" data-toggle="modal" data-target="#myModal5">
    <div class="row">
    <div class="col-xl-10 col-lg-10 col-md-10 col-sm-10 col-10 ">
      <div class="row">
      
        <i class="fas fa-genderless pl-4 pt-4 text-dark fa-2x"></i>
        <img src="images/balls.png" class="pl-3 pt-4 mb-0" style="width: 35%;height: 100%; " alt="..."></div>
      <p class="text-white para pt-2 text-left">Sex</p>
</div>
<div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-2 pt-4">
  <a class=" Text-white anchr" href="#"> <i class="fas fa-chevron-right"></i></a>
</div>
</div></button>
 
</div>

<div class="col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12 inner mt-4 mr-2 ">
  <button class=" btn btn-link w-100" type="button" data-toggle="modal" data-target="#myModal6">
    <div class="row">
    <div class="col-xl-10 col-lg-10 col-md-10 col-sm-10 col-10 ">
      <div class="row">
        
        <i class="fas fa-language pl-4 pt-4 text-dark fa-2x"></i>
        <img src="images/balls.png" class="pl-3 pt-4 mb-0" style="width: 35%;height: 100%; " alt="..."></div>
      <p class="text-white para pt-2 text-left">Language</p>
</div>
<div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-2 pt-4">
  <a class=" Text-white anchr" href="#"> <i class="fas fa-chevron-right"></i></a>
</div>
</div></button>

</div>


<div class="col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12 inner mt-4 mr-2 ">
  <button class=" btn btn-link w-100" type="button" data-toggle="modal" data-target="#myModal7">
    <div class="row">
    <div class="col-xl-10 col-lg-10 col-md-10 col-sm-10 col-10 ">
      <div class="row">
        <i class="fas fa-people-carry pl-4 pt-4 text-dark fa-2x"></i>
        
        <img src="images/balls.png" class="pl-3 pt-4 mb-0" style="width: 35%;height: 100%; " alt="..."></div>
      <p class="text-white para pt-2 text-left">Consumerism</p>
</div>
<div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-2 pt-4">
  <a class=" Text-white anchr" href="#"> <i class="fas fa-chevron-right"></i></a>
</div>
</div></button>
 
</div>


<div class="col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12 inner mt-4 mr-2 ">
  <button class=" btn btn-link w-100" type="button" data-toggle="modal" data-target="#myModal8">
    <div class="row">
    <div class="col-xl-10 col-lg-10 col-md-10 col-sm-10 col-10 ">
      <div class="row">
        <i class="fas fa-wine-glass pl-4 pt-4 text-dark fa-2x"></i>
      
        <img src="images/balls.png" class="pl-3 pt-4 mb-0" style="width: 35%;height: 100%; " alt="..."></div>
      <p class="text-white para pt-2 text-left">Drinking,Drugs & Smoking</p>
</div>
<div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-2 pt-4">
  <a class=" Text-white anchr" href="#"> <i class="fas fa-chevron-right"></i></a>
</div>
</div></button>
  
</div>


</div>
</div>




</section>

    <section class="trailer ">
        <div class="container  pt-4 pb-5">
          <div class="container">
                <div class="row ">
                  <div class="col-xl-8 col-lg-8 col-md-12 col-sm-12 col-12">
                             <h2 class="pt-3 pb-3 font-weight-bold">Videos: Trailer</h2>
                      </div>
                      <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12">
                        <a class="carousel-control-prev " href="#carouselExampleControls20" role="button" data-slide="prev">
                          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                          <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next " href="#carouselExampleControls20" role="button" data-slide="next">
                          <span class="carousel-control-next-icon" aria-hidden="true"></span>
                          <span class="sr-only">Next</span>
                        </a>
                 </div>



                   </div>
                   </div>
                   <div class="container">
                       <div class="row pt-4 mt-4 popu">
                          <div id="carouselExampleControls20" class="carousel slide" data-ride="carousel" data-interval="false">
                            <div class="carousel-inner">
                              <div class="carousel-item active">
                                <div class="row ">
                                  
                                  <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12  landscape_img">
                                    <a href="#">
                              <img src="images/landscape1.jpg" class="img-fluid" alt="">
                              <div class="container button_playy">
                                <div class="row"><i class="far fa-play-circle fa-3x px-3 text-white" ></i>
                                <p class="font-weight-bold pt-2 pt-3 text-white">Gully Boy:</p>
                                <p class="font-weight-bold pt-3 text-white">Gully Boy</p>
                              </div>
                              </div></a>
                                  </div>  
                                 
                                  <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-4  none landscape_img">
                                    <a href="#"><img src="images/landscape2.jpg" class="img-fluid" alt="">
                                   <div class="container button_playy">
                                    <div class="row"><i class="far fa-play-circle fa-3x px-3 text-white" ></i>
                                    <p class="font-weight-bold pt-2 pt-3 text-white">Gully Boy:</p>
                                    <p class="font-weight-bold pt-3 text-white">Gully Boy</p>
                                  </div>
                                  </div>
                                </a>
                                  </div>
                                 
                                  <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4   dnone none landscape_img">
                                    <a href="#"><img src="images/landscape3.jpg" class="img-fluid" alt="">
                                    <div class="container button_playy">
                                      <div class="row"><i class="far fa-play-circle fa-3x px-3 text-white" ></i>
                                      <p class="font-weight-bold pt-2 pt-3 text-white">Gully Boy:</p>
                                      <p class="font-weight-bold pt-3 text-white">Gully Boy</p>
                                    </div>
                                    </div>
                                    </a>
                                  </div>
                                  
                                </div>
                              </div>
                              <div class="carousel-item">
                                <div class="row ">
                                  
                                  <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12  landscape_img">
                                    <a href="#">
                              <img src="images/landscape1.jpg" class="img-fluid" alt="">
                              <div class="container button_playy">
                                <div class="row"><i class="far fa-play-circle fa-3x px-3 text-white" ></i>
                                <p class="font-weight-bold pt-2 pt-3 text-white">Gully Boy:</p>
                                <p class="font-weight-bold pt-3 text-white">Gully Boy</p>
                              </div>
                              </div></a>
                                  </div>  
                                 
                                  <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-4  none landscape_img">
                                    <a href="#"><img src="images/landscape2.jpg" class="img-fluid" alt="">
                                   <div class="container button_playy">
                                    <div class="row"><i class="far fa-play-circle fa-3x px-3 text-white" ></i>
                                    <p class="font-weight-bold pt-2 pt-3 text-white">Gully Boy:</p>
                                    <p class="font-weight-bold pt-3 text-white">Gully Boy</p>
                                  </div>
                                  </div>
                                </a>
                                  </div>
                                 
                                  <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4   dnone none landscape_img">
                                    <a href="#"><img src="images/landscape3.jpg" class="img-fluid" alt="">
                                    <div class="container button_playy">
                                      <div class="row"><i class="far fa-play-circle fa-3x px-3 text-white" ></i>
                                      <p class="font-weight-bold pt-2 pt-3 text-white">Gully Boy:</p>
                                      <p class="font-weight-bold pt-3 text-white">Gully Boy</p>
                                    </div>
                                    </div>
                                    </a>
                                  </div>
                                  
                                </div>
                            
                              </div>
                            </div>
                          
                          </div>
                        </div>
                    </div>  
             </div>
   
    </section>

    <section class="watch">
        <div class="container pt-5 pb-5">
          <div class="container">
          <div class="row ">
            <div class="col-xl-8 col-lg-8 col-md-12 col-sm-12 col-12">
                       <h2 class="pt-3 pb-3 font-weight-bold">Watch</h2>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12">
                  <a class="carousel-control-prev " href="#carouselExampleControls21" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                  </a>
                  <a class="carousel-control-next " href="#carouselExampleControls21" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                  </a>
           </div>
</div>
          </div>
             <div class="container ">
                    <div class="row pt-4 mt-4">
                        <div id="carouselExampleControls21" class="carousel slide" data-ride="carousel" data-interval="false">
                            <div class="carousel-inner">
                              <div class="carousel-item active">
                                <div class="row ">
                                  <div class="col-xl-3 col-lg-3 col-md-3 col-sm-4 col-6 ">
                             <img src="images/animate.png" style="width: 100% ; " alt="..." class="animate">
                             <button type="button" class="btn btn-lg animatebtn">ANIMATION</button>

                                  </div>
                                  <div class="col-xl-3 col-lg-3 col-md-3 col-sm-4 col-6 ">
                                    <img src="images/action.png" style="width: 100% ; " alt="..." class="animate">
                             <button type="button" class="btn btn-lg animatebtn">ACTION</button>
                                  </div>
                                  <div class="col-xl-3 col-lg-3 col-md-3 col-sm-4 col-4 fnone">
                                    <img src="images/disney.png" style="width: 100% ; " alt="..." class="animate">
                                    <button type="button" class="btn btn-lg animatebtn">DISNEY</button>
                                  </div>
                                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-4 dnone none ">
                                  <img src="images/superhero.png" style="width: 100% ; " alt="..." class="animate">
                             <button type="button" class="btn btn-lg animatebtn">SUPERHERO</button>
                                  </div>
                                
                                </div>
                              </div>
                              <div class="carousel-item">
                                <div class="row ">
                                  <div class="col-xl-3 col-lg-3 col-md-3 col-sm-4 col-6 ">
                             <img src="images/animate.png" style="width: 100% ; " alt="..." class="animate">
                             <button type="button" class="btn btn-lg animatebtn">ANIMATION</button>

                                  </div>
                                  <div class="col-xl-3 col-lg-3 col-md-3 col-sm-4 col-6 ">
                                    <img src="images/action.png" style="width: 100% ; " alt="..." class="animate">
                             <button type="button" class="btn btn-lg animatebtn">ACTION</button>
                                  </div>
                                  <div class="col-xl-3 col-lg-3 col-md-3 col-sm-4 col-4 fnone">
                                    <img src="images/disney.png" style="width: 100% ; " alt="..." class="animate">
                                    <button type="button" class="btn btn-lg animatebtn">DISNEY</button>
                                  </div>
                                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-4 dnone none ">
                                  <img src="images/superhero.png" style="width: 100% ; " alt="..." class="animate">
                             <button type="button" class="btn btn-lg animatebtn">SUPERHERO</button>
                                  </div>
                                
                                </div>
                              </div>
                            </div>
                           
                          </div>
                    </div>
               </div>   
             </div>
   
    </section>

    <section class="people ">
        <div class="container pt-5 pb-5">
          <div class="container">
            <div class="row ">
              <div class="col-xl-8 col-lg-8 col-md-12 col-sm-12 col-12">
                         <h2 class="pt-3 pb-3 font-weight-bold">People also Liked</h2>
                  </div>
                  <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12">
                    <a class="carousel-control-prev " href="#carouselExampleControls22" role="button" data-slide="prev">
                      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                      <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next " href="#carouselExampleControls22" role="button" data-slide="next">
                      <span class="carousel-control-next-icon" aria-hidden="true"></span>
                      <span class="sr-only">Next</span>
                    </a>
             </div>

               </div>
               </div>
               <div class="container">
                <div class="row pt-4 mt-4 popu">
                   <div id="carouselExampleControls22" class="carousel slide" data-ride="carousel" data-interval="false">
                     <div class="carousel-inner">
                       <div class="carousel-item active">
                         <div class="row ">
       
                           <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6 col-12 hover2">
                             <a href="#"> <img src="images/img1.png" style="width: 100% ; " alt="..."></a>
                             <div class="position_effect">
                               <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i class="fas fa-check"></i></button>
                               <button type="button" class="btn btn-light css-ksiwc2 seentext">Seen it</button>
                             </div>
                            
         
                             <div class="position_effect2">
                               <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i class="fas fa-plus"></i></button>
                               <button type="button" class="btn btn-light css-ksiwc2 seentext">+ Want to see</button>
                             </div>
                             <div class="position_effect3">
                               <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i class="fas fa-star"></i></button>
                               <button type="button" class="btn btn-light css-ksiwc2 seentext"><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i></button>
                             </div>
                             <h4 class="pt-3 text-white">2017,Hindi ,Action</h4>
                       <p class="font-weight-bold text-white">Gully Boy</p>
                       <div class="overlay4">
                         <h1 class="pt-3 text font-weight-bold">Gully Boy</h1>
                   <h1 class="pt-3 text2 font-italic ">Khoj says</h1>
                     <img src="images/check.png"  class="text3" alt="..."><h1 class="pt-3 text4 ">Age 8+</h1><img src="images/rating.png"  class="text5" alt="...">
                     <h1 class="pt-3 text6 ">Maidaan is an upcoming Indian Hindi-language biographical sports</h1>
                       <h1 class="pt-3 text7 ">Full Expert views</h1>
                       <img src="images/play.png"  class="text8" alt="...">
                       <h1 class="pt-3 text9 ">Video Reviews</h1>
                                     </div>
                           </div>
                           <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6 col-4 none hover2">
                             <a href="#"> <img src="images/img2.png" style="width: 100%; " alt="..."></a>
                             <div class="position_effect">
                               <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i class="fas fa-check"></i></button>
                               <button type="button" class="btn btn-light css-ksiwc2 seentext">Seen it</button>
                             </div>
                            
         
                             <div class="position_effect2">
                               <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i class="fas fa-plus"></i></button>
                               <button type="button" class="btn btn-light css-ksiwc2 seentext">+ Want to see</button>
                             </div>
                             <div class="position_effect3">
                               <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i class="fas fa-star"></i></button>
                               <button type="button" class="btn btn-light css-ksiwc2 seentext"><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i></button>
                             </div>
                             <h4 class="pt-3 text-white">2017,Hindi ,Action</h4>
                       <p class="font-weight-bold text-white">Gully Boy</p>
                       <div class="overlay4">
                         <h1 class="pt-3 text font-weight-bold">Gully Boy</h1>
                   <h1 class="pt-3 text2 font-italic ">Khoj says</h1>
                     <img src="images/check.png"  class="text3" alt="..."><h1 class="pt-3 text4 ">Age 8+</h1><img src="images/rating.png"  class="text5" alt="...">
                     <h1 class="pt-3 text6 ">Maidaan is an upcoming Indian Hindi-language biographical sports</h1>
                       <h1 class="pt-3 text7 ">Full Expert views</h1>
                       <img src="images/play.png"  class="text8" alt="...">
                       <h1 class="pt-3 text9 ">Video Reviews</h1>
                                     </div>
                           </div>
                           <div class="col-xl-3 col-lg-3 col-md-4 col-sm-4 col-4 dnone none hover2">
                             <a href="#"> <img src="images/img3.png" style="width: 100%; " alt="..."></a>
                             <div class="position_effect">
                               <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i class="fas fa-check"></i></button>
                               <button type="button" class="btn btn-light css-ksiwc2 seentext">Seen it</button>
                             </div>
                            
         
                             <div class="position_effect2">
                               <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i class="fas fa-plus"></i></button>
                               <button type="button" class="btn btn-light css-ksiwc2 seentext">+ Want to see</button>
                             </div>
                             <div class="position_effect3">
                               <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i class="fas fa-star"></i></button>
                               <button type="button" class="btn btn-light css-ksiwc2 seentext"><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i></button>
                             </div>
                             <h4 class="pt-3 text-white">2017,Hindi ,Action</h4>
                       <p class="font-weight-bold text-white">Gully Boy</p>
                       <div class="overlay4">
                         <h1 class="pt-3 text font-weight-bold">Gully Boy</h1>
                   <h1 class="pt-3 text2 font-italic ">Khoj says</h1>
                     <img src="images/check.png"  class="text3" alt="..."><h1 class="pt-3 text4 ">Age 8+</h1><img src="images/rating.png"  class="text5" alt="...">
                     <h1 class="pt-3 text6 ">Maidaan is an upcoming Indian Hindi-language biographical sports</h1>
                       <h1 class="pt-3 text7 ">Full Expert views</h1>
                       <img src="images/play.png"  class="text8" alt="...">
                       <h1 class="pt-3 text9 ">Video Reviews</h1>
                                     </div>
                           </div>
                           <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-3 non none hover2">
                             <a href="#"> <img src="images/img1.png" style="width: 100% ; " alt="..."></a>
                             <div class="position_effect">
                               <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i class="fas fa-check"></i></button>
                               <button type="button" class="btn btn-light css-ksiwc2 seentext">Seen it</button>
                             </div>
                            
         
                             <div class="position_effect2">
                               <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i class="fas fa-plus"></i></button>
                               <button type="button" class="btn btn-light css-ksiwc2 seentext">+ Want to see</button>
                             </div>
                             <div class="position_effect3">
                               <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i class="fas fa-star"></i></button>
                               <button type="button" class="btn btn-light css-ksiwc2 seentext"><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i></button>
                             </div>
                             <h4 class="pt-3 text-white">2017,Hindi ,Action</h4>
                       <p class="font-weight-bold text-white">Gully Boy</p>
                       <div class="overlay4">
                         <h1 class="pt-3 text font-weight-bold">Gully Boy</h1>
                   <h1 class="pt-3 text2 font-italic ">Khoj says</h1>
                     <img src="images/check.png"  class="text3" alt="..."><h1 class="pt-3 text4 ">Age 8+</h1><img src="images/rating.png"  class="text5" alt="...">
                     <h1 class="pt-3 text6 ">Maidaan is an upcoming Indian Hindi-language biographical sports</h1>
                       <h1 class="pt-3 text7 ">Full Expert views</h1>
                       <img src="images/play.png"  class="text8" alt="...">
                       <h1 class="pt-3 text9 ">Video Reviews</h1>
                                     </div>
                           </div>
                         </div>
                       </div>
                       <div class="carousel-item">
                         <div class="row ">
       
                           <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6 col-12 hover2">
                             <a href="#"> <img src="images/img1.png" style="width: 100% ; " alt="..."></a>
                             <div class="position_effect">
                               <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i class="fas fa-check"></i></button>
                               <button type="button" class="btn btn-light css-ksiwc2 seentext">Seen it</button>
                             </div>
                            
         
                             <div class="position_effect2">
                               <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i class="fas fa-plus"></i></button>
                               <button type="button" class="btn btn-light css-ksiwc2 seentext">+ Want to see</button>
                             </div>
                             <div class="position_effect3">
                               <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i class="fas fa-star"></i></button>
                               <button type="button" class="btn btn-light css-ksiwc2 seentext"><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i></button>
                             </div>
                             <h4 class="pt-3 text-white">2017,Hindi ,Action</h4>
                       <p class="font-weight-bold text-white">Gully Boy</p>
                       <div class="overlay4">
                         <h1 class="pt-3 text font-weight-bold">Gully Boy</h1>
                   <h1 class="pt-3 text2 font-italic ">Khoj says</h1>
                     <img src="images/check.png"  class="text3" alt="..."><h1 class="pt-3 text4 ">Age 8+</h1><img src="images/rating.png"  class="text5" alt="...">
                     <h1 class="pt-3 text6 ">Maidaan is an upcoming Indian Hindi-language biographical sports</h1>
                       <h1 class="pt-3 text7 ">Full Expert views</h1>
                       <img src="images/play.png"  class="text8" alt="...">
                       <h1 class="pt-3 text9 ">Video Reviews</h1>
                                     </div>
                           </div>
                           <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6 col-4 none hover2">
                             <a href="#"> <img src="images/img2.png" style="width: 100%; " alt="..."></a>
                             <div class="position_effect">
                               <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i class="fas fa-check"></i></button>
                               <button type="button" class="btn btn-light css-ksiwc2 seentext">Seen it</button>
                             </div>
                            
         
                             <div class="position_effect2">
                               <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i class="fas fa-plus"></i></button>
                               <button type="button" class="btn btn-light css-ksiwc2 seentext">+ Want to see</button>
                             </div>
                             <div class="position_effect3">
                               <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i class="fas fa-star"></i></button>
                               <button type="button" class="btn btn-light css-ksiwc2 seentext"><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i></button>
                             </div>
                             <h4 class="pt-3 text-white">2017,Hindi ,Action</h4>
                       <p class="font-weight-bold text-white">Gully Boy</p>
                       <div class="overlay4">
                         <h1 class="pt-3 text font-weight-bold">Gully Boy</h1>
                   <h1 class="pt-3 text2 font-italic ">Khoj says</h1>
                     <img src="images/check.png"  class="text3" alt="..."><h1 class="pt-3 text4 ">Age 8+</h1><img src="images/rating.png"  class="text5" alt="...">
                     <h1 class="pt-3 text6 ">Maidaan is an upcoming Indian Hindi-language biographical sports</h1>
                       <h1 class="pt-3 text7 ">Full Expert views</h1>
                       <img src="images/play.png"  class="text8" alt="...">
                       <h1 class="pt-3 text9 ">Video Reviews</h1>
                                     </div>
                           </div>
                           <div class="col-xl-3 col-lg-3 col-md-4 col-sm-4 col-4 dnone none hover2">
                             <a href="#"> <img src="images/img3.png" style="width: 100%; " alt="..."></a>
                             <div class="position_effect">
                               <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i class="fas fa-check"></i></button>
                               <button type="button" class="btn btn-light css-ksiwc2 seentext">Seen it</button>
                             </div>
                            
         
                             <div class="position_effect2">
                               <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i class="fas fa-plus"></i></button>
                               <button type="button" class="btn btn-light css-ksiwc2 seentext">+ Want to see</button>
                             </div>
                             <div class="position_effect3">
                               <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i class="fas fa-star"></i></button>
                               <button type="button" class="btn btn-light css-ksiwc2 seentext"><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i></button>
                             </div>
                             <h4 class="pt-3 text-white">2017,Hindi ,Action</h4>
                       <p class="font-weight-bold text-white">Gully Boy</p>
                       <div class="overlay4">
                         <h1 class="pt-3 text font-weight-bold">Gully Boy</h1>
                   <h1 class="pt-3 text2 font-italic ">Khoj says</h1>
                     <img src="images/check.png"  class="text3" alt="..."><h1 class="pt-3 text4 ">Age 8+</h1><img src="images/rating.png"  class="text5" alt="...">
                     <h1 class="pt-3 text6 ">Maidaan is an upcoming Indian Hindi-language biographical sports</h1>
                       <h1 class="pt-3 text7 ">Full Expert views</h1>
                       <img src="images/play.png"  class="text8" alt="...">
                       <h1 class="pt-3 text9 ">Video Reviews</h1>
                                     </div>
                           </div>
                           <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-3 non none hover2">
                             <a href="#"> <img src="images/img1.png" style="width: 100% ; " alt="..."></a>
                             <div class="position_effect">
                               <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i class="fas fa-check"></i></button>
                               <button type="button" class="btn btn-light css-ksiwc2 seentext">Seen it</button>
                             </div>
                            
         
                             <div class="position_effect2">
                               <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i class="fas fa-plus"></i></button>
                               <button type="button" class="btn btn-light css-ksiwc2 seentext">+ Want to see</button>
                             </div>
                             <div class="position_effect3">
                               <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i class="fas fa-star"></i></button>
                               <button type="button" class="btn btn-light css-ksiwc2 seentext"><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i></button>
                             </div>
                             <h4 class="pt-3 text-white">2017,Hindi ,Action</h4>
                       <p class="font-weight-bold text-white">Gully Boy</p>
                       <div class="overlay4">
                         <h1 class="pt-3 text font-weight-bold">Gully Boy</h1>
                   <h1 class="pt-3 text2 font-italic ">Khoj says</h1>
                     <img src="images/check.png"  class="text3" alt="..."><h1 class="pt-3 text4 ">Age 8+</h1><img src="images/rating.png"  class="text5" alt="...">
                     <h1 class="pt-3 text6 ">Maidaan is an upcoming Indian Hindi-language biographical sports</h1>
                       <h1 class="pt-3 text7 ">Full Expert views</h1>
                       <img src="images/play.png"  class="text8" alt="...">
                       <h1 class="pt-3 text9 ">Video Reviews</h1>
                                     </div>
                           </div>
                         </div>
                       </div>
                     </div>
                   
                   </div>
                 </div>
             </div>  
                      
             </div>
   
    </section>

    <section class="ott ">
        <div class="container pt-5 pb-5">
          <div class="container">
            <div class="row ">
              <div class="col-xl-8 col-lg-8 col-md-12 col-sm-12 col-12">
                         <h2 class="pt-3 pb-3 font-weight-bold">More(OTT-Disney Movies)</h2>
                  </div>
                  <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12">
                    <a class="carousel-control-prev " href="#carouselExampleControlop" role="button" data-slide="prev">
                      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                      <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next " href="#carouselExampleControlsop" role="button" data-slide="next">
                      <span class="carousel-control-next-icon" aria-hidden="true"></span>
                      <span class="sr-only">Next</span>
                    </a>
             </div>



               </div>
               </div>
               <div class="container">
                <div class="row pt-4 mt-4 popu">
                   <div id="carouselExampleControlsop" class="carousel slide" data-ride="carousel" data-interval="false">
                     <div class="carousel-inner">
                       <div class="carousel-item active">
                         <div class="row ">
       
                           <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6 col-12 hover2">
                             <a href="#"> <img src="images/img1.png" style="width: 100% ; " alt="..."></a>
                             <div class="position_effect">
                               <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i class="fas fa-check"></i></button>
                               <button type="button" class="btn btn-light css-ksiwc2 seentext">Seen it</button>
                             </div>
                            
         
                             <div class="position_effect2">
                               <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i class="fas fa-plus"></i></button>
                               <button type="button" class="btn btn-light css-ksiwc2 seentext">+ Want to see</button>
                             </div>
                             <div class="position_effect3">
                               <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i class="fas fa-star"></i></button>
                               <button type="button" class="btn btn-light css-ksiwc2 seentext"><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i></button>
                             </div>
                             <h4 class="pt-3 text-white">2017,Hindi ,Action</h4>
                       <p class="font-weight-bold text-white">Gully Boy</p>
                       <div class="overlay4">
                         <h1 class="pt-3 text font-weight-bold">Gully Boy</h1>
                   <h1 class="pt-3 text2 font-italic ">Khoj says</h1>
                     <img src="images/check.png"  class="text3" alt="..."><h1 class="pt-3 text4 ">Age 8+</h1><img src="images/rating.png"  class="text5" alt="...">
                     <h1 class="pt-3 text6 ">Maidaan is an upcoming Indian Hindi-language biographical sports</h1>
                       <h1 class="pt-3 text7 ">Full Expert views</h1>
                       <img src="images/play.png"  class="text8" alt="...">
                       <h1 class="pt-3 text9 ">Video Reviews</h1>
                                     </div>
                           </div>
                           <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6 col-4 none hover2">
                             <a href="#"> <img src="images/img2.png" style="width: 100%; " alt="..."></a>
                             <div class="position_effect">
                               <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i class="fas fa-check"></i></button>
                               <button type="button" class="btn btn-light css-ksiwc2 seentext">Seen it</button>
                             </div>
                            
         
                             <div class="position_effect2">
                               <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i class="fas fa-plus"></i></button>
                               <button type="button" class="btn btn-light css-ksiwc2 seentext">+ Want to see</button>
                             </div>
                             <div class="position_effect3">
                               <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i class="fas fa-star"></i></button>
                               <button type="button" class="btn btn-light css-ksiwc2 seentext"><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i></button>
                             </div>
                             <h4 class="pt-3 text-white">2017,Hindi ,Action</h4>
                       <p class="font-weight-bold text-white">Gully Boy</p>
                       <div class="overlay4">
                         <h1 class="pt-3 text font-weight-bold">Gully Boy</h1>
                   <h1 class="pt-3 text2 font-italic ">Khoj says</h1>
                     <img src="images/check.png"  class="text3" alt="..."><h1 class="pt-3 text4 ">Age 8+</h1><img src="images/rating.png"  class="text5" alt="...">
                     <h1 class="pt-3 text6 ">Maidaan is an upcoming Indian Hindi-language biographical sports</h1>
                       <h1 class="pt-3 text7 ">Full Expert views</h1>
                       <img src="images/play.png"  class="text8" alt="...">
                       <h1 class="pt-3 text9 ">Video Reviews</h1>
                                     </div>
                           </div>
                           <div class="col-xl-3 col-lg-3 col-md-4 col-sm-4 col-4 dnone none hover2">
                             <a href="#"> <img src="images/img3.png" style="width: 100%; " alt="..."></a>
                             <div class="position_effect">
                               <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i class="fas fa-check"></i></button>
                               <button type="button" class="btn btn-light css-ksiwc2 seentext">Seen it</button>
                             </div>
                            
         
                             <div class="position_effect2">
                               <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i class="fas fa-plus"></i></button>
                               <button type="button" class="btn btn-light css-ksiwc2 seentext">+ Want to see</button>
                             </div>
                             <div class="position_effect3">
                               <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i class="fas fa-star"></i></button>
                               <button type="button" class="btn btn-light css-ksiwc2 seentext"><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i></button>
                             </div>
                             <h4 class="pt-3 text-white">2017,Hindi ,Action</h4>
                       <p class="font-weight-bold text-white">Gully Boy</p>
                       <div class="overlay4">
                         <h1 class="pt-3 text font-weight-bold">Gully Boy</h1>
                   <h1 class="pt-3 text2 font-italic ">Khoj says</h1>
                     <img src="images/check.png"  class="text3" alt="..."><h1 class="pt-3 text4 ">Age 8+</h1><img src="images/rating.png"  class="text5" alt="...">
                     <h1 class="pt-3 text6 ">Maidaan is an upcoming Indian Hindi-language biographical sports</h1>
                       <h1 class="pt-3 text7 ">Full Expert views</h1>
                       <img src="images/play.png"  class="text8" alt="...">
                       <h1 class="pt-3 text9 ">Video Reviews</h1>
                                     </div>
                           </div>
                           <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-3 non none hover2">
                             <a href="#"> <img src="images/img1.png" style="width: 100% ; " alt="..."></a>
                             <div class="position_effect">
                               <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i class="fas fa-check"></i></button>
                               <button type="button" class="btn btn-light css-ksiwc2 seentext">Seen it</button>
                             </div>
                            
         
                             <div class="position_effect2">
                               <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i class="fas fa-plus"></i></button>
                               <button type="button" class="btn btn-light css-ksiwc2 seentext">+ Want to see</button>
                             </div>
                             <div class="position_effect3">
                               <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i class="fas fa-star"></i></button>
                               <button type="button" class="btn btn-light css-ksiwc2 seentext"><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i></button>
                             </div>
                             <h4 class="pt-3 text-white">2017,Hindi ,Action</h4>
                       <p class="font-weight-bold text-white">Gully Boy</p>
                       <div class="overlay4">
                         <h1 class="pt-3 text font-weight-bold">Gully Boy</h1>
                   <h1 class="pt-3 text2 font-italic ">Khoj says</h1>
                     <img src="images/check.png"  class="text3" alt="..."><h1 class="pt-3 text4 ">Age 8+</h1><img src="images/rating.png"  class="text5" alt="...">
                     <h1 class="pt-3 text6 ">Maidaan is an upcoming Indian Hindi-language biographical sports</h1>
                       <h1 class="pt-3 text7 ">Full Expert views</h1>
                       <img src="images/play.png"  class="text8" alt="...">
                       <h1 class="pt-3 text9 ">Video Reviews</h1>
                                     </div>
                           </div>
                         </div>
                       </div>
                       <div class="carousel-item">
                         <div class="row ">
       
                           <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6 col-12 hover2">
                             <a href="#"> <img src="images/img1.png" style="width: 100% ; " alt="..."></a>
                             <div class="position_effect">
                               <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i class="fas fa-check"></i></button>
                               <button type="button" class="btn btn-light css-ksiwc2 seentext">Seen it</button>
                             </div>
                            
         
                             <div class="position_effect2">
                               <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i class="fas fa-plus"></i></button>
                               <button type="button" class="btn btn-light css-ksiwc2 seentext">+ Want to see</button>
                             </div>
                             <div class="position_effect3">
                               <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i class="fas fa-star"></i></button>
                               <button type="button" class="btn btn-light css-ksiwc2 seentext"><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i></button>
                             </div>
                             <h4 class="pt-3 text-white">2017,Hindi ,Action</h4>
                       <p class="font-weight-bold text-white">Gully Boy</p>
                       <div class="overlay4">
                         <h1 class="pt-3 text font-weight-bold">Gully Boy</h1>
                   <h1 class="pt-3 text2 font-italic ">Khoj says</h1>
                     <img src="images/check.png"  class="text3" alt="..."><h1 class="pt-3 text4 ">Age 8+</h1><img src="images/rating.png"  class="text5" alt="...">
                     <h1 class="pt-3 text6 ">Maidaan is an upcoming Indian Hindi-language biographical sports</h1>
                       <h1 class="pt-3 text7 ">Full Expert views</h1>
                       <img src="images/play.png"  class="text8" alt="...">
                       <h1 class="pt-3 text9 ">Video Reviews</h1>
                                     </div>
                           </div>
                           <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6 col-4 none hover2">
                             <a href="#"> <img src="images/img2.png" style="width: 100%; " alt="..."></a>
                             <div class="position_effect">
                               <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i class="fas fa-check"></i></button>
                               <button type="button" class="btn btn-light css-ksiwc2 seentext">Seen it</button>
                             </div>
                            
         
                             <div class="position_effect2">
                               <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i class="fas fa-plus"></i></button>
                               <button type="button" class="btn btn-light css-ksiwc2 seentext">+ Want to see</button>
                             </div>
                             <div class="position_effect3">
                               <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i class="fas fa-star"></i></button>
                               <button type="button" class="btn btn-light css-ksiwc2 seentext"><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i></button>
                             </div>
                             <h4 class="pt-3 text-white">2017,Hindi ,Action</h4>
                       <p class="font-weight-bold text-white">Gully Boy</p>
                       <div class="overlay4">
                         <h1 class="pt-3 text font-weight-bold">Gully Boy</h1>
                   <h1 class="pt-3 text2 font-italic ">Khoj says</h1>
                     <img src="images/check.png"  class="text3" alt="..."><h1 class="pt-3 text4 ">Age 8+</h1><img src="images/rating.png"  class="text5" alt="...">
                     <h1 class="pt-3 text6 ">Maidaan is an upcoming Indian Hindi-language biographical sports</h1>
                       <h1 class="pt-3 text7 ">Full Expert views</h1>
                       <img src="images/play.png"  class="text8" alt="...">
                       <h1 class="pt-3 text9 ">Video Reviews</h1>
                                     </div>
                           </div>
                           <div class="col-xl-3 col-lg-3 col-md-4 col-sm-4 col-4 dnone none hover2">
                             <a href="#"> <img src="images/img3.png" style="width: 100%; " alt="..."></a>
                             <div class="position_effect">
                               <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i class="fas fa-check"></i></button>
                               <button type="button" class="btn btn-light css-ksiwc2 seentext">Seen it</button>
                             </div>
                            
         
                             <div class="position_effect2">
                               <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i class="fas fa-plus"></i></button>
                               <button type="button" class="btn btn-light css-ksiwc2 seentext">+ Want to see</button>
                             </div>
                             <div class="position_effect3">
                               <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i class="fas fa-star"></i></button>
                               <button type="button" class="btn btn-light css-ksiwc2 seentext"><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i></button>
                             </div>
                             <h4 class="pt-3 text-white">2017,Hindi ,Action</h4>
                       <p class="font-weight-bold text-white">Gully Boy</p>
                       <div class="overlay4">
                         <h1 class="pt-3 text font-weight-bold">Gully Boy</h1>
                   <h1 class="pt-3 text2 font-italic ">Khoj says</h1>
                     <img src="images/check.png"  class="text3" alt="..."><h1 class="pt-3 text4 ">Age 8+</h1><img src="images/rating.png"  class="text5" alt="...">
                     <h1 class="pt-3 text6 ">Maidaan is an upcoming Indian Hindi-language biographical sports</h1>
                       <h1 class="pt-3 text7 ">Full Expert views</h1>
                       <img src="images/play.png"  class="text8" alt="...">
                       <h1 class="pt-3 text9 ">Video Reviews</h1>
                                     </div>
                           </div>
                           <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-3 non none hover2">
                             <a href="#"> <img src="images/img1.png" style="width: 100% ; " alt="..."></a>
                             <div class="position_effect">
                               <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i class="fas fa-check"></i></button>
                               <button type="button" class="btn btn-light css-ksiwc2 seentext">Seen it</button>
                             </div>
                            
         
                             <div class="position_effect2">
                               <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i class="fas fa-plus"></i></button>
                               <button type="button" class="btn btn-light css-ksiwc2 seentext">+ Want to see</button>
                             </div>
                             <div class="position_effect3">
                               <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i class="fas fa-star"></i></button>
                               <button type="button" class="btn btn-light css-ksiwc2 seentext"><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i></button>
                             </div>
                             <h4 class="pt-3 text-white">2017,Hindi ,Action</h4>
                       <p class="font-weight-bold text-white">Gully Boy</p>
                       <div class="overlay4">
                         <h1 class="pt-3 text font-weight-bold">Gully Boy</h1>
                   <h1 class="pt-3 text2 font-italic ">Khoj says</h1>
                     <img src="images/check.png"  class="text3" alt="..."><h1 class="pt-3 text4 ">Age 8+</h1><img src="images/rating.png"  class="text5" alt="...">
                     <h1 class="pt-3 text6 ">Maidaan is an upcoming Indian Hindi-language biographical sports</h1>
                       <h1 class="pt-3 text7 ">Full Expert views</h1>
                       <img src="images/play.png"  class="text8" alt="...">
                       <h1 class="pt-3 text9 ">Video Reviews</h1>
                                     </div>
                           </div>
                         </div>
                       </div>
                     </div>
                   
                   </div>
                 </div>
             </div>    
                      
             </div>
   
    </section>


    <footer class="footer ">
      <div class="container mt-5 pb-5 pt-5">
          <div class="row pt-5 first">
            <div class="container">
              <div class="col-xl-2 col-lg-2 col-md-12 col-sm-12 col-12 pb-5 ">
                  <img src="images/khojlogo.png" alt="...">

              </div>
              <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12">
                  &nbsp;
              </div>
              <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                  <div class="row">
                      <a class="nav-link " href="#"> <i class="fab fa-facebook-f"></i> &nbsp; facebook</a>
                      <a class="nav-link active  Text-white" href="#"><i class="fab fa-twitter"></i> &nbsp;
                          Twitter</a>
                      <a class="nav-link active  Text-white" href="#"><i class="fab fa-google-plus-g"></i> &nbsp;
                          Google+</a>
                      <a class="nav-link active  Text-white" href="#"><i class="fab fa-vimeo-v"></i> &nbsp; Vimeo</a>
                      <a class="nav-link active  Text-white" href="#"><i class="fas fa-rss"></i> &nbsp; RSS</a>
                  </div>
              </div>
          </div>
          </div>
          <div class="row pt-5">
              <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6 col-6">
                  <h2 class="font-weight-bold pt-5">Movie Categories</h2>
                  <div class="row pt-5">
                      <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                          <ul class="pl-0 lists">
                              <li> <a class="nav-link " href="#"> Action </a></li>
                              <li><a class="nav-link " href="#"> Adventure </a></li>
                              <li><a class="nav-link " href="#"> Animation </a></li>
                              <li><a class="nav-link " href="#"> Comedy </a></li>
                              <li><a class="nav-link " href="#"> Crime </a></li>
                      </div>
                      <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                          <ul class="pl-0 lists">
                              <li> <a class="nav-link " href="#">Drama </a></li>
                              <li><a class="nav-link " href="#"> Fantacy </a></li>
                              <li><a class="nav-link " href="#"> Horror</a></li>
                              <li><a class="nav-link " href="#"> Mystrey </a></li>
                              <li><a class="nav-link " href="#"> Romance </a></li>
                          </ul>
                      </div>
                  </div>
              </div>
              <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6 col-6">
                  <h2 class="font-weight-bold pt-5">TV Series</h2>
                  <div class="row pt-5">
                      <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                          <ul class="pl-0 lists">
                              <li><a class="nav-link " href="#">Valentine Day</a></li>
                              <li><a class="nav-link " href="#"> Underrated Comedies</a></li>
                              <li><a class="nav-link " href="#">Scary TV Series </a></li>
                              <li><a class="nav-link " href="#"> Best 2018 Documentaries </a></li>
                              <li><a class="nav-link " href="#"> Classic Shows </a></li>
                          </ul>
                      </div>
                      <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                          <ul class="pl-0 lists">
                              <li><a class="nav-link " href="#"> Big TV Premieres </a></li>
                              <li><a class="nav-link " href="#"> Reality TV Shows </a></li>
                              <li><a class="nav-link " href="#"> Original Shows</a></li>
                              <li><a class="nav-link " href="#"> Suprise of the Year Shows </a></li>
                          </ul>
                      </div>
                  </div>
              </div>
              <div class="col-xl-3 col-lg-3 col-md-12 col-sm-12 col-12 ml-auto">

                <div class="container">
                  <div class="row  ">
                      <h2 class="font-weight-bold  pt-5">Support</h2>
                  </div>
                  <div class="row pt-5 pl-0">
                      <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 pl-0 ">
                          <ul class="pl-0 lists">
                              <li> <a class="nav-link " href="#"> My Account</a></li>
                              <li><a class="nav-link " href="#"> FAQ</a></li>
                              <li><a class="nav-link " href="#">Watch on TV</a></li>
                              <li><a class="nav-link " href="#">Help Center</a></li>
                              <li><a class="nav-link " href="#"> Crime </a></li>
                          </ul>
                      </div>
                  </div>
                  </div>
              </div>
          </div>
      </div>
      <div class="container-fluid">
          <div class="container">
              <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                  <p class="text-white copy pt-5"> © 2021 KhojApp. All Rights Reserved.</p>
              </div>
              <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                  <p class="text-white copy text-right pt-5"> Privacy Policy</p>
              </div>
          </div>
      </div>
  </footer>

  <script>
    function myFunction() {
      var dots = document.getElementById("dots");
      var moreText = document.getElementById("more");
      var btnText = document.getElementById("myBtn");
    
      if (dots.style.display === "none") {
        dots.style.display = "inline";
        btnText.innerHTML = "Read more"; 
        moreText.style.display = "none";
      } else {
        dots.style.display = "none";
        btnText.innerHTML = "Read less"; 
        moreText.style.display = "inline";
      }
    }

// When the user clicks on <div>, open the popup
  function myFunction() {
    var popup = document.getElementById("myPopup");
    popup.classList.toggle("show");
  }



  function openCity(evt, cityName) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " active";
}



function autocomplete(inp, arr) {
  /*the autocomplete function takes two arguments,
  the text field element and an array of possible autocompleted values:*/
  var currentFocus;
  /*execute a function when someone writes in the text field:*/
  inp.addEventListener("input", function(e) {
      var a, b, i, val = this.value;
      /*close any already open lists of autocompleted values*/
      closeAllLists();
      if (!val) { return false;}
      currentFocus = -1;
      /*create a DIV element that will contain the items (values):*/
      a = document.createElement("DIV");
      a.setAttribute("id", this.id + "autocomplete-list");
      a.setAttribute("class", "autocomplete-items");
      /*append the DIV element as a child of the autocomplete container:*/
      this.parentNode.appendChild(a);
      /*for each item in the array...*/
      for (i = 0; i < arr.length; i++) {
        /*check if the item starts with the same letters as the text field value:*/
        if (arr[i].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
          /*create a DIV element for each matching element:*/
          b = document.createElement("DIV");
          /*make the matching letters bold:*/
          b.innerHTML = "<strong>" + arr[i].substr(0, val.length) + "</strong>";
          b.innerHTML += arr[i].substr(val.length);
          /*insert a input field that will hold the current array item's value:*/
          b.innerHTML += "<input type='hidden' value='" + arr[i] + "'>";
          /*execute a function when someone clicks on the item value (DIV element):*/
          b.addEventListener("click", function(e) {
              /*insert the value for the autocomplete text field:*/
              inp.value = this.getElementsByTagName("input")[0].value;
              /*close the list of autocompleted values,
              (or any other open lists of autocompleted values:*/
              closeAllLists();
          });
          a.appendChild(b);
        }
      }
  });
  /*execute a function presses a key on the keyboard:*/
  inp.addEventListener("keydown", function(e) {
      var x = document.getElementById(this.id + "autocomplete-list");
      if (x) x = x.getElementsByTagName("div");
      if (e.keyCode == 40) {
        /*If the arrow DOWN key is pressed,
        increase the currentFocus variable:*/
        currentFocus++;
        /*and and make the current item more visible:*/
        addActive(x);
      } else if (e.keyCode == 38) { //up
        /*If the arrow UP key is pressed,
        decrease the currentFocus variable:*/
        currentFocus--;
        /*and and make the current item more visible:*/
        addActive(x);
      } else if (e.keyCode == 13) {
        /*If the ENTER key is pressed, prevent the form from being submitted,*/
        e.preventDefault();
        if (currentFocus > -1) {
          /*and simulate a click on the "active" item:*/
          if (x) x[currentFocus].click();
        }
      }
  });
  function addActive(x) {
    /*a function to classify an item as "active":*/
    if (!x) return false;
    /*start by removing the "active" class on all items:*/
    removeActive(x);
    if (currentFocus >= x.length) currentFocus = 0;
    if (currentFocus < 0) currentFocus = (x.length - 1);
    /*add class "autocomplete-active":*/
    x[currentFocus].classList.add("autocomplete-active");
  }
  function removeActive(x) {
    /*a function to remove the "active" class from all autocomplete items:*/
    for (var i = 0; i < x.length; i++) {
      x[i].classList.remove("autocomplete-active");
    }
  }
  function closeAllLists(elmnt) {
    /*close all autocomplete lists in the document,
    except the one passed as an argument:*/
    var x = document.getElementsByClassName("autocomplete-items");
    for (var i = 0; i < x.length; i++) {
      if (elmnt != x[i] && elmnt != inp) {
        x[i].parentNode.removeChild(x[i]);
      }
    }
  }
  /*execute a function when someone clicks in the document:*/
  document.addEventListener("click", function (e) {
      closeAllLists(e.target);
  });
}

/*An array containing all the country names in the world:*/
var countries = ["Afghanistan","Albania","Algeria","Andorra","Angola","Anguilla","Antigua & Barbuda","Argentina","Armenia","Aruba","Australia","Austria","Azerbaijan","Bahamas","Bahrain","Bangladesh","Barbados","Belarus","Belgium","Belize","Benin","Bermuda","Bhutan","Bolivia","Bosnia & Herzegovina","Botswana","Brazil","British Virgin Islands","Brunei","Bulgaria","Burkina Faso","Burundi","Cambodia","Cameroon","Canada","Cape Verde","Cayman Islands","Central Arfrican Republic","Chad","Chile","China","Colombia","Congo","Cook Islands","Costa Rica","Cote D Ivoire","Croatia","Cuba","Curacao","Cyprus","Czech Republic","Denmark","Djibouti","Dominica","Dominican Republic","Ecuador","Egypt","El Salvador","Equatorial Guinea","Eritrea","Estonia","Ethiopia","Falkland Islands","Faroe Islands","Fiji","Finland","France","French Polynesia","French West Indies","Gabon","Gambia","Georgia","Germany","Ghana","Gibraltar","Greece","Greenland","Grenada","Guam","Guatemala","Guernsey","Guinea","Guinea Bissau","Guyana","Haiti","Honduras","Hong Kong","Hungary","Iceland","India","Indonesia","Iran","Iraq","Ireland","Isle of Man","Israel","Italy","Jamaica","Japan","Jersey","Jordan","Kazakhstan","Kenya","Kiribati","Kosovo","Kuwait","Kyrgyzstan","Laos","Latvia","Lebanon","Lesotho","Liberia","Libya","Liechtenstein","Lithuania","Luxembourg","Macau","Macedonia","Madagascar","Malawi","Malaysia","Maldives","Mali","Malta","Marshall Islands","Mauritania","Mauritius","Mexico","Micronesia","Moldova","Monaco","Mongolia","Montenegro","Montserrat","Morocco","Mozambique","Myanmar","Namibia","Nauro","Nepal","Netherlands","Netherlands Antilles","New Caledonia","New Zealand","Nicaragua","Niger","Nigeria","North Korea","Norway","Oman","Pakistan","Palau","Palestine","Panama","Papua New Guinea","Paraguay","Peru","Philippines","Poland","Portugal","Puerto Rico","Qatar","Reunion","Romania","Russia","Rwanda","Saint Pierre & Miquelon","Samoa","San Marino","Sao Tome and Principe","Saudi Arabia","Senegal","Serbia","Seychelles","Sierra Leone","Singapore","Slovakia","Slovenia","Solomon Islands","Somalia","South Africa","South Korea","South Sudan","Spain","Sri Lanka","St Kitts & Nevis","St Lucia","St Vincent","Sudan","Suriname","Swaziland","Sweden","Switzerland","Syria","Taiwan","Tajikistan","Tanzania","Thailand","Timor L'Este","Togo","Tonga","Trinidad & Tobago","Tunisia","Turkey","Turkmenistan","Turks & Caicos","Tuvalu","Uganda","Ukraine","United Arab Emirates","United Kingdom","United States of America","Uruguay","Uzbekistan","Vanuatu","Vatican City","Venezuela","Vietnam","Virgin Islands (US)","Yemen","Zambia","Zimbabwe"];

/*initiate the autocomplete function on the "myInput" element, and pass along the countries array as possible autocomplete values:*/
autocomplete(document.getElementById("myInput"), countries);



// dropdown menu 

document.addEventListener("DOMContentLoaded", function(){
// make it as accordion for smaller screens
if (window.innerWidth > 992) {

	document.querySelectorAll('.navbar .nav-item').forEach(function(everyitem){

		everyitem.addEventListener('mouseover', function(e){

			let el_link = this.querySelector('a[data-bs-toggle]');

			if(el_link != null){
				let nextEl = el_link.nextElementSibling;
				el_link.classList.add('show');
				nextEl.classList.add('show');
			}

		});
		everyitem.addEventListener('mouseleave', function(e){
			let el_link = this.querySelector('a[data-bs-toggle]');

			if(el_link != null){
				let nextEl = el_link.nextElementSibling;
				el_link.classList.remove('show');
				nextEl.classList.remove('show');
			}


		})
	});

}
// end if innerWidth
}); 









    </script>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
</body>

</html>