@extends('registernew')
<style>
    #login-blade { box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);  margin-bottom: 50px}
    #login-card { padding: 30px; }
    #login-card .card-header { color: #000; font-size: 28px ; font-weight: bold; margin-bottom: 30px}
    .social-icon-login a { color: #fff }
    .social-icon-login i { left:30px!important; }

    #pricing-plans-background-image{
        margin-top: 105px;
    }

</style>
@section('content')

<div id="content-pro" style="background-color: #fff">                   
    @if($message = Session::get('credentials'))
    <div class="btn btn-danger" style="width: 100%;  position: absolute; top: 102px">
        <p style="color: #fff; font-size: 20px">{{ $message }}</p>
    </div>
    @endif
    <div id="pricing-plans-background-image">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-3"></div>
                <div class="col-md-6" id="login-blade">
                    <div class="card" id="login-card">
                        <div class="card-header">Forgot Password</div>

                        <div class="card-body">
                            <form method="POST" action="{{ url('forgot_password') }}">
                                @csrf
                                @if(session('message'))
                                  {{session('message')}}
                                @endif
                                <div class="form-group row">
                                    <label for="email" class="col-sm-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                                    <div class="col-md-6">
                                        <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>
                                        @if(!empty(session()->get('fail')))
                                <p style="color:red; margin-top: 10px;">{{ session()->get('fail') }}</p>
                                @endif
                                        <h6 id="username-result"></h6>

                                        @if ($errors->has('email'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>


                                <div class="form-group row mb-0">
                                    <div class="col-md-12">
                                       <br><br>
                                        <button type="submit" id="submit" class="btn btn-primary">
                                            {{ __('Send Password Reset Link') }}
                                        </button>

                                        
                                    </div>
                                </div>
                            </form>
                            
                            
                    </div>
                </div>
                <div class="col-md-2"></div>
            </div>
        </div>
    </div>
</div>


@endsection
