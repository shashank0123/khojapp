@extends('layouts.admin')


@section('content')
<h4 class="header-title m-t-0 m-b-30">Settings</h4>
<h4 class="header-title m-t-0 m-b-30">Facebook</h4>
@if(Session::get('success'))
<div class="alert alert-success"> {{Session::get('success')}} </div>
@endif
@if (count($errors) > 0)
<div class="alert alert-danger">
	<ul>
		@foreach ($errors->all() as $error)
		<li>{{ $error }}</li>
		@endforeach
	</ul>
</div>
@endif

<form method="POST" action="{{url('admin/setting/website')}}" enctype="multipart/form-data">
	@csrf
	
	
	<div class="row input-control">
		<label class="col-sm-3">Website Name</label>
		<div class="col-sm-6">			
			<input type="text" name="name" id="name" value="{{$data['name'] ?? ''}}" class="form-control">
		</div>
	</div>
	<div class="row input-control">
		<label class="col-sm-3">Logo</label>
		<div class="col-sm-6">			
			<input type="file" name="logo" id="logo"  class="form-control">
		</div>
		<div class="col-sm-3"><img src="{{asset('asset/images/'.$data['logo'])}}" style="height: 60px; height: auto;" /></div>
	</div>

	<div class="row input-control">
		<label class="col-sm-3">Adddress (Area)</label>
		<div class="col-sm-6">			
			<input type="text" name="area" id="area" value="{{$data['area'] ?? ''}}" class="form-control">
		</div>
	</div>
	<div class="row input-control">
		<label class="col-sm-3">City</label>
		<div class="col-sm-6">			
			<input type="text" name="city" id="city" value="{{$data['city'] ?? ''}}" class="form-control">
		</div>
	</div>
	<div class="row input-control">
		<label class="col-sm-3">State</label>
		<div class="col-sm-6">			
			<input type="text" name="state" id="state" value="{{$data['state'] ?? ''}}" class="form-control">
		</div>
	</div>
	<div class="row input-control">
		<label class="col-sm-3">PinCode</label>
		<div class="col-sm-6">			
			<input type="text" name="pincode" id="pincode" value="{{$data['pincode'] ?? ''}}" class="form-control">
		</div>
	</div>
	
	<div class="row input-control">
		<label class="col-sm-3">Country</label>
		<div class="col-sm-6">			
			<input type="text" name="country" id="country" value="{{$data['country'] ?? ''}}" class="form-control">
		</div>
	</div>


	<div class="row input-control">
		<label class="col-sm-3">Contact Number</label>
		<div class="col-sm-6">			
			<input type="text" name="contact" id="contact" value="{{$data['contact'] ?? ''}}" class="form-control">
		</div>
	</div>
	<div class="row input-control">
		<label class="col-sm-3">Contact Email Id</label>
		<div class="col-sm-6">			
			<input type="text" name="contact_mail" id="contact_mail" value="{{$data['contact_mail'] ?? ''}}" class="form-control">
		</div>
	</div>
	<div class="row input-control">
		<label class="col-sm-3">Subscription Email</label>
		<div class="col-sm-6">			
			<input type="text" name="subsc_mail" id="subsc_mail" value="{{$data['subsc_mail'] ?? ''}}" class="form-control">
		</div>
	</div>

	<div class="row input-control">
		<label class="col-sm-3">Website URL</label>
		<div class="col-sm-6">			
			<input type="text" name="web_url" id="web_url" value="{{$data['web_url'] ?? ''}}" class="form-control">
		</div>
	</div>



	
	<div class="row form-group">
		<div class="col-sm-6 col-sm-offset-5">
			<button type="submit" name="save" class="btn btn-primary">Save</button> 
		</div>
	</div>


</form>


@endsection
