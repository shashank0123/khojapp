<?php 

if (isset(auth()->user()->profile_pic))
$profile_pic = '/images/user/'.auth()->user()->profile_pic;
else {
	$profile_pic = '/images/default_user.png';
}

$username = explode(' ',Auth::user()->name);

?>


@extends('layouts.khoj_new')

@section('content')
<style>
	 #clickBro { display: none; }
#file { display: none; }
.breadcumb li { display: inline-block !important; }
</style>

<div class="hero user-hero">
	@if($errors->any())
	<div class="alert alert-danger" style="position: absolute; top: 2px">
		@foreach($errors->all() as $error)
		<li style="color: #fff">{{ $error }}</li>
		@endforeach
	</div>
	@endif

	@if($message = Session::get('message'))
	<div class="btn btn-success" style="width: 100%; position: absolute; top: 2px">
		<p style="color: #fff">{{ $message }}</p>
	</div>
	@endif
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="hero-ct">
					<h1>{{$user->name}}</h1>
					<ul class="breadcumb">
						<li class="active"><a href="/">Home</a></li>
						<li> <span class="ion-ios-arrow-right"></span>Reset Password</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="page-single">
	<div class="container">
		<div class="row ipad-width">
			<div class="col-md-3 col-sm-12 col-xs-12">
				<div class="user-information">
					<div class="user-img">

						<form method="post" action="/upload-image" enctype="multipart/form-data" id="upload_image">
							@csrf
							<input type="hidden" name="member_id" id="uid" value="{{auth()->user()->id}}">
						<a href="#"><img src="{{$profile_pic}}" alt="" style="width: 80%; margin: auto"><br></a>
							{{-- @if(!empty($profile->profimage))
							<span id="uploaded_image">
								<img src="/assetsss/images/AdminProduct/{{$profile->profimage}}" width="150px" height="150px">
							</span>
							@else
							<span id="uploaded_image"></span>
							@endif --}}
							<br>
							{{-- <img src="{{ asset('images/product/fashion-12.jpg') }}" style="width:100px;height:100px;border-radius:50%;margin: auto;display: block; margin-bottom:15px"> --}}
							<label for="file"  class="redbtn">Change Pic</label>
							<input type="file" name="file" id="file" onchange='submitMe()'/>
							<input type="submit" name="submit" id="clickBro" />
						</form>
						
						{{-- <a href="#" class="redbtn">Change avatar</a> --}}





						{{-- <a href="#"><img src="{{$profile_pic}}" alt="" style="width: 80%; margin: auto"><br></a> --}}
					</div>
					<div class="user-fav">
						<p onclick="showdropdown1()" style="cursor: pointer;">Account Details&nbsp;&nbsp;<i class="fa fa-angle-down"></i></p>
						<ul style="display: none;" id="dropdown1">
							<li><a href="/favourites/{{auth()->user()->id}}">&nbsp;&nbsp;&nbsp;Favourite movies</a></li>
							{{-- <li><a href="/playlists/{{auth()->user()->id}}">&nbsp;&nbsp;&nbsp;My Playlist</a></li> --}}
							<li><a href="/watchlists/{{auth()->user()->id}}">&nbsp;&nbsp;&nbsp;My WatchList</a></li>
						</ul>
					</div>
					<div class="user-fav">
						<p onclick="showdropdown2()" style="cursor: pointer;">Settings&nbsp;&nbsp;<i class="fa fa-angle-down"></i></p>
						<ul style="display: none;" id="dropdown2">
							<li class="active"><a href="/profile">&nbsp;&nbsp;&nbsp;Profile</a></li>
							<li class="active"><a href="/reset-password">&nbsp;&nbsp;&nbsp;Change password</a></li>
							
						</ul>
					</div>

					<div class="user-fav">
						{{-- <p>Settings</p> --}}
						<ul>
							<li><a href="{{ route('logout') }}"
								onclick="event.preventDefault();
								document.getElementById('logout-form').submit();">Log out</a>
								<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
									{{ csrf_field() }}
								</form>
							</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="col-md-9 col-sm-12 col-xs-12">
				<div class="form-style-1 user-pro" action="#">
					<div class="row">
					
					<form action="/passwordUpdate/{{Auth::user()->id}}" class="password" method="GET" id='pass'>
						<h4>Change password</h4>
						<div class="row">
							<div class="col-md-6 form-it">
								<label>Old Password</label>
								<input type="text" placeholder="**********" name="old_password">
							</div>
						</div>
						<div class="row">
							<div class="col-md-6 form-it">
								<label>New Password</label>
								<input type="text" placeholder="***************" name="new_password">
							</div>
						</div>
						<div class="row">
							<div class="col-md-6 form-it">
								<label>Confirm New Password</label>
								<input type="text" placeholder="*************** " name="confirm_password">
							</div>
						</div>
						<div class="row">
							<div class="col-md-2">
								<input class="submit" type="submit" value="change">
							</div>
						</div>	
					</form>
				</div>

				</div>
			</div>
		</div>
	</div>
</div>
<!-- footer section-->

<script>
	function submitMe() {
    $('#file').click();
    $("#clickBro").click();
  }

  function showdropdown1() {
    $('#dropdown1').toggle();
   
  }

  function showdropdown2() {
    $('#dropdown2').toggle();
   
  }

</script>

@endsection