<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Movie;
use App\Genre;
use App\PublicPlaylist;
use Redirect;
class publicPlaylistController extends Controller
{
    public function playlist(Request $request)
    {
    	$playlists = PublicPlaylist::all();
    	return view('admin.publicplaylist.playlist', compact('playlists'));
    }

    public function addplaylist(Request $request)
    {
    	// $playlists = PublicPlaylist::all();
    	return view('admin.publicplaylist.add');
    }

    public function saveplaylist(Request $request)
    {
    	$data = $request->all();
        if($request->hasFile('poster')) 
          {
           $file=$request->poster;
           $file->move(public_path(). '/poster/', time().$file->getClientOriginalName());   
           // $file->move(public_path(). '/thumbnail/', time().$file->getClientOriginalName());   
           $poster = time().$file->getClientOriginalName();
         }
         else $poster = "";
    	$newData = PublicPlaylist::insert(array(
		  'playlist_name' => $data['title'] ?? '',

		  'playlist_order' => $data['order'] ?? '',
		  'description' => $data['description'],
          'playlist_content' => $data['movie_id'],
          'poster' => $poster,
		)); 
		 $message = 'Content successfully added.';
		 return redirect()->back()->with('success',$message);
    	// return view('admin.template');
    }


public function editplaylist(Request $request,$id){

$type = 'Edit';
  $playlist = PublicPlaylist::find($id);

  return view('admin.publicplaylist.add',compact('playlist','type'));
}

public function updateplaylist(Request $request,$id)
    {
      $data = $request->all();
      $newData = PublicPlaylist::where('id',$id)->first();
        if($request->hasFile('poster')) 
          {
           $file=$request->poster;
           $file->move(public_path(). '/poster/', time().$file->getClientOriginalName());   
           // $file->move(public_path(). '/thumbnail/', time().$file->getClientOriginalName());   
           $newData->poster = time().$file->getClientOriginalName();
         }
         


      $newData->playlist_name = $data['title'] ?? '';

      $newData->playlist_order = $data['order'] ?? '';
      $newData->description = $data['description'] ?? '';
          $newData->playlist_content = $data['movie_id'] ?? '';
          $newData->update();
  
     $message = 'Playlist successfully updated.';
     return redirect()->back()->with('success',$message);
      // return view('admin.template');
    }
    
public function deleteplaylist(Request $request,$id){
  PublicPlaylist::where('id',$id)->delete();
  $message = 'Playlist successfully deleted.';
     return redirect()->back()->with('success',$message);
}
       
}
