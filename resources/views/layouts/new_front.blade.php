<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7 no-js" lang="en-US">
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8 no-js" lang="en-US">
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html lang="en" class="no-js">

<head>
    <!-- Basic need -->
    <title>KHOJ</title>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="">
    <link rel="profile" href="#">
   <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" media="all">
<link rel="icon" href="/asset/images/logo1.png" type="image/x-icon"/>

<meta name="csrf-token" content="{{ csrf_token() }}">

        <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" media="all">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css" rel="stylesheet" media="all">
<link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">

        <!-- Bootstrap bootstrap-touch-slider Slider Main Style Sheet -->
        <link href="http://bootstrapthemes.co/demo/resource/BootstrapCarouselTouchSlider/bootstrap-touch-slider.css" rel="stylesheet" media="all">
    <!--Google Font-->
    <link rel="stylesheet" href='http://fonts.googleapis.com/css?family=Dosis:400,700,500|Nunito:300,400,600' />
    <!-- Mobile specific meta -->
    <meta name=viewport content="width=device-width, initial-scale=1">
    <meta name="format-detection" content="telephone-no">

    <!-- CSS files -->
    <link rel="stylesheet" href="/asset/css/plugins.css">
    <link rel="stylesheet" href="/asset/css/style.css">

    <link rel="stylesheet" href="https://unpkg.com/swiper/css/swiper.css">
<link rel="stylesheet" href="https://unpkg.com/swiper/css/swiper.min.css">

<style>
    @import url(https://fonts.googleapis.com/css?family=Lato:100,300,400,700);
@import url(https://raw.github.com/FortAwesome/Font-Awesome/master/docs/assets/css/font-awesome.min.css);

#wrap {
  margin: 10px 0px;
  display: inline-block;
  position: relative;
  height: 60px;
  float: right;
  padding: 0;
  position: relative;
}

#search {
  height: 50px;
  font-size: 16px;
  display: inline-block;
  font-family: "Lato";
  font-weight: 100;
  border: none;
  outline: none;
  color: #fff;
  padding: 3px;
  padding-right: 60px;
  width: 0px;
  position: absolute;
  top: 0;
  right: 0;
  background: none;
  z-index: 3;
  transition: width .4s cubic-bezier(0.000, 0.795, 0.000, 1.000);
  cursor: pointer;
}

#search:focus:hover {
  border-bottom: 1px solid #BBB;
}

#search:focus {
  width: 200px;
  z-index: 1;
  border-bottom: 1px solid #BBB;
  cursor: text;
}
#search_submit {
  height: 67px;
  width: 63px;
  display: inline-block;
  color:red;
  float: right;
  background: url('/asset/images/search.png') center center no-repeat;
  /*background: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAMAAABg3Am1AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAADNQTFRFU1NT9fX1lJSUXl5e1dXVfn5+c3Nz6urqv7+/tLS0iYmJqampn5+fysrK39/faWlp////Vi4ZywAAABF0Uk5T/////////////////////wAlrZliAAABLklEQVR42rSWWRbDIAhFHeOUtN3/ags1zaA4cHrKZ8JFRHwoXkwTvwGP1Qo0bYObAPwiLmbNAHBWFBZlD9j0JxflDViIObNHG/Do8PRHTJk0TezAhv7qloK0JJEBh+F8+U/hopIELOWfiZUCDOZD1RADOQKA75oq4cvVkcT+OdHnqqpQCITWAjnWVgGQUWz12lJuGwGoaWgBKzRVBcCypgUkOAoWgBX/L0CmxN40u6xwcIJ1cOzWYDffp3axsQOyvdkXiH9FKRFwPRHYZUaXMgPLeiW7QhbDRciyLXJaKheCuLbiVoqx1DVRyH26yb0hsuoOFEPsoz+BVE0MRlZNjGZcRQyHYkmMp2hBTIzdkzCTc/pLqOnBrk7/yZdAOq/q5NPBH1f7x7fGP4C3AAMAQrhzX9zhcGsAAAAASUVORK5CYII=) center center no-repeat;*/
  text-indent: -10000px;
  border: none;
  position: absolute;
  top: 0;
  right: 0;
  z-index: 2;
  cursor: pointer;
  opacity: 1;
  cursor: pointer;
  transition: opacity .4s ease;
}

#search_submit:hover {
  opacity: 0.8;
}

.dropdown {
  position: relative;
  display: inline-block;
  z-index: 999 !important
}

.dropdown-content {
  display: none;
  position: absolute;
  background-color: #f9f9f9;
  min-width: 180px;
  left: 30%;
  box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
  padding: 12px 16px;
  z-index: 999 !important;
}

.dropdown:hover .dropdown-content {
  display: block;
}

#profile-option a { color: #555!important;}
#profile-option a:hover { color: #006699!important;}
#profile-option li { padding: 8px!important;}

</style>

</head>
<body>
<!--preloading-->
<div id="preloader">
    <img class="logo" src="/asset/images/logo1.png" alt="" width="119" height="58">
    <div id="status">
        <span></span>
        <span></span>
    </div>
</div>
<!--end of preloading-->
<!--login form popup-->
{{-- <div class="login-wrapper" id="login-content">
    <div class="login-content">
        <a href="#" class="close">x</a>
        <h3>Login</h3>
        <form method="post" >
            <div class="row">
                 <label for="username">
                    Username:
                    <input type="text" name="username" id="username" placeholder="Username"  required="required" />
                </label>
            </div>
           
            <div class="row">
                <label for="password">
                    Password:
                    <input type="password" name="password" id="password" placeholder="******"  required="required" />
                </label>
            </div>
            <div class="row">
                <div class="remember">
                    <div>
                        <input type="checkbox" name="remember" value="Remember me"><span>Remember me</span>
                    </div>
                    <a href="#">Forget password ?</a>
                </div>
            </div>
           <div class="row">
             <button type="submit">Login</button>
           </div>
        </form>
        <div class="row">
            <p>Or via social</p>
            <div class="social-btn-2">
                <a class="fb" href="#"><i class="ion-social-facebook"></i>Facebook</a>
                <a class="tw" href="#"><i class="ion-social-googleplus"></i>Google</a>
            </div>
        </div>
    </div>
</div> --}}
<!--end of login form popup-->
<!--signup form popup-->
{{-- <div class="login-wrapper"  id="signup-content">
    <div class="login-content">
        <a href="#" class="close">x</a>
        <h3>sign up</h3>
        <form method="post" >
            <div class="row">
                 <label for="username-2">
                    Username:
                    <input type="text" name="username" id="username-2" placeholder="Enter  Username:"  required="required" />
                </label>
            </div>
           
            <div class="row">
                <label for="email-2">
                    your email:
                    <input type="password" name="email" id="email-2" placeholder="your email:" required="required" />
                </label>
            </div>
             <div class="row">
                <label for="password-2">
                    Password:
                    <input type="password" name="password" id="password-2" placeholder=""  required="required" />
                </label>
            </div>
             <div class="row">
                <label for="repassword-2">
                    re-type Password:
                    <input type="password" name="password" id="repassword-2" placeholder=""  required="required" />
                </label>
            </div>
           <div class="row">
             <button type="submit">sign up</button>
           </div>
        </form>
    </div>
</div> --}}
<!--end of signup form popup-->



 <!-- Modal -->
        <div class="modal fade" id="LoginModal" tabindex="-1" role="dialog" aria-labelledby="LoginModal" aria-hidden="true">
             <button type="button" class="close float-close-pro" data-dismiss="modal" aria-label="Close">
                       <span aria-hidden="true">&times;</span>
            </button>
          <div class="modal-dialog modal-dialog-centered modal-md" role="document">
            <div class="modal-content">
                 <div class="modal-header-pro">
                     <h2>Welcome Back</h2>
                     <h6>Sign in to your account to continue using SKRN</h6>
                 </div>
                 <div class="modal-body-pro social-login-modal-body-pro">
                     <div class="row">
                       <div class="col-sm-8">
                          <div class="registration-social-login-container">
                         <form method="POST" action="{{ route('login') }}">
                            @csrf
                             <div class="form-group">
                                 <input type="text" name="email" class="form-control" id="username" placeholder="Username">
                             </div>
                             <div class="form-group">
                                 <input type="password" name="password" class="form-control" id="password" placeholder="Password">
                             </div>
                             <div class="form-group">
                                 <input type="submit" name="submit" value="Sign In" class="btn btn-success" id="login-submit">
                             </div>
                             <div class="container-fluid">
                                 <div class="row no-gutters">
                                    <div class="col checkbox-remember-pro"><input type="checkbox" id="checkbox-remember"><label for="checkbox-remember" class="col-form-label">Remember me</label></div>
                                    <div class="col forgot-your-password"><a href="#!">Forgot your password?</a></div>
                                </div>
                            </div><!-- close .container-fluid -->
                        
                         </form>
                     
                         <div class="registration-social-login-or">or</div>
                         
                     </div><!-- close .registration-social-login-container -->
                       </div>
                       <div class="col-sm-4">
                         <div class="registration-social-login-options">
                        <h6>Sign in with your social account</h6>
                        <div class="social-icon-login facebook-color"><i class="fab fa-facebook-f"></i> Facebook</div>
                        <div class="social-icon-login twitter-color"><i class="fab fa-twitter"></i> Twitter</div>
                        <div class="social-icon-login google-color"><i class="fab fa-google-plus-g"></i> Google</div>
                     </div><!-- close .registration-social-login-options -->
                       </div>
                     </div> 
                    
                     
                     
                     
                     <div class="clearfix"></div>
                     

              </div><!-- close .modal-body -->
                
             <a class="not-a-member-pro" href="register">Not a member? <span>Join Today!</span></a>
            </div><!-- close .modal-content -->
          </div><!-- close .modal-dialog -->
        </div><!-- close .modal --> 

<!-- BEGIN | Header -->
<!-- BEGIN | Header -->
<header class="ht-header full-width-hd">
        <div class="row">
            <nav id="mainNav" class="navbar navbar-default navbar-custom">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header logo">
                    <div class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <div id="nav-icon1">
                            <span></span>
                            <span></span>
                            <span></span>
                        </div>
                    </div>

                    {{-- <div class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" style="display: block;">
                        <span class="sr-only">Toggle navigation</span>
                        <div id="nav-icon1">
                            <span></span>
                            <span></span>
                            <span></span>
                        </div>
                    </div> --}}
                    <a href="/"><img class="logo" src="/asset/images/logo1.png" alt="" width="119" height="58"></a>
                </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse flex-parent" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav flex-child-menu menu-left">
                        <li class="hidden">
                            <a href="#page-top"></a>
                        </li>
                        
                            
                                <li class="dropdown first">
                            <a class=" dropdown-toggle lv1" data-toggle="dropdown" data-hover="dropdown">
                            MOVIES<i class="fa fa-angle-down" aria-hidden="true"></i>
                            </a>
                            <ul class="dropdown-menu level1">
                                <li class="dropdown">
                                    <a href="/movies-list/latest">New Arrival{{-- <i class="fa fa-caret-right" aria-hidden="true"></i> --}}</a>
                                    {{-- <ul class="dropdown-menu level2">
                                        <li><a href="/movie-detail">Movie 1</a></li>
                                        <li><a href="/movie-detail">Movie 2</a></li>
                                        <li><a href="/movies-list">More..</a></li>
                                    </ul> --}}
                                </li>
                                <li class="dropdown">
                                    <a href="/movies-list/upcoming">Upcoming{{-- <i class="fa fa-caret-right" aria-hidden="true"></i> --}}</a>
                                   {{--  <ul class="dropdown-menu level2">
                                        <li><a href="/movie-detail">Movie 1</a></li>
                                        <li><a href="/movie-detail">Movie 2</a></li>
                                        <li><a href="/movies-list">More..</a></li>
                                    </ul> --}}
                                </li>
                                <li class="dropdown">
                                    <a href="/movies-list/popular">Popular{{-- <i class="fa fa-caret-right" aria-hidden="true"></i> --}}</a>
                                    {{-- <ul class="dropdown-menu level2">
                                        <li><a href="/movie-detail">Movie 1</a></li>
                                        <li><a href="/movie-detail">Movie 2</a></li>
                                        <li><a href="/movies-list">More..</a></li>
                                    </ul> --}}
                                </li>
                            </ul>
                        </li>
                    <li class="dropdown first">
                            {{-- <a class=" dropdown-toggle lv1" data-toggle="dropdown" data-hover="dropdown"> --}}
                            <a href="/t-series" class=" dropdown-toggle lv1" >
                            T Series
                            {{-- <i class="fa fa-angle-down" aria-hidden="true"></i> --}}
                            </a>
                            {{-- <ul class="dropdown-menu level1">
                               
                                <li><a href="moviegrid.html">Show1</a></li>
                                <li><a href="moviegridfw.html">Show2</a></li>
                                <li><a href="movielist.html">Show3</a></li>
                                
                            </ul> --}}
                        </li>
                       {{--  <li class="dropdown first">
                            <a class=" dropdown-toggle lv1" data-toggle="dropdown" data-hover="dropdown">
                            SERIAL<i class="fa fa-angle-down" aria-hidden="true"></i>
                            </a>
                            <ul class="dropdown-menu level1">
                                <li class="dropdown">
                                    <a href="#">Hindi<i class="fa fa-caret-right" aria-hidden="true"></i></a>
                                    <ul class="dropdown-menu level2">
                                        <li><a href="/serial-detail">Serial 1</a></li>
                                        <li><a href="/serial-detail">Serial 2</a></li>
                                        <li><a href="/serials-list">More..</a></li>
                                    </ul>
                                </li>
                                <li class="dropdown">
                                    <a href="#">English<i class="fa fa-caret-right" aria-hidden="true"></i></a>
                                    <ul class="dropdown-menu level2">
                                        <li><a href="/serial-detail">Serial 1</a></li>
                                        <li><a href="/serial-detail">Serial 2</a></li>
                                        <li><a href="/serials-list">More..</a></li>
                                    </ul>
                                </li>
                                <li class="dropdown">
                                    <a href="#">Bangali<i class="fa fa-caret-right" aria-hidden="true"></i></a>
                                    <ul class="dropdown-menu level2">
                                        <li><a href="/serial-detail">Serial 1</a></li>
                                        <li><a href="/serial-detail">Serial 2</a></li>
                                        <li><a href="/serials-list">More..</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </li> --}}
                        {{-- <li class="dropdown first">
                            <a class=" dropdown-toggle lv1" data-toggle="dropdown" data-hover="dropdown">
                            CHANNEL<i class="fa fa-angle-down" aria-hidden="true"></i>
                            </a>
                            <ul class="dropdown-menu level1">
                               
                                <li><a href="channel-shows">Star Plus</a></li>
                                <li><a href="channel-shows">Sony</a></li>
                                <li><a href="channel-shows">9X Tashan</a></li>
                                <li><a href="channel-shows">MH-1</a></li>
                                
                            </ul>
                        </li> --}}
                                
                                <li class="dropdown first">
                            <a class=" dropdown-toggle lv1" data-toggle="dropdown" data-hover="dropdown">
                            GENRE<i class="fa fa-angle-down" aria-hidden="true"></i>
                            </a>
                            <ul class="dropdown-menu level1">
                               @if(!empty($genres))
                               @foreach($genres as $gen)
                                <li><a href="/genres/{{$gen->name}}">{{$gen->name}}</a></li>
                                @endforeach
                                @endif
                            </ul>
                        </li>


                                
                                <li><a href="/movies-list/trending">TRENDING</a></li>
                                
                            
                    
                        
                        
                    </ul>
                    <ul class="nav navbar-nav flex-child-menu menu-right">
                                  <div id="wrap">
  <form action="/search" autocomplete="on" method="GET">
  <input id="search" name="search" type="text" placeholder="What're we looking for ?"><input id="search_submit" value="Rechercher" type="submit" >
  </form>
</div>    
                        
                       {{--  <li class=""><a href="/login">LOG In</a></li>
                        <li class="btn signupLink"><a href="/register">sign up</a></li> --}}
                   

        <?php if (isset(auth()->user()->profile_pic))
               $profile_pic = '/images/user/'.auth()->user()->profile_pic;
              else {
                $profile_pic = '/final/images/demo/account-profile.jpg';
              }
            ?>                
                @guest
                            <a href="/register" class="registerbutton">Register</a>
                
                      <a class="loginbutton" style="color: #fff" data-toggle="modal" data-target="#LoginModal" role="button">Sign In</a>
                        @else

                        <div class="dropdown">
  <span>  <div class="row" style="text-align: right;">
    <div class="col-sm-4">
       <img src="{{$profile_pic}}" alt="{{ Auth::user()->name }}" id="profile-image">
    </div>
    <div class="col-sm-8">
       <span id="username-header">
        <br>{{ Auth::user()->name }} <i class="fa fa-angle-down" aria-hidden="true"></i></span>
    </div>
  </div>
                 
                 
                <!-- close #header-user-profile-click --></span>
  <div class="dropdown-content" style="z-index: 999 !important">
  <ul class="dropdown" id="profile-option">  
                    <li><a href="/profile"><span class="icon-User"></span>My Profile</a></li>
                    <li><a href="/favourites/{{Auth::user()->id}}"><span class="icon-Favorite-Window"></span>My Favorites</a></li>
                    {{-- <li><a href="/account"><span class="icon-Gears"></span>Account Details</a></li> --}}
                    <li><a href="#!"><span class="icon-Life-Safer"></span>Help/Support</a></li>
                    <li><a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form></li>
                  </ul>
  </div>
</div>
                         


                   </ul>
                </div>
            <!-- /.navbar-collapse -->
        </nav>

                </div><!-- close #header-user-profile-menu -->
              </div><!-- close #header-user-profile -->
              
             
                        @endguest
        <!-- search form -->
        </div>
    
</header>


<!-- END | Header -->


@yield('content')


<!-- footer v2 section-->
<footer class="ht-footer full-width-ft">
    <div class="row">
        <div class="flex-parent-ft">
            <div class="flex-child-ft item1">
                 <a href="/"><img class="logo" src="/asset/images/logo1.png" alt=""></a>
                 <p>5th Avenue st, manhattan<br>
                New York, NY 10001</p>
                <p>Call us: <a href="#">(+01) 202 342 6789</a></p>
            </div>
            <div class="flex-child-ft item2">
                <h4>Resources</h4>
                <ul>
                    <li><a href="#">About</a></li> 
                    <li><a href="#">Blockbuster</a></li>
                    <li><a href="#">Contact Us</a></li>
                    <li><a href="#">Forums</a></li>
                    <li><a href="#">Blog</a></li>
                    <li><a href="#">Help Center</a></li>
                </ul>
            </div>
            <div class="flex-child-ft item3">
                <h4>Legal</h4>
                <ul>
                    <li><a href="#">Terms of Use</a></li> 
                    <li><a href="#">Privacy Policy</a></li> 
                    <li><a href="#">Security</a></li>
                </ul>
            </div>
            <div class="flex-child-ft item4">
                <h4>Account</h4>
                <ul>
                    <li><a href="#">My Account</a></li> 
                    <li><a href="#">Watchlist</a></li>  
                    <li><a href="#">Collections</a></li>
                    <li><a href="#">User Guide</a></li>
                </ul>
            </div>
            <div class="flex-child-ft item5">
                <h4>Newsletter</h4>
                <p>Subscribe to our newsletter system now <br> to get latest news from us.</p>
                <form action="#">
                    <input type="text" placeholder="Enter your email">
                </form>
                <a href="#" class="btn">Subscribe now <i class="ion-ios-arrow-forward"></i></a>
            </div>
        </div>
        <div class="ft-copyright">
            <div class="ft-left">
                <p>© 2017 Blockbuster. All Rights Reserved. Designed by leehari.</p>
            </div>
            <div class="backtotop">
                <p><a href="#" id="back-to-top">Back to top  <i class="ion-ios-arrow-thin-up"></i></a></p>
            </div>
        </div>
    </div>
</footer>
<!-- end of footer v2 section-->
  
        
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.touchswipe/1.6.18/jquery.touchSwipe.min.js"></script>
        

        <!-- Bootstrap bootstrap-touch-slider Slider Main JS File -->
        <script src="http://bootstrapthemes.co/demo/resource/BootstrapCarouselTouchSlider/bootstrap-touch-slider.js"></script>
        
        <script type="text/javascript">
            $('#bootstrap-touch-slider').bsTouchSlider();
        </script>

  <script src="https://unpkg.com/swiper/js/swiper.js"></script>
<script src="https://unpkg.com/swiper/js/swiper.min.js"></script>

<script src="/asset/js/jquery.js"></script>
<script src="/asset/js/plugins.js"></script>
<script src="/asset/js/plugins2.js"></script>
<script src="/asset/js/custom.js"></script>
</body>

</html>