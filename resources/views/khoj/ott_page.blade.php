<?php
use App\Genre;
use App\FeaturedImage;




//variable declaration for default settings 
if (!isset($type)){
  $type = 'None';
}

if (!isset($miny)){
  $miny = '1500';
}

if (!isset($maxy)){
  $maxy = date('Y');
}

if (!isset($minr)){
  $minr = '0';
}

if (!isset($maxr)){
  $maxr = '10';
}

$languages = DB::table('languages')
->leftJoin('contents','contents.original_language','languages.code')
->where('contents.original_language',"!=",NULL)
->where('languages.status','Active')
->select('contents.original_language','languages.english_name')
->orderBy('languages.display_priority','ASC')
->distinct('contents.original_language')
->get();

?>

@extends('layouts.khoj_new')
@section('content')
<style>
  .liked{
  opacity: 0.3
}

</style>
<div class="container-fluid" style="margin: 2% 5%">
  <div class="gallery gallery-responsive portfolio_slider text-center">
   <h1>Your favorite movies, TV Shows and Webseries are at one place now.</h1>
   <h2>Search and Enjoy!!</h2>
   <span>We would like to recommend the movies/Shows based on your taste hence please help us by ...</span>
   <p>Please chose the OTT you are subscribed to. (user can select any number of ott or does not chose any OTT) </p>
  </div>
</div>

<div class="row" style="margin-bottom: 20px; text-align: center;">
  <div class="container">
    <button  onclick="skipmovie()" class="btn btn-warning btn-large text-center">Skip</button>
  </div>
</div>



{{-- 2. Please chose the OTT you are subscribed to. (user can select any number of ott or does not chose any OTT) 
3. Select your preferred language
 --}}

  <div class="col-sm-10 col-lg-10 col-xs-12 col-md-offset-1 col-sm-offset-1 right-filter">
    

    <div class="movie-content" id="dynamic-search" style="height: 100%">
      
      @if (isset($languages))
      <?php $space = 0; $rat=0;?>
      @foreach ($languages as $element)
      <?php 
      $flag = 1;
      $space++;
      
      ?>

      <div onclick="selectmovie({{$element->original_language}})" id="div{{$element->original_language}}" class="search-response dynamic" style="width:6%">
        <div class="movie-item">
         <div class="mv-img" style="height: 100%;">
          <button style="color: black;margin: 10px" class="btn btn-dark">{{$element->english_name}}</button>
          <div class="overlay">
            <a href="#" class="icon" title="User Profile">
              <i class="fa fa-user"></i>
            </a>
          </div>
        </div>   
    </div>
  {{-- </a> --}}
</div>
<?php $rat++; ?>
@endforeach



@endif

@if($flag == 0)
<h4 class="result-sec">No result found</h4>
@endif

</div>


<div class="movie-content" id="dynamic-search" style="height: 100%">
      
      @if (isset($contents))
      <?php $space = 0; $rat=0;?>
      @foreach ($contents as $element)
      <?php 
      $parent_title = "";

      $flag = 1; 
      $space++;
      
      ?>

      <div onclick="selectmovie({{$element->provider_id}})" id="div{{$element->provider_id}}" class="search-response dynamic" style="width:6%">
        <div class="movie-item">
         <div class="mv-img" style="height: 100%;">
          <img src="/asset{{$element->icon_url}}" alt="{{$element->title ?? ''}}" class="poster-image web-image">
          <div class="overlay">
            <a href="#" class="icon" title="User Profile">
              <i class="fa fa-user"></i>
            </a>
          </div>
        </div>   
    </div>
  {{-- </a> --}}
</div>
<?php $rat++; ?>
@endforeach

<div class="row" style="margin-bottom: 20px">
  <div class="container col-md-offset-5">
    <button onclick="nextmovie()" class="btn btn-primary btn-large text-center">Submit</button>
  </div>
</div>

@endif

@if($flag == 0)
<h4 class="result-sec">No result found</h4>
@endif

</div>
</div>

@endsection 

@section('script')

<script type="text/javascript">
var total = 5;
function selectmovie(id)
{

  elementname = 'div'+id
  var element = document.getElementById(elementname)
  $.ajax({

    url : '/updateuserpreference',
    type : 'GET',
    data : {
        'id' : id
    },
    // dataType:'json',
    success : function(data) {          
    console.log(data)    
    console.log(data.resposeText)    
    document.cookie = "ott="+id;
        // alert('Data: '+data);
    },
    error : function(request,error)
    {
        // alert("Request: "+JSON.stringify(request));
    }
});
  // alert(id+" is the selected movie. Left = "+total)

  element.classList.add("liked");
  if (total == 1){
    window.location.href = "/";
  }
  total--;
}

function skipmovie()
{
  document.cookie = "ott=0";
  window.location.href = "/"
}

function nextmovie()
{
  document.cookie = "ott=1";
  window.location.href = "/"
}
</script>

<script type="text/javascript">


  $('.app').on('click',function(){
    $('#app').toggle();
    $('.app .fa').toggleClass('fa-angle-down');
  });

  $('.rate').on('click',function(){
    $('#rate').toggle();
    $('.rate .fa').toggleClass('fa-angle-down');
  });

  $('.year').on('click',function(){
    $('#year').toggle();
    $('.year .fa').toggleClass('fa-angle-down');
  });

  $('.genre').on('click',function(){
    $('#genre').toggle();
    $('.genre .fa').toggleClass('fa-angle-down');
  });

  $('.certified').on('click',function(){
    $('#certified').toggle();
    $('.certified .fa').toggleClass('fa-angle-down');
    // $('.certified .fa').addClass('fa-angle-down');
  });

  $('.language').on('click',function(){
    $('#language').toggle();
    $('.language .fa').toggleClass('fa-angle-down');
  });

  $('.top_rated_imdb').on('click',function(){
    $('#top_rated_imdb').toggle();
    $('.top_rated_imdb .fa').toggleClass('fa-angle-down');
  });

  $('.top_rated_tmdb').on('click',function(){
    $('#top_rated_tmdb').toggle();
    $('.top_rated_tmdb .fa').toggleClass('fa-angle-down');
  });


  

</script>

@endsection
