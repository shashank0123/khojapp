<?php
//variable declaration for default settings 
if (!isset($type)){
  $type = 'None';
}

if (!isset($miny)){
  $miny = '1500';
}

if (!isset($maxy)){
  $maxy = date('Y');
}

if (!isset($minr)){
  $minr = '0';
}

if (!isset($maxr)){
  $maxr = '10';
}



?>

@extends('layouts.khoj_new')

@section('banner')

<style>
.portfolio_slider {
 
}
.portfolio_slider {
   margin: 10px 0 15px;
  /*margin: 30px;*/
  max-width: 1600px;
  flex: 1 1 0;
  background-color: transparent;
}

.inner {
  padding: 6px;
}

.inner img {
  width: 100%;
  border-radius: 10px;
  height: auto;
}
img:hover {
  opacity: 0.9;
}
.featured_slider{
  padding : 0 !important;
}
.featured_slider .slider{
  width:100%;
  height: auto;
  max-height: 325px;
  margin:30px auto 0;
  /*margin:20px auto;*/
  text-align: center;
  padding:0px;
  color:white;
  .parent-slide{padding:15px;}
  img{display: block;margin:auto;}
}

.liked{
  opacity: 0.3
}

@media screen and (max-width:580px){
  .featured_slider .slider{
  margin:95px auto 0;
  text-align: center;
 }
}

.featured_img {
 width: 99%; 
  height: 98%;
   margin: 0 0.5%; 
   border: 3px solid #aaa; 
 }
 .slick-list { 
 padding: 10px auto !important }



</style>

<div class="container-fluid featured_slider"> 

  <div class="slider" id="featured_slider">
    @if(!empty($featureds))
    @foreach($featureds as $feature)
    <div class="slide">
      <a href="{{$feature->redirect_url ?? ''}}"><img src="{{asset($feature->poster)}}" alt="" class="img-responsive featured_img" /></a>

    </div>
    @endforeach 
    @endif    
  </div>
</div>
@endsection

@section('content')



<div class="container-fluid" style="margin: 2% 5%">
  <div class="gallery gallery-responsive portfolio_slider text-center">
   <h1>Your favorite movies, TV Shows and Webseries are at one place now.</h1>
   <h2>Search and Enjoy!!</h2>
   <span>We would like to recommend the movies/Shows based on your taste hence please help us by ...</span>
   <p>Choosing minimum 5 movies which you like most or you can </p>
  </div>
</div>

<div class="row" style="margin-bottom: 20px; text-align: center;">
  <div class="container">
    <button  onclick="skipmovie()" class="btn btn-warning btn-large text-center">Skip</button>
  </div>
</div>




{{-- 2. Please chose the OTT you are subscribed to. (user can select any number of ott or does not chose any OTT) 
3. Select your preferred language
 --}}

  <div class="col-sm-10 col-lg-10 col-xs-12 col-md-offset-1 col-sm-offset-1 right-filter">
    

    <div class="movie-content" id="dynamic-search" style="height: 100%">
      
      @if (isset($contents))
      <?php $space = 0; $rat=0;?>
      @foreach ($contents as $element)
      <?php 
      $parent_title = "";

      $flag = 1; 
      $space++;
      if($element->parent_id>0){
        $parent_title = DB::table('contents')->where('content_id',$element->parent_id)->first();
      }
      ?>

      <div onclick="selectmovie({{$element->content_id}})" id="div{{$element->content_id}}" class="col-sm-3 col-lg-2 col-md-3 col-xs-6 search-response dynamic" style="padding-left: 1%; padding-right: 1%">

       <?php $urlen = preg_replace('/[^a-zA-Z0-9_.]/', '_', (strtolower($element->title))).'-'.$element->content_id; ?>  

       {{-- <a  href="/detail/{{$element->content_type}}/{{$urlen}}"> --}}



        <div class="movie-item">
         <div class="mv-img">
          @if(!empty($element->poster) && strpos($element->poster, 'poster/') == false && file_exists(public_path().'/poster/'.$element->poster))
          <img src="/poster/{{$element->poster}}" alt="{{$element->title ?? ''}}" class="poster-image web-image">
          @else
          <img src="/images/default.png" alt="{{$element->title ?? ''}}" class="web-image">
          @endif
          <div class="overlay">
            <a href="#" class="icon" title="User Profile">
              <i class="fa fa-user"></i>
            </a>
          </div>
        </div>
        <div class="hvr-inner">
         {{-- <i class="fa fa-play" aria-hidden="true"></i>  --}}
       </div>
       <div class="title-in">

        <p>
         &nbsp;&nbsp;IMDB: <span style="color: #fff; font-size: 12px;">@if(isset($element->imdb_score)){{$element->imdb_score}}@else{{'N/A'}}@endif</span>
       </p>
     </div>
     <div class="imdb">
      <div class="row" style="text-align: left;">
        <?php $urlen = preg_replace('/[^a-zA-Z0-9_.]/', '_', (strtolower($element->title))).'-'.$element->content_id; ?>
        <span><a href="/detail/{{$element->content_type}}/{{$urlen}}" style="font-size: 15px; color : #fff"> 
          <?php
          if(isset($element->title))
            echo substr(explode(':',$element->title)[0],0,18); 
          elseif(isset($element->original_title))
            echo substr($element->original_title,0,25); 
          if(isset($parent_title) && isset($parent_title->title))
            echo " (".substr($parent_title->title,0,25).") ";
          elseif(isset($parent_title) && isset($parent_title->original_title))
            echo " (".substr($parent_title->original_title,0,25).") ";
          ?></a></span>

        </div>
        <div class="row rate-row" style="font-size: 12px; color:#9ba09b!important;text-transform: uppercase; ">
          <?php if ($element->original_language == 'hi') $element->original_language = "Hindi";?>
          <?php if ($element->original_language == 'en') $element->original_language = "English";?>
          <?php if ($element->original_language == 'te') $element->original_language = "Telugu";?>
          <?php if ($element->original_language == 'bn') $element->original_language = "Bengali";?>
          <?php if ($element->original_language == 'kn') $element->original_language = "Kannada";?>
          <?php if ($element->original_language == 'es') $element->original_language = "Spanish";?>
          <?php if ($element->original_language == 'ru') $element->original_language = "Russian";?>
          <?php if ($element->original_language == 'gu') $element->original_language = "Gujrati";?>
          <?php if ($element->original_language == 'ml') $element->original_language = "Malyalam";?>
          <?php if ($element->original_language == 'pa') $element->original_language = "Punjabi";?>

          {{ucfirst($element->content_type)?? ''}} @if(!empty($element->original_language))<span class="dot"></span>&nbsp;&nbsp;@endif
           {{ $element->original_language ?? ''}}
          @if(isset($element->age_certification))<span class="dot"></span> &nbsp;&nbsp;&nbsp;{{$element->age_certification}}@else<span class="dot"></span> &nbsp;&nbsp;&nbsp;{{'U/A'}}@endif

         

        </div>
      </div>
    </div>
  {{-- </a> --}}
</div>
<?php $rat++; ?>
@endforeach

@endif

@if($flag == 0)
<h4 class="result-sec">No result found</h4>
@endif

</div>

<div class="row" style="margin-bottom: 20px; text-align: center;">
  <div class="container">
    <button onclick="nextmovie()" class="btn btn-primary btn-large text-center">Submit</button>
  </div>
</div>

</div>



@endsection 
@section('script')

<script type="text/javascript">
var total = 5;
function selectmovie(id)
{
  elementname = 'div'+id
  var element = document.getElementById(elementname)
  $.ajax({

    url : '/updateuserpreference',
    type : 'GET',
    data : {
        'id' : id
    },
    // dataType:'json',
    success : function(data) {          
    console.log(data)    
    console.log(data.resposeText)    
    document.cookie = "genres="+id;
        // alert('Data: '+data);
    },
    error : function(request,error)
    {
        // alert("Request: "+JSON.stringify(request));
    }
});
  // alert(id+" is the selected movie. Left = "+total)

  element.classList.add("liked");
  if (total == 1){
    window.location.href = "/selectott";
  }
  total--;
}

function skipmovie()
{
  document.cookie = "genres=0";
  window.location.href = "/"
}

function nextmovie()
{
  document.cookie = "genres=1";
  window.location.href = "/selectott"
}

</script>



  



@endsection
