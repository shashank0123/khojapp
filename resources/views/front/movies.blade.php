@extends('layouts.new_front')


@section('content')


<div class="movie-items  full-width">
	<div class="row">
		<div class="col-md-12">
			<div class="title-hd">
				<h2>{{$side}}</h2>
				<br><br>
				{{-- <a href="#" class="nowshow">Now Showing <i class="ion-ios-arrow-right"></i></a>
				<a href="#" class="viewall">View all <i class="ion-ios-arrow-right"></i></a> --}}

			</div>
			
				<ul class="tab-links">
					{{-- <li class="subtitle">Showing all results for keyword</li> --}}
					                      
				</ul>        
			            
			            	<div class="row">
			            		@if (isset($contents->results))
					@foreach ($contents->results as $element)
			            		<div class="col-sm-3 search-response">
									<div class="movie-item">
				            			<div class="mv-img">
				            				@if(isset($element->poster_path))
				            				<img src="https://image.tmdb.org/t/p/w300{{$element->poster_path}}" alt=""  style="width: 240px; height: 340px">
				            				@else
				            				<img src="/images/default.png" alt="" style="width: 240px; height: 340px">				            				
				            				@endif
				            			</div>
				            			<div class="hvr-inner">
				            				<a  href="/movie-detail/{{$element->id}}">
				            					<i class="fa fa-play" aria-hidden="true"></i> </a>
				            			</div>
				            			<div class="title-in">
				            				<h6><a href="/movie-detail/{{$element->id}}">
				            					@if(isset($element->title))
				            					{{$element->title}}
				            					@endif
				            				</a></h6>
				            				<p><i class="ion-android-star"></i><span>
				            					@if(isset($element->poster_path))
				            					{{$element->vote_average}}
				            					@endif
				            					</span> /10</p>
				            			</div>
				            		</div>
								</div>
								@endforeach
			            	@endif

								
			            		
			            	</div>
			           
			        
			        
			          
		       	 	
			   
			    <hr class="hrstyle">
			

			
			
			
		</div>
	</div>
	
</div>


@endsection 