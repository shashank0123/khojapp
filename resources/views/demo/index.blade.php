@extends('demo.layout.khoj2')

@section('content')

<body>
  <!--Setting Modal -->
  <div class="modal fade5" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle"
    aria-hidden="true">
    <div class="modal-dialog5" role="document">
      <div class="modal-content5">
        <div class="modal-header5">
          <h1 class="modal-title" id="exampleModalLongTitle">Settings
            <button type="button" class="close pr-5" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </h1>
        </div>
        <div class="modal-body5">
          <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 pr-0">
            <h3>Account</h3>
            <div class="tab">
              <button class="tablinks" onclick="openCity(event, 'SignIn')" id="defaultOpen"> <i
                  class="fas fa-sign-in-alt pr-3"></i>Sign In<i class="fas fa-chevron-right"></i></button>
              <button class="tablinks" onclick="openCity(event, 'MyLanguage')"><i class="fas fa-language pr-3"></i>My
                Language<i class="fas fa-chevron-right"></i></button>
              <button class="tablinks" onclick="openCity(event, 'MyProviders')"><i
                  class="fas fa-satellite-dish pr-3"></i>My Providers<i class="fas fa-chevron-right"></i></button>
              <button class="tablinks" onclick="openCity(event, 'MyGenres')"><i class="fas fa-mars-double pr-3"></i>My
                Genres<i class="fas fa-chevron-right"></i></button>
              <button class="tablinks" onclick="openCity(event, 'MyActors')"><i class="fas fa-podcast pr-3"></i>My
                Actors<i class="fas fa-chevron-right"></i></button>
              <button class="tablinks" onclick="openCity(event, 'MyFilmmakers')"><i class="fas fa-tv pr-3"></i>My
                Filmmakers<i class="fas fa-chevron-right"></i></button>
            </div>
            <div id="SignIn" class="tabcontent">

              <img src="/newassets/images/Khojlogo.png">
              <h2 class="pt-4">Sign in to synchronize your watchlist across all your devices</h2>
              <button type="button" class="btn btn-lg mt-4 conti" data-toggle="modal"
                data-target="#my-modal">Continue</button>

            </div>
            <div id="MyLanguage" class="tabcontent">
              <div class="row  pt-4">
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 ">
                  <h3 class="pt-2 pr-2">Select All &nbsp;
                    <label class="switch mr-3">
                      <input type="checkbox">
                      <span class="slider round"></span>
                    </label>
                  </h3>
                </div>
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 ">
                  <a href="#">
                    <h3 class="pt-2 pr-2 text-right"><i class="fas fa-undo"></i>&nbsp; Clear</h3>
                  </a>

                </div>


              </div>
              <div class="row">
                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 ">
                  <button type="button" class="btn btn-lg mt-4 my_lang">
                    <div class="row">
                      <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4 ">
                        <p class="alpha_bet ">E</p>
                      </div>
                      <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-8 ">
                        <p class="text-left pt-2">English</p>
                      </div>
                    </div>
                  </button>
                </div>

                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 ">
                  <button type="button" class="btn btn-lg mt-4 my_lang">
                    <div class="row">
                      <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4 ">
                        <p class="alpha_bet">हि</p>
                      </div>
                      <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-8 ">
                        <p class="text-left pt-2">Hindi</p>
                      </div>
                    </div>
                  </button>
                </div>

              </div>
              <div class="row">
                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 ">
                  <button type="button" class="btn btn-lg mt-4 my_lang">
                    <div class="row">
                      <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4 ">
                        <p class="alpha_bet">తె</p>
                      </div>
                      <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-8 ">
                        <p class="text-left pt-2">Telugu</p>
                      </div>
                    </div>
                  </button>
                </div>

                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 ">
                  <button type="button" class="btn btn-lg mt-4 my_lang">
                    <div class="row">
                      <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4 ">
                        <p class="alpha_bet">മ</p>
                      </div>
                      <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-8 ">
                        <p class="text-left pt-2">Malayalam</p>
                      </div>
                    </div>
                  </button>
                </div>

              </div>
              <div class="row">
                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 ">
                  <button type="button" class="btn btn-lg mt-4 my_lang">
                    <div class="row">
                      <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4 ">
                        <p class="alpha_bet">த</p>
                      </div>
                      <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-8 ">
                        <p class="text-left pt-2">Tamil</p>
                      </div>
                    </div>
                  </button>
                </div>

                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 ">
                  <button type="button" class="btn btn-lg mt-4 my_lang">
                    <div class="row">
                      <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4 ">
                        <p class="alpha_bet">म</p>
                      </div>
                      <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-8 ">
                        <p class="text-left pt-2">Marathi</p>
                      </div>
                    </div>
                  </button>
                </div>

              </div>

              <div class="row">
                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 ">
                  <button type="button" class="btn btn-lg mt-4 my_lang">
                    <div class="row">
                      <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4 ">
                        <p class="alpha_bet">ગુ</p>
                      </div>
                      <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-8 ">
                        <p class="text-left pt-2">Gujrati</p>
                      </div>
                    </div>
                  </button>
                </div>

                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 ">
                  <button type="button" class="btn btn-lg mt-4 my_lang">
                    <div class="row">
                      <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4 ">
                        <p class="alpha_bet">ਪੰ</p>
                      </div>
                      <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-8 ">
                        <p class="text-left pt-2">Punjabi</p>
                      </div>
                    </div>
                  </button>
                </div>

              </div>
              <div class="row">
                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 ">
                  <button type="button" class="btn btn-lg mt-4 my_lang">
                    <div class="row">
                      <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4 ">
                        <p class="alpha_bet">ଓ</p>
                      </div>
                      <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-8 ">
                        <p class="text-left pt-2">Oddisa</p>
                      </div>
                    </div>
                  </button>
                </div>
                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 ">

                </div>

              </div>
              <div class="row justify-content-center pt-4">
                <button type="button" class="btn-primary  rounded px-5 py-2">Lets Go</button>
              </div>

            </div>
            <div id="MyProviders" class="tabcontent">
              <div class="row  pt-4">
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 ">
                  <h3 class="pt-2 pr-2">Select All &nbsp;
                    <label class="switch mr-3">
                      <input type="checkbox">
                      <span class="slider round"></span>
                    </label>
                  </h3>
                </div>
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 ">
                  <a href="#">
                    <h3 class="pt-2 pr-2 text-right"><i class="fas fa-undo"></i>&nbsp; Clear</h3>
                  </a>

                </div>


              </div>
              <div class="row">
                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 ">
                  <button type="button" class="btn btn-lg mt-4 my_lang">
                    <div class="row">
                      <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4 ">
                        <img src="/newassets/images/netflix-252.png " width="40" height="40">
                      </div>
                      <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-8 ">
                        <p class="text-left pt-3">Netflix</p>
                      </div>
                    </div>
                  </button>
                </div>

                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 ">
                  <button type="button" class="btn btn-lg mt-4 my_lang">
                    <div class="row">
                      <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4 ">
                        <img src="/newassets/images/amazon-prime-video-436.png " width="40" height="40">
                      </div>
                      <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-8 ">
                        <p class="text-left pt-3">Prime Video</p>
                      </div>
                    </div>
                  </button>
                </div>

              </div>
              <div class="row">
                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 ">
                  <button type="button" class="btn btn-lg mt-4 my_lang">
                    <div class="row">
                      <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4 ">
                        <img src="/newassets/images/disney-hotstar-323.png " width="40" height="40">
                      </div>
                      <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-8 ">
                        <p class="text-left pt-3">Disney+ Hotstar</p>
                      </div>
                    </div>
                  </button>
                </div>

                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 ">
                  <button type="button" class="btn btn-lg mt-4 my_lang">
                    <div class="row">
                      <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4 ">
                        <img src="/newassets/images/zee5-945.png " width="40" height="40">
                      </div>
                      <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-8 ">
                        <p class="text-left pt-3">Zee5</p>
                      </div>
                    </div>
                  </button>
                </div>

              </div>
              <div class="row">
                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 ">
                  <button type="button" class="btn btn-lg mt-4 my_lang">
                    <div class="row">
                      <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4 ">
                        <img src="/newassets/images/sony-liv-485.png " width="40" height="40">
                      </div>
                      <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-8 ">
                        <p class="text-left pt-3">SonyLIv</p>
                      </div>
                    </div>
                  </button>
                </div>

                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 ">
                  <button type="button" class="btn btn-lg mt-4 my_lang">
                    <div class="row">
                      <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4 ">
                        <img src="/newassets/images/voot.jpg " width="40" height="40">
                      </div>
                      <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-8 ">
                        <p class="text-left pt-3">Voot</p>
                      </div>
                    </div>
                  </button>
                </div>

              </div>

              <div class="row">
                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 ">
                  <button type="button" class="btn btn-lg mt-4 my_lang">
                    <div class="row">
                      <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4 ">
                        <img src="/newassets/images/mx-player-34.png " width="40" height="40">
                      </div>
                      <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-8 ">
                        <p class="text-left pt-3">Mx Player</p>
                      </div>
                    </div>
                  </button>
                </div>

                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 ">
                  <button type="button" class="btn btn-lg mt-4 my_lang">
                    <div class="row">
                      <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4 ">
                        <img src="/newassets/images/jio-cinema-665.png " width="40" height="40">
                      </div>
                      <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-8 ">
                        <p class="text-left pt-3">Jio Cinema</p>
                      </div>
                    </div>
                  </button>
                </div>

              </div>
              <div class="row">
                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 ">
                  <button type="button" class="btn btn-lg mt-4 my_lang">
                    <div class="row">
                      <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4 ">
                        <img src="/newassets/images/dicovery-538.png " width="40" height="40">
                      </div>
                      <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-8 ">
                        <p class="text-left pt-3">Discovery+</p>
                      </div>
                    </div>
                  </button>
                </div>
                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 ">
                  <button type="button" class="btn btn-lg mt-4 my_lang">
                    <div class="row">
                      <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4 ">
                        <img src="/newassets/images/sunnxt-393.png " width="40" height="40">
                      </div>
                      <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-8 ">
                        <p class="text-left pt-3">Sun Nxt</p>
                      </div>
                    </div>
                  </button>
                </div>
              </div>
              <div class="row">
                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 ">
                  <button type="button" class="btn btn-lg mt-4 my_lang">
                    <div class="row">
                      <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4 ">
                        <img src="/newassets/images/dicovery-538.png " width="40" height="40">
                      </div>
                      <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-8 ">
                        <p class="text-left pt-3">Discovery+</p>
                      </div>
                    </div>
                  </button>
                </div>
                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 ">
                  <button type="button" class="btn btn-lg mt-4 my_lang">
                    <div class="row">
                      <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4 ">
                        <img src="/newassets/images/sunnxt-393.png " width="40" height="40">
                      </div>
                      <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-8 ">
                        <p class="text-left pt-3">Sun Nxt</p>
                      </div>
                    </div>
                  </button>
                </div>
              </div>

              <div class="row">
                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 ">
                  <button type="button" class="btn btn-lg mt-4 my_lang">
                    <div class="row">
                      <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4 ">
                        <img src="/newassets/images/dicovery-538.png " width="40" height="40">
                      </div>
                      <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-8 ">
                        <p class="text-left pt-3">Discovery+</p>
                      </div>
                    </div>
                  </button>
                </div>
                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 ">
                  <button type="button" class="btn btn-lg mt-4 my_lang">
                    <div class="row">
                      <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4 ">
                        <img src="/newassets/images/sunnxt-393.png " width="40" height="40">
                      </div>
                      <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-8 ">
                        <p class="text-left pt-3">Sun Nxt</p>
                      </div>
                    </div>
                  </button>
                </div>
              </div>

              <div class="row">
                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 ">
                  <button type="button" class="btn btn-lg mt-4 my_lang">
                    <div class="row">
                      <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4 ">
                        <img src="/newassets/images/dicovery-538.png " width="40" height="40">
                      </div>
                      <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-8 ">
                        <p class="text-left pt-3">Discovery+</p>
                      </div>
                    </div>
                  </button>
                </div>
                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 ">
                  <button type="button" class="btn btn-lg mt-4 my_lang">
                    <div class="row">
                      <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4 ">
                        <img src="/newassets/images/sunnxt-393.png " width="40" height="40">
                      </div>
                      <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-8 ">
                        <p class="text-left pt-3">Sun Nxt</p>
                      </div>
                    </div>
                  </button>
                </div>
              </div>

              <div class="row">
                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 ">
                  <button type="button" class="btn btn-lg mt-4 my_lang">
                    <div class="row">
                      <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4 ">
                        <img src="/newassets/images/dicovery-538.png " width="40" height="40">
                      </div>
                      <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-8 ">
                        <p class="text-left pt-3">Discovery+</p>
                      </div>
                    </div>
                  </button>
                </div>
                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 ">
                  <button type="button" class="btn btn-lg mt-4 my_lang">
                    <div class="row">
                      <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4 ">
                        <img src="/newassets/images/sunnxt-393.png " width="40" height="40">
                      </div>
                      <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-8 ">
                        <p class="text-left pt-3">Sun Nxt</p>
                      </div>
                    </div>
                  </button>
                </div>
              </div>
              <div class="row justify-content-center pt-4">
                <button type="button" class="btn-primary rounded px-5 py-2">Lets Go</button>
              </div>
            </div>
            <div id="MyGenres" class="tabcontent">
              <div class="row  pt-4">
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 ">
                  <h3 class="pt-2 pr-2">Select All &nbsp;
                    <label class="switch mr-3">
                      <input type="checkbox">
                      <span class="slider round"></span>
                    </label>
                  </h3>
                </div>
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 ">
                  <a href="#">
                    <h3 class="pt-2 pr-2 text-right"><i class="fas fa-undo"></i>&nbsp; Clear</h3>
                  </a>
                </div>
              </div>
              <div class="row pt-3">
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                  <a href="#"><img src="/newassets/images/adventure-849.png" class="filter_img" style="width: 100% ; "
                      alt="img-responsive" class="img-fluid ">
                    <p class="text-center">Adventure</p>
                  </a>
                </div>

                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                  <a href="#"><img src="/newassets/images/drama-346.png" class="filter_img" style="width: 100% ; "
                      alt="img-responsive" class="img-fluid ">
                    <p class="text-center">Drama</p>
                  </a>
                </div>

                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                  <a href="#"><img src="/newassets/images/war-314.png" class="filter_img" style="width: 100% ; "
                      alt="img-responsive" class="img-fluid ">
                    <p class="text-center">War</p>
                  </a>
                </div>



              </div>
              <div class="row pt-3">
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                  <a href="#"><img src="/newassets/images/horror-527.png" class="filter_img" style="width: 100% ; "
                      alt="img-responsive" class="img-fluid ">
                    <p class="text-center">Horror</p>
                  </a>
                </div>

                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                  <a href="#"> <img src="/newassets/images/western-341.png" class="filter_img" style="width: 100% ; "
                      alt="img-responsive" class="img-fluid ">
                    <p class="text-center">Westeren</p>
                  </a>
                </div>

                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                  <a href="#"><img src="/newassets/images/comedy-938.png" class="filter_img" style="width: 100% ; "
                      alt="img-responsive" class="img-fluid ">
                    <p class="text-center">Comedy</p>
                  </a>
                </div>
              </div>
              <div class="row pt-3">
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                  <a href="#"> <img src="/newassets/images/adventure-849.png" class="filter_img" style="width: 100% ; "
                      alt="img-responsive" class="img-fluid ">
                    <p class="text-center">Adventure</p>
                  </a>
                </div>

                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                  <a href="#"> <img src="/newassets/images/drama-346.png" class="filter_img" style="width: 100% ; "
                      alt="img-responsive" class="img-fluid ">
                    <p class="text-center">Drama</p>
                  </a>
                </div>

                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                  <a href="#"> <img src="/newassets/images/war-314.png" class="filter_img" style="width: 100% ; "
                      alt="img-responsive" class="img-fluid ">
                    <p class="text-center">War</p>
                  </a>
                </div>



              </div>
              <div class="row pt-3">
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                  <a href="#"> <img src="/newassets/images/horror-527.png" class="filter_img" style="width: 100% ; "
                      alt="img-responsive" class="img-fluid ">
                    <p class="text-center">Horror</p>
                  </a>
                </div>

                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                  <a href="#"> <img src="/newassets/images/western-341.png" class="filter_img" style="width: 100% ; "
                      alt="img-responsive" class="img-fluid ">
                    <p class="text-center">Westeren</p>
                  </a>
                </div>

                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                  <a href="#"> <img src="/newassets/images/comedy-938.png" class="filter_img" style="width: 100% ; "
                      alt="img-responsive" class="img-fluid ">
                    <p class="text-center">Comedy</p>
                  </a>
                </div>



              </div>
              <div class="row pt-3">
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                  <a href="#"> <img src="/newassets/images/adventure-849.png" class="filter_img" style="width: 100% ; "
                      alt="img-responsive" class="img-fluid ">
                    <p class="text-center">Adventure</p>
                  </a>
                </div>

                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                  <a href="#"><img src="/newassets/images/drama-346.png" class="filter_img" style="width: 100% ; "
                      alt="img-responsive" class="img-fluid ">
                    <p class="text-center">Drama</p>
                  </a>
                </div>

                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                  <a href="#"> <img src="/newassets/images/war-314.png" class="filter_img" style="width: 100% ; "
                      alt="img-responsive" class="img-fluid ">
                    <p class="text-center">War</p>
                  </a>
                </div>



              </div>
              <div class="row pt-3">
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                  <a href="#"> <img src="/newassets/images/horror-527.png" class="filter_img" style="width: 100% ; "
                      alt="img-responsive" class="img-fluid ">
                    <p class="text-center">Horror</p>
                  </a>
                </div>

                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                  <a href="#"> <img src="/newassets/images/western-341.png" class="filter_img" style="width: 100% ; "
                      alt="img-responsive" class="img-fluid ">
                    <p class="text-center">Westeren</p>
                  </a>
                </div>

                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                  <a href="#"> <img src="/newassets/images/comedy-938.png" class="filter_img" style="width: 100% ; "
                      alt="img-responsive" class="img-fluid ">
                    <p class="text-center">Comedy</p>
                  </a>
                </div>

              </div>
              <div class="row justify-content-center pt-4">
                <button type="button" class="btn-primary rounded px-5 py-2">Lets Go</button>
              </div>
            </div>
            <div id="MyActors" class="tabcontent">
              <div class="row  pt-4">
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 ">
                  <h3 class="pt-2 pr-2">Select All &nbsp;
                    <label class="switch mr-3">
                      <input type="checkbox">
                      <span class="slider round"></span>
                    </label>
                  </h3>
                </div>
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 ">
                  <a href="#">
                    <h3 class="pt-2 pr-2 text-right"><i class="fas fa-undo"></i>&nbsp; Clear</h3>
                  </a>

                </div>


              </div>
              <div class="row pt-3">
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                  <a href="#"><img src="/newassets/images/ranbir.png" class="filter_img" style="width: 100% ; "
                      alt="img-responsive" class="img-fluid ">
                    <h4 class="text-center">Ranbir kapoor</h4>
                  </a>
                </div>

                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                  <a href="#"> <img src="/newassets/images/alia.png" class="filter_img" style="width: 100% ; " alt="img-responsive"
                      class="img-fluid ">
                    <h4 class="text-center">Alia Bhatt</h4>
                  </a>
                </div>

                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                  <a href="#"> <img src="/newassets/images/ranveer.png" class="filter_img" style="width: 100% ; "
                      alt="img-responsive" class="img-fluid ">
                    <h4 class="text-center">Ranveer singh</h4>
                  </a>
                </div>

              </div>
              <div class="row pt-3">
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                  <a href="#"> <img src="/newassets/images/ranbir.png" class="filter_img" style="width: 100% ; "
                      alt="img-responsive" class="img-fluid ">
                    <h4 class="text-center">Ranbir kapoor</h4>
                  </a>
                </div>

                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                  <a href="#"><img src="/newassets/images/alia.png" class="filter_img" style="width: 100% ; " alt="img-responsive"
                      class="img-fluid ">
                    <h4 class="text-center">Alia Bhatt</h4><a href="#"></a>
                </div>

                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                  <a href="#"><img src="/newassets/images/ranveer.png" class="filter_img" style="width: 100% ; "
                      alt="img-responsive" class="img-fluid ">
                    <h4 class="text-center">Ranveer singh</h4>
                  </a>
                </div>

              </div>
              <div class="row pt-3">
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                  <a href="#"> <img src="/newassets/images/ranbir.png" class="filter_img" style="width: 100% ; "
                      alt="img-responsive" class="img-fluid ">
                    <h4 class="text-center">Ranbir kapoor</h4>
                  </a>
                </div>

                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                  <a href="#"> <img src="/newassets/images/alia.png" class="filter_img" style="width: 100% ; " alt="img-responsive"
                      class="img-fluid ">
                    <h4 class="text-center">Alia Bhatt</h4>
                  </a>
                </div>

                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                  <a href="#"> <img src="/newassets/images/ranveer.png" class="filter_img" style="width: 100% ; "
                      alt="img-responsive" class="img-fluid ">
                    <h4 class="text-center">Ranveer singh</h4>
                  </a>
                </div>

              </div>
              <div class="row pt-3">
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                  <a href="#"> <img src="/newassets/images/ranbir.png" class="filter_img" style="width: 100% ; "
                      alt="img-responsive" class="img-fluid ">
                    <h4 class="text-center">Ranbir kapoor</h4>
                  </a>
                </div>

                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                  <a href="#"> <img src="/newassets/images/alia.png" class="filter_img" style="width: 100% ; " alt="img-responsive"
                      class="img-fluid ">
                    <h4 class="text-center">Alia Bhatt</h4>
                  </a>
                </div>

                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                  <a href="#"> <img src="/newassets/images/ranveer.png" class="filter_img" style="width: 100% ; "
                      alt="img-responsive" class="img-fluid ">
                    <h4 class="text-center">Ranveer singh</h4>
                  </a>
                </div>

              </div>
              <div class="row justify-content-center pt-4">
                <button type="button" class="btn-primary rounded px-5 py-2">Lets Go</button>
              </div>
            </div>
            <div id="MyFilmmakers" class="tabcontent">
              <div class="row  pt-4">
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 ">
                  <h3 class="pt-2 pr-2">Select All &nbsp;
                    <label class="switch mr-3">
                      <input type="checkbox">
                      <span class="slider round"></span>
                    </label>
                  </h3>
                </div>
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 ">
                  <a href="#">
                    <h3 class="pt-2 pr-2 text-right"><i class="fas fa-undo"></i>&nbsp; Clear</h3>
                  </a>

                </div>


              </div>
              <div class="row pt-3">
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                  <a href="#"><img src="/newassets/images/ranbir.png" class="filter_img" style="width: 100% ; "
                      alt="img-responsive" class="img-fluid ">
                    <h4 class="text-center">Ranbir kapoor</h4>
                  </a>
                </div>

                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                  <a href="#"> <img src="/newassets/images/alia.png" class="filter_img" style="width: 100% ; " alt="img-responsive"
                      class="img-fluid ">
                    <h4 class="text-center">Alia Bhatt</h4>
                  </a>
                </div>

                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                  <a href="#"> <img src="/newassets/images/ranveer.png" class="filter_img" style="width: 100% ; "
                      alt="img-responsive" class="img-fluid ">
                    <h4 class="text-center">Ranveer singh</h4>
                  </a>
                </div>

              </div>
              <div class="row pt-3">
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                  <a href="#"> <img src="/newassets/images/ranbir.png" class="filter_img" style="width: 100% ; "
                      alt="img-responsive" class="img-fluid ">
                    <h4 class="text-center">Ranbir kapoor</h4>
                  </a>
                </div>

                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                  <a href="#"><img src="/newassets/images/alia.png" class="filter_img" style="width: 100% ; " alt="img-responsive"
                      class="img-fluid ">
                    <h4 class="text-center">Alia Bhatt</h4><a href="#"></a>
                </div>

                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                  <a href="#"><img src="/newassets/images/ranveer.png" class="filter_img" style="width: 100% ; "
                      alt="img-responsive" class="img-fluid ">
                    <h4 class="text-center">Ranveer singh</h4>
                  </a>
                </div>

              </div>
              <div class="row pt-3">
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                  <a href="#"> <img src="/newassets/images/ranbir.png" class="filter_img" style="width: 100% ; "
                      alt="img-responsive" class="img-fluid ">
                    <h4 class="text-center">Ranbir kapoor</h4>
                  </a>
                </div>

                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                  <a href="#"> <img src="/newassets/images/alia.png" class="filter_img" style="width: 100% ; " alt="img-responsive"
                      class="img-fluid ">
                    <h4 class="text-center">Alia Bhatt</h4>
                  </a>
                </div>

                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                  <a href="#"> <img src="/newassets/images/ranveer.png" class="filter_img" style="width: 100% ; "
                      alt="img-responsive" class="img-fluid ">
                    <h4 class="text-center">Ranveer singh</h4>
                  </a>
                </div>

              </div>
              <div class="row pt-3">
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                  <a href="#"> <img src="/newassets/images/ranbir.png" class="filter_img" style="width: 100% ; "
                      alt="img-responsive" class="img-fluid ">
                    <h4 class="text-center">Ranbir kapoor</h4>
                  </a>
                </div>

                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                  <a href="#"> <img src="/newassets/images/alia.png" class="filter_img" style="width: 100% ; " alt="img-responsive"
                      class="img-fluid ">
                    <h4 class="text-center">Alia Bhatt</h4>
                  </a>
                </div>

                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                  <a href="#"> <img src="/newassets/images/ranveer.png" class="filter_img" style="width: 100% ; "
                      alt="img-responsive" class="img-fluid ">
                    <h4 class="text-center">Ranveer singh</h4>
                  </a>
                </div>

              </div>
              <div class="row justify-content-center pt-4">
                <button type="button" class="btn-primary rounded px-5 py-2">Lets Go</button>
              </div>
            </div>

          </div>
        </div>
      </div>
    </div>
  </div>
  <section class="top_bar">
    <div class="container">
      <div class="row pt-2">
        <button type="button" class="btn leftpop" data-toggle="modal" data-target="#exampleModal">
          Feedback
        </button>
        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6">
          <div class="row">
            <a class="nav-link " href="#"> <img src="/newassets/images/googleicon.png" width="20" alt=""></a>
            <a class="nav-link active  Text-white" href="#"><img src="/newassets/images/apple.png" width="25" class="ml-4"
                alt=""></a>
          </div>
        </div>
        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 pr-1">
          <ul class="list-inline list-unstyled mb-0">
            <li class="list-inline-item">
              <button type="button" class="btn_fasco pt-1" data-toggle="modal" data-target="#exampleModalLong">
                <a class="nav-link" href="#"><i class="fas fa-cog"></i></a>
            </li>
            </button>
            <li class="list-inline-item">
              <div class="container d-flex justify-content-center registermodal"> <button class="btn p-0 login  mr-2"
                  data-toggle="modal" data-target="#my-modal">
                  <i class="fas fa-user fa-2x"></i> </button>
                <div id="my-modal" class="modal fade fix_modal" tabindex="-1" role="dialog" aria-hidden="true">
                  <div class="modal-dialog justify-content-center " role="document">
                    <div class="modal-content border-0 mx-3">
                      <div class="modal-body p-0">
                        <div class="row justify-content-center">
                          <div class="col-auto">
                            <div class="card">
                              <div class="card-header  pb-0 border-0">
                              </div>
                              <div class="card-body pt-0">
                                <div class="row justify-content-center text-center">
                                  <div class="col">
                                    <h2 class="mb-2 pt-1"><b>Welcome Back Login below.</b></h2>
                                  </div>
                                </div>
                                <div class="row justify-content-center my-1">
                                  <div class="col-12">
                                    <p class="text-center">We'll send you welcome emails and never post.</p>
                                  </div>
                                </div>
                                <div class="row justify-content-center pt-1">
                                  <a href="#"> <button type="button" class="btn btn-icon  text-left "><span><img
                                          src="https://img.icons8.com/color/48/000000/google-logo.png"
                                          class="img-fluid mr-1" width="35"></span> </button></a>
                                  <a href="#"> <button type="button" class="btn btn-icon  text-left "><span><img
                                          src="https://i.imgur.com/URmkevm.png" class="img-fluid mr-1"
                                          width="35"></span> </button> </a>
                                  <a href="#"> <button type="button" class="btn btn-icon  text-left "><span><img
                                          src="https://img.icons8.com/ios-filled/50/000000/mac-os.png"
                                          class="img-fluid mr-1" width="35"></span> </button></a>
                                </div>
                                <div class="row ">
                                  <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 pt-4">
                                    <ul class="nav nav-pills mb-1 justify-content-center registry " id="pills-tab"
                                      role="tablist">
                                      <li class="nav-item text-center">
                                        <a class="nav-link active" id="pills-home-tab" data-toggle="pill"
                                          href="#pills-homemy" role="tab" aria-controls="pills-home"
                                          aria-selected="true">Signup</a>
                                      </li>
                                      <li class="nav-item text-center">
                                        <a class="nav-link" id="pills-profile-tab" data-toggle="pill"
                                          href="#pills-profilemy" role="tab" aria-controls="pills-profile"
                                          aria-selected="false">Login</a>
                                      </li>
                                    </ul>
                                    <div class="tab-content signi" id="pills-tabContent">
                                      <div class="tab-pane fade show active" id="pills-homemy" role="tabpanel"
                                        aria-labelledby="pills-home-tab">
                                        <div class="container">
                                          <div class="row justify-content-center">
                                            <div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 col-12 ">
                                              <form>
                                                <div class="form-group">
                                                   
                                                  <div class="searchformfld">
                                                    <input type="text" class="candidateName" id="candidateName" name="candidateName" placeholder=" "/>
                                                    <label for="candidateName">Your name</label>
                                                </div>

                                                <div class="searchformfld formfld">
                                                  <input type="email" class="email"  placeholder=" "/>
                                                  <label for="candidateName">Email</label>
                                              </div>
                                              <div class="searchformfld formfld">
                                                <input type="password" class="password" placeholder=" "/>
                                                <label for="candidateName">Password</label>
                                            </div>
                                           
                                                  
                                                  <input type="checkbox" class="form-check-input" id="exampleCheck1">
                                                  <label class="form-check-label pl-4" for="exampleCheck1">Receive
                                                    welcome emails</label>
                                                </div>
                                              </form>
                                              <div class="row text-center">
                                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12  ">
                                                  <button type="button"
                                                    class="btn btn-primary btn-lg signbtn mt-1">Signup</button>
                                                </div>
                                              </div>


                                              <div class="row">

                                                <p class="text-center pt-1">By creating an account you consent to having
                                                  your data <br>collected to be used for generating recommendations,
                                                  market research,<br> and improving the service
                                                  <br> Learn more: Privacy | Terms of Service
                                                </p>

                                              </div>


                                            </div>
                                          </div>
                                        </div>

                                      </div>
                                      <div class="tab-pane fade" id="pills-profilemy" role="tabpanel"
                                        aria-labelledby="pills-profile-tab">
                                        <div class="row justify-content-center">
                                          <div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 col-12 ">

                                            <form>
                                              <div class="searchformfld formfld">
                                                <input type="email" class="email"  placeholder=" "/>
                                                <label for="candidateName">Email</label>
                                            </div>
                                            <div class="searchformfld formfld">
                                              <input type="password" class="password" placeholder=" "/>
                                              <label for="candidateName">Password</label>
                                          </div>
                                            </form>
                                            <div class="row text-center">
                                              <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12  ">
                                                <button type="button"
                                                  class="btn btn-primary btn-lg signbtn mt-3">Login</button>
                                              </div>
                                            </div>

                                            <div class="row justify-content-center pt-2">

                                              <a href="#">Forgot Password?</a>

                                            </div>

                                          </div>
                                        </div>

                                      </div>

                                    </div>
                                  </div>
                                </div>

                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

            </li>
            <li class="list-inline-item">
              <ul class="nav ">
                <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle bg-white" href="#" id="navbarDropdown" role="button"
                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Theme Option
                  </a>
                  <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="#">Dark</a>
                    <a class="dropdown-item" href="#">White</a>
                  </div>
                </li>
              </ul>
            </li>
          </ul>
        </div>
      </div>
    </div>

    <!-- feedback Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
      aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel"></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body modell">
            <div class="container">
              <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 pb-4 ">
                  <h2 class="input">We Want Your Input!</h2>
                  <form>
                    <div class="form-group">
                      <label for="exampleFormControlTextarea1"></label>
                      <textarea class="form-control " id="exampleFormControlTextarea1"
                        placeholder="Questions, bug reports, missing or wrong data, praise, feature requests — help us improve!"
                        rows="3"></textarea>
                    </div>
                    <div class="form-group">
                      <div class="md-form mb-0">
                        <label for="name" class="">Your name</label>
                        <input type="text" id="name" name="name" class="form-control">
                      </div>

                      <label for="exampleInputEmail1">Email address</label>
                      <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">

                    </div>

                  </form>
                  <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 pl-0 pt-3">


                    <p class="note">Note: Without giving us an email we can't reply to you!</p>
                  </div>
                  <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 text-right pt-3">
                    <button type="button" class="btn btn-secondary btn-lg sub">
                      submit
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- feedback Modal end -->

  </section>
  <section class="nav_bar bg-white">
    <div class="container">
      <nav class="navbar navbar-expand-lg navbar-light bg-white mb-0">
        <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
          <a href="index.html"> <img src="/newassets/images/khojlogo.png" alt=""></a>
          <button class="navbar-toggler float-right" type="button" data-toggle="collapse"
            data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
            aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
        </div>
        <div class="col-xl-9 col-lg-9 col-md-9 col-sm-12 col-12 ">
          <div class="collapse navbar-collapse" id="navbarSupportedContent">

            <ul class="navbar-nav ml-auto mr-auto">
              <li class="nav-item  active px-2 font-weight-bold">

                <a class="nav-link" href="index.html">Home <span class="sr-only">(current)</span></a>
              </li>

              <li class="nav-item dropdown font-weight-bold">
                <a class="nav-link dropdown-toggle" href="movies.html"  data-bs-toggle="dropdown">
                  Movies
                </a>
                <div class="dropdown-menu Drop_first" aria-labelledby="navbarDropdown">
                  <div class="container-fluid px-5">
                    <div class="row">
                      <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12 border-right">
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                          <h3 class="text-dark font-weight-bold py-3">Movie of the day</h3>
                          <img src="/newassets/images/dropimage.jpg" class="img-fluid" width="200" alt="">
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                          <h5 class="text-dark mt-5 pt-5">2017,Adventure comedy,Roman..</h5>
                          <h4 class="text-dark font-weight-bold pt-4">The Big Sick</h4>
                          <h5 class="text-dark  pt-2"> Pakistan-born comedian Kumail Nanjiani and grad student Emily Gardner fall in love but
                            struggle as their cultures clash. When Emily contracts a mysterious illness,</h5>
                         <button class="btn btn-link font-weight-bold mt-4 pl-0"><h4>Watch Now</h4></button>
                         <button class="btn font-weight-bold bg-light mt-4 pl-0"><h4>+ PLaylist</h4></button>
                        </div>

                      </div>
                      <div class="col-xl-2 col-lg-2 col-md-6 col-sm-6 col-12">
                        <h3 class="text-dark font-weight-bold py-3">Movie Genre</h3>
                        <ul class="pl-0">
<li class="list-unstyled"><a href ="#" class="text-dark " >Action</a></li>
<li class="list-unstyled"><a href ="" class="text-dark">Adventure</a></li>
<li class="list-unstyled"><a href ="" class="text-dark">Comedy</a></li>
<li class="list-unstyled"><a href ="" class="text-dark">Drama</a></li>
<li class="list-unstyled"><a href ="" class="text-dark">Sci-Fy</a></li>
<li class="list-unstyled"><a href ="" class="text-dark">Thriller</a></li>
<li class="list-unstyled"><a href ="" class="text-dark">Biography</a></li>
<li class="list-unstyled"><a href ="" class="text-dark">Family</a></li>
<li class="list-unstyled"><a href ="" class="text-dark">Romance</a></li>

                        </ul>
                      </div>
                      <div class="col-xl-2 col-lg-2 col-md-6 col-sm-6 col-12 border-right">
                        <ul class="pt-5 pl-0 mt-3">
                          <li class="list-unstyled"><a href ="#" class="text-dark" >Animation</a></li>
                          <li class="list-unstyled"><a href ="" class="text-dark">adventure</a></li>
                          <li class="list-unstyled"><a href ="" class="text-dark">Comedy</a></li>
                          <li class="list-unstyled"><a href ="" class="text-dark">Drama</a></li>
                          <li class="list-unstyled"><a href ="" class="text-dark">Sci-Fy</a></li>
                          <li class="list-unstyled"><a href ="" class="text-dark">Thriller</a></li>
                         
                          
                                                  </ul>
                      </div>
                      <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12">
                        <h3 class="text-dark font-weight-bold py-3">Movie Genre</h3>
                        <div id="carouselExampleControlsdrop2" class="carousel slide " data-ride="carousel">
                          <div class="carousel-inner">
                            <div class="carousel-item active">
                              <img class="img-fluid" src="/newassets/images/qqqq.jpg" alt="">
                            </div>
                            <div class="carousel-item">
                              <img class="img-fluid" src="/newassets/images/qqqq2.jpg" alt="">
                            </div>
                            <div class="carousel-item">
                              <img class="img-fluid" src="/newassets/images/qqqq3.jpg" alt="">
                            </div>
                          </div>
                          <a class="carousel-control-prev" href="#carouselExampleControlsdrop2" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                          </a>
                          <a class="carousel-control-next" href="#carouselExampleControlsdrop2" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                          </a>
                        </div>
                      </div>
                    </div>
                  </div>

                </div>
              </li>
              <li class="nav-item px-2  dropdown font-weight-bold">
                <a class="nav-link dropdown-toggle"  href="tvseries.html"  data-bs-toggle="dropdown">
                  Tv-series
                </a>
                <div class="dropdown-menu Second_drop" aria-labelledby="navbarDropdown22">
                  <div class="container-fluid px-5">
                    <div class="row">
                      <div class="col-xl-3 col-lg-3 col-md-12 col-sm-12 col-12 border-right">
                        <ul class="pl-0 pt-5 mt-2">
                          <li class="list-unstyled"><a href ="#" class="text-dark " >Trending Tv Shows</a></li>
                          <li class="list-unstyled"><a href ="" class="text-dark">Popular Now</a></li>
                          <li class="list-unstyled"><a href ="" class="text-dark">New This Month</a></li>
                          <li class="list-unstyled"><a href ="" class="text-dark">Khoj Exclusive</a></li>
                          <li class="list-unstyled"><a href ="" class="text-dark">Just For Kids</a></li>
                          <li class="list-unstyled"><a href ="" class="text-dark">Featured Shows</a></li>
                          <li class="list-unstyled"><a href ="" class="text-dark">Tv Shows Genre</a></li>
                          <li class="list-unstyled"><a href ="" class="text-dark">Tv Series:Genre</a></li>
                          <li class="list-unstyled"><a href ="" class="text-dark">Tv Series:Drama</a></li>
                          
                                                  </ul>

                      </div>

                      <div class="col-xl-9 col-lg-9 col-md-12 col-sm-12 col-12">
                        <h3 class="text-dark font-weight-bold py-3">Movie Genre</h3>
                        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                          <ol class="carousel-indicators">
                            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                           
                          </ol>
                          <div class="carousel-inner">
                            <div class="carousel-item active">
                            
                              <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-3">
                                <img class="img-fluid" src="/newassets/images/qqqq.jpg" alt="">
                                <h4 class="pt-4">Action,Drama,2015-2016</h4>
                                <h4 class="font-weight-bold">Chicago med</h4>
                                </div>

                                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-3">
                                  <img class="img-fluid" src="/newassets/images/qqqq2.jpg" alt="">
                                  <h4 class="pt-4">Action,Drama,2015-2016</h4>
                                  <h4 class="font-weight-bold">Chicago med</h4>
                                </div>
                                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-3">
                                  <img class="img-fluid" src="/newassets/images/qqqq3.jpg" alt="">
                                  <h4 class="pt-4">Action,Drama,2015-2016</h4>
                                  <h4 class="font-weight-bold">Chicago med</h4>
                                </div>
                                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-3">
                                  <img class="img-fluid" src="/newassets/images/qqqq.jpg" alt="">
                                  <h4 class="pt-4">Action,Drama,2015-2016</h4>
                                  <h4 class="font-weight-bold">Chicago med</h4>
                                </div>

                            </div>
                            <div class="carousel-item">
                              <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-3">
                                <img class="img-fluid" src="/newassets/images/qqqq.jpg" alt="">
                                <h4 class="pt-4">Action,Drama,2015-2016</h4>
                                <h4 class="font-weight-bold">Chicago med</h4>
                                </div>

                                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-3">
                                  <img class="img-fluid" src="/newassets/images/qqqq2.jpg" alt="">
                                  <h4 class="pt-4">Action,Drama,2015-2016</h4>
                                  <h4 class="font-weight-bold">Chicago med</h4>
                                </div>
                                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-3">
                                  <img class="img-fluid" src="/newassets/images/qqqq3.jpg" alt="">
                                  <h4 class="pt-4">Action,Drama,2015-2016</h4>
                                  <h4 class="font-weight-bold">Chicago med</h4>
                                </div>
                                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-3">
                                  <img class="img-fluid" src="/newassets/images/qqqq.jpg" alt="">
                                  <h4 class="pt-4">Action,Drama,2015-2016</h4>
                                  <h4 class="font-weight-bold">Chicago med</h4>
                                </div>
                            </div>
                           
                          </div>
                          <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                          </a>
                          <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                          </a>
                        </div>
                      </div>
                    </div>
                  </div>

                </div>
              </li>


              <li class="nav-item px-3 font-weight-bold">
                <a class="nav-link" href="free.html">Free</a>
              </li>
              <li class="nav-item px-3 font-weight-bold">
                <a class="nav-link" href="blog.html">Blog</a>
              </li>
            </ul>
            <form autocomplete="off" action="/action_page.php" style="max-width:450px;">
              <div class="input-icons autocomplete">
                <i class="fas fa-search icon">
                </i>
                <input id="myInput" class="pl-3" type="text" name="myCountry" placeholder="Search">
              </div>
            </form>
          </div>
        </div>
      </nav>

    </div>
  </section>

  <section class="banner">
    <div class="container-fluid">
      <div class="row">


        <div id="myCarousel1" class="carousel slide" data-ride="carousel">
          <div class="carousel-inner" role="listbox">
            <div class="item active">
              <img src="/newassets/images/banner.jpg" class="img-fluid" alt="Responsive image">
            </div>
            <div class="item">
              <img src="/newassets/images/banner2.jpg" class="img-fluid" alt="Responsive image">
            </div>
            <div class="item">
              <img src="/newassets/images/banner3.jpg" class="img-fluid" alt="Responsive image">
            </div>
            <div class="item">
              <img src="/newassets/images/banner4.jpg" class="img-fluid" alt="Responsive image">
            </div>-->
            <a class="left carousel-control" href="#myCarousel1" role="button" data-slide="prev">
              <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
              <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#myCarousel1" role="button" data-slide="next">
              <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
              <span class="sr-only">Next</span>
            </a>
          </div>

          <div id="thumbCarousel">


            <div class='demo-container'>
              <div class='carousel'>
                <input checked='checked' class='carousel__activator' id='carousel-slide-activator-1' name='carousel'
                  type='radio'>
                <input class='carousel__activator' id='carousel-slide-activator-2' name='carousel' type='radio'>
                <input class='carousel__activator' id='carousel-slide-activator-3' name='carousel' type='radio'>
                <div class='carousel__controls'>
                  <label class='carousel__control carousel__control--forward' for='carousel-slide-activator-2'>
                    <i class="fas fa-chevron-right text-white"></i>
                  </label>
                </div>
                <div class='carousel__controls'>
                  <label class='carousel__control carousel__control--backward' for='carousel-slide-activator-1'>
                    <i class="fas fa-chevron-left text-white"></i>
                  </label>
                  <label class='carousel__control carousel__control--forward' for='carousel-slide-activator-3'>
                    <i class="fas fa-chevron-right text-white"></i>
                  </label>
                </div>
                <div class='carousel__controls'>
                  <label class='carousel__control carousel__control--backward' for='carousel-slide-activator-2'>
                    <i class="fas fa-chevron-left text-white"></i>
                  </label>
                </div>
                <div class='carousel__screen'>
                  <div class='carousel__track'>
                    <div
                      class='carousel__item carousel__item--mobile-in-2 carousel__item--tablet-in-2 carousel__item--desktop-in-4'>
                      <div class='demo-content'>
                        <div data-target="#myCarousel1" data-slide-to="0" class="thumb active"><img
                            src="/newassets/images/gallery1.jpg" alt="" style="width: 230px; height: 180px"></div>
                      </div>
                    </div>
                    <div
                      class='carousel__item carousel__item--mobile-in-2 carousel__item--tablet-in-2 carousel__item--desktop-in-4'>
                      <div class='demo-content'>
                        <div data-target="#myCarousel1" data-slide-to="1" class="thumb"><img src="/newassets/images/gallery2.jpg"
                            alt="" style="width: 230px; height: 180px"></div>
                      </div>
                    </div>
                    <div
                      class='carousel__item carousel__item--mobile-in-2 carousel__item--tablet-in-2 carousel__item--desktop-in-4'>
                      <div class='demo-content'>
                        <div data-target="#myCarousel1" data-slide-to="2" class="thumb"><img src="/newassets/images/gallery3.jpg"
                            alt="" style="width: 230px; height: 180px"></div>
                      </div>
                    </div>
                    <div
                      class='carousel__item carousel__item--mobile-in-2 carousel__item--tablet-in-2 carousel__item--desktop-in-4'>
                      <div class='demo-content'>
                        <div data-target="#myCarousel1" data-slide-to="3" class="thumb"><img src="/newassets/images/gallery4.jpg"
                            alt="" style="width: 230px; height: 180px"></div>
                      </div>
                    </div>
                    <div
                      class='carousel__item carousel__item--mobile-in-2 carousel__item--tablet-in-2 carousel__item--desktop-in-4'>
                      <div class='demo-content'>
                        <div data-target="#myCarousel1" data-slide-to="1" class="thumb"><img src="/newassets/images/gallery2.jpg"
                            alt="" style="width: 230px; height: 180px"></div>
                      </div>
                    </div>
                    <div
                      class='carousel__item carousel__item--mobile-in-2 carousel__item--tablet-in-2 carousel__item--desktop-in-4'>
                      <div class='demo-content'>
                        <div data-target="#myCarousel1" data-slide-to="3" class="thumb"><img src="/newassets/images/gallery4.jpg"
                            alt="" style="width: 230px; height: 180px"></div>
                      </div>
                    </div>
                    <div
                      class='carousel__item carousel__item--mobile-in-2 carousel__item--tablet-in-2 carousel__item--desktop-in-4'>
                      <div class='demo-content'>
                        <div data-target="#myCarousel1" data-slide-to="3" class="thumb"><img src="/newassets/images/gallery4.jpg"
                            alt="" style="width: 230px; height: 180px"></div>
                      </div>
                    </div>
                    <div
                      class='carousel__item carousel__item--mobile-in-2 carousel__item--tablet-in-2 carousel__item--desktop-in-4'>
                      <div class='demo-content'>
                        <div data-target="#myCarousel1" data-slide-to="2" class="thumb"><img src="/newassets/images/gallery3.jpg"
                            alt="" style="width: 230px; height: 180px"></div>
                      </div>
                    </div>
                    <div
                      class='carousel__item carousel__item--mobile-in-2 carousel__item--tablet-in-2 carousel__item--desktop-in-4'>
                      <div class='demo-content'>
                        <div data-target="#myCarousel1" data-slide-to="2" class="thumb"><img src="/newassets/images/gallery3.jpg"
                            alt="" style="width: 230px; height: 180px"></div>
                      </div>
                    </div>
                    <div
                      class='carousel__item carousel__item--mobile-in-2 carousel__item--tablet-in-2 carousel__item--desktop-in-4'>
                      <div class='demo-content'>
                        <div data-target="#myCarousel1" data-slide-to="3" class="thumb"><img src="/newassets/images/gallery4.jpg"
                            alt="" style="width: 230px; height: 180px"></div>
                      </div>
                    </div>
                    <div
                      class='carousel__item carousel__item--mobile-in-2 carousel__item--tablet-in-2 carousel__item--desktop-in-4'>
                      <div class='demo-content'>
                        <div data-target="#myCarousel1" data-slide-to="1" class="thumb"><img src="/newassets/images/gallery2.jpg"
                            alt="" style="width: 230px; height: 180px"></div>
                      </div>
                    </div>
                    <div
                      class='carousel__item carousel__item--mobile-in-2 carousel__item--tablet-in-2 carousel__item--desktop-in-4'>
                      <div class='demo-content'>
                        <div data-target="#myCarousel1" data-slide-to="3" class="thumb"><img src="/newassets/images/gallery4.jpg"
                            alt="" style="width: 230px; height: 180px"></div>
                      </div>
                    </div>

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    </div>
  </section>

  <section class="streaming mb-5">
    <div class="container">
      <div class="container">
        <div class="row">
          <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 stream">
            <h1 class="font-weight-bold">Streaming services/</h1>
            <h2 class="ott">&nbsp;(Select your favourite OTT and
              search)</h2>
          </div>
        </div>
      </div>
      <div class="container">
        <div class="row mt-5 mb-5">
          <div id="carouselExampleControls" class="carousel slide" data-ride="carousel" data-interval="false">
            <div class="carousel-inner">
              <div class="carousel-item active">
                <div class="row ">
                  <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1 col-1">

                  </div>
                  <div class="col-xl-1 col-lg-1 col-md-2 col-sm-2 col-2">
                    <a href="#"> <img src="/newassets/images/mxplayer.png" style="width: 100% ; border-radius: 20px;"
                        alt="..."></a>
                  </div>
                  <div class="col-xl-1 col-lg-1 col-md-2 col-sm-2 col-2">
                    <a href="#"> <img src="/newassets/images/voot.jpg" style="width: 100%; border-radius: 20px;" alt="..."></a>
                  </div>
                  <div class="col-xl-1 col-lg-1 col-md-2 col-sm-2 col-2">
                    <a href="#"> <img src="/newassets/images/ullu.png" style="width: 100%; border-radius: 20px;" alt="..."></a>
                  </div>
                  <div class="col-xl-1 col-lg-1 col-md-2 col-sm-2 col-2">
                    <a href="#"> <img src="/newassets/images/netflix.jpg" style="width: 100% ; border-radius: 20px;" alt="..."></a>
                  </div>
                  <div class="col-xl-1 col-lg-1 col-md-2 col-sm-2 col-2">
                    <a href="#"> <img src="/newassets/images/altbalaji.jpg" style="width: 100%; border-radius: 20px;"
                        alt="..."></a>
                  </div>
                  <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1 col-1 dnone non">
                    <a href="#"> <img src="/newassets/images/prime.jpg" style="width: 100%; border-radius: 20px;" alt="..."></a>
                  </div>
                  <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1 col-1 dnone non">
                    <a href="#"> <img src="/newassets/images/sony.jpg" style="width: 100%; border-radius: 20px;" alt="..."></a>
                  </div>
                  <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1 col-1 dnone non">
                    <a href="#"> <img src="/newassets/images/disnep.jpg" style="width: 100%; border-radius: 20px;" alt="..."></a>
                  </div>
                  <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1 col-1 dnone non">
                    <a href="#"> <img src="/newassets/images/eros.jpg" style="width: 100%; border-radius: 20px;" alt="..."></a>
                  </div>
                  <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1 col-1 dnone non ">
                    <a href="#"> <img src="/newassets/images/ullu.png" style="width: 100%; border-radius: 20px;" alt="..."></a>
                  </div>
                  <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1 col-1 ">

                  </div>
                </div>
              </div>
              <div class="carousel-item">
                <div class="row  ">
                  <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1 col-1">

                  </div>

                  <div class="col-xl-1 col-lg-1 col-md-2 col-sm-2 col-2">
                    <a href="#"> <img src="/newassets/images/prime.jpg" style="width: 100%; border-radius: 20px;" alt="..."></a>
                  </div>
                  <div class="col-xl-1 col-lg-1 col-md-2 col-sm-2 col-2">
                    <a href="#"> <img src="/newassets/images/sony.jpg" style="width: 100%; border-radius: 20px;" alt="..."></a>
                  </div>
                  <div class="col-xl-1 col-lg-1 col-md-2 col-sm-2 col-2">
                    <a href="#"> <img src="/newassets/images/disnep.jpg" style="width: 100%; border-radius: 20px;" alt="..."></a>
                  </div>
                  <div class="col-xl-1 col-lg-1 col-md-2 col-sm-2 col-2">
                    <a href="#"> <img src="/newassets/images/eros.jpg" style="width: 100%; border-radius: 20px;" alt="..."></a>
                  </div>
                  <div class="col-xl-1 col-lg-1 col-md-2 col-sm-2 col-2">
                    <a href="#"> <img src="/newassets/images/ullu.png" style="width: 100%; border-radius: 20px;" alt="..."></a>
                  </div>
                  <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1 col-1 dnone non">
                    <a href="#"> <img src="/newassets/images/mxplayer.png" style="width: 100% ; border-radius: 20px;"
                        alt="..."></a>
                  </div>
                  <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1 col-1 dnone non ">
                    <a href="#"> <img src="/newassets/images/voot.jpg" style="width: 100%; border-radius: 20px;" alt="..."></a>
                  </div>
                  <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1 col-1 dnone non">
                    <a href="#"> <img src="/newassets/images/ullu.png" style="width: 100%; border-radius: 20px;" alt="..."></a>
                  </div>
                  <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1 col-1 dnone non ">
                    <a href="#"> <img src="/newassets/images/netflix.jpg" style="width: 100% ; border-radius: 20px;" alt="..."></a>
                  </div>
                  <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1 col-1 dnone non">
                    <a href="#"> <img src="/newassets/images/altbalaji.jpg" style="width: 100%; border-radius: 20px;"
                        alt="..."></a>
                  </div>
                  <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1 col-1">

                  </div>

                </div>
              </div>

            </div>
            <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
              <span class="carousel-control-prev-icon" aria-hidden="true"></span>
              <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
              <span class="carousel-control-next-icon" aria-hidden="true"></span>
              <span class="sr-only">Next</span>
            </a>

          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="popular ">
    <div class="container tele pt-5 pb-5">

      <div class="row">

        <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12  ">
          <div class="today acti"> <a class="nav-link active  Text-white pl-0" href="#">Today /</a>
            <a class="nav-link active  Text-white" href="#">This Week / </a>
            <a class="nav-link active  Text-white" href="#">Last 30 Days /</a>
          </div>
          <h1 class="font-weight-bold pt-3 colm ">Popular Movies to watch Now</h1>
          <h3 class="pt-3 ">Most Watched Movies By Days </h3>
        </div>
        <div class="col-xl-8 col-lg-8 col-md-12 col-sm-12 col-12">
          <div class="row">
            <div class="col-md-12">
              <p class="view "> <a class="nav-link active Text-white" href="#">View More</a></p>
              <div id="carouselExampleControls2" class="carousel slide" data-ride="carousel" data-interval="false">
                <div class="carousel-inner">
                  <div class="carousel-item active">
                    <div class="row ">
                      <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12 ksiwc hover">
                        <a href="#"> <img src="/newassets/images/img1.png" style="width: 100% ; " alt="...">
                        </a>

                        <div class="position_effect">
                          <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                              class="fas fa-check"></i></button>
                          <button type="button" class="btn btn-light css-ksiwc2 seentext">Seen it</button>
                        </div>
                        <div class="position_effect2">
                          <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                              class="fas fa-plus"></i></button>
                          <button type="button" class="btn btn-light css-ksiwc2 seentext">+ Want to see</button>
                        </div>
                        <div class="position_effect3">
                          <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                              class="fas fa-star"></i></button>
                          <button type="button" class="btn btn-light css-ksiwc2 seentext"><i class="fas fa-star"></i><i
                              class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i
                              class="fas fa-star"></i></button>
                        </div>

                        <h4 class="pt-3 text-white">2017,Hindi ,Action</h4>
                        <p class="font-weight-bold text-white">Maidaan (Inspired By A True Story) in Hindi</p>
                        <div class="overlay3">
                          <h1 class="pt-3 text font-weight-bold">Gully Boy</h1>
                          <h1 class="pt-3 text2 font-italic ">Khoj says</h1>
                          <img src="/newassets/images/check.png" class="text3" alt="...">
                          <h1 class="pt-3 text4 ">Age 8+</h1><img src="/newassets/images/rating.png" class="text5" alt="...">
                          <h1 class="pt-3 text6 ">Maidaan is an upcoming Indian Hindi-language biographical sports</h1>
                          <h1 class="pt-3 text7 ">Full Expert views</h1>
                          <img src="/newassets/images/play.png" class="text8" alt="...">
                          <h1 class="pt-3 text9 ">Video Reviews</h1>
                        </div>
                      </div>
                      <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12 ksiwc none hover">
                        <a href="#"> <img src="/newassets/images/img2.png" style="width: 100%; " alt="..."></a>
                        <div class="position_effect">
                          <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                              class="fas fa-check"></i></button>
                          <button type="button" class="btn btn-light css-ksiwc2 seentext">Seen it</button>
                        </div>


                        <div class="position_effect2">
                          <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                              class="fas fa-plus"></i></button>
                          <button type="button" class="btn btn-light css-ksiwc2 seentext">+ Want to see</button>
                        </div>
                        <div class="position_effect3">
                          <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                              class="fas fa-star"></i></button>
                          <button type="button" class="btn btn-light css-ksiwc2 seentext"><i class="fas fa-star"></i><i
                              class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i
                              class="fas fa-star"></i></button>
                        </div>

                        <h4 class="pt-3 text-white">2017,Hindi ,Action</h4>
                        <p class="font-weight-bold text-white">Gully Boy</p>
                        <div class="overlay3">
                          <h1 class="pt-3 text font-weight-bold">Gully Boy</h1>
                          <h1 class="pt-3 text2 font-italic ">Khoj says</h1>
                          <img src="/newassets/images/check.png" class="text3" alt="...">
                          <h1 class="pt-3 text4 ">Age 8+</h1><img src="/newassets/images/rating.png" class="text5" alt="...">
                          <h1 class="pt-3 text6 ">Maidaan is an upcoming Indian Hindi-language biographical sports</h1>
                          <h1 class="pt-3 text7 ">Full Expert views</h1>
                          <img src="/newassets/images/play.png" class="text8" alt="...">
                          <h1 class="pt-3 text9 ">Video Reviews</h1>
                        </div>
                      </div>
                      <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4 none dnone  ksiwc  hover">
                        <a href="#"> <img src="/newassets/images/img3.png" style="width: 100%; " alt="..."></a>
                        <div class="position_effect">
                          <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                              class="fas fa-check"></i></button>
                          <button type="button" class="btn btn-light css-ksiwc2 seentext">Seen it</button>
                        </div>


                        <div class="position_effect2">
                          <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                              class="fas fa-plus"></i></button>
                          <button type="button" class="btn btn-light css-ksiwc2 seentext">+ Want to see</button>
                        </div>
                        <div class="position_effect3">
                          <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                              class="fas fa-star"></i></button>
                          <button type="button" class="btn btn-light css-ksiwc2 seentext"><i class="fas fa-star"></i><i
                              class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i
                              class="fas fa-star"></i></button>
                        </div>

                        <h4 class="pt-3 text-white">2017,Hindi ,Action</h4>
                        <p class="font-weight-bold text-white">Gully Boy</p>
                        <div class="overlay3">
                          <h1 class="pt-3 text font-weight-bold">Gully Boy</h1>
                          <h1 class="pt-3 text2 font-italic ">Khoj says</h1>
                          <img src="/newassets/images/check.png" class="text3" alt="...">
                          <h1 class="pt-3 text4 ">Age 8+</h1><img src="/newassets/images/rating.png" class="text5" alt="...">
                          <h1 class="pt-3 text6 ">Maidaan is an upcoming Indian Hindi-language biographical sports</h1>
                          <h1 class="pt-3 text7 ">Full Expert views</h1>
                          <img src="/newassets/images/play.png" class="text8" alt="...">
                          <h1 class="pt-3 text9 ">Video Reviews</h1>
                        </div>
                      </div>

                    </div>
                  </div>
                  <div class="carousel-item">
                    <div class="row ">
                      <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12 ksiwc none hover">
                        <a href="#"> <img src="/newassets/images/img1.png" style="width: 100% ; " alt="...">
                        </a>
                        <div class="position_effect">
                          <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                              class="fas fa-check"></i></button>
                          <button type="button" class="btn btn-light css-ksiwc2 seentext">Seen it</button>
                        </div>


                        <div class="position_effect2">
                          <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                              class="fas fa-plus"></i></button>
                          <button type="button" class="btn btn-light css-ksiwc2 seentext">+ Want to see</button>
                        </div>
                        <div class="position_effect3">
                          <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                              class="fas fa-star"></i></button>
                          <button type="button" class="btn btn-light css-ksiwc2 seentext"><i class="fas fa-star"></i><i
                              class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i
                              class="fas fa-star"></i></button>
                        </div>

                        <h4 class="pt-3 text-white">2017,Hindi ,Action</h4>
                        <p class="font-weight-bold text-white">Gully Boy</p>
                        <div class="overlay3">
                          <h1 class="pt-3 text font-weight-bold">Gully Boy</h1>
                          <h1 class="pt-3 text2 font-italic ">Khoj says</h1>
                          <img src="/newassets/images/check.png" class="text3" alt="...">
                          <h1 class="pt-3 text4 ">Age 8+</h1><img src="/newassets/images/rating.png" class="text5" alt="...">
                          <h1 class="pt-3 text6 ">Maidaan is an upcoming Indian Hindi-language biographical sports</h1>
                          <h1 class="pt-3 text7 ">Full Expert views</h1>
                          <img src="/newassets/images/play.png" class="text8" alt="...">
                          <h1 class="pt-3 text9 ">Video Reviews</h1>
                        </div>
                      </div>
                      <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12 ksiwc  hover">
                        <a href="#"> <img src="/newassets/images/img2.png" style="width: 100%; " alt="..."></a>
                        <div class="position_effect">
                          <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                              class="fas fa-check"></i></button>
                          <button type="button" class="btn btn-light css-ksiwc2 seentext">Seen it</button>
                        </div>


                        <div class="position_effect2">
                          <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                              class="fas fa-plus"></i></button>
                          <button type="button" class="btn btn-light css-ksiwc2 seentext">+ Want to see</button>
                        </div>
                        <div class="position_effect3">
                          <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                              class="fas fa-star"></i></button>
                          <button type="button" class="btn btn-light css-ksiwc2 seentext"><i class="fas fa-star"></i><i
                              class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i
                              class="fas fa-star"></i></button>
                        </div>

                        <h4 class="pt-3 text-white">2017,Hindi ,Action</h4>
                        <p class="font-weight-bold text-white">Gully Boy</p>
                        <div class="overlay3">
                          <h1 class="pt-3 text font-weight-bold">Gully Boy</h1>
                          <h1 class="pt-3 text2 font-italic ">Khoj says</h1>
                          <img src="/newassets/images/check.png" class="text3" alt="...">
                          <h1 class="pt-3 text4 ">Age 8+</h1><img src="/newassets/images/rating.png" class="text5" alt="...">
                          <h1 class="pt-3 text6 ">Maidaan is an upcoming Indian Hindi-language biographical sports</h1>
                          <h1 class="pt-3 text7 ">Full Expert views</h1>
                          <img src="/newassets/images/play.png" class="text8" alt="...">
                          <h1 class="pt-3 text9 ">Video Reviews</h1>
                        </div>
                      </div>
                      <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4 none  ksiwc dnone hover">
                        <a href="#"> <img src="/newassets/images/img3.png" style="width: 100%; " alt="..."></a>
                        <div class="position_effect">
                          <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                              class="fas fa-check"></i></button>
                          <button type="button" class="btn btn-light css-ksiwc2 seentext">Seen it</button>
                        </div>


                        <div class="position_effect2">
                          <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                              class="fas fa-plus"></i></button>
                          <button type="button" class="btn btn-light css-ksiwc2 seentext">+ Want to see</button>
                        </div>
                        <div class="position_effect3">
                          <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                              class="fas fa-star"></i></button>
                          <button type="button" class="btn btn-light css-ksiwc2 seentext"><i class="fas fa-star"></i><i
                              class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i
                              class="fas fa-star"></i></button>
                        </div>

                        <h4 class="pt-3 text-white">2017,Hindi ,Action</h4>
                        <p class="font-weight-bold text-white">Gully Boy</p>
                        <div class="overlay3">
                          <h1 class="pt-3 text font-weight-bold">Gully Boy</h1>
                          <h1 class="pt-3 text2 font-italic ">Khoj says</h1>
                          <img src="/newassets/images/check.png" class="text3" alt="...">
                          <h1 class="pt-3 text4 ">Age 8+</h1><img src="/newassets/images/rating.png" class="text5" alt="...">
                          <h1 class="pt-3 text6 ">Maidaan is an upcoming Indian Hindi-language biographical sports</h1>
                          <h1 class="pt-3 text7 ">Full Expert views</h1>
                          <img src="/newassets/images/play.png" class="text8" alt="...">
                          <h1 class="pt-3 text9 ">Video Reviews</h1>
                        </div>
                      </div>

                    </div>
                  </div>
                </div>
                <a class="carousel-control-prev " href="#carouselExampleControls2" role="button" data-slide="prev">
                  <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                  <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next " href="#carouselExampleControls2" role="button" data-slide="next">
                  <span class="carousel-control-next-icon" aria-hidden="true"></span>
                  <span class="sr-only">Next</span>
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="tv_series">
    <div class="container  pt-5 pb-5">
      <div class="row">
        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-8 col-12">
          <div class="today"> <a class="nav-link active  Text-white" href="#">Today /</a>
            <a class="nav-link active  Text-white" href="#">This Week / </a>
            <a class="nav-link active  Text-white" href="#">Last 30 Days /</a>
          </div>
        </div>

        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-4 col-12">

          <p class="view"> <a class="nav-link active Text-white" href="#">View More</a></p>


        </div>
      </div>

      <div class="container">
        <div class="row">

          <div class="col-xl-8 col-lg-8 col-md-12 col-sm-12 col-12 order-xl-1 order-lg-1 order-md-2 order-sm-2 order-2">

            <div class="container">
              <div class="row">
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12 ksiwc hover">
                  <div class="text-left text-white">
                    <a href="#"> <img src="/newassets/images/img1.png" style="width: 90% ; " alt="..."></a>
                    <div class="position_effectA">
                      <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                          class="fas fa-check"></i></button>
                      <button type="button" class="btn btn-light css-ksiwc2 seentext">Seen it</button>
                    </div>
                    <div class="position_effectB">
                      <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                          class="fas fa-plus"></i></button>
                      <button type="button" class="btn btn-light css-ksiwc2 seentext">+ Want to see</button>
                    </div>
                    <div class="position_effectC">
                      <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                          class="fas fa-star"></i></button>
                      <button type="button" class="btn btn-light css-ksiwc2 seentext"><i class="fas fa-star"></i><i
                          class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i
                          class="fas fa-star"></i></button>
                    </div>
                    <h4 class="pt-3">2017,Hindi ,Action</h4>
                    <p class="font-weight-bold">Gully Boy</p>
                    <div class="overlay3">
                      <h1 class="pt-3 text font-weight-bold">Gully Boy</h1>
                      <h1 class="pt-3 text2 font-italic ">Khoj says</h1>
                      <img src="/newassets/images/check.png" class="text3" alt="...">
                      <h1 class="pt-3 text4 ">Age 8+</h1><img src="/newassets/images/rating.png" class="text5" alt="...">
                      <h1 class="pt-3 text6 ">Maidaan is an upcoming Indian Hindi-language biographical sports</h1>
                      <h1 class="pt-3 text7 ">Full Expert views</h1>
                      <img src="/newassets/images/play.png" class="text8" alt="...">
                      <h1 class="pt-3 text9 ">Video Reviews</h1>
                    </div>
                  </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12  ksiwc hover">
                  <div class="text-left text-white">
                    <a href="#"> <img src="/newassets/images/img4.png" style="width: 90% ; " alt="..."></a>
                    <div class="position_effectA">
                      <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                          class="fas fa-check"></i></button>
                      <button type="button" class="btn btn-light css-ksiwc2 seentext">Seen it</button>
                    </div>


                    <div class="position_effectB">
                      <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                          class="fas fa-plus"></i></button>
                      <button type="button" class="btn btn-light css-ksiwc2 seentext">+ Want to see</button>
                    </div>
                    <div class="position_effectC">
                      <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                          class="fas fa-star"></i></button>
                      <button type="button" class="btn btn-light css-ksiwc2 seentext"><i class="fas fa-star"></i><i
                          class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i
                          class="fas fa-star"></i></button>
                    </div>
                    <h4 class="pt-3">2017,Hindi ,Action</h4>
                    <p class="font-weight-bold">Gully Boy</p>
                    <div class="overlay3">
                      <h1 class="pt-3 text font-weight-bold">Gully Boy</h1>
                      <h1 class="pt-3 text2 font-italic ">Khoj says</h1>
                      <img src="/newassets/images/check.png" class="text3" alt="...">
                      <h1 class="pt-3 text4 ">Age 8+</h1><img src="/newassets/images/rating.png" class="text5" alt="...">
                      <h1 class="pt-3 text6 ">Maidaan is an upcoming Indian Hindi-language biographical sports</h1>
                      <h1 class="pt-3 text7 ">Full Expert views</h1>
                      <img src="/newassets/images/play.png" class="text8" alt="...">
                      <h1 class="pt-3 text9 ">Video Reviews</h1>
                    </div>
                  </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 dnone col-12 ksiwc hover">
                  <div class="text-left text-white">
                    <a href="#"> <img src="/newassets/images/img5.png" style="width: 90% ; " alt="..."></a>
                    <div class="position_effectA">
                      <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                          class="fas fa-check"></i></button>
                      <button type="button" class="btn btn-light css-ksiwc2 seentext">Seen it</button>
                    </div>


                    <div class="position_effectB">
                      <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                          class="fas fa-plus"></i></button>
                      <button type="button" class="btn btn-light css-ksiwc2 seentext">+ Want to see</button>
                    </div>
                    <div class="position_effectC">
                      <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                          class="fas fa-star"></i></button>
                      <button type="button" class="btn btn-light css-ksiwc2 seentext"><i class="fas fa-star"></i><i
                          class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i
                          class="fas fa-star"></i></button>
                    </div>
                    <h4 class="pt-3">2017,Hindi ,Action</h4>
                    <p class="font-weight-bold">Gully Boy</p>
                    <div class="overlay3">
                      <h1 class="pt-3 text font-weight-bold">Gully Boy</h1>
                      <h1 class="pt-3 text2 font-italic ">Khoj says</h1>
                      <img src="/newassets/images/check.png" class="text3" alt="...">
                      <h1 class="pt-3 text4 ">Age 8+</h1><img src="/newassets/images/rating.png" class="text5" alt="...">
                      <h1 class="pt-3 text6 ">Maidaan is an upcoming Indian Hindi-language biographical sports</h1>
                      <h1 class="pt-3 text7 ">Full Expert views</h1>
                      <img src="/newassets/images/play.png" class="text8" alt="...">
                      <h1 class="pt-3 text9 ">Video Reviews</h1>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12 order-xl-2 order-lg-2 order-md-1 order-sm-1 order-1">

            <h1 class="font-weight-bold pt-3 head">Popular Tv Series watch Now</h1>
            <h3 class="pt-3 pb-5">Most Watched Movies By Days </h3>
          </div>
        </div>
      </div>


      <div class="row images">
        <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-12 ksiwc hover">
          <div class="text-left text-white ">
            <a href="#"> <img src="/newassets/images/pic1.png" style="width: 100% ; " alt="..."></a>
            <div class="position_effect">
              <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i class="fas fa-check"></i></button>
              <button type="button" class="btn btn-light css-ksiwc2 seentext">Seen it</button>
            </div>


            <div class="position_effect2">
              <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i class="fas fa-plus"></i></button>
              <button type="button" class="btn btn-light css-ksiwc2 seentext">+ Want to see</button>
            </div>
            <div class="position_effect3">
              <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i class="fas fa-star"></i></button>
              <button type="button" class="btn btn-light css-ksiwc2 seentext"><i class="fas fa-star"></i><i
                  class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i
                  class="fas fa-star"></i></button>
            </div>
            <h4 class="pt-3">2017,Hindi ,Action</h4>
            <p class="font-weight-bold">Gully Boy</p>
            <div class="overlay2">
              <h1 class="pt-3 tex font-weight-bold">Gully Boy</h1>
              <h1 class="pt-3 text2 font-italic ">Khoj says</h1>
              <img src="/newassets/images/check.png" class="text3" alt="...">
              <h1 class="pt-3 text4 ">Age 8+</h1><img src="/newassets/images/rating.png" class="text5" alt="...">
              <h1 class="pt-3 tex6 ">Maidaan is an upcoming Indian Hindi-language biographical sports</h1>
              <h1 class="pt-3 text7 ">Full Expert views</h1>
              <img src="/newassets/images/play.png" class="text8" alt="...">
              <h1 class="pt-3 tex9 ">Video Reviews</h1>
            </div>
          </div>
        </div>
        <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-12 ksiwc  hover">
          <div class="text-left text-white">
            <a href="#"> <img src="/newassets/images/pic2.png" style="width: 100% ; " alt="..."></a>
            <div class="position_effect">
              <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i class="fas fa-check"></i></button>
              <button type="button" class="btn btn-light css-ksiwc2 seentext">Seen it</button>
            </div>


            <div class="position_effect2">
              <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i class="fas fa-plus"></i></button>
              <button type="button" class="btn btn-light css-ksiwc2 seentext">+ Want to see</button>
            </div>
            <div class="position_effect3">
              <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i class="fas fa-star"></i></button>
              <button type="button" class="btn btn-light css-ksiwc2 seentext"><i class="fas fa-star"></i><i
                  class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i
                  class="fas fa-star"></i></button>
            </div>
            <h4 class="pt-3">2017,Hindi ,Action</h4>
            <p class="font-weight-bold">Gully Boy</p>
            <div class="overlay2">
              <h1 class="pt-3 tex font-weight-bold">Gully Boy</h1>
              <h1 class="pt-3 text2 font-italic ">Khoj says</h1>
              <img src="/newassets/images/check.png" class="text3" alt="...">
              <h1 class="pt-3 text4 ">Age 8+</h1><img src="/newassets/images/rating.png" class="text5" alt="...">
              <h1 class="pt-3 tex6 ">Maidaan is an upcoming Indian Hindi-language biographical sports</h1>
              <h1 class="pt-3 text7 ">Full Expert views</h1>
              <img src="/newassets/images/play.png" class="text8" alt="...">
              <h1 class="pt-3 tex9 ">Video Reviews</h1>
            </div>
          </div>
        </div>
        <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-6 none ksiwc  hover ">
          <div class="text-left text-white">
            <a href="#"> <img src="/newassets/images/pic3.png" style="width: 100% ; " alt="..."></a>
            <div class="position_effect">
              <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i class="fas fa-check"></i></button>
              <button type="button" class="btn btn-light css-ksiwc2 seentext">Seen it</button>
            </div>


            <div class="position_effect2">
              <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i class="fas fa-plus"></i></button>
              <button type="button" class="btn btn-light css-ksiwc2 seentext">+ Want to see</button>
            </div>
            <div class="position_effect3">
              <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i class="fas fa-star"></i></button>
              <button type="button" class="btn btn-light css-ksiwc2 seentext"><i class="fas fa-star"></i><i
                  class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i
                  class="fas fa-star"></i></button>
            </div>
            <h4 class="pt-3">2017,Hindi ,Action</h4>
            <p class="font-weight-bold">Gully Boy</p>
            <div class="overlay2">
              <h1 class="pt-3 tex font-weight-bold">Gully Boy</h1>
              <h1 class="pt-3 text2 font-italic ">Khoj says</h1>
              <img src="/newassets/images/check.png" class="text3" alt="...">
              <h1 class="pt-3 text4 ">Age 8+</h1><img src="/newassets/images/rating.png" class="text5" alt="...">
              <h1 class="pt-3 tex6 ">Maidaan is an upcoming Indian Hindi-language biographical sports</h1>
              <h1 class="pt-3 text7 ">Full Expert views</h1>
              <img src="/newassets/images/play.png" class="text8" alt="...">
              <h1 class="pt-3 tex9 ">Video Reviews</h1>
            </div>
          </div>
        </div>
        <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-6 none ksiwc hover">
          <div class="text-left text-white">
            <a href="#"> <img src="/newassets/images/pic3.png" style="width: 100% ; " alt="..."></a>
            <div class="position_effect">
              <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i class="fas fa-check"></i></button>
              <button type="button" class="btn btn-light css-ksiwc2 seentext">Seen it</button>
            </div>


            <div class="position_effect2">
              <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i class="fas fa-plus"></i></button>
              <button type="button" class="btn btn-light css-ksiwc2 seentext">+ Want to see</button>
            </div>
            <div class="position_effect3">
              <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i class="fas fa-star"></i></button>
              <button type="button" class="btn btn-light css-ksiwc2 seentext"><i class="fas fa-star"></i><i
                  class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i
                  class="fas fa-star"></i></button>
            </div>
            <h4 class="pt-3">2017,Hindi ,Action</h4>
            <p class="font-weight-bold">Gully Boy</p>
            <div class="overlay2">
              <h1 class="pt-3 tex font-weight-bold">Gully Boy</h1>
              <h1 class="pt-3 text2 font-italic ">Khoj says</h1>
              <img src="/newassets/images/check.png" class="text3" alt="...">
              <h1 class="pt-3 text4 ">Age 8+</h1><img src="/newassets/images/rating.png" class="text5" alt="...">
              <h1 class="pt-3 tex6 ">Maidaan is an upcoming Indian Hindi-language biographical sports</h1>
              <h1 class="pt-3 text7 ">Full Expert views</h1>
              <img src="/newassets/images/play.png" class="text8" alt="...">
              <h1 class="pt-3 tex9 ">Video Reviews</h1>
            </div>
          </div>
        </div>
      </div>
      <div class="row justify-content-center pt-5">
        <button type="button" class="btn btn-primary btn-lg text-center shadow-lg ">+ View More</button>
      </div>
    </div>
  </section>

  <section class="add">
    <div class="container ">
      <div class="row add1">
        <img src="/newassets/images/add1.jpg" style="width: 100% ; " alt="...">
      </div>
    </div>

  </section>

  <section class="romantic ">
    <div class="container tele pt-5 pb-5">
      <div class="row">
        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-8 col-12">
          <div class="today"> <a class="nav-link active  Text-white" href="#">Today /</a>
            <a class="nav-link active  Text-white" href="#">This Week / </a>
            <a class="nav-link active  Text-white" href="#">Last 30 Days /</a>
          </div>
        </div>

        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-4 col-12">

          <p class="view"> <a class="nav-link active Text-white" href="#">View More</a></p>


        </div>
      </div>
      <div class="container">
        <div class="row">
          <div class="col-xl-8 col-lg-8 col-md-12 col-sm-12 col-12 order-xl-1 order-lg-1 order-md-2 order-sm-2 order-2">
            <div class="row popu">
              <div id="carouselExampleControls3" class="carousel slide " data-ride="carousel" data-interval="false">
                <div class="carousel-inner">
                  <div class="carousel-item active">
                    <div class="row ">
                      <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12 ksiwc hover">
                        <a href="#"> <img src="/newassets/images/img1.png" style="width: 100% ; " alt="...">
                        </a>
                        <div class="position_effect">
                          <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                              class="fas fa-check"></i></button>
                          <button type="button" class="btn btn-light css-ksiwc2 seentext">Seen it</button>
                        </div>


                        <div class="position_effect2">
                          <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                              class="fas fa-plus"></i></button>
                          <button type="button" class="btn btn-light css-ksiwc2 seentext">+ Want to see</button>
                        </div>
                        <div class="position_effect3">
                          <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                              class="fas fa-star"></i></button>
                          <button type="button" class="btn btn-light css-ksiwc2 seentext"><i class="fas fa-star"></i><i
                              class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i
                              class="fas fa-star"></i></button>
                        </div>

                        <h4 class="pt-3 text-white">2017,Hindi ,Action</h4>
                        <p class="font-weight-bold text-white">Gully Boy</p>
                        <div class="overlay3">
                          <h1 class="pt-3 text font-weight-bold">Gully Boy</h1>
                          <h1 class="pt-3 text2 font-italic ">Khoj says</h1>
                          <img src="/newassets/images/check.png" class="text3" alt="...">
                          <h1 class="pt-3 text4 ">Age 8+</h1><img src="/newassets/images/rating.png" class="text5" alt="...">
                          <h1 class="pt-3 text6 ">Maidaan is an upcoming Indian Hindi-language biographical sports</h1>
                          <h1 class="pt-3 text7 ">Full Expert views</h1>
                          <img src="/newassets/images/play.png" class="text8" alt="...">
                          <h1 class="pt-3 text9 ">Video Reviews</h1>
                        </div>
                      </div>
                      <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-6 none ksiwc hover">
                        <a href="#"> <img src="/newassets/images/img2.png" style="width: 100%; " alt="..."></a>
                        <div class="position_effect">
                          <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                              class="fas fa-check"></i></button>
                          <button type="button" class="btn btn-light css-ksiwc2 seentext">Seen it</button>
                        </div>


                        <div class="position_effect2">
                          <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                              class="fas fa-plus"></i></button>
                          <button type="button" class="btn btn-light css-ksiwc2 seentext">+ Want to see</button>
                        </div>
                        <div class="position_effect3">
                          <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                              class="fas fa-star"></i></button>
                          <button type="button" class="btn btn-light css-ksiwc2 seentext"><i class="fas fa-star"></i><i
                              class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i
                              class="fas fa-star"></i></button>
                        </div>

                        <h4 class="pt-3 text-white">2017,Hindi ,Action</h4>
                        <p class="font-weight-bold text-white">Gully Boy</p>
                        <div class="overlay3">
                          <h1 class="pt-3 text font-weight-bold">Gully Boy</h1>
                          <h1 class="pt-3 text2 font-italic ">Khoj says</h1>
                          <img src="/newassets/images/check.png" class="text3" alt="...">
                          <h1 class="pt-3 text4 ">Age 8+</h1><img src="/newassets/images/rating.png" class="text5" alt="...">
                          <h1 class="pt-3 text6 ">Maidaan is an upcoming Indian Hindi-language biographical sports</h1>
                          <h1 class="pt-3 text7 ">Full Expert views</h1>
                          <img src="/newassets/images/play.png" class="text8" alt="...">
                          <h1 class="pt-3 text9 ">Video Reviews</h1>
                        </div>
                      </div>
                      <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4 none dnone ksiwc hover">
                        <a href="#"> <img src="/newassets/images/img3.png" style="width: 100%; " alt="..."></a>
                        <div class="position_effect">
                          <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                              class="fas fa-check"></i></button>
                          <button type="button" class="btn btn-light css-ksiwc2 seentext">Seen it</button>
                        </div>


                        <div class="position_effect2">
                          <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                              class="fas fa-plus"></i></button>
                          <button type="button" class="btn btn-light css-ksiwc2 seentext">+ Want to see</button>
                        </div>
                        <div class="position_effect3">
                          <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                              class="fas fa-star"></i></button>
                          <button type="button" class="btn btn-light css-ksiwc2 seentext"><i class="fas fa-star"></i><i
                              class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i
                              class="fas fa-star"></i></button>
                        </div>

                        <h4 class="pt-3 text-white">2017,Hindi ,Action</h4>
                        <p class="font-weight-bold text-white">Gully Boy</p>
                        <div class="overlay3">
                          <h1 class="pt-3 text font-weight-bold">Gully Boy</h1>
                          <h1 class="pt-3 text2 font-italic ">Khoj says</h1>
                          <img src="/newassets/images/check.png" class="text3" alt="...">
                          <h1 class="pt-3 text4 ">Age 8+</h1><img src="/newassets/images/rating.png" class="text5" alt="...">
                          <h1 class="pt-3 text6 ">Maidaan is an upcoming Indian Hindi-language biographical sports</h1>
                          <h1 class="pt-3 text7 ">Full Expert views</h1>
                          <img src="/newassets/images/play.png" class="text8" alt="...">
                          <h1 class="pt-3 text9 ">Video Reviews</h1>
                        </div>
                      </div>

                    </div>
                  </div>
                  <div class="carousel-item">
                    <div class="row ">
                      <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-4 dnone none ksiwc hover">
                        <a href="#"> <img src="/newassets/images/img1.png" style="width: 100% ; " alt="...">
                        </a>
                        <div class="position_effect">
                          <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                              class="fas fa-check"></i></button>
                          <button type="button" class="btn btn-light css-ksiwc2 seentext">Seen it</button>
                        </div>


                        <div class="position_effect2">
                          <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                              class="fas fa-plus"></i></button>
                          <button type="button" class="btn btn-light css-ksiwc2 seentext">+ Want to see</button>
                        </div>
                        <div class="position_effect3">
                          <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                              class="fas fa-star"></i></button>
                          <button type="button" class="btn btn-light css-ksiwc2 seentext"><i class="fas fa-star"></i><i
                              class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i
                              class="fas fa-star"></i></button>
                        </div>

                        <h4 class="pt-3 text-white">2017,Hindi ,Action</h4>
                        <p class="font-weight-bold text-white">Gully Boy</p>
                        <div class="overlay3">
                          <h1 class="pt-3 text font-weight-bold">Gully Boy</h1>
                          <h1 class="pt-3 text2 font-italic ">Khoj says</h1>
                          <img src="/newassets/images/check.png" class="text3" alt="...">
                          <h1 class="pt-3 text4 ">Age 8+</h1><img src="/newassets/images/rating.png" class="text5" alt="...">
                          <h1 class="pt-3 text6 ">Maidaan is an upcoming Indian Hindi-language biographical sports</h1>
                          <h1 class="pt-3 text7 ">Full Expert views</h1>
                          <img src="/newassets/images/play.png" class="text8" alt="...">
                          <h1 class="pt-3 text9 ">Video Reviews</h1>
                        </div>
                      </div>
                      <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-6 none ksiwc hover">
                        <a href="#"> <img src="/newassets/images/img2.png" style="width: 100%; " alt="..."></a>
                        <div class="position_effect">
                          <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                              class="fas fa-check"></i></button>
                          <button type="button" class="btn btn-light css-ksiwc2 seentext">Seen it</button>
                        </div>


                        <div class="position_effect2">
                          <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                              class="fas fa-plus"></i></button>
                          <button type="button" class="btn btn-light css-ksiwc2 seentext">+ Want to see</button>
                        </div>
                        <div class="position_effect3">
                          <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                              class="fas fa-star"></i></button>
                          <button type="button" class="btn btn-light css-ksiwc2 seentext"><i class="fas fa-star"></i><i
                              class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i
                              class="fas fa-star"></i></button>
                        </div>

                        <h4 class="pt-3 text-white">2017,Hindi ,Action</h4>
                        <p class="font-weight-bold text-white">Gully Boy</p>
                        <div class="overlay3">
                          <h1 class="pt-3 text font-weight-bold">Gully Boy</h1>
                          <h1 class="pt-3 text2 font-italic ">Khoj says</h1>
                          <img src="/newassets/images/check.png" class="text3" alt="...">
                          <h1 class="pt-3 text4 ">Age 8+</h1><img src="/newassets/images/rating.png" class="text5" alt="...">
                          <h1 class="pt-3 text6 ">Maidaan is an upcoming Indian Hindi-language biographical sports</h1>
                          <h1 class="pt-3 text7 ">Full Expert views</h1>
                          <img src="/newassets/images/play.png" class="text8" alt="...">
                          <h1 class="pt-3 text9 ">Video Reviews</h1>
                        </div>
                      </div>
                      <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12   ksiwc hover">
                        <a href="#"> <img src="/newassets/images/img5.png" style="width: 100%; " alt="..."></a>
                        <div class="position_effect">
                          <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                              class="fas fa-check"></i></button>
                          <button type="button" class="btn btn-light css-ksiwc2 seentext">Seen it</button>
                        </div>


                        <div class="position_effect2">
                          <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                              class="fas fa-plus"></i></button>
                          <button type="button" class="btn btn-light css-ksiwc2 seentext">+ Want to see</button>
                        </div>
                        <div class="position_effect3">
                          <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                              class="fas fa-star"></i></button>
                          <button type="button" class="btn btn-light css-ksiwc2 seentext"><i class="fas fa-star"></i><i
                              class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i
                              class="fas fa-star"></i></button>
                        </div>

                        <h4 class="pt-3 text-white">2017,Hindi ,Action</h4>
                        <p class="font-weight-bold text-white">Gully Boy</p>
                        <div class="overlay3">
                          <h1 class="pt-3 text font-weight-bold">Gully Boy</h1>
                          <h1 class="pt-3 text2 font-italic ">Khoj says</h1>
                          <img src="/newassets/images/check.png" class="text3" alt="...">
                          <h1 class="pt-3 text4 ">Age 8+</h1><img src="/newassets/images/rating.png" class="text5" alt="...">
                          <h1 class="pt-3 text6 ">Maidaan is an upcoming Indian Hindi-language biographical sports</h1>
                          <h1 class="pt-3 text7 ">Full Expert views</h1>
                          <img src="/newassets/images/play.png" class="text8" alt="...">
                          <h1 class="pt-3 text9 ">Video Reviews</h1>
                        </div>
                      </div>

                    </div>
                  </div>
                </div>
                <a class="carousel-control-prev " href="#carouselExampleControls3" role="button" data-slide="prev">
                  <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                  <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next " href="#carouselExampleControls3" role="button" data-slide="next">
                  <span class="carousel-control-next-icon" aria-hidden="true"></span>
                  <span class="sr-only">Next</span>
                </a>
              </div>
            </div>
          </div>
          <div
            class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12  order-xl-2 order-lg-2 order-md-1 order-sm-1 order-1">


            <h1 class="font-weight-bold text-left colm acti">Romantic for Valentine Days</h1>
            <h3 class="pt-3 pb-5 text-left acti">Most Watched Movies By Days </h3>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="delta">
    <img src="/newassets/images/deltabravo.png" style="width: 100% ; " class="img-fluid" alt="Responsive image">
    <div class="container bravo">
      <div class="row">
        <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 mx-auto">
          <h1 class="font-weight-bold head">Delta Bravo</h1>

          <p class="para text-white">strange black entity from another world bonds with Peter Parker and causes inner
            turmoil as
            he contends with new villains, temptations, and revenge.</p>
          <div class="row buttn">
            <button type="button" class="btn btn-primary btn-lg text-center shadow-lg ml-4 ">Watchlist</button>&nbsp;
            &nbsp;
            <button type="button" class="btn btn-primary btn-lg text-center watch ">+ Playlist</button>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="action">
    <div class="container tele pt-5 pb-5">
      <div class="row">
        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-8 col-12">
          <div class="today"> <a class="nav-link active  Text-white" href="#">Today /</a>
            <a class="nav-link active  Text-white" href="#">This Week / </a>
            <a class="nav-link active  Text-white" href="#">Last 30 Days /</a>
          </div>
        </div>

        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-4 col-12">

          <p class="view"> <a class="nav-link active Text-white" href="#">View More</a></p>


        </div>
      </div>


      <div class="container">
        <div class="row">
          <div class="col-xl-8 col-lg-8 col-md-12 col-sm-12 col-12 order-xl-1 order-lg-1 order-md-2 order-sm-2 order-2">
            <div class="row popu ">

              <div id="carouselExampleControls4" class="carousel slide" data-ride="carousel" data-interval="false">
                <div class="carousel-inner">
                  <div class="carousel-item active">
                    <div class="row ">
                      <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12 ksiwc hover">
                        <a href="#"> <img src="/newassets/images/img1.png" style="width: 100% ; " alt="...">
                        </a>
                        <div class="position_effect">
                          <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                              class="fas fa-check"></i></button>
                          <button type="button" class="btn btn-light css-ksiwc2 seentext">Seen it</button>
                        </div>


                        <div class="position_effect2">
                          <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                              class="fas fa-plus"></i></button>
                          <button type="button" class="btn btn-light css-ksiwc2 seentext">+ Want to see</button>
                        </div>
                        <div class="position_effect3">
                          <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                              class="fas fa-star"></i></button>
                          <button type="button" class="btn btn-light css-ksiwc2 seentext"><i class="fas fa-star"></i><i
                              class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i
                              class="fas fa-star"></i></button>
                        </div>

                        <h4 class="pt-3 text-white">2017,Hindi ,Action</h4>
                        <p class="font-weight-bold text-white">Gully Boy</p>
                        <div class="overlay3">
                          <h1 class="pt-3 text font-weight-bold">Gully Boy</h1>
                          <h1 class="pt-3 text2 font-italic ">Khoj says</h1>
                          <img src="/newassets/images/check.png" class="text3" alt="...">
                          <h1 class="pt-3 text4 ">Age 8+</h1><img src="/newassets/images/rating.png" class="text5" alt="...">
                          <h1 class="pt-3 text6 ">Maidaan is an upcoming Indian Hindi-language biographical sports</h1>
                          <h1 class="pt-3 text7 ">Full Expert views</h1>
                          <img src="/newassets/images/play.png" class="text8" alt="...">
                          <h1 class="pt-3 text9 ">Video Reviews</h1>
                        </div>
                      </div>
                      <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-6 ksiwc none hover">
                        <a href="#"> <img src="/newassets/images/img2.png" style="width: 100%; " alt="..."></a>
                        <div class="position_effect">
                          <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                              class="fas fa-check"></i></button>
                          <button type="button" class="btn btn-light css-ksiwc2 seentext">Seen it</button>
                        </div>


                        <div class="position_effect2">
                          <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                              class="fas fa-plus"></i></button>
                          <button type="button" class="btn btn-light css-ksiwc2 seentext">+ Want to see</button>
                        </div>
                        <div class="position_effect3">
                          <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                              class="fas fa-star"></i></button>
                          <button type="button" class="btn btn-light css-ksiwc2 seentext"><i class="fas fa-star"></i><i
                              class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i
                              class="fas fa-star"></i></button>
                        </div>

                        <h4 class="pt-3 text-white">2017,Hindi ,Action</h4>
                        <p class="font-weight-bold text-white">Gully Boy</p>
                        <div class="overlay3">
                          <h1 class="pt-3 text font-weight-bold">Gully Boy</h1>
                          <h1 class="pt-3 text2 font-italic ">Khoj says</h1>
                          <img src="/newassets/images/check.png" class="text3" alt="...">
                          <h1 class="pt-3 text4 ">Age 8+</h1><img src="/newassets/images/rating.png" class="text5" alt="...">
                          <h1 class="pt-3 text6 ">Maidaan is an upcoming Indian Hindi-language biographical sports</h1>
                          <h1 class="pt-3 text7 ">Full Expert views</h1>
                          <img src="/newassets/images/play.png" class="text8" alt="...">
                          <h1 class="pt-3 text9 ">Video Reviews</h1>
                        </div>
                      </div>
                      <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4 none dnone ksiwc hover">
                        <a href="#"> <img src="/newassets/images/img3.png" style="width: 100%; " alt="..."></a>
                        <div class="position_effect">
                          <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                              class="fas fa-check"></i></button>
                          <button type="button" class="btn btn-light css-ksiwc2 seentext">Seen it</button>
                        </div>


                        <div class="position_effect2">
                          <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                              class="fas fa-plus"></i></button>
                          <button type="button" class="btn btn-light css-ksiwc2 seentext">+ Want to see</button>
                        </div>
                        <div class="position_effect3">
                          <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                              class="fas fa-star"></i></button>
                          <button type="button" class="btn btn-light css-ksiwc2 seentext"><i class="fas fa-star"></i><i
                              class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i
                              class="fas fa-star"></i></button>
                        </div>

                        <h4 class="pt-3 text-white">2017,Hindi ,Action</h4>
                        <p class="font-weight-bold text-white">Gully Boy</p>
                        <div class="overlay3">
                          <h1 class="pt-3 text font-weight-bold">Gully Boy</h1>
                          <h1 class="pt-3 text2 font-italic ">Khoj says</h1>
                          <img src="/newassets/images/check.png" class="text3" alt="...">
                          <h1 class="pt-3 text4 ">Age 8+</h1><img src="/newassets/images/rating.png" class="text5" alt="...">
                          <h1 class="pt-3 text6 ">Maidaan is an upcoming Indian Hindi-language biographical sports</h1>
                          <h1 class="pt-3 text7 ">Full Expert views</h1>
                          <img src="/newassets/images/play.png" class="text8" alt="...">
                          <h1 class="pt-3 text9 ">Video Reviews</h1>
                        </div>
                      </div>

                    </div>
                  </div>
                  <div class="carousel-item">
                    <div class="row ">
                      <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12 none dnone ksiwc hover">
                        <a href="#"> <img src="/newassets/images/img1.png" style="width: 100% ; " alt="...">
                        </a>
                        <div class="position_effect">
                          <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                              class="fas fa-check"></i></button>
                          <button type="button" class="btn btn-light css-ksiwc2 seentext">Seen it</button>
                        </div>


                        <div class="position_effect2">
                          <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                              class="fas fa-plus"></i></button>
                          <button type="button" class="btn btn-light css-ksiwc2 seentext">+ Want to see</button>
                        </div>
                        <div class="position_effect3">
                          <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                              class="fas fa-star"></i></button>
                          <button type="button" class="btn btn-light css-ksiwc2 seentext"><i class="fas fa-star"></i><i
                              class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i
                              class="fas fa-star"></i></button>
                        </div>

                        <h4 class="pt-3 text-white">2017,Hindi ,Action</h4>
                        <p class="font-weight-bold text-white">Gully Boy</p>
                        <div class="overlay3">
                          <h1 class="pt-3 text font-weight-bold">Gully Boy</h1>
                          <h1 class="pt-3 text2 font-italic ">Khoj says</h1>
                          <img src="/newassets/images/check.png" class="text3" alt="...">
                          <h1 class="pt-3 text4 ">Age 8+</h1><img src="/newassets/images/rating.png" class="text5" alt="...">
                          <h1 class="pt-3 text6 ">Maidaan is an upcoming Indian Hindi-language biographical sports</h1>
                          <h1 class="pt-3 text7 ">Full Expert views</h1>
                          <img src="/newassets/images/play.png" class="text8" alt="...">
                          <h1 class="pt-3 text9 ">Video Reviews</h1>
                        </div>
                      </div>
                      <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-6 none ksiwc hover">
                        <a href="#"> <img src="/newassets/images/img2.png" style="width: 100%; " alt="..."></a>
                        <div class="position_effect">
                          <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                              class="fas fa-check"></i></button>
                          <button type="button" class="btn btn-light css-ksiwc2 seentext">Seen it</button>
                        </div>


                        <div class="position_effect2">
                          <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                              class="fas fa-plus"></i></button>
                          <button type="button" class="btn btn-light css-ksiwc2 seentext">+ Want to see</button>
                        </div>
                        <div class="position_effect3">
                          <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                              class="fas fa-star"></i></button>
                          <button type="button" class="btn btn-light css-ksiwc2 seentext"><i class="fas fa-star"></i><i
                              class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i
                              class="fas fa-star"></i></button>
                        </div>

                        <h4 class="pt-3 text-white">2017,Hindi ,Action</h4>
                        <p class="font-weight-bold text-white">Gully Boy</p>
                        <div class="overlay3">
                          <h1 class="pt-3 text font-weight-bold">Gully Boy</h1>
                          <h1 class="pt-3 text2 font-italic ">Khoj says</h1>
                          <img src="/newassets/images/check.png" class="text3" alt="...">
                          <h1 class="pt-3 text4 ">Age 8+</h1><img src="/newassets/images/rating.png" class="text5" alt="...">
                          <h1 class="pt-3 text6 ">Maidaan is an upcoming Indian Hindi-language biographical sports</h1>
                          <h1 class="pt-3 text7 ">Full Expert views</h1>
                          <img src="/newassets/images/play.png" class="text8" alt="...">
                          <h1 class="pt-3 text9 ">Video Reviews</h1>
                        </div>
                      </div>
                      <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12  ksiwc hover">
                        <a href="#"> <img src="/newassets/images/img5.png" style="width: 100%; " alt="..."></a>
                        <div class="position_effect">
                          <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                              class="fas fa-check"></i></button>
                          <button type="button" class="btn btn-light css-ksiwc2 seentext">Seen it</button>
                        </div>


                        <div class="position_effect2">
                          <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                              class="fas fa-plus"></i></button>
                          <button type="button" class="btn btn-light css-ksiwc2 seentext">+ Want to see</button>
                        </div>
                        <div class="position_effect3">
                          <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                              class="fas fa-star"></i></button>
                          <button type="button" class="btn btn-light css-ksiwc2 seentext"><i class="fas fa-star"></i><i
                              class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i
                              class="fas fa-star"></i></button>
                        </div>

                        <h4 class="pt-3 text-white">2017,Hindi ,Action</h4>
                        <p class="font-weight-bold text-white">Gully Boy</p>
                        <div class="overlay3">
                          <h1 class="pt-3 text font-weight-bold">Gully Boy</h1>
                          <h1 class="pt-3 text2 font-italic ">Khoj says</h1>
                          <img src="/newassets/images/check.png" class="text3" alt="...">
                          <h1 class="pt-3 text4 ">Age 8+</h1><img src="/newassets/images/rating.png" class="text5" alt="...">
                          <h1 class="pt-3 text6 ">Maidaan is an upcoming Indian Hindi-language biographical sports</h1>
                          <h1 class="pt-3 text7 ">Full Expert views</h1>
                          <img src="/newassets/images/play.png" class="text8" alt="...">
                          <h1 class="pt-3 text9 ">Video Reviews</h1>
                        </div>
                      </div>

                    </div>
                  </div>
                </div>
                <a class="carousel-control-prev " href="#carouselExampleControls4" role="button" data-slide="prev">
                  <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                  <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next " href="#carouselExampleControls4" role="button" data-slide="next">
                  <span class="carousel-control-next-icon" aria-hidden="true"></span>
                  <span class="sr-only">Next</span>
                </a>
              </div>
            </div>
          </div>
          <div
            class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12 order-xl-2 order-lg-2 order-md-1 order-sm-1 order-1 colm ">
            <h1 class="font-weight-bold text-right acti pt-3">Action Drama Movies</h1>
            <h3 class="pt-3 pb-5 text-right acti">Most Watched Movies By Days </h3>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="featured">
    <img src="/newassets/images/deltabravo.png" style="width: 100% ; " class="img-fluid" alt="Responsive image">
    <div class="container web">
      <div class="row">
        <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 mx-auto">
          <h1 class="font-weight-bold head">Featured Web Series</h1>

          <p class="para text-white">strange black entity from another world bonds with Peter Parker and causes inner
            turmoil as he contends with new villains, temptations, and revenge.</p>
          <div class="row buttn">
            <button type="button" class="btn btn-primary btn-lg text-center shadow-lg ml-4 ">Watchlist</button>&nbsp;
            &nbsp;
            <button type="button" class="btn btn-primary btn-lg text-center watch ml-4">+ Playlist</button>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="addd">
    <div class="container ">
      <div class="row add2">
        <img src="/newassets/images/add2.jpg" style="width: 100% ; " alt="...">
      </div>
    </div>

  </section>

  <section class="genre mb-5">
    <div class="container">
      <div class="row">
        <div class="col-xl-6 col-lg-6 col-md-8 col-sm-8 col-8">
          <h2 class="font-weight-bold">Whats your Genre, Boss</h2>
        </div>
        <div class="col-xl-6 col-lg-6 col-md-4 col-sm-4 col-4">
          <p class="view text-right"> <a class="nav-link active Text-white" href="#">View More</a></p>
        </div>
      </div>
      <div class="row mt-5 mb-5">
        <div id="carouselExampleControls5" class="carousel slide" data-ride="carousel" data-interval="false">
          <div class="carousel-inner">
            <div class="carousel-item active">
              <div class="row ">
                <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1 col-1">

                </div>
                <div class="col-xl-1 col-lg-1 col-md-2 col-sm-2 col-2">
                  <a href="#"> <img src="/newassets/images/genre1.png" style="width: 100% ;" alt="..."></a>
                  <p class=" pt-2 lang font-weight-bold text-white text-center">Love</p>
                </div>
                <div class="col-xl-1 col-lg-1 col-md-2 col-sm-2 col-2">
                  <a href="#"> <img src="/newassets/images/genre2.png" style="width: 100%;" alt="..."></a>
                  <p class=" pt-2 lang font-weight-bold text-white text-center">Romance</p>
                </div>
                <div class="col-xl-1 col-lg-1 col-md-2 col-sm-2 col-2">
                  <a href="#"> <img src="/newassets/images/genre3.png" style="width: 100%; " alt="..."></a>
                  <p class=" pt-2 lang font-weight-bold text-white text-center">Horror</p>
                </div>
                <div class="col-xl-1 col-lg-1 col-md-2 col-sm-2 col-2">
                  <a href="#"> <img src="/newassets/images/genre4.png" style="width: 100% ; " alt="..."></a>
                  <p class=" pt-2 lang font-weight-bold text-white text-center">Comedy</p>
                </div>
                <div class="col-xl-1 col-lg-1 col-md-2 col-sm-2 col-2">
                  <a href="#"> <img src="/newassets/images/genre5.png" style="width: 100%; " alt="..."></a>
                  <p class=" pt-2 lang font-weight-bold text-white text-center">Action</p>
                </div>
                <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1 col-1 none dnone non">
                  <a href="#"> <img src="/newassets/images/genre1.png" style="width: 100% ;" alt="..."></a>
                  <p class=" pt-2 lang font-weight-bold text-white text-center">Adventure</p>
                </div>
                <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1 col-1  none dnone non">
                  <a href="#"> <img src="/newassets/images/genre2.png" style="width: 100%;" alt="..."></a>
                  <p class=" pt-2 lang font-weight-bold text-white text-center">Western</p>
                </div>
                <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1 col-1 none dnone non">
                  <a href="#"> <img src="/newassets/images/genre3.png" style="width: 100%; " alt="..."></a>
                  <p class=" pt-2 lang font-weight-bold text-white text-center">Musical</p>
                </div>
                <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1 col-1 none dnone non">
                  <a href="#"> <img src="/newassets/images/genre4.png" style="width: 100% ; " alt="..."></a>
                  <p class=" pt-2 lang font-weight-bold text-white text-center">History</p>
                </div>
                <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1 col-1 none dnone non">
                  <a href="#"> <img src="/newassets/images/genre5.png" style="width: 100%; " alt="..."></a>
                  <p class=" pt-2 lang font-weight-bold text-white text-center">War</p>
                </div>
                <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1 col-1">

                </div>
              </div>
            </div>
            <div class="carousel-item">
              <div class="row  ">
                <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1 col-1">

                </div>
                <div class="col-xl-1 col-lg-1 col-md-2 col-sm-2 col-2">
                  <a href="#"> <img src="/newassets/images/genre1.png" style="width: 100% ;" alt="..."></a>
                  <p class=" pt-2 lang font-weight-bold text-white text-center">Love</p>
                </div>
                <div class="col-xl-1 col-lg-1 col-md-2 col-sm-2 col-2">
                  <a href="#"> <img src="/newassets/images/genre2.png" style="width: 100%;" alt="..."></a>
                  <p class=" pt-2 lang font-weight-bold text-white text-center">Romance</p>
                </div>
                <div class="col-xl-1 col-lg-1 col-md-2 col-sm-2 col-2">
                  <a href="#"> <img src="/newassets/images/genre3.png" style="width: 100%; " alt="..."></a>
                  <p class=" pt-2 lang font-weight-bold text-white text-center">Horror</p>
                </div>
                <div class="col-xl-1 col-lg-1 col-md-2 col-sm-2 col-2">
                  <a href="#"> <img src="/newassets/images/genre4.png" style="width: 100% ; " alt="..."></a>
                  <p class=" pt-2 lang font-weight-bold text-white text-center">War</p>
                </div>
                <div class="col-xl-1 col-lg-1 col-md-2 col-sm-2 col-2">
                  <a href="#"> <img src="/newassets/images/genre5.png" style="width: 100%; " alt="..."></a>
                  <p class=" pt-2 lang font-weight-bold text-white text-center">History</p>
                </div>
                <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1 col-1 none dnone non">
                  <a href="#"> <img src="/newassets/images/genre1.png" style="width: 100% ;" alt="..."></a>
                  <p class=" pt-2 lang font-weight-bold text-white text-center">Musical</p>
                </div>
                <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1 col-1 none dnone non">
                  <a href="#"> <img src="/newassets/images/genre2.png" style="width: 100%;" alt="..."></a>
                  <p class=" pt-2 lang font-weight-bold text-white text-center">Western</p>
                </div>
                <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1 col-1 none dnone non ">
                  <a href="#"> <img src="/newassets/images/genre3.png" style="width: 100%; " alt="..."></a>
                  <p class=" pt-2 lang font-weight-bold text-white text-center">Adventure</p>
                </div>
                <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1 col-1 none dnone non">
                  <a href="#"> <img src="/newassets/images/genre4.png" style="width: 100% ; " alt="..."></a>
                  <p class=" pt-2 lang font-weight-bold text-white text-center">Action</p>
                </div>
                <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1 col-1 none dnone non">
                  <a href="#"> <img src="/newassets/images/genre5.png" style="width: 100%; " alt="..."></a>
                  <p class=" pt-2 lang font-weight-bold text-white text-center">Comedy</p>
                </div>

                <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1 col-1">

                </div>
              </div>
            </div>

          </div>
          <a class="carousel-control-prev" href="#carouselExampleControls5" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="carousel-control-next" href="#carouselExampleControls5" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
        </div>
      </div>
    </div>
  </section>

  <section class="funniest">
    <div class="container tele pt-5 pb-5">

      <div class="row">

        <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12 ">
          <div class="today"> <a class="nav-link active  Text-white" href="#">Today /</a>
            <a class="nav-link active  Text-white" href="#">This Week / </a>
            <a class="nav-link active  Text-white" href="#">Last 30 Days /</a>
          </div>
          <h2 class="font-weight-bold colm">Funniest Comedy Movie of 2018</h2>
          <h4 class="pt-3 text-white bord">View All </h4>



        </div>
        <div class="col-xl-8 col-lg-8 col-md-12 col-sm-12 col-12">
          <div class="row">
            <div class="col-md-12">
              <p class="view"> <a class="nav-link active Text-white" href="#">View More</a></p>
              <div id="carouselExampleControls8" data-touch="true" class="carousel slide" data-ride="carousel"
                data-interval="false">
                <div class="carousel-inner">

                  <div class="carousel-item active">
                    <div class="row ">
                      <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12 ksiwc hover">
                        <a href="#"> <img src="/newassets/images/img1.png" style="width: 100% ; " alt="...">
                        </a>
                        <div class="position_effect">
                          <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                              class="fas fa-check"></i></button>
                          <button type="button" class="btn btn-light css-ksiwc2 seentext">Seen it</button>
                        </div>


                        <div class="position_effect2">
                          <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                              class="fas fa-plus"></i></button>
                          <button type="button" class="btn btn-light css-ksiwc2 seentext">+ Want to see</button>
                        </div>
                        <div class="position_effect3">
                          <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                              class="fas fa-star"></i></button>
                          <button type="button" class="btn btn-light css-ksiwc2 seentext"><i class="fas fa-star"></i><i
                              class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i
                              class="fas fa-star"></i></button>
                        </div>

                        <h4 class="pt-3 text-white">2017,Hindi ,Action</h4>
                        <p class="font-weight-bold text-white">Gully Boy</p>
                        <div class="overlay3">
                          <h1 class="pt-3 text font-weight-bold">Gully Boy</h1>
                          <h1 class="pt-3 text2 font-italic ">Khoj says</h1>
                          <img src="/newassets/images/check.png" class="text3" alt="...">
                          <h1 class="pt-3 text4 ">Age 8+</h1><img src="/newassets/images/rating.png" class="text5" alt="...">
                          <h1 class="pt-3 text6 ">Maidaan is an upcoming Indian Hindi-language biographical sports</h1>
                          <h1 class="pt-3 text7 ">Full Expert views</h1>
                          <img src="/newassets/images/play.png" class="text8" alt="...">
                          <h1 class="pt-3 text9 ">Video Reviews</h1>
                        </div>
                      </div>
                      <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-6 ksiwc none hover">
                        <a href="#"> <img src="/newassets/images/img2.png" style="width: 100%; " alt="..."></a>
                        <div class="position_effect">
                          <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                              class="fas fa-check"></i></button>
                          <button type="button" class="btn btn-light css-ksiwc2 seentext">Seen it</button>
                        </div>


                        <div class="position_effect2">
                          <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                              class="fas fa-plus"></i></button>
                          <button type="button" class="btn btn-light css-ksiwc2 seentext">+ Want to see</button>
                        </div>
                        <div class="position_effect3">
                          <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                              class="fas fa-star"></i></button>
                          <button type="button" class="btn btn-light css-ksiwc2 seentext"><i class="fas fa-star"></i><i
                              class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i
                              class="fas fa-star"></i></button>
                        </div>

                        <h4 class="pt-3 text-white">2017,Hindi ,Action</h4>
                        <p class="font-weight-bold text-white">Gully Boy</p>
                        <div class="overlay3">
                          <h1 class="pt-3 text font-weight-bold">Gully Boy</h1>
                          <h1 class="pt-3 text2 font-italic ">Khoj says</h1>
                          <img src="/newassets/images/check.png" class="text3" alt="...">
                          <h1 class="pt-3 text4 ">Age 8+</h1><img src="/newassets/images/rating.png" class="text5" alt="...">
                          <h1 class="pt-3 text6 ">Maidaan is an upcoming Indian Hindi-language biographical sports</h1>
                          <h1 class="pt-3 text7 ">Full Expert views</h1>
                          <img src="/newassets/images/play.png" class="text8" alt="...">
                          <h1 class="pt-3 text9 ">Video Reviews</h1>
                        </div>
                      </div>
                      <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4 ksiwc none dnone hover">
                        <a href="#"> <img src="/newassets/images/img3.png" style="width: 100%; " alt="..."></a>
                        <div class="position_effect">
                          <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                              class="fas fa-check"></i></button>
                          <button type="button" class="btn btn-light css-ksiwc2 seentext">Seen it</button>
                        </div>


                        <div class="position_effect2">
                          <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                              class="fas fa-plus"></i></button>
                          <button type="button" class="btn btn-light css-ksiwc2 seentext">+ Want to see</button>
                        </div>
                        <div class="position_effect3">
                          <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                              class="fas fa-star"></i></button>
                          <button type="button" class="btn btn-light css-ksiwc2 seentext"><i class="fas fa-star"></i><i
                              class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i
                              class="fas fa-star"></i></button>
                        </div>

                        <h4 class="pt-3 text-white">2017,Hindi ,Action</h4>
                        <p class="font-weight-bold text-white">Gully Boy</p>
                        <div class="overlay3">
                          <h1 class="pt-3 text font-weight-bold">Gully Boy</h1>
                          <h1 class="pt-3 text2 font-italic ">Khoj says</h1>
                          <img src="/newassets/images/check.png" class="text3" alt="...">
                          <h1 class="pt-3 text4 ">Age 8+</h1><img src="/newassets/images/rating.png" class="text5" alt="...">
                          <h1 class="pt-3 text6 ">Maidaan is an upcoming Indian Hindi-language biographical sports</h1>
                          <h1 class="pt-3 text7 ">Full Expert views</h1>
                          <img src="/newassets/images/play.png" class="text8" alt="...">
                          <h1 class="pt-3 text9 ">Video Reviews</h1>
                        </div>
                      </div>

                    </div>
                  </div>
                  <div class="carousel-item">
                    <div class="row ">
                      <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12 ksiwc hover">
                        <a href="#"> <img src="/newassets/images/img1.png" style="width: 100% ; " alt="...">
                        </a>
                        <div class="position_effect">
                          <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                              class="fas fa-check"></i></button>
                          <button type="button" class="btn btn-light css-ksiwc2 seentext">Seen it</button>
                        </div>


                        <div class="position_effect2">
                          <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                              class="fas fa-plus"></i></button>
                          <button type="button" class="btn btn-light css-ksiwc2 seentext">+ Want to see</button>
                        </div>
                        <div class="position_effect3">
                          <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                              class="fas fa-star"></i></button>
                          <button type="button" class="btn btn-light css-ksiwc2 seentext"><i class="fas fa-star"></i><i
                              class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i
                              class="fas fa-star"></i></button>
                        </div>

                        <h4 class="pt-3 text-white">2017,Hindi ,Action</h4>
                        <p class="font-weight-bold text-white">Gully Boy</p>
                        <div class="overlay3">
                          <h1 class="pt-3 text font-weight-bold">Gully Boy</h1>
                          <h1 class="pt-3 text2 font-italic ">Khoj says</h1>
                          <img src="/newassets/images/check.png" class="text3" alt="...">
                          <h1 class="pt-3 text4 ">Age 8+</h1><img src="/newassets/images/rating.png" class="text5" alt="...">
                          <h1 class="pt-3 text6 ">Maidaan is an upcoming Indian Hindi-language biographical sports</h1>
                          <h1 class="pt-3 text7 ">Full Expert views</h1>
                          <img src="/newassets/images/play.png" class="text8" alt="...">
                          <h1 class="pt-3 text9 ">Video Reviews</h1>
                        </div>
                      </div>
                      <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-6 none ksiwc hover">
                        <a href="#"> <img src="/newassets/images/img2.png" style="width: 100%; " alt="..."></a>
                        <div class="position_effect">
                          <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                              class="fas fa-check"></i></button>
                          <button type="button" class="btn btn-light css-ksiwc2 seentext">Seen it</button>
                        </div>


                        <div class="position_effect2">
                          <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                              class="fas fa-plus"></i></button>
                          <button type="button" class="btn btn-light css-ksiwc2 seentext">+ Want to see</button>
                        </div>
                        <div class="position_effect3">
                          <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                              class="fas fa-star"></i></button>
                          <button type="button" class="btn btn-light css-ksiwc2 seentext"><i class="fas fa-star"></i><i
                              class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i
                              class="fas fa-star"></i></button>
                        </div>

                        <h4 class="pt-3 text-white">2017,Hindi ,Action</h4>
                        <p class="font-weight-bold text-white">Gully Boy</p>
                        <div class="overlay3">
                          <h1 class="pt-3 text font-weight-bold">Gully Boy</h1>
                          <h1 class="pt-3 text2 font-italic ">Khoj says</h1>
                          <img src="/newassets/images/check.png" class="text3" alt="...">
                          <h1 class="pt-3 text4 ">Age 8+</h1><img src="/newassets/images/rating.png" class="text5" alt="...">
                          <h1 class="pt-3 text6 ">Maidaan is an upcoming Indian Hindi-language biographical sports</h1>
                          <h1 class="pt-3 text7 ">Full Expert views</h1>
                          <img src="/newassets/images/play.png" class="text8" alt="...">
                          <h1 class="pt-3 text9 ">Video Reviews</h1>
                        </div>
                      </div>
                      <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4 ksiwc none dnone hover">
                        <a href="#"> <img src="/newassets/images/img3.png" style="width: 100%; " alt="..."></a>
                        <div class="position_effect">
                          <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                              class="fas fa-check"></i></button>
                          <button type="button" class="btn btn-light css-ksiwc2 seentext">Seen it</button>
                        </div>


                        <div class="position_effect2">
                          <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                              class="fas fa-plus"></i></button>
                          <button type="button" class="btn btn-light css-ksiwc2 seentext">+ Want to see</button>
                        </div>
                        <div class="position_effect3">
                          <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                              class="fas fa-star"></i></button>
                          <button type="button" class="btn btn-light css-ksiwc2 seentext"><i class="fas fa-star"></i><i
                              class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i
                              class="fas fa-star"></i></button>
                        </div>

                        <h4 class="pt-3 text-white">2017,Hindi ,Action</h4>
                        <p class="font-weight-bold text-white">Gully Boy</p>
                        <div class="overlay3">
                          <h1 class="pt-3 text font-weight-bold">Gully Boy</h1>
                          <h1 class="pt-3 text2 font-italic ">Khoj says</h1>
                          <img src="/newassets/images/check.png" class="text3" alt="...">
                          <h1 class="pt-3 text4 ">Age 8+</h1><img src="/newassets/images/rating.png" class="text5" alt="...">
                          <h1 class="pt-3 text6 ">Maidaan is an upcoming Indian Hindi-language biographical sports</h1>
                          <h1 class="pt-3 text7 ">Full Expert views</h1>
                          <img src="/newassets/images/play.png" class="text8" alt="...">
                          <h1 class="pt-3 text9 ">Video Reviews</h1>
                        </div>
                      </div>

                    </div>
                  </div>
                </div>

                <a class="carousel-control-prev " href="#carouselExampleControls8" role="button" data-slide="prev">
                  <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                  <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next " href="#carouselExampleControls8" role="button" data-slide="next">
                  <span class="carousel-control-next-icon" aria-hidden="true"></span>
                  <span class="sr-only">Next</span>
                </a>

              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

  </section>

  <section class="coming_soon ">
    <div class="container tele pt-5 pb-5">

      <div class="row">
        <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12  ">
          <div class="today"> <a class="nav-link active  Text-white" href="#">Today /</a>
            <a class="nav-link active  Text-white" href="#">This Week / </a>
            <a class="nav-link active  Text-white" href="#">Last 30 Days /</a>
          </div>
          <h1 class="font-weight-bold colm cola">Coming<br> soon</h1>
          <h3 class="pt-3 cola">Most Watched Movies By Days </h3>
        </div>
        <div class="col-xl-8 col-lg-8 col-md-12 col-sm-12 col-12">
          <div class="row justify-content-end popu">
            <p class="view moor"> <a class="nav-link active Text-white" href="#">View More</a></p>
            <div id="carouselExampleControls6" class="carousel slide" data-ride="carousel" data-interval="false">
              <div class="carousel-inner">
                <div class="carousel-item active">
                  <div class="row ">
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12 ksiwc hover">
                      <a href="#"> <img src="/newassets/images/img1.png" style="width: 100% ; " alt="...">
                      </a>
                      <div class="position_effect">
                        <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                            class="fas fa-check"></i></button>
                        <button type="button" class="btn btn-light css-ksiwc2 seentext">Seen it</button>
                      </div>


                      <div class="position_effect2">
                        <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                            class="fas fa-plus"></i></button>
                        <button type="button" class="btn btn-light css-ksiwc2 seentext">+ Want to see</button>
                      </div>
                      <div class="position_effect3">
                        <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                            class="fas fa-star"></i></button>
                        <button type="button" class="btn btn-light css-ksiwc2 seentext"><i class="fas fa-star"></i><i
                            class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i
                            class="fas fa-star"></i></button>
                      </div>

                      <h4 class="pt-3 text-white">2017,Hindi ,Action</h4>
                      <p class="font-weight-bold text-white">Gully Boy</p>
                      <div class="overlay3">
                        <h1 class="pt-3 text font-weight-bold">Gully Boy</h1>
                        <h1 class="pt-3 text2 font-italic ">Khoj says</h1>
                        <img src="/newassets/images/check.png" class="text3" alt="...">
                        <h1 class="pt-3 text4 ">Age 8+</h1><img src="/newassets/images/rating.png" class="text5" alt="...">
                        <h1 class="pt-3 text6 ">Maidaan is an upcoming Indian Hindi-language biographical sports</h1>
                        <h1 class="pt-3 text7 ">Full Expert views</h1>
                        <img src="/newassets/images/play.png" class="text8" alt="...">
                        <h1 class="pt-3 text9 ">Video Reviews</h1>
                      </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-6 none ksiwc hover">
                      <a href="#"> <img src="/newassets/images/img2.png" style="width: 100%; " alt="..."></a>
                      <div class="position_effect">
                        <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                            class="fas fa-check"></i></button>
                        <button type="button" class="btn btn-light css-ksiwc2 seentext">Seen it</button>
                      </div>


                      <div class="position_effect2">
                        <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                            class="fas fa-plus"></i></button>
                        <button type="button" class="btn btn-light css-ksiwc2 seentext">+ Want to see</button>
                      </div>
                      <div class="position_effect3">
                        <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                            class="fas fa-star"></i></button>
                        <button type="button" class="btn btn-light css-ksiwc2 seentext"><i class="fas fa-star"></i><i
                            class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i
                            class="fas fa-star"></i></button>
                      </div>

                      <h4 class="pt-3 text-white">2017,Hindi ,Action</h4>
                      <p class="font-weight-bold text-white">Gully Boy</p>
                      <div class="overlay3">
                        <h1 class="pt-3 text font-weight-bold">Gully Boy</h1>
                        <h1 class="pt-3 text2 font-italic ">Khoj says</h1>
                        <img src="/newassets/images/check.png" class="text3" alt="...">
                        <h1 class="pt-3 text4 ">Age 8+</h1><img src="/newassets/images/rating.png" class="text5" alt="...">
                        <h1 class="pt-3 text6 ">Maidaan is an upcoming Indian Hindi-language biographical sports</h1>
                        <h1 class="pt-3 text7 ">Full Expert views</h1>
                        <img src="/newassets/images/play.png" class="text8" alt="...">
                        <h1 class="pt-3 text9 ">Video Reviews</h1>
                      </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4 none dnone ksiwc hover">
                      <a href="#"> <img src="/newassets/images/img3.png" style="width: 100%; " alt="..."></a>
                      <div class="position_effect">
                        <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                            class="fas fa-check"></i></button>
                        <button type="button" class="btn btn-light css-ksiwc2 seentext">Seen it</button>
                      </div>


                      <div class="position_effect2">
                        <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                            class="fas fa-plus"></i></button>
                        <button type="button" class="btn btn-light css-ksiwc2 seentext">+ Want to see</button>
                      </div>
                      <div class="position_effect3">
                        <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                            class="fas fa-star"></i></button>
                        <button type="button" class="btn btn-light css-ksiwc2 seentext"><i class="fas fa-star"></i><i
                            class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i
                            class="fas fa-star"></i></button>
                      </div>

                      <h4 class="pt-3 text-white">2017,Hindi ,Action</h4>
                      <p class="font-weight-bold text-white">Gully Boy</p>
                      <div class="overlay3">
                        <h1 class="pt-3 text font-weight-bold">Gully Boy</h1>
                        <h1 class="pt-3 text2 font-italic ">Khoj says</h1>
                        <img src="/newassets/images/check.png" class="text3" alt="...">
                        <h1 class="pt-3 text4 ">Age 8+</h1><img src="/newassets/images/rating.png" class="text5" alt="...">
                        <h1 class="pt-3 text6 ">Maidaan is an upcoming Indian Hindi-language biographical sports</h1>
                        <h1 class="pt-3 text7 ">Full Expert views</h1>
                        <img src="/newassets/images/play.png" class="text8" alt="...">
                        <h1 class="pt-3 text9 ">Video Reviews</h1>
                      </div>
                    </div>

                  </div>
                </div>
                <div class="carousel-item">
                  <div class="row ">
                    
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12 ksiwc hover">
                      <a href="#"> <img src="/newassets/images/img4.png" class="img-fluid" style="width: 100% ; " alt="...">
                      </a>
                      <div class="position_effect">
                        <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                            class="fas fa-check"></i></button>
                        <button type="button" class="btn btn-light css-ksiwc2 seentext">Seen it</button>
                      </div>


                      <div class="position_effect2">
                        <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                            class="fas fa-plus"></i></button>
                        <button type="button" class="btn btn-light css-ksiwc2 seentext">+ Want to see</button>
                      </div>
                      <div class="position_effect3">
                        <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                            class="fas fa-star"></i></button>
                        <button type="button" class="btn btn-light css-ksiwc2 seentext"><i class="fas fa-star"></i><i
                            class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i
                            class="fas fa-star"></i></button>
                      </div>

                      <h4 class="pt-3 text-white">2017,Hindi ,Action</h4>
                      <p class="font-weight-bold text-white">Gully Boy</p>
                      <div class="overlay3">
                        <h1 class="pt-3 text font-weight-bold">Gully Boy</h1>
                        <h1 class="pt-3 text2 font-italic ">Khoj says</h1>
                        <img src="/newassets/images/check.png" class="text3" alt="...">
                        <h1 class="pt-3 text4 ">Age 8+</h1><img src="/newassets/images/rating.png" class="text5" alt="...">
                        <h1 class="pt-3 text6 ">Maidaan is an upcoming Indian Hindi-language biographical sports</h1>
                        <h1 class="pt-3 text7 ">Full Expert views</h1>
                        <img src="/newassets/images/play.png" class="text8" alt="...">
                        <h1 class="pt-3 text9 ">Video Reviews</h1>
                      </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-6 none ksiwc hover">
                      <a href="#"> <img src="/newassets/images/img1.png" style="width: 100%; " alt="..."></a>
                      <div class="position_effect">
                        <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                            class="fas fa-check"></i></button>
                        <button type="button" class="btn btn-light css-ksiwc2 seentext">Seen it</button>
                      </div>


                      <div class="position_effect2">
                        <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                            class="fas fa-plus"></i></button>
                        <button type="button" class="btn btn-light css-ksiwc2 seentext">+ Want to see</button>
                      </div>
                      <div class="position_effect3">
                        <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                            class="fas fa-star"></i></button>
                        <button type="button" class="btn btn-light css-ksiwc2 seentext"><i class="fas fa-star"></i><i
                            class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i
                            class="fas fa-star"></i></button>
                      </div>

                      <h4 class="pt-3 text-white">2017,Hindi ,Action</h4>
                      <p class="font-weight-bold text-white">Gully Boy</p>
                      <div class="overlay3">
                        <h1 class="pt-3 text font-weight-bold">Gully Boy</h1>
                        <h1 class="pt-3 text2 font-italic ">Khoj says</h1>
                        <img src="/newassets/images/check.png" class="text3" alt="...">
                        <h1 class="pt-3 text4 ">Age 8+</h1><img src="/newassets/images/rating.png" class="text5" alt="...">
                        <h1 class="pt-3 text6 ">Maidaan is an upcoming Indian Hindi-language biographical sports</h1>
                        <h1 class="pt-3 text7 ">Full Expert views</h1>
                        <img src="/newassets/images/play.png" class="text8" alt="...">
                        <h1 class="pt-3 text9 ">Video Reviews</h1>
                      </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-4 none dnone ksiwc hover">
                      <a href="#"> <img src="/newassets/images/img5.png" style="width: 100%; " alt="..."></a>
                      <div class="position_effect">
                        <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                            class="fas fa-check"></i></button>
                        <button type="button" class="btn btn-light css-ksiwc2 seentext">Seen it</button>
                      </div>


                      <div class="position_effect2">
                        <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                            class="fas fa-plus"></i></button>
                        <button type="button" class="btn btn-light css-ksiwc2 seentext">+ Want to see</button>
                      </div>
                      <div class="position_effect3">
                        <button type="button" class="btn btn-light css-ksiwc2 icon_text"><i
                            class="fas fa-star"></i></button>
                        <button type="button" class="btn btn-light css-ksiwc2 seentext"><i class="fas fa-star"></i><i
                            class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i
                            class="fas fa-star"></i></button>
                      </div>

                      <h4 class="pt-3 text-white">2017,Hindi ,Action</h4>
                      <p class="font-weight-bold text-white">Gully Boy</p>
                      <div class="overlay3">
                        <h1 class="pt-3 text font-weight-bold">Gully Boy</h1>
                        <h1 class="pt-3 text2 font-italic ">Khoj says</h1>
                        <img src="/newassets/images/check.png" class="text3" alt="...">
                        <h1 class="pt-3 text4 ">Age 8+</h1><img src="/newassets/images/rating.png" class="text5" alt="...">
                        <h1 class="pt-3 text6 ">Maidaan is an upcoming Indian Hindi-language biographical sports</h1>
                        <h1 class="pt-3 text7 ">Full Expert views</h1>
                        <img src="/newassets/images/play.png" class="text8" alt="...">
                        <h1 class="pt-3 text9 ">Video Reviews</h1>
                      </div>
                    </div>

                  </div>
                </div>
              </div>
              <a class="carousel-control-prev " href="#carouselExampleControls6" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
              </a>
              <a class="carousel-control-next " href="#carouselExampleControls6" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="language mb-5">
    <div class="container">
      <div class="row boss">
        <div class="col-xl-6 col-lg-6 col-md-8 col-sm-8 col-8">
          <h2 class="font-weight-bold">Whats your Language, Boss</h2>
        </div>
        <div class="col-xl-6 col-lg-6 col-md-4 col-sm-4 col-4">
          <p class="view text-right"> <a class="nav-link active Text-white" href="#">View More</a></p>
        </div>
      </div>
      <div class="row mt-5 mb-5">
        <div id="carouselExampleControls7" class="carousel slide" data-ride="carousel" data-interval="false">
          <div class="carousel-inner">
            <div class="carousel-item active">
              <div class="row ">
                <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1 col-1">

                </div>
                <div class="col-xl-1 col-lg-1 col-md-2 col-sm-2 col-2">
                  <a href="#"> <img src="/newassets/images/genre1.png" style="width: 100% ;" alt="..."></a>
                  <p class=" pt-2 lang font-weight-bold text-white text-center">English</p>
                </div>
                <div class="col-xl-1 col-lg-1 col-md-2 col-sm-2 col-2">
                  <a href="#"> <img src="/newassets/images/genre2.png" style="width: 100%;" alt="..."></a>
                  <p class=" pt-2 lang font-weight-bold text-white text-center">Hindi</p>
                </div>
                <div class="col-xl-1 col-lg-1 col-md-2 col-sm-2 col-2">
                  <a href="#"> <img src="/newassets/images/genre3.png" style="width: 100%; " alt="..."></a>
                  <p class=" pt-2 lang font-weight-bold text-white text-center">Punjabi</p>
                </div>
                <div class="col-xl-1 col-lg-1 col-md-2 col-sm-2 col-2">
                  <a href="#"> <img src="/newassets/images/genre4.png" style="width: 100% ; " alt="..."></a>
                  <p class=" pt-2 lang font-weight-bold text-white text-center">Tamil</p>
                </div>
                <div class="col-xl-1 col-lg-1 col-md-2 col-sm-2 col-2">
                  <a href="#"> <img src="/newassets/images/genre5.png" style="width: 100%; " alt="..."></a>
                  <p class=" pt-2 lang font-weight-bold text-white text-center">Telugu</p>
                </div>
                <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1 col-1 none dnone non">
                  <a href="#"> <img src="/newassets/images/genre1.png" style="width: 100% ;" alt="..."></a>
                  <p class=" pt-2 lang font-weight-bold text-white text-center">Kannada</p>
                </div>
                <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1 col-1 none dnone non">
                  <a href="#"> <img src="/newassets/images/genre2.png" style="width: 100%;" alt="..."></a>
                  <p class=" pt-2 lang font-weight-bold text-white text-center">Marathi</p>
                </div>
                <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1 col-1 none dnone non">
                  <a href="#"> <img src="/newassets/images/genre3.png" style="width: 100%; " alt="..."></a>
                  <p class=" pt-2 lang font-weight-bold text-white text-center">Bengali</p>
                </div>
                <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1 col-1 none dnone non">
                  <a href="#"> <img src="/newassets/images/genre4.png" style="width: 100% ; " alt="..."></a>
                  <p class=" pt-2 lang font-weight-bold text-white text-center">English</p>
                </div>
                <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1 col-1 none dnone non">
                  <a href="#"> <img src="/newassets/images/genre5.png" style="width: 100%; " alt="..."></a>
                  <p class=" pt-2 lang font-weight-bold text-white text-center">English</p>
                </div>
                <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1 col-1">

                </div>
              </div>
            </div>
            <div class="carousel-item">
              <div class="row  ">
                <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1 col-1">

                </div>

                <div class="col-xl-1 col-lg-1 col-md-2 col-sm-2 col-2">
                  <a href="#"> <img src="/newassets/images/genre1.png" style="width: 100% ;" alt="..."></a>
                  <p class=" pt-2 lang font-weight-bold text-white text-center">Kannada</p>
                </div>
                <div class="col-xl-1 col-lg-1 col-md-2 col-sm-2 col-2">
                  <a href="#"> <img src="/newassets/images/genre2.png" style="width: 100%;" alt="..."></a>
                  <p class=" pt-2 lang font-weight-bold text-white text-center">Marathi</p>
                </div>
                <div class="col-xl-1 col-lg-1 col-md-2 col-sm-2 col-2">
                  <a href="#"> <img src="/newassets/images/genre3.png" style="width: 100%; " alt="..."></a>
                  <p class=" pt-2 lang font-weight-bold text-white text-center">Bengali</p>
                </div>
                <div class="col-xl-1 col-lg-1 col-md-2 col-sm-2 col-2">
                  <a href="#"> <img src="/newassets/images/genre4.png" style="width: 100% ; " alt="..."></a>
                  <p class=" pt-2 lang font-weight-bold text-white text-center">English</p>
                </div>
                <div class="col-xl-1 col-lg-1 col-md-2 col-sm-2 col-2">
                  <a href="#"> <img src="/newassets/images/genre5.png" style="width: 100%; " alt="..."></a>
                  <p class=" pt-2 lang font-weight-bold text-white text-center">English</p>
                </div>
                <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1 col-1 none dnone non">
                  <a href="#"> <img src="/newassets/images/genre1.png" style="width: 100% ;" alt="..."></a>
                  <p class=" pt-2 lang font-weight-bold text-white text-center">English</p>
                </div>
                <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1 col-1 none dnone non">
                  <a href="#"> <img src="/newassets/images/genre2.png" style="width: 100%;" alt="..."></a>
                  <p class=" pt-2 lang font-weight-bold text-white text-center">Hindi</p>
                </div>
                <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1 col-1 none dnone non">
                  <a href="#"> <img src="/newassets/images/genre3.png" style="width: 100%; " alt="..."></a>
                  <p class=" pt-2 lang font-weight-bold text-white text-center">Punjabi</p>
                </div>
                <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1 col-1 none dnone non">
                  <a href="#"> <img src="/newassets/images/genre4.png" style="width: 100% ; " alt="..."></a>
                  <p class=" pt-2 lang font-weight-bold text-white text-center">Tamil</p>
                </div>
                <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1 col-1 none dnone non">
                  <a href="#"> <img src="/newassets/images/genre5.png" style="width: 100%; " alt="..."></a>
                  <p class=" pt-2 lang font-weight-bold text-white text-center">Telugu</p>
                </div>
                <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1 col-1">

                </div>
              </div>
            </div>

          </div>
          <a class="carousel-control-prev" href="#carouselExampleControls7" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="carousel-control-next" href="#carouselExampleControls7" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
        </div>
      </div>
    </div>
  </section>

  <section class="top">
    <div class="container">
      <div class="row ">
        <div class="col-xl-3 col-lg-3 col-md-12 col-sm-12 col-12 blue">
          <div class="row one">
            <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 ">
              <p class="font-weight-bold text-white pl-4 pt-3">Top 5 List</p>
            </div>
            <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 flex ">
              <p class="mov text-right pt-3"> <a class="nav-link active Text-white" href="#">Movies</a></p>
              <p class="mov text-right pt-3"> <a class="nav-link active Text-white" href="#">TvSeries</a></p>
            </div>
          </div>

          <div class="row one">
            <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-2 ">
              <p class="font-weight-bold text-white pl-4 pt-3 two">1</p>
            </div>
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6  ">
              <p class="mov text-left pt-3 mb-0 "> <a class="nav-link active Text-white" href="#">2017</a></p>
              <p class="mov text-left mb-0"> <a class="nav-link active Text-white" href="#">Delta Bravo</a></p>
              <p class="mov text-left mb-0"> <a class="nav-link active Text-white" href="#">Action</a></p>
            </div>
            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4 ">
            </div>
          </div>

          <div class="row one">
            <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-2 ">
              <p class="font-weight-bold text-white pl-4 pt-3 two">2</p>
            </div>
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6  ">
              <p class="mov text-left pt-3 mb-0 "> <a class="nav-link active Text-white" href="#">2017</a></p>
              <p class="mov text-left mb-0"> <a class="nav-link active Text-white" href="#">Delta Bravo</a></p>
              <p class="mov text-left mb-0"> <a class="nav-link active Text-white" href="#">Action</a></p>
            </div>
            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4 ">
            </div>
          </div>

          <div class="row one">
            <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-2 ">
              <p class="font-weight-bold text-white pl-4 pt-3 two">3</p>
            </div>
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6  ">
              <p class="mov text-left pt-3 mb-0 "> <a class="nav-link active Text-white" href="#">2017</a></p>
              <p class="mov text-left mb-0"> <a class="nav-link active Text-white" href="#">Delta Bravo</a></p>
              <p class="mov text-left mb-0"> <a class="nav-link active Text-white" href="#">Action</a></p>
            </div>
            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4 ">
            </div>
          </div>

          <div class="row one">
            <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-2 ">
              <p class="font-weight-bold text-white pl-4 pt-3 two">4</p>
            </div>
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6  ">
              <p class="mov text-left pt-3 mb-0 "> <a class="nav-link active Text-white" href="#">2017</a></p>
              <p class="mov text-left mb-0"> <a class="nav-link active Text-white" href="#">Delta Bravo</a></p>
              <p class="mov text-left mb-0"> <a class="nav-link active Text-white" href="#">Action</a></p>
            </div>
            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4 ">
            </div>
          </div>

          <div class="row one">
            <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-2 ">
              <p class="font-weight-bold text-white pl-4 pt-3 two">5</p>
            </div>
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6  ">
              <p class="mov text-left pt-3 mb-0 "> <a class="nav-link active Text-white" href="#">2017</a></p>
              <p class="mov text-left mb-0"> <a class="nav-link active Text-white" href="#">Delta Bravo</a></p>
              <p class="mov text-left mb-0"> <a class="nav-link active Text-white" href="#">Action</a></p>
            </div>
            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4 ">
            </div>
          </div>

          <div class="row one">
            <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-2 ">
              <p class="font-weight-bold text-white pl-4 pt-3 two">6</p>
            </div>
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6  ">
              <p class="mov text-left pt-3 mb-0 "> <a class="nav-link active Text-white" href="#">2017</a></p>
              <p class="mov text-left mb-0"> <a class="nav-link active Text-white" href="#">Delta Bravo</a></p>
              <p class="mov text-left mb-0"> <a class="nav-link active Text-white" href="#">Action</a></p>
            </div>
            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4 ">
            </div>
          </div>

          <div class="row one">
            <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-2 ">
              <p class="font-weight-bold text-white pl-4 pt-3 two">7</p>
            </div>
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6  ">
              <p class="mov text-left pt-3 mb-0 "> <a class="nav-link active Text-white" href="#">2017</a></p>
              <p class="mov text-left mb-0"> <a class="nav-link active Text-white" href="#">Delta Bravo</a></p>
              <p class="mov text-left mb-0"> <a class="nav-link active Text-white" href="#">Action</a></p>
            </div>
            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4 ">
            </div>
          </div>

          <div class="row one">
            <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-2 ">
              <p class="font-weight-bold text-white pl-4 pt-3 two">8</p>
            </div>
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6  ">
              <p class="mov text-left pt-3 mb-0 "> <a class="nav-link active Text-white" href="#">2017</a></p>
              <p class="mov text-left mb-0"> <a class="nav-link active Text-white" href="#">Delta Bravo</a></p>
              <p class="mov text-left mb-0"> <a class="nav-link active Text-white" href="#">Action</a></p>
            </div>
            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4 ">
            </div>
          </div>

          <div class="row one">
            <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-2 ">
              <p class="font-weight-bold text-white pl-4 pt-3 two">9</p>
            </div>
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6  ">
              <p class="mov text-left pt-3 mb-0 "> <a class="nav-link active Text-white" href="#">2017</a></p>
              <p class="mov text-left mb-0"> <a class="nav-link active Text-white" href="#">Delta Bravo</a></p>
              <p class="mov text-left mb-0"> <a class="nav-link active Text-white" href="#">Action</a></p>
            </div>
            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4 ">
            </div>
          </div>

        </div>
        <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1 col-1">
          &nbsp;
        </div>
        <div class="col-xl-8 col-lg-8 col-md-12 col-sm-12 col-12">
          <div class="row one">
            <div class="col-xl-6 col-lg-6 col-md-4 col-sm-4 col-4 ">
              <p class="font-weight-bold text-white pl-4 pt-3">Newest Movies</p>
            </div>
            <div class="col-xl-6 col-lg-6 col-md-8 col-sm-8 col-8 justify-content-end flex ">
              <p class="mov text-right pt-3"> <a class="nav-link active Text-white" href="#">Today</a></p>
              <p class="mov text-right pt-3"> <a class="nav-link active Text-white" href="#">This Week</a></p>
              <p class="mov text-right pt-3"> <a class="nav-link active Text-white" href="#">Last 30 Days</a></p>
            </div>
          </div>

          <div class="row">
            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4 pt-5">
              <img src="/newassets/images/lastimg.jpg" style="width: 100%; " alt="...">
            </div>
            <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-8 pt-5 ">
              <p class="mov text-white  pt-3"> 2018</p>
              <p class="mov text-white "> The Last Witness</p>
              <p class="mov text-white "> In 1892, a legendary Army captain reluctantly agrees to escort a Cheyenne
                chief
                and his family through dangerous territory.</p>
              <div class="row buttn pl-4">
                <button type="button"
                  class="btn btn-primary btn-lg text-center shadow-lg mt-5 watch2">Watchlist</button>&nbsp; &nbsp;
                <button type="button" class="btn btn-primary btn-lg text-center watch mt-5">+ Playlist</button>
              </div>
            </div>
          </div>
          <div class="col-xl-5 col-lg-5 col-md-12 col-sm-12 col-12 ">
            <div class="row one pt-5">
              <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4 pt-5 pb-2">
                <img src="/newassets/images/lastimg.jpg" style="width: 100%; " alt="...">
              </div>
              <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 pt-5 ">
                <p class="mov text-left  mb-0 "> <a class="nav-link active Text-white" href="#">2017</a></p>
                <p class="mov text-left mb-0"> <a class="nav-link active Text-white" href="#">Delta Bravo</a></p>
                <p class="mov text-left mb-0"> <a class="nav-link active Text-white" href="#">Action</a></p>
              </div>
              <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-2 pt-5">
              </div>
            </div>

            <div class="row one pt-1">
              <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4 pt-5 pb-2">
                <img src="/newassets/images/lastimg.jpg" style="width: 100%; " alt="...">
              </div>
              <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 pt-5 ">
                <p class="mov text-left  mb-0 "> <a class="nav-link active Text-white" href="#">2017</a></p>
                <p class="mov text-left mb-0"> <a class="nav-link active Text-white" href="#">Delta Bravo</a></p>
                <p class="mov text-left mb-0"> <a class="nav-link active Text-white" href="#">Action</a></p>
              </div>
              <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-2 pt-5">
              </div>
            </div>

            <div class="row one pt-1">
              <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4 pt-5 pb-2">
                <img src="/newassets/images/lastimg.jpg" style="width: 100%; " alt="...">
              </div>
              <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 pt-5 ">
                <p class="mov text-left  mb-0 "> <a class="nav-link active Text-white" href="#">2017</a></p>
                <p class="mov text-left mb-0"> <a class="nav-link active Text-white" href="#">Delta Bravo</a></p>
                <p class="mov text-left mb-0"> <a class="nav-link active Text-white" href="#">Action</a></p>
              </div>
              <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-2 pt-5">
              </div>
            </div>

            <div class="row one pt-1">
              <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4 pt-5 pb-2">
                <img src="/newassets/images/lastimg.jpg" style="width: 100%; " alt="...">
              </div>
              <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 pt-5 ">
                <p class="mov text-left  mb-0 "> <a class="nav-link active Text-white" href="#">2017</a></p>
                <p class="mov text-left mb-0"> <a class="nav-link active Text-white" href="#">Delta Bravo</a></p>
                <p class="mov text-left mb-0"> <a class="nav-link active Text-white" href="#">Action</a></p>
              </div>
              <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-2 pt-5">
              </div>
            </div>

            <div class="row one pt-1">
              <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4 pt-5 pb-2">
                <img src="/newassets/images/lastimg.jpg" style="width: 100%; " alt="...">
              </div>
              <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 pt-5 ">
                <p class="mov text-left  mb-0 "> <a class="nav-link active Text-white" href="#">2017</a></p>
                <p class="mov text-left mb-0"> <a class="nav-link active Text-white" href="#">Delta Bravo</a></p>
                <p class="mov text-left mb-0"> <a class="nav-link active Text-white" href="#">Action</a></p>
              </div>
              <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-2 pt-5">
              </div>
            </div>

          </div>
          <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-2 "></div>

          <div class="col-xl-5 col-lg-5 col-md-12 col-sm-12 col-12 ">
            <div class="row one pt-5">
              <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4 pt-5 pb-2">
                <img src="/newassets/images/lastimg.jpg" style="width: 100%; " alt="...">
              </div>
              <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 pt-5 ">
                <p class="mov text-left  mb-0 "> <a class="nav-link active Text-white" href="#">2017</a></p>
                <p class="mov text-left mb-0"> <a class="nav-link active Text-white" href="#">Delta Bravo</a></p>
                <p class="mov text-left mb-0"> <a class="nav-link active Text-white" href="#">Action</a></p>
              </div>
              <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-2 pt-5">
              </div>
            </div>

            <div class="row one pt-1">
              <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4 pt-5 pb-2">
                <img src="/newassets/images/lastimg.jpg" style="width: 100%; " alt="...">
              </div>
              <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 pt-5 ">
                <p class="mov text-left  mb-0 "> <a class="nav-link active Text-white" href="#">2017</a></p>
                <p class="mov text-left mb-0"> <a class="nav-link active Text-white" href="#">Delta Bravo</a></p>
                <p class="mov text-left mb-0"> <a class="nav-link active Text-white" href="#">Action</a></p>
              </div>
              <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-2 pt-5">
              </div>
            </div>

            <div class="row one pt-1">
              <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4 pt-5 pb-2">
                <img src="/newassets/images/lastimg.jpg" style="width: 100%; " alt="...">
              </div>
              <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 pt-5 ">
                <p class="mov text-left  mb-0 "> <a class="nav-link active Text-white" href="#">2017</a></p>
                <p class="mov text-left mb-0"> <a class="nav-link active Text-white" href="#">Delta Bravo</a></p>
                <p class="mov text-left mb-0"> <a class="nav-link active Text-white" href="#">Action</a></p>
              </div>
              <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-2 pt-5">
              </div>
            </div>

            <div class="row one pt-1">
              <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4 pt-5 pb-2">
                <img src="/newassets/images/lastimg.jpg" style="width: 100%; " alt="...">
              </div>
              <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 pt-5 ">
                <p class="mov text-left  mb-0 "> <a class="nav-link active Text-white" href="#">2017</a></p>
                <p class="mov text-left mb-0"> <a class="nav-link active Text-white" href="#">Delta Bravo</a></p>
                <p class="mov text-left mb-0"> <a class="nav-link active Text-white" href="#">Action</a></p>
              </div>
              <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-2 pt-5">
              </div>
            </div>

            <div class="row one pt-1">
              <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4 pt-5 pb-2">
                <img src="/newassets/images/lastimg.jpg" style="width: 100%; " alt="...">
              </div>
              <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 pt-5 ">
                <p class="mov text-left  mb-0 "> <a class="nav-link active Text-white" href="#">2017</a></p>
                <p class="mov text-left mb-0"> <a class="nav-link active Text-white" href="#">Delta Bravo</a></p>
                <p class="mov text-left mb-0"> <a class="nav-link active Text-white" href="#">Action</a></p>
              </div>
              <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-2 pt-5">
              </div>
            </div>

          </div>


        </div>
      </div>
  </section>

  <section class="blog">
    <div class="container">
      <div class="row pt-5">
        <div class="container">
          <h1 class="font-weight-bold up">Blog</h1>
        </div>
      </div>
      <div class="row pt-5">
        <div class="container">
          <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 pt-5 pl-0">
            <a href="blog%20detail.html"> <img src="/newassets/images/blog1.jpg" style="width: 100%; " alt="..."></a>
          </div>
          <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 ">
            <a href="blog%20detail.html"> <h2 class="font-weight-bold world">World Starts With Word Peace</h2></a>
            <div class="row">
              <p class="view text-white pl-2 "> <a class="nav-link active Text-white" href="#">Tennis, TV Rumors</a></p>
              <p class="text-white font-weight-bold pr-1 pt-2">:</p>
              <p class="text-white font-italic  pt-2">February 5 2019</p>
            </div>
            <p class="text-white pt-2 pl-2">Praesent iaculis, purus ac vehicula mattis, arcu lorem blandit
              nisl, non laoreet dui mi eget elit. Donec porttitor</p>
          </div>
        </div>
      </div>

      <div class="row pt-5">
        <div class="container">
          <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 pt-5 pl-0">
            <a href="blog%20detail.html"> <img src="/newassets/images/blog2.jpg" style="width: 100%; " alt="..."></a>
          </div>
          <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
            <a href="blog%20detail.html">   <h2 class="font-weight-bold world">World Starts With Word Peace</h2></a>
            <div class="row">
              <p class="view text-white pl-2 "> <a class="nav-link active Text-white" href="#">Tennis, TV Rumors</a></p>
              <p class="text-white font-weight-bold pr-1 pt-2">:</p>
              <p class="text-white font-italic  pt-2">February 5 2019</p>
            </div>
            <p class="text-white pt-2 pl-2">Praesent iaculis, purus ac vehicula mattis, arcu lorem blandit
              nisl, non laoreet dui mi eget elit. Donec porttitor</p>
          </div>
        </div>
      </div>





    </div>
  </section>
@endsection