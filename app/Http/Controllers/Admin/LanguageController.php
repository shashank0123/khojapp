<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class LanguageController extends Controller
{
    public function index(){
        $page = 0;
        if(isset($_GET['page'])){
            $page = $_GET['page']-1;
        }
        $languages = DB::table('languages')->orderBy('display_priority','ASC')->orderBy('english_name','ASC')->paginate(35);

        return view('admin.languages.list',compact('languages','page'));
    }

public function getSearchedResult(Request $request){
    	$page = 0;
        $keyword = $request->keyword;
    	if(isset($_GET['page'])){
    		$page = $_GET['page']-1;
    	}
    	$languages = DB::table('languages')->where('english_name','LIKE','%'.$keyword.'%')->orderBy('display_priority','ASC')->orderBy('english_name','ASC')->paginate(35);

    	return view('admin.languages.list',compact('languages','page','keyword'));
    }

     public function update(Request $request)
    {
        $data = $request->all();

        $provider = DB::table('languages')->where('id',$data['id'])->update(
            array(
                'display_priority' => $data['display_priority'],
            )
        );

        return redirect()->back()->with('message','Data successfully updated');
    }

    public function changeStatus($status , $id)
    {
        

        $provider = DB::table('languages')->where('id',$id)->update(
            array(
                'status' => $status,
            )
        );

        return redirect()->back()->with('message','Status successfully updated');
    }



}
