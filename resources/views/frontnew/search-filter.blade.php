<?php
$show_count =0;
if($message == 'No data available'){
	?>
	<style type="text/css">
	#dynamic-paginate { display: none; }
</style>
<?php
}
?>	
@if (!empty($contents))
<?php $rat=0; ?>
<input type="hidden" name="type" value="{{$total_pages}}" id="dynamic_total_page">
@foreach ($contents as $element)
<?php
$show_count++;
?>
<div class="col-sm-3 col-lg-2 col-xs-6 search-response dynamic">
	@if($type == 'Tv')							
	<a  href="/tvseries/@if(isset($element->id)){{$element->id}}@endif">
		@elseif($type=='Movies')
		<a  href="/movie/@if(isset($element->id)){{$element->id}}@endif">
			@elseif($type=='All')

			@if(isset($element->media_type) && $element->media_type == 'movie')
			<a href="/movie/@if(isset($element->id)){{$element->id}}@endif"><?php 
			if(isset($element->original_title))
				echo preg_replace('/[^A-Za-z0-9 \-]/', '', $element->original_title); ?>
			@else
			<a href="/tv/@if(isset($element->id)){{$element->id}}@endif"><?php 
			if(isset($element->name))
				echo preg_replace('/[^A-Za-z0-9 \-]/', '', $element->name); ?>
			@endif

			@endif
			<div class="movie-item">
				<div class="mv-img">
					@if(isset($element->poster_path))
					<img src="https://image.tmdb.org/t/p/w300{{$element->poster_path}}" alt="" class="web-image">
					@else
					<img src="/images/default.png" alt="" class="web-image">
					@endif
				</div>
				<div class="hvr-inner">
					@if($type == 'Tv')							
					<a  href="/tvseries/@if(isset($element->id)){{$element->id}}@endif"><i class="fa fa-play" aria-hidden="true"></i> </a>
					@else
					<a  href="/movie/@if(isset($element->id)){{$element->id}}@endif"><i class="fa fa-play" aria-hidden="true"></i> </a>
					@endif					
				</div>
				<div class="title-in">
					<p><i class="ion-android-star"></i><span>@if(isset($element->vote_average)){{$element->vote_average}}@else{{'0'}}@endif</span> /10</p>
				</div>
				<div class="imdb">
					<div class="row">
						@if($type == 'Tv')
						@if(isset($element->original_name))
						<h6><a href="/tvseries/@if(isset($element->id)){{$element->id}}@endif"><?php 
						if(isset($element->original_name))
							echo substr($element->original_name,0,5);
							# echo substr(preg_replace('/[^A-Za-z0-9 \-]/', '', $element->original_name),0,5);
							 ?>
								
							</a></h6>
						@else
						<h6><a href="/tvseries/@if(isset($element->id)){{$element->id}}@endif"><?php 
						if(isset($element->title))
							echo substr($element->title,0,50); 
							# echo substr(preg_replace('/[^A-Za-z0-9 \-]/', '', $element->title),0,50); 
						?>								
							</a></h6>
						@endif
						@elseif($type == 'Movies')

						<h6><a href="/movie/@if(isset($element->id)){{$element->id}}@endif"><?php 
						if(isset($element->title))
							echo substr($element->title,0,50);  
							// echo substr(preg_replace('/[^A-Za-z0-9 \-]/', '', $element->title),0,50); 
						?></a></h6>
						@else
						@if(isset($element->media_type) && $element->media_type == 'movie')
						<h6><a href="/movie/@if(isset($element->id)){{$element->id}}@endif"><?php 
						if(isset($element->original_title))
							echo substr($element->original_title,0,50); 
							// echo substr(preg_replace('/[^A-Za-z0-9 \-]/', '', $element->original_title),0,50);
							 ?></a></h6>
						@else
						<h6><a href="/tv/@if(isset($element->id)){{$element->id}}@endif"><?php 
						if(isset($element->name))
							echo substr($element->name,0,50); 
							// echo substr(preg_replace('/[^A-Za-z0-9 \-]/', '', $element->name),0,50); 
						?></a></h6>
						@endif
						@endif
					</div>
					<div class="row">
						<div class="col-sm-6 col-xs-6"><b>IMDB</b>: <span style="color: yellow">@if(isset($imdb_rating[$rat])){{$imdb_rating[$rat]}}@else{{'N/A'}}@endif</span></div>
						<div class="col-sm-3 col-xs-3">RT</div>
						<div class="col-sm-3 col-xs-3">UA</div>
					</div>
				</div>
			</div>
		</a>
	</div>
<?php $rat++; ?>
	@endforeach
	@endif

	@if($show_count == 0)
	<h2 style="text-align: center;color: #fff">No Result Found
		<br><br></h2>
		@endif

	</div>