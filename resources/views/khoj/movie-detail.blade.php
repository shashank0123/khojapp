@extends('layouts.khoj_new')
<?php use \App\Http\Controllers\KhojNewController; $url = []; 
use App\FeaturedImage;
$homeside = FeaturedImage::where('status','Active')->where('page_title','moviedetailscast')->where('start_date','<=',date('Y-m-d'))->where('end_date','>=',date('Y-m-d'))->orderBy('display_priority', 'asc')->get();
$titleside = FeaturedImage::where('status','Active')->where('page_title','moviedetailstitle')->where('start_date','<=',date('Y-m-d'))->where('end_date','>=',date('Y-m-d'))->orderBy('display_priority', 'asc')->get();


?>

@section('tags')
{{$movie->tags ?? ''.$movie->title ?? ''.','.$movie->title ?? ''.' Movie Review,'.$movie->title ?? ''.' Review,'.$movie->title ?? ''.' Film Review,'.$movie->title ?? ''.' movie rating,'.$movie->title ?? ''.' Star rating,'.$movie->title ?? ''.' News,'.$movie->title ?? ''.' Streaming,Gulabo '.$movie->title ?? ''.' '.$movie->title ?? ''.' khojaap online movie search, watch online, '}} {{$movie->title ?? ''}} {{ $movie->original_title ?? '' }}
@endsection

@section('title')

@if(!empty($movie->meta_title)){{$movie->meta_title ?? ''}}
@elseif(!empty($movie->title)){{"Find out where ".$movie->title." is available to watch online" ?? ''}}
@else{{"Find out where ".$movie->original_title." is available to watch online" ?? ''}}@endif
@endsection

@section('description')

@if(!empty($movie->meta_description)){{$movie->meta_description ?? ''}}
@elseif(!empty($movie->short_description)){{$movie->short_description ?? ''}}
@elseif(!empty($movie->title)){{$movie->title ?? ''}}
@else{{$movie->original_title ?? ''}}@endif

@endsection


@section('banner')
<div class="row ipad-width2" >
	<div class="col-md-12 col-sm-12 col-xs-12">
		@if($errors->any())
		<div class="alert alert-danger" style="position: absolute; top: 2px">
			@foreach($errors->all() as $error)
			<li style="color: #fff">{{ $error }}</li>
			@endforeach
		</div>
		@endif
		@if($message = Session::get('message'))
		<div class="btn btn-success" style="width: 100%; position: absolute; top: 2px">
			<p style="color: #fff">{{ $message }}</p>
		</div>
		@endif
		@if(!empty($clips[0]) && ($clips[0]->external_id != ''))
		<div class="movie-img ">
			<iframe width="100%" height="350px" src="@if(!empty($clips[0]))https://www.youtube.com/embed/{{$clips[0]->external_id}}@endif" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
		</div>
		@else
		@section('banner')
		@if(!empty($banners) && count($banners)>0)
		<div id="bootstrap-touch-slider" class="carousel bs-slider fade  control-round indicators-line" data-ride="carousel" data-pause="hover" data-interval="false" >
			<!-- Indicators -->
			<!-- Wrapper For Slides -->
			<div class="carousel-inner" role="listbox" style="margin-top: 0; max-height: 350px; width: auto;">
				<?php $i=0; $bcount = 0;?>
				@foreach ($banners as $banner)
				<!-- Third Slide -->
				<div class="item @if($i==0){{'active'}}@endif" style="max-height: 350px; width: auto;">
					<!-- Slide Background -->
					@if(!empty($banner->banner_url) && file_exists(public_path().'/banners'.$banner->banner_url))
					@php $bcount++ @endphp
					<img src="{{asset('banners'.$banner->banner_url)}}" alt=""  class="slide-image" style="max-height: 350px; width: auto;" placeholder="{{ $movie->title ?? $movie->original_title }}"/>
					
					<!-- <img src="/images/default.png" alt=""  class="slide-image"/> -->
					@endif
					<div class="bs-slider-overlay"></div>
				</div>
				<!-- End of Slide -->
				<?php $i++; ?>
				@endforeach
			</div>
			<ol class="carousel-indicators">
				<?php $i=0; ?>
				@if($bcount>0)
				@foreach($banners as $banner)
				<li data-target="#bootstrap-touch-slider" data-slide-to="{{$i}}" class="@if($i==0){{'active'}}@endif"></li>
				<?php $i++; ?>
				@endforeach
				@endif
			</ol>
         <!-- <a class="left carousel-control" href="#bootstrap-touch-slider" role="button" data-slide="prev">
            <span class="fa fa-angle-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a> -->
        <!-- Right Control -->
         <!-- <a class="right carousel-control" href="#bootstrap-touch-slider" role="button" data-slide="next">
            <span class="fa fa-angle-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a> -->
    </div>
    @endif
    @endsection
    @endif
</div>
</div>
@endsection
@section('content')
<style type="text/css">
	@media only screen and (max-width: 760px) {

		#setproviders {
			background-color: #555;
			width: 25% !important; 
			margin-left: 2%;
			margin-bottom: 5px;
			margin-top: 1%;
		}
	}
</style>
<?php
//dd($movie);
if (!$movie ){
	//die();
	return redirect('/');
}
?>
<div class="page-single movie-single movie_single">
	<div class="container">
		<div class="row">
			<div class="col-sm-12" style="text-align: left;">
				<?php 
				$get11 = ""; $get22 = "" ; $get33 = "";
				$name1 = ""; $name2 = "" ; $name3 = "";
				$link = "<a href='/'> HOME </a>";
				if ($movie)
					$get1 = DB::table('contents')->where('content_id',$movie->parent_id)->first();

				if(!empty($get1)){
					$urlen = preg_replace('/[^a-zA-Z0-9_.]/', '-', (strtolower($get1->title))).'-'.$get1->content_id;
					$get11 = "/detail/".$get1->content_type."/".$urlen;
					$name1 = $get1->title ?? '';


					$get2 = DB::table('contents')->where('content_id',$get1->parent_id)->first();

					if(!empty($get2)){
						$urlen = preg_replace('/[^a-zA-Z0-9_.]/', '-', (strtolower($get2->title))).'-'.$get2->content_id;
						$get22 = "/detail/".$get2->content_type."/".$urlen;
						$name2 = $get2->title ?? '';

						$get3 = DB::table('contents')->where('content_id',$get2->parent_id)->first();

						if(!empty($get3)){
							$urlen = preg_replace('/[^a-zA-Z0-9_.]/', '-', (strtolower($get3->title))).'-'.$get3->content_id;
							$get33 = "/detail/".$get3->content_type."/".$urlen;
							$name3 = $get3->title ?? '';
						}
					}
				}

				?>
				<h5 style="font-size: 14px;">
					<a href="{{url('/')}}">&nbsp;&nbsp;&nbsp;&nbsp; HOME</a> @if($get33!="") > <a href="{{$get33}}">{{$name3}}</a>@endif @if($get22!="") > <a href="{{$get22}}">{{$name2}}</a>@endif @if($get11!="") > <a href="{{$get11}}">{{$name1}}</a>@endif > <a href="{{url()->current()}}"><span style="color: #ffffff">{{$movie->title ?? ''}}</span></a>
				</h5>
				<br>
			</div>
		</div>
	</div>
	<div class="container">
		@php $miny=1900; $maxy=date('Y'); $minr=0; $maxr=10; @endphp
		<div class="row ">
			<div class="col-md-12 col-sm-12 col-xs-12 ">
				<div class="container row"  id="movie-detail">
					<div class="col-sm-4" style="text-align: center;">
						@if(!empty($movie->poster) && strpos($movie->poster, 'poster/') == false)
						<img src="/poster/{{$movie->poster}}" alt="{{$movie->title ?? ''}}" class="web-images">
						@else
						<img src="/images/default.png" alt="{{$movie->title ?? ''}}" class="web-images">
						@endif
						<div class="row">
							<div class="col-md-12 col-sm-12 col-xs-12">
								<div class="synopsys" onclick="getSynopsys()">
									<h4>Synopsys <i class="fa fa-angle-down"></i></h4>
									<div class="synopsys-list" style="cursor: pointer; max-height: 220px; overflow: auto; color: white; padding-right: 15px">
										{{$movie->short_description ?? 'No Description Available'}}
									</div>
								</div>
								<div class="cast" onclick="getCast()">
									<h4><br>Cast <i class="fa fa-angle-down"></i></h4>
									<div class="cast-list" style="cursor: pointer; max-height: 300px; overflow-y: auto">
										<div class="row">
											@if(!empty($casts))
											@php $ci=0 @endphp 
											@foreach($casts as $cast)
											<div style="min-height: 110px;" class="col-sm-4 col-xs-4 cast-info" >
												<?php $urlen = preg_replace('/[^a-zA-Z0-9_.]/', '-', (strtolower($cast->name))).'-'.$cast->person_id; ?>
												<a href="{{url('casts/'.$urlen)}}">
													@if(!empty($cast->poster))
													<img class="lazy" data-src="@if (filter_var($cast->poster, FILTER_VALIDATE_URL)){{$cast->poster}}@else{{asset('images/default_user.png')}}@endif" alt="{{$cast->name ?? ''}}" style="width: 60%; height: 67px; margin:0 10%; border-radius: 50%;">
													@else
													<img class="lazy" data-src="{{asset('images/default_user.png')}}" alt="{{$cast->name ?? ''}}" style="width: 60%; height: auto; margin:0 10%">
													@endif
													<br>
													{{substr($cast->name ?? '',0,14)}}  </a>
													
												</div>
												@php $ci++ @endphp
												@endforeach
												@if($ci==0)
												{{'No Cast Available'}}
												@endif
												@endif
											</div>
										</div>
									</div>

								</div>
							</div>
							@if (isset($homeside))
							@foreach ($homeside as $ads)
							<div>
								<a href="{{$ads->redirect_url ?? ''}}">
									<img src="{{asset($ads->poster)}}" style="width:100%; height: 240px">
								</a>

							</div>
							@endforeach
							@endif
						</div>
						<div class="col-sm-8">
							<div class="movie-detail">
								@if(!empty($movie->title))
								<h1 style="margin-bottom: 2px" class="bd-hd">{{$movie->title}} </h1>
								@elseif(!empty($movie->original_title))
								<h1 style="margin-bottom: 2px" class="bd-hd">{{$movie->original_title}} </h1>
								@endif
								<div class="col-md-4">
									<div style="font-size: 14; color: #9ba09b; padding-left: 5px">
										@if(isset($movie->original_release_year) && $movie->original_release_year!="") 
										<i class="ion-calculator" style="font-size: 14px; "></i>  &nbsp;{{$movie->original_release_year ?? ""}} 


										

										@elseif(!empty($movie->cinema_release_date) && $movie->cinema_release_date != '0000-00-00')
										<i class="ion-calculator" style="font-size: 14px;"></i> &nbsp;{{explode('-',$movie->cinema_release_date)[0] }}

										@endif &nbsp;&nbsp;&nbsp;


										<?php
										if(!empty($movie->runtime)){
											if ($movie->content_source != "justwatch"){
												$hours = floor($movie->runtime / (60*60));
												$min = round(($movie->runtime - ($hours * (60*60)))/60);
											}
											else{
												$hours = floor($movie->runtime / 60);
												$min = $movie->runtime - ($hours * 60);
											}
											?>

											<i class="ion-clock" style="font-size: 14px; "></i>&nbsp;

											<?php
											echo $hours." hrs ".$min." mins";
										}

										?>
									</div>

									<div class="rating">
										<ul>
											@if($movie->content_type)
											<li style="font-size: 12px; color: #9ba09b;">  {{strtoupper($movie->content_type)}}</li>
											@endif

											@if($movie->imdb_score)
											<li style="font-size: 12px; color: #9ba09b;">  IMDB&nbsp;-   &nbsp;   {{$movie->imdb_score}}</li>
											@endif

											@if($movie->tmdb_score)
											<li style="font-size: 12px; color: #9ba09b;">  RATING&nbsp;-&nbsp;&nbsp;{{$movie->tmdb_score}}</li>
											@endif 

											<li style="font-size: 12px; color: #9ba09b">@if(!empty($movie->age_certification))
												<span style=" color: #9ba09b">{{$movie->age_certification}}</span>
												@else
												<span style=" color: #9ba09b">{{'U/A'}}</span>
												@endif
											</li>
										</ul>
									</div>
									<div class="movie-genre">
										<ul>
											@if(!empty($genres))
											@foreach($genres as $genre)
											@if(!empty($genre->name))
											<li>{{$genre->name}}</li>
											@endif
											@endforeach
											@endif
										</ul>
									</div>
									<div class="social-btn" id="hide-icon">
										<div class="container row" style="padding-left: 0px;">
											<div class="col-sm-10">
												@if (!empty($favourite))
												<a href="#" class="parent-btn yellow" title="Already in your favourite list"><i class="ion-heart yellow-icon"></i></a>
												@else
												<a href="/favourite/{{'Movie'}}/{{$movie->content_id}}" class="parent-btn" title="Add to favourite"><i class="ion-heart"></i></a>
												@endif
												@if (!empty($watchlist))						
												<a href="#" class="parent-btn yellow" title="Already in your watchlist"><i class="ion-plus yellow-icon"></i></a>
												@else
												<a href="/watchlist/{{'Movie'}}/{{$movie->content_id}}" class="parent-btn" title="Add to watchlist"><i class="ion-plus"></i></a>
												@endif
												<?php $urlen = preg_replace('/[^a-zA-Z0-9_.]/', '-', (strtolower($movie->title))).'-'.$movie->content_id; ?>
												<a href="https://www.facebook.com/sharer/sharer.php?u={{url()->current()}}"><i class="ion-social-facebook" style=" background-color: #006699; color: #fff; padding: 8px 14px; border-radius: 50%; font-size: 14px; margin-right: 9px"></i></a>&nbsp;
												<a href="http://www.twitter.com/intent/tweet?url={{url('movie')}}/{{$urlen}}"><i class="ion-social-twitter" style=" background-color: #00ACED; color: #fff; padding: 8px 10px; border-radius: 50%; font-size: 14px"></i></a>&nbsp;
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-8">
									
									@if (isset($titleside))
									@foreach ($titleside as $ads)
									<div>
										<a href="{{$ads->redirect_url ?? ''}}">
											<img src="{{asset($ads->poster)}}" style="width:100%; height: 150px">
										</a>

									</div>
									@endforeach
									@endif



								</div>
								<div class="row ipad-width2 app-icons" style="margin-top: 45px">
									<div class="col-md-12 col-sm-12 col-xs-12">
										<h4>
										Watch Now</h4>
										<div class="app-get">
											<div class="row">


												<table class="table table-bordered">
													<tbody>
														@php $countpro = 0 @endphp
														@if(!empty($streams) && count($streams)>0)
														<tr>
															<th style="width: 20px" id="movie-providers"><h4>Stream<br><br></h4></th>
															<td colspan="14">
																<?php $i = 0; $prev = ""; $qlty = "";?>
																@foreach($streams as $con)
																<?php if (in_array($con->web_url, $url) && $con->web_url != ""){continue;} ?>
																<?php 

																$url[] = $con->web_url;


																?>
																<?php
																$countpro++;
																$providers1 = DB::table('providers')->where('provider_id',$con->provider_id)->first(); 
																if(!empty($providers1)){

																	if ($con->retail_price <= 0 || $con->monetization_type == 'flatrate' || $con->monetization_type == 'Flatrate' ||  $con->monetization_type == 'free'){ 
																		if($prev == $con->provider_id ){
																			continue;
																		}
																		?>
																		@if ($con->retail_price < 1 && $i == 0)
																		<?php $i = 1; ?>

																		@endif
																		<?php $con->web_url = str_replace('n//', 'n/', $con->web_url) ?>
																		<div class="col-sm-1 col-xs-2" id="setproviders">
																			
																			@if($providers1->title=='YouTube')
																			@if(!empty($con->web_url))
																			<?php


																			if(!empty($con->web_url))
																				$url = explode('https://www.youtube.com/watch?v=',$con->web_url ?? '');
																			if (isset($url[1])) $url2 = explode('&',$url[1]);
																			$url1 = $url2[0] ?? '';
																			?>
																			<a data-toggle="modal" data-target="#exampleModal"  onclick="showVideo('{{ $url1 ?? ' ' }}')">
																				@else

																				<a href="{{KhojNewController::prepareurl($con->web_url ?? '')}}" target="blank">

																					@endif

																					@else

																					<a href="{{KhojNewController::prepareurl($con->web_url ?? '')}}" target="blank">

																						@endif

																						<img src="{{asset('/asset'.$providers1->icon_url)}}" alt="{{$providers1->title ?? ''}}" class="app-img">
																						<p>@if ($con->retail_price > 0) {{$con->retail_price}}@elseif($con->monetization_type == 'flatrate' || $con->monetization_type == 'Flatrate'){{'Subs'}}@else{{ucfirst($con->monetization_type)}}@endif <span style="color: yellow"> {{strtoupper($con->video_quality)}} </span>
																						</p>
																					</a>
																				</div>
																				<?php 
																				$qlty = $con->video_quality;
																				$prev = $con->provider_id;
																			}
																		} ?>
																		@endforeach

																	</td>
																</tr>
																@endif


																@if(!empty($buys) && count($buys)>0)
																<tr>
																	<th id="movie-providers"><h4>Buy/Rent<br><br></h4></th>
																	<td colspan="14">

																		<?php $i = 0; $prev = ""; $qlty = "";?>

																		@foreach($buys as $con)
																		<?php if (in_array($con->web_url, $url)){continue;} ?>
																		<?php 

																		$url[] = $con->web_url;


																		?>
																		<?php
																		$countpro++;
																		$providers1 = DB::table('providers')->where('provider_id',$con->provider_id)->first(); 
																		if(!empty($providers1)){
																			$i++;
																			if($prev == $con->provider_id ){
																				continue;
																			}
																			?>

																			<div class="col-sm-1 col-xs-2" id="setproviders">
																				<a href="{{KhojNewController::prepareurl($con->web_url)}}" target="blank">
																					<img src="{{asset('/asset'.$providers1->icon_url)}}" alt="{{$providers1->title ?? ''}}" class="app-img">
																					<p>@if ($con->retail_price > 0) {{$con->retail_price}}@elseif($con->monetization_type == 'flatrate' || $con->monetization_type == 'Flatrate'){{'Subs'}}@else{{ucfirst($con->monetization_type)}}@endif <span style="color: yellow"> {{strtoupper($con->video_quality)}} </span>
																					</p>
																				</a>
																			</div>
																			<?php 
																			$qlty = $con->video_quality;
																			$prev = $con->provider_id;

																		} ?>
																		@endforeach

																	</td>
																</tr>
																@endif
															</tbody>
														</table> 

														@if($countpro == 0)
														<p style="font-size: 16px; padding-left: 20px">No source available right now to watch this.</p>
														@endif

													</div>
												</div>
											</div>
										</div>

										@if($movie->content_type  == 'show')
										<!-- For Seasons -->
										<div class="row ipad-width2 trailors" style="margin-top: 35px">
											<div class="col-md-12 col-sm-12 col-xs-12">
												<h4>Seasons</h4>
												<div class="row" style="margin-top: 10px">
													<div class="slick-multiItem">
														<?php $count_m = 0; ?>
														@if(!empty($seasons))
														@foreach($seasons as $season)
														<?php
														$release_date="";
														if(!empty($season->cinema_release_date))
															$release_date = explode('-',$season->cinema_release_date);
														$count_m=1;
														?>
														<?php $urlen = preg_replace('/[^a-zA-Z0-9_.]/', '-', (strtolower($season->title))).'-'.$season->content_id; ?>
														<div class="slide-it" id="season-slide">
															<!-- <a onclick="showEpisodes({{$season->content_id}})"> -->
																<a href="/detail/{{$season->content_type}}/{{$urlen}}" style="padding-bottom: 30px">
																	<div class="movie-item">
																		<div class="mv-img">
																			@if(!empty($season->poster) && strpos($season->poster, 'poster/') == false && file_exists(public_path().'/poster/'.$season->poster))
																			<img src="/poster/{{$season->poster}}" alt="{{$season->title ?? ''}}" alt="" class="web-image">
																			@else
																			<img src="/images/default.png" alt="{{$season->title ?? ''}}" class="web-image">
																			@endif
																		</div>
																		<div class="hvr-inner">
																			{{-- <i class="fa fa-play" aria-hidden="true"></i>  --}}
																		</div>
																		<div class="title-in">

																			<p>
																				IMDB: <span style="color: #fff; font-size: 12px;">@if(isset($season->imdb_score)){{$season->imdb_score}}@else{{'N/A'}}@endif</span>
																			</p>
																		</div>


																	</div>
																</a>
																<div class="imdb" style="bottom: -30px">
																	<div class="row" style="text-align: left; width: 90%; margin: 0 5%">
																		<?php $urlen = preg_replace('/[^a-zA-Z0-9_.]/', '-', (strtolower($season->title))).'-'.$season->content_id; ?>
																		<span><a href="/detail/{{$season->content_type}}/{{$urlen}}" class="season-content"> 
																			<?php
																			if(isset($season->title))
																				echo substr(explode(':',$season->title)[0],0,18); 
																			elseif(isset($season->original_title))
																				echo substr($season->original_title,0,25); 
																			if(isset($parent_title) && isset($parent_title->title))
																				echo " (".substr($parent_title->title,0,25).") ";
																			elseif(isset($parent_title) && isset($parent_title->original_title))
																				echo " (".substr($parent_title->original_title,0,25).") ";
																			?></a></span>

																		</div>
																		<div class="row rate-row" style="font-size: 12px; color:#9ba09b!important;text-transform: uppercase; ">
																			<?php if ($season->original_language == 'hi') $season->original_language = "Hindi";?>
																			<?php if ($season->original_language == 'en') $season->original_language = "English";?>
																			<?php if ($season->original_language == 'te') $season->original_language = "Telugu";?>
																			<?php if ($season->original_language == 'bn') $season->original_language = "Bengali";?>
																			<?php if ($season->original_language == 'kn') $season->original_language = "Kannada";?>
																			<?php if ($season->original_language == 'es') $season->original_language = "Spanish";?>
																			<?php if ($season->original_language == 'ru') $season->original_language = "Russian";?>
																			<?php if ($season->original_language == 'gu') $season->original_language = "Gujrati";?>
																			<?php if ($season->original_language == 'ml') $season->original_language = "Malyalam";?>
																			<?php if ($season->original_language == 'pa') $season->original_language = "Punjabi";?>

																			<span class="rate-content"> {{ucfirst($season->content_type)?? ''}} @if(!empty($season->original_language))<span class="dot"></span>&nbsp;&nbsp;@endif
																			{{ $season->original_language ?? ''}}
																			@if(isset($season->age_certification))<span class="dot"></span> &nbsp;&nbsp;&nbsp;{{$season->age_certification}}@else<span class="dot"></span> &nbsp;&nbsp;&nbsp;{{'U/A'}}@endif</span>



																		</div>
																	</div>
																</a>
															</div>
															@endforeach
															@endif
														</div>
													</div>
													@if($count_m == 0)
													<div style="text-align: left;margin: 20px; color: #ffffff">No Seasons Available</div>
													@endif
												</div>
											</div>
											@endif



											@php $count_m = $episodestotal; @endphp
											@if($movie->content_type == 'tv-season' && count($episodes)>0)
											<div class="row ipad-width2 trailors" style="margin-top: 45px">
												<div class="col-md-12 col-sm-12 col-xs-12">
													<h4>New Episodes Of {{$season_one}}<br><br></h4>
													<div class="row all-episodes">

														@foreach($episodes as $episode)
														<div class="col-sm-12" style="border-bottom: 1px solid rgba(50,50,50,0.1); padding: 15px 0 5px">
															<div class="row" style=" cursor: pointer;" onclick="showEpisodes({{$episode->content_id}})">
																<div class="col-sm-11 col-xs-11">
																	<h5 style="font-size: 15px; font-weight: normal;">
																		{{$season_one}} 
																		<span style="color: #9ba09b">
																			@if (!empty($episode->episode_number))-{{'E'.$episode->episode_number}}@else {{ "E".$count_m}}@endif 
																			@if(!empty($episode->original_title))
																			-{{$episode->original_title}}
																			@elseif(!empty($episode->title))
																			-{{$episode->title}}
																			@endif
																		</span>
																	</h5>
																</div>
																<div class="col-xs-1 col-sm-1" style="text-align: right;">
																	<i class="fa fa-angle-down" style="font-size: 20px"></i>&nbsp;&nbsp;&nbsp;
																</div>
															</div>
														</div>
														@php $count_m-- @endphp
														<div class="col-sm-12" id="episode{{$episode->content_id}}" style="display: none; border: 1px solid #fff; border-top: 1px solid rgba(50,50,50,0.1); border-radius: 5px">
															@if(!empty($episode->short_description ))
															<div class="content" style="padding: 2px 10px">
																<p style="font-size: 14px">{{$episode->short_description ?? ''}}</p>
															</div>
															@endif

															<div class="row ipad-width2 app-icons" style="margin-top: 5px; padding:  10px">
																<div class="col-md-12 col-sm-12 col-xs-12" >
																	<h4>
																	Watch Now</h4>
																	<div class="app-get">
																		<div class="row">
																			<?php
																			$flatrate = array ( 'cinema','flatrate','free','ads');
																			$streams = DB::table('content_providers')
																			->where('content_id',$episode->content_id)
																			->whereIn('monetization_type',$flatrate)
																			->select('content_id','provider_id','video_quality','monetization_type','retail_price','currency','web_url')
																			->orderBy('provider_id', 'DESC')
																			->distinct('provider_id')
																			->get();

                      // $providers->stream = $streams;
																			$buy = array ( 'cinema','buy','rent');

																			$buys = DB::table('content_providers')
																			->where('content_id',$episode->content_id)
																			->select('content_id','provider_id','video_quality','monetization_type','retail_price','currency','web_url')
																			->where('retail_price','>',0)
																			->whereIn('monetization_type',$buy)
																			->orderBy('provider_id', 'DESC')
																			->distinct('provider_id')
																			->get();

																			?>

																			<table class="table table-bordered">
																				<tbody>
																					@php $countpro = 0 @endphp
																					@if(!empty($streams) && count($streams)>0)
																					<?php $i = 0; $prev = ""; $qlty = ""; $url = [];?>

																					<tr>
																						<th style="width: 20px" id="movie-providers"><h4>Stream<br><br></h4></th>
																						<td colspan="14">

																							@foreach($streams as $con)

																							<?php
																							$countpro++;
																							$providers1 = DB::table('providers')->where('provider_id',$con->provider_id)->first(); 
																							if(!empty($providers1)){

																								if ($con->retail_price <= 0 || $con->monetization_type == 'flatrate' || $con->monetization_type == 'Flatrate' ||  $con->monetization_type == 'free'){ 

																									?>
																									@if ($con->retail_price < 1 && $i == 0)
																									<?php $i = 1;  ?>

																									@endif
																									<?php 
																									if (in_array($con->web_url, $url)){continue;}
																									$url[] = $con->web_url;


																									?>
																									<?php  ?>
																									<div class="col-sm-1 col-xs-3" id="setproviders">


																										@if($providers1->title=='YouTube')

																										@if(!empty($con->web_url))
																										<?php

																										$url = explode('https://www.youtube.com/watch?v=',$con->web_url ?? '');
																										if (isset($url[1])) $url2 = explode('&',$url[1]);
																										$url1 = $url2[0] ?? '';
																										?>
																										<a data-toggle="modal" data-target="#exampleModal"  onclick="showVideo('{{ $url1 ?? '' }}')">


																											@else

																											<a href="{{KhojNewController::prepareurl($con->web_url ?? '')}}" target="blank">
																												@endif

																												@else

																												<a href="{{KhojNewController::prepareurl($con->web_url)}}" target="blank">

																													@endif

																													<img src="{{asset('/asset'.$providers1->icon_url)}}" alt="{{$provider1->title ?? ''}}" class="app-img">
																													<p>@if ($con->retail_price > 0) {{$con->retail_price}}@elseif($con->monetization_type == 'flatrate' || $con->monetization_type == 'Flatrate'){{'Subs'}}@else{{ucfirst($con->monetization_type)}}@endif <span style="color: yellow"> {{strtoupper($con->video_quality)}} </span>
																													</p>
																												</a>
																											</div>
																											<?php 
																											$qlty = $con->video_quality;
																											$prev = $con->provider_id;
																										}
																									} ?>
																									@endforeach

																								</td>
																							</tr>
																							@endif


																							@if(!empty($buys) && count($buys)>0)
																							<tr>
																								<th id="movie-providers"><h4>Buy/Rent<br><br></h4></th>
																								<td colspan="14">

																									<?php $i = 0; $prev = ""; $qlty = "";?>
																									<?php if (in_array($con->web_url, $url)){continue;} ?>
																									<?php 

																									$url[] = $con->web_url;


																									?>
																									@foreach($buys as $con)
																									<?php
																									$countpro++;
																									$providers1 = DB::table('providers')->where('provider_id',$con->provider_id)->first(); 
																									if(!empty($providers1)){
																										$i++;
																										if($prev == $con->provider_id ){
																											continue;
																										}
																										?>

																										<div class="col-sm-1 col-xs-3" id="setproviders">
																											<a href="{{KhojNewController::prepareurl($con->web_url)}}" target="blank">
																												<img src="{{asset('/asset'.$providers1->icon_url)}}" alt="{{$provider1->title ?? ''}}" class="app-img">
																												<p>@if ($con->retail_price > 0) {{$con->retail_price}}@elseif($con->monetization_type == 'flatrate' || $con->monetization_type == 'Flatrate'){{'Subs'}}@else{{ucfirst($con->monetization_type)}}@endif <span style="color: yellow"> {{strtoupper($con->video_quality)}} </span>
																												</p>
																											</a>
																										</div>
																										<?php 
																										$qlty = $con->video_quality;
																										$prev = $con->provider_id;

																									} ?>
																									@endforeach

																								</td>
																							</tr>
																							@endif
																						</tbody>
																					</table> 

																					@if($countpro == 0)
																					<p>No source available right now to watch this.</p>
																					@endif

																				</div>
																			</div>
																		</div>
																	</div>

																</div>
																@endforeach


																@if($episodestotal>5)
																<div style="text-align: center;" id="downarrow1" onclick="showAllEpisodes({{$movie->content_id}})">
																	<i class="fa fa-angle-double-down"></i>
																</div>

																<div style="text-align: center;" id="uparrow1" onclick="showSelectedEpisodes({{$movie->content_id}})">
																	<i class="fa fa-angle-double-up"></i>
																</div>
																@endif

																@if($episodestotal == 0)
																<div style="text-align: left;margin: 20px; color: #ffffff">No Episode Available</div>
																@endif
															</div>
														</div>
													</div>
													@endif



													@if($movie->content_type == 'show' && count($episodes)>0)
													<div class="row ipad-width2 trailors" style="margin-top: 45px">
														<div class="col-md-12 col-sm-12 col-xs-12">
															<h4>New Episodes Of {{$season_one}}<br><br></h4>
															<div class="row all-episodes">
																@php $count_m = $episodestotal; @endphp

																@foreach($episodes as $episode)
																<div class="col-sm-12" style="border-bottom: 1px solid rgba(50,50,50,0.1); padding: 15px 0 5px">
																	<div class="row" style=" cursor: pointer;" onclick="showEpisodes({{$episode->content_id}})">
																		<div class="col-sm-11 col-xs-11">
																			<h5 style="font-size: 15px; font-weight: normal;">
																				{{$season_one}} 
																				<span style="color: #9ba09b">
																					@if (!empty($episode->episode_number))-{{'E'.$episode->episode_number}}@else {{" E".$count_m}}@endif 
																					@if(!empty($episode->original_title))
																					-{{$episode->original_title}}
																					@elseif(!empty($episode->title))
																					-{{$episode->title}}
																					@endif
																				</span>
																			</h5>
																		</div>
																		@php $count_m-- @endphp
																		<div class="col-xs-1 col-sm-1" style="text-align: right;">
																			<i class="fa fa-angle-down" style="font-size: 20px"></i>&nbsp;&nbsp;&nbsp;
																		</div>
																	</div>
																</div>
																<div class="col-sm-12" id="episode{{$episode->content_id}}" style="display: none; border: 1px solid #fff; border-top: 1px solid rgba(50,50,50,0.1); border-radius: 5px">
																	@if(!empty($episode->short_description ))
																	<div class="content" style="padding: 2px 10px">
																		<p style="font-size: 14px">{{$episode->short_description ?? ''}}</p>
																	</div>
																	@endif

																	<div class="row ipad-width2 app-icons" style="margin-top: 5px; padding:  10px">
																		<div class="col-md-12 col-sm-12 col-xs-12" >
																			<h4>
																			Watch Now</h4>
																			<div class="app-get">
																				<div class="row">
																					<?php
																					$flatrate = array ( 'cinema','flatrate','free','ads');
																					$streams = DB::table('content_providers')
																					->where('content_id',$episode->content_id)
																					->whereIn('monetization_type',$flatrate)
																					->select('content_id','provider_id','video_quality','monetization_type','retail_price','currency','web_url')
																					->orderBy('provider_id', 'DESC')
																					->distinct('provider_id')
																					->get();

                      // $providers->stream = $streams;
																					$buy = array ( 'cinema','buy','rent');

																					$buys = DB::table('content_providers')
																					->where('content_id',$episode->content_id)
																					->select('content_id','provider_id','video_quality','monetization_type','retail_price','currency','web_url')
																					->where('retail_price','>',0)
																					->whereIn('monetization_type',$buy)
																					->orderBy('provider_id', 'DESC')
																					->distinct('provider_id')
																					->get();



																					?>

																					<table class="table table-bordered">
																						<tbody>
																							@php $countpro = 0 @endphp
																							@if(!empty($streams) && count($streams)>0)

																							<tr>
																								<th style="width: 20px" id="movie-providers"><h4>Stream<br><br></h4></th>
																								<td colspan="14">
																									<?php $i = 0; $prev = ""; $qlty = ""; $url = [];?>
																									@foreach($streams as $con)
																									<?php if (in_array($con->web_url, $url)){continue;} ?>
																									<?php 

																									$url[] = $con->web_url;


																									?>
																									<?php
																									$countpro++;
																									$providers1 = DB::table('providers')->where('provider_id',$con->provider_id)->first(); 
																									if(!empty($providers1)){

																										if ($con->retail_price <= 0 || $con->monetization_type == 'flatrate' || $con->monetization_type == 'Flatrate' ||  $con->monetization_type == 'free'){ 
																											if($prev == $con->provider_id ){
																												continue;
																											}
																											?>
																											@if ($con->retail_price < 1 && $i == 0)
																											<?php $i = 1; ?>

																											@endif
																											<div class="col-sm-1 col-xs-2" id="setproviders">



																												@if($providers1->title=='YouTube')
																												@if(!empty($con->web_url))

																												<?php

																												$url = explode('https://www.youtube.com/watch?v=',$con->web_url ?? '');
																												if (isset($url[1])) $url2 = explode('&',$url[1]);
																												$url1 = $url2[0] ?? '';
																												?>
																												<a data-toggle="modal" data-target="#exampleModal"  onclick="showVideo('{{ $url1 ?? '' }}')">

																													@else

																													<a href="{{KhojNewController::prepareurl($con->web_url ?? '')}}" target="blank">

																														@endif

																														@else

																														<a href="{{KhojNewController::prepareurl($con->web_url)}}" target="blank">

																															@endif



																															<img src="{{asset('/asset'.$providers1->icon_url)}}" alt="{{$provider1->title ?? ''}}" class="app-img">
																															<p>@if ($con->retail_price > 0) {{$con->retail_price}}@elseif($con->monetization_type == 'flatrate' || $con->monetization_type == 'Flatrate'){{'Subs'}}@else{{ucfirst($con->monetization_type)}}@endif <span style="color: yellow"> {{strtoupper($con->video_quality)}} </span>
																															</p>
																														</a>
																													</div>
																													<?php 
																													$qlty = $con->video_quality;
																													$prev = $con->provider_id;
																												}
																											} ?>
																											@endforeach

																										</td>
																									</tr>
																									@endif


																									@if(!empty($buys) && count($buys)>0)
																									<?php if (in_array($con->web_url, $url)){continue;} ?>
																									<?php 

																									$url[] = $con->web_url;


																									?>
																									<tr>
																										<th id="movie-providers"><h4>Buy/Rent<br><br></h4></th>
																										<td colspan="14">

																											<?php $i = 0; $prev = ""; $qlty = "";?>

																											@foreach($buys as $con)
																											<?php
																											$countpro++;
																											$providers1 = DB::table('providers')->where('provider_id',$con->provider_id)->first(); 
																											if(!empty($providers1)){
																												$i++;
																												if($prev == $con->provider_id ){
																													continue;
																												}
																												?>

																												<div class="col-sm-1 col-xs-1" id="setproviders">
																													<a href="{{KhojNewController::prepareurl($con->web_url)}}" target="blank">
																														<img src="{{asset('/asset'.$providers1->icon_url)}}" alt="{{$provider1->title ?? ''}}" class="app-img">
																														<p>@if ($con->retail_price > 0) {{$con->retail_price}}@elseif($con->monetization_type == 'flatrate' || $con->monetization_type == 'Flatrate'){{'Subs'}}@else{{ucfirst($con->monetization_type)}}@endif <span style="color: yellow"> {{strtoupper($con->video_quality)}} </span>
																														</p>
																													</a>
																												</div>
																												<?php 
																												$qlty = $con->video_quality;
																												$prev = $con->provider_id;

																											} ?>
																											@endforeach

																										</td>
																									</tr>
																									@endif
																								</tbody>
																							</table> 

																							@if($countpro == 0)
																							<p>No source available right now to watch this.</p>
																							@endif

																						</div>
																					</div>
																				</div>
																			</div>

																		</div>
																		@endforeach

																		@if($episodestotal>5)
																		<div style="text-align: center;" id="downarrow2" onclick="showAllEpisodes({{$movie->content_id}})">
																			<i class="fa fa-angle-double-down"></i>
																		</div>

																		<div style="text-align: center;" id="uparrow2" onclick="showSelectedEpisodes({{$movie->content_id}})">
																			<i class="fa fa-angle-double-up"></i>
																		</div>
																		@endif

																		@if($episodestotal == 0)
																		<div style="text-align: left;margin: 20px; color: #ffffff">No Episode Available</div>
																		@endif
																	</div>
																</div>
															</div>
															@endif





															<?php $videoc = 0; ?>
															@if(!empty($clips))
															<div class="row ipad-width2 trailors" style="margin-top: 45px">
																<div class="col-md-12 col-sm-12 col-xs-12">
																	<h4>
																		<br><br>
																		VIDEOS: TRAILERS, TEASERS, FEATURETTES<br><br>
																	</h4>
																	<div class="row">
																		@foreach($clips as $clip)

																		@if($clip->external_id != '' )
																		<?php $videoc++; ?>
																		<div class="col-sm-6"><iframe src="https://www.youtube.com/embed/@if(!empty($clip->external_id)){{$clip->external_id}}@endif" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen style="border: 5px solid rgba(20,20,20,0.5); width: 100%; height: auto"></iframe></div>
																		@endif
																		@endforeach
																	</div>
																	@if($videoc == 0)
																	<div style="text-align: left;margin: 20px; color: #ffffff">No Video Available</div>
																	@endif
																</div>
															</div>
															@endif








															<div class="row ipad-width2 trailors" style="margin-top: 35px">
																<div class="col-md-12 col-sm-12 col-xs-12">
																	<h4>People also liked</h4>
																	<div class="row" style="margin-top: 20px">
																		<div class="slick-multiItem">
																			<?php $count_m = 0; ?>
																			@if(!empty($related_movies))
																			@foreach($related_movies as $movie)
																			<?php
																			$release_date="";
																			if(!empty($movie->cinema_release_date))
																				$release_date = explode('-',$movie->cinema_release_date);
																			$count_m=1;
																			?>
																			<?php $urlen = preg_replace('/[^a-zA-Z0-9_.]/', '-', (strtolower($movie->title))).'-'.$movie->content_id; ?>
																			<div class="slide-it search-response2" >
																				<!-- <a onclick="showEpisodes({{$movie->content_id}})"> -->

																					<a  href="/detail/{{$movie->content_type}}/{{$urlen}}">



																						<div class="movie-item">
																							<div class="mv-img">
																								@if(!empty($movie->poster) && strpos($movie->poster, 'poster/') == false && file_exists(public_path().'/poster/'.$movie->poster))
																								<img src="/poster/{{$movie->poster}}" alt="{{$movie->title ?? ''}}" class="web-image">
																								@else
																								<img src="/images/default.png" alt="{{$movie->title ?? ''}}" class="web-image">
																								@endif
																							</div>
																							<div class="hvr-inner">
																								{{-- <i class="fa fa-play" aria-hidden="true"></i>  --}}
																							</div>
																							<div class="title-in">

																								<p style="margin-left: 7%">
																									IMDB: <span style="color: #fff; font-size: 12px;">@if(isset($movie->imdb_score)){{$movie->imdb_score}}@else{{'N/A'}}@endif</span>
																								</p>
																							</div>
																							<div class="imdb" id="people-content">
																								<div class="row" style="text-align: left;">
																									<?php $urlen = preg_replace('/[^a-zA-Z0-9_.]/', '-', (strtolower($movie->title))).'-'.$movie->content_id; ?>
																									<span><a href="/detail/{{$movie->content_type}}/{{$urlen}}" style="font-size: 15px; color : #fff; padding: 0 7%"> 
																										<?php
																										if(isset($movie->title))
																											echo substr(explode(':',$movie->title)[0],0,18); 
																										elseif(isset($movie->original_title))
																											echo substr($movie->original_title,0,25); 
																										if(isset($parent_title) && isset($parent_title->title))
																											echo " (".substr($parent_title->title,0,25).") ";
																										elseif(isset($parent_title) && isset($parent_title->original_title))
																											echo " (".substr($parent_title->original_title,0,25).") ";
																										?></a></span>

																									</div>
																									<div class="row rate-row" style="font-size: 12px; color:#9ba09b!important;text-transform: uppercase; ">
																										<?php if ($movie->original_language == 'hi') $movie->original_language = "Hindi";?>
																										<?php if ($movie->original_language == 'en') $movie->original_language = "English";?>
																										<?php if ($movie->original_language == 'te') $movie->original_language = "Telugu";?>
																										<?php if ($movie->original_language == 'bn') $movie->original_language = "Bengali";?>
																										<?php if ($movie->original_language == 'kn') $movie->original_language = "Kannada";?>
																										<?php if ($movie->original_language == 'es') $movie->original_language = "Spanish";?>
																										<?php if ($movie->original_language == 'ru') $movie->original_language = "Russian";?>
																										<?php if ($movie->original_language == 'gu') $movie->original_language = "Gujrati";?>
																										<?php if ($movie->original_language == 'ml') $movie->original_language = "Malyalam";?>
																										<?php if ($movie->original_language == 'pa') $movie->original_language = "Punjabi";?>

																										<span style="padding: 0 7%"> {{ucfirst($movie->content_type)?? ''}} @if(!empty($movie->original_language))<span class="dot"></span>&nbsp;&nbsp;@endif
																										{{ $movie->original_language ?? ''}}
																										@if(isset($movie->age_certification))<span class="dot"></span> &nbsp;&nbsp;&nbsp;{{$movie->age_certification}}@else<span class="dot"></span> &nbsp;&nbsp;&nbsp;{{'U/A'}}@endif</span>



																									</div>
																								</div>
																							</div>
																						</a>
																					</div>
																					@endforeach
																					@endif
																				</div>
																			</div>
																			@if($count_m == 0)
																			<div style="text-align: left;margin: 20px; color: #ffffff">No Show / Movie Available</div>
																			@endif
																		</div>
																	</div>










																</div>
															</div>
														</div>
													</div>
												</div>
												<!-- Related Movies & Shows -->
											</div>
										</div>
									</div>



									<!-- Modal -->
									<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
										<div class="modal-dialog" role="document">
											<div class="modal-content">
												<div class="modal-header">
													<h5 class="modal-title" id="exampleModalLabel"></h5>
													<button type="button" class="close" data-dismiss="modal" aria-label="Close">
														<span aria-hidden="true">&times;</span>
													</button>
												</div>
												<div class="modal-body">
													<iframe width="100%" height="350px" id="freeVideo" src="" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
												</div>

											</div>
										</div>
									</div>

									@endsection

									@section('script')


									<script type="text/javascript">

										function showAllEpisodes(id){

											var CSRF_Token = $('meta[name="csrf-token"]').attr('content'); 

											$.ajax({
												type: "POST",
												url: "/show-episodes",
												data:{ _token: CSRF_Token, id: id, type: 'all' },
												success:function(msg){
													$('.all-episodes').empty();
													$('.all-episodes').html(msg);
													$('#downarrow1').hide();
													$('#downarrow2').hide();
													$('#uparrow1').show();
													$('#uparrow2').show();
												}
											});
										}

										function showSelectedEpisodes(id){

											var CSRF_Token = $('meta[name="csrf-token"]').attr('content'); 

											$.ajax({
												type: "POST",
												url: "/show-episodes",
												data:{ _token: CSRF_Token, id: id, type: '5' },
												success:function(msg){
													$('.all-episodes').empty();
													$('.all-episodes').html(msg);
													$('#uparrow1').hide();
													$('#uparrow2').hide();
													$('#downarrow1').show();
													$('#downarrow2').show();

												}
											});
										}


										function getSynopsys(){
											$('.synopsys-list').toggle();
										}

										function getCast(){
   	// $('.cast-list').toggle();
   }
   
   function getCrew(){
   	$('.crew-list').toggle();
   }
   
   function getReviews(){
   	$('.reviews-list').toggle();
   }
   
   function watchStream(id){
   	$('#show_stream'+id).toggle();
   }
   
   
   function showEpisodes(id){
   	$('#episode'+id).toggle();

   }


   function showVideo(videolink){
   	var videoLink = 'https://www.youtube.com/embed/'+videolink;

   	$('#freeVideo').attr('src',videoLink)
   }



</script>







@endsection