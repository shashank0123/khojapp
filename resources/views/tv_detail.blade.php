@extends('layouts.user_layout')

@section('content')

<div id="content-sidebar-pro">
			
			<div id="content-sidebar-image">
				<img src="https://image.tmdb.org/t/p/w300{{$contents->poster_path}}" alt="Movie Poster">
			</div>
			
			<div class="content-sidebar-section">
				<h2 class="content-sidebar-sub-header">True Blood</h2>
				<ul class="progression-studios-slider-rating">
					<li>PG-13</li><li>HD</li>
				</ul>
			</div><!-- close .content-sidebar-section -->
			
			<div class="content-sidebar-section">
				<h4 class="content-sidebar-sub-header">First Air Date</h4>
				<div class="content-sidebar-short-description">{{Carbon\Carbon::parse($contents->first_air_date)->format('d M Y')}} (USA)</div>
			</div><!-- close .content-sidebar-section -->
			
			<div class="content-sidebar-section">
				<h4 class="content-sidebar-sub-header">Length</h4>
				<div class="content-sidebar-short-description">{{$contents->episode_run_time[0]}} mins</div>
			</div><!-- close .content-sidebar-section -->
			
			<div class="content-sidebar-section">
				<h4 class="content-sidebar-sub-header">Director</h4>
				@foreach ($directors as $director)
					<div class="content-sidebar-short-description">{{$director}}</div>
					@endforeach
			</div><!-- close .content-sidebar-section -->

			<div class="content-sidebar-section">
				<h4 class="content-sidebar-sub-header">Writers</h4>
				@foreach ($writers as $writer)
					<div class="content-sidebar-short-description">{{$writer}}</div>
				@endforeach
			</div><!-- close .content-sidebar-section -->

			
			
			<div class="content-sidebar-section">
				<h2 class="content-sidebar-sub-header adjusted-recent-reviews">Recent Reviews</h2>
				<ul id="sidebar-reviews-pro">
					@foreach ($reviewslist as $review)
					<li>
				      <div
				        class="circle-rating-pro"
				        data-value="0.86"
				        data-animation-start-value="0.86"
				        data-size="32"
				        data-thickness="3"
				        data-fill="{
				          &quot;color&quot;: &quot;#42b740&quot;
				        }"
				        data-empty-fill="#def6de"
				        data-reverse="true"
				      ><span style="color:#42b740;">8.6</span></div>
						<h6>{{ $review->author}}</h6>
						<div class="sidebar-review-time">October 22, 2017</div>
						<div class="spoiler-review">Contains Spoiler</div>
						<p>{{ (strlen($review->content) > 112) ? substr($review->content,0,112).'...' : $review->content}}</p>
					</li>

					
					@endforeach				
				</ul>
				<a href="#!" class="btn btn-green-pro btn-sm">See All Reviews</a>
			</div><!-- close .content-sidebar-section -->
			
		</div><!-- close #content-sidebar-pro -->
<main id="col-main-with-sidebar">
			
			<div id="movie-detail-header-pro" style="background-image:url('/final/images/demo/dashboard-movie-poster.jpg')">
				
				<div class="progression-studios-slider-more-options">
					<i class="fas fa-ellipsis-h"></i>
					<ul>
						<li><a href="#!">Add to Favorites</a></li>
						<li><a href="#!">Add to Watchlist</a></li>
						<li><a href="#!">Add to Playlist</a></li>
						<li><a href="#!">Share...</a></li>
						<li><a href="#!">Write A Review</a></li>
					</ul>
				</div>
				
				<a class="movie-detail-header-play-btn afterglow" href="#VideoLightbox-1"><i class="fas fa-play"></i></a>
				
	         <video id="VideoLightbox-1"  poster="../../cdn.plyr.io/static/demo/View_From_A_Blue_Moon_Trailer-HD6654.jpg?v1" width="960" height="540">
	             <source src="https://cdn.plyr.io/static/demo/View_From_A_Blue_Moon_Trailer-HD.mp4" type="video/mp4">
	             <source src="https://cdn.plyr.io/static/demo/View_From_A_Blue_Moon_Trailer-HD.webm" type="video/webm">
	         </video>
				
				<div id="movie-detail-header-media">
					<div class="dashboard-container">
						<h5>Media</h5>						
						<div class="row">
							<div class="col-6 col-md-4 col-lg-4">
								<a class="movie-detail-media-link afterglow" href="#VideoLightbox-1">
									<div class="movie-detail-media-image">
										<img src="/final/images/demo/holly-mandarich-236721-unsplash.jpg">
										<span><i class="fas fa-play"></i></span>
										<h6>Trailer</h6>
									</div>
								</a>
							</div>
							<div class="col-6 col-md-4 col-lg-4">
								<a class="movie-detail-media-link afterglow" href="#VideoLightbox-1">
									<div class="movie-detail-media-image">
										<img src="/final/images/demo/jonathan-pendleton-6555-unsplash.jpg">
										<span><i class="fas fa-play"></i></span>
										<h6>Interview</h6>
									</div>
								</a>
							</div>
							<div class="col-6 col-md-4 col-lg-4">
								<a class="movie-detail-media-link" href="#!">
									<div class="movie-detail-media-image">
										<img src="/final/images/demo/zachary-shea-577599-unsplash.jpg">
										<span><i class="fas fa-play"></i></span>
										<h6>Movie Stills</h6>
									</div>
								</a>
							</div>
						</div><!-- close .row -->
					</div><!-- close .dashboard-container -->
				</div><!-- close #movie-detail-header-media -->
				
				<div id="movie-detail-gradient-pro"></div>
			</div><!-- close #movie-detail-header-pro -->
			
			
			<div id="movie-detail-rating">
				<div class="dashboard-container">
					<div class="row">
						<div class="col-sm">
							<h5>Rate True Blood</h5>
							
							<div class="rating-pro">
   							  <label>
   							    <input type="radio" name="rating-pro" value="10" title="10 stars"> 10
   							  </label>
  							  <label>
  							    <input type="radio" name="rating-pro" value="9" title="9 stars"> 9
  							  </label>
  							  <label>
  							    <input type="radio" name="rating-pro" value="8" title="8 stars"> 8
  							  </label>
  							  <label>
  							    <input type="radio" name="rating-pro" value="7" title="7 stars"> 7
  							  </label>
 							  <label>
 							    <input type="radio" name="rating-pro" value="6" title="6 stars"> 6
 							  </label>
							  <label>
							    <input type="radio" name="rating-pro" value="5" title="5 stars"> 5
							  </label>
							  <label>
							    <input type="radio" name="rating-pro" value="4" title="4 stars"> 4
							  </label>
							  <label>
							    <input type="radio" name="rating-pro" value="3" title="3 stars"> 3
							  </label>
							  <label>
							    <input type="radio" name="rating-pro" value="2" title="2 stars"> 2
							  </label>
							  <label>
							    <input type="radio" name="rating-pro" value="1" title="1 star"> 1
							  </label>
							</div>
							
						</div>
						<div class="col-sm">
							<h6>User Rating</h6>
					      <div
					        class="circle-rating-pro"
					        data-value="{{$contents->vote_average/10}}"
					        data-animation-start-value="{{$contents->vote_average/10}}"
					        data-size="40"
					        data-thickness="3"
					        data-fill="{
					          &quot;color&quot;: &quot;#42b740&quot;
					        }"
					        data-empty-fill="#def6de"
					        data-reverse="true"
					      ><span style="color:#42b740;">{{$contents->vote_average}}</span></div>
							<div class="clearfix"></div>
						</div>
					</div><!-- close .row -->
				</div><!-- close .dashboard-container -->
			</div><!-- close #movie-detail-rating -->
			
			<div class="dashboard-container">
				
				
				<div class="movie-details-section">
					<h2>Storyline</h2>
					<p>{{$contents->overview}}</p>
				</div><!-- close .movie-details-section -->

				<div class="movie-details-section">
					<h2>The Cast</h2>
					<div class="row">
						
						@foreach ($casts as $cast)

						<div class="col-12 col-md-6 col-lg-6 col-xl-4">
							<div class="item-listing-container-skrn">
								<a href="#!"><img src="https://image.tmdb.org/t/p/w300{{$cast->profile_path}}" alt="Cast"></a>
								<div class="item-listing-text-skrn item-listing-movie-casting">
									<h6><a href="#!">{{$cast->name}}</a></h6>
									<div class="movie-casting-sub-title">{{$cast->character}}</div>
								</div><!-- close .item-listing-text-skrn -->
							</div><!-- close .item-listing-container-skrn -->
						</div><!-- close .col -->						
					@endforeach
						
						
						
						
						
					</div><!-- close .row -->
				</div><!-- close .movie-details-section -->

					
				<div class="movie-details-section">
					<h2>Similar Movies</h2>
					<div class="row">
						<div class="col-12 col-md-6 col-lg-6 col-xl-4">
							<div class="item-listing-container-skrn">
								<a href="dashboard-movie-profile"><img src="/final/images/demo/listing-4.jpg" alt="Listing"></a>
								<div class="item-listing-text-skrn">
									<div class="item-listing-text-skrn-vertical-align"><h6><a href="dashboard-movie-profile">Bad Neighbors 2</a></h6>
								      <div
								        class="circle-rating-pro"
								        data-value="0.72"
								        data-animation-start-value="0.72"
								        data-size="32"
								        data-thickness="3"
								        data-fill="{
								          &quot;color&quot;: &quot;#42b740&quot;
								        }"
								        data-empty-fill="#def6de"
								        data-reverse="true"
								      ><span style="color:#42b740;">7.2</span></div>
									</div><!-- close .item-listing-text-skrn-vertical-align -->
								</div><!-- close .item-listing-text-skrn -->
							</div><!-- close .item-listing-container-skrn -->
						</div><!-- close .col -->
					
					
						<div class="col-12 col-md-6 col-lg-6 col-xl-4">
							<div class="item-listing-container-skrn">
								<a href="dashboard-movie-profile"><img src="/final/images/demo/listing-5.jpg" alt="Listing"></a>
								<div class="item-listing-text-skrn">
									<div class="item-listing-text-skrn-vertical-align"><h6><a href="dashboard-movie-profile">Star Wars: Rogue One</a></h6>
								      <div
								        class="circle-rating-pro"
								        data-value="0.86"
								        data-animation-start-value="0.86"
								        data-size="32"
								        data-thickness="3"
								        data-fill="{
								          &quot;color&quot;: &quot;#42b740&quot;
								        }"
								        data-empty-fill="#def6de"
								        data-reverse="true"
								      ><span style="color:#42b740;">8.6</span></div>
									</div><!-- close .item-listing-text-skrn-vertical-align -->
								</div><!-- close .item-listing-text-skrn -->
							</div><!-- close .item-listing-container-skrn -->
						</div><!-- close .col -->
					
						<div class="col-12 col-md-6 col-lg-6 col-xl-4">
							<div class="item-listing-container-skrn">
								<a href="dashboard-movie-profile"><img src="/final/images/demo/listing-6.jpg" alt="Listing"></a>
								<div class="item-listing-text-skrn">
									<div class="item-listing-text-skrn-vertical-align"><h6><a href="dashboard-movie-profile">The Imitation Game</a></h6>
								      <div
								        class="circle-rating-pro"
								        data-value="0.6"
								        data-animation-start-value="0.6"
								        data-size="32"
								        data-thickness="3"
								        data-fill="{
								          &quot;color&quot;: &quot;#ff4141&quot;
								        }"
								        data-empty-fill="#ffe1e1"
								        data-reverse="true"
								      ><span style="color:#ff4141;">6.0</span></div>
									</div><!-- close .item-listing-text-skrn-vertical-align -->
								</div><!-- close .item-listing-text-skrn -->
							</div><!-- close .item-listing-container-skrn -->
						</div><!-- close .col -->
					</div><!-- close .row -->
				
				</div><!-- close .movie-details-section -->
				
			</div><!-- close .dashboard-container -->
		</main>


@endsection

