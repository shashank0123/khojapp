<?php
use App\Seo;
use App\Genre;
use App\FeaturedImage;
use App\IpRequest;
$checkip = IpRequest::where('ipaddress', \Request::ip())->first();
if ($checkip){
  $checkip->url_visited = url()->current();
  $checkip->increment('count_request');
}
else
{
  $checkip = new IpRequest;
  $checkip->ipaddress = \Request::ip();
  $checkip->count_request = 1;
  $checkip->url_visited = url()->current();
  $checkip->save();
}

$page_no = 1;
$previous = 1;
$next = 2;

if(!empty($_GET['page'])){
  $page_no = $_GET['page'];
}

if($page_no>1){
  $previous = $page_no -1;
  $next = $page_no+1;  
}


$seo = Seo::first();


//variable declaration for default settings 
if (!isset($type)){
  $type = 'None';
}

if (!isset($miny)){
  $miny = '1500';
}

if (!isset($maxy)){
  $maxy = date('Y');
}

if (!isset($minr)){
  $minr = '0';
}

if (!isset($maxr)){
  $maxr = '10';
}






//$fp = fopen('/var/www/html/public/userip.html', 'a');//opens file in append mode  
//fwrite($fp, \Request::ip()." ".date('Y-m-d H:i:s')."<br>");  
//{{-- fwrite($fp, 'appending data');   --}}
//fclose($fp);  


$languages = \Cache::rememberForever('languages', function () {
                            return $contents = DB::table('languages')
                                                ->leftJoin('contents','contents.original_language','languages.code')
                                                ->where('contents.original_language',"!=",NULL)
                                                ->where('languages.status','Active')
                                                ->select('contents.original_language','languages.english_name')
                                                ->orderBy('languages.display_priority','ASC')
                                                ->distinct('contents.original_language')
                                                ->get();

                        });


$all_providers = DB::table('providers')->where('display_priority', '>', 0)->orderBy('display_priority','ASC')->get();

$all_genres = \Cache::rememberForever('all_genres', function () {
                            return $contents = Genre::select('genre_id','title')->distinct('genre_id')->get();

                        });
$flag = 0;

// $featureds = session()->get('featureds');
$homeside = FeaturedImage::where('status','Active')->where('page_title','homeside')->where('start_date','<=',date('Y-m-d'))->where('end_date','>=',date('Y-m-d'))->orderBy('display_priority', 'asc')->get();

$movielist = FeaturedImage::where('status','Active')->where('page_title','movielist')->where('start_date','<=',date('Y-m-d'))->where('end_date','>=',date('Y-m-d'))->orderBy('display_priority', 'asc')->get();
$featureds = FeaturedImage::where('status','Active')->where('page_title',$tab)->where('start_date','<=',date('Y-m-d'))->where('end_date','>=',date('Y-m-d'))->orderBy('display_priority', 'asc')->get();

if(count($featureds)<=0){
  $featureds = FeaturedImage::where('status','Active')->where('page_title','default')->where('start_date','<=',date('Y-m-d'))->where('end_date','>=',date('Y-m-d'))->orderBy('display_priority', 'asc')->get();
}



$minyr = \Cache::rememberForever('minyr', function () {
                            return $contents = DB::table('contents')->where('original_release_year','>','1900')->min('original_release_year');

                        });


$maxyr = \Cache::rememberForever('maxyr', function () {
                            return $contents = DB::table('contents')->max('original_release_year');

                        });
?>

@extends('layouts.khoj_new') 

@section('tags')
{{$seo->page_tags ?? 'khojaap online movie search, watch online '}}
@endsection

@section('title')
{{$seo->page_title ?? 'Khojapp - Stream online movies on khoj'}}
@endsection

@section('description')
{{$seo->meta_description ?? 'We show you where you can legally watch movies and TV shows that you love. You are kept up to date with what is new on Netflix, Amazon Prime, iTunes and many other streaming platforms.'}}
@endsection

@section('banner')

<style>
  .portfolio_slider {

  }
  .portfolio_slider {
   margin: 10px 0 15px;
   /*margin: 30px;*/
   max-width: 1600px;
   flex: 1 1 0;
   background-color: transparent;
 }

 .inner {
  padding: 6px;
}

.inner img {
  width: 100%;
  border-radius: 10px;
  height: auto;
}
img:hover {
  opacity: 0.9;
}
.featured_slider{
  padding : 0 !important;
}
.featured_slider .slider{
  width:100%;
  height: auto;
  max-height: 325px;
  margin:30px auto 0;
  /*margin:20px auto;*/
  text-align: center;
  padding:0px;
  color:white;
  .parent-slide{padding:15px;}
  img{display: block;margin:auto;}
}

@media screen and (max-width:580px){
  .featured_slider .slider{
    margin:95px auto 0;
    text-align: center;
  }
}

.featured_img {
 width: 99%; 
 height: 98%;
 margin: 0 0.5%; 
 border: 3px solid #aaa; 
}
.slick-list { 
 padding: 10px auto !important }

  .mySlides {display: none;}
img {vertical-align: middle;}

/* Slideshow container */
.slideshow-container {
  position: relative;
  margin: auto;
}

/* Caption text */
.text {
  color: #f2f2f2;
  font-size: 15px;
  padding: 8px 12px;
  position: absolute;
  bottom: 8px;
  width: 100%;
  text-align: center;
}

/* Number text (1/3 etc) */
.numbertext {
  color: #f2f2f2;
  font-size: 12px;
  padding: 8px 12px;
  position: absolute;
  top: 0;
}

/* The dots/bullets/indicators */
.dot {
  height: 7px;
  width: 7px;
  margin: 5px 0 0 0;
  background-color: #bbb;
  border-radius: 50%;
  display: inline-block;
  transition: background-color 0.6s ease;
}

.active {
  background-color: #717171;
}

/* Fading animation */
.fade {
  -webkit-animation-name: fade;
  -webkit-animation-duration: 1.5s;
  animation-name: fade;
  animation-duration: 1.5s;
}

@-webkit-keyframes fade {
  from {opacity: .4}
  to {opacity: 1}
}

@keyframes fade {
  from {opacity: .4}
  to {opacity: 1}
}

/* On smaller screens, decrease text size */
@media only screen and (max-width: 300px) {
  .text {font-size: 11px}
}
@media only screen and (max-width: 760px) {
  .featured_slider {margin-top: 16%}
}

</style>


  




<button onclick="showFilter()" class="btn btn-primary open-filter">Filters 
  {{-- <img scr="{{asset('/asset/images/filter.png')}}" style="width: 20px; height: 20px"> --}}
</button>
@endsection

@section('content')
<div class="container-fluid featured_slider"> 

<div class="slideshow-container">

                         @if(!empty($featureds))
                        @foreach($featureds as $feature)

                        <div class="mySlides fade"><a href="{{$feature->redirect_url ?? ''}}">
                            <img src="{{asset($feature->poster)}}" style="width:100%">
                            </a>
                        </div>

                        @endforeach
    @endif    


                    </div>

  
</div>

<input type="hidden" name="type" value="{{$type}}" id="type">
<input type="hidden" name="sort" value="@if(isset($sort)){{$sort}}@endif" id="sort_filter">
<input type="hidden" name="lang" value="@if(isset($langs)){{$langs}}@endif" id="lang_filter">
<input type="hidden" name="type" value="1" id="paginate">
<input type="hidden" name="genre_name" value="@if(isset($genre)){{$genre}}@endif" id="genre_name">
<input type="hidden" name="rated" value="@if(isset($rated)){{$rated}}@endif" id="rated">
<input type="hidden" name="keyword" value="@if(isset($keyword)){{$keyword}}@endif" id="keyword">
<input type="hidden" name="provider_id" id="provider_id" value="@if(isset($provider)){{$provider}}@endif">
<?php if  (isset($_GET['provider'])){
  $style1 = 'style="opacity:.3;"'; 
}else {
 $style1 = "";
}
?>

<div class="container-fluid" style="margin: 0 5%">
  <div class="gallery gallery-responsive portfolio_slider ">
    <?php $i = 1;$j = 1;?>
    @if(!empty($all_providers))
    @foreach($all_providers as $provider)
    <div class="inner" <?php if (isset($_GET['provider']) && $_GET['provider'] != $provider->technical_name.'_'.$provider->provider_id){ echo $style1; $j = 0;}?> onclick="getProvider({{$provider->provider_id}},'{{$provider->technical_name}}')"><img src="{{asset('asset'.$provider->icon_url)}}" title="{{$provider->title}}"></div>
    <?php $i += $j;?>
    @endforeach 
    @endif    
  </div>
</div>

<div class="col-sm-2 left-side-menu">
  <div class="left-main">
    <h4>Refine Your Search</h4> <br>
    <div class="close-filter">

      <!-- <button onclick="getReset2()" class="btn btn-default"><b><i class="fa fa-repeat"></i></b></button> -->
      <!-- <button onclick="getReset2()" class="btn btn-default"><b>RESET</b></button> -->
      <button onclick="getReset2()" class="btn btn-default"><i class="fa fa-repeat"></i></button>
      <!-- <button onclick="hideFilter()" class="btn btn-default"><b>X</b></button> -->
    </div>

    <h4 class="app">

      <div class="row">
        <div class="col-xs-9 col-sm-9">
          <span>Category</span>
        </div>

        <div class="col-xs-3 col-sm-3" class="align-right">
          &nbsp;&nbsp;<i class="fa fa-angle-right"></i>
        </div>
      </div>

    </h4>

    <div class="checkbox" id="app">
      All<input type="radio" name="type[]" value="all" checked onchange="addSearch('searchtype','all')" id="resetMovie"><br>
      <!-- All<input type="radio" name="type[]" value="all"  @if($type == 'all'){{'checked'}}@endif onchange="addSearch('searchtype','all')" id="resetMovie"><br> -->
      Movies<input type="radio" name="type[]" value="movie"  @if($type == 'movie'){{'checked'}}@endif onchange="addSearch('searchtype','movie')" id="resetMovie"><br>
      <!-- Web Series<input type="radio" name="type[]" value="web" @if($type == 'web'){{'checked'}}@endif onchange="addSearch('searchtype','Web')"><br>             -->
      TV Serials<input type="radio" name="type[]" value="show" @if($type == 'show'){{'checked'}}@endif onchange="addSearch('searchtype','show')"><br>
    </div>
    <br>

    <h4 class="year">

      <div class="row">
        <div class="col-xs-9 col-sm-9">
          <span>Release Year</span>
        </div>

        <div class="col-xs-3 col-sm-3" class="align-right">
          &nbsp;&nbsp;<i class="fa fa-angle-right"></i>
        </div>
      </div>
      
    </h4>
    <div class="wrapper" id="year" @if((isset($_GET['miny']) && $_GET['miny']>$minyr ) || (isset($_GET['maxy']) && $_GET['maxy']<$maxyr ) ) style="display: block;" @endif >
      <fieldset class="filter-price">

        <div class="price-field" >
          <!-- <input type="range" min="{{$minyr}}" max="{{$maxyr}}" value="{{$miny}}" id="lower" onchange="getYearFilter1()"> -->
          <!-- <input type="range" min="{{$minyr}}" max="{{$maxyr}}" value="{{$maxy}}" id="upper" onchange="getYearFilter2()"> -->
          <input type="range" min="{{$minyr}}" max="{{$maxyr}}" value="{{$miny}}" id="lower">
          <input type="range" min="{{$minyr}}" max="{{$maxyr}}" value="{{$maxy}}" id="upper">
        </div>
        <div class="price-wrap">
          {{-- <span class="price-title">FILTER</span> --}}
          <div class="price-container">
            <div class="price-wrap-1">

              <label for="one"></label>
              <input id="one" style="color: #fff!important" readonly="readonly">
            </div>
            <div class="price-wrap_line" style="color: #fff!important">-</div>
            <div class="price-wrap-2">
              <label for="two"></label>
              <input id="two" style="color: #fff!important" readonly="readonly">

            </div>
          </div>
        </div>
        <br>
      </fieldset>
    </div>
    <br>

    <h4 class="rate">

      <div class="row">
        <div class="col-xs-9 col-sm-9">
          <span>Rating</span>
        </div>

        <div class="col-xs-3 col-sm-3" class="align-right">
          &nbsp;&nbsp;<i class="fa fa-angle-right"></i>
        </div>
      </div>

    </h4>
    <div class="wrapper" id="rate" @if((isset($_GET['minr']) && $_GET['minr']>0 ) ||  (isset($_GET['maxr']) && $_GET['minr']<10 )) style="display: block;" @endif>
      <fieldset class="filter-price">

        <div class="price-field">
          <!-- <input type="range" min="0" max="10" value="{{$minr}}" id="lower-rate" onchange="getRateFilter1()"> -->
          <!-- <input type="range" min="0" max="10" value="{{$maxr}}" id="upper-rate" onchange="getRateFilter2()"> -->
          <input type="range" min="0" max="10" value="{{$minr}}" id="lower-rate" readonly="readonly" >
          <input type="range" min="0" max="10" value="{{$maxr}}" id="upper-rate" readonly="readonly" >
        </div>
        <div class="price-wrap">
          {{-- <span class="price-title">FILTER</span> --}}
          <div class="price-container">
            <div class="price-wrap-1">

              <label for="one-rate"></label>
              <input id="one-rate" style="color: #fff!important">
            </div>
            <div class="price-wrap_line" style="color: #fff!important">-</div>
            <div class="price-wrap-2">
              <label for="two-rate"></label>
              <input id="two-rate" style="color: #fff!important">

            </div>
          </div>
        </div>
        <br>
      </fieldset>
    </div>
    <br>

    <!-- Modal -->
    <div class="modal fade" id="languageselect" tabindex="-1" role="dialog" aria-labelledby="feedback-content" data-backdrop="static" data-keyboard="false" aria-hidden="true" >

     <div class="modal-dialog modal-dialog-centered modal-md" role="document" style="margin-top: 20vh">
      <button type="button" class="close float-close-pro" data-dismiss="modal" aria-label="Close">
       <i class="fa fa-user"></i>
     </button>
     <div class="modal-content">
       <div class="modal-header-pro">
         <h2> </h2>
         <h6>Select language for better suggestion</h6>
       </div>
       <div class="modal-body-pro social-login-modal-body-pro">
         <div class="row" style="text-align: center;">



           <div class="col-sm-10 col-sm-offset-1">

            @foreach ($languages as $langs)
            <div class="col-lg-3">
              <div class="input-group">
               <input type="checkbox" id="myCheckbox1" />
               <label for="myCheckbox1">{{$langs->english_name}}</label>
             </div><!-- /input-group -->
           </div><!-- /.col-lg-6 -->
           @endforeach


           <input name="submit" value="Continue" class="btn btn-danger" data-dismiss="modal" aria-label="Close" style="margin: 30px auto 45px">

         </div> 


       </div>





       <a class="not-a-member-pro" ><span>Your Feedback is important for us. </span></a>
     </div><!-- close .modal-content -->
   </div><!-- close .modal-dialog -->
 </div><!-- close .modal --> 
</div>



<h4 class="genre">
  <div class="row">
    <div class="col-xs-9 col-sm-9">
      <span>Genre</span>
    </div>

    <div class="col-xs-3 col-sm-3" class="align-right">
      &nbsp;&nbsp;<i class="fa fa-angle-right"></i>
    </div>
  </div>

</h4>
<div class="checkbox" id="genre" @if(isset($_GET['genre']) && $_GET['genre']>0) style="display: block;" @endif>
  @if(!empty($all_genres))
  All<input type="radio" name="genre" value="0" {{'checked'}} onclick="addSearch('genre','0')" ><br>
  @foreach($all_genres as $gen)
  {{$gen->title}}<input type="radio" name="genre" value="{{$gen->genre_id}}" @if(isset($_GET['genre']) && $_GET['genre']==$gen->genre_id){{'selected'}} @endif onclick="addSearch('genre','{{$gen->genre_id}}')" ><br>
  @endforeach
  @endif
</div>
<br>

<h4 class="certified">
  <div class="row">
    <div class="col-xs-9 col-sm-9">
      <span>Rated</span>
    </div>

    <div class="col-xs-3 col-sm-3" class="align-right">
      &nbsp;&nbsp;<i class="fa fa-angle-right"></i>
    </div>
  </div>
</h4>
<div class="checkbox" id="certified" @if(isset($_GET['rated']) && $_GET['rated']!='all') style="display: block;" @endif>
 All<input type="radio" name="rated" id="rated_U" value="U" onclick="addSearch('rated','all')" {{'checked'}}><br>  
 U<input type="radio" name="rated" id="rated_U" value="U" onclick="addSearch('rated','U')" @if(!empty($rated)) @if($rated == 'U'){{'checked'}}@endif @endif><br>
 U/A<input type="radio" name="rated" id="rated_UA" value="UA" onclick="addSearch('rated','UA')" @if(!empty($rated)) @if($rated == 'UA'){{'checked'}}@endif @endif><br>
 A<input type="radio" name="rated" value="A" id="rated_A" onclick="addSearch('rated','A')" @if(!empty($rated)) @if($rated == 'A'){{'checked'}}@endif @endif><br>
 <br>
</div>
<br>
<h4 class="language" style="cursor: pointer;">

  <div class="row">
    <div class="col-xs-9 col-sm-9">
      <span>Language</span>
    </div>

    <div class="col-xs-3 col-sm-3" class="align-right">
      &nbsp;&nbsp;<i class="fa fa-angle-right"></i>
    </div>
  </div>


</h4>
<div class="checkbox" id="language" @if(isset($_GET['lang']) && $_GET['lang']=='All') style="display: block;" @endif >

  @if(isset($tab))
  <!-- <select name="lang" id="lang" class="form-sort" onchange="getLang('lang',)" style="width: 95%"> -->
    <!-- <option value="">-- Select Language --</option> -->
    @if(isset($languages))
    ALL<input type="radio" name="lang" {{'checked'}} id="rated_U" value="All" onclick="addSearch('lang','All')" ><br>

    @foreach($languages as $lang)

    {{ucfirst($lang->english_name)}}<input type="radio" name="lang" id="lang{{$lang->original_language}}" value="{{$lang->original_language}}" onclick="addSearch('lang','{{$lang->original_language}}')" @if(isset($_GET['lang']) && $_GET['lang']== $lang->original_language){{'selected'}} @endif><br>

    <!-- <option value="{{$lang->original_language}}" @if(isset($langs)) @if($langs==$lang->original_language) {{'selected'}} @endif @endif>{{$lang->english_name}}</option> -->
    @endforeach
    @endif

    <!-- </select> -->
    @endif

  </div>
  <br>

      <!-- <h4 class="top_rated_imdb">
        <div class="row">
          <div class="col-xs-9 col-sm-9">
            <span>Top 10 (IMDB)</span>
          </div>
          <div class="col-xs-3 col-sm-3" class="align-right">
            &nbsp;&nbsp;<i class="fa fa-angle-right"></i>
          </div>
        </div>        
      </h4>
      <div id="top_rated_imdb">
        <ul>
          <li><a href="{{url('top-10/movies/imdb')}}">Movies</a></li>
          <li><a href="{{url('top-10/tv-shows/imdb')}}">Tv Shows</a></li>
          <li><a href="{{url('top-10/viral-videos/imdb')}}">Viral Videos</a></li>
        </ul>
      </div>
      <br>

      <h4 class="top_rated_tmdb">
        <div class="row">
          <div class="col-xs-9 col-sm-9">
            <span>Top 10 (TMDB)</span>
          </div>
          <div class="col-xs-3 col-sm-3" class="align-right">
            &nbsp;&nbsp;<i class="fa fa-angle-right"></i>
          </div>
        </div>        
      </h4>
      <div id="top_rated_tmdb">
        <ul>
          <li><a href="{{url('top-10/movies/tmdb')}}">Movies</a></li>
          <li><a href="{{url('top-10/tv-shows/tmdb')}}">Tv Shows</a></li>
          <li><a href="{{url('top-10/viral-videos/tmdb')}}">Viral Videos</a></li>
        </ul>
      </div>
      <br> -->

    </div>
    @if (isset($homeside))
    @foreach ($homeside as $ads)
    <div>
      <a href="{{$ads->redirect_url ?? ''}}">
                            <img src="{{asset($ads->poster)}}" style="width:100%; height: 450px">
                            </a>
      
    </div>
    @endforeach
    @endif


  </div>
  <div class="col-sm-10 col-lg-10 col-xs-12 right-filter">
    <div class="row" style="width: 100%;">
      <div class="col-sm-6 col-xs-12">
        @if(isset($keyword))
        <span>Search result for "<i>{{$keyword}}</i>"&nbsp;&nbsp;
          <a href="{{url('/')}}"><u>RESET</u></a>
        </span>
        @endif
      </div>
      <div class="col-sm-6 col-xs-12">
        @if(isset($tab))
        <?php if (isset($_GET['order']) && $_GET['order']) {$order = $_GET['order']; }?>
        <div class="col-sm-3 col-xs-6 pull-right">
          <select name="order" id="order" class="form-sort" onchange="addSearch('order',document.getElementById('order').value)" style="width: 90%">
            <option value="desc" @if(isset($_GET['order']) && $_GET['order']=='desc'){{'selected'}} @endif>Descending </option>
            <option value="asc" @if(isset($_GET['order']) && $_GET['order']=='asc'){{'selected'}} @endif>Ascending</option>

          </select>
        </div>
        <div class="col-sm-3 col-xs-6 pull-right">
         <?php if (isset($_GET['sort_by']) && $_GET['sort_by']) {$sort = $_GET['sort_by']; }?>
         <select name="sort_by" id="sort" class="form-sort" onchange="addSearch('sort_by',document.getElementById('sort').value)" style="width: 90%">
          <option value="">Sort By --</option>
          <option value="relevance" @if(isset($_GET['sort_by']) && $_GET['sort_by']=='relevance'){{'selected'}} @endif>Relevance</option>
          <option value="release_year" @if(isset($_GET['sort_by']) && $_GET['sort_by']=='release_year'){{'selected'}} @endif>Release Year</option>
          
          <option value="rating" @if(isset($_GET['sort_by']) && $_GET['sort_by']=='rating'){{'selected'}} @endif>Rating</option>
          <option value="popularity" @if(isset($_GET['sort_by']) && $_GET['sort_by']=='popularity'){{'selected'}} @endif>Popularity </option>
          
        </select>
      </div>

      @endif
    </div>
  </div>


  <div class="movie-content" id="dynamic-search" style="height: 100%">
    <div class="row">
      <div class="applied-filters">
        <ul>

          <?php if (isset($_GET['searchtype'])){?>
            <li>
              Category : <?php echo ucfirst($_GET['searchtype']);?>
            </li>
          <?php } ?>


          <?php if (isset($_GET['sort_by'])){?>
            <li>
              Sort By : <?php echo ucfirst($_GET['sort_by']); ?>
            </li>
          <?php } ?>


            <?php if (isset($_GET['order'])){?>
          <li>
              Order : <?php echo $_GET['order']=='asc' ? 'Ascending':'Descending';?>
          </li>
            <?php } ?>

            <?php if (!empty($_GET['provider'])){?>
          <li>
             OTT : <?php echo $_GET['provider']?>
          </li>
           <?php } ?>

            <?php if (isset($_GET['lang'])){?>
          <li>
             Language : <?php echo strtoupper($_GET['lang']); ?>
          </li>
           <?php } ?>

            <?php if (isset($_GET['rated'])){?>
          <li>
             Rated : <?php echo strtoupper($_GET['rated']); ?>
          </li>
           <?php } ?>

            <?php if (isset($_GET['minr'])){?>
          <li>
               Min Rate : <?php echo $_GET['minr']?>
          </li>
             <?php } ?>

            <?php if (isset($_GET['maxr'])){?>
          <li>
             Max Rate : <?php echo $_GET['maxr']?>
          </li>
           <?php } ?>

            <?php if (isset($_GET['miny'])){?>
          <li>
             Min Yesr : <?php echo $_GET['miny']?>
          </li>
           <?php } ?>

            <?php if (isset($_GET['maxy'])){?>
          <li>
             Max Year : <?php echo $_GET['maxy']?>
          </li>
           <?php } ?>

            <?php if (isset($_GET['genre'])){?>
          <li>
             Genre : <?php echo $_GET['genre']?>
          </li>
           <?php } ?>

        </ul>
      </div>
    </div>

    @if (isset($contents))
    <?php $space = 0; $rat=0; $adcount = 0; $moviecount = 0;?>
    @foreach ($contents as $element)
    <?php 
    $parent_title = "";
    $moviecount++;
    $flag = 1; 
    $space++;
    if(!empty($element->parent_id) && $element->parent_id>0){
      $parent_title = DB::table('contents')->where('content_id',$element->parent_id)->first();
    }
    ?>

    
    <!-- =====================  advertisement ================================ -->

    <?php 
    if (count($movielist)>1)
    if ($moviecount == 4 || $moviecount == 21){ 
      $xtra = 0;
          if ($moviecount == 21){
            $xtra = 1;
          }
          else $xtra = 0;


      ?>

    <div class="col-sm-3 col-lg-2 col-md-3 col-xs-6 search-response dynamic" style="padding-left: 1%; padding-right: 1%">

    

     <a  href="<?php echo $movielist[$xtra]->redirect_url?>">



      <div class="movie-item">
       <div class="mv-img">
        <img data-src="{{asset($movielist[$xtra]->poster)}}" alt="{{$element->title ?? ''}}" class="poster-image web-image lazy">
       
      </div>
      <div class="hvr-inner">
       {{-- <i class="fa fa-play" aria-hidden="true"></i>  --}}
     </div>
    
  
  </div>
</a>
</div>

<?php } ?>



    <!-- =====================  advertisement ================================ -->













    <div class="col-sm-3 col-lg-2 col-md-3 col-xs-6 search-response dynamic" style="padding-left: 1%; padding-right: 1%">

     <?php $urlen = preg_replace('/[^a-zA-Z0-9_.]/', '-', (strtolower($element->title))).'-'.$element->content_id; ?>  

     <a  href="/detail/{{$element->content_type}}/{{$urlen}}">



      <div class="movie-item">
       <div class="mv-img">
        @if(!empty($element->poster) && strpos($element->poster, 'poster/') == false && file_exists(public_path().'/poster/'.$element->poster))
        <img data-src="/poster/{{$element->poster}}" alt="{{$element->title ?? ''}}" class="poster-image web-image lazy">
        @else
        <img src="/images/default.png" alt="{{$element->title ?? ''}}" class="web-image">
        @endif
      </div>
      <div class="hvr-inner">
       {{-- <i class="fa fa-play" aria-hidden="true"></i>  --}}
     </div>
     <div class="title-in">

      <p>
       &nbsp;&nbsp;IMDB: <span style="color: #fff; font-size: 12px;">@if(isset($element->imdb_score)){{$element->imdb_score}}@else{{'N/A'}}@endif</span>
     </p>
   </div>
   <div class="imdb">
    <div class="row" style="text-align: left;">
      <?php $urlen = preg_replace('/[^a-zA-Z0-9_.]/', '-', (strtolower($element->title))).'-'.$element->content_id; ?>
      <span><a href="/detail/{{$element->content_type}}/{{$urlen}}" style="font-size: 15px; color : #fff"> 
        <?php
        if(isset($element->title))
          echo substr(explode(':',$element->title)[0],0,18); 
        elseif(isset($element->original_title))
          echo substr($element->original_title,0,25); 
        if(isset($parent_title) && isset($parent_title->title))
          echo " (".substr($parent_title->title,0,25).") ";
        elseif(isset($parent_title) && isset($parent_title->original_title))
          echo " (".substr($parent_title->original_title,0,25).") ";
        ?></a></span>

      </div>
      <div class="row rate-row" style="font-size: 12px; color:#9ba09b!important;text-transform: uppercase; ">
        <?php if ($element->original_language == 'hi') $element->original_language = "Hindi";?>
        <?php if ($element->original_language == 'en') $element->original_language = "English";?>
        <?php if ($element->original_language == 'te') $element->original_language = "Telugu";?>
        <?php if ($element->original_language == 'ta') $element->original_language = "Tamil";?>
        <?php if ($element->original_language == 'bn') $element->original_language = "Bengali";?>
        <?php if ($element->original_language == 'kn') $element->original_language = "Kannada";?>
        <?php if ($element->original_language == 'es') $element->original_language = "Spanish";?>
        <?php if ($element->original_language == 'ru') $element->original_language = "Russian";?>
        <?php if ($element->original_language == 'gu') $element->original_language = "Gujrati";?>
        <?php if ($element->original_language == 'ml') $element->original_language = "Malyalam";?>
        <?php if ($element->original_language == 'pa') $element->original_language = "Punjabi";?>

        {{ucfirst($element->content_type)?? ''}} @if(!empty($element->original_language))<span class="dot"></span>&nbsp;&nbsp;@endif
        {{ $element->original_language ?? ''}}
        @if(isset($element->age_certification))<span class="dot"></span> &nbsp;&nbsp;&nbsp;{{$element->age_certification}}@else<span class="dot"></span> &nbsp;&nbsp;&nbsp;{{'U/A'}}@endif



      </div>
    </div>
  </div>
</a>
</div>
<?php $rat++; ?>
@endforeach

@endif

@if($flag == 0)
<h4 class="result-sec">No result found</h4>
@endif

</div>
</div>

@endsection 

@section('paginate')


<div class="container" style="text-align: center;">
  <div class="row">
    @if (!is_array($contents) && $tab != "Select Your favourite" && $contents->links() != null)
    {{$contents->links()}}
    @else
    <nav aria-label="Page navigation example">
      <ul class="pagination pg-blue">
        <?php if (isset($contents[0]->record_cnt)) {
          $totalpages = ceil($contents[0]->record_cnt/30);
          if (!isset($_GET['page'])){
            $page = 1;
          }
          else
            $page = $_GET['page'];
          ?>
          <li class="page-item"><a class="page-link" href="
            ?page={{$previous}}<?php if (isset($_GET['sort_by'])){?>&sort_by=<?php echo $_GET['sort_by']?><?php } ?><?php if (!empty($_GET['search'])){?>&search=<?php echo $_GET['search']?><?php } ?><?php if (!empty($_GET['provider'])){?>&provider=<?php echo $_GET['provider']?><?php } ?><?php if (isset($_GET['order'])){?>&order=<?php echo $_GET['order']?><?php } ?><?php if (isset($_GET['searchtype'])){?>&searchtype=<?php echo $_GET['searchtype']?><?php } ?><?php if (isset($_GET['lang'])){?>&lang=<?php echo $_GET['lang']?><?php } ?><?php if (isset($_GET['rated'])){?>&rated=<?php echo $_GET['rated']?><?php } ?><?php if (isset($_GET['minr'])){?>&minr=<?php echo $_GET['minr']?><?php } ?><?php if (isset($_GET['genre'])){?>&genre=<?php echo $_GET['genre']?><?php } ?><?php if (isset($_GET['maxr'])){?>&maxr=<?php echo $_GET['maxr']?><?php } ?><?php if (isset($_GET['miny'])){?>&miny=<?php echo $_GET['miny']?><?php } ?><?php if (isset($_GET['maxy'])){?>&maxy=<?php echo $_GET['maxy']?><?php } ?>">Previous</a></li>



            <?php for ($i=$page_no; $i <= $totalpages; $i++) { 
              if ($i > $page_no+5)
               continue;?>

             <li class="page-item @if ($page == $i) {{'active'}} @endif"><a href="?page={{$i}}<?php if (isset($_GET['sort_by'])){?>&sort_by=<?php echo $_GET['sort_by']?><?php } ?><?php if (!empty($_GET['search'])){?>&search=<?php echo $_GET['search']?><?php } ?><?php if (!empty($_GET['provider'])){?>&provider=<?php echo $_GET['provider']?><?php } ?><?php if (isset($_GET['order'])){?>&order=<?php echo $_GET['order']?><?php } ?><?php if (isset($_GET['searchtype'])){?>&searchtype=<?php echo $_GET['searchtype']?><?php } ?><?php if (isset($_GET['lang'])){?>&lang=<?php echo $_GET['lang']?><?php } ?><?php if (isset($_GET['rated'])){?>&rated=<?php echo $_GET['rated']?><?php } ?><?php if (isset($_GET['minr'])){?>&minr=<?php echo $_GET['minr']?><?php } ?><?php if (isset($_GET['genre'])){?>&genre=<?php echo $_GET['genre']?><?php } ?><?php if (isset($_GET['maxr'])){?>&maxr=<?php echo $_GET['maxr']?><?php } ?><?php if (isset($_GET['miny'])){?>&miny=<?php echo $_GET['miny']?><?php } ?><?php if (isset($_GET['maxy'])){?>&maxy=<?php echo $_GET['maxy']?><?php } ?>" class="page-link">{{$i}}</a></li>
            <?php } ?>
            <li class="page-item"><a class="page-link" 

              href="?page={{$next}}<?php if (isset($_GET['sort_by'])){?>&sort_by=<?php echo $_GET['sort_by']?><?php } ?><?php if (!empty($_GET['search'])){?>&search=<?php echo $_GET['search']?><?php } ?><?php if (!empty($_GET['provider'])){?>&provider=<?php echo $_GET['provider']?><?php } ?><?php if (isset($_GET['order'])){?>&order=<?php echo $_GET['order']?><?php } ?><?php if (isset($_GET['searchtype'])){?>&searchtype=<?php echo $_GET['searchtype']?><?php } ?><?php if (isset($_GET['lang'])){?>&lang=<?php echo $_GET['lang']?><?php } ?><?php if (isset($_GET['rated'])){?>&rated=<?php echo $_GET['rated']?><?php } ?><?php if (isset($_GET['minr'])){?>&minr=<?php echo $_GET['minr']?><?php } ?><?php if (isset($_GET['genre'])){?>&genre=<?php echo $_GET['genre']?><?php } ?><?php if (isset($_GET['maxr'])){?>&maxr=<?php echo $_GET['maxr']?><?php } ?><?php if (isset($_GET['miny'])){?>&miny=<?php echo $_GET['miny']?><?php } ?><?php if (isset($_GET['maxy'])){?>&maxy=<?php echo $_GET['maxy']?><?php } ?>">Next</a></li>
            <?php      } ?>
          </ul>
        </nav>
        @endif

      </div>
    </div>


    @endsection

    @section('script')

    <script type="text/javascript">

      window.onload=function(){
        $('#featured_slider').slick({
          autoplay:true,
          autoplaySpeed: 5000,
          arrows:true,
          prevArrow:'<button type="button" class="slick-prev"></button>',
          nextArrow:'<button type="button" class="slick-next"></button>',
          centerMode:true,
          slidesToShow:1,
          adaptiveHeight: true,
          slidesToScroll:1
        });
      };
    </script>
    <script>
      $('.gallery-responsive').slick({
        dots: false,
        infinite: true,
        @if ($i)
        initialSlide:{{$i}},
        @endif
        speed: 300,
        slidesToShow: 18,
        slidesToScroll: 10,
        responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 12,
            slidesToScroll: 8,
            infinite: false,
            @if ($i)
            initialSlide:{{$i}},
            @endif
            dots: false
          }
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 8,
            @if ($i)
            initialSlide:{{$i}},
            @endif
            slidesToScroll: 5
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 6,
            @if ($i)
            initialSlide:{{$i}},
            @endif
            slidesToScroll: 4
          }
        }
    // You can unslick at a given breakpoint now by adding:
    // settings: "unslick"
    // instead of a settings object
    ]
  });


</script>

<script type="text/javascript">


  var lowerSlider1 = document.querySelector('#lower-rate');
  var upperSlider1 = document.querySelector('#upper-rate');

  document.querySelector('#two-rate').value=upperSlider1.value;
  document.querySelector('#one-rate').value=lowerSlider1.value;

  var lowerVal1 = parseInt(lowerSlider1.value);
  var upperVal1 = parseInt(upperSlider1.value);
  
  upperSlider1.oninput = function () {

    lowerVal1 = parseInt(lowerSlider1.value);
    upperVal1 = parseInt(upperSlider1.value);

    if (upperVal1 <= (lowerVal1 + 1)) {
      lowerSlider1.value = upperVal1 - 1;

      if (lowerVal1 == lowerSlider1.min) {
        upperSlider1.value = 1;
      }
    }
    min = lowerSlider1.value;
    max = upperSlider1.value;
    document.querySelector('#two-rate').value=max;
    document.querySelector('#one-rate').value=min
    getRateFilter2();
    
  };

  lowerSlider1.oninput = function () {

    lowerVal1 = parseInt(lowerSlider1.value);
    upperVal1 = parseInt(upperSlider1.value);
    if (lowerVal1 >= (upperVal1 - 1)) {
      upperSlider1.value = lowerVal1 + 1;
      if (upperVal1 == upperSlider1.max) {
        lowerSlider1.value = parseInt(upperSlider1.max) - 1;
      }
    }
    var max = upperSlider1.value;
    var min = lowerSlider1.value;
    document.querySelector('#one-rate').value=min
    document.querySelector('#two-rate').value=max

    getRateFilter1();

  };

</script>

<script type="text/javascript">


  var lowerSlider2 = document.querySelector('#lower');
  var  upperSlider2 = document.querySelector('#upper');

  document.querySelector('#two').value=upperSlider2.value;
  document.querySelector('#one').value=lowerSlider2.value;

  var lowerVal2 = parseInt(lowerSlider2.value);
  var upperVal2 = parseInt(upperSlider2.value);

  upperSlider2.oninput = function () {
    lowerVal2 = parseInt(lowerSlider2.value);
    upperVal2 = parseInt(upperSlider2.value);

    if (upperVal2 < lowerVal2 + 2) {
      lowerSlider2.value = upperVal2 - 2;
      if (lowerVal2 == lowerSlider2.min) {
        upperSlider2.value = 2;
      }
    }
    var max = upperSlider2.value;
    var min = lowerSlider2.value;
    document.querySelector('#one').value=min
    document.querySelector('#two').value=max

    getYearFilter2();
  };

  lowerSlider2.oninput = function () {
    lowerVal2 = parseInt(lowerSlider2.value);
    upperVal2 = parseInt(upperSlider2.value);
    if (lowerVal2 > upperVal2 - 2) {
      upperSlider2.value = lowerVal2 + 2;
      if (upperVal2 == upperSlider2.max) {
        lowerSlider2.value = parseInt(upperSlider2.max) - 2;
      }
    }
    var max = upperSlider2.value;
    var min = lowerSlider2.value;
    document.querySelector('#one').value=min
    document.querySelector('#two').value=max
    getYearFilter1();
  }; 

</script>

<script type="text/javascript">

  function hideFilter(){
    $('.left-side-menu').hide();
    $('.open-filter').show();
    $('.open-filter').show();
    $('.right-filter').removeClass('col-sm-10');
    $('.right-filter').removeClass('col-lg-10');
    // $('.slide-image').removeClass('mv-img');
    $('.mv-img').css('height','360px!important');
    $('.right-filter').addClass('col-sm-12');
    $('.right-filter').addClass('col-lg-12');
  }

  function showFilter(){
    $('.open-filter').hide();
    $('.right-filter').removeClass('col-sm-12');
    $('.right-filter').removeClass('col-lg-12');
    $('.slide-image').addClass('mv-img');
    $('.mv-img').css('height','290px!important');
    $('.right-filter').addClass('col-sm-10');
    $('.right-filter').addClass('col-lg-10');
    $('.left-side-menu').show();
  }


  function addSearch(parameter,data){
    tmp = location.search
    .substr(1)
    .split("&");
    newdata = [];
    found = 0;
    tmp.forEach(function(item){
      if (item.split('=')[0] == parameter){
        found = 1;
        str = parameter+'='+data;
        newdata.push(str);
      }
      else
        newdata.push(item);
      
    });
    if (found == 0){
      str = parameter+'='+data;
      newdata.push(str);
    }
    parameters = newdata.join('&')
    window.location.href = location.href.split('?')[0]+'?'+parameters;
  }





  function getProvider(id,t_name){

    var data = t_name+"_"+id;
    addSearch('provider', data)
  }

  function getYearFilter1(){
    var data1 = $('#one').val();
    addSearch('miny', data1)

  }

  function getYearFilter2(){
    var data2 = $('#two').val();

    addSearch('maxy', data2)  
  }

  function getRateFilter1(){
    var data1 = $('#one-rate').val();
    addSearch('minr', data1)

  }

  function getRateFilter2(){
    var data2 = $('#two-rate').val();
    addSearch('maxr', data2) 
  }



  function getReset2()
  {
    window.location.href = "/";
  }

</script>

<script type="text/javascript">
  var slideIndex = 0;
showSlides();

  $('.app').on('click',function(){
    $('#app').toggle();
    $('.app .fa').toggleClass('fa-angle-down');
  });

  $('.rate').on('click',function(){
    $('#rate').toggle();
    $('.rate .fa').toggleClass('fa-angle-down');
  });

  $('.year').on('click',function(){
    $('#year').toggle();
    $('.year .fa').toggleClass('fa-angle-down');
  });

  $('.genre').on('click',function(){
    $('#genre').toggle();
    $('.genre .fa').toggleClass('fa-angle-down');
  });

  $('.certified').on('click',function(){
    $('#certified').toggle();
    $('.certified .fa').toggleClass('fa-angle-down');
    // $('.certified .fa').addClass('fa-angle-down');
  });

  $('.language').on('click',function(){
    $('#language').toggle();
    $('.language .fa').toggleClass('fa-angle-down');
  });

  $('.top_rated_imdb').on('click',function(){
    $('#top_rated_imdb').toggle();
    $('.top_rated_imdb .fa').toggleClass('fa-angle-down');
  });

  $('.top_rated_tmdb').on('click',function(){
    $('#top_rated_tmdb').toggle();
    $('.top_rated_tmdb .fa').toggleClass('fa-angle-down');
  });


 //   $(document).ready(function(){   
 //    alpha = 1;
 //    <?php
  // if (isset($_GET['deletecookies'])) echo "alpha = 5"; 
 ?>
 //    if (alpha == 5 ){
 //      //deletecookies 
 //      alert('cookies being deleted');
 //    }
 //  if (!!$.cookie('genres')) {
 //    // alert('hi');
 //   // have cookie
 //  } else {
 //    // alert('h3llo');
 //    // header('Location:"/selectmovie"')
 //    window.location.href = "/selectmovie";
 //    // $('#languageselect').modal({backdrop: false});
 //   // no cookie
 //  }
 // });  


function showSlides() {
  var i;
  var slides = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("dot");
  for (i = 0; i < slides.length; i++) {
    slides[i].style.display = "none";
  }
  slideIndex++;
  if (slideIndex > slides.length) {slideIndex = 1}
  for (i = 0; i < dots.length; i++) {
    dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex-1].style.display = "block";
  dots[slideIndex-1].className += " active";
  setTimeout(showSlides, 3000); // Change image every 2 seconds
}


</script>

@endsection
