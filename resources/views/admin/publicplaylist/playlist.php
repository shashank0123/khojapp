@extends('layouts.admin')


@section('content')

<h4 class="header-title m-t-0 m-b-30">Default Example</h4>
<h4 class="header-title m-t-0 m-b-30">User List</h4>
@if(Session::has('message'))
   <div class="alert alert-success">{{ Session::get('message') }}</div>
@endif
<table id="datatable" class="table table-striped table-bordered">
    <thead>
        <tr>
            <th>id</th>
            <th>Title</th>
            <th>order</th>
            <th>movies</th>
            
           
        </tr>
    </thead>

    <tbody>
    	@foreach ($playlists as $user)
        <tr>
            <td>{{ $user->id }}</td>
            <td>{{ $user->playlist_name ?? ''}}</td>
            <td>{{ $user->playlist_order ?? ''}}</td>
            <td>{{ $user->playlist_content ?? ''}}</td>
            
        </tr>
        @endforeach
       
    </tbody>
</table> 
                        
                        


                        


                        

                    
@endsection

