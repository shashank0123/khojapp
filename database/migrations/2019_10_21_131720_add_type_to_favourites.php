<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTypeToFavourites extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('favourites', function (Blueprint $table) {
            $table->string('type');
        }); 

        Schema::table('playlists', function (Blueprint $table) {
            $table->string('type');
        }); 

        Schema::table('watch_lists', function (Blueprint $table) {
            $table->string('type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('favourites', function (Blueprint $table) {
            //
        });
        Schema::table('playlists', function (Blueprint $table) {
            //
        });
        Schema::table('watch_lists', function (Blueprint $table) {
            //
        });
    }
}
