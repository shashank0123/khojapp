@extends('layouts.admin')


@section('content')

<h4 class="header-title m-t-0 m-b-30">Default Example</h4>
<h4 class="header-title m-t-0 m-b-30">User List</h4>
@if(Session::has('message'))
   <div class="alert alert-success">{{ Session::get('message') }}</div>
@endif
<table id="datatable" class="table table-striped table-bordered">
    <thead>
        <tr>
            <th>id</th>
            <th>Name</th>
            <th>Email</th>
            <th>mobile</th>
            <th>user_type</th>
            {{-- <th>Action</th> --}}
           
        </tr>
    </thead>

    <tbody>
    	@foreach ($users as $user)
        <tr>
            <td>{{ $user->id }}</td>
            <td>{{ $user->name ?? ''}}</td>
            <td>{{ $user->email ?? ''}}</td>
            <td>{{ $user->mobile ?? ''}}</td>
            <td>{{ $user->user_type }}</td>            
            
        </tr>
        @endforeach
       
    </tbody>
</table> 
                        
                        


                        


                        

                    
@endsection

