@extends('layouts.khoj_new')
@php $miny=1900; $maxy=date('Y'); $minr=0; $maxr=10; @endphp
@section('content')
<br><br>
<div class="container">
	<div class="row align-items-center">

	<div class="col-md-6">
		<div class="about-sec1">

			<h3 >WHAT WE DO</h3>
			<h5>For our users Apps for movie & TV show fans</h5>
			  <p>We show you where you can legally watch movies and TV shows that you love. You  are kept up to date with what is new on Netflix, Amazon Prime, iTunes and many other streaming platforms. Our simple filter system allows you to see only what is important to you.<br>
              We also tell you where and when to watch movies on the big screen so you never miss when a movie is running in cinema again.</p>
              {{-- <button type="button" class="btn btn-primary about-btn1">Learn more about our apps</button> --}}
			
		</div>
	</div>
	<div class="col-md-6">
		<div class="about-img" style="text-align: right;">
			<img class="img-responsive" src="{{asset('asset/img/about.jpg')}}" style="height: 200px; width: auto; margin-left: 50px">
		</div>
	</div>
		
	</div>
</div>


<section>
	<div class="container" id="margin50-50">
		<div class="row">
			<div class="col-lg-4 col-md-6">
				<div class="about-card1" style="background:purple; ">
                    <div class="about-card_title">
                    	<h4>44</h4>
                    <p>Staff</p>
                   </div>
                </div>
			</div>

			<div class="col-lg-4 col-md-6">
				<div class="about-card1" style="background:hotpink;">
                    <div class="about-card_title">
                    	<h4>44</h4>
                    <p>Countries</p>
                   </div>
                </div>
				
			</div>

			<div class="col-lg-4 col-md-6" >
				<div class="about-card1" style="background:orangered;">
                    <div class="about-card_title">
                    	<h4>44</h4>
                    <p>User Profiles</p>
                   </div>
                </div>
				
			</div>
		</div>
		
	</div>
</section>



<h2 class="head-title">MEET THE FOUNDERS</h2>


<section>
	<ul class="one">
  <li class="transition">
    <div class="wrapper"> <img class="transition" src="{{asset('asset/img/img_01.png')}}">
      <ul class="social">
        <li>Young artist from Barcelona. She hates Comic Sans. <br>
          Find her on:</li>
        <li class="transition"><a href="#"><img src="{{asset('asset/img/twitter.svg')}}"></a></li>
        <li class="transition"><a href="#"><img src="{{asset('asset/img/behance.svg')}}"></a></li>
        <li class="transition"><a href="#"><img src="{{asset('asset/img/facebook.svg')}}"></a></li>
        <li class="transition"><a href="#"><img src="{{asset('asset/img/skype.svg')}}"></a></li>
      </ul>
      <span class="transition">
      <h3>Lucy Copycat <em>CEO & Founder</em></h3>
      <img src="{{asset('asset/img/more.svg')}}"> </span> </div>
  </li>
  <li class="transition">
    <div class="wrapper"> <img class="transition" src="{{asset('asset/img/img_01.png')}}">
      <ul class="social">
        <li>Best lorem dolor guy from Detroit. Loves Helvetica. <br>
          Find him on:</li>
        <li class="transition"><a href="#"><img src="{{asset('asset/img/twitter.svg')}}"></a></li>
        <li class="transition"><a href="#"><img src="{{asset('asset/img/behance.svg')}}"></a></li>
        <li class="transition"><a href="#"><img src="{{asset('asset/img/facebook.svg')}}"></a></li>
        <li class="transition"><a href="#"><img src="{{asset('asset/img/skype.svg')}}"></a></li>
      </ul>
      <span class="transition">
      <h3>Johan Van Font <em>Front end developer</em></h3>
      <img src="{{asset('asset/img/more.svg')}}"> </span> </div>
  </li>
  <li class="transition">
    <div class="wrapper"> <img class="transition" src="{{asset('asset/img/img_01.png')}}">
      <ul class="social">
        <li>Founder of Lorem dolor web site. Loves Verdana. <br>
          Find him on:</li>
        <li class="transition"><a href="#"><img src="{{asset('asset/img/twitter.svg')}}"></a></li>
        <li class="transition"><a href="#"><img src="{{asset('asset/img/behance.svg')}}"></a></li>
        <li class="transition"><a href="#"><img src="{{asset('asset/img/facebook.svg')}}"></a></li>
        <li class="transition"><a href="#"><img src="{{asset('asset/img/skype.svg')}}"></a></li>
      </ul>
      <span class="transition">
      <h3>Johan Van Font <em>Front end developer</em></h3>
      <img src="{{asset('asset/img/more.svg')}}"> </span> </div>
  </li>
  <li class="transition">
    <div class="wrapper"> <img class="transition" src="{{asset('asset/img/img_01.png')}}">
      <ul class="social">
        <li>Times New Roman but in love with Verdana Bold. <br>
          Find him on:</li>
        <li class="transition"><a href="#"><img src="{{asset('asset/img/twitter.svg')}}"></a></li>
        <li class="transition"><a href="#"><img src="{{asset('asset/img/behance.svg')}}"></a></li>
        <li class="transition"><a href="#"><img src="{{asset('asset/img/facebook.svg')}}"></a></li>
        <li class="transition"><a href="#"><img src="{{asset('asset/img/skype.svg')}}"></a></li>
      </ul>
      <span class="transition">
      <h3>Johan Van Font <em>Front end developer</em></h3>
      <img src="{{asset('asset/img/more.svg')}}"> </span> </div>
  </li>
</ul>
</section>

<h2 class="head-title">WE WANT TO HEAR FROM YOU</h2>
<section>
	<div class="container" id="about-sec3">
		<div class="row">
			<div class="col-lg-4 col-md-6">
				<div class="about-sec3">
					<p>We work tirelessly to make the experience of using our apps the best that it can be and we love any feedback or suggestions you may have in order for us to improve further.</p>
				</div>
			</div>

			<div class="col-lg-4 col-md-6">
				<div class="about-sec3">
					<p>We work tirelessly to make the experience of using our apps the best that it can be and we love any feedback or suggestions you may have in order for us to improve further.</p>
				</div>
				
			</div>

			<div class="col-lg-4 col-md-6">
				<div class="about-sec3">
					<p>We work tirelessly to make the experience of using our apps the best that it can be and we love any feedback or suggestions you may have in order for us to improve further.</p>
				</div>
				
			</div>
		</div>
		{{-- <button type="button" class="btn btn-primary about-btn1">Write to info@khoj.com</button> --}}
	</div>
</section>
@endsection