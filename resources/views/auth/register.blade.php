@extends('registernew')

@section('content')
<style type="text/css">
.bottom-nav ul li { color: #fff !important; }
#content-pro { background-color: #ffffff!important  }
</style>
<div id="content-pro" >                   
    <div id="pricing-plans-background-image">
        <div class="container">
            <div class="registration-steps-page-container" style="margin-top: 90px">
                
               <form class="registration-steps-form" method="POST" action="{{ route('register') }}">
                @csrf
                <div class="row">
                    <div class="col-sm-8">
                        <div class="registration-social-login-container">                             <h1>Register</h1><br>

                           <div class="form-group">
                               <label for="full-name" class="col-form-label">Full Name</label>
                               <input type="text" name="name" class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }}" id="full-name" value="{{ old('name') }}" required autofocus placeholder="John Doe">
                               @if ($errors->has('name'))
                               <span class="invalid-feedback">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="form-group">
                           <label for="email" class="col-form-label">Email</label>
                           <input type="email" name="email" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" id="email_id" value="{{ old('email') }}" placeholder="" onchange="checkEmail()">

                           <p style="color: red" class="emailValidate"></p>
                           @if ($errors->has('email'))
                               <span class="invalid-feedback">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                            @endif
                       </div>
                       <div class="form-group">
                           <label for="mobile" class="col-form-label">Mobile</label>
                           <input type="text" name="mobile" class="form-control {{ $errors->has('mobile') ? ' is-invalid' : '' }}" id="mobile" value="{{ old('mobile') }}" placeholder="" onchange="checkMobile()" pattern="[6-9]{1}[0-9]{9}" maxlength="10">
                           <p style="color: red" class="mobileValidate"></p>
                           @if ($errors->has('mobile'))
                               <span class="invalid-feedback">
                                <strong>{{ $errors->first('mobile') }}</strong>
                            </span>
                            @endif
                       </div>
                       <div class="form-group">
                           
                           <label for="password" class="col-form-label">Password</label>
                           <input type="password" name="password" class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}" id="password" value="{{ old('password') }}" placeholder="&middot;&middot;&middot;&middot;&middot;&middot;">
                            @if ($errors->has('password'))
                               <span class="invalid-feedback">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                            @endif
                       </div>
                       <div class="form-group">
                           <label for="confirm-password" class="col-form-label">&nbsp;</label>
                           <input type="password" name="password_confirmation" class="form-control" id="confirm-password" placeholder="Confirm Password">
                       </div>
                       
                       <div class="registration-social-login-or">or</div>
                       
                   </div><!-- close .registration-social-login-container -->
               </div>
               <div class="col-sm-4">
                   <div class="registration-social-login-options">
                    <h6>Sign in with one of your social accounts</h6>
                    <div class="social-icon-login facebook-color"><a href="{{ url('redirect/facebook') }}" style="color: #fff"><i class="fab fa-facebook-f"></i> <span>Facebook</span></a></div>
                    {{-- <div class="social-icon-login twitter-color"><i class="fab fa-twitter"></i> Twitter</div> --}}
                    <div class="social-icon-login google-color"><a href="{{ url('redirect/google') }}" style="color: #fff"><i class="fab fa-google-plus-g"></i> <span>Google</span></a></div>
                </div><!-- close .registration-social-login-options -->
            </div>
        </div>

        
        
        
        
        <div class="clearfix"></div>
        <div class="form-group last-form-group-continue">
            <button class="btn btn-success" id="submitButton">Continue</button>
            <span class="checkbox-remember-pro"><input type="checkbox" required id="checkbox-terms"><label for="checkbox-terms" class="col-form-label">By clicking "Continue", you agree to our <a href="#!">Terms of Use</a> and 
                <a href="#!">Privacy Policy</a> including the use of cookies.</label></span>
                <div class="clearfix"></div>
            </div>
        </form>
        
    </div><!-- close .registration-steps-page-container -->
    
</div><!-- close .container -->
</div><!-- close #pricing-plans-background-image -->

</div><!-- close #content-pro -->
@endsection


@section('script')
<script type="text/javascript">
function checkEmail(){
  var email = $('#email_id').val();

  var CSRF_Token = $('meta[name="csrf-token"]').attr('content'); 

      $.ajax({
        type: "POST",
        url: "/checkEmail",
        data:{ _token: CSRF_Token, email: email},
        success:function(msg){
          // alert(msg)
          if(msg == 1){

            $('.emailValidate').show();
          $('.emailValidate').text('Email already exixts. Try another');  
          $('#submitButton').attr('disabled',true);
          }
          else{
            $('.emailValidate').hide();
          $('#submitButton').prop('disabled',false);
          }
        }
      });
}

function checkMobile(){
  var mobile = $('#mobile').val();

  var CSRF_Token = $('meta[name="csrf-token"]').attr('content'); 

      $.ajax({
        type: "POST",
        url: "/checkMobile",
        data:{ _token: CSRF_Token, mobile: mobile},
        success:function(msg){
          if(msg == 1){

            $('.mobileValidate').show();
          $('.mobileValidate').text('Mobile number already registred. Try another');  
          $('#submitButton').attr('disabled',true);
          }
          else{
            $('.mobileValidate').hide();
          $('#submitButton').prop('disabled',false);
          }
        }
      });
}
</script>

@3ndsection