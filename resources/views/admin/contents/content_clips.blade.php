@extends('layouts.admin')


@section('content')
<h4 class="header-title m-t-0 m-b-30">Default Example</h4>
<h4 class="header-title m-t-0 m-b-30">All Clips</h4>
@if(Session::get('success'))
<div class="alert alert-success"> {{Session::get('success')}} </div>
@endif
@if (count($errors) > 0)
<div class="alert alert-danger">
	<ul>
		@foreach ($errors->all() as $error)
		<li>{{ $error }}</li>
		@endforeach
	</ul>
</div>
@endif

<form method="POST" action="{{url('admin/store_clip/'.$id)}}" enctype="multipart/form-data">
	@csrf
	
	<div class="row input-control">
		<label class="col-sm-3">Select Type</label>
		<div class="col-sm-6">
			
			<select name='type' id="type" class="form-control">				
				<option value="trailer">Trailer</option>
				<option value="featurette">Featurette</option>
			</select>

		</div>
	</div>

	<div class="row input-control">
		<label class="col-sm-3">Link (URL)</label>
		<div class="col-sm-6">
			
			<input type="text" name="url" id="url" class="form-control">

		</div>
	</div>

	<div class="row input-control">
		<label class="col-sm-3">Title</label>
		<div class="col-sm-6">
			
			<input type="text" name="title" id="title" class="form-control">

		</div>
	</div>


	<div class="row form-group">
		<div class="col-sm-6 col-sm-offset-5">
			<button type="submit" name="save" class="btn btn-primary">Save</button> 
		</div>
	</div>

		<h4> Clips of <i>'{{ucfirst($content->title)}}'</i>: </h4>
<div class="row">
		<br><br>
		@if(!empty($content_clips))
		@foreach($content_clips as $clip)
					
					<div class="col-sm-4">
						<iframe width="auto" height="250px" src="https://www.youtube.com/embed/@if(isset($clip->external_id)){{$clip->external_id}}@endif" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
						<div style='position: absolute; top: 0; right: 0'>
							<a href="{{url('admin/delete_clip/'.$clip->content_id.'/'.$clip->clip_id)}}"><i class="fa fa-times"></i></a>
						</div>
					</div>
					@endforeach
					@endif
	</div>


</form>


@endsection
