<?php
use App\Setting;

$web_logo = Setting::where('key','website_logo')->first();
$logo = $web_logo->value ?? '';

$web_name = Setting::where('key','website_name')->first();
$webname = $web_name->value ?? '';

$web_area = Setting::where('key','area')->first();
$area = $web_area->value ?? '';

$web_city = Setting::where('key','city')->first();
$city = $web_city->value ?? '';

$web_state = Setting::where('key','state')->first();
$state = $web_state->value ?? '';

$web_pincode = Setting::where('key','pincode')->first();
$pincode = $web_pincode->value ?? '';

$web_country = Setting::where('key','country')->first();
$country = $web_country->value ?? '';

$contact_no = Setting::where('key','contact_number')->first();
$contact = $contact_no->value ?? '';

$contact_mail = Setting::where('key','contact_us_email')->first();
$mail = $contact_mail->value ?? '';

?>

<!doctype html>
<?php $type = 'Movie';?>
<html lang="en">
<head>
  <title>KHOJ</title>
  <meta charset="UTF-8">
  <meta name="description" content="">
  <meta name="keywords" content="">
  <meta name="author" content="">
  <link rel="profile" href="#">
  <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" media="all">
  <link rel="icon" href="/asset/images/logo1.png" type="image/x-icon"/>

  <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" media="all">
  <link href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css" rel="stylesheet" media="all">
  <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">

  <!-- Bootstrap bootstrap-touch-slider Slider Main Style Sheet -->
  <link href="http://bootstrapthemes.co/demo/resource/BootstrapCarouselTouchSlider/bootstrap-touch-slider.css" rel="stylesheet" media="all">
  <!--Google Font-->
  <link rel="stylesheet" href='http://fonts.googleapis.com/css?family=Dosis:400,700,500|Nunito:300,400,600' />
  <!-- Mobile specific meta -->
  <meta name=viewport content="width=device-width, initial-scale=1">
  <meta name="format-detection" content="telephone-no">

  <meta name="csrf-token" content="{{ csrf_token() }}">

  <!-- CSS files -->
  <link rel="stylesheet" href="/asset/css/plugins.css">
  <link rel="stylesheet" href="/asset/css/style.css">

  <link rel="stylesheet" href="https://unpkg.com/swiper/css/swiper.css">
  <link rel="stylesheet" href="https://unpkg.com/swiper/css/swiper.min.css">

  <script src='https://kit.fontawesome.com/a076d05399.js'></script>

  <style>
  @import url(https://fonts.googleapis.com/css?family=Lato:100,300,400,700);
  @import url(https://raw.github.com/FortAwesome/Font-Awesome/master/docs/assets/css/font-awesome.min.css);


  #wrap {
    margin: 10px 0px;
    display: inline-block;
    position: relative;
    height: 60px;
    float: right;
    padding: 0;
    position: relative;
  }

  #search {
    height: 50px;
    font-size: 16px;
    display: inline-block;
    font-family: "Lato";
    font-weight: 100;
    border: none;
    outline: none;
    color: #fff;
    padding: 3px;
    padding-right: 60px;
    width: 0px;
    position: absolute;
    top: 0;
    right: 0;
    background: none;
    z-index: 3;
    transition: width .4s cubic-bezier(0.000, 0.795, 0.000, 1.000);
    cursor: pointer;
  }

  #search:focus:hover {
    border-bottom: 1px solid #BBB;
  }

  #search:focus {
    width: 200px;
    z-index: 1;
    border-bottom: 1px solid #BBB;
    cursor: text;
  }
  #search_submit {
    height: 63px;
    width: 63px;
    display: inline-block;
    top: -3px;
    color:red;
    float: right;
    background: url(/icons/search.png) center center no-repeat;
    text-indent: -10000px;
    border: none;
    position: absolute;
    top: 0;
    right: 0;
    z-index: 2;
    cursor: pointer;
    opacity: 1;
    cursor: pointer;
    transition: opacity .4s ease;
  }

  #search_submit:hover {
    opacity: 0.8;
  }
</style>
</head>
<body style="background-color: #fff">





      <!-- Modal -->
      <div class="modal fade" id="feedback-content" tabindex="-1" role="dialog" aria-labelledby="feedback-content" aria-hidden="true" style="margin-top: 105px">

       <div class="modal-dialog modal-dialog-centered modal-md" role="document">
        <button type="button" class="close float-close-pro" data-dismiss="modal" aria-label="Close">
         <i class="fa fa-user"></i>
       </button>
       <div class="modal-content">
         <div class="modal-header-pro">
           <h2>Welcome to KHOJ</h2>
           <h6>Send us your Feedback</h6>
         </div>
         <div class="modal-body-pro social-login-modal-body-pro">
           <div class="row">

             <div class="col-sm-1"></div>

             <div class="col-sm-10">
              <div class="registration-social-login-container">
               <form method="get" id="login-form" action="{{ url('feedback') }}">
                @csrf
                <div class="form-group">
                 <input type="text" name="name" class="form-control" id="name" placeholder="Your Name" required="request">
               </div>
               <div class="form-group">
                 <input type="email" name="email" class="form-control" id="email" placeholder="Your Email" required="request">
               </div>
               <div class="form-group">
                 <input type="text" name="subject" class="form-control" id="subject" placeholder="Your Subject" required="request">
               </div>
               <div class="form-group">
                 <textarea type="text" name="message" class="form-control" id="message" placeholder="Message Here" required="request"></textarea>
                 <h6 id="username-result"></h6>
               </div>         
               <div class="form-group">           
                 <input type="submit" name="submit" value="Submit" class="btn btn-success" >
                 <input type="button" name="submit" value="Cancel" class="btn btn-danger" data-dismiss="modal" aria-label="Close">
               </div>         
             </form>     
           </div>
         </div> 
         <div class="col-sm-1"></div>


       </div><!-- close .modal-body -->
       <div class="clearfix"></div>

       <a class="not-a-member-pro" ><span>Your Feedback is important for us. </span></a>
     </div><!-- close .modal-content -->
   </div><!-- close .modal-dialog -->
 </div><!-- close .modal --> 
</div>



<!-- Modal -->
<div class="modal fade" id="feedback-success" tabindex="-1" role="dialog" aria-labelledby="feedback-content" aria-hidden="true">

 <div class="modal-dialog modal-dialog-centered modal-md" role="document" style="margin-top: 20vh">
  <button type="button" class="close float-close-pro" data-dismiss="modal" aria-label="Close">
   <i class="fa fa-user"></i>
 </button>
 <div class="modal-content">
   <div class="modal-header-pro">
     <h2> </h2>
     <!-- <h6>Send us your Feedback</h6> -->
   </div>
   <div class="modal-body-pro social-login-modal-body-pro">
     <div class="row" style="text-align: center;">

       <div class="col-sm-1"></div>

       <div class="col-sm-10">

        <h2>Thank You For Your Feedback</h2>
        <input name="submit" value="Continue" class="btn btn-danger" data-dismiss="modal" aria-label="Close" style="margin: 30px auto 45px">

      </div> 
      <div class="col-sm-1"></div>

    </div>
       <div class="clearfix"></div>

    <a class="not-a-member-pro" ><span>Your Feedback is important for us. </span></a>
  </div><!-- close .modal-content -->
</div><!-- close .modal-dialog -->
</div><!-- close .modal --> 
</div>


  <!--preloading-->
  <div id="preloader">
    <img class="logo" src="/asset/images/logo1.png" alt="" width="119" height="58">
    <div id="status">
      <span></span>
      <span></span>
    </div>
  </div>


<!-- BEGIN | Header -->
<div class="sticky">
  <header class="ht-header full-width-hd">
    <div class="row">
      <nav id="mainNav" class="navbar navbar-default navbar-custom">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header logo">
          <div class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
            <span class="sr-only">Toggle navigation</span>
            <div id="nav-icon1" onclick="showNav()">
              <span></span>
              <span></span>
              <span></span>
            </div>
          </div>

          @if(!empty($logo))
          <a href="{{url('/')}}">
            <img class="logo" src="{{url('asset/images/'.$logo)}}" alt="" width="119" height="58"></a>
            @else
            <a href="{{url('/')}}">
              <img class="logo" src="/asset/images/logo1.png" alt="" width="119" height="58"></a>
              @endif
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse flex-parent" id="bs-example-navbar-collapse-1">
              <div class="show-nav">
                <ul>
                  @if(isset(auth()->user()->id))
                  <li><a href="/profile"><span class="icon-User"></span>My Profile</a></li>
                  <li><a href="/favourites/{{Auth::user()->id}}"><span class="icon-Favorite-Window"></span>My Favorites</a></li>
                  <li><a href="{{ route('logout') }}"
                    onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();">
                    Logout
                  </a>

                  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                  </form></li>
                  @else
                  <li class=""> <a href="/new-release" class="@if(isset($tab) && $tab=='New'){{'active_nav'}}@endif">New Release</a></li>
                  <li class=""> <a href="{{url('free')}}" class="@if(isset($tab) && $tab=='Free'){{'active_nav'}}@endif">Free</a></li>
                  <li class=""> <a href="{{url('viral')}}" class="@if(isset($tab) && $tab=='Viral'){{'active_nav'}}@endif">Viral Videos</a></li>

                  <li> <a href="{{url('about-us')}}">About</a></li>
                  <li> <a href="{{url('contact-us')}}">Contact</a></li>
                  <li> <a href="{{url('privacy-policy')}}">Privacy Policy</a></li>
                  <li> <a href="{{url('frequently-asked-questions')}}">FAQ</a></li>
                  <li> <a href="/login">Sign In</a></li>
                  <li> <a href="/register">Register</a></li>
                  <li class="no-show"> <a href="{{url('free')}}">Free</a></li>
                  <li>
                    <button class="btn btn-warning" data-toggle="modal" data-target="#feedback-content">Feeback</button>
                  </li>
                  @endif
                </ul>
              </div>

              <ul class="nav navbar-nav flex-child-menu menu-left" id="full-div">
                <li class="hidden">
                  <a href="#page-top"></a>
                </li>

                <!-- For Global Search -->
                <form action="/search" autocomplete="off" method="GET" style="width: 90%;" id="window-form">

                  <div class="row">
                    <div class="col-sm-2">
                      <select class="form-control" name="searchtype" id="searchtype" style="width: 98%; display: none">
                        <option value="all" @if($type == 'all'){{'selected'}}@endif>All</option>

                      </select>
                    </div>
                    <div class="col-sm-8">
                      <input type="search" name="search" id="search1" placeholder="Search for movies, web series, TV series etc" class="form-control" style="width: 100%" value="@if(isset($keyword)){{$keyword}}@endif" onkeyup="getResult()" autocomplete="off">

                      <div style="position: absolute; z-index: 999; width: 100%; margin:0%; background-color: #000;border: 1px solid #000; box-shadow: 0 15px 20px rgba(0, 0, 0, 1); max-height: 400px; overflow-y: auto" id="dynamic-search-result">

                      </div>
                    </div>
                    <div class="col-sm-1">
                      <img src="/asset/images/search.png" class="search-dialog form-control" onclick="callSubmit()">
                    </div>
                    <div class="col-sm-1">

                    </div>
                  </div>   

                  <input id="search_submit" value="Rechercher" type="submit" style="display: none;" >

                </form>
                <!-- End for Global Search -->

              </ul>

              <ul class="nav navbar-nav flex-child-menu menu-right" id="full-div">

               <?php if (isset(auth()->user()->profile_pic))
               $profile_pic = '/images/user/'.auth()->user()->profile_pic;
               else {
                $profile_pic = '/images/default_user.png';
              }
              ?>                
              @guest
              <a href="/register" class="registerbutton">Register</a>

              <a href="/login" class="loginbutton" style="color: #fff"  role="button">Sign In</a>
              @else

              <div class="dropdown">

                <span> 
                 <div class="row" style="text-align: right;">
                  <div class="col-sm-4">
                   <img src="{{$profile_pic}}" alt="{{ Auth::user()->name }}" id="profile-image">
                 </div>
                 <div class="col-sm-8">
                  <div style="padding-top: 7px">
                   <span id="username-header">
                    {{ Auth::user()->name }} <i class="fa fa-angle-down" aria-hidden="true"></i></span>
                  </div>
                </div>
              </div>
            </span>

            <div class="dropdown-content" style="z-index: 99 !important">
              <ul class="dropdown" id="profile-option">  
                <li><a href="/profile"><span class="icon-User"></span>My Profile</a></li>
                <li><a href="/favourites/{{Auth::user()->id}}"><span class="icon-Favorite-Window"></span>My Favorites</a></li>
                {{-- <li><a href="/account"><span class="icon-Gears"></span>Account Details</a></li> --}}
                {{-- <li><a href="#!"><span class="icon-Life-Safer"></span>Help/Support</a></li> --}}
                <li><a href="{{ route('logout') }}"
                  onclick="event.preventDefault();
                  document.getElementById('logout-form').submit();">
                  Logout
                </a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                  {{ csrf_field() }}
                </form></li>
              </ul>
            </div>

          </div>

        </ul>
      </div>
      <!-- /.navbar-collapse -->
    </nav>

  </div><!-- close #header-user-profile-menu -->
</div><!-- close #header-user-profile -->

@endguest
<!-- search form -->

</div>

</header>


<div class="container-fluid response-search">
  <form action="/search" autocomplete="on" method="GET" style="width: 100%; margin: auto" id="mobile-form">
    <div class="row">
      <div class="col-xs-1">
        <select class="form-control" name="searchtype" id="searchtype2" style="width: 98%; display: none;">
          <option value="all" @if($type == 'All'){{'selected'}}@endif>All</option>
          </select>
      </div>
      <div class="col-xs-8">
       <input type="search" name="search" id="search2" placeholder="Search for movies, web series, TV series etc" class="form-control" class="search-bar" value="@if(isset($keyword)){{$keyword}}@endif" onkeyup="getResult2()" autocomplete="off">

       <div style="position: absolute; z-index: 999; width: 180%; left: -80px; margin:0%; background-color: #000;border: 1px solid #000; box-shadow: 0 15px 20px rgba(0, 0, 0, 1); height: auto" id="dynamic-search-result2">
       </div>

     </div>
     <div class="col-xs-2">
      &nbsp;<img src="/asset/images/search.png" class="reset-dialog form-group" onclick="callSubmit2()">
    </div>
    
  </div>
  <input id="search_submit2" value="Rechercher" type="submit" style="display: none;" >
</form>
</div>


<div class="container-fluid bottom-nav" @if(!empty(Auth::user())) style="margin-top: 68px;" @endif>
  <div class="row">
    
    <div class="col-sm-10 col-xs-12">
      <ul>
        <li> <a href="/" class="@if(isset($tab) && $tab=='Home'){{'active_nav'}}@endif">Home</a></li>
          <li> <a href="/trending" class="@if(isset($tab) && $tab=='Trending'){{'active_nav'}}@endif">Trending</a></li>
          <li> <a href="/upcoming" class="@if(isset($tab) && $tab=='Upcoming'){{'active_nav'}}@endif">Upcoming</a></li>
          <li> <a href="@if(isset(auth()->user()->id)){{'/watchlists/'}}{{auth()->user()->id}}@else{{url('/login')}} @endif">Watchlist</a></li>
          <li class="no-show"> <a href="/new-release" class="@if(isset($tab) && $tab=='New'){{'active_nav'}}@endif">New Release</a></li>
          <li class="no-show"> <a href="{{url('free')}}" class="@if(isset($tab) && $tab=='Free'){{'active_nav'}}@endif">Free</a></li>
          <li class="no-show"> <a href="{{url('viral')}}" class="@if(isset($tab) && $tab=='Viral'){{'active_nav'}}@endif">Viral Videos</a></li>
          <li class="" onclick="showFilter2()"> <a style="cursor: pointer;"><img src="{{asset('asset/images/filter.png')}}" style="width: 20px ; height: 20px" title="filter"/> </a></li>
        </ul>
      </div>

      <div class="col-sm-2 col-xs-2 feedback" >
       <button class="btn btn-warning" data-toggle="modal" data-target="#feedback-content">Feeback</button>
     </div>

   </div>    
 </div>
</div>







    @yield('content')




  <!-- footer v2 section-->
  <footer class="ht-footer full-width-ft">
    <div class="row">
      <div class="flex-parent-ft">
        <div class="flex-child-ft item1">
         <a href="/">
          @if(!empty($logo))
          <a href="{{url('/')}}">
          <img class="logo" src="{{url('asset/images/'.$logo)}}'" alt="" style="margin-bottom: 20px"></a>
          @else
          <a href="{{url('/')}}">
          <img class="logo" src="/asset/images/logo1.png" alt="" style="margin-bottom: 20px"></a>
          @endif
        </a>
        <h4><u>Address</u></h4>
        <p>{{$area ?? ''}}<br>
         {{$city ?? ''}}<br>
         {{$state ?? ''}} - {{$pincode ?? ''}}<br>
       </p>
       {{-- <h4><u>Call us</u></h4> --}}
       <p><u style="font-size: 16px; font-weight: bold;">Call us</u>: <a href="tel:{{$contact ?? ''}}" style="font-size: 14px">(+91) {{$contact ?? ''}}</a></p>
       <p><u style="font-size: 16px; font-weight: bold;">Email us</u>: <a href="mailto:{{$mail ?? ''}}" style="font-size: 14px">{{$mail ?? ''}}</a></p>
     </div>
     <div class="flex-child-ft item2">
      <h4>Resources</h4>
      <ul>
        <li><a href="{{url('/')}}">Home</a></li> 
        <li><a href="{{url('about-us')}}">About</a></li> 
        <li><a href="{{url('contact-us')}}">Contact Us</a></li>
        
      </ul>
    </div>
    <div class="flex-child-ft item3">
      <h4>Legal</h4>
      <ul>
        {{-- <li><a href="#">Terms of Use</a></li>  --}}
        <li><a href="{{url('privacy-policy')}}">Privacy Policy</a></li> 
        <li><a href="{{url('frequently-asked-questions')}}">FAQ</a></li>
      </ul>
    </div>
    <div class="flex-child-ft item4">
      <h4>Account</h4>
      <ul>
        <li><a href="@if(isset(auth()->user()->id)){{url('profile')}}@else{{url('login')}}@endif">My Account</a></li> 
        <li><a href="@if(isset(auth()->user()->id)){{url('watchlists')}}/{{auth()->user()->id}}@else{{url('login')}}@endif">Watchlist</a></li>  
        {{-- <li><a href="#">Collections</a></li>
        <li><a href="#">User Guide</a></li> --}}
      </ul>
    </div>
    <div class="flex-child-ft item5">
      <h4>Newsletter</h4>
      <p>Subscribe to our newsletter system now <br> to get latest news from us.</p>
      
      <input type="email" placeholder="Enter your email" name="newsletter" id="newsletter" required style="border-radius: 3px">
      
      <a style="cursor: pointer;" class="btn" onclick="clickNewsSubmit()">Subscribe now <i class="ion-ios-arrow-forward"></i></a>
      <p id="result" style="text-align: center;color: #fff; margin-top: 10px;"></p>
    </div>
  </div>
  <div class="ft-copyright">
    <div class="ft-left">
      <p>© 2019 Blockbuster. All Rights Reserved. Designed by Backstage Supporters.</p>
    </div>
    <div class="backtotop">
      <p><a href="#" id="back-to-top">Back to top  <i class="ion-ios-arrow-thin-up"></i></a></p>
    </div>
  </div>
</div>
</footer>
<!-- end of footer v2 section-->


<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#feedback-success" id="showSuccess" hidden="hidden">Feeback</button>  

  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.touchswipe/1.6.18/jquery.touchSwipe.min.js"></script>


  <!-- Bootstrap bootstrap-touch-slider Slider Main JS File -->
  <script src="http://bootstrapthemes.co/demo/resource/BootstrapCarouselTouchSlider/bootstrap-touch-slider.js"></script>

  <script type="text/javascript">
    $('#bootstrap-touch-slider').bsTouchSlider();
  </script>

  <script src="https://unpkg.com/swiper/js/swiper.js"></script>
  <script src="https://unpkg.com/swiper/js/swiper.min.js"></script>

  <script src="/asset/js/jquery.js"></script>
  <script src="/asset/js/plugins.js"></script>
  <script src="/asset/js/plugins2.js"></script>
  <script src="/asset/js/custom.js"></script>

  @yield('script')

  <script>

    @if(!empty(session()->get('feedback_success')))
      // alert('Hii');
      $('#showSuccess').click();
      @endif

    function viewInSearch(id){
      data = $('#cast'+id).text(); 
      $('#search1').val(data);
      $('#search2').val(data);
      $('#search_submit').click();    
    }

    function getSearchPage(){
      $('#search_submit').click();    
    }

    function getSearchPage2(){
      $('#search_submit2').click();    
    }

    function getResult(){
      var data = $('#search1').val();
      var search = $('#searchtype').val();
      var CSRF_Token = $('meta[name="csrf-token"]').attr('content'); 

      $.ajax({
        type: "POST",
        url: "/dynamic-search",
        data:{ _token: CSRF_Token, data: data, search: search},
        success:function(msg){
          $('#dynamic-search-result').html(msg);  
        }
      });

    }

    function getResult2(){
      var data = $('#search2').val();
      var search = $('#searchtype2').val();
      var CSRF_Token = $('meta[name="csrf-token"]').attr('content'); 

      $.ajax({
        type: "POST",
        url: "/dynamic-search",
        data:{ _token: CSRF_Token, data: data, search: search},
        success:function(msg){
          $('#dynamic-search-result2').html(msg);  
        }
      });

    }

    function callSubmit(){
      var search = $('#search1').val();
      if(search == ""){
        alert('First enter your search');
      }
      else{
        $('#search_submit').click();
      }
    }

    function callSubmit2(){
      var search = $('#search2').val();
      if(search == ""){
        alert('First enter your search');
      }
      else{
        $('#search_submit2').click();
      }
    }

    
    function clickNewsSubmit(){

      var news = $('#newsletter').val();
    // alert(news);
    if(news == ""){
      $('#result').text('First enter your email');
    }
    else{
      var CSRF_Token = $('meta[name="csrf-token"]').attr('content');
      $('#result').empty();
      $.ajax({              
        url: '/submit-newsletter',
        type: 'POST',             
        data: { _token: CSRF_Token, email : news},
        success: function (data) { 

          $('#result').text(data.message);

        },

      });
    }
  }



</script>
</body>
</html>