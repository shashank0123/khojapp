@extends('layouts.khoj_new')

<?php if (isset($_GET['page_id'])){
	$next = $_GET['page_id']+1;
	$prev = $_GET['page_id']-1;
}
else {
	$prev = '0';
	$next = '1';
}

$flag = 0;
$featureds = session()->get('featureds');

?>

@section('banner')

<div id="bootstrap-touch-slider" class="carousel bs-slider fade  control-round indicators-line" data-ride="carousel" data-pause="hover" data-interval="false" >
 <!-- Indicators -->
 <ol class="carousel-indicators">
  <li data-target="#bootstrap-touch-slider" data-slide-to="0" class="active"></li>
  <li data-target="#bootstrap-touch-slider" data-slide-to="1"></li>
  <li data-target="#bootstrap-touch-slider" data-slide-to="2"></li>
</ol>
<!-- Wrapper For Slides -->
<div class="carousel-inner" role="listbox">
  @if(!empty($featureds))
  <?php $i=0; ?>
  @foreach ($featureds as $featured)
  <!-- Third Slide -->
  <div class="item @if($i==0){{'active'}}@endif">
   <!-- Slide Background -->
   @if(isset($featured->poster_path))
   <img src="https://image.tmdb.org/t/p/w500{{$featured->poster_path}}" alt=""  class="slide-image"/>
   @else
   <img src="/images/default.png" alt="Bootstrap Touch Slider"  class="slide-image"/>
   @endif
   <div class="bs-slider-overlay"></div>
   <div class="container">
    <div class="row">
     <!-- Slide Text Layer -->
     <div class="slide-text slide_style_left headertxt">
      <h4 data-animation="animated zoomInRight">{{$featured->name}}</h4>
      <p data-animation="animated fadeInLeft">{{$featured->type}}</p>
      <a href="<?php if ($featured->type == 'Series'){?>{{'tv'}}<?php } else echo 'movie';?>_detail/{{$featured->movie_id}}" target="_blank" class="btn btn-default" data-animation="animated fadeInLeft">Watch Now</a>
      <div class="social-btn" style="margin-top: 8px;">
       <a href="<?php if ($featured->type == 'Series'){?>{{'tv'}}<?php } else echo 'movie';?>_detail/{{$featured->movie_id}}" class="parent-btn" tabindex="0"><i class="ion-play"></i> Watch Trailer</a>
       <a href="/favourite/{{$featured->movie_id}}" class="parent-btn" tabindex="0"><i class="ion-heart"></i> Add to Favorite</a>
       <div class="hover-bnt">
        <a href="#" class="parent-btn" tabindex="0"><i class="ion-android-share-alt"></i>share</a>
        <div class="hvr-item">
         <a href="#" class="hvr-grow" tabindex="0"><i class="ion-social-facebook"></i></a>
         <a href="#" class="hvr-grow" tabindex="0"><i class="ion-social-twitter"></i></a>
         <a href="#" class="hvr-grow" tabindex="0"><i class="ion-social-googleplus"></i></a>
         <a href="#" class="hvr-grow" tabindex="0"><i class="ion-social-youtube"></i></a>
       </div>
     </div>
   </div>
 </div>
</div>
</div>
</div>
<!-- End of Slide -->
<?php $i++; ?>
@endforeach
@endif
<!-- Second Slide -->

<!-- End of Slide -->
<!-- Third Slide -->

<!-- End of Slide -->  </div>
<!-- End of Wrapper For Slides -->
<!-- Left Control -->
<a class="left carousel-control" href="#bootstrap-touch-slider" role="button" data-slide="prev">
 <span class="fa fa-angle-left" aria-hidden="true"></span>
 <span class="sr-only">Previous</span>
</a>
<!-- Right Control -->
<a class="right carousel-control" href="#bootstrap-touch-slider" role="button" data-slide="next">
 <span class="fa fa-angle-right" aria-hidden="true"></span>
 <span class="sr-only">Next</span>
</a>
</div>

<button onclick="showFilter()" class="btn btn-primary open-filter">Filters 
  {{-- <img scr="{{asset('/asset/images/filter.png')}}" style="width: 20px; height: 20px"> --}}
</button>
@endsection

@section('content')

<input type="hidden" name="type" value="{{$type}}" id="type">
<input type="hidden" name="sort" value="" id="sort_filter">
<input type="hidden" name="lang" value="Holywood" id="lang_filter">
<input type="hidden" name="type" value="1" id="paginate">
<input type="hidden" name="total_pages" value="{{$total_pages}}" id="total_page">
<input type="hidden" name="genre_name" value="" id="genre_name">
<input type="hidden" name="rated" value="" id="rated">
<input type="hidden" name="predict" value="{{$predict}}" id="predict">
<input type="hidden" name="keyword" value="@if(isset($keyword)){{$keyword}}@endif" id="keyword">
<div class="col-sm-2 left-side-menu">
  <div class="left-main">
    <h4>Filters</h4> <br>
    <div class="close-filter">

      <!-- <button onclick="getReset2()" class="btn btn-default"><b><i class="fa fa-repeat"></i></b></button> -->
      <button onclick="getReset2()" class="btn btn-default"><b>RESET</b></button>
      <button onclick="hideFilter()" class="btn btn-default"><b>X</b></button>
    </div>
    <h4 class="app"><span>Select Category</span>&nbsp;&nbsp;<i class="fa fa-angle-down"></i></h4>
    <div class="checkbox" id="app">
      All<input type="radio" name="type[]" value="Movies"  @if($type == 'All'){{'checked'}}@endif onchange="getType('All')" id="resetMovie"><br>
      Movies<input type="radio" name="type[]" value="Movies"  @if($type == 'Movies'){{'checked'}}@endif onchange="getType('Movies')" id="resetMovie"><br>
      Web Series<input type="radio" name="type[]" value="Web Series" @if($type == 'Web Series'){{'checked'}}@endif onchange="getType('Web')"><br>            
      TV Serials<input type="radio" name="type[]" value="TV Serials" @if($type == 'TV Serials'){{'checked'}}@endif onchange="getType('Tv')"><br>
    </div>
    <br>

    <h4 class="year"><span>Release Year</span>&nbsp;&nbsp;<i class="fa fa-angle-down"></i></h4>
    <div class="wrapper" id="year">
      <fieldset class="filter-price">

        <div class="price-field">
          <input type="range" min="1900" max="2050" value="1900" id="lower" onchange="getYearFilter1()">
          <input type="range" min="1900" max="2050" value="2050" id="upper" onchange="getYearFilter2()">
        </div>
        <div class="price-wrap">
          {{-- <span class="price-title">FILTER</span> --}}
          <div class="price-container">
            <div class="price-wrap-1">

              <label for="one"></label>
              <input id="one" style="color: #fff!important">
            </div>
            <div class="price-wrap_line" style="color: #fff!important">-</div>
            <div class="price-wrap-2">
              <label for="two"></label>
              <input id="two" style="color: #fff!important">

            </div>
          </div>
        </div>
      </fieldset>
    </div>
    <br><br>

    <h4 class="rate"><span>Rating</span>&nbsp;&nbsp;<i class="fa fa-angle-down"></i></h4>
    <div class="wrapper" id="rate">
      <fieldset class="filter-price">

        <div class="price-field">
          <input type="range" min="0" max="10" value="0" id="lower-rate" onchange="getRateFilter1()">
          <input type="range" min="0" max="10" value="10" id="upper-rate" onchange="getRateFilter2()">
        </div>
        <div class="price-wrap">
          {{-- <span class="price-title">FILTER</span> --}}
          <div class="price-container">
            <div class="price-wrap-1">

              <label for="one-rate"></label>
              <input id="one-rate" style="color: #fff!important">
            </div>
            <div class="price-wrap_line" style="color: #fff!important">-</div>
            <div class="price-wrap-2">
              <label for="two-rate"></label>
              <input id="two-rate" style="color: #fff!important">

            </div>
          </div>
        </div>
      </fieldset>
    </div>
    <br><br>

    <h4 class="genre"><span>Genre</span>&nbsp;&nbsp;<i class="fa fa-angle-down"></i></h4>
    <div class="checkbox" id="genre">
      @if(!empty($genres))
      @foreach($genres as $gen)
      {{$gen->name}}<input type="radio" name="genre" value="{{$gen->id}}" onclick="getGenre('{{$gen->id}}')"><br>
      @endforeach
      @endif
    </div>
    <br><br>

    <h4 class="certified"><span>Rated</span>&nbsp;&nbsp;<i class="fa fa-angle-down"></i></h4>
    <div class="checkbox" id="certified">
      U<input type="radio" name="rated" id="rated_U" value="U" onclick="getRated('U')"><br>
      {{-- PG<input type="radio" name="rated" value="PG" onclick="getRated('PG')"><br> --}}
      U/A<input type="radio" name="rated" id="rated_UA" value="UA" onclick="getRated('UA')"><br>
      {{-- R<input type="radio" name="rated" value="R" onclick="getRated('R')"><br> --}}
      A<input type="radio" name="rated" value="A" id="rated_A" onclick="getRated('A')"><br>
      <br>
    </div>
    <br><br>

  </div>

</div>
<div class="col-sm-10 col-lg-10 col-xs-12 right-filter">

  <div class="movie-content" id="dynamic-search" style="height: 100%">
    @if(isset($keyword))
    <h4>Search result for "<i>{{$keyword}}</i>"&nbsp;&nbsp;
      <a href="{{url('/')}}"><u>RESET</u></a>
    </h4>
    @endif
    @if (isset($contents->results))
    <?php $space = 0; $rat=0;?>
    @foreach ($contents->results as $element)
    <?php $flag = 1; $space++;?>
    
    <div class="col-sm-3 col-lg-2 col-xs-6 search-response dynamic">

      @if($type == 'Movies')              
      <a  href="/movie/{{$element->id}}">
        @else
        <a  href="/tvseries/{{$element->id}}">
          @endif

          <div class="movie-item">
           <div class="mv-img">
            @if(isset($element->poster_path))
            <img src="https://image.tmdb.org/t/p/w300{{$element->poster_path}}" alt="Bootstrap Touch Slider"  class="slide-image web-image"/>
            @else
            <img src="/images/default.png" class="web-image" alt=""/>
            @endif
          </div>
          <div class="hvr-inner">
           {{-- <i class="fa fa-play" aria-hidden="true"></i>  --}}
         </div>
         <div class="title-in">
          
          <p>
           <i class="ion-android-star"></i>
           <span> {{$element->vote_average}}</span>/10
         </p>
       </div>
       <div class="imdb">
        <div class="row">
        @if($type == 'Movies')
          <h6><a href="/movie/{{$element->id}}"> <?php 
          echo substr($element->title,0,50); 
          // echo substr(preg_replace('/[^A-Za-z0-9 \-]/', '', $element->title),0,50); 
          ?></a></h6>
          @else
          <h6><a href="/tvseries/{{$element->id}}"><?php 
          echo substr($element->title,0,50); 
          // echo substr(preg_replace('/[^A-Za-z0-9 \-]/', '', $element->title),0,50); 
          ?></a></h6>
          @endif
        </div>
        <div class="row rate-row">
        <div class="col-sm-6 col-xs-6"><b>IMDB</b>: <span style="color: yellow">@if(isset($imdb_rating[$rat])){{$imdb_rating[$rat]}}@else{{'N/A'}}@endif</span></div>
        <div class="col-sm-3 col-xs-3">RT</div>
        <div class="col-sm-3 col-xs-3">UA</div>
      </div>
      </div>
    </div>
  </a>
</div>
<?php $rat++; ?>
@endforeach
@endif

@if($flag == 0)
<h4 class="result-sec">No result found</h4>
@endif

</div>
</div>

@endsection 

@section('paginate')

<style>
  #paginate-ul li { display: inline-block; background-color: #ddd; padding: 5px 10px; color: #000; cursor: pointer; }
  #paginate-ul li i{color: #000; }
  #paginate-ul  .active{ display: inline-block; background-color: #006699; padding: 5px 10px; color: #fff; }
  #paginate-ul { text-align: center; }
  #paginate-ul #li1,#paginate-ul #li2,#paginate-ul #li3,#paginate-ul #li4,#paginate-ul #li5{ display: none; }

</style>
@if($total_pages>0)
<div class="paginate-class" id="dynamic-paginate">
  <ul id="paginate-ul">
    <li  onclick="getFirst(1)"><i class="fa fa-arrow-left"></i></li>
    <li class="active" id="li1" >1</li>
    <li id="li2" onclick="get2()">2</li>
    <li id="li3" onclick="get3()">3</li>
    <li id="li4" onclick="get4()">4</li>
    <li id="li5" onclick="get5()">5</li>
    <li  onclick="getLast()"><i class="fa fa-arrow-right"></i></li>
  </ul>
</div>
@endif

@endsection

@section('script')

<script type="text/javascript">

  function hideFilter(){
    $('.left-side-menu').hide();
    $('.open-filter').show();
    $('.open-filter').show();
    $('.right-filter').removeClass('col-sm-10');
    $('.right-filter').removeClass('col-lg-10');
    $('.slide-image').removeClass('web-image');
    $('.slide-image').css('max-height','350px');
    $('.right-filter').addClass('col-sm-12');
    $('.right-filter').addClass('col-lg-12');
  }

  function showFilter(){
    $('.open-filter').hide();
    $('.right-filter').removeClass('col-sm-12');
    $('.right-filter').removeClass('col-lg-12');
    $('.slide-image').addClass('web-image');
    $('.slide-image').css('max-height','270px');
    $('.right-filter').addClass('col-sm-10');
    $('.right-filter').addClass('col-lg-10');
    $('.left-side-menu').show();
  }

  function getGenre(data){
   $('#genre_name').val(data);
   $('#li2').text(1);
   get2();
 }

 function getRated(value){
  $('#rated').val(value);
  $('#li2').text(1);
  get2();
}

function getYearFilter1(){
  $('#li2').text(1);
  get2();
}

function getYearFilter2(){
  $('#li2').text(1);
  get2();
}

function getRateFilter1(){
  $('#li2').text(1);
  get2();
}

function getRateFilter2(){
  $('#li2').text(1);
  get2();
}

function getType(type){
 $('#type').val(type);
 $('#li2').text(1);
 get2();
}

function getReset2()
{
  window.location.href = "/";
}

function search(){
  var CSRF_Token = $('meta[name="csrf-token"]').attr('content');

  var sort = $('#sort_filter').val();
  var type = $('#type').val();
  var page = $('#paginate').val();
  var rated = $('#rated').val();
  var genre = $('#genre_name').val();
  var search = $('#keyword').val();
  var min_year = $('#one').val();
  var max_year = $('#two').val();
  var min_rate = $('#one-rate').val();
  var max_rate = $('#two-rate').val();
  var predict = $('#predict').val();
  var lang = $('#lang_filter').val();
  var side = <?php echo "'".$side."'"; ?>;


  $.ajax({
    type: "POST",
    url: "/search-filter",
    data:{ _token: CSRF_Token, rated: rated, type: type,genre: genre, search: search, min_y : min_year, max_y : max_year, min_r : min_rate, max_r: max_rate,page: page, sort: sort,predict: predict,side: side,lang: lang},
    success:function(msg){
      $('#dynamic-search').show();      
      $('#dynamic-search').html(msg);   
      $('#paginate').val(1);
      $('#total_page').val($('#dynamic_total_page').val());
      $('#predict').val(predict);     
    }
  });
}



var lowerSlider = document.querySelector('#lower');
var  upperSlider = document.querySelector('#upper');

document.querySelector('#two').value=upperSlider.value;
document.querySelector('#one').value=lowerSlider.value;

var lowerVal = parseInt(lowerSlider.value);
var upperVal = parseInt(upperSlider.value);

upperSlider.oninput = function () {
  lowerVal = parseInt(lowerSlider.value);
  upperVal = parseInt(upperSlider.value);

  if (upperVal < lowerVal + 4) {
    lowerSlider.value = upperVal - 4;
    if (lowerVal == lowerSlider.min) {
      upperSlider.value = 4;
    }
  }
  document.querySelector('#two').value=this.value
};

lowerSlider.oninput = function () {
  lowerVal = parseInt(lowerSlider.value);
  upperVal = parseInt(upperSlider.value);
  if (lowerVal > upperVal - 4) {
    upperSlider.value = lowerVal + 4;
    if (upperVal == upperSlider.max) {
      lowerSlider.value = parseInt(upperSlider.max) - 4;
    }
  }
  document.querySelector('#one').value=this.value
}; 
</script>

<script type="text/javascript">var lowerSlider = document.querySelector('#lower-rate');
var upperSlider = document.querySelector('#upper-rate');

document.querySelector('#two-rate').value=upperSlider.value;
document.querySelector('#one-rate').value=lowerSlider.value;

var lowerVal = parseInt(lowerSlider.value);
var upperVal = parseInt(upperSlider.value);

upperSlider.oninput = function () {
  lowerVal = parseInt(lowerSlider.value);
  upperVal = parseInt(upperSlider.value);

  if (upperVal < lowerVal + 1) {
    lowerSlider.value = upperVal - 1;
    if (lowerVal == lowerSlider.min) {
      upperSlider.value = 1;
    }
  }
  document.querySelector('#two-rate').value=this.value
};

lowerSlider.oninput = function () {
  lowerVal = parseInt(lowerSlider.value);
  upperVal = parseInt(upperSlider.value);
  if (lowerVal > upperVal - 1) {
    upperSlider.value = lowerVal + 1;
    if (upperVal == upperSlider.max) {
      lowerSlider.value = parseInt(upperSlider.max) - 1;
    }
  }
  document.querySelector('#one-rate').value=this.value
};

$('.app').on('click',function(){
  $('#app').toggle();
});

$('.rate').on('click',function(){
  $('#rate').toggle();
});

$('.year').on('click',function(){
  $('#year').toggle();
});

$('.genre').on('click',function(){
  $('#genre').toggle();
});

$('.certified').on('click',function(){
  $('#certified').toggle();
});

</script>

@endsection