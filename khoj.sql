-- MySQL dump 10.13  Distrib 5.7.30, for Linux (x86_64)
--
-- Host: localhost    Database: khoj
-- ------------------------------------------------------
-- Server version	5.7.30-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `access_requests`
--

DROP TABLE IF EXISTS `access_requests`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `access_requests` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `casts`
--

DROP TABLE IF EXISTS `casts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `casts` (
  `cast_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dob` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `profile_path` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `movie_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`cast_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7483 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `content_banners`
--

DROP TABLE IF EXISTS `content_banners`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `content_banners` (
  `banner_id` int(11) NOT NULL AUTO_INCREMENT,
  `content_id` int(11) NOT NULL,
  `banner_url` varchar(250) NOT NULL,
  `display_priority` int(11) NOT NULL DEFAULT '0',
  `isLive` bit(1) NOT NULL DEFAULT b'0',
  `content_source_url` varchar(250) NOT NULL,
  PRIMARY KEY (`banner_id`,`content_id`,`banner_url`),
  UNIQUE KEY `content_id_content_source_url` (`content_id`,`content_source_url`)
) ENGINE=InnoDB AUTO_INCREMENT=135831 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `content_casting`
--

DROP TABLE IF EXISTS `content_casting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `content_casting` (
  `content_id` int(11) NOT NULL,
  `person_id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL,
  `character_name` varchar(250) DEFAULT NULL,
  `role` varchar(150) DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `poster` varchar(191) DEFAULT '',
  `exec_status` varchar(191) NOT NULL DEFAULT '0',
  `tmdb_id` varchar(191) NOT NULL DEFAULT '0',
  `priority` varchar(191) NOT NULL DEFAULT '99',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`content_id`,`person_id`,`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `content_clips`
--

DROP TABLE IF EXISTS `content_clips`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `content_clips` (
  `clip_id` int(11) NOT NULL AUTO_INCREMENT,
  `content_id` int(11) DEFAULT NULL,
  `type` varchar(50) DEFAULT NULL,
  `provider` varchar(50) DEFAULT NULL,
  `external_id` varchar(250) DEFAULT NULL,
  `display_priority` int(11) DEFAULT '0',
  `title` varchar(250) DEFAULT NULL,
  `poster` varchar(191) DEFAULT NULL,
  PRIMARY KEY (`clip_id`),
  UNIQUE KEY `content_id_type_provider_external_id` (`content_id`,`type`,`provider`,`external_id`)
) ENGINE=InnoDB AUTO_INCREMENT=22634 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `content_genres`
--

DROP TABLE IF EXISTS `content_genres`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `content_genres` (
  `content_id` int(11) NOT NULL,
  `genre_id` int(11) NOT NULL,
  PRIMARY KEY (`content_id`,`genre_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `content_providers`
--

DROP TABLE IF EXISTS `content_providers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `content_providers` (
  `content_id` int(11) NOT NULL,
  `provider_id` int(11) NOT NULL,
  `video_quality` varchar(50) NOT NULL,
  `type` varchar(50) DEFAULT NULL,
  `monetization_type` varchar(50) DEFAULT NULL,
  `retail_price` decimal(10,2) DEFAULT NULL,
  `currency` varchar(10) DEFAULT NULL,
  `web_url` varchar(1024) DEFAULT NULL,
  `deeplink_android_url` varchar(1024) DEFAULT NULL,
  `deeplink_ios_url` varchar(1024) DEFAULT NULL,
  `deeplink_android_tv_url` varchar(1024) DEFAULT NULL,
  `subtitle_languages` varchar(50) DEFAULT NULL,
  `audio_languages` varchar(50) DEFAULT NULL,
  `date_available` date DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `isLive` bit(1) DEFAULT b'1',
  `random_text` varchar(251) NOT NULL DEFAULT 'sdfhjkl',
  PRIMARY KEY (`content_id`,`provider_id`,`video_quality`,`random_text`),
  KEY `content_id` (`content_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `contents`
--

DROP TABLE IF EXISTS `contents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contents` (
  `content_id` int(11) NOT NULL AUTO_INCREMENT,
  `content_type` varchar(50) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `full_path` varchar(255) DEFAULT NULL,
  `poster` varchar(255) DEFAULT NULL,
  `short_description` text,
  `original_release_year` int(11) DEFAULT NULL,
  `original_title` varchar(255) DEFAULT NULL,
  `localized_release_date` varchar(20) DEFAULT NULL,
  `original_language` varchar(10) DEFAULT NULL,
  `runtime` int(11) DEFAULT NULL,
  `cinema_release_date` varchar(50) DEFAULT NULL,
  `cinema_release_week` varchar(10) DEFAULT NULL,
  `is_nationwide_cinema_release_date` bit(1) DEFAULT NULL,
  `age_certification` varchar(10) DEFAULT NULL,
  `imdb_score` float DEFAULT NULL,
  `tmdb_score` float DEFAULT NULL,
  `tmdb_popularity` float DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `show_id` int(11) DEFAULT NULL,
  `season_number` int(11) DEFAULT NULL,
  `total_seasons` int(11) DEFAULT NULL,
  `season_id` int(11) DEFAULT NULL,
  `episode_number` int(11) DEFAULT NULL,
  `total_episodes` int(11) DEFAULT NULL,
  `score_updated` datetime DEFAULT NULL,
  `content_source` varchar(50) NOT NULL,
  `content_source_id` varchar(50) NOT NULL,
  `content_loaded` bit(1) DEFAULT NULL,
  `click_count` bigint(20) NOT NULL DEFAULT '0',
  `execute_status` varchar(191) NOT NULL DEFAULT '0',
  `slug` varchar(255) DEFAULT NULL,
  `parent_id` varchar(191) NOT NULL DEFAULT '0',
  `justwatch_score` varchar(191) DEFAULT NULL,
  `tags` varchar(191) DEFAULT NULL,
  `meta_title` varchar(191) DEFAULT NULL,
  `meta_description` text,
  `view_type` varchar(191) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `featured` varchar(191) NOT NULL DEFAULT '0',
  `tmdb_id` varchar(45) DEFAULT '0',
  `imdb_id` varchar(45) DEFAULT NULL,
  `revenue` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`content_id`),
  UNIQUE KEY `content_type_content_source_content_source_id` (`content_type`,`content_source`,`content_source_id`)
) ENGINE=InnoDB AUTO_INCREMENT=315477 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `countries`
--

DROP TABLE IF EXISTS `countries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `countries` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `country` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `crews`
--

DROP TABLE IF EXISTS `crews`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `crews` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `movie_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cast_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `job` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6702 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `directors`
--

DROP TABLE IF EXISTS `directors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `directors` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `movie_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cast_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `episodes`
--

DROP TABLE IF EXISTS `episodes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `episodes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `episode_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `episode_desc` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `runtime` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `air_date` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `favourites`
--

DROP TABLE IF EXISTS `favourites`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `favourites` (
  `favourite_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `movie_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`favourite_id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `featured_images`
--

DROP TABLE IF EXISTS `featured_images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `featured_images` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `image_title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `poster` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `redirect_url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `display_priority` double(8,2) NOT NULL DEFAULT '5.00',
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `page_title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `feedback`
--

DROP TABLE IF EXISTS `feedback`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `feedback` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `genres`
--

DROP TABLE IF EXISTS `genres`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `genres` (
  `genre_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(250) DEFAULT NULL,
  `technical_name` varchar(150) DEFAULT NULL,
  `short_name` varchar(10) DEFAULT NULL,
  `slug` varchar(150) DEFAULT NULL,
  `content_source` varchar(50) DEFAULT NULL,
  `content_source_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`genre_id`),
  UNIQUE KEY `content_source_id` (`content_source_id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ip_requests`
--

DROP TABLE IF EXISTS `ip_requests`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ip_requests` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ipaddress` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url_visited` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `count_request` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ip_requests_ipaddress_unique` (`ipaddress`)
) ENGINE=InnoDB AUTO_INCREMENT=4484 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `languages`
--

DROP TABLE IF EXISTS `languages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `languages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `english_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `display_priority` double(8,2) NOT NULL DEFAULT '999.00',
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Active',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=188 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=82 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `movies`
--

DROP TABLE IF EXISTS `movies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `movies` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `movie_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `overview` varchar(2000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `runtime` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `release_date` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vote_average` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `poster_path` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `featured` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `genre_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `full_path` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `original_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `localized_release_date` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `original_language` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cinema_release_date` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cinema_release_week` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_nationwide_cinema_release_date` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `age_certification` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `imdb_score` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tmdb_score` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tmdb_popularity` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_modified` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `show_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `season_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `total_seasons` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `season_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `episode_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `total_episodes` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `score_updated` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content_source` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content_loaded` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `newsletters`
--

DROP TABLE IF EXISTS `newsletters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `newsletters` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Inactive',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `notifications`
--

DROP TABLE IF EXISTS `notifications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notifications` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notification` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `persons`
--

DROP TABLE IF EXISTS `persons`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `persons` (
  `person_id` int(11) NOT NULL AUTO_INCREMENT,
  `full_name` varchar(150) NOT NULL,
  `tmdb_popularity` float DEFAULT NULL,
  `date_of_birth` date DEFAULT NULL,
  `short_description` text,
  `modified_date` datetime DEFAULT NULL,
  `content_source` varchar(50) DEFAULT NULL,
  `content_source_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`person_id`),
  UNIQUE KEY `content_source_id` (`content_source_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1107661 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `playlists`
--

DROP TABLE IF EXISTS `playlists`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `playlists` (
  `playlist_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `movie_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `playlist_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`playlist_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `profiles`
--

DROP TABLE IF EXISTS `profiles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `profiles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gender` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `genres` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `providers`
--

DROP TABLE IF EXISTS `providers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `providers` (
  `provider_id` int(11) NOT NULL AUTO_INCREMENT,
  `profile_id` int(11) DEFAULT NULL,
  `title` varchar(150) DEFAULT NULL,
  `technical_name` varchar(150) DEFAULT NULL,
  `short_name` varchar(10) DEFAULT NULL,
  `slug` varchar(150) DEFAULT NULL,
  `display_priority` int(11) DEFAULT NULL,
  `icon_url` varchar(150) DEFAULT NULL,
  `content_source` varchar(50) DEFAULT NULL,
  `content_source_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`provider_id`),
  UNIQUE KEY `content_source_id` (`content_source_id`)
) ENGINE=InnoDB AUTO_INCREMENT=733 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `public_playlists`
--

DROP TABLE IF EXISTS `public_playlists`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `public_playlists` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `playlist_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `playlist_content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `playlist_order` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `poster` varchar(245) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ratings`
--

DROP TABLE IF EXISTS `ratings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ratings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `movie_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rating` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `review` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `recommendations`
--

DROP TABLE IF EXISTS `recommendations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `recommendations` (
  `recommendation_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `movie_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `reason` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`recommendation_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `reminders`
--

DROP TABLE IF EXISTS `reminders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reminders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `completed` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `search_filters`
--

DROP TABLE IF EXISTS `search_filters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `search_filters` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'web',
  `filters` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `search_keywords`
--

DROP TABLE IF EXISTS `search_keywords`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `search_keywords` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keyword` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `exec_status` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2347 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `seos`
--

DROP TABLE IF EXISTS `seos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `seos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `page_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Home',
  `page_tags` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `page_title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `page_description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sessions`
--

DROP TABLE IF EXISTS `sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sessions` (
  `id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_agent` text COLLATE utf8mb4_unicode_ci,
  `payload` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_activity` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `sessions_id_unique` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `settings`
--

DROP TABLE IF EXISTS `settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `test`
--

DROP TABLE IF EXISTS `test`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `test` (
  `idtest` int(11) NOT NULL AUTO_INCREMENT,
  `testcol` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`idtest`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_preferences`
--

DROP TABLE IF EXISTS `user_preferences`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_preferences` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `prefered_genres` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `prefered_movies` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `prefered_language` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile` varchar(127) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `google` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `facebook` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `profile_pic` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'user',
  `provider` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  UNIQUE KEY `users_mobile_unique` (`mobile`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `watch_lists`
--

DROP TABLE IF EXISTS `watch_lists`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `watch_lists` (
  `watchlist_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `movie_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`watchlist_id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping routines for database 'khoj'
--
/*!50003 DROP PROCEDURE IF EXISTS `ret_home` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `ret_home`(p_genre_id INT, p_content_type VARCHAR(50), p_from_release_year VARCHAR(30), p_to_release_year VARCHAR(30), p_age_certification VARCHAR(10), p_original_language VARCHAR(10), p_start INT, p_max INT, p_provider_id INT, p_sort_by VARCHAR(50), p_order VARCHAR(10))
BEGIN
  
  DECLARE l_keyword VARCHAR(100);
  DECLARE l_len INT;
  DECLARE l_record_cnt INT;
  DECLARE p_keyword VARCHAR(100);
  SET p_keyword = '';
  
  CREATE TEMPORARY TABLE IF NOT EXISTS tmp_content_provider_h(content_id INT);
  TRUNCATE TABLE tmp_content_provider_h;
  
  CREATE TEMPORARY TABLE IF NOT EXISTS tmp_content_genres_h(content_id INT, genre_id INT);
  TRUNCATE TABLE tmp_content_genres_h;
  
  CREATE TEMPORARY TABLE IF NOT EXISTS tmp_contents_h(content_id INT, priority INT);
  TRUNCATE TABLE tmp_contents_h;
  
  CREATE TEMPORARY TABLE IF NOT EXISTS tmp_priority_h(content_id INT, priority INT);
  TRUNCATE TABLE tmp_priority_h;
  
  CREATE TEMPORARY TABLE IF NOT EXISTS tmp_ids_h(content_id INT, priority INT);
  TRUNCATE TABLE tmp_ids_h;
  
  SET l_len = LENGTH(p_keyword);
  SET l_keyword = CONCAT('%', p_keyword, '%');
  
  IF p_genre_id <> 0 THEN
    INSERT INTO tmp_content_genres_h(content_id, genre_id)
    SELECT content_id, genre_id FROM content_genres WHERE genre_id = CASE WHEN p_genre_id = 0 THEN genre_id ELSE p_genre_id END;
  END IF;
  
  IF p_provider_id <> 0 THEN
    INSERT INTO tmp_content_provider_h(content_id)
    SELECT DISTINCT content_id FROM content_providers WHERE provider_id = p_provider_id;
  END IF;
  
  
      
  INSERT INTO tmp_contents_h(content_id, priority)
  SELECT DISTINCT A.content_id, A.featured FROM contents A LEFT OUTER JOIN content_casting B ON (A.content_id = B.content_id)
  WHERE A.original_release_year >= p_from_release_year
  AND   A.original_release_year <= p_to_release_year
  AND   IFNULL(A.age_certification,'') = CASE WHEN p_age_certification = 'ALL' THEN IFNULL(A.age_certification,'') ELSE p_age_certification END
  AND   IFNULL(A.original_language,'') = CASE WHEN p_original_language = 'ALL' THEN IFNULL(A.original_language,'') ELSE p_original_language END
  AND   IFNULL(A.content_type,'') = CASE WHEN p_content_type = 'ALL' THEN IFNULL(A.content_type,'') ELSE p_content_type END
  AND   IFNULL(A.content_type,'') IN ('movie','show'); 
  
  IF (p_provider_id = 0 AND p_genre_id = 0) THEN
    INSERT INTO tmp_ids_h(content_id, priority)
  SELECT DISTINCT A.content_id, A.priority
    FROM tmp_contents_h A LEFT OUTER JOIN tmp_content_genres_h B ON (A.content_id = B.content_id)
                        LEFT OUTER JOIN tmp_content_provider_h C ON (A.content_id = C.content_id);
  ELSEIF (p_provider_id <> 0 AND p_genre_id = 0) THEN
    INSERT INTO tmp_ids_h(content_id, priority)
  SELECT DISTINCT A.content_id, A.priority
    FROM tmp_contents_h A LEFT OUTER JOIN tmp_content_genres_h B ON (A.content_id = B.content_id)
                        JOIN tmp_content_provider_h C ON (A.content_id = C.content_id);
  ELSEIF (p_provider_id = 0 AND p_genre_id <> 0) THEN
    INSERT INTO tmp_ids_h(content_id, priority)
  SELECT DISTINCT A.content_id, A.priority
    FROM tmp_contents_h A INNER JOIN tmp_content_genres_h B ON (A.content_id = B.content_id)
                        LEFT OUTER JOIN tmp_content_provider_h C ON (A.content_id = C.content_id);
  ELSE
    INSERT INTO tmp_ids_h(content_id, priority)
  SELECT DISTINCT A.content_id, A.priority
    FROM tmp_contents_h A INNER JOIN tmp_content_genres_h B ON (A.content_id = B.content_id)
                        INNER JOIN tmp_content_provider_h C ON (A.content_id = C.content_id);
  END IF;
 
  
  INSERT INTO tmp_priority_h(content_id, priority)
  SELECT A.content_id, CASE WHEN IFNULL(A.priority,0) = 0 THEN 20 ELSE A.priority END
  FROM tmp_ids_h A;
  
  TRUNCATE TABLE tmp_ids_h;
  
  INSERT INTO tmp_ids_h(content_id)
  SELECT content_id FROM tmp_priority_h WHERE priority = 20;
  
  DELETE FROM tmp_priority_h WHERE priority = 20;
  
  INSERT INTO tmp_priority_h(content_id, priority)
  SELECT A.content_id, MIN(CASE WHEN A.role = 'ACTOR' THEN CASE WHEN UPPER(A.name) = UPPER(p_keyword) THEN 20 ELSE 30 END 
                                WHEN A.role = 'PRODUCER' THEN CASE WHEN UPPER(A.name) = UPPER(p_keyword) THEN 40 ELSE 50 END 
                                WHEN A.role = 'DIRECTOR' THEN CASE WHEN UPPER(A.name) = UPPER(p_keyword) THEN 60 ELSE 70 END 
                                ELSE CASE WHEN A.name = UPPER(p_keyword) THEN 80 ELSE 90 END END) AS priority 
  FROM content_casting A, tmp_ids_h B 
  WHERE A.content_id = B.content_id
  GROUP BY A.content_id
  ORDER BY 1;
  
  
  
  SELECT COUNT(1) INTO l_record_cnt FROM tmp_priority_h;
  
  IF UPPER(p_sort_by) = UPPER('relevance') THEN
    SELECT  A.*, l_record_cnt AS record_cnt, B.priority FROM contents A, tmp_priority_h B 
    WHERE A.content_id = B.content_id 
    ORDER BY B.priority, A.imdb_score DESC, A.click_count DESC, A.original_release_year DESC
    LIMIT p_start, p_max;
  ELSEIF UPPER(p_sort_by) = UPPER('rating') AND UPPER(p_order) = UPPER('DESC') THEN
    SELECT  A.*, l_record_cnt AS record_cnt, B.priority FROM contents A, tmp_priority_h B 
    WHERE A.content_id = B.content_id 
    ORDER BY A.imdb_score DESC, B.priority, A.click_count DESC, A.original_release_year DESC
    LIMIT p_start, p_max;
  ELSEIF UPPER(p_sort_by) = UPPER('rating') AND UPPER(p_order) = UPPER('ASC') THEN
    SELECT  A.*, l_record_cnt AS record_cnt, B.priority FROM contents A, tmp_priority_h B 
    WHERE A.content_id = B.content_id 
    ORDER BY A.imdb_score ASC, B.priority, A.click_count DESC, A.original_release_year DESC
    LIMIT p_start, p_max;
  ELSEIF UPPER(p_sort_by) = UPPER('popularity') AND UPPER(p_order) = UPPER('DESC') THEN
    SELECT  A.*, l_record_cnt AS record_cnt, B.priority FROM contents A, tmp_priority_h B 
    WHERE A.content_id = B.content_id 
    ORDER BY A.click_count DESC, B.priority, A.imdb_score DESC, A.original_release_year DESC
    LIMIT p_start, p_max;
  ELSEIF UPPER(p_sort_by) = UPPER('popularity') AND UPPER(p_order) = UPPER('ASC') THEN
    SELECT  A.*, l_record_cnt AS record_cnt, B.priority FROM contents A, tmp_priority_h B 
    WHERE A.content_id = B.content_id 
    ORDER BY A.click_count ASC, B.priority, A.imdb_score DESC, A.original_release_year DESC
    LIMIT p_start, p_max;
  ELSEIF UPPER(p_sort_by) = UPPER('Release_year') AND UPPER(p_order) = UPPER('DESC') THEN
    SELECT  A.*, l_record_cnt AS record_cnt, B.priority FROM contents A, tmp_priority_h B 
    WHERE A.content_id = B.content_id 
    ORDER BY A.original_release_year DESC, B.priority, A.imdb_score DESC, A.click_count DESC
    LIMIT p_start, p_max;
  ELSEIF UPPER(p_sort_by) = UPPER('Release_year') AND UPPER(p_order) = UPPER('ASC') THEN
    SELECT  A.*, l_record_cnt AS record_cnt, B.priority FROM contents A, tmp_priority_h B 
    WHERE A.content_id = B.content_id 
    ORDER BY A.original_release_year ASC, B.priority, A.imdb_score DESC, A.click_count DESC
    LIMIT p_start, p_max;
  END IF;
  
  DROP TABLE tmp_content_genres_h;
  DROP TABLE tmp_contents_h;
  DROP TABLE tmp_ids_h;
  DROP TABLE tmp_priority_h;
  DROP TABLE tmp_content_provider_h;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `ret_search` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `ret_search`(p_keyword VARCHAR(100), p_genre_id INT, p_content_type VARCHAR(50), p_from_release_year VARCHAR(30), p_to_release_year VARCHAR(30), p_age_certification VARCHAR(10), p_original_language VARCHAR(10), p_start INT, p_max INT, p_person_id INT, p_provider_id INT, p_sort_by VARCHAR(50), p_order VARCHAR(10))
BEGIN
  
  DECLARE l_keyword VARCHAR(100);
  DECLARE l_len INT;
  DECLARE l_record_cnt INT;
  
  CREATE TEMPORARY TABLE IF NOT EXISTS tmp_content_provider(content_id INT);
  TRUNCATE TABLE tmp_content_provider;
  
  CREATE TEMPORARY TABLE IF NOT EXISTS tmp_content_genres(content_id INT, genre_id INT);
  TRUNCATE TABLE tmp_content_genres;
  
  CREATE TEMPORARY TABLE IF NOT EXISTS tmp_contents(content_id INT);
  TRUNCATE TABLE tmp_contents;
  
  CREATE TEMPORARY TABLE IF NOT EXISTS tmp_priority(content_id INT, priority INT);
  TRUNCATE TABLE tmp_priority;
  
  CREATE TEMPORARY TABLE IF NOT EXISTS tmp_ids(content_id INT);
  TRUNCATE TABLE tmp_ids;
  
  SET l_len = LENGTH(p_keyword);
  SET l_keyword = CONCAT('%', p_keyword, '%');
  
  IF p_genre_id <> 0 THEN
    INSERT INTO tmp_content_genres(content_id, genre_id)
    SELECT content_id, genre_id FROM content_genres WHERE genre_id = CASE WHEN p_genre_id = 0 THEN genre_id ELSE p_genre_id END;
  END IF;
  
  IF p_provider_id <> 0 THEN
    INSERT INTO tmp_content_provider(content_id)
    SELECT DISTINCT content_id FROM content_providers WHERE provider_id = p_provider_id;
  END IF;
  
  IF p_person_id = 0 THEN
      
    INSERT INTO tmp_contents(content_id)
    SELECT DISTINCT A.content_id FROM contents A LEFT OUTER JOIN content_casting B ON (A.content_id = B.content_id)
    WHERE A.original_release_year >= p_from_release_year
    AND   A.original_release_year <= p_to_release_year
    AND   IFNULL(A.age_certification,'') = CASE WHEN p_age_certification = 'ALL' THEN IFNULL(A.age_certification,'') ELSE p_age_certification END
    AND   IFNULL(A.original_language,'') = CASE WHEN p_original_language = 'ALL' THEN IFNULL(A.original_language,'') ELSE p_original_language END
    AND   IFNULL(A.content_type,'') = CASE WHEN p_content_type = 'ALL' THEN IFNULL(A.content_type,'') ELSE p_content_type END
    AND   IFNULL(A.content_type,'') IN ('movie','show')
    AND   (A.title LIKE l_keyword OR A.tags LIKE l_keyword OR B.name LIKE l_keyword); 
    
    IF p_provider_id = 0 THEN
      INSERT INTO tmp_ids(content_id)
    SELECT DISTINCT A.content_id 
      FROM tmp_contents A LEFT OUTER JOIN tmp_content_genres B ON (A.content_id = B.content_id)
                          LEFT OUTER JOIN tmp_content_provider C ON (A.content_id = C.content_id);
    ELSE
      INSERT INTO tmp_ids(content_id)
    SELECT DISTINCT A.content_id 
      FROM tmp_contents A LEFT OUTER JOIN tmp_content_genres B ON (A.content_id = B.content_id)
                          JOIN tmp_content_provider C ON (A.content_id = C.content_id);
    END IF;
  ELSE
    INSERT INTO tmp_contents(content_id)
    SELECT DISTINCT A.content_id FROM contents A, content_casting B
    WHERE A.original_release_year >= p_from_release_year
    AND   A.original_release_year <= p_to_release_year
    AND   IFNULL(A.age_certification,'') = CASE WHEN p_age_certification = 'ALL' THEN IFNULL(A.age_certification,'') ELSE p_age_certification END
    AND   IFNULL(A.original_language,'') = CASE WHEN p_original_language = 'ALL' THEN IFNULL(A.original_language,'') ELSE p_original_language END
    AND   IFNULL(A.content_type,'') = CASE WHEN p_content_type = 'ALL' THEN IFNULL(A.content_type,'') ELSE p_content_type END
    AND   IFNULL(A.content_type,'') IN ('movie','show')
    AND   A.content_id = B.content_id
    AND   B.person_id = p_person_id; 
    
    IF (p_provider_id = 0 AND p_genre_id = 0) THEN
      INSERT INTO tmp_ids(content_id)
    SELECT DISTINCT A.content_id 
      FROM tmp_contents A LEFT OUTER JOIN tmp_content_genres B ON (A.content_id = B.content_id)
                          LEFT OUTER JOIN tmp_content_provider C ON (A.content_id = C.content_id);
    ELSEIF (p_provider_id <> 0 AND p_genre_id = 0) THEN
      INSERT INTO tmp_ids(content_id)
    SELECT DISTINCT A.content_id 
      FROM tmp_contents A LEFT OUTER JOIN tmp_content_genres B ON (A.content_id = B.content_id)
                          JOIN tmp_content_provider C ON (A.content_id = C.content_id);
    ELSEIF (p_provider_id = 0 AND p_genre_id <> 0) THEN
      INSERT INTO tmp_ids(content_id)
    SELECT DISTINCT A.content_id 
      FROM tmp_contents A INNER JOIN tmp_content_genres B ON (A.content_id = B.content_id)
                          LEFT OUTER JOIN tmp_content_provider C ON (A.content_id = C.content_id);
    ELSE
      INSERT INTO tmp_ids(content_id)
    SELECT DISTINCT A.content_id 
      FROM tmp_contents A INNER JOIN tmp_content_genres B ON (A.content_id = B.content_id)
                          INNER JOIN tmp_content_provider C ON (A.content_id = C.content_id);
    END IF;
    
    SELECT name INTO p_keyword FROM content_casting WHERE person_id = p_person_id LIMIT 1;
    
  END IF;
  
  
  
  INSERT INTO tmp_priority(content_id, priority)
  SELECT A.content_id, CASE WHEN UPPER(B.title) = UPPER(p_keyword) THEN 0 WHEN UPPER(LEFT(B.title, l_len)) = UPPER(p_keyword) THEN 1 ELSE 2 END AS priority
  FROM tmp_ids A, contents B
  WHERE A.content_id = B.content_id;
  
  TRUNCATE TABLE tmp_ids;
  
  INSERT INTO tmp_ids(content_id)
  SELECT content_id FROM tmp_priority WHERE priority = 2;
  
  DELETE FROM tmp_priority WHERE priority = 2;
  
  INSERT INTO tmp_priority(content_id, priority)
  SELECT A.content_id, MIN(CASE WHEN A.role = 'ACTOR' THEN CASE WHEN UPPER(A.name) = UPPER(p_keyword) THEN 2 ELSE 3 END 
                                WHEN A.role = 'PRODUCER' THEN CASE WHEN UPPER(A.name) = UPPER(p_keyword) THEN 4 ELSE 5 END 
                                WHEN A.role = 'DIRECTOR' THEN CASE WHEN UPPER(A.name) = UPPER(p_keyword) THEN 6 ELSE 7 END 
                                ELSE CASE WHEN A.name = UPPER(p_keyword) THEN 8 ELSE 9 END END) AS priority 
  FROM content_casting A, tmp_ids B 
  WHERE A.content_id = B.content_id
  GROUP BY A.content_id
  ORDER BY 1;
  
  
  
  SELECT COUNT(1) INTO l_record_cnt FROM tmp_priority;
  
  IF UPPER(p_sort_by) = UPPER('relevance') THEN
    SELECT  A.*, l_record_cnt AS record_cnt FROM contents A, tmp_priority B 
    WHERE A.content_id = B.content_id 
    ORDER BY B.priority, A.imdb_score DESC, A.click_count DESC, A.original_release_year DESC
    LIMIT p_start, p_max;
  ELSEIF UPPER(p_sort_by) = UPPER('rating') AND UPPER(p_order) = UPPER('DESC') THEN
    SELECT  A.*, l_record_cnt AS record_cnt FROM contents A, tmp_priority B 
    WHERE A.content_id = B.content_id 
    ORDER BY A.imdb_score DESC, B.priority, A.click_count DESC, A.original_release_year DESC
    LIMIT p_start, p_max;
  ELSEIF UPPER(p_sort_by) = UPPER('rating') AND UPPER(p_order) = UPPER('ASC') THEN
    SELECT  A.*, l_record_cnt AS record_cnt FROM contents A, tmp_priority B 
    WHERE A.content_id = B.content_id 
    ORDER BY A.imdb_score ASC, B.priority, A.click_count DESC, A.original_release_year DESC
    LIMIT p_start, p_max;
  ELSEIF UPPER(p_sort_by) = UPPER('popularity') AND UPPER(p_order) = UPPER('DESC') THEN
    SELECT  A.*, l_record_cnt AS record_cnt FROM contents A, tmp_priority B 
    WHERE A.content_id = B.content_id 
    ORDER BY A.click_count DESC, B.priority, A.imdb_score DESC, A.original_release_year DESC
    LIMIT p_start, p_max;
  ELSEIF UPPER(p_sort_by) = UPPER('popularity') AND UPPER(p_order) = UPPER('ASC') THEN
    SELECT  A.*, l_record_cnt AS record_cnt FROM contents A, tmp_priority B 
    WHERE A.content_id = B.content_id 
    ORDER BY A.click_count ASC, B.priority, A.imdb_score DESC, A.original_release_year DESC
    LIMIT p_start, p_max;
  ELSEIF UPPER(p_sort_by) = UPPER('Release_year') AND UPPER(p_order) = UPPER('DESC') THEN
    SELECT  A.*, l_record_cnt AS record_cnt FROM contents A, tmp_priority B 
    WHERE A.content_id = B.content_id 
    ORDER BY A.original_release_year DESC, B.priority, A.imdb_score DESC, A.click_count DESC
    LIMIT p_start, p_max;
  ELSEIF UPPER(p_sort_by) = UPPER('Release_year') AND UPPER(p_order) = UPPER('ASC') THEN
    SELECT  A.*, l_record_cnt AS record_cnt FROM contents A, tmp_priority B 
    WHERE A.content_id = B.content_id 
    ORDER BY A.original_release_year ASC, B.priority, A.imdb_score DESC, A.click_count DESC
    LIMIT p_start, p_max;
  END IF;
  
  DROP TABLE tmp_content_genres;
  DROP TABLE tmp_contents;
  DROP TABLE tmp_ids;
  DROP TABLE tmp_priority;
  DROP TABLE tmp_content_provider;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `ret_search_help` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `ret_search_help`(p_keyword VARCHAR(100), p_content_type VARCHAR(50), p_limit INT)
BEGIN
  
  
  DECLARE l_keyword VARCHAR(150);
  
  CREATE TEMPORARY TABLE IF NOT EXISTS tmp_content(content_id INT, title VARCHAR(255), original_title VARCHAR(255), original_release_year INT, poster VARCHAR(255), content_type VARCHAR(50), priority INT) DEFAULT CHARSET=utf8;
  TRUNCATE TABLE tmp_content;
  
  CREATE TEMPORARY TABLE IF NOT EXISTS tmp_contentF(content_id INT, title VARCHAR(255), original_title VARCHAR(255), original_release_year INT, poster VARCHAR(255), content_type VARCHAR(50)) DEFAULT CHARSET=utf8;
  TRUNCATE TABLE tmp_contentF;
  
  CREATE TEMPORARY TABLE IF NOT EXISTS tmp_person(person_id INT, name VARCHAR(255)) DEFAULT CHARSET=utf8;
  TRUNCATE TABLE tmp_person;
  
  SET l_keyword = CONCAT('%',p_keyword, '%');
  
  INSERT INTO tmp_content(content_id, title, original_title, original_release_year, poster, content_type, priority)
  SELECT DISTINCT A.content_id, A.title, A.original_title, A.original_release_year, A.poster, A.content_type, 1 AS priority 
  FROM contents A
  WHERE A.title LIKE l_keyword
  AND   IFNULL(A.content_type,'') IN ('movie','show')
  AND   IFNULL(A.content_type,'') = CASE WHEN p_content_type = 'ALL' THEN IFNULL(A.content_type,'') ELSE p_content_type END;
  
  IF LENGTH(p_keyword) >= 5 THEN
    INSERT INTO tmp_content(content_id, title, original_title, original_release_year, poster, content_type, priority)
    SELECT DISTINCT A.content_id, A.title, A.original_title, A.original_release_year, A.poster, A.content_type, 2 AS priority 
    FROM contents A, content_casting B
    WHERE B.content_id = A.content_id 
    AND B.name = p_keyword
    AND   IFNULL(A.content_type,'') IN ('movie','show')
    AND   IFNULL(A.content_type,'') = CASE WHEN p_content_type = 'ALL' THEN IFNULL(A.content_type,'') ELSE p_content_type END;
  END IF;
  INSERT INTO tmp_contentF(content_id, title, original_title, original_release_year, poster, content_type)
  SELECT content_id, title, original_title, original_release_year, poster, content_type FROM tmp_content ORDER BY priority, title LIMIT p_limit;
  
  INSERT INTO tmp_person(person_id, name)
  SELECT DISTINCT person_id, name FROM content_casting WHERE name LIKE l_keyword ORDER BY 2 LIMIT p_limit;
  
  SELECT CONCAT( 'content,', content_id, ',', title, ',', IFNULL(original_title,''), ',', original_release_year, ',', poster, ',', content_type) AS result FROM tmp_contentF
  UNION
  SELECT CONCAT( 'person,', person_id,  ',', name) AS result FROM tmp_person;
  
  DROP TABLE tmp_content;
  DROP TABLE tmp_person;
  DROP TABLE tmp_contentF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `upg_title_language` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `upg_title_language`()
BEGIN

  

  DECLARE l_code VARCHAR(20);
  DECLARE l_english_name VARCHAR(50);
  DECLARE l_name_patern VARCHAR(50);
  DECLARE l_name_replace VARCHAR(50);
  
  DECLARE l_content_id BIGINT;
  DECLARE l_title TEXT;
  DECLARE l_original_language VARCHAR(50);
  
  DECLARE no_more_rows BOOLEAN; 
  DECLARE loop_cntr INT DEFAULT 0; 
  DECLARE num_rows_l INT DEFAULT 0; 
  DECLARE num_rows_c INT DEFAULT 0; 

  DECLARE cur_languahe CURSOR FOR 
  SELECT code, english_name FROM languages ORDeR BY 1;
  
  DECLARE cur_content CURSOR FOR 
  SELECT content_id, LEFT(title,100) AS title, original_language FROM contents WHERE title LIKE l_name_patern OR original_language = l_english_name ORDER By 1;

  DECLARE CONTINUE HANDLER FOR NOT FOUND SET no_more_rows = TRUE; 
  
  CREATE TEMPORARY TABLE IF NOT EXISTS tmp_raw(code VARCHAR(20), english_name VARCHAR(50), content_id BIGINT, title TEXT, original_language VARCHAR(50));
  TRUNCATE TABLE tmp_raw;
  
  OPEN cur_languahe; 
  SELECT FOUND_ROWS() INTO num_rows_l; 
  lbl_l: LOOP 
    IF num_rows_l = 0 THEN 
      LEAVE lbl_l; 
    END IF; 
    SET num_rows_l = num_rows_l - 1;
    FETCH cur_languahe INTO l_code, l_english_name; 
    
    SET l_name_patern = CONCAT('%(',l_english_name,')%');
    SET l_name_replace = CONCAT('(',l_english_name,')');
    OPEN cur_content; 
    SELECT FOUND_ROWS() INTO num_rows_c; 
    lbl_c: LOOP 
      IF num_rows_c = 0 THEN 
        LEAVE lbl_c; 
      END IF; 
      SET num_rows_c = num_rows_c - 1;
      FETCH cur_content INTO l_content_id, l_title, l_original_language;
      
      INSERT INTO tmp_raw
      SELECT l_code, l_english_name, l_content_id, l_title, l_original_language;
      
      UPDATE contents SET original_language = l_code,
                          title = replace(title, l_name_replace, '')
      WHERE content_id = l_content_id;
      
    END LOOP; 
    CLOSE cur_content;
  
    
  END LOOP; 
  CLOSE cur_languahe;
  
  SELECT * FROM tmp_raw;
  
  DROP TABLE tmp_raw;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-06-26 15:25:12
