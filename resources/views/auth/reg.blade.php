@extends('registernew')

@section('content')
<div id="content-pro">                   
            <div id="pricing-plans-background-image">
                <div class="container">
                        <div class="registration-steps-page-container">
                            
                         <form class="registration-steps-form" method="POST" action="{{ route('register') }}">
                            @csrf
                            <div class="row">
                                <div class="col-sm-8">
                                    <div class="registration-social-login-container">                             

                                 <div class="form-group">
                                     <label for="full-name" class="col-form-label">Full Name</label>
                                     <input type="text" name="name" class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }}" id="full-name" value="{{ old('name') }}" required autofocus placeholder="John Doe">
                                      @error('name')
                                          <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                          </span>
                                          @enderror
                                 </div>
                                 <div class="form-group">
                                     <label for="email" class="col-form-label">Email</label>
                                     <input type="text" name="email" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" id="email" value="{{ old('enail') }}" placeholder="">
                                     @error('email')
                                          <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                          </span>
                                          @enderror
                                 </div>
                                 <div class="form-group">
                                     
                                         <label for="password" class="col-form-label">Password</label>
                                         <input type="password" name="password" class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}" id="password" value="{{ old('password') }}" placeholder="&middot;&middot;&middot;&middot;&middot;&middot;">
                                        </div>
                                        <div class="form-group">
                                         <label for="confirm-password" class="col-form-label">&nbsp;</label>
                                         <input type="password" name="password_confirmation" class="form-control" id="confirm-password" placeholder="Confirm Password">
                                         @error('password')
                                          <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                          </span>
                                          @enderror
                                        </div>
                                    
                                
                                 
                                 <div class="registration-social-login-or">or</div>
                                 
                             </div><!-- close .registration-social-login-container -->
                                </div>
                                <div class="col-sm-4">
                                     <div class="registration-social-login-options">
                                <h6>Sign up with one of your social accounts</h6>
                                <div class="social-icon-login facebook-color"><i class="fab fa-facebook-f"></i> <span>Facebook</span></div>
                                <div class="social-icon-login twitter-color"><i class="fab fa-twitter"></i> Twitter</div>
                                <div class="social-icon-login google-color"><i class="fab fa-google-plus-g"></i> Google</div>
                             </div><!-- close .registration-social-login-options -->
                                </div>
                            </div>

                             
                             
                            
                             
                             <div class="clearfix"></div>
                             <div class="form-group last-form-group-continue">
                                <button class="btn btn-success">Continue</button>
                                 <span class="checkbox-remember-pro"><input type="checkbox" id="checkbox-terms"><label for="checkbox-terms" class="col-form-label">By clicking "Continue", you agree to our <a href="#!">Terms of Use</a> and 
                                        <a href="#!">Privacy Policy</a> including the use of cookies.</label></span>
                                 <div class="clearfix"></div>
                             </div>
                         </form>
                            
                        </div><!-- close .registration-steps-page-container -->
                    
                </div><!-- close .container -->
            </div><!-- close #pricing-plans-background-image -->
            
        </div><!-- close #content-pro -->
@endsection
