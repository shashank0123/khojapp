@extends('layouts.vodi')
@section('contents')
<div id="content" class="site-content" tabindex="-1">
	<div class="container"><nav class="masvideos-breadcrumb">
		<a href="https://demo3.madrasthemes.com/vodi-demos/main">Home</a><span class="delimiter"><svg width="4px" height="7px"><path fill-rule="evenodd" d="M3.978,3.702 C3.986,3.785 3.966,3.868 3.903,3.934 L1.038,6.901 C0.920,7.022 0.724,7.029 0.598,6.916 L0.143,6.506 C0.017,6.393 0.010,6.203 0.127,6.082 L2.190,3.945 C2.276,3.829 2.355,3.690 2.355,3.548 C2.355,3.214 1.947,2.884 1.947,2.884 L1.963,2.877 L0.080,0.905 C-0.037,0.783 -0.029,0.593 0.095,0.479 L0.547,0.068 C0.671,-0.045 0.866,-0.039 0.983,0.083 L3.823,3.056 C3.866,3.102 3.875,3.161 3.885,3.218 C3.945,3.267 3.988,3.333 3.988,3.415 L3.988,3.681 C3.988,3.689 3.979,3.694 3.978,3.702 Z"></path></svg></span>Movies</nav>
		<div class="site-content__inner">
			<div id="primary" class="content-area"> <header class="page-header">
				<h1 class="page-title">Movies</h1>
			</header>
			<div class="vodi-control-bar">
				<div class="vodi-control-bar__left">
					<div class="widget masvideos widget_layered_nav masvideos-movies-tags-filter-widget"><ul class="masvideos-widget-movies-layered-nav-list">
						<li class="masvideos-widget-movies-layered-nav-list__item masvideos-layered-nav-term "><i class="far fa-window-maximize"></i>
							<a rel="nofollow" href="/react2/movies/?rating_filter=9,5&amp;filter_tag=4k-ultra">4K Ultra</a>
						</li>
						<li class="masvideos-widget-movies-layered-nav-list__item masvideos-layered-nav-term "><i class="fas fa-chess-knight"></i>
							<a rel="nofollow" href="/react2/movies/?rating_filter=9,5&amp;filter_tag=brother">Brother</a>
						</li>
					</ul>
				</div>
			</div>
			<div class="vodi-control-bar__right"> 
				<ul class="archive-view-switcher nav nav-tabs">
					<li class="nav-item">
						<a id="vodi-archive-view-switcher-grid" class="nav-link active" data-archive-columns="6" data-toggle="tab" data-archive-class="grid" title="Grid View" href="#vodi-archive-view-content"><svg xmlns="http://www.w3.org/2000/svg" width="18px" height="15px"><path fill-rule="evenodd" fill="rgb(176, 183, 188)" d="M16.500,10.999 C15.671,10.999 15.000,10.327 15.000,9.500 C15.000,8.671 15.671,7.999 16.500,7.999 C17.328,7.999 18.000,8.671 18.000,9.500 C18.000,10.327 17.328,10.999 16.500,10.999 ZM16.500,6.999 C15.671,6.999 15.000,6.328 15.000,5.499 C15.000,4.671 15.671,3.999 16.500,3.999 C17.328,3.999 18.000,4.671 18.000,5.499 C18.000,6.328 17.328,6.999 16.500,6.999 ZM16.500,3.000 C15.671,3.000 15.000,2.328 15.000,1.499 C15.000,0.671 15.671,-0.001 16.500,-0.001 C17.328,-0.001 18.000,0.671 18.000,1.499 C18.000,2.328 17.328,3.000 16.500,3.000 ZM11.500,14.999 C10.672,14.999 10.000,14.328 10.000,13.499 C10.000,12.671 10.672,11.999 11.500,11.999 C12.328,11.999 13.000,12.671 13.000,13.499 C13.000,14.328 12.328,14.999 11.500,14.999 ZM11.500,10.999 C10.672,10.999 10.000,10.327 10.000,9.500 C10.000,8.671 10.672,7.999 11.500,7.999 C12.328,7.999 13.000,8.671 13.000,9.500 C13.000,10.327 12.328,10.999 11.500,10.999 ZM11.500,6.999 C10.672,6.999 10.000,6.328 10.000,5.499 C10.000,4.671 10.672,3.999 11.500,3.999 C12.328,3.999 13.000,4.671 13.000,5.499 C13.000,6.328 12.328,6.999 11.500,6.999 ZM11.500,3.000 C10.672,3.000 10.000,2.328 10.000,1.499 C10.000,0.671 10.672,-0.001 11.500,-0.001 C12.328,-0.001 13.000,0.671 13.000,1.499 C13.000,2.328 12.328,3.000 11.500,3.000 ZM6.500,14.999 C5.671,14.999 5.000,14.328 5.000,13.499 C5.000,12.671 5.671,11.999 6.500,11.999 C7.328,11.999 8.000,12.671 8.000,13.499 C8.000,14.328 7.328,14.999 6.500,14.999 ZM6.500,10.999 C5.671,10.999 5.000,10.327 5.000,9.500 C5.000,8.671 5.671,7.999 6.500,7.999 C7.328,7.999 8.000,8.671 8.000,9.500 C8.000,10.327 7.328,10.999 6.500,10.999 ZM6.500,6.999 C5.671,6.999 5.000,6.328 5.000,5.499 C5.000,4.671 5.671,3.999 6.500,3.999 C7.328,3.999 8.000,4.671 8.000,5.499 C8.000,6.328 7.328,6.999 6.500,6.999 ZM6.500,3.000 C5.671,3.000 5.000,2.328 5.000,1.499 C5.000,0.671 5.671,-0.001 6.500,-0.001 C7.328,-0.001 8.000,0.671 8.000,1.499 C8.000,2.328 7.328,3.000 6.500,3.000 ZM1.500,14.999 C0.671,14.999 -0.000,14.328 -0.000,13.499 C-0.000,12.671 0.671,11.999 1.500,11.999 C2.328,11.999 3.000,12.671 3.000,13.499 C3.000,14.328 2.328,14.999 1.500,14.999 ZM1.500,10.999 C0.671,10.999 -0.000,10.327 -0.000,9.500 C-0.000,8.671 0.671,7.999 1.500,7.999 C2.328,7.999 3.000,8.671 3.000,9.500 C3.000,10.327 2.328,10.999 1.500,10.999 ZM1.500,6.999 C0.671,6.999 -0.000,6.328 -0.000,5.499 C-0.000,4.671 0.671,3.999 1.500,3.999 C2.328,3.999 3.000,4.671 3.000,5.499 C3.000,6.328 2.328,6.999 1.500,6.999 ZM1.500,3.000 C0.671,3.000 -0.000,2.328 -0.000,1.499 C-0.000,0.671 0.671,-0.001 1.500,-0.001 C2.328,-0.001 3.000,0.671 3.000,1.499 C3.000,2.328 2.328,3.000 1.500,3.000 ZM16.500,11.999 C17.328,11.999 18.000,12.671 18.000,13.499 C18.000,14.328 17.328,14.999 16.500,14.999 C15.671,14.999 15.000,14.328 15.000,13.499 C15.000,12.671 15.671,11.999 16.500,11.999 Z"></path>
						</svg>
					</a>
				</li>
				<li class="nav-item">
					<a id="vodi-archive-view-switcher-grid-extended" class="nav-link " data-archive-columns="6" data-toggle="tab" data-archive-class="grid-extended" title="Grid View Spacious" href="#vodi-archive-view-content"><svg xmlns="http://www.w3.org/2000/svg" width="17px" height="15px"><path fill-rule="evenodd" fill="rgb(180, 187, 192)" d="M15.500,8.999 C14.671,8.999 14.000,8.328 14.000,7.499 C14.000,6.671 14.671,5.999 15.500,5.999 C16.328,5.999 17.000,6.671 17.000,7.499 C17.000,8.328 16.328,8.999 15.500,8.999 ZM15.500,2.999 C14.671,2.999 14.000,2.328 14.000,1.499 C14.000,0.671 14.671,-0.000 15.500,-0.000 C16.328,-0.000 17.000,0.671 17.000,1.499 C17.000,2.328 16.328,2.999 15.500,2.999 ZM8.500,14.999 C7.671,14.999 7.000,14.328 7.000,13.499 C7.000,12.671 7.671,11.999 8.500,11.999 C9.328,11.999 10.000,12.671 10.000,13.499 C10.000,14.328 9.328,14.999 8.500,14.999 ZM8.500,8.999 C7.671,8.999 7.000,8.328 7.000,7.499 C7.000,6.671 7.671,5.999 8.500,5.999 C9.328,5.999 10.000,6.671 10.000,7.499 C10.000,8.328 9.328,8.999 8.500,8.999 ZM8.500,2.999 C7.671,2.999 7.000,2.328 7.000,1.499 C7.000,0.671 7.671,-0.000 8.500,-0.000 C9.328,-0.000 10.000,0.671 10.000,1.499 C10.000,2.328 9.328,2.999 8.500,2.999 ZM1.500,14.999 C0.671,14.999 -0.000,14.328 -0.000,13.499 C-0.000,12.671 0.671,11.999 1.500,11.999 C2.328,11.999 3.000,12.671 3.000,13.499 C3.000,14.328 2.328,14.999 1.500,14.999 ZM1.500,8.999 C0.671,8.999 -0.000,8.328 -0.000,7.499 C-0.000,6.671 0.671,5.999 1.500,5.999 C2.328,5.999 3.000,6.671 3.000,7.499 C3.000,8.328 2.328,8.999 1.500,8.999 ZM1.500,2.999 C0.671,2.999 -0.000,2.328 -0.000,1.499 C-0.000,0.671 0.671,-0.000 1.500,-0.000 C2.328,-0.000 3.000,0.671 3.000,1.499 C3.000,2.328 2.328,2.999 1.500,2.999 ZM15.500,11.999 C16.328,11.999 17.000,12.671 17.000,13.499 C17.000,14.328 16.328,14.999 15.500,14.999 C14.671,14.999 14.000,14.328 14.000,13.499 C14.000,12.671 14.671,11.999 15.500,11.999 Z"></path>
					</svg>
				</a>
			</li>
			<li class="nav-item">
				<a id="vodi-archive-view-switcher-list-large" class="nav-link " data-archive-columns="6" data-toggle="tab" data-archive-class="list-large" title="List Large View" href="#vodi-archive-view-content"><svg xmlns="http://www.w3.org/2000/svg" width="18px" height="15px"><path fill-rule="evenodd" fill="rgb(112, 112, 112)" d="M5.000,13.999 L5.000,12.999 L18.000,12.999 L18.000,13.999 L5.000,13.999 ZM5.000,6.999 L18.000,6.999 L18.000,7.999 L5.000,7.999 L5.000,6.999 ZM5.000,0.999 L18.000,0.999 L18.000,1.999 L5.000,1.999 L5.000,0.999 ZM1.500,14.999 C0.671,14.999 -0.000,14.327 -0.000,13.499 C-0.000,12.671 0.671,11.999 1.500,11.999 C2.328,11.999 3.000,12.671 3.000,13.499 C3.000,14.327 2.328,14.999 1.500,14.999 ZM1.500,8.999 C0.671,8.999 -0.000,8.328 -0.000,7.499 C-0.000,6.671 0.671,5.999 1.500,5.999 C2.328,5.999 3.000,6.671 3.000,7.499 C3.000,8.328 2.328,8.999 1.500,8.999 ZM1.500,2.999 C0.671,2.999 -0.000,2.328 -0.000,1.499 C-0.000,0.671 0.671,-0.001 1.500,-0.001 C2.328,-0.001 3.000,0.671 3.000,1.499 C3.000,2.328 2.328,2.999 1.500,2.999 Z"></path>
				</svg>
			</a>
		</li>
		<li class="nav-item">
			<a id="vodi-archive-view-switcher-list-small" class="nav-link " data-archive-columns="6" data-toggle="tab" data-archive-class="list-small" title="List View" href="#vodi-archive-view-content"><svg xmlns="http://www.w3.org/2000/svg" width="18px" height="15px"><path fill-rule="evenodd" fill="rgb(112, 112, 112)" d="M5.000,13.999 L5.000,12.999 L18.000,12.999 L18.000,13.999 L5.000,13.999 ZM5.000,8.999 L18.000,8.999 L18.000,10.000 L5.000,10.000 L5.000,8.999 ZM5.000,4.999 L18.000,4.999 L18.000,5.999 L5.000,5.999 L5.000,4.999 ZM5.000,0.999 L18.000,0.999 L18.000,1.999 L5.000,1.999 L5.000,0.999 ZM1.500,14.999 C0.671,14.999 -0.000,14.327 -0.000,13.499 C-0.000,12.671 0.671,11.999 1.500,11.999 C2.328,11.999 3.000,12.671 3.000,13.499 C3.000,14.327 2.328,14.999 1.500,14.999 ZM1.500,10.999 C0.671,10.999 -0.000,10.328 -0.000,9.499 C-0.000,8.671 0.671,7.999 1.500,7.999 C2.328,7.999 3.000,8.671 3.000,9.499 C3.000,10.328 2.328,10.999 1.500,10.999 ZM1.500,6.999 C0.671,6.999 -0.000,6.328 -0.000,5.499 C-0.000,4.671 0.671,3.999 1.500,3.999 C2.328,3.999 3.000,4.671 3.000,5.499 C3.000,6.328 2.328,6.999 1.500,6.999 ZM1.500,2.999 C0.671,2.999 -0.000,2.328 -0.000,1.499 C-0.000,0.671 0.671,-0.001 1.500,-0.001 C2.328,-0.001 3.000,0.671 3.000,1.499 C3.000,2.328 2.328,2.999 1.500,2.999 Z"></path>
			</svg>
		</a>
	</li>
	<li class="nav-item">
		<a id="vodi-archive-view-switcher-list" class="nav-link " data-archive-columns="6" data-toggle="tab" data-archive-class="list" title="List Small View" href="#vodi-archive-view-content"><svg xmlns="http://www.w3.org/2000/svg" width="17px" height="13px"><path fill-rule="evenodd" fill="rgb(180, 187, 192)" d="M-0.000,13.000 L-0.000,11.999 L17.000,11.999 L17.000,13.000 L-0.000,13.000 ZM-0.000,7.999 L17.000,7.999 L17.000,8.999 L-0.000,8.999 L-0.000,7.999 ZM-0.000,3.999 L17.000,3.999 L17.000,4.999 L-0.000,4.999 L-0.000,3.999 ZM-0.000,-0.001 L17.000,-0.001 L17.000,0.999 L-0.000,0.999 L-0.000,-0.001 Z"></path>
		</svg>
	</a>
</li>
</ul>
<div class="movies-ordering">
	<div class="handheld-sidebar-toggle"><button class="btn sidebar-toggler" type="button"><i class="fas fa-sliders-h"></i><span>Filters</span></button>
	</div><svg class="svg-icon svg-icon__sort" aria-hidden="true" role="img" focusable="false" width="17px" height="14px"><path fill-rule="evenodd" d="M4.034,-0.001 C4.248,0.009 4.401,0.071 4.578,0.113 C4.699,0.294 4.899,0.408 4.967,0.644 C4.967,1.606 4.967,2.568 4.967,3.529 C4.967,5.972 4.967,8.414 4.967,10.856 C4.980,10.856 4.993,10.856 5.006,10.856 C5.641,10.224 6.276,9.591 6.911,8.958 C7.041,8.873 7.329,8.745 7.572,8.806 C7.930,8.896 8.016,9.121 8.233,9.337 C8.293,10.165 7.817,10.389 7.377,10.818 C6.639,11.539 5.900,12.260 5.161,12.982 C4.928,13.209 4.395,13.909 4.073,13.969 C3.952,13.787 3.760,13.663 3.606,13.513 C3.270,13.184 2.933,12.855 2.596,12.526 C2.052,11.982 1.507,11.438 0.963,10.894 C0.717,10.666 0.471,10.438 0.224,10.211 C0.148,10.110 0.119,9.993 0.030,9.907 C0.015,9.698 -0.048,9.491 0.069,9.337 C0.171,8.957 0.746,8.634 1.235,8.882 C1.922,9.540 2.609,10.198 3.296,10.856 C3.296,7.465 3.296,4.073 3.296,0.682 C3.358,0.600 3.351,0.467 3.412,0.379 C3.511,0.235 3.714,0.158 3.840,0.037 C3.938,0.035 3.984,0.034 4.034,-0.001 ZM12.781,0.037 C12.820,0.037 12.859,0.037 12.898,0.037 C13.999,1.125 15.101,2.214 16.202,3.302 C16.427,3.522 17.287,4.153 16.902,4.668 C16.828,4.945 16.613,4.994 16.435,5.162 C16.280,5.174 16.124,5.187 15.969,5.200 C15.631,5.108 15.447,4.842 15.230,4.630 C14.712,4.137 14.193,3.643 13.675,3.150 C13.675,6.553 13.675,9.958 13.675,13.362 C13.514,13.560 13.485,13.804 13.209,13.893 C13.076,14.007 12.700,14.044 12.548,13.931 C11.760,13.719 12.004,12.233 12.004,11.273 C12.004,8.566 12.004,5.858 12.004,3.150 C11.991,3.150 11.978,3.150 11.965,3.150 C11.676,3.589 10.996,4.095 10.604,4.479 C10.404,4.673 10.198,4.996 9.943,5.124 C9.784,5.204 9.589,5.200 9.360,5.200 C9.238,5.102 9.043,5.080 8.932,4.972 C8.848,4.890 8.822,4.751 8.738,4.668 C8.699,3.730 9.312,3.462 9.827,2.960 C10.811,1.986 11.796,1.011 12.781,0.037 Z"></path></svg> <form method="get">
		<select name="orderby" class="orderby" onchange="this.form.submit();">
			<option value="title-asc">From A to Z</option>
			<option value="title-desc">From Z to A</option>
			<option value="release_date" selected="selected">Latest</option>
			<option value="menu_order">Menu Order</option>
			<option value="rating">Rating</option>
			<option value="views">Views</option>
			<option value="likes">Likes</option>
		</select>
		<input type="hidden" name="paged" value="1">
		<input type="hidden" name="rating_filter" value="9,5"> </form>
	</div>
</div>
</div>
<div class="vodi-archive-wrapper" data-view="grid">
	<div class="movies columns-6">
		<div class="movies__inner">
			<div class="post-290 movie type-movie status-publish has-post-thumbnail hentry movie_genre-action movie_genre-adventure movie_tag-4k-ultra movie_tag-brother movie_tag-king movie_tag-viking">
				<div class="movie__poster">
					<a href="/react2/movie/oceans-8/289096" class="masvideos-LoopMovie-link masvideos-loop-movie__link movie__link"><img width="300" height="450" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/20-300x450.jpg" class="movie__poster--image" alt="" srcset="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/20-300x450.jpg 300w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/20-66x98.jpg 66w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/20-200x300.jpg 200w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/20-600x900.jpg 600w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/20-150x225.jpg 150w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/20.jpg 668w" sizes="(max-width: 300px) 100vw, 300px">
					</a>
				</div>
				<div class="movie__body">
					<div class="movie__info">
						<div class="movie__info--head">
							<div class="movie__meta"><span class="movie__meta--release-year">2018</span><span class="movie__meta--genre">
								<a href="/react2/genre/action/" rel="tag">Action</a>, <a href="/react2/genre/adventure/" rel="tag">Adventure</a></span>
							</div>
							<a href="/react2/movie/oceans-8/289096" class="masvideos-LoopMovie-link masvideos-loop-movie__link movie__link"><h3 class="masvideos-loop-movie__title  movie__title">Ocean's 8</h3>
							</a>
						</div> 
						<div class="movie__short-description">
							<div>
								<p>Debbie Ocean gathers an all-female crew to attempt an impossible heist at New York City's yearly Met Gala.</p>
							</div>
						</div>
						<div class="movie__actions">
							<a href="/react2/movie/oceans-8/289096" class="movie-actions--link_watch">Watch Now</a> 
							<div class="movie-actions--link_add-to-playlist dropdown">
								<a class="dropdown-toggle" href="/react2/movie/oceans-8/289096" data-toggle="dropdown">+ Playlist</a>
								<div class="dropdown-menu">
									<a class="login-link" href="https://demo3.madrasthemes.com/vodi-demos/main/my-account/movie-playlists/">Sign in to add this movie to a playlist.</a>
								</div>
							</div>
						</div>
					</div>
					<div class="movie__review-info"> <a href="/react2/movie/oceans-8/#reviews" class="avg-rating">
						<span class="rating-with-count">
							<svg class="vodi-svg" width="40px" height="39px" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 39"><title>play</title><path fill-rule="evenodd" d="M19.633,-0.000 C21.509,0.035 21.530,1.174 22.167,2.414 C23.329,4.679 24.406,7.067 25.572,9.338 C25.853,9.886 26.431,11.640 26.918,11.834 C27.486,12.203 29.345,12.109 30.165,12.316 C32.170,12.825 34.489,12.860 36.500,13.364 C37.516,13.618 38.689,13.413 39.430,13.927 C39.689,14.107 39.770,14.504 39.984,14.732 C40.047,16.499 39.096,16.843 38.163,17.792 C36.473,19.509 34.784,21.227 33.095,22.944 C32.585,23.462 31.092,24.543 31.036,25.359 C31.423,25.951 31.307,27.455 31.511,28.258 C32.138,30.727 32.213,33.522 32.857,35.987 C33.142,37.078 33.016,38.241 32.303,38.724 C31.108,39.533 29.632,38.193 28.819,37.758 C26.695,36.623 24.601,35.624 22.483,34.457 C21.979,34.179 20.607,33.178 20.108,33.088 C19.748,33.023 18.163,34.107 17.812,34.296 C15.557,35.505 13.340,36.640 11.080,37.839 C10.548,38.120 9.180,39.226 8.309,38.966 C6.955,38.558 6.874,36.993 7.280,35.423 C7.716,33.733 7.697,31.880 8.151,30.109 C8.527,28.642 8.907,26.529 9.022,24.957 C8.092,24.344 7.202,23.107 6.408,22.300 C4.760,20.625 3.059,18.990 1.340,17.389 C0.646,16.742 -0.578,15.515 0.311,14.249 C0.915,13.388 2.364,13.656 3.557,13.364 C6.678,12.599 10.114,12.468 13.298,11.834 C14.186,9.747 15.306,7.711 16.307,5.716 C16.954,4.426 17.496,3.163 18.128,1.931 C18.334,1.531 18.358,1.093 18.603,0.724 C18.845,0.362 19.299,0.273 19.633,-0.000 Z"></path></svg> <span class="avg-rating-number"> 9.0</span>
						</span>
						<span class="rating-number-with-text">
							<span class="avg-rating-number"> 9.0</span>
							<span class="avg-rating-text">
								<span>1</span> Vote </span>
							</span>
						</a>
						<div class="viewers-count">
						</div>
					</div>
				</div>
			</div>
			<div class="post-330 movie type-movie status-publish has-post-thumbnail hentry movie_genre-action movie_genre-thriller movie_tag-4k-ultra movie_tag-brother movie_tag-premieres movie_tag-viking movie_music-alan-silvestri movie_photos-jack-kirby movie_photos-jim-starlin movie_photos-trent-opaloch">
				<div class="movie__poster">
					<a href="/react2/movie/the-tale/289096" class="masvideos-LoopMovie-link masvideos-loop-movie__link movie__link"><img width="300" height="450" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/40-murder-by-death-300x450.jpg" class="movie__poster--image" alt="" srcset="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/40-murder-by-death-300x450.jpg 300w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/40-murder-by-death-66x98.jpg 66w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/40-murder-by-death-600x900.jpg 600w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/40-murder-by-death-150x225.jpg 150w" sizes="(max-width: 300px) 100vw, 300px">
					</a>
				</div>
				<div class="movie__body">
					<div class="movie__info">
						<div class="movie__info--head">
							<div class="movie__meta"><span class="movie__meta--release-year">2018</span><span class="movie__meta--genre">
								<a href="/react2/genre/action/" rel="tag">Action</a>, <a href="/react2/genre/thriller/" rel="tag">Thriller</a></span>
							</div>
							<a href="/react2/movie/the-tale/289096" class="masvideos-LoopMovie-link masvideos-loop-movie__link movie__link"><h3 class="masvideos-loop-movie__title  movie__title">The Tale</h3>
							</a>
						</div> 
						<div class="movie__short-description">
							<div><p>A woman filming a documentary on childhood rape victims starts to question the nature of her childhood relationship with her riding instructor and running coach</p>
							</div>
						</div>
						<div class="movie__actions">
							<a href="/react2/movie/the-tale/289096" class="movie-actions--link_watch">Watch Now</a> 
							<div class="movie-actions--link_add-to-playlist dropdown">
								<a class="dropdown-toggle" href="/react2/movie/the-tale/289096" data-toggle="dropdown">+ Playlist</a>
								<div class="dropdown-menu">
									<a class="login-link" href="https://demo3.madrasthemes.com/vodi-demos/main/my-account/movie-playlists/">Sign in to add this movie to a playlist.</a>
								</div>
							</div>
						</div>
					</div>
					<div class="movie__review-info"> <a href="/react2/movie/the-tale/#reviews" class="avg-rating">
						<span class="rating-with-count">
							<svg class="vodi-svg" width="40px" height="39px" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 39"><title>play</title><path fill-rule="evenodd" d="M19.633,-0.000 C21.509,0.035 21.530,1.174 22.167,2.414 C23.329,4.679 24.406,7.067 25.572,9.338 C25.853,9.886 26.431,11.640 26.918,11.834 C27.486,12.203 29.345,12.109 30.165,12.316 C32.170,12.825 34.489,12.860 36.500,13.364 C37.516,13.618 38.689,13.413 39.430,13.927 C39.689,14.107 39.770,14.504 39.984,14.732 C40.047,16.499 39.096,16.843 38.163,17.792 C36.473,19.509 34.784,21.227 33.095,22.944 C32.585,23.462 31.092,24.543 31.036,25.359 C31.423,25.951 31.307,27.455 31.511,28.258 C32.138,30.727 32.213,33.522 32.857,35.987 C33.142,37.078 33.016,38.241 32.303,38.724 C31.108,39.533 29.632,38.193 28.819,37.758 C26.695,36.623 24.601,35.624 22.483,34.457 C21.979,34.179 20.607,33.178 20.108,33.088 C19.748,33.023 18.163,34.107 17.812,34.296 C15.557,35.505 13.340,36.640 11.080,37.839 C10.548,38.120 9.180,39.226 8.309,38.966 C6.955,38.558 6.874,36.993 7.280,35.423 C7.716,33.733 7.697,31.880 8.151,30.109 C8.527,28.642 8.907,26.529 9.022,24.957 C8.092,24.344 7.202,23.107 6.408,22.300 C4.760,20.625 3.059,18.990 1.340,17.389 C0.646,16.742 -0.578,15.515 0.311,14.249 C0.915,13.388 2.364,13.656 3.557,13.364 C6.678,12.599 10.114,12.468 13.298,11.834 C14.186,9.747 15.306,7.711 16.307,5.716 C16.954,4.426 17.496,3.163 18.128,1.931 C18.334,1.531 18.358,1.093 18.603,0.724 C18.845,0.362 19.299,0.273 19.633,-0.000 Z"></path></svg> <span class="avg-rating-number"> 8.5</span>
						</span>
						<span class="rating-number-with-text">
							<span class="avg-rating-number"> 8.5</span>
							<span class="avg-rating-text">
								<span>2</span> Votes </span>
							</span>
						</a>
						<div class="viewers-count">
						</div>
					</div>
				</div>
			</div>
			<div class="post-270 movie type-movie status-publish has-post-thumbnail hentry movie_genre-action movie_genre-horror movie_tag-4k-ultra movie_tag-brother movie_tag-premieres movie_tag-viking">
				<div class="movie__poster">
					<a href="/react2/movie/the-strangers-prey-at-night/289096" class="masvideos-LoopMovie-link masvideos-loop-movie__link movie__link"><img width="300" height="450" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/10-300x450.jpg" class="movie__poster--image" alt="" srcset="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/10-300x450.jpg 300w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/10-66x98.jpg 66w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/10-200x300.jpg 200w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/10-600x900.jpg 600w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/10-150x225.jpg 150w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/10.jpg 668w" sizes="(max-width: 300px) 100vw, 300px">
					</a>
				</div>
				<div class="movie__body">
					<div class="movie__info">
						<div class="movie__info--head">
							<div class="movie__meta"><span class="movie__meta--release-year">2018</span><span class="movie__meta--genre">
								<a href="/react2/genre/action/" rel="tag">Action</a>, <a href="/react2/genre/horror/" rel="tag">Horror</a></span>
							</div>
							<a href="/react2/movie/the-strangers-prey-at-night/289096" class="masvideos-LoopMovie-link masvideos-loop-movie__link movie__link"><h3 class="masvideos-loop-movie__title  movie__title">The Strangers: Prey At Night</h3>
							</a>
						</div> 
						<div class="movie__short-description">
							<div>
								<p>A family of four staying at a secluded mobile home park for the night are stalked and then hunted by three masked psychopaths.</p>
							</div>
						</div>
						<div class="movie__actions">
							<a href="/react2/movie/the-strangers-prey-at-night/289096" class="movie-actions--link_watch">Watch Now</a> 
							<div class="movie-actions--link_add-to-playlist dropdown">
								<a class="dropdown-toggle" href="/react2/movie/the-strangers-prey-at-night/289096" data-toggle="dropdown">+ Playlist</a>
								<div class="dropdown-menu">
									<a class="login-link" href="https://demo3.madrasthemes.com/vodi-demos/main/my-account/movie-playlists/">Sign in to add this movie to a playlist.</a>
								</div>
							</div>
						</div>
					</div>
					<div class="movie__review-info"> <a href="/react2/movie/the-strangers-prey-at-night/289096#reviews" class="avg-rating">
						<span class="rating-with-count">
							<svg class="vodi-svg" width="40px" height="39px" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 39"><title>play</title><path fill-rule="evenodd" d="M19.633,-0.000 C21.509,0.035 21.530,1.174 22.167,2.414 C23.329,4.679 24.406,7.067 25.572,9.338 C25.853,9.886 26.431,11.640 26.918,11.834 C27.486,12.203 29.345,12.109 30.165,12.316 C32.170,12.825 34.489,12.860 36.500,13.364 C37.516,13.618 38.689,13.413 39.430,13.927 C39.689,14.107 39.770,14.504 39.984,14.732 C40.047,16.499 39.096,16.843 38.163,17.792 C36.473,19.509 34.784,21.227 33.095,22.944 C32.585,23.462 31.092,24.543 31.036,25.359 C31.423,25.951 31.307,27.455 31.511,28.258 C32.138,30.727 32.213,33.522 32.857,35.987 C33.142,37.078 33.016,38.241 32.303,38.724 C31.108,39.533 29.632,38.193 28.819,37.758 C26.695,36.623 24.601,35.624 22.483,34.457 C21.979,34.179 20.607,33.178 20.108,33.088 C19.748,33.023 18.163,34.107 17.812,34.296 C15.557,35.505 13.340,36.640 11.080,37.839 C10.548,38.120 9.180,39.226 8.309,38.966 C6.955,38.558 6.874,36.993 7.280,35.423 C7.716,33.733 7.697,31.880 8.151,30.109 C8.527,28.642 8.907,26.529 9.022,24.957 C8.092,24.344 7.202,23.107 6.408,22.300 C4.760,20.625 3.059,18.990 1.340,17.389 C0.646,16.742 -0.578,15.515 0.311,14.249 C0.915,13.388 2.364,13.656 3.557,13.364 C6.678,12.599 10.114,12.468 13.298,11.834 C14.186,9.747 15.306,7.711 16.307,5.716 C16.954,4.426 17.496,3.163 18.128,1.931 C18.334,1.531 18.358,1.093 18.603,0.724 C18.845,0.362 19.299,0.273 19.633,-0.000 Z"></path></svg> <span class="avg-rating-number"> 9.0</span>
						</span>
						<span class="rating-number-with-text">
							<span class="avg-rating-number"> 9.0</span>
							<span class="avg-rating-text">
								<span>1</span> Vote </span>
							</span>
						</a>
						<div class="viewers-count">
						</div>
					</div>
				</div>
			</div>
			<div class="post-316 movie type-movie status-publish has-post-thumbnail hentry movie_genre-action movie_genre-family movie_tag-4k-ultra movie_tag-brother movie_tag-premieres movie_tag-viking">
				<div class="movie__poster">
					<a href="/react2/movie/i-can-only-imagine/289096" class="masvideos-LoopMovie-link masvideos-loop-movie__link movie__link"><img width="300" height="450" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/33-a-woman-under-the-influence-300x450.jpg" class="movie__poster--image" alt="" srcset="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/33-a-woman-under-the-influence-300x450.jpg 300w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/33-a-woman-under-the-influence-66x98.jpg 66w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/33-a-woman-under-the-influence-600x900.jpg 600w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/33-a-woman-under-the-influence-150x225.jpg 150w" sizes="(max-width: 300px) 100vw, 300px">
					</a>
				</div>
				<div class="movie__body">
					<div class="movie__info">
						<div class="movie__info--head">
							<div class="movie__meta"><span class="movie__meta--release-year">2018</span><span class="movie__meta--genre">
								<a href="/react2/genre/action/" rel="tag">Action</a>, <a href="/react2/genre/family/" rel="tag">Family</a></span>
							</div>
							<a href="/react2/movie/i-can-only-imagine/289096" class="masvideos-LoopMovie-link masvideos-loop-movie__link movie__link"><h3 class="masvideos-loop-movie__title  movie__title">I Can Only Imagine</h3>
							</a>
						</div> 
						<div class="movie__short-description">
							<div>
								<p>The inspiring and unknown true story behind MercyMe's beloved, chart topping song that brings ultimate hope to so many is a gripping reminder of the power of true forgiveness.</p>
							</div>
						</div>
						<div class="movie__actions">
							<a href="/react2/movie/i-can-only-imagine/289096" class="movie-actions--link_watch">Watch Now</a> 
							<div class="movie-actions--link_add-to-playlist dropdown">
								<a class="dropdown-toggle" href="/react2/movie/i-can-only-imagine/289096" data-toggle="dropdown">+ Playlist</a>
								<div class="dropdown-menu">
									<a class="login-link" href="https://demo3.madrasthemes.com/vodi-demos/main/my-account/movie-playlists/">Sign in to add this movie to a playlist.</a>
								</div>
							</div>
						</div>
					</div>
					<div class="movie__review-info"> <a href="/react2/movie/i-can-only-imagine/289096#reviews" class="avg-rating">
						<span class="rating-with-count">
							<svg class="vodi-svg" width="40px" height="39px" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 39"><title>play</title><path fill-rule="evenodd" d="M19.633,-0.000 C21.509,0.035 21.530,1.174 22.167,2.414 C23.329,4.679 24.406,7.067 25.572,9.338 C25.853,9.886 26.431,11.640 26.918,11.834 C27.486,12.203 29.345,12.109 30.165,12.316 C32.170,12.825 34.489,12.860 36.500,13.364 C37.516,13.618 38.689,13.413 39.430,13.927 C39.689,14.107 39.770,14.504 39.984,14.732 C40.047,16.499 39.096,16.843 38.163,17.792 C36.473,19.509 34.784,21.227 33.095,22.944 C32.585,23.462 31.092,24.543 31.036,25.359 C31.423,25.951 31.307,27.455 31.511,28.258 C32.138,30.727 32.213,33.522 32.857,35.987 C33.142,37.078 33.016,38.241 32.303,38.724 C31.108,39.533 29.632,38.193 28.819,37.758 C26.695,36.623 24.601,35.624 22.483,34.457 C21.979,34.179 20.607,33.178 20.108,33.088 C19.748,33.023 18.163,34.107 17.812,34.296 C15.557,35.505 13.340,36.640 11.080,37.839 C10.548,38.120 9.180,39.226 8.309,38.966 C6.955,38.558 6.874,36.993 7.280,35.423 C7.716,33.733 7.697,31.880 8.151,30.109 C8.527,28.642 8.907,26.529 9.022,24.957 C8.092,24.344 7.202,23.107 6.408,22.300 C4.760,20.625 3.059,18.990 1.340,17.389 C0.646,16.742 -0.578,15.515 0.311,14.249 C0.915,13.388 2.364,13.656 3.557,13.364 C6.678,12.599 10.114,12.468 13.298,11.834 C14.186,9.747 15.306,7.711 16.307,5.716 C16.954,4.426 17.496,3.163 18.128,1.931 C18.334,1.531 18.358,1.093 18.603,0.724 C18.845,0.362 19.299,0.273 19.633,-0.000 Z"></path></svg> <span class="avg-rating-number"> 9.0</span>
						</span>
						<span class="rating-number-with-text">
							<span class="avg-rating-number"> 9.0</span>
							<span class="avg-rating-text">
								<span>1</span> Vote </span>
							</span>
						</a>
						<div class="viewers-count">
						</div>
					</div>
				</div>
			</div>
			<div class="post-334 movie type-movie status-publish has-post-thumbnail hentry movie_genre-action movie_genre-fantacy movie_tag-4k-ultra movie_tag-brother movie_tag-premieres movie_tag-viking">
				<div class="movie__poster">
					<a href="/react2/movie/every-day/289096" class="masvideos-LoopMovie-link masvideos-loop-movie__link movie__link"><img width="300" height="450" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/42-nim-project-300x450.jpg" class="movie__poster--image" alt="" srcset="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/42-nim-project-300x450.jpg 300w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/42-nim-project-66x98.jpg 66w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/42-nim-project-600x900.jpg 600w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/42-nim-project-150x225.jpg 150w" sizes="(max-width: 300px) 100vw, 300px">
					</a>
				</div>
				<div class="movie__body">
					<div class="movie__info">
						<div class="movie__info--head">
							<div class="movie__meta"><span class="movie__meta--release-year">2018</span><span class="movie__meta--genre">
								<a href="/react2/genre/action/" rel="tag">Action</a>, <a href="/react2/genre/fantacy/" rel="tag">Fantacy</a></span>
							</div>
							<a href="/react2/movie/every-day/289096" class="masvideos-LoopMovie-link masvideos-loop-movie__link movie__link"><h3 class="masvideos-loop-movie__title  movie__title">Every Day</h3>
							</a>
						</div> 
						<div class="movie__short-description">
							<div>
								<p>A shy teenager falls for someone who transforms into another person every day</p>
							</div>
						</div>
						<div class="movie__actions">
							<a href="/react2/movie/every-day/289096" class="movie-actions--link_watch">Watch Now</a> 
							<div class="movie-actions--link_add-to-playlist dropdown">
								<a class="dropdown-toggle" href="/react2/movie/every-day/289096" data-toggle="dropdown">+ Playlist</a>
								<div class="dropdown-menu">
									<a class="login-link" href="https://demo3.madrasthemes.com/vodi-demos/main/my-account/movie-playlists/">Sign in to add this movie to a playlist.</a>
								</div>
							</div>
						</div>
					</div>
					<div class="movie__review-info"> <a href="/react2/movie/every-day/289096#reviews" class="avg-rating">
						<span class="rating-with-count">
							<svg class="vodi-svg" width="40px" height="39px" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 39"><title>play</title><path fill-rule="evenodd" d="M19.633,-0.000 C21.509,0.035 21.530,1.174 22.167,2.414 C23.329,4.679 24.406,7.067 25.572,9.338 C25.853,9.886 26.431,11.640 26.918,11.834 C27.486,12.203 29.345,12.109 30.165,12.316 C32.170,12.825 34.489,12.860 36.500,13.364 C37.516,13.618 38.689,13.413 39.430,13.927 C39.689,14.107 39.770,14.504 39.984,14.732 C40.047,16.499 39.096,16.843 38.163,17.792 C36.473,19.509 34.784,21.227 33.095,22.944 C32.585,23.462 31.092,24.543 31.036,25.359 C31.423,25.951 31.307,27.455 31.511,28.258 C32.138,30.727 32.213,33.522 32.857,35.987 C33.142,37.078 33.016,38.241 32.303,38.724 C31.108,39.533 29.632,38.193 28.819,37.758 C26.695,36.623 24.601,35.624 22.483,34.457 C21.979,34.179 20.607,33.178 20.108,33.088 C19.748,33.023 18.163,34.107 17.812,34.296 C15.557,35.505 13.340,36.640 11.080,37.839 C10.548,38.120 9.180,39.226 8.309,38.966 C6.955,38.558 6.874,36.993 7.280,35.423 C7.716,33.733 7.697,31.880 8.151,30.109 C8.527,28.642 8.907,26.529 9.022,24.957 C8.092,24.344 7.202,23.107 6.408,22.300 C4.760,20.625 3.059,18.990 1.340,17.389 C0.646,16.742 -0.578,15.515 0.311,14.249 C0.915,13.388 2.364,13.656 3.557,13.364 C6.678,12.599 10.114,12.468 13.298,11.834 C14.186,9.747 15.306,7.711 16.307,5.716 C16.954,4.426 17.496,3.163 18.128,1.931 C18.334,1.531 18.358,1.093 18.603,0.724 C18.845,0.362 19.299,0.273 19.633,-0.000 Z"></path></svg> <span class="avg-rating-number"> 9.0</span>
						</span>
						<span class="rating-number-with-text">
							<span class="avg-rating-number"> 9.0</span>
							<span class="avg-rating-text">
								<span>1</span> Vote </span>
							</span>
						</a>
						<div class="viewers-count">
						</div>
					</div>
				</div>
			</div>
			<div class="post-720 movie type-movie status-publish has-post-thumbnail hentry movie_genre-action movie_genre-adventure movie_tag-brother movie_tag-king movie_tag-premieres movie_tag-viking">
				<div class="movie__poster">
					<a href="/react2/movie/euphoria/289096" class="masvideos-LoopMovie-link masvideos-loop-movie__link movie__link"><img width="300" height="450" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/23-300x450.jpg" class="movie__poster--image" alt="" srcset="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/23-300x450.jpg 300w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/23-66x98.jpg 66w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/23-200x300.jpg 200w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/23-600x900.jpg 600w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/23-150x225.jpg 150w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/23.jpg 668w" sizes="(max-width: 300px) 100vw, 300px">
					</a>
				</div>
				<div class="movie__body">
					<div class="movie__info">
						<div class="movie__info--head">
							<div class="movie__meta"><span class="movie__meta--release-year">2018</span><span class="movie__meta--genre">
								<a href="/react2/genre/action/" rel="tag">Action</a>, <a href="/react2/genre/adventure/" rel="tag">Adventure</a></span>
							</div>
							<a href="/react2/movie/euphoria/289096" class="masvideos-LoopMovie-link masvideos-loop-movie__link movie__link"><h3 class="masvideos-loop-movie__title  movie__title">Euphoria</h3>
							</a>
						</div> 
						<div class="movie__short-description">
							<div>
								<p>Sisters in conflict travelling through Europe toward a mystery destination</p>
							</div>
						</div>
						<div class="movie__actions">
							<a href="/react2/movie/euphoria/289096" class="movie-actions--link_watch">Watch Now</a> 
							<div class="movie-actions--link_add-to-playlist dropdown">
								<a class="dropdown-toggle" href="/react2/movie/euphoria/289096" data-toggle="dropdown">+ Playlist</a>
								<div class="dropdown-menu">
									<a class="login-link" href="https://demo3.madrasthemes.com/vodi-demos/main/my-account/movie-playlists/">Sign in to add this movie to a playlist.</a>
								</div>
							</div>
						</div>
					</div>
					<div class="movie__review-info"> <a href="/react2/movie/euphoria/289096#reviews" class="avg-rating">
						<span class="rating-with-count">
							<svg class="vodi-svg" width="40px" height="39px" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 39"><title>play</title><path fill-rule="evenodd" d="M19.633,-0.000 C21.509,0.035 21.530,1.174 22.167,2.414 C23.329,4.679 24.406,7.067 25.572,9.338 C25.853,9.886 26.431,11.640 26.918,11.834 C27.486,12.203 29.345,12.109 30.165,12.316 C32.170,12.825 34.489,12.860 36.500,13.364 C37.516,13.618 38.689,13.413 39.430,13.927 C39.689,14.107 39.770,14.504 39.984,14.732 C40.047,16.499 39.096,16.843 38.163,17.792 C36.473,19.509 34.784,21.227 33.095,22.944 C32.585,23.462 31.092,24.543 31.036,25.359 C31.423,25.951 31.307,27.455 31.511,28.258 C32.138,30.727 32.213,33.522 32.857,35.987 C33.142,37.078 33.016,38.241 32.303,38.724 C31.108,39.533 29.632,38.193 28.819,37.758 C26.695,36.623 24.601,35.624 22.483,34.457 C21.979,34.179 20.607,33.178 20.108,33.088 C19.748,33.023 18.163,34.107 17.812,34.296 C15.557,35.505 13.340,36.640 11.080,37.839 C10.548,38.120 9.180,39.226 8.309,38.966 C6.955,38.558 6.874,36.993 7.280,35.423 C7.716,33.733 7.697,31.880 8.151,30.109 C8.527,28.642 8.907,26.529 9.022,24.957 C8.092,24.344 7.202,23.107 6.408,22.300 C4.760,20.625 3.059,18.990 1.340,17.389 C0.646,16.742 -0.578,15.515 0.311,14.249 C0.915,13.388 2.364,13.656 3.557,13.364 C6.678,12.599 10.114,12.468 13.298,11.834 C14.186,9.747 15.306,7.711 16.307,5.716 C16.954,4.426 17.496,3.163 18.128,1.931 C18.334,1.531 18.358,1.093 18.603,0.724 C18.845,0.362 19.299,0.273 19.633,-0.000 Z"></path></svg> <span class="avg-rating-number"> 9.0</span>
						</span>
						<span class="rating-number-with-text">
							<span class="avg-rating-number"> 9.0</span>
							<span class="avg-rating-text">
								<span>1</span> Vote </span>
							</span>
						</a>
						<div class="viewers-count">
						</div>
					</div>
				</div>
			</div>
			<div class="post-296 movie type-movie status-publish has-post-thumbnail hentry movie_genre-action movie_genre-mystery movie_tag-4k-ultra movie_tag-brother movie_tag-king movie_tag-viking">
				<div class="movie__poster">
					<a href="/react2/movie/don-of-thieves/289096" class="masvideos-LoopMovie-link masvideos-loop-movie__link movie__link"><img width="300" height="450" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/23-300x450.jpg" class="movie__poster--image" alt="" srcset="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/23-300x450.jpg 300w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/23-66x98.jpg 66w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/23-200x300.jpg 200w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/23-600x900.jpg 600w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/23-150x225.jpg 150w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/23.jpg 668w" sizes="(max-width: 300px) 100vw, 300px">
					</a>
				</div>
				<div class="movie__body">
					<div class="movie__info">
						<div class="movie__info--head">
							<div class="movie__meta"><span class="movie__meta--release-year">2018</span><span class="movie__meta--genre">
								<a href="/react2/genre/action/" rel="tag">Action</a>, <a href="/react2/genre/mystery/" rel="tag">Mystery</a></span>
							</div>
							<a href="/react2/movie/don-of-thieves/289096" class="masvideos-LoopMovie-link masvideos-loop-movie__link movie__link"><h3 class="masvideos-loop-movie__title  movie__title">Don Of Thieves</h3>
							</a>
						</div> 
						<div class="movie__short-description">
							<div>
								<p>A gritty crime saga which follows the lives of an elite unit of the LA County Sheriff's Dept. and the state's most successful bank robbery crew as the outlaws plan a seemingly impossible heist on the Federal Reserve Bank</p>
							</div>
						</div>
						<div class="movie__actions">
							<a href="/react2/movie/don-of-thieves/289096" class="movie-actions--link_watch">Watch Now</a> 
							<div class="movie-actions--link_add-to-playlist dropdown">
								<a class="dropdown-toggle" href="/react2/movie/don-of-thieves/289096" data-toggle="dropdown">+ Playlist</a>
								<div class="dropdown-menu">
									<a class="login-link" href="https://demo3.madrasthemes.com/vodi-demos/main/my-account/movie-playlists/">Sign in to add this movie to a playlist.</a>
								</div>
							</div>
						</div>
					</div>
					<div class="movie__review-info"> <a href="/react2/movie/don-of-thieves/289096#reviews" class="avg-rating">
						<span class="rating-with-count">
							<svg class="vodi-svg" width="40px" height="39px" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 39"><title>play</title><path fill-rule="evenodd" d="M19.633,-0.000 C21.509,0.035 21.530,1.174 22.167,2.414 C23.329,4.679 24.406,7.067 25.572,9.338 C25.853,9.886 26.431,11.640 26.918,11.834 C27.486,12.203 29.345,12.109 30.165,12.316 C32.170,12.825 34.489,12.860 36.500,13.364 C37.516,13.618 38.689,13.413 39.430,13.927 C39.689,14.107 39.770,14.504 39.984,14.732 C40.047,16.499 39.096,16.843 38.163,17.792 C36.473,19.509 34.784,21.227 33.095,22.944 C32.585,23.462 31.092,24.543 31.036,25.359 C31.423,25.951 31.307,27.455 31.511,28.258 C32.138,30.727 32.213,33.522 32.857,35.987 C33.142,37.078 33.016,38.241 32.303,38.724 C31.108,39.533 29.632,38.193 28.819,37.758 C26.695,36.623 24.601,35.624 22.483,34.457 C21.979,34.179 20.607,33.178 20.108,33.088 C19.748,33.023 18.163,34.107 17.812,34.296 C15.557,35.505 13.340,36.640 11.080,37.839 C10.548,38.120 9.180,39.226 8.309,38.966 C6.955,38.558 6.874,36.993 7.280,35.423 C7.716,33.733 7.697,31.880 8.151,30.109 C8.527,28.642 8.907,26.529 9.022,24.957 C8.092,24.344 7.202,23.107 6.408,22.300 C4.760,20.625 3.059,18.990 1.340,17.389 C0.646,16.742 -0.578,15.515 0.311,14.249 C0.915,13.388 2.364,13.656 3.557,13.364 C6.678,12.599 10.114,12.468 13.298,11.834 C14.186,9.747 15.306,7.711 16.307,5.716 C16.954,4.426 17.496,3.163 18.128,1.931 C18.334,1.531 18.358,1.093 18.603,0.724 C18.845,0.362 19.299,0.273 19.633,-0.000 Z"></path></svg> <span class="avg-rating-number"> 9.0</span>
						</span>
						<span class="rating-number-with-text">
							<span class="avg-rating-number"> 9.0</span>
							<span class="avg-rating-text">
								<span>1</span> Vote </span>
							</span>
						</a>
						<div class="viewers-count">
						</div>
					</div>
				</div>
			</div>
			<div class="post-318 movie type-movie status-publish has-post-thumbnail hentry movie_genre-action movie_genre-history movie_tag-brother movie_tag-king movie_tag-premieres movie_tag-viking">
				<span class="movie__badge"><span class="movie__badge--featured">Featured</span></span>
				<div class="movie__poster">
					<a href="/react2/movie/12-strong/289096" class="masvideos-LoopMovie-link masvideos-loop-movie__link movie__link"><img width="300" height="450" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/34-al-jazda-300x450.jpg" class="movie__poster--image" alt="" srcset="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/34-al-jazda-300x450.jpg 300w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/34-al-jazda-66x98.jpg 66w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/34-al-jazda-600x900.jpg 600w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/34-al-jazda-150x225.jpg 150w" sizes="(max-width: 300px) 100vw, 300px">
					</a>
				</div>
				<div class="movie__body">
					<div class="movie__info">
						<div class="movie__info--head">
							<div class="movie__meta"><span class="movie__meta--release-year">2018</span><span class="movie__meta--genre">
								<a href="/react2/genre/action/" rel="tag">Action</a>, <a href="/react2/genre/history/" rel="tag">History</a></span>
							</div>
							<a href="/react2/movie/12-strong/289096" class="masvideos-LoopMovie-link masvideos-loop-movie__link movie__link"><h3 class="masvideos-loop-movie__title  movie__title">12 Strong</h3>
							</a>
						</div> 
						<div class="movie__short-description">
							<div>
								<p>12 Strong tells the story of the first Special Forces team deployed to Afghanistan after 9/11; under the leadership of a new captain, the team must work with an Afghan warlord to take down the Taliban</p>
							</div>
						</div>
						<div class="movie__actions">
							<a href="/react2/movie/12-strong/289096" class="movie-actions--link_watch">Watch Now</a> 
							<div class="movie-actions--link_add-to-playlist dropdown">
								<a class="dropdown-toggle" href="/react2/movie/12-strong/289096" data-toggle="dropdown">+ Playlist</a>
								<div class="dropdown-menu">
									<a class="login-link" href="https://demo3.madrasthemes.com/vodi-demos/main/my-account/movie-playlists/">Sign in to add this movie to a playlist.</a>
								</div>
							</div>
						</div>
					</div>
					<div class="movie__review-info"> <a href="/react2/movie/12-strong/289096#reviews" class="avg-rating">
						<span class="rating-with-count">
							<svg class="vodi-svg" width="40px" height="39px" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 39"><title>play</title><path fill-rule="evenodd" d="M19.633,-0.000 C21.509,0.035 21.530,1.174 22.167,2.414 C23.329,4.679 24.406,7.067 25.572,9.338 C25.853,9.886 26.431,11.640 26.918,11.834 C27.486,12.203 29.345,12.109 30.165,12.316 C32.170,12.825 34.489,12.860 36.500,13.364 C37.516,13.618 38.689,13.413 39.430,13.927 C39.689,14.107 39.770,14.504 39.984,14.732 C40.047,16.499 39.096,16.843 38.163,17.792 C36.473,19.509 34.784,21.227 33.095,22.944 C32.585,23.462 31.092,24.543 31.036,25.359 C31.423,25.951 31.307,27.455 31.511,28.258 C32.138,30.727 32.213,33.522 32.857,35.987 C33.142,37.078 33.016,38.241 32.303,38.724 C31.108,39.533 29.632,38.193 28.819,37.758 C26.695,36.623 24.601,35.624 22.483,34.457 C21.979,34.179 20.607,33.178 20.108,33.088 C19.748,33.023 18.163,34.107 17.812,34.296 C15.557,35.505 13.340,36.640 11.080,37.839 C10.548,38.120 9.180,39.226 8.309,38.966 C6.955,38.558 6.874,36.993 7.280,35.423 C7.716,33.733 7.697,31.880 8.151,30.109 C8.527,28.642 8.907,26.529 9.022,24.957 C8.092,24.344 7.202,23.107 6.408,22.300 C4.760,20.625 3.059,18.990 1.340,17.389 C0.646,16.742 -0.578,15.515 0.311,14.249 C0.915,13.388 2.364,13.656 3.557,13.364 C6.678,12.599 10.114,12.468 13.298,11.834 C14.186,9.747 15.306,7.711 16.307,5.716 C16.954,4.426 17.496,3.163 18.128,1.931 C18.334,1.531 18.358,1.093 18.603,0.724 C18.845,0.362 19.299,0.273 19.633,-0.000 Z"></path></svg> <span class="avg-rating-number"> 8.5</span>
						</span>
						<span class="rating-number-with-text">
							<span class="avg-rating-number"> 8.5</span>
							<span class="avg-rating-text">
								<span>2</span> Votes </span>
							</span>
						</a>
						<div class="viewers-count">
						</div>
					</div>
				</div>
			</div>
			<div class="post-332 movie type-movie status-publish has-post-thumbnail hentry movie_genre-action movie_genre-comedy movie_tag-brother movie_tag-king movie_tag-premieres movie_tag-viking">
				<span class="movie__badge"><span class="movie__badge--featured">Featured</span></span>
				<div class="movie__poster">
					<a href="/react2/movie/freak-show/" class="masvideos-LoopMovie-link masvideos-loop-movie__link movie__link"><img width="300" height="450" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/41-na-ostrzu-300x450.jpg" class="movie__poster--image" alt="" srcset="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/41-na-ostrzu-300x450.jpg 300w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/41-na-ostrzu-66x98.jpg 66w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/41-na-ostrzu-600x900.jpg 600w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/41-na-ostrzu-150x225.jpg 150w" sizes="(max-width: 300px) 100vw, 300px">
					</a>
				</div>
				<div class="movie__body">
					<div class="movie__info">
						<div class="movie__info--head">
							<div class="movie__meta"><span class="movie__meta--release-year">2018</span><span class="movie__meta--genre">
								<a href="/react2/genre/action/" rel="tag">Action</a>, <a href="/react2/genre/comedy/" rel="tag">Comedy</a></span>
							</div>
							<a href="/react2/movie/freak-show/" class="masvideos-LoopMovie-link masvideos-loop-movie__link movie__link"><h3 class="masvideos-loop-movie__title  movie__title">Freak Show</h3>
							</a>
						</div> 
						<div class="movie__short-description">
							<div>
								<p>Follows the story of teenager Billy Bloom who, despite attending an ultra conservative high school, makes the decision to run for homecoming queen</p>
							</div>
						</div>
						<div class="movie__actions">
							<a href="/react2/movie/freak-show/" class="movie-actions--link_watch">Watch Now</a> 
							<div class="movie-actions--link_add-to-playlist dropdown">
								<a class="dropdown-toggle" href="/react2/movie/freak-show/" data-toggle="dropdown">+ Playlist</a>
								<div class="dropdown-menu">
									<a class="login-link" href="https://demo3.madrasthemes.com/vodi-demos/main/my-account/movie-playlists/">Sign in to add this movie to a playlist.</a>
								</div>
							</div>
						</div>
					</div>
					<div class="movie__review-info"> <a href="/react2/movie/freak-show/#reviews" class="avg-rating">
						<span class="rating-with-count">
							<svg class="vodi-svg" width="40px" height="39px" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 39"><title>play</title><path fill-rule="evenodd" d="M19.633,-0.000 C21.509,0.035 21.530,1.174 22.167,2.414 C23.329,4.679 24.406,7.067 25.572,9.338 C25.853,9.886 26.431,11.640 26.918,11.834 C27.486,12.203 29.345,12.109 30.165,12.316 C32.170,12.825 34.489,12.860 36.500,13.364 C37.516,13.618 38.689,13.413 39.430,13.927 C39.689,14.107 39.770,14.504 39.984,14.732 C40.047,16.499 39.096,16.843 38.163,17.792 C36.473,19.509 34.784,21.227 33.095,22.944 C32.585,23.462 31.092,24.543 31.036,25.359 C31.423,25.951 31.307,27.455 31.511,28.258 C32.138,30.727 32.213,33.522 32.857,35.987 C33.142,37.078 33.016,38.241 32.303,38.724 C31.108,39.533 29.632,38.193 28.819,37.758 C26.695,36.623 24.601,35.624 22.483,34.457 C21.979,34.179 20.607,33.178 20.108,33.088 C19.748,33.023 18.163,34.107 17.812,34.296 C15.557,35.505 13.340,36.640 11.080,37.839 C10.548,38.120 9.180,39.226 8.309,38.966 C6.955,38.558 6.874,36.993 7.280,35.423 C7.716,33.733 7.697,31.880 8.151,30.109 C8.527,28.642 8.907,26.529 9.022,24.957 C8.092,24.344 7.202,23.107 6.408,22.300 C4.760,20.625 3.059,18.990 1.340,17.389 C0.646,16.742 -0.578,15.515 0.311,14.249 C0.915,13.388 2.364,13.656 3.557,13.364 C6.678,12.599 10.114,12.468 13.298,11.834 C14.186,9.747 15.306,7.711 16.307,5.716 C16.954,4.426 17.496,3.163 18.128,1.931 C18.334,1.531 18.358,1.093 18.603,0.724 C18.845,0.362 19.299,0.273 19.633,-0.000 Z"></path></svg> <span class="avg-rating-number"> 9.0</span>
						</span>
						<span class="rating-number-with-text">
							<span class="avg-rating-number"> 9.0</span>
							<span class="avg-rating-text">
								<span>1</span> Vote </span>
							</span>
						</a>
						<div class="viewers-count">
						</div>
					</div>
				</div>
			</div>
			<div class="post-714 movie type-movie status-publish has-post-thumbnail hentry movie_genre-action movie_genre-documentary movie_tag-4k-ultra movie_tag-brother movie_tag-king movie_tag-premieres movie_music-alan-silvestri movie_photos-jack-kirby movie_photos-jim-starlin movie_photos-trent-opaloch">
				<span class="movie__badge"><span class="movie__badge--featured">Featured</span></span>
				<div class="movie__poster">
					<a href="/react2/movie/paradigm-lost/" class="masvideos-LoopMovie-link masvideos-loop-movie__link movie__link"><img width="300" height="450" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/51-walk-hard-300x450.jpg" class="movie__poster--image" alt="" srcset="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/51-walk-hard-300x450.jpg 300w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/51-walk-hard-66x98.jpg 66w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/51-walk-hard-600x900.jpg 600w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/51-walk-hard-150x225.jpg 150w" sizes="(max-width: 300px) 100vw, 300px">
					</a>
				</div>
				<div class="movie__body">
					<div class="movie__info">
						<div class="movie__info--head">
							<div class="movie__meta"><span class="movie__meta--release-year">2017</span><span class="movie__meta--genre">
								<a href="/react2/genre/action/" rel="tag">Action</a>, <a href="/react2/genre/documentary/" rel="tag">Documentary</a></span>
							</div>
							<a href="/react2/movie/paradigm-lost/" class="masvideos-LoopMovie-link masvideos-loop-movie__link movie__link"><h3 class="masvideos-loop-movie__title  movie__title">Paradigm Lost</h3>
							</a>
						</div> 
						<div class="movie__short-description">
							<div><p>Poor Boyz Productions presents a Kai Lenny &amp; Johnny DeCesare film, PARADIGM LOST in co-production with Red Bull Media House.</p>
							</div>
						</div>
						<div class="movie__actions">
							<a href="/react2/movie/paradigm-lost/" class="movie-actions--link_watch">Watch Now</a> 
							<div class="movie-actions--link_add-to-playlist dropdown">
								<a class="dropdown-toggle" href="/react2/movie/paradigm-lost/" data-toggle="dropdown">+ Playlist</a>
								<div class="dropdown-menu">
									<a class="login-link" href="https://demo3.madrasthemes.com/vodi-demos/main/my-account/movie-playlists/">Sign in to add this movie to a playlist.</a>
								</div>
							</div>
						</div>
					</div>
					<div class="movie__review-info"> <a href="/react2/movie/paradigm-lost/#reviews" class="avg-rating">
						<span class="rating-with-count">
							<svg class="vodi-svg" width="40px" height="39px" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 39"><title>play</title><path fill-rule="evenodd" d="M19.633,-0.000 C21.509,0.035 21.530,1.174 22.167,2.414 C23.329,4.679 24.406,7.067 25.572,9.338 C25.853,9.886 26.431,11.640 26.918,11.834 C27.486,12.203 29.345,12.109 30.165,12.316 C32.170,12.825 34.489,12.860 36.500,13.364 C37.516,13.618 38.689,13.413 39.430,13.927 C39.689,14.107 39.770,14.504 39.984,14.732 C40.047,16.499 39.096,16.843 38.163,17.792 C36.473,19.509 34.784,21.227 33.095,22.944 C32.585,23.462 31.092,24.543 31.036,25.359 C31.423,25.951 31.307,27.455 31.511,28.258 C32.138,30.727 32.213,33.522 32.857,35.987 C33.142,37.078 33.016,38.241 32.303,38.724 C31.108,39.533 29.632,38.193 28.819,37.758 C26.695,36.623 24.601,35.624 22.483,34.457 C21.979,34.179 20.607,33.178 20.108,33.088 C19.748,33.023 18.163,34.107 17.812,34.296 C15.557,35.505 13.340,36.640 11.080,37.839 C10.548,38.120 9.180,39.226 8.309,38.966 C6.955,38.558 6.874,36.993 7.280,35.423 C7.716,33.733 7.697,31.880 8.151,30.109 C8.527,28.642 8.907,26.529 9.022,24.957 C8.092,24.344 7.202,23.107 6.408,22.300 C4.760,20.625 3.059,18.990 1.340,17.389 C0.646,16.742 -0.578,15.515 0.311,14.249 C0.915,13.388 2.364,13.656 3.557,13.364 C6.678,12.599 10.114,12.468 13.298,11.834 C14.186,9.747 15.306,7.711 16.307,5.716 C16.954,4.426 17.496,3.163 18.128,1.931 C18.334,1.531 18.358,1.093 18.603,0.724 C18.845,0.362 19.299,0.273 19.633,-0.000 Z"></path></svg> <span class="avg-rating-number"> 9.0</span>
						</span>
						<span class="rating-number-with-text">
							<span class="avg-rating-number"> 9.0</span>
							<span class="avg-rating-text">
								<span>2</span> Votes </span>
							</span>
						</a>
						<div class="viewers-count">
						</div>
					</div>
				</div>
			</div>
			<div class="post-322 movie type-movie status-publish has-post-thumbnail hentry movie_genre-adventure movie_genre-comedy movie_genre-romance movie_tag-4k-ultra movie_tag-king movie_tag-premieres movie_tag-viking">
				<div class="movie__poster">
					<a href="/react2/movie/the-big-sick/" class="masvideos-LoopMovie-link masvideos-loop-movie__link movie__link"><img width="300" height="450" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/36-cztery-lwy-300x450.jpg" class="movie__poster--image" alt="" srcset="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/36-cztery-lwy-300x450.jpg 300w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/36-cztery-lwy-66x98.jpg 66w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/36-cztery-lwy-600x900.jpg 600w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/36-cztery-lwy-150x225.jpg 150w" sizes="(max-width: 300px) 100vw, 300px">
					</a>
				</div>
				<div class="movie__body">
					<div class="movie__info">
						<div class="movie__info--head">
							<div class="movie__meta"><span class="movie__meta--release-year">2017</span><span class="movie__meta--genre">
								<a href="/react2/genre/adventure/" rel="tag">Adventure</a>, <a href="/react2/genre/comedy/" rel="tag">Comedy</a>, <a href="/react2/genre/romance/" rel="tag">Romance</a></span>
							</div>
							<a href="/react2/movie/the-big-sick/" class="masvideos-LoopMovie-link masvideos-loop-movie__link movie__link"><h3 class="masvideos-loop-movie__title  movie__title">The Big Sick</h3>
							</a>
						</div> 
						<div class="movie__short-description">
							<div>
								<p>Pakistan-born comedian Kumail Nanjiani and grad student Emily Gardner fall in love but struggle as their cultures clash. When Emily contracts a mysterious illness, Kumail finds himself forced to face her feisty parents, his family's expectations, and his true feelings</p>
							</div>
						</div>
						<div class="movie__actions">
							<a href="/react2/movie/the-big-sick/" class="movie-actions--link_watch">Watch Now</a> 
							<div class="movie-actions--link_add-to-playlist dropdown">
								<a class="dropdown-toggle" href="/react2/movie/the-big-sick/" data-toggle="dropdown">+ Playlist</a>
								<div class="dropdown-menu">
									<a class="login-link" href="https://demo3.madrasthemes.com/vodi-demos/main/my-account/movie-playlists/">Sign in to add this movie to a playlist.</a>
								</div>
							</div>
						</div>
					</div>
					<div class="movie__review-info"> <a href="/react2/movie/the-big-sick/#reviews" class="avg-rating">
						<span class="rating-with-count">
							<svg class="vodi-svg" width="40px" height="39px" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 39"><title>play</title><path fill-rule="evenodd" d="M19.633,-0.000 C21.509,0.035 21.530,1.174 22.167,2.414 C23.329,4.679 24.406,7.067 25.572,9.338 C25.853,9.886 26.431,11.640 26.918,11.834 C27.486,12.203 29.345,12.109 30.165,12.316 C32.170,12.825 34.489,12.860 36.500,13.364 C37.516,13.618 38.689,13.413 39.430,13.927 C39.689,14.107 39.770,14.504 39.984,14.732 C40.047,16.499 39.096,16.843 38.163,17.792 C36.473,19.509 34.784,21.227 33.095,22.944 C32.585,23.462 31.092,24.543 31.036,25.359 C31.423,25.951 31.307,27.455 31.511,28.258 C32.138,30.727 32.213,33.522 32.857,35.987 C33.142,37.078 33.016,38.241 32.303,38.724 C31.108,39.533 29.632,38.193 28.819,37.758 C26.695,36.623 24.601,35.624 22.483,34.457 C21.979,34.179 20.607,33.178 20.108,33.088 C19.748,33.023 18.163,34.107 17.812,34.296 C15.557,35.505 13.340,36.640 11.080,37.839 C10.548,38.120 9.180,39.226 8.309,38.966 C6.955,38.558 6.874,36.993 7.280,35.423 C7.716,33.733 7.697,31.880 8.151,30.109 C8.527,28.642 8.907,26.529 9.022,24.957 C8.092,24.344 7.202,23.107 6.408,22.300 C4.760,20.625 3.059,18.990 1.340,17.389 C0.646,16.742 -0.578,15.515 0.311,14.249 C0.915,13.388 2.364,13.656 3.557,13.364 C6.678,12.599 10.114,12.468 13.298,11.834 C14.186,9.747 15.306,7.711 16.307,5.716 C16.954,4.426 17.496,3.163 18.128,1.931 C18.334,1.531 18.358,1.093 18.603,0.724 C18.845,0.362 19.299,0.273 19.633,-0.000 Z"></path></svg> <span class="avg-rating-number"> 5.0</span>
						</span>
						<span class="rating-number-with-text">
							<span class="avg-rating-number"> 5.0</span>
							<span class="avg-rating-text">
								<span>1</span> Vote </span>
							</span>
						</a>
						<div class="viewers-count">
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="page-control-bar-bottom"> <p class="masvideos-result-count masvideos-movies-result-count">
Showing all 11 results </p>
</div>
</div>
<div id="secondary" class="widget-area sidebar-area movie-sidebar" role="complementary">
	<div class="widget-area-inner">
		<div class="widget widget_vodi_movies_filter">
			<div id="masvideos_movies_filter_widget-2" class="widget masvideos widget_layered_nav masvideos-movies-filter-widget">
				<div class="widget-header"><span class="widget-title">Categories</span>
				</div><ul class="masvideos-widget-movies-layered-nav-list">
					<li class="masvideos-widget-movies-layered-nav-list__item masvideos-layered-nav-term ">
						<a rel="nofollow" href="/react2/movies/?rating_filter=9,5&amp;filter_genre=action&amp;query_type_genre=or">Action</a>
					</li>
					<li class="masvideos-widget-movies-layered-nav-list__item masvideos-layered-nav-term ">
						<a rel="nofollow" href="/react2/movies/?rating_filter=9,5&amp;filter_genre=adventure&amp;query_type_genre=or">Adventure</a>
					</li>
					<li class="masvideos-widget-movies-layered-nav-list__item masvideos-layered-nav-term ">
						<a rel="nofollow" href="/react2/movies/?rating_filter=9,5&amp;filter_genre=comedy&amp;query_type_genre=or">Comedy</a>
					</li>
					<li class="masvideos-widget-movies-layered-nav-list__item masvideos-layered-nav-term ">
						<a rel="nofollow" href="/react2/movies/?rating_filter=9,5&amp;filter_genre=documentary&amp;query_type_genre=or">Documentary</a>
					</li>
					<li class="masvideos-widget-movies-layered-nav-list__item masvideos-layered-nav-term ">
						<a rel="nofollow" href="/react2/movies/?rating_filter=9,5&amp;filter_genre=family&amp;query_type_genre=or">Family</a>
					</li>
					<li class="masvideos-widget-movies-layered-nav-list__item masvideos-layered-nav-term ">
						<a rel="nofollow" href="/react2/movies/?rating_filter=9,5&amp;filter_genre=fantacy&amp;query_type_genre=or">Fantacy</a>
					</li>
					<li class="masvideos-widget-movies-layered-nav-list__item masvideos-layered-nav-term ">
						<a rel="nofollow" href="/react2/movies/?rating_filter=9,5&amp;filter_genre=history&amp;query_type_genre=or">History</a>
					</li>
					<li class="masvideos-widget-movies-layered-nav-list__item masvideos-layered-nav-term ">
						<a rel="nofollow" href="/react2/movies/?rating_filter=9,5&amp;filter_genre=horror&amp;query_type_genre=or">Horror</a>
					</li>
					<li class="masvideos-widget-movies-layered-nav-list__item masvideos-layered-nav-term ">
						<a rel="nofollow" href="/react2/movies/?rating_filter=9,5&amp;filter_genre=mystery&amp;query_type_genre=or">Mystery</a>
					</li>
					<li class="masvideos-widget-movies-layered-nav-list__item masvideos-layered-nav-term ">
						<a rel="nofollow" href="/react2/movies/?rating_filter=9,5&amp;filter_genre=romance&amp;query_type_genre=or">Romance</a>
					</li>
					<li class="masvideos-widget-movies-layered-nav-list__item masvideos-layered-nav-term ">
						<a rel="nofollow" href="/react2/movies/?rating_filter=9,5&amp;filter_genre=thriller&amp;query_type_genre=or">Thriller</a>
					</li>
				</ul>
			</div>
			<div id="masvideos_movies_tags_filter_widget-2" class="widget masvideos widget_layered_nav masvideos-movies-tags-filter-widget"><ul class="masvideos-widget-movies-layered-nav-list">
				<li class="masvideos-widget-movies-layered-nav-list__item masvideos-layered-nav-term "><i class="far fa-window-maximize"></i>
					<a rel="nofollow" href="/react2/movies/?rating_filter=9,5&amp;filter_tag=4k-ultra&amp;query_type_tag=or">4K Ultra</a>
				</li>
				<li class="masvideos-widget-movies-layered-nav-list__item masvideos-layered-nav-term "><i class="fas fa-chess-knight"></i>
					<a rel="nofollow" href="/react2/movies/?rating_filter=9,5&amp;filter_tag=brother&amp;query_type_tag=or">Brother</a>
				</li>
			</ul>
		</div>
		<div id="masvideos_movies_year_filter-2" class="widget masvideos masvideos-widget_movies_year_filter">
			<div class="widget-header"><span class="widget-title">Movies by Year</span>
			</div>
			<ul>
				<li class="masvideos-layered-nav-movies-year">
					<a href="/vodi-demos/main/movies/?rating_filter=9%2C5&amp;year_filter=2020"><span>2020</span> </a>
				</li>
				<li class="masvideos-layered-nav-movies-year">
					<a href="/vodi-demos/main/movies/?rating_filter=9%2C5&amp;year_filter=2019"><span>2019</span> </a>
				</li>
				<li class="masvideos-layered-nav-movies-year">
					<a href="/vodi-demos/main/movies/?rating_filter=9%2C5&amp;year_filter=2018"><span>2018</span> </a>
				</li>
				<li class="masvideos-layered-nav-movies-year">
					<a href="/vodi-demos/main/movies/?rating_filter=9%2C5&amp;year_filter=2017"><span>2017</span> </a>
				</li>
				<li class="masvideos-layered-nav-movies-year">
					<a href="/vodi-demos/main/movies/?rating_filter=9%2C5&amp;year_filter=2016"><span>2016</span> </a>
				</li>
				<li class="masvideos-layered-nav-movies-year">
					<a href="/vodi-demos/main/movies/?rating_filter=9%2C5&amp;year_filter=2015"><span>2015</span> </a>
				</li>
				<li class="masvideos-layered-nav-movies-year">
					<a href="/vodi-demos/main/movies/?rating_filter=9%2C5&amp;year_filter=2014"><span>2014</span> </a>
				</li>
				<li class="masvideos-layered-nav-movies-year">
					<a href="/vodi-demos/main/movies/?rating_filter=9%2C5&amp;year_filter=2013"><span>2013</span> </a>
				</li>
				<li class="masvideos-layered-nav-movies-year">
					<a href="/vodi-demos/main/movies/?rating_filter=9%2C5&amp;year_filter=2012"><span>2012</span> </a>
				</li>
				<li class="masvideos-layered-nav-movies-year">
					<a href="/vodi-demos/main/movies/?rating_filter=9%2C5&amp;year_filter=2011"><span>2011</span> </a>
				</li>
				<li class="masvideos-layered-nav-movies-year">
					<a href="/vodi-demos/main/movies/?rating_filter=9%2C5&amp;year_filter=2010"><span>2010</span> </a>
				</li>
			</ul>
		</div>
		<div id="masvideos_movies_rating_filter-2" class="widget masvideos movies_widget_rating_filter">
			<div class="widget-header"><span class="widget-title">Filter by Rating</span>
			</div>
			<ul>
				<li class="masvideos-layered-nav-rating">
					<a href="/vodi-demos/main/movies/?rating_filter=9,5,10">
						<div class="star-rating">
							<div class="star-rating"><span class="screen-reader-text">10.0 rating</span>
								<div class="star star-full" aria-hidden="true">
								</div>
								<div class="star star-full" aria-hidden="true">
								</div>
								<div class="star star-full" aria-hidden="true">
								</div>
								<div class="star star-full" aria-hidden="true">
								</div>
								<div class="star star-full" aria-hidden="true">
								</div>
								<div class="star star-full" aria-hidden="true">
								</div>
								<div class="star star-full" aria-hidden="true">
								</div>
								<div class="star star-full" aria-hidden="true">
								</div>
								<div class="star star-full" aria-hidden="true">
								</div>
								<div class="star star-full" aria-hidden="true">
								</div>
							</div>
						</div> (2)
					</a>
				</li>
				<li class="masvideos-layered-nav-rating chosen">
					<a href="/vodi-demos/main/movies/?rating_filter=5">
						<div class="star-rating">
							<div class="star-rating"><span class="screen-reader-text">9.0 rating</span>
								<div class="star star-full" aria-hidden="true">
								</div>
								<div class="star star-full" aria-hidden="true">
								</div>
								<div class="star star-full" aria-hidden="true">
								</div>
								<div class="star star-full" aria-hidden="true">
								</div>
								<div class="star star-full" aria-hidden="true">
								</div>
								<div class="star star-full" aria-hidden="true">
								</div>
								<div class="star star-full" aria-hidden="true">
								</div>
								<div class="star star-full" aria-hidden="true">
								</div>
								<div class="star star-full" aria-hidden="true">
								</div>
								<div class="star star-empty" aria-hidden="true">
								</div>
							</div>
						</div> (10)</a>
					</li>
					<li class="masvideos-layered-nav-rating">
						<a href="/vodi-demos/main/movies/?rating_filter=9,5,8">
							<div class="star-rating">
								<div class="star-rating"><span class="screen-reader-text">8.0 rating</span>
									<div class="star star-full" aria-hidden="true">
									</div>
									<div class="star star-full" aria-hidden="true">
									</div>
									<div class="star star-full" aria-hidden="true">
									</div>
									<div class="star star-full" aria-hidden="true">
									</div>
									<div class="star star-full" aria-hidden="true">
									</div>
									<div class="star star-full" aria-hidden="true">
									</div>
									<div class="star star-full" aria-hidden="true">
									</div>
									<div class="star star-full" aria-hidden="true">
									</div>
									<div class="star star-empty" aria-hidden="true">
									</div>
									<div class="star star-empty" aria-hidden="true">
									</div>
								</div>
							</div> (27)
						</a>
					</li>
					<li class="masvideos-layered-nav-rating">
						<a href="/vodi-demos/main/movies/?rating_filter=9,5,7">
							<div class="star-rating">
								<div class="star-rating"><span class="screen-reader-text">7.0 rating</span>
									<div class="star star-full" aria-hidden="true">
									</div>
									<div class="star star-full" aria-hidden="true">
									</div>
									<div class="star star-full" aria-hidden="true">
									</div>
									<div class="star star-full" aria-hidden="true">
									</div>
									<div class="star star-full" aria-hidden="true">
									</div>
									<div class="star star-full" aria-hidden="true">
									</div>
									<div class="star star-full" aria-hidden="true">
									</div>
									<div class="star star-empty" aria-hidden="true">
									</div>
									<div class="star star-empty" aria-hidden="true">
									</div>
									<div class="star star-empty" aria-hidden="true">
									</div>
								</div>
							</div> (13)
						</a>
					</li>
					<li class="masvideos-layered-nav-rating chosen">
						<a href="/vodi-demos/main/movies/?rating_filter=9">
							<div class="star-rating">
								<div class="star-rating"><span class="screen-reader-text">5.0 rating</span>
									<div class="star star-full" aria-hidden="true">
									</div>
									<div class="star star-full" aria-hidden="true">
									</div>
									<div class="star star-full" aria-hidden="true">
									</div>
									<div class="star star-full" aria-hidden="true">
									</div>
									<div class="star star-full" aria-hidden="true">
									</div>
									<div class="star star-empty" aria-hidden="true">
									</div>
									<div class="star star-empty" aria-hidden="true">
									</div>
									<div class="star star-empty" aria-hidden="true">
									</div>
									<div class="star star-empty" aria-hidden="true">
									</div>
									<div class="star star-empty" aria-hidden="true">
									</div>
								</div>
							</div> (1)
						</a>
					</li>
				</ul>
			</div>
		</div>
		<div id="masvideos_movies_widget-2" class="widget masvideos masvideos_movies_widget masvideos-movies-widget">
			<div class="widget-header"><span class="widget-title">Top 5 List</span>
			</div>
			<div class="masvideos masvideos-movies ">
				<div class="movies columns-1">
					<div class="movies__inner">
						<div class="post-720 movie type-movie status-publish has-post-thumbnail hentry movie_genre-action movie_genre-adventure movie_tag-brother movie_tag-king movie_tag-premieres movie_tag-viking">
							<div class="movie__poster">
								<a href="/react2/movie/euphoria/289096" class="masvideos-LoopMovie-link masvideos-loop-movie__link movie__link"><img width="300" height="450" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/23-300x450.jpg" class="movie__poster--image" alt="" srcset="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/23-300x450.jpg 300w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/23-66x98.jpg 66w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/23-200x300.jpg 200w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/23-600x900.jpg 600w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/23-150x225.jpg 150w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/23.jpg 668w" sizes="(max-width: 300px) 100vw, 300px">
								</a>
							</div>
							<div class="movie__body"><span class="movie__meta--release-year">2018</span>
								<a href="/react2/movie/euphoria/289096" class="masvideos-LoopMovie-link masvideos-loop-movie__link movie__link"><h3 class="masvideos-loop-movie__title  movie__title">Euphoria</h3>
								</a><span class="movie__meta--genre">
									<a href="/react2/genre/action/" rel="tag">Action</a>, <a href="/react2/genre/adventure/" rel="tag">Adventure</a></span>
								</div>
							</div>
							<div class="post-714 movie type-movie status-publish has-post-thumbnail hentry movie_genre-action movie_genre-documentary movie_tag-4k-ultra movie_tag-brother movie_tag-king movie_tag-premieres movie_music-alan-silvestri movie_photos-jack-kirby movie_photos-jim-starlin movie_photos-trent-opaloch">
								<div class="movie__poster">
									<a href="/react2/movie/paradigm-lost/" class="masvideos-LoopMovie-link masvideos-loop-movie__link movie__link"><img width="300" height="450" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/51-walk-hard-300x450.jpg" class="movie__poster--image" alt="" srcset="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/51-walk-hard-300x450.jpg 300w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/51-walk-hard-66x98.jpg 66w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/51-walk-hard-600x900.jpg 600w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/51-walk-hard-150x225.jpg 150w" sizes="(max-width: 300px) 100vw, 300px">
									</a>
								</div>
								<div class="movie__body"><span class="movie__meta--release-year">2017</span>
									<a href="/react2/movie/paradigm-lost/" class="masvideos-LoopMovie-link masvideos-loop-movie__link movie__link"><h3 class="masvideos-loop-movie__title  movie__title">Paradigm Lost</h3>
									</a><span class="movie__meta--genre">
										<a href="/react2/genre/action/" rel="tag">Action</a>, <a href="/react2/genre/documentary/" rel="tag">Documentary</a></span>
									</div>
								</div>
								<div class="post-334 movie type-movie status-publish has-post-thumbnail hentry movie_genre-action movie_genre-fantacy movie_tag-4k-ultra movie_tag-brother movie_tag-premieres movie_tag-viking">
									<div class="movie__poster">
										<a href="/react2/movie/every-day/289096" class="masvideos-LoopMovie-link masvideos-loop-movie__link movie__link"><img width="300" height="450" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/42-nim-project-300x450.jpg" class="movie__poster--image" alt="" srcset="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/42-nim-project-300x450.jpg 300w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/42-nim-project-66x98.jpg 66w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/42-nim-project-600x900.jpg 600w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/42-nim-project-150x225.jpg 150w" sizes="(max-width: 300px) 100vw, 300px">
										</a>
									</div>
									<div class="movie__body"><span class="movie__meta--release-year">2018</span>
										<a href="/react2/movie/every-day/289096" class="masvideos-LoopMovie-link masvideos-loop-movie__link movie__link"><h3 class="masvideos-loop-movie__title  movie__title">Every Day</h3>
										</a><span class="movie__meta--genre">
											<a href="/react2/genre/action/" rel="tag">Action</a>, <a href="/react2/genre/fantacy/" rel="tag">Fantacy</a></span>
										</div>
									</div>
									<div class="post-332 movie type-movie status-publish has-post-thumbnail hentry movie_genre-action movie_genre-comedy movie_tag-brother movie_tag-king movie_tag-premieres movie_tag-viking">
										<div class="movie__poster">
											<a href="/react2/movie/freak-show/" class="masvideos-LoopMovie-link masvideos-loop-movie__link movie__link"><img width="300" height="450" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/41-na-ostrzu-300x450.jpg" class="movie__poster--image" alt="" srcset="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/41-na-ostrzu-300x450.jpg 300w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/41-na-ostrzu-66x98.jpg 66w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/41-na-ostrzu-600x900.jpg 600w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/41-na-ostrzu-150x225.jpg 150w" sizes="(max-width: 300px) 100vw, 300px">
											</a>
										</div>
										<div class="movie__body"><span class="movie__meta--release-year">2018</span>
											<a href="/react2/movie/freak-show/" class="masvideos-LoopMovie-link masvideos-loop-movie__link movie__link"><h3 class="masvideos-loop-movie__title  movie__title">Freak Show</h3>
											</a><span class="movie__meta--genre">
												<a href="/react2/genre/action/" rel="tag">Action</a>, <a href="/react2/genre/comedy/" rel="tag">Comedy</a></span>
											</div>
										</div>
										<div class="post-330 movie type-movie status-publish has-post-thumbnail hentry movie_genre-action movie_genre-thriller movie_tag-4k-ultra movie_tag-brother movie_tag-premieres movie_tag-viking movie_music-alan-silvestri movie_photos-jack-kirby movie_photos-jim-starlin movie_photos-trent-opaloch">
											<div class="movie__poster">
												<a href="/react2/movie/the-tale/289096" class="masvideos-LoopMovie-link masvideos-loop-movie__link movie__link"><img width="300" height="450" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/40-murder-by-death-300x450.jpg" class="movie__poster--image" alt="" srcset="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/40-murder-by-death-300x450.jpg 300w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/40-murder-by-death-66x98.jpg 66w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/40-murder-by-death-600x900.jpg 600w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/40-murder-by-death-150x225.jpg 150w" sizes="(max-width: 300px) 100vw, 300px">
												</a>
											</div>
											<div class="movie__body"><span class="movie__meta--release-year">2018</span>
												<a href="/react2/movie/the-tale/289096" class="masvideos-LoopMovie-link masvideos-loop-movie__link movie__link"><h3 class="masvideos-loop-movie__title  movie__title">The Tale</h3>
												</a><span class="movie__meta--genre">
													<a href="/react2/genre/action/" rel="tag">Action</a>, <a href="/react2/genre/thriller/" rel="tag">Thriller</a></span>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		@endsection