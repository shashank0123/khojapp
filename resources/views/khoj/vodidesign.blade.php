@extends('layouts.vodi')
@section('contents')
<div id="content" class="site-content" tabindex="-1">
	<div class="container">
		<div class="site-content__inner">
			<div id="primary" class="content-area">
				<main id="main" class="site-main" role="main">
					<article id="post-343" class="post-343 page type-page status-publish hentry">
						<div class="page__content">
							<div id="movies-sliders-5f87f9fc10614" class="movies-sliders style-v1">
								<div class="movies-sliders-single-content" data-ride="vodi-slick-carousel" data-wrap=".movies-sliders-single-content__inner" data-slick="{&quot;infinite&quot;:false,&quot;slidesToShow&quot;:1,&quot;slidesToScroll&quot;:1,&quot;arrows&quot;:false,&quot;asNavFor&quot;:&quot;#movies-sliders-5f87f9fc10614 .movies-sliders-gallery-images__inner&quot;}">
									<div class="movies-sliders-single-content__inner"  id="slidercontainer">
										
										<div class="movie-slide" style=" background-size: cover; background-image: url( https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/slider-8-1.jpg ); height: 676px;">
											<div class="container">
												<div class="single-featured-movie">
													<div class="single-featured-movie__inner">
														<a href="/react2/movie/american-made/" class="masvideos-LoopMovie-link masvideos-loop-movie__link movie__link"><h3 class="masvideos-loop-movie__title  movie__title">American Made</h3></a>
														<div class="movie__meta"><span class="movie__meta--release-year">2017</span><span class="movie__meta--genre"><a href="/react2/genre/moviecomedy/" rel="tag">Comedy</a></span><span class="movie__meta--duration">1hr 55 mins</span>
														</div>
														<div class="movie__actions"><a href="/react2/movie/american-made/" class="movie-actions--link_watch">Watch Now</a> 
															<div class="movie-actions--link_add-to-playlist dropdown">
																<a class="dropdown-toggle" href="/react2/movie/american-made/" data-toggle="dropdown">+ Playlist</a>
																<div class="dropdown-menu">
																	<a class="login-link" href="/login?movie-playlists/">Sign in to add this movie to a playlist.</a> 
																</div>
															</div>
														</div> 
													</div>
												</div>
											</div>
										</div>
										


										<div class="movie-slide" style=" background-size: cover; background-image: url( https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/slider-1-1.jpg ); height: 676px;">
											<div class="container">
												<div class="single-featured-movie">
													<div class="single-featured-movie__inner">
														<a href="/react2/movie/the-convenient-groom/" class="masvideos-LoopMovie-link masvideos-loop-movie__link movie__link"><h3 class="masvideos-loop-movie__title  movie__title">The Convenient Groom</h3></a>
														<div class="movie__meta"><span class="movie__meta--release-year">2016</span><span class="movie__meta--genre"><a href="/react2/genre/movieaction/" rel="tag">Action</a>, <a href="/react2/genre/movieadventure/" rel="tag">Adventure</a>, <a href="/react2/genre/movieromance/" rel="tag">Romance</a></span><span class="movie__meta--duration">1hr 24mins</span>
														</div>
														<div class="movie__actions"><a href="/react2/movie/the-convenient-groom/" class="movie-actions--link_watch">Watch Now</a> 
															<div class="movie-actions--link_add-to-playlist dropdown">
																<a class="dropdown-toggle" href="/react2/movie/the-convenient-groom/" data-toggle="dropdown">+ Playlist</a>
																<div class="dropdown-menu">
																	<a class="login-link" href="/login?movie-playlists/">Sign in to add this movie to a playlist.</a> 
																</div>
															</div>
														</div> 
													</div>
												</div>
											</div>
										</div>
										<div class="movie-slide" style=" background-size: cover; background-image: url( https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/slider-3.jpg ); height: 676px;">
											<div class="container">
												<div class="single-featured-movie">
													<div class="single-featured-movie__inner">
														<a href="/react2/movie/pacific-rim-uprising/" class="masvideos-LoopMovie-link masvideos-loop-movie__link movie__link"><h3 class="masvideos-loop-movie__title  movie__title">Pacific Rim: Uprising</h3></a>
														<div class="movie__meta"><span class="movie__meta--release-year">2018</span><span class="movie__meta--genre"><a href="/react2/genre/movieaction/" rel="tag">Action</a>, <a href="/react2/genre/moviesci-fi/" rel="tag">Sci-Fi</a></span><span class="movie__meta--duration">1hr 51 mins</span>
														</div>
														<div class="movie__actions"><a href="/react2/movie/pacific-rim-uprising/" class="movie-actions--link_watch">Watch Now</a> 
															<div class="movie-actions--link_add-to-playlist dropdown">
																<a class="dropdown-toggle" href="/react2/movie/pacific-rim-uprising/" data-toggle="dropdown">+ Playlist</a>
																<div class="dropdown-menu">
																	<a class="login-link" href="/login?movie-playlists/">Sign in to add this movie to a playlist.</a> 
																</div>
															</div>
														</div> 
													</div>
												</div>
											</div>
										</div>
										<div class="movie-slide" style=" background-size: cover; background-image: url( https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/slider-6-1.jpg ); height: 676px;">
											<div class="container">
												<div class="single-featured-movie">
													<div class="single-featured-movie__inner">
														<a href="/react2/movie/black-mirror/" class="masvideos-LoopMovie-link masvideos-loop-movie__link movie__link"><h3 class="masvideos-loop-movie__title  movie__title">Black Mirror</h3></a>
														<div class="movie__meta"><span class="movie__meta--release-year">2018</span><span class="movie__meta--genre"><a href="/react2/genre/movieaction/" rel="tag">Action</a>, <a href="/react2/genre/moviemystery/" rel="tag">Mystery</a></span><span class="movie__meta--duration">30min</span>
														</div>
														<div class="movie__actions"><a href="/react2/movie/black-mirror/" class="movie-actions--link_watch">Watch Now</a> 
															<div class="movie-actions--link_add-to-playlist dropdown">
																<a class="dropdown-toggle" href="/react2/movie/black-mirror/" data-toggle="dropdown">+ Playlist</a>
																<div class="dropdown-menu">
																	<a class="login-link" href="/login?movie-playlists/">Sign in to add this movie to a playlist.</a> 
																</div>
															</div>
														</div> 
													</div>
												</div>
											</div>
										</div>
										<div class="movie-slide" style=" background-size: cover; background-image: url( https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/slider-4-1.jpg ); height: 676px;">
											<div class="container">
												<div class="single-featured-movie">
													<div class="single-featured-movie__inner">
														<a href="/react2/movie/the-convenient-groom/" class="masvideos-LoopMovie-link masvideos-loop-movie__link movie__link"><h3 class="masvideos-loop-movie__title  movie__title">The Convenient Groom</h3></a>
														<div class="movie__meta"><span class="movie__meta--release-year">2016</span><span class="movie__meta--genre"><a href="/react2/genre/movieaction/" rel="tag">Action</a>, <a href="/react2/genre/movieadventure/" rel="tag">Adventure</a>, <a href="/react2/genre/movieromance/" rel="tag">Romance</a></span><span class="movie__meta--duration">1hr 24mins</span>
														</div>
														<div class="movie__actions"><a href="/react2/movie/the-convenient-groom/" class="movie-actions--link_watch">Watch Now</a> 
															<div class="movie-actions--link_add-to-playlist dropdown">
																<a class="dropdown-toggle" href="/react2/movie/the-convenient-groom/" data-toggle="dropdown">+ Playlist</a>
																<div class="dropdown-menu">
																	<a class="login-link" href="/login?movie-playlists/">Sign in to add this movie to a playlist.</a> 
																</div>
															</div>
														</div> 
													</div>
												</div>
											</div>
										</div>
										<div class="movie-slide" style=" background-size: cover; background-image: url( https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/slider-7.jpg ); height: 676px;">
											<div class="container">
												<div class="single-featured-movie">
													<div class="single-featured-movie__inner">
														<a href="/react2/movie/paradigm-lost/" class="masvideos-LoopMovie-link masvideos-loop-movie__link movie__link"><h3 class="masvideos-loop-movie__title  movie__title">Paradigm Lost</h3></a>
														<div class="movie__meta"><span class="movie__meta--release-year">2017</span><span class="movie__meta--genre"><a href="/react2/genre/movieaction/" rel="tag">Action</a>, <a href="/react2/genre/moviedocumentary/" rel="tag">Documentary</a></span><span class="movie__meta--duration">1h 02 mins</span>
														</div>
														<div class="movie__actions"><a href="/react2/movie/paradigm-lost/" class="movie-actions--link_watch">Watch Now</a> 
															<div class="movie-actions--link_add-to-playlist dropdown">
																<a class="dropdown-toggle" href="/react2/movie/paradigm-lost/" data-toggle="dropdown">+ Playlist</a>
																<div class="dropdown-menu">
																	<a class="login-link" href="/login?movie-playlists/">Sign in to add this movie to a playlist.</a> 
																</div>
															</div>
														</div> 
													</div>
												</div>
											</div>
										</div>
										<div class="movie-slide" style=" background-size: cover; background-image: url( https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/slider-5.jpg ); height: 676px;">
											<div class="container">
												<div class="single-featured-movie">
													<div class="single-featured-movie__inner">
														<a href="/react2/movie/black-mirror/" class="masvideos-LoopMovie-link masvideos-loop-movie__link movie__link"><h3 class="masvideos-loop-movie__title  movie__title">Black Mirror</h3></a>
														<div class="movie__meta"><span class="movie__meta--release-year">2018</span><span class="movie__meta--genre"><a href="/react2/genre/movieaction/" rel="tag">Action</a>, <a href="/react2/genre/moviemystery/" rel="tag">Mystery</a></span><span class="movie__meta--duration">30min</span>
														</div>
														<div class="movie__actions"><a href="/react2/movie/black-mirror/" class="movie-actions--link_watch">Watch Now</a> 
															<div class="movie-actions--link_add-to-playlist dropdown">
																<a class="dropdown-toggle" href="/react2/movie/black-mirror/" data-toggle="dropdown">+ Playlist</a>
																<div class="dropdown-menu">
																	<a class="login-link" href="/login?movie-playlists/">Sign in to add this movie to a playlist.</a> 
																</div>
															</div>
														</div> 
													</div>
												</div>
											</div>
										</div>
										<div class="movie-slide" style=" background-size: cover; background-image: url( https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/slider-2.jpg ); height: 676px;">
											<div class="container">
												<div class="single-featured-movie">
													<div class="single-featured-movie__inner">
														<a href="/react2/movie/fantastic-beasts-and-where-to-find-them/" class="masvideos-LoopMovie-link masvideos-loop-movie__link movie__link"><h3 class="masvideos-loop-movie__title  movie__title">Fantastic Beasts and Where to Find Them</h3></a>
														<div class="movie__meta"><span class="movie__meta--release-year">2016</span><span class="movie__meta--genre"><a href="/react2/genre/movieaction/" rel="tag">Action</a>, <a href="/react2/genre/movieannimation/" rel="tag">Annimation</a>, <a href="/react2/genre/moviefamily/" rel="tag">Family</a></span><span class="movie__meta--duration">2hr 13 mins</span>
														</div>
														<div class="movie__actions"><a href="/react2/movie/fantastic-beasts-and-where-to-find-them/" class="movie-actions--link_watch">Watch Now</a> 
															<div class="movie-actions--link_add-to-playlist dropdown">
																<a class="dropdown-toggle" href="/react2/movie/fantastic-beasts-and-where-to-find-them/" data-toggle="dropdown">+ Playlist</a>
																<div class="dropdown-menu">
																	<a class="login-link" href="/login?movie-playlists/">Sign in to add this movie to a playlist.</a> 
																</div>
															</div>
														</div> 
													</div>
												</div>
											</div>
										</div> 
									</div>
								</div>
								<div class="movies-sliders-gallery-images-wrap">
									<h4 class="gallery-title">Todays Recomendation</h4> 
									<div class="movies-sliders-gallery-images" data-ride="vodi-slick-carousel" data-wrap=".movies-sliders-gallery-images__inner" data-slick="{&quot;infinite&quot;:false,&quot;slidesToShow&quot;:1,&quot;slidesToScroll&quot;:1,&quot;arrows&quot;:true,&quot;dots&quot;:false,&quot;focusOnSelect&quot;:true,&quot;touchMove&quot;:true,&quot;asNavFor&quot;:&quot;#movies-sliders-5f87f9fc10614 .movies-sliders-single-content__inner&quot;,&quot;rows&quot;:3,&quot;slidesPerRow&quot;:3,&quot;responsive&quot;:[{&quot;breakpoint&quot;:767,&quot;settings&quot;:{&quot;rows&quot;:1,&quot;slidesPerRow&quot;:1,&quot;slidesToShow&quot;:2}},{&quot;breakpoint&quot;:992,&quot;settings&quot;:{&quot;rows&quot;:1,&quot;slidesPerRow&quot;:1,&quot;slidesToShow&quot;:3,&quot;slidesToScroll&quot;:3}},{&quot;breakpoint&quot;:1200,&quot;settings&quot;:{&quot;rows&quot;:1,&quot;slidesPerRow&quot;:1,&quot;slidesToShow&quot;:4,&quot;slidesToScroll&quot;:4}}]}">
										<div class="movies-sliders-gallery-images__inner">
											<figure class="movie-slide-gallery-image" data-thumb="0">
												<img width="235" height="147" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/s-8.jpg" class="movsie-slide-gallery-image" alt="" /> </figure><figure class="movie-slide-gallery-image" data-thumb="1">
													<img width="235" height="147" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/s-4.jpg" class="movsie-slide-gallery-image" alt="" /> </figure><figure class="movie-slide-gallery-image" data-thumb="2">
														<img width="235" height="147" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/s-3.jpg" class="movsie-slide-gallery-image" alt="" /> </figure><figure class="movie-slide-gallery-image" data-thumb="3">
															<img width="235" height="147" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/s-1.jpg" class="movsie-slide-gallery-image" alt="" /> </figure><figure class="movie-slide-gallery-image" data-thumb="4">
																<img width="235" height="147" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/s-6.jpg" class="movsie-slide-gallery-image" alt="" /> </figure><figure class="movie-slide-gallery-image" data-thumb="5">
																	<img width="235" height="147" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/s-7.jpg" class="movsie-slide-gallery-image" alt="" /> </figure><figure class="movie-slide-gallery-image" data-thumb="6">
																		<img width="235" height="147" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/s-5.jpg" class="movsie-slide-gallery-image" alt="" /> </figure><figure class="movie-slide-gallery-image" data-thumb="7">
																			<img width="235" height="147" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/s-2.jpg" class="movsie-slide-gallery-image" alt="" /> </figure> 
																		</div>
																	</div>
																</div>
															</div>
															<section class="home-section home-movie-section-aside-header has-bg-color dark" style="padding-bottom: 13px; padding-top: 9px; ">
																<div class="container">
																	<div class="home-movie-section-aside-header__inner">
																		<div class="masvideos masvideos-movies ">
																			<div class="movies columns-7">
																				<div class="movies__inner" id="moviessection">
																					
																					
																				</div>
																			</div> 
																		</div>
																	</div>
																	<div class="home-section__footer-view-more-action">
																		<span class="home-section__footer-view-more-action__inner">
																			<a href="/react2/category/movie" class="home-section__footer-view-more-action--link">+ View more</a>
																		</span>
																	</div>
																</section>
																<section id="section-movies-carousel-aside-header-5f87f9fc21c6a" class="home-section section-movies-carousel-aside-header has-section-header has-bg-color light header-right" style="padding-bottom: 11px; ">
																	<div class="container">
																		<ul class="nav nav-tabs"><li class="nav-item"><a class="nav-link active" href="#">Today</a></li><li class="nav-item"><a class="nav-link" href="#">This week</a></li><li class="nav-item"><a class="nav-link" href="#">Last 30 days</a></li></ul> 
																		<div class="section-movies-carousel-aside-header__inner" id="specialevent">
																			<header class="home-section__header section-movies-carousel-aside-header__header"><h2 class="home-section__title">Romantic for Valentines Day</h2>
																				<div class="section-movies-carousel-aside-header__custom-arrows">
																				</div>
																				<div class="home-section__action"><a href="#" class="home-section__action-link">View All</a>
																				</div>
																			</header>
																			<div class="section-movies-carousel__carousel">
																				<div class="movies-carousel__inner" data-ride="vodi-slick-carousel" data-wrap=".movies__inner" data-slick="{&quot;slidesToShow&quot;:6,&quot;slidesToScroll&quot;:1,&quot;dots&quot;:false,&quot;arrows&quot;:true,&quot;autoplay&quot;:false,&quot;infinite&quot;:false,&quot;responsive&quot;:[{&quot;breakpoint&quot;:768,&quot;settings&quot;:{&quot;slidesToShow&quot;:2,&quot;slidesToScroll&quot;:1}},{&quot;breakpoint&quot;:992,&quot;settings&quot;:{&quot;slidesToShow&quot;:3,&quot;slidesToScroll&quot;:1}},{&quot;breakpoint&quot;:1200,&quot;settings&quot;:{&quot;slidesToShow&quot;:4,&quot;slidesToScroll&quot;:1}}],&quot;appendArrows&quot;:&quot;#section-movies-carousel-aside-header-5f87f9fc21c6a .section-movies-carousel-aside-header__custom-arrows&quot;}">
																					<div class="masvideos masvideos-movies ">
																						<div class="movies columns-6">
																							<div class="movies__inner">
																								<div class="post-2930 movie type-movie status-publish has-post-thumbnail hentry movie_genre-action">
																									<div class="movie__poster"><a href="/react2/movie/delta-bravo/" class="masvideos-LoopMovie-link masvideos-loop-movie__link movie__link"><img width="300" height="450" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/3-300x450.jpg" class="movie__poster--image" alt="" srcset="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/3-300x450.jpg 300w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/3-66x98.jpg 66w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/3-200x300.jpg 200w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/3-600x900.jpg 600w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/3-150x225.jpg 150w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/3.jpg 668w" sizes="(max-width: 300px) 100vw, 300px" /></a>
																									</div>
																									<div class="movie__body">
																										<div class="movie__info">
																											<div class="movie__info--head">
																												<div class="movie__meta"><span class="movie__meta--release-year">2017</span><span class="movie__meta--genre"><a href="/react2/genre/movieaction/" rel="tag">Action</a></span>
																												</div><a href="/react2/movie/delta-bravo/" class="masvideos-LoopMovie-link masvideos-loop-movie__link movie__link"><h3 class="masvideos-loop-movie__title  movie__title">Delta Bravo</h3></a>
																											</div> 
																											<div class="movie__short-description">
																												<div>
																													<p>strange black entity from another world bonds with Peter Parker and causes inner turmoil as he contends with new villains, temptations, and revenge.</p> 
																												</div> 
																											</div>
																											<div class="movie__actions"><a href="/react2/movie/delta-bravo/" class="movie-actions--link_watch">Watch Now</a> 
																												<div class="movie-actions--link_add-to-playlist dropdown">
																													<a class="dropdown-toggle" href="/react2/movie/delta-bravo/" data-toggle="dropdown">+ Playlist</a>
																													<div class="dropdown-menu">
																														<a class="login-link" href="/login?movie-playlists/">Sign in to add this movie to a playlist.</a> 
																													</div>
																												</div>

																											</div>
																										</div>
																										<div class="movie__review-info"> <a href="/react2/movie/delta-bravo/#reviews" class="avg-rating">
																											<span class="rating-with-count">
																												<svg class="vodi-svg" width="40px" height="39px" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 39"><title>play</title><path fill-rule="evenodd" d="M19.633,-0.000 C21.509,0.035 21.530,1.174 22.167,2.414 C23.329,4.679 24.406,7.067 25.572,9.338 C25.853,9.886 26.431,11.640 26.918,11.834 C27.486,12.203 29.345,12.109 30.165,12.316 C32.170,12.825 34.489,12.860 36.500,13.364 C37.516,13.618 38.689,13.413 39.430,13.927 C39.689,14.107 39.770,14.504 39.984,14.732 C40.047,16.499 39.096,16.843 38.163,17.792 C36.473,19.509 34.784,21.227 33.095,22.944 C32.585,23.462 31.092,24.543 31.036,25.359 C31.423,25.951 31.307,27.455 31.511,28.258 C32.138,30.727 32.213,33.522 32.857,35.987 C33.142,37.078 33.016,38.241 32.303,38.724 C31.108,39.533 29.632,38.193 28.819,37.758 C26.695,36.623 24.601,35.624 22.483,34.457 C21.979,34.179 20.607,33.178 20.108,33.088 C19.748,33.023 18.163,34.107 17.812,34.296 C15.557,35.505 13.340,36.640 11.080,37.839 C10.548,38.120 9.180,39.226 8.309,38.966 C6.955,38.558 6.874,36.993 7.280,35.423 C7.716,33.733 7.697,31.880 8.151,30.109 C8.527,28.642 8.907,26.529 9.022,24.957 C8.092,24.344 7.202,23.107 6.408,22.300 C4.760,20.625 3.059,18.990 1.340,17.389 C0.646,16.742 -0.578,15.515 0.311,14.249 C0.915,13.388 2.364,13.656 3.557,13.364 C6.678,12.599 10.114,12.468 13.298,11.834 C14.186,9.747 15.306,7.711 16.307,5.716 C16.954,4.426 17.496,3.163 18.128,1.931 C18.334,1.531 18.358,1.093 18.603,0.724 C18.845,0.362 19.299,0.273 19.633,-0.000 Z" /></svg> <span class="avg-rating-number"> 8.0</span>
																											</span>
																											<span class="rating-number-with-text">
																												<span class="avg-rating-number"> 8.0</span>
																												<span class="avg-rating-text">
																													<span>1</span> Vote </span>
																												</span>
																											</a>
																											<div class="viewers-count">
																											</div>
																										</div>
																									</div>
																								</div>

																							</div>
																						</div>
																					</div> 
																				</div>
																			</div>
																		</div>
																	</div>
																</section>
																<section class="home-section section-featured-movie dark" style="background-image: url( https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/05/2-home-v2-bg-new-1.jpg );">
																	<div class="container">
																		<div class="section-featured-movie__inner">
																			<div class="featured-movie__content">
																				<img width="187" height="59" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/05/4-home-v2-featured-logo-new1-1.png" class="featured-movie__content-image" alt="" /> 
																				<div class="masvideos masvideos-movies ">
																					<div class="movies columns-1">
																						<div class="movies__inner">
																							<div class="post-2930 movie type-movie status-publish has-post-thumbnail hentry movie_genre-action">
																								<div class="movie__poster"><a href="/react2/movie/delta-bravo/" class="masvideos-LoopMovie-link masvideos-loop-movie__link movie__link"><img width="300" height="450" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/3-300x450.jpg" class="movie__poster--image" alt="" srcset="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/3-300x450.jpg 300w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/3-66x98.jpg 66w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/3-200x300.jpg 200w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/3-600x900.jpg 600w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/3-150x225.jpg 150w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/3.jpg 668w" sizes="(max-width: 300px) 100vw, 300px" /></a>
																								</div>
																								<div class="movie__body">
																									<div class="movie__info">
																										<div class="movie__info--head">
																											<div class="movie__meta"><span class="movie__meta--release-year">2017</span><span class="movie__meta--genre"><a href="/react2/genre/movieaction/" rel="tag">Action</a></span>
																											</div><a href="/react2/movie/delta-bravo/" class="masvideos-LoopMovie-link masvideos-loop-movie__link movie__link"><h3 class="masvideos-loop-movie__title  movie__title">Delta Bravo</h3></a>
																										</div> 
																										<div class="movie__short-description">
																											<div>
																												<p>strange black entity from another world bonds with Peter Parker and causes inner turmoil as he contends with new villains, temptations, and revenge.</p> 
																											</div> 
																										</div>
																										<div class="movie__actions"><a href="/react2/movie/delta-bravo/" class="movie-actions--link_watch">Watch Now</a> 
																											<div class="movie-actions--link_add-to-playlist dropdown">
																												<a class="dropdown-toggle" href="/react2/movie/delta-bravo/" data-toggle="dropdown">+ Playlist</a>
																												<div class="dropdown-menu">
																													<a class="login-link" href="/login?movie-playlists/">Sign in to add this movie to a playlist.</a> 
																												</div>
																											</div>

																										</div>
																									</div>
																									<div class="movie__review-info"> <a href="/react2/movie/delta-bravo/#reviews" class="avg-rating">
																										<span class="rating-with-count">
																											<svg class="vodi-svg" width="40px" height="39px" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 39"><title>play</title><path fill-rule="evenodd" d="M19.633,-0.000 C21.509,0.035 21.530,1.174 22.167,2.414 C23.329,4.679 24.406,7.067 25.572,9.338 C25.853,9.886 26.431,11.640 26.918,11.834 C27.486,12.203 29.345,12.109 30.165,12.316 C32.170,12.825 34.489,12.860 36.500,13.364 C37.516,13.618 38.689,13.413 39.430,13.927 C39.689,14.107 39.770,14.504 39.984,14.732 C40.047,16.499 39.096,16.843 38.163,17.792 C36.473,19.509 34.784,21.227 33.095,22.944 C32.585,23.462 31.092,24.543 31.036,25.359 C31.423,25.951 31.307,27.455 31.511,28.258 C32.138,30.727 32.213,33.522 32.857,35.987 C33.142,37.078 33.016,38.241 32.303,38.724 C31.108,39.533 29.632,38.193 28.819,37.758 C26.695,36.623 24.601,35.624 22.483,34.457 C21.979,34.179 20.607,33.178 20.108,33.088 C19.748,33.023 18.163,34.107 17.812,34.296 C15.557,35.505 13.340,36.640 11.080,37.839 C10.548,38.120 9.180,39.226 8.309,38.966 C6.955,38.558 6.874,36.993 7.280,35.423 C7.716,33.733 7.697,31.880 8.151,30.109 C8.527,28.642 8.907,26.529 9.022,24.957 C8.092,24.344 7.202,23.107 6.408,22.300 C4.760,20.625 3.059,18.990 1.340,17.389 C0.646,16.742 -0.578,15.515 0.311,14.249 C0.915,13.388 2.364,13.656 3.557,13.364 C6.678,12.599 10.114,12.468 13.298,11.834 C14.186,9.747 15.306,7.711 16.307,5.716 C16.954,4.426 17.496,3.163 18.128,1.931 C18.334,1.531 18.358,1.093 18.603,0.724 C18.845,0.362 19.299,0.273 19.633,-0.000 Z" /></svg> <span class="avg-rating-number"> 8.0</span>
																										</span>
																										<span class="rating-number-with-text">
																											<span class="avg-rating-number"> 8.0</span>
																											<span class="avg-rating-text">
																												<span>1</span> Vote </span>
																											</span>
																										</a>
																										<div class="viewers-count">
																										</div>
																									</div>
																								</div>
																							</div>
																						</div>
																					</div>
																				</div>
																			</div>
																			<div class="featured-movie__action">
																				<a data-fancybox data-src="#hidden-movie-content-2930" class="single-movie-popup-btn featured-movie__action-icon" href="javascript:;">
																					<i class="fas fa-play"></i>
																				</a>
																				<div class="single-movie-popup" id="hidden-movie-content-2930">
																					<div class="movie__player"><iframe width="1024" height="574" src="https://www.youtube.com/embed/rKVEoyTedv4" frameborder="0" allowfullscreen></iframe>
																					</div>  </div>
																				</div>
																			</div>
																		</div>
																	</section>
																	<section id="section-movies-carousel-aside-header-5f87f9fc27530" class="home-section section-movies-carousel-aside-header has-section-header has-bg-color dark header-right" style="padding-bottom: 9px; padding-top: 60px; ">
																		<div class="container">
																			<ul class="nav nav-tabs"><li class="nav-item"><a class="nav-link active" href="#">Today</a></li><li class="nav-item"><a class="nav-link" href="#">This week</a></li><li class="nav-item"><a class="nav-link" href="#">Last 30 days</a></li></ul> 
																			<div class="section-movies-carousel-aside-header__inner">
																				<header class="home-section__header section-movies-carousel-aside-header__header"><h2 class="home-section__title">Action &amp; <br>Drama Movies</h2>
																					<div class="section-movies-carousel-aside-header__custom-arrows">
																					</div>
																					<div class="home-section__action"><a href="#" class="home-section__action-link">View All</a>
																					</div></header>
																					<div class="section-movies-carousel__carousel">
																						<div class="movies-carousel__inner" data-ride="vodi-slick-carousel" data-wrap=".movies__inner" data-slick="{&quot;slidesToShow&quot;:6,&quot;slidesToScroll&quot;:1,&quot;dots&quot;:false,&quot;arrows&quot;:true,&quot;autoplay&quot;:false,&quot;infinite&quot;:false,&quot;responsive&quot;:[{&quot;breakpoint&quot;:768,&quot;settings&quot;:{&quot;slidesToShow&quot;:2,&quot;slidesToScroll&quot;:1}},{&quot;breakpoint&quot;:992,&quot;settings&quot;:{&quot;slidesToShow&quot;:3,&quot;slidesToScroll&quot;:1}},{&quot;breakpoint&quot;:1200,&quot;settings&quot;:{&quot;slidesToShow&quot;:4,&quot;slidesToScroll&quot;:1}}],&quot;appendArrows&quot;:&quot;#section-movies-carousel-aside-header-5f87f9fc27530 .section-movies-carousel-aside-header__custom-arrows&quot;}">
																							<div class="masvideos masvideos-movies ">
																								<div class="movies columns-6">
																									<div class="movies__inner">
																										<div class="post-722 movie type-movie status-publish has-post-thumbnail hentry movie_genre-action movie_genre-comedy movie_tag-4k-ultra movie_tag-brother movie_tag-premieres movie_tag-viking">
																											<div class="movie__poster"><a href="/react2/movie/oh-lucy/" class="masvideos-LoopMovie-link masvideos-loop-movie__link movie__link"><img width="300" height="450" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/33-a-woman-under-the-influence-300x450.jpg" class="movie__poster--image" alt="" srcset="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/33-a-woman-under-the-influence-300x450.jpg 300w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/33-a-woman-under-the-influence-66x98.jpg 66w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/33-a-woman-under-the-influence-600x900.jpg 600w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/33-a-woman-under-the-influence-150x225.jpg 150w" sizes="(max-width: 300px) 100vw, 300px" /></a>
																											</div>
																											<div class="movie__body">
																												<div class="movie__info">
																													<div class="movie__info--head">
																														<div class="movie__meta"><span class="movie__meta--release-year">2018</span><span class="movie__meta--genre"><a href="/react2/genre/movieaction/" rel="tag">Action</a>, <a href="/react2/genre/moviecomedy/" rel="tag">Comedy</a></span>
																														</div><a href="/react2/movie/oh-lucy/" class="masvideos-LoopMovie-link masvideos-loop-movie__link movie__link"><h3 class="masvideos-loop-movie__title  movie__title">Oh Lucy</h3></a>
																													</div> 
																													<div class="movie__short-description">
																														<div>
																															<p>A lonely woman living in Tokyo decides to take an English class where she discovers her alter ego, Lucy.</p> 
																														</div> 
																													</div>
																													<div class="movie__actions"><a href="/react2/movie/oh-lucy/" class="movie-actions--link_watch">Watch Now</a> 
																														<div class="movie-actions--link_add-to-playlist dropdown">
																															<a class="dropdown-toggle" href="/react2/movie/oh-lucy/" data-toggle="dropdown">+ Playlist</a>
																															<div class="dropdown-menu">
																																<a class="login-link" href="/login?movie-playlists/">Sign in to add this movie to a playlist.</a> 
																															</div>
																														</div>

																													</div>
																												</div>
																												<div class="movie__review-info"> <a href="/react2/movie/oh-lucy/#reviews" class="avg-rating">
																													<span class="rating-with-count">
																														<svg class="vodi-svg" width="40px" height="39px" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 39"><title>play</title><path fill-rule="evenodd" d="M19.633,-0.000 C21.509,0.035 21.530,1.174 22.167,2.414 C23.329,4.679 24.406,7.067 25.572,9.338 C25.853,9.886 26.431,11.640 26.918,11.834 C27.486,12.203 29.345,12.109 30.165,12.316 C32.170,12.825 34.489,12.860 36.500,13.364 C37.516,13.618 38.689,13.413 39.430,13.927 C39.689,14.107 39.770,14.504 39.984,14.732 C40.047,16.499 39.096,16.843 38.163,17.792 C36.473,19.509 34.784,21.227 33.095,22.944 C32.585,23.462 31.092,24.543 31.036,25.359 C31.423,25.951 31.307,27.455 31.511,28.258 C32.138,30.727 32.213,33.522 32.857,35.987 C33.142,37.078 33.016,38.241 32.303,38.724 C31.108,39.533 29.632,38.193 28.819,37.758 C26.695,36.623 24.601,35.624 22.483,34.457 C21.979,34.179 20.607,33.178 20.108,33.088 C19.748,33.023 18.163,34.107 17.812,34.296 C15.557,35.505 13.340,36.640 11.080,37.839 C10.548,38.120 9.180,39.226 8.309,38.966 C6.955,38.558 6.874,36.993 7.280,35.423 C7.716,33.733 7.697,31.880 8.151,30.109 C8.527,28.642 8.907,26.529 9.022,24.957 C8.092,24.344 7.202,23.107 6.408,22.300 C4.760,20.625 3.059,18.990 1.340,17.389 C0.646,16.742 -0.578,15.515 0.311,14.249 C0.915,13.388 2.364,13.656 3.557,13.364 C6.678,12.599 10.114,12.468 13.298,11.834 C14.186,9.747 15.306,7.711 16.307,5.716 C16.954,4.426 17.496,3.163 18.128,1.931 C18.334,1.531 18.358,1.093 18.603,0.724 C18.845,0.362 19.299,0.273 19.633,-0.000 Z" /></svg> <span class="avg-rating-number"> 8.0</span>
																													</span>
																													<span class="rating-number-with-text">
																														<span class="avg-rating-number"> 8.0</span>
																														<span class="avg-rating-text">
																															<span>1</span> Vote </span>
																														</span>
																													</a>
																													<div class="viewers-count">
																													</div>
																												</div>
																											</div>
																										</div>
																										<div class="post-720 movie type-movie status-publish has-post-thumbnail hentry movie_genre-action movie_genre-adventure movie_tag-brother movie_tag-king movie_tag-premieres movie_tag-viking">
																											<div class="movie__poster"><a href="/react2/movie/euphoria/" class="masvideos-LoopMovie-link masvideos-loop-movie__link movie__link"><img width="300" height="450" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/23-300x450.jpg" class="movie__poster--image" alt="" srcset="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/23-300x450.jpg 300w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/23-66x98.jpg 66w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/23-200x300.jpg 200w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/23-600x900.jpg 600w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/23-150x225.jpg 150w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/23.jpg 668w" sizes="(max-width: 300px) 100vw, 300px" /></a>
																											</div>
																											<div class="movie__body">
																												<div class="movie__info">
																													<div class="movie__info--head">
																														<div class="movie__meta"><span class="movie__meta--release-year">2018</span><span class="movie__meta--genre"><a href="/react2/genre/movieaction/" rel="tag">Action</a>, <a href="/react2/genre/movieadventure/" rel="tag">Adventure</a></span>
																														</div><a href="/react2/movie/euphoria/" class="masvideos-LoopMovie-link masvideos-loop-movie__link movie__link"><h3 class="masvideos-loop-movie__title  movie__title">Euphoria</h3></a>
																													</div> 
																													<div class="movie__short-description">
																														<div>
																															<p>Sisters in conflict travelling through Europe toward a mystery destination</p> 
																														</div> 
																													</div>
																													<div class="movie__actions"><a href="/react2/movie/euphoria/" class="movie-actions--link_watch">Watch Now</a> 
																														<div class="movie-actions--link_add-to-playlist dropdown">
																															<a class="dropdown-toggle" href="/react2/movie/euphoria/" data-toggle="dropdown">+ Playlist</a>
																															<div class="dropdown-menu">
																																<a class="login-link" href="/login?movie-playlists/">Sign in to add this movie to a playlist.</a> 
																															</div>
																														</div>
																													</div>
																												</div>
																												<div class="movie__review-info"> <a href="/react2/movie/euphoria/#reviews" class="avg-rating">
																													<span class="rating-with-count">
																														<svg class="vodi-svg" width="40px" height="39px" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 39"><title>play</title><path fill-rule="evenodd" d="M19.633,-0.000 C21.509,0.035 21.530,1.174 22.167,2.414 C23.329,4.679 24.406,7.067 25.572,9.338 C25.853,9.886 26.431,11.640 26.918,11.834 C27.486,12.203 29.345,12.109 30.165,12.316 C32.170,12.825 34.489,12.860 36.500,13.364 C37.516,13.618 38.689,13.413 39.430,13.927 C39.689,14.107 39.770,14.504 39.984,14.732 C40.047,16.499 39.096,16.843 38.163,17.792 C36.473,19.509 34.784,21.227 33.095,22.944 C32.585,23.462 31.092,24.543 31.036,25.359 C31.423,25.951 31.307,27.455 31.511,28.258 C32.138,30.727 32.213,33.522 32.857,35.987 C33.142,37.078 33.016,38.241 32.303,38.724 C31.108,39.533 29.632,38.193 28.819,37.758 C26.695,36.623 24.601,35.624 22.483,34.457 C21.979,34.179 20.607,33.178 20.108,33.088 C19.748,33.023 18.163,34.107 17.812,34.296 C15.557,35.505 13.340,36.640 11.080,37.839 C10.548,38.120 9.180,39.226 8.309,38.966 C6.955,38.558 6.874,36.993 7.280,35.423 C7.716,33.733 7.697,31.880 8.151,30.109 C8.527,28.642 8.907,26.529 9.022,24.957 C8.092,24.344 7.202,23.107 6.408,22.300 C4.760,20.625 3.059,18.990 1.340,17.389 C0.646,16.742 -0.578,15.515 0.311,14.249 C0.915,13.388 2.364,13.656 3.557,13.364 C6.678,12.599 10.114,12.468 13.298,11.834 C14.186,9.747 15.306,7.711 16.307,5.716 C16.954,4.426 17.496,3.163 18.128,1.931 C18.334,1.531 18.358,1.093 18.603,0.724 C18.845,0.362 19.299,0.273 19.633,-0.000 Z" /></svg> <span class="avg-rating-number"> 9.0</span>
																													</span>
																													<span class="rating-number-with-text">
																														<span class="avg-rating-number"> 9.0</span>
																														<span class="avg-rating-text">
																															<span>1</span> Vote </span>
																														</span>
																													</a>
																													<div class="viewers-count">

																													</div>
																												</div>
																											</div>
																										</div>
																										<div class="post-718 movie type-movie status-publish has-post-thumbnail hentry movie_genre-action movie_genre-mystery movie_tag-brother movie_tag-hero movie_tag-premieres movie_tag-viking">
																											<div class="movie__poster"><a href="/react2/movie/black-mirror/" class="masvideos-LoopMovie-link masvideos-loop-movie__link movie__link"><img width="300" height="450" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/13-300x450.jpg" class="movie__poster--image" alt="" srcset="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/13-300x450.jpg 300w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/13-66x98.jpg 66w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/13-200x300.jpg 200w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/13-600x900.jpg 600w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/13-150x225.jpg 150w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/13.jpg 668w" sizes="(max-width: 300px) 100vw, 300px" /></a>
																											</div>
																											<div class="movie__body">
																												<div class="movie__info">
																													<div class="movie__info--head">
																														<div class="movie__meta"><span class="movie__meta--release-year">2018</span><span class="movie__meta--genre"><a href="/react2/genre/movieaction/" rel="tag">Action</a>, <a href="/react2/genre/moviemystery/" rel="tag">Mystery</a></span>
																														</div><a href="/react2/movie/black-mirror/" class="masvideos-LoopMovie-link masvideos-loop-movie__link movie__link"><h3 class="masvideos-loop-movie__title  movie__title">Black Mirror</h3></a>
																													</div> 
																													<div class="movie__short-description">
																														<div>
																															<p>In 1984, a young programmer begins to question reality as he works to adapt a fantasy novel into a video game.</p> 
																														</div> 
																													</div>
																													<div class="movie__actions"><a href="/react2/movie/black-mirror/" class="movie-actions--link_watch">Watch Now</a> 
																														<div class="movie-actions--link_add-to-playlist dropdown">
																															<a class="dropdown-toggle" href="/react2/movie/black-mirror/" data-toggle="dropdown">+ Playlist</a>
																															<div class="dropdown-menu">
																																<a class="login-link" href="/login?movie-playlists/">Sign in to add this movie to a playlist.</a> 
																															</div>
																														</div>

																													</div>
																												</div>
																												<div class="movie__review-info"> <a href="/react2/movie/black-mirror/#reviews" class="avg-rating">
																													<span class="rating-with-count">
																														<svg class="vodi-svg" width="40px" height="39px" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 39"><title>play</title><path fill-rule="evenodd" d="M19.633,-0.000 C21.509,0.035 21.530,1.174 22.167,2.414 C23.329,4.679 24.406,7.067 25.572,9.338 C25.853,9.886 26.431,11.640 26.918,11.834 C27.486,12.203 29.345,12.109 30.165,12.316 C32.170,12.825 34.489,12.860 36.500,13.364 C37.516,13.618 38.689,13.413 39.430,13.927 C39.689,14.107 39.770,14.504 39.984,14.732 C40.047,16.499 39.096,16.843 38.163,17.792 C36.473,19.509 34.784,21.227 33.095,22.944 C32.585,23.462 31.092,24.543 31.036,25.359 C31.423,25.951 31.307,27.455 31.511,28.258 C32.138,30.727 32.213,33.522 32.857,35.987 C33.142,37.078 33.016,38.241 32.303,38.724 C31.108,39.533 29.632,38.193 28.819,37.758 C26.695,36.623 24.601,35.624 22.483,34.457 C21.979,34.179 20.607,33.178 20.108,33.088 C19.748,33.023 18.163,34.107 17.812,34.296 C15.557,35.505 13.340,36.640 11.080,37.839 C10.548,38.120 9.180,39.226 8.309,38.966 C6.955,38.558 6.874,36.993 7.280,35.423 C7.716,33.733 7.697,31.880 8.151,30.109 C8.527,28.642 8.907,26.529 9.022,24.957 C8.092,24.344 7.202,23.107 6.408,22.300 C4.760,20.625 3.059,18.990 1.340,17.389 C0.646,16.742 -0.578,15.515 0.311,14.249 C0.915,13.388 2.364,13.656 3.557,13.364 C6.678,12.599 10.114,12.468 13.298,11.834 C14.186,9.747 15.306,7.711 16.307,5.716 C16.954,4.426 17.496,3.163 18.128,1.931 C18.334,1.531 18.358,1.093 18.603,0.724 C18.845,0.362 19.299,0.273 19.633,-0.000 Z" /></svg> <span class="avg-rating-number"> 8.0</span>
																													</span>
																													<span class="rating-number-with-text">
																														<span class="avg-rating-number"> 8.0</span>
																														<span class="avg-rating-text">
																															<span>3</span> Votes </span>
																														</span>
																													</a>
																													<div class="viewers-count">
																													</div>
																												</div>
																											</div>
																										</div>

																										<div class="post-714 movie type-movie status-publish has-post-thumbnail hentry movie_genre-action movie_genre-documentary movie_tag-4k-ultra movie_tag-brother movie_tag-king movie_tag-premieres movie_music-alan-silvestri movie_photos-jack-kirby movie_photos-jim-starlin movie_photos-trent-opaloch">
																											<span class="movie__badge"><span class="movie__badge--featured">Featured</span></span>
																											<div class="movie__poster"><a href="/react2/movie/paradigm-lost/" class="masvideos-LoopMovie-link masvideos-loop-movie__link movie__link"><img width="300" height="450" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/51-walk-hard-300x450.jpg" class="movie__poster--image" alt="" srcset="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/51-walk-hard-300x450.jpg 300w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/51-walk-hard-66x98.jpg 66w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/51-walk-hard-600x900.jpg 600w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/51-walk-hard-150x225.jpg 150w" sizes="(max-width: 300px) 100vw, 300px" /></a>
																											</div>
																											<div class="movie__body">
																												<div class="movie__info">
																													<div class="movie__info--head">
																														<div class="movie__meta"><span class="movie__meta--release-year">2017</span><span class="movie__meta--genre"><a href="/react2/genre/movieaction/" rel="tag">Action</a>, <a href="/react2/genre/moviedocumentary/" rel="tag">Documentary</a></span>
																														</div><a href="/react2/movie/paradigm-lost/" class="masvideos-LoopMovie-link masvideos-loop-movie__link movie__link"><h3 class="masvideos-loop-movie__title  movie__title">Paradigm Lost</h3></a>
																													</div> 
																													<div class="movie__short-description">
																														<div><p>Poor Boyz Productions presents a Kai Lenny &amp; Johnny DeCesare film, PARADIGM LOST in co-production with Red Bull Media House.</p>
																														</div> 
																													</div>
																													<div class="movie__actions"><a href="/react2/movie/paradigm-lost/" class="movie-actions--link_watch">Watch Now</a> 
																														<div class="movie-actions--link_add-to-playlist dropdown">
																															<a class="dropdown-toggle" href="/react2/movie/paradigm-lost/" data-toggle="dropdown">+ Playlist</a>
																															<div class="dropdown-menu">
																																<a class="login-link" href="/login?movie-playlists/">Sign in to add this movie to a playlist.</a> 
																															</div>
																														</div>

																													</div>
																												</div>
																												<div class="movie__review-info"> <a href="/react2/movie/paradigm-lost/#reviews" class="avg-rating">
																													<span class="rating-with-count">
																														<svg class="vodi-svg" width="40px" height="39px" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 39"><title>play</title><path fill-rule="evenodd" d="M19.633,-0.000 C21.509,0.035 21.530,1.174 22.167,2.414 C23.329,4.679 24.406,7.067 25.572,9.338 C25.853,9.886 26.431,11.640 26.918,11.834 C27.486,12.203 29.345,12.109 30.165,12.316 C32.170,12.825 34.489,12.860 36.500,13.364 C37.516,13.618 38.689,13.413 39.430,13.927 C39.689,14.107 39.770,14.504 39.984,14.732 C40.047,16.499 39.096,16.843 38.163,17.792 C36.473,19.509 34.784,21.227 33.095,22.944 C32.585,23.462 31.092,24.543 31.036,25.359 C31.423,25.951 31.307,27.455 31.511,28.258 C32.138,30.727 32.213,33.522 32.857,35.987 C33.142,37.078 33.016,38.241 32.303,38.724 C31.108,39.533 29.632,38.193 28.819,37.758 C26.695,36.623 24.601,35.624 22.483,34.457 C21.979,34.179 20.607,33.178 20.108,33.088 C19.748,33.023 18.163,34.107 17.812,34.296 C15.557,35.505 13.340,36.640 11.080,37.839 C10.548,38.120 9.180,39.226 8.309,38.966 C6.955,38.558 6.874,36.993 7.280,35.423 C7.716,33.733 7.697,31.880 8.151,30.109 C8.527,28.642 8.907,26.529 9.022,24.957 C8.092,24.344 7.202,23.107 6.408,22.300 C4.760,20.625 3.059,18.990 1.340,17.389 C0.646,16.742 -0.578,15.515 0.311,14.249 C0.915,13.388 2.364,13.656 3.557,13.364 C6.678,12.599 10.114,12.468 13.298,11.834 C14.186,9.747 15.306,7.711 16.307,5.716 C16.954,4.426 17.496,3.163 18.128,1.931 C18.334,1.531 18.358,1.093 18.603,0.724 C18.845,0.362 19.299,0.273 19.633,-0.000 Z" /></svg> <span class="avg-rating-number"> 9.0</span>
																													</span>
																													<span class="rating-number-with-text">
																														<span class="avg-rating-number"> 9.0</span>
																														<span class="avg-rating-text">
																															<span>2</span> Votes </span>
																														</span>
																													</a>
																													<div class="viewers-count">
																													</div>
																												</div>
																											</div>
																										</div>

																										<div class="post-710 movie type-movie status-publish has-post-thumbnail hentry movie_genre-action movie_genre-sport movie_tag-4k-ultra movie_tag-brother movie_tag-premieres movie_tag-viking">
																											<div class="movie__poster"><a href="/react2/movie/dirt/" class="masvideos-LoopMovie-link masvideos-loop-movie__link movie__link"><img width="300" height="450" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/49-the-sure-thing-300x450.jpg" class="movie__poster--image" alt="" srcset="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/49-the-sure-thing-300x450.jpg 300w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/49-the-sure-thing-66x98.jpg 66w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/49-the-sure-thing-600x900.jpg 600w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/49-the-sure-thing-150x225.jpg 150w" sizes="(max-width: 300px) 100vw, 300px" /></a>
																											</div>
																											<div class="movie__body">
																												<div class="movie__info">
																													<div class="movie__info--head">
																														<div class="movie__meta"><span class="movie__meta--release-year">2018</span><span class="movie__meta--genre"><a href="/react2/genre/movieaction/" rel="tag">Action</a>, <a href="/react2/genre/moviesport/" rel="tag">Sport</a></span>
																														</div><a href="/react2/movie/dirt/" class="masvideos-LoopMovie-link masvideos-loop-movie__link movie__link"><h3 class="masvideos-loop-movie__title  movie__title">Dirt</h3></a>
																													</div> 
																													<div class="movie__short-description">
																														<div>
																															<p>In search of a lifeline for his struggling off road racing team, a man takes on a young car thief looking for a second chance, but as their worlds collide, they must struggle to forge a successful alliance.</p> 
																														</div> 
																													</div>
																													<div class="movie__actions"><a href="/react2/movie/dirt/" class="movie-actions--link_watch">Watch Now</a> 
																														<div class="movie-actions--link_add-to-playlist dropdown">
																															<a class="dropdown-toggle" href="/react2/movie/dirt/" data-toggle="dropdown">+ Playlist</a>
																															<div class="dropdown-menu">
																																<a class="login-link" href="/login?movie-playlists/">Sign in to add this movie to a playlist.</a> 
																															</div>
																														</div>

																													</div>
																												</div>
																												<div class="movie__review-info"> <a href="/react2/movie/dirt/#reviews" class="avg-rating">
																													<span class="rating-with-count">
																														<svg class="vodi-svg" width="40px" height="39px" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 39"><title>play</title><path fill-rule="evenodd" d="M19.633,-0.000 C21.509,0.035 21.530,1.174 22.167,2.414 C23.329,4.679 24.406,7.067 25.572,9.338 C25.853,9.886 26.431,11.640 26.918,11.834 C27.486,12.203 29.345,12.109 30.165,12.316 C32.170,12.825 34.489,12.860 36.500,13.364 C37.516,13.618 38.689,13.413 39.430,13.927 C39.689,14.107 39.770,14.504 39.984,14.732 C40.047,16.499 39.096,16.843 38.163,17.792 C36.473,19.509 34.784,21.227 33.095,22.944 C32.585,23.462 31.092,24.543 31.036,25.359 C31.423,25.951 31.307,27.455 31.511,28.258 C32.138,30.727 32.213,33.522 32.857,35.987 C33.142,37.078 33.016,38.241 32.303,38.724 C31.108,39.533 29.632,38.193 28.819,37.758 C26.695,36.623 24.601,35.624 22.483,34.457 C21.979,34.179 20.607,33.178 20.108,33.088 C19.748,33.023 18.163,34.107 17.812,34.296 C15.557,35.505 13.340,36.640 11.080,37.839 C10.548,38.120 9.180,39.226 8.309,38.966 C6.955,38.558 6.874,36.993 7.280,35.423 C7.716,33.733 7.697,31.880 8.151,30.109 C8.527,28.642 8.907,26.529 9.022,24.957 C8.092,24.344 7.202,23.107 6.408,22.300 C4.760,20.625 3.059,18.990 1.340,17.389 C0.646,16.742 -0.578,15.515 0.311,14.249 C0.915,13.388 2.364,13.656 3.557,13.364 C6.678,12.599 10.114,12.468 13.298,11.834 C14.186,9.747 15.306,7.711 16.307,5.716 C16.954,4.426 17.496,3.163 18.128,1.931 C18.334,1.531 18.358,1.093 18.603,0.724 C18.845,0.362 19.299,0.273 19.633,-0.000 Z" /></svg> <span class="avg-rating-number"> 8.0</span>
																													</span>
																													<span class="rating-number-with-text">
																														<span class="avg-rating-number"> 8.0</span>
																														<span class="avg-rating-text">
																															<span>1</span> Vote </span>
																														</span>
																													</a>
																													<div class="viewers-count">
																													</div>
																												</div>
																											</div>
																										</div>

																										<div class="post-708 movie type-movie status-publish has-post-thumbnail hentry movie_genre-action movie_genre-annimation movie_genre-family movie_tag-4k-ultra movie_tag-brother movie_tag-dubbing movie_tag-premieres">
																											<span class="movie__badge"><span class="movie__badge--featured">Featured</span></span>
																											<div class="movie__poster"><a href="/react2/movie/fantastic-beasts-and-where-to-find-them/" class="masvideos-LoopMovie-link masvideos-loop-movie__link movie__link"><img width="300" height="450" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/48-take-shelter-300x450.jpg" class="movie__poster--image" alt="" srcset="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/48-take-shelter-300x450.jpg 300w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/48-take-shelter-66x98.jpg 66w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/48-take-shelter-600x900.jpg 600w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/48-take-shelter-150x225.jpg 150w" sizes="(max-width: 300px) 100vw, 300px" /></a>
																											</div>
																											<div class="movie__body">
																												<div class="movie__info">
																													<div class="movie__info--head">
																														<div class="movie__meta"><span class="movie__meta--release-year">2016</span><span class="movie__meta--genre"><a href="/react2/genre/movieaction/" rel="tag">Action</a>, <a href="/react2/genre/movieannimation/" rel="tag">Annimation</a>, <a href="/react2/genre/moviefamily/" rel="tag">Family</a></span>
																														</div><a href="/react2/movie/fantastic-beasts-and-where-to-find-them/" class="masvideos-LoopMovie-link masvideos-loop-movie__link movie__link"><h3 class="masvideos-loop-movie__title  movie__title">Fantastic Beasts and Where to Find Them</h3></a>
																													</div> 
																													<div class="movie__short-description">
																														<div>
																															<p>The adventures of writer Newt Scamander in New York&#8217;s secret community of witches and wizards seventy years before Harry Potter reads his book in school.</p> 
																														</div> 
																													</div>
																													<div class="movie__actions"><a href="/react2/movie/fantastic-beasts-and-where-to-find-them/" class="movie-actions--link_watch">Watch Now</a> 
																														<div class="movie-actions--link_add-to-playlist dropdown">
																															<a class="dropdown-toggle" href="/react2/movie/fantastic-beasts-and-where-to-find-them/" data-toggle="dropdown">+ Playlist</a>
																															<div class="dropdown-menu">
																																<a class="login-link" href="/login?movie-playlists/">Sign in to add this movie to a playlist.</a> 
																															</div>
																														</div>

																													</div>
																												</div>
																												<div class="movie__review-info"> <a href="/react2/movie/fantastic-beasts-and-where-to-find-them/#reviews" class="avg-rating">
																													<span class="rating-with-count">
																														<svg class="vodi-svg" width="40px" height="39px" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 39"><title>play</title><path fill-rule="evenodd" d="M19.633,-0.000 C21.509,0.035 21.530,1.174 22.167,2.414 C23.329,4.679 24.406,7.067 25.572,9.338 C25.853,9.886 26.431,11.640 26.918,11.834 C27.486,12.203 29.345,12.109 30.165,12.316 C32.170,12.825 34.489,12.860 36.500,13.364 C37.516,13.618 38.689,13.413 39.430,13.927 C39.689,14.107 39.770,14.504 39.984,14.732 C40.047,16.499 39.096,16.843 38.163,17.792 C36.473,19.509 34.784,21.227 33.095,22.944 C32.585,23.462 31.092,24.543 31.036,25.359 C31.423,25.951 31.307,27.455 31.511,28.258 C32.138,30.727 32.213,33.522 32.857,35.987 C33.142,37.078 33.016,38.241 32.303,38.724 C31.108,39.533 29.632,38.193 28.819,37.758 C26.695,36.623 24.601,35.624 22.483,34.457 C21.979,34.179 20.607,33.178 20.108,33.088 C19.748,33.023 18.163,34.107 17.812,34.296 C15.557,35.505 13.340,36.640 11.080,37.839 C10.548,38.120 9.180,39.226 8.309,38.966 C6.955,38.558 6.874,36.993 7.280,35.423 C7.716,33.733 7.697,31.880 8.151,30.109 C8.527,28.642 8.907,26.529 9.022,24.957 C8.092,24.344 7.202,23.107 6.408,22.300 C4.760,20.625 3.059,18.990 1.340,17.389 C0.646,16.742 -0.578,15.515 0.311,14.249 C0.915,13.388 2.364,13.656 3.557,13.364 C6.678,12.599 10.114,12.468 13.298,11.834 C14.186,9.747 15.306,7.711 16.307,5.716 C16.954,4.426 17.496,3.163 18.128,1.931 C18.334,1.531 18.358,1.093 18.603,0.724 C18.845,0.362 19.299,0.273 19.633,-0.000 Z" /></svg> <span class="avg-rating-number"> 7.0</span>
																													</span>
																													<span class="rating-number-with-text">
																														<span class="avg-rating-number"> 7.0</span>
																														<span class="avg-rating-text">
																															<span>1</span> Vote </span>
																														</span>
																													</a>
																													<div class="viewers-count">
																													</div>
																												</div>
																											</div>
																										</div>

																										<div class="post-704 movie type-movie status-publish has-post-thumbnail hentry movie_genre-action movie_genre-drama movie_tag-4k-ultra movie_tag-brother movie_tag-premieres movie_tag-viking">
																											<div class="movie__poster"><a href="/react2/movie/bpm/" class="masvideos-LoopMovie-link masvideos-loop-movie__link movie__link"><img width="300" height="450" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/46-she-is-funny-that-way-300x450.jpg" class="movie__poster--image" alt="" srcset="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/46-she-is-funny-that-way-300x450.jpg 300w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/46-she-is-funny-that-way-66x98.jpg 66w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/46-she-is-funny-that-way-600x900.jpg 600w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/46-she-is-funny-that-way-150x225.jpg 150w" sizes="(max-width: 300px) 100vw, 300px" /></a>
																											</div>
																											<div class="movie__body">
																												<div class="movie__info">
																													<div class="movie__info--head">
																														<div class="movie__meta"><span class="movie__meta--release-year">2018</span><span class="movie__meta--genre"><a href="/react2/genre/movieaction/" rel="tag">Action</a>, <a href="/react2/genre/moviedrama/" rel="tag">Drama</a></span>
																														</div><a href="/react2/movie/bpm/" class="masvideos-LoopMovie-link masvideos-loop-movie__link movie__link"><h3 class="masvideos-loop-movie__title  movie__title">Bpm</h3></a>
																													</div> 
																													<div class="movie__short-description">
																														<div>
																															<p>Members of the advocacy group ACT UP Paris demand action by the government and pharmaceutical companies to combat the AIDS epidemic in the early 1990s.</p> 
																														</div> 
																													</div>
																													<div class="movie__actions"><a href="/react2/movie/bpm/" class="movie-actions--link_watch">Watch Now</a> 
																														<div class="movie-actions--link_add-to-playlist dropdown">
																															<a class="dropdown-toggle" href="/react2/movie/bpm/" data-toggle="dropdown">+ Playlist</a>
																															<div class="dropdown-menu">
																																<a class="login-link" href="/login?movie-playlists/">Sign in to add this movie to a playlist.</a> 
																															</div>
																														</div>

																													</div>
																												</div>
																												<div class="movie__review-info"> <a href="/react2/movie/bpm/#reviews" class="avg-rating">
																													<span class="rating-with-count">
																														<svg class="vodi-svg" width="40px" height="39px" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 39"><title>play</title><path fill-rule="evenodd" d="M19.633,-0.000 C21.509,0.035 21.530,1.174 22.167,2.414 C23.329,4.679 24.406,7.067 25.572,9.338 C25.853,9.886 26.431,11.640 26.918,11.834 C27.486,12.203 29.345,12.109 30.165,12.316 C32.170,12.825 34.489,12.860 36.500,13.364 C37.516,13.618 38.689,13.413 39.430,13.927 C39.689,14.107 39.770,14.504 39.984,14.732 C40.047,16.499 39.096,16.843 38.163,17.792 C36.473,19.509 34.784,21.227 33.095,22.944 C32.585,23.462 31.092,24.543 31.036,25.359 C31.423,25.951 31.307,27.455 31.511,28.258 C32.138,30.727 32.213,33.522 32.857,35.987 C33.142,37.078 33.016,38.241 32.303,38.724 C31.108,39.533 29.632,38.193 28.819,37.758 C26.695,36.623 24.601,35.624 22.483,34.457 C21.979,34.179 20.607,33.178 20.108,33.088 C19.748,33.023 18.163,34.107 17.812,34.296 C15.557,35.505 13.340,36.640 11.080,37.839 C10.548,38.120 9.180,39.226 8.309,38.966 C6.955,38.558 6.874,36.993 7.280,35.423 C7.716,33.733 7.697,31.880 8.151,30.109 C8.527,28.642 8.907,26.529 9.022,24.957 C8.092,24.344 7.202,23.107 6.408,22.300 C4.760,20.625 3.059,18.990 1.340,17.389 C0.646,16.742 -0.578,15.515 0.311,14.249 C0.915,13.388 2.364,13.656 3.557,13.364 C6.678,12.599 10.114,12.468 13.298,11.834 C14.186,9.747 15.306,7.711 16.307,5.716 C16.954,4.426 17.496,3.163 18.128,1.931 C18.334,1.531 18.358,1.093 18.603,0.724 C18.845,0.362 19.299,0.273 19.633,-0.000 Z" /></svg> <span class="avg-rating-number"> 7.0</span>
																													</span>
																													<span class="rating-number-with-text">
																														<span class="avg-rating-number"> 7.0</span>
																														<span class="avg-rating-text">
																															<span>1</span> Vote </span>
																														</span>
																													</a>
																													<div class="viewers-count">
																													</div>
																												</div>
																											</div>
																										</div>

																										<div class="post-698 movie type-movie status-publish has-post-thumbnail hentry movie_genre-action movie_genre-adventure movie_genre-comedy movie_tag-brother movie_tag-king movie_tag-premieres movie_tag-viking">
																											<div class="movie__poster"><a href="/react2/movie/the-accountant/" class="masvideos-LoopMovie-link masvideos-loop-movie__link movie__link"><img width="300" height="450" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/43-only-lovers-left-alive-300x450.jpg" class="movie__poster--image" alt="" srcset="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/43-only-lovers-left-alive-300x450.jpg 300w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/43-only-lovers-left-alive-66x98.jpg 66w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/43-only-lovers-left-alive-600x900.jpg 600w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/43-only-lovers-left-alive-150x225.jpg 150w" sizes="(max-width: 300px) 100vw, 300px" /></a>
																											</div>
																											<div class="movie__body">
																												<div class="movie__info">
																													<div class="movie__info--head">
																														<div class="movie__meta"><span class="movie__meta--release-year">2016</span><span class="movie__meta--genre"><a href="/react2/genre/movieaction/" rel="tag">Action</a>, <a href="/react2/genre/movieadventure/" rel="tag">Adventure</a>, <a href="/react2/genre/moviecomedy/" rel="tag">Comedy</a></span>
																														</div><a href="/react2/movie/the-accountant/" class="masvideos-LoopMovie-link masvideos-loop-movie__link movie__link"><h3 class="masvideos-loop-movie__title  movie__title">The Accountant</h3></a>
																													</div> 
																													<div class="movie__short-description">
																														<div>
																															<p>As a math savant uncooks the books for a new client, the Treasury Department closes in on his activities, and the body count starts to rise.</p> 
																														</div> 
																													</div>
																													<div class="movie__actions"><a href="/react2/movie/the-accountant/" class="movie-actions--link_watch">Watch Now</a> 
																														<div class="movie-actions--link_add-to-playlist dropdown">
																															<a class="dropdown-toggle" href="/react2/movie/the-accountant/" data-toggle="dropdown">+ Playlist</a>
																															<div class="dropdown-menu">
																																<a class="login-link" href="/login?movie-playlists/">Sign in to add this movie to a playlist.</a> 
																															</div>
																														</div>

																													</div>
																												</div>
																												<div class="movie__review-info"> <a href="/react2/movie/the-accountant/#reviews" class="avg-rating">
																													<span class="rating-with-count">
																														<svg class="vodi-svg" width="40px" height="39px" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 39"><title>play</title><path fill-rule="evenodd" d="M19.633,-0.000 C21.509,0.035 21.530,1.174 22.167,2.414 C23.329,4.679 24.406,7.067 25.572,9.338 C25.853,9.886 26.431,11.640 26.918,11.834 C27.486,12.203 29.345,12.109 30.165,12.316 C32.170,12.825 34.489,12.860 36.500,13.364 C37.516,13.618 38.689,13.413 39.430,13.927 C39.689,14.107 39.770,14.504 39.984,14.732 C40.047,16.499 39.096,16.843 38.163,17.792 C36.473,19.509 34.784,21.227 33.095,22.944 C32.585,23.462 31.092,24.543 31.036,25.359 C31.423,25.951 31.307,27.455 31.511,28.258 C32.138,30.727 32.213,33.522 32.857,35.987 C33.142,37.078 33.016,38.241 32.303,38.724 C31.108,39.533 29.632,38.193 28.819,37.758 C26.695,36.623 24.601,35.624 22.483,34.457 C21.979,34.179 20.607,33.178 20.108,33.088 C19.748,33.023 18.163,34.107 17.812,34.296 C15.557,35.505 13.340,36.640 11.080,37.839 C10.548,38.120 9.180,39.226 8.309,38.966 C6.955,38.558 6.874,36.993 7.280,35.423 C7.716,33.733 7.697,31.880 8.151,30.109 C8.527,28.642 8.907,26.529 9.022,24.957 C8.092,24.344 7.202,23.107 6.408,22.300 C4.760,20.625 3.059,18.990 1.340,17.389 C0.646,16.742 -0.578,15.515 0.311,14.249 C0.915,13.388 2.364,13.656 3.557,13.364 C6.678,12.599 10.114,12.468 13.298,11.834 C14.186,9.747 15.306,7.711 16.307,5.716 C16.954,4.426 17.496,3.163 18.128,1.931 C18.334,1.531 18.358,1.093 18.603,0.724 C18.845,0.362 19.299,0.273 19.633,-0.000 Z" /></svg> <span class="avg-rating-number"> 8.0</span>
																													</span>
																													<span class="rating-number-with-text">
																														<span class="avg-rating-number"> 8.0</span>
																														<span class="avg-rating-text">
																															<span>1</span> Vote </span>
																														</span>
																													</a>
																													<div class="viewers-count">
																													</div>
																												</div>
																											</div>
																										</div>

																									</div>
																								</div>
																							</div> 
																						</div>
																					</div>
																				</div>
																			</div>
																		</section>
																		<section id="section-movies-carousel-aside-header-5f87f9fc2afb3" class="home-section section-movies-carousel-aside-header has-section-header has-bg-color dark more-dark" style="padding-top: 59px; padding-bottom: 18px; ">
																			<div class="container">
																				<ul class="nav nav-tabs"><li class="nav-item"><a class="nav-link active" href="#">Today</a></li><li class="nav-item"><a class="nav-link" href="#">This week</a></li><li class="nav-item"><a class="nav-link" href="#">Last 30 days</a></li></ul> 
																				<div class="section-movies-carousel-aside-header__inner">
																					<header class="home-section__header section-movies-carousel-aside-header__header"><h2 class="home-section__title">Funniest Comedy Movies of 2018</h2>
																						<div class="section-movies-carousel-aside-header__custom-arrows">
																						</div>
																						<div class="home-section__action"><a href="#" class="home-section__action-link">View All</a>
																						</div></header>
																						<div class="section-movies-carousel__carousel">
																							<div class="movies-carousel__inner" data-ride="vodi-slick-carousel" data-wrap=".movies__inner" data-slick="{&quot;slidesToShow&quot;:6,&quot;slidesToScroll&quot;:1,&quot;dots&quot;:false,&quot;arrows&quot;:true,&quot;autoplay&quot;:false,&quot;infinite&quot;:false,&quot;responsive&quot;:[{&quot;breakpoint&quot;:768,&quot;settings&quot;:{&quot;slidesToShow&quot;:2,&quot;slidesToScroll&quot;:1}},{&quot;breakpoint&quot;:992,&quot;settings&quot;:{&quot;slidesToShow&quot;:3,&quot;slidesToScroll&quot;:1}},{&quot;breakpoint&quot;:1200,&quot;settings&quot;:{&quot;slidesToShow&quot;:4,&quot;slidesToScroll&quot;:1}}],&quot;appendArrows&quot;:&quot;#section-movies-carousel-aside-header-5f87f9fc2afb3 .section-movies-carousel-aside-header__custom-arrows&quot;}">
																								<div class="masvideos masvideos-movies ">
																									<div class="movies columns-6">
																										<div class="movies__inner">
																											<div class="post-324 movie type-movie status-publish has-post-thumbnail hentry movie_genre-action movie_genre-adventure movie_genre-animation movie_genre-thriller movie_tag-4k-ultra movie_tag-king movie_tag-premieres movie_tag-viking">
																												<span class="movie__badge"><span class="movie__badge--featured">Featured</span></span>
																												<div class="movie__poster"><a href="/react2/movie/war-for-the-planet-of-the-apes/" class="masvideos-LoopMovie-link masvideos-loop-movie__link movie__link"><img width="300" height="450" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/37-dangerous-mind-300x450.jpg" class="movie__poster--image" alt="" srcset="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/37-dangerous-mind-300x450.jpg 300w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/37-dangerous-mind-66x98.jpg 66w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/37-dangerous-mind-600x900.jpg 600w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/37-dangerous-mind-150x225.jpg 150w" sizes="(max-width: 300px) 100vw, 300px" /></a>
																												</div>
																												<div class="movie__body">
																													<div class="movie__info">
																														<div class="movie__info--head">
																															<div class="movie__meta"><span class="movie__meta--release-year">2017</span><span class="movie__meta--genre"><a href="/react2/genre/movieaction/" rel="tag">Action</a>, <a href="/react2/genre/movieadventure/" rel="tag">Adventure</a>, <a href="/react2/genre/movieanimation/" rel="tag">Animation</a>, <a href="/react2/genre/moviethriller/" rel="tag">Thriller</a></span>
																															</div><a href="/react2/movie/war-for-the-planet-of-the-apes/" class="masvideos-LoopMovie-link masvideos-loop-movie__link movie__link"><h3 class="masvideos-loop-movie__title  movie__title">War for the Planet of the Apes</h3></a>
																														</div> 
																														<div class="movie__short-description">
																															<div>
																																<p>After the apes suffer unimaginable losses, Caesar wrestles with his darker instincts and begins his own mythic quest to avenge his kind</p> 
																															</div> 
																														</div>
																														<div class="movie__actions"><a href="/react2/movie/war-for-the-planet-of-the-apes/" class="movie-actions--link_watch">Watch Now</a> 
																															<div class="movie-actions--link_add-to-playlist dropdown">
																																<a class="dropdown-toggle" href="/react2/movie/war-for-the-planet-of-the-apes/" data-toggle="dropdown">+ Playlist</a>
																																<div class="dropdown-menu">
																																	<a class="login-link" href="/login?movie-playlists/">Sign in to add this movie to a playlist.</a> 
																																</div>
																															</div>

																														</div>
																													</div>
																													<div class="movie__review-info"> <a href="/react2/movie/war-for-the-planet-of-the-apes/#reviews" class="avg-rating">
																														<span class="rating-with-count">
																															<svg class="vodi-svg" width="40px" height="39px" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 39"><title>play</title><path fill-rule="evenodd" d="M19.633,-0.000 C21.509,0.035 21.530,1.174 22.167,2.414 C23.329,4.679 24.406,7.067 25.572,9.338 C25.853,9.886 26.431,11.640 26.918,11.834 C27.486,12.203 29.345,12.109 30.165,12.316 C32.170,12.825 34.489,12.860 36.500,13.364 C37.516,13.618 38.689,13.413 39.430,13.927 C39.689,14.107 39.770,14.504 39.984,14.732 C40.047,16.499 39.096,16.843 38.163,17.792 C36.473,19.509 34.784,21.227 33.095,22.944 C32.585,23.462 31.092,24.543 31.036,25.359 C31.423,25.951 31.307,27.455 31.511,28.258 C32.138,30.727 32.213,33.522 32.857,35.987 C33.142,37.078 33.016,38.241 32.303,38.724 C31.108,39.533 29.632,38.193 28.819,37.758 C26.695,36.623 24.601,35.624 22.483,34.457 C21.979,34.179 20.607,33.178 20.108,33.088 C19.748,33.023 18.163,34.107 17.812,34.296 C15.557,35.505 13.340,36.640 11.080,37.839 C10.548,38.120 9.180,39.226 8.309,38.966 C6.955,38.558 6.874,36.993 7.280,35.423 C7.716,33.733 7.697,31.880 8.151,30.109 C8.527,28.642 8.907,26.529 9.022,24.957 C8.092,24.344 7.202,23.107 6.408,22.300 C4.760,20.625 3.059,18.990 1.340,17.389 C0.646,16.742 -0.578,15.515 0.311,14.249 C0.915,13.388 2.364,13.656 3.557,13.364 C6.678,12.599 10.114,12.468 13.298,11.834 C14.186,9.747 15.306,7.711 16.307,5.716 C16.954,4.426 17.496,3.163 18.128,1.931 C18.334,1.531 18.358,1.093 18.603,0.724 C18.845,0.362 19.299,0.273 19.633,-0.000 Z" /></svg> <span class="avg-rating-number"> 8.0</span>
																														</span>
																														<span class="rating-number-with-text">
																															<span class="avg-rating-number"> 8.0</span>
																															<span class="avg-rating-text">
																																<span>1</span> Vote </span>
																															</span>
																														</a>
																														<div class="viewers-count">
																														</div>
																													</div>
																												</div>
																											</div>

																											<div class="post-320 movie type-movie status-publish has-post-thumbnail hentry movie_genre-action movie_genre-adventure movie_tag-4k-ultra movie_tag-king movie_tag-premieres movie_tag-viking">
																												<div class="movie__poster"><a href="/react2/movie/renegades/" class="masvideos-LoopMovie-link masvideos-loop-movie__link movie__link"><img width="300" height="450" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/35-brothers-bloom-300x450.jpg" class="movie__poster--image" alt="" srcset="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/35-brothers-bloom-300x450.jpg 300w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/35-brothers-bloom-66x98.jpg 66w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/35-brothers-bloom-600x900.jpg 600w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/35-brothers-bloom-150x225.jpg 150w" sizes="(max-width: 300px) 100vw, 300px" /></a>
																												</div>
																												<div class="movie__body">
																													<div class="movie__info">
																														<div class="movie__info--head">
																															<div class="movie__meta"><span class="movie__meta--release-year">2018</span><span class="movie__meta--genre"><a href="/react2/genre/movieaction/" rel="tag">Action</a>, <a href="/react2/genre/movieadventure/" rel="tag">Adventure</a></span>
																															</div><a href="/react2/movie/renegades/" class="masvideos-LoopMovie-link masvideos-loop-movie__link movie__link"><h3 class="masvideos-loop-movie__title  movie__title">Renegades</h3></a>
																														</div> 
																														<div class="movie__short-description">
																															<div>
																																<p>A team of Navy SEALs discover an underwater treasure in a Bosnian lake.</p> 
																															</div> 
																														</div>
																														<div class="movie__actions"><a href="/react2/movie/renegades/" class="movie-actions--link_watch">Watch Now</a> 
																															<div class="movie-actions--link_add-to-playlist dropdown">
																																<a class="dropdown-toggle" href="/react2/movie/renegades/" data-toggle="dropdown">+ Playlist</a>
																																<div class="dropdown-menu">
																																	<a class="login-link" href="/login?movie-playlists/">Sign in to add this movie to a playlist.</a> 
																																</div>
																															</div>

																														</div>
																													</div>
																													<div class="movie__review-info"> <a href="/react2/movie/renegades/#reviews" class="avg-rating">
																														<span class="rating-with-count">
																															<svg class="vodi-svg" width="40px" height="39px" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 39"><title>play</title><path fill-rule="evenodd" d="M19.633,-0.000 C21.509,0.035 21.530,1.174 22.167,2.414 C23.329,4.679 24.406,7.067 25.572,9.338 C25.853,9.886 26.431,11.640 26.918,11.834 C27.486,12.203 29.345,12.109 30.165,12.316 C32.170,12.825 34.489,12.860 36.500,13.364 C37.516,13.618 38.689,13.413 39.430,13.927 C39.689,14.107 39.770,14.504 39.984,14.732 C40.047,16.499 39.096,16.843 38.163,17.792 C36.473,19.509 34.784,21.227 33.095,22.944 C32.585,23.462 31.092,24.543 31.036,25.359 C31.423,25.951 31.307,27.455 31.511,28.258 C32.138,30.727 32.213,33.522 32.857,35.987 C33.142,37.078 33.016,38.241 32.303,38.724 C31.108,39.533 29.632,38.193 28.819,37.758 C26.695,36.623 24.601,35.624 22.483,34.457 C21.979,34.179 20.607,33.178 20.108,33.088 C19.748,33.023 18.163,34.107 17.812,34.296 C15.557,35.505 13.340,36.640 11.080,37.839 C10.548,38.120 9.180,39.226 8.309,38.966 C6.955,38.558 6.874,36.993 7.280,35.423 C7.716,33.733 7.697,31.880 8.151,30.109 C8.527,28.642 8.907,26.529 9.022,24.957 C8.092,24.344 7.202,23.107 6.408,22.300 C4.760,20.625 3.059,18.990 1.340,17.389 C0.646,16.742 -0.578,15.515 0.311,14.249 C0.915,13.388 2.364,13.656 3.557,13.364 C6.678,12.599 10.114,12.468 13.298,11.834 C14.186,9.747 15.306,7.711 16.307,5.716 C16.954,4.426 17.496,3.163 18.128,1.931 C18.334,1.531 18.358,1.093 18.603,0.724 C18.845,0.362 19.299,0.273 19.633,-0.000 Z" /></svg> <span class="avg-rating-number"> 8.0</span>
																														</span>
																														<span class="rating-number-with-text">
																															<span class="avg-rating-number"> 8.0</span>
																															<span class="avg-rating-text">
																																<span>1</span> Vote </span>
																															</span>
																														</a>
																														<div class="viewers-count">
																														</div>
																													</div>
																												</div>
																											</div>

																											<div class="post-314 movie type-movie status-publish has-post-thumbnail hentry movie_genre-action movie_genre-documentary movie_tag-4k-ultra movie_tag-king movie_tag-premieres movie_tag-viking">
																												<div class="movie__poster"><a href="/react2/movie/my-generation/" class="masvideos-LoopMovie-link masvideos-loop-movie__link movie__link"><img width="300" height="450" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/32-a-long-way-down-300x450.jpg" class="movie__poster--image" alt="" srcset="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/32-a-long-way-down-300x450.jpg 300w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/32-a-long-way-down-66x98.jpg 66w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/32-a-long-way-down-600x900.jpg 600w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/32-a-long-way-down-150x225.jpg 150w" sizes="(max-width: 300px) 100vw, 300px" /></a>
																												</div>
																												<div class="movie__body">
																													<div class="movie__info">
																														<div class="movie__info--head">
																															<div class="movie__meta"><span class="movie__meta--release-year">2018</span><span class="movie__meta--genre"><a href="/react2/genre/movieaction/" rel="tag">Action</a>, <a href="/react2/genre/moviedocumentary/" rel="tag">Documentary</a></span>
																															</div><a href="/react2/movie/my-generation/" class="masvideos-LoopMovie-link masvideos-loop-movie__link movie__link"><h3 class="masvideos-loop-movie__title  movie__title">My Generation</h3></a>
																														</div> 
																														<div class="movie__short-description">
																															<div><p>The cultural revolution that occurred in the 1960s England is explored in this documentary.</p>
																															</div> 
																														</div>
																														<div class="movie__actions"><a href="/react2/movie/my-generation/" class="movie-actions--link_watch">Watch Now</a> 
																															<div class="movie-actions--link_add-to-playlist dropdown">
																																<a class="dropdown-toggle" href="/react2/movie/my-generation/" data-toggle="dropdown">+ Playlist</a>
																																<div class="dropdown-menu">
																																	<a class="login-link" href="/login?movie-playlists/">Sign in to add this movie to a playlist.</a> 
																																</div>
																															</div>

																														</div>
																													</div>
																													<div class="movie__review-info"> <a href="/react2/movie/my-generation/#reviews" class="avg-rating">
																														<span class="rating-with-count">
																															<svg class="vodi-svg" width="40px" height="39px" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 39"><title>play</title><path fill-rule="evenodd" d="M19.633,-0.000 C21.509,0.035 21.530,1.174 22.167,2.414 C23.329,4.679 24.406,7.067 25.572,9.338 C25.853,9.886 26.431,11.640 26.918,11.834 C27.486,12.203 29.345,12.109 30.165,12.316 C32.170,12.825 34.489,12.860 36.500,13.364 C37.516,13.618 38.689,13.413 39.430,13.927 C39.689,14.107 39.770,14.504 39.984,14.732 C40.047,16.499 39.096,16.843 38.163,17.792 C36.473,19.509 34.784,21.227 33.095,22.944 C32.585,23.462 31.092,24.543 31.036,25.359 C31.423,25.951 31.307,27.455 31.511,28.258 C32.138,30.727 32.213,33.522 32.857,35.987 C33.142,37.078 33.016,38.241 32.303,38.724 C31.108,39.533 29.632,38.193 28.819,37.758 C26.695,36.623 24.601,35.624 22.483,34.457 C21.979,34.179 20.607,33.178 20.108,33.088 C19.748,33.023 18.163,34.107 17.812,34.296 C15.557,35.505 13.340,36.640 11.080,37.839 C10.548,38.120 9.180,39.226 8.309,38.966 C6.955,38.558 6.874,36.993 7.280,35.423 C7.716,33.733 7.697,31.880 8.151,30.109 C8.527,28.642 8.907,26.529 9.022,24.957 C8.092,24.344 7.202,23.107 6.408,22.300 C4.760,20.625 3.059,18.990 1.340,17.389 C0.646,16.742 -0.578,15.515 0.311,14.249 C0.915,13.388 2.364,13.656 3.557,13.364 C6.678,12.599 10.114,12.468 13.298,11.834 C14.186,9.747 15.306,7.711 16.307,5.716 C16.954,4.426 17.496,3.163 18.128,1.931 C18.334,1.531 18.358,1.093 18.603,0.724 C18.845,0.362 19.299,0.273 19.633,-0.000 Z" /></svg> <span class="avg-rating-number"> 10.0</span>
																														</span>
																														<span class="rating-number-with-text">
																															<span class="avg-rating-number"> 10.0</span>
																															<span class="avg-rating-text">
																																<span>1</span> Vote </span>
																															</span>
																														</a>
																														<div class="viewers-count">
																														</div>
																													</div>
																												</div>
																											</div>

																											<div class="post-310 movie type-movie status-publish has-post-thumbnail hentry movie_genre-action movie_genre-adventure movie_genre-biography movie_tag-4k-ultra movie_tag-king movie_tag-premieres movie_tag-viking">
																												<div class="movie__poster"><a href="/react2/movie/mary-shelley/" class="masvideos-LoopMovie-link masvideos-loop-movie__link movie__link"><img width="300" height="450" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/30-300x450.jpg" class="movie__poster--image" alt="" srcset="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/30-300x450.jpg 300w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/30-66x98.jpg 66w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/30-200x300.jpg 200w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/30-600x900.jpg 600w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/30-150x225.jpg 150w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/30.jpg 668w" sizes="(max-width: 300px) 100vw, 300px" /></a>
																												</div>
																												<div class="movie__body">
																													<div class="movie__info">
																														<div class="movie__info--head">
																															<div class="movie__meta"><span class="movie__meta--release-year">2017</span><span class="movie__meta--genre"><a href="/react2/genre/movieaction/" rel="tag">Action</a>, <a href="/react2/genre/movieadventure/" rel="tag">Adventure</a>, <a href="/react2/genre/moviebiography/" rel="tag">Biography</a></span>
																															</div><a href="/react2/movie/mary-shelley/" class="masvideos-LoopMovie-link masvideos-loop-movie__link movie__link"><h3 class="masvideos-loop-movie__title  movie__title">Mary Shelley</h3></a>
																														</div> 
																														<div class="movie__short-description">
																															<div>
																																<p>Life and facts of Mary Wollstonecraft Godwin, who at 16 met 21 year old poet Percy Shelley, resulting in the writing of Frankenstein.</p> 
																															</div> 
																														</div>
																														<div class="movie__actions"><a href="/react2/movie/mary-shelley/" class="movie-actions--link_watch">Watch Now</a> 
																															<div class="movie-actions--link_add-to-playlist dropdown">
																																<a class="dropdown-toggle" href="/react2/movie/mary-shelley/" data-toggle="dropdown">+ Playlist</a>
																																<div class="dropdown-menu">
																																	<a class="login-link" href="/login?movie-playlists/">Sign in to add this movie to a playlist.</a> 
																																</div>
																															</div>

																														</div>
																													</div>
																													<div class="movie__review-info"> <a href="/react2/movie/mary-shelley/#reviews" class="avg-rating">
																														<span class="rating-with-count">
																															<svg class="vodi-svg" width="40px" height="39px" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 39"><title>play</title><path fill-rule="evenodd" d="M19.633,-0.000 C21.509,0.035 21.530,1.174 22.167,2.414 C23.329,4.679 24.406,7.067 25.572,9.338 C25.853,9.886 26.431,11.640 26.918,11.834 C27.486,12.203 29.345,12.109 30.165,12.316 C32.170,12.825 34.489,12.860 36.500,13.364 C37.516,13.618 38.689,13.413 39.430,13.927 C39.689,14.107 39.770,14.504 39.984,14.732 C40.047,16.499 39.096,16.843 38.163,17.792 C36.473,19.509 34.784,21.227 33.095,22.944 C32.585,23.462 31.092,24.543 31.036,25.359 C31.423,25.951 31.307,27.455 31.511,28.258 C32.138,30.727 32.213,33.522 32.857,35.987 C33.142,37.078 33.016,38.241 32.303,38.724 C31.108,39.533 29.632,38.193 28.819,37.758 C26.695,36.623 24.601,35.624 22.483,34.457 C21.979,34.179 20.607,33.178 20.108,33.088 C19.748,33.023 18.163,34.107 17.812,34.296 C15.557,35.505 13.340,36.640 11.080,37.839 C10.548,38.120 9.180,39.226 8.309,38.966 C6.955,38.558 6.874,36.993 7.280,35.423 C7.716,33.733 7.697,31.880 8.151,30.109 C8.527,28.642 8.907,26.529 9.022,24.957 C8.092,24.344 7.202,23.107 6.408,22.300 C4.760,20.625 3.059,18.990 1.340,17.389 C0.646,16.742 -0.578,15.515 0.311,14.249 C0.915,13.388 2.364,13.656 3.557,13.364 C6.678,12.599 10.114,12.468 13.298,11.834 C14.186,9.747 15.306,7.711 16.307,5.716 C16.954,4.426 17.496,3.163 18.128,1.931 C18.334,1.531 18.358,1.093 18.603,0.724 C18.845,0.362 19.299,0.273 19.633,-0.000 Z" /></svg> <span class="avg-rating-number"> 8.0</span>
																														</span>
																														<span class="rating-number-with-text">
																															<span class="avg-rating-number"> 8.0</span>
																															<span class="avg-rating-text">
																																<span>1</span> Vote </span>
																															</span>
																														</a>
																														<div class="viewers-count">
																														</div>
																													</div>
																												</div>
																											</div>

																											<div class="post-308 movie type-movie status-publish has-post-thumbnail hentry movie_genre-action movie_genre-romance movie_tag-4k-ultra movie_tag-brother movie_tag-king movie_tag-viking">
																												<span class="movie__badge"><span class="movie__badge--featured">Featured</span></span>
																												<div class="movie__poster"><a href="/react2/movie/love-simon/" class="masvideos-LoopMovie-link masvideos-loop-movie__link movie__link"><img width="300" height="450" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/29-300x450.jpg" class="movie__poster--image" alt="" srcset="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/29-300x450.jpg 300w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/29-66x98.jpg 66w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/29-200x300.jpg 200w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/29-600x900.jpg 600w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/29-150x225.jpg 150w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/29.jpg 668w" sizes="(max-width: 300px) 100vw, 300px" /></a>
																												</div>
																												<div class="movie__body">
																													<div class="movie__info">
																														<div class="movie__info--head">
																															<div class="movie__meta"><span class="movie__meta--release-year">2018</span><span class="movie__meta--genre"><a href="/react2/genre/movieaction/" rel="tag">Action</a>, <a href="/react2/genre/movieromance/" rel="tag">Romance</a></span>
																															</div><a href="/react2/movie/love-simon/" class="masvideos-LoopMovie-link masvideos-loop-movie__link movie__link"><h3 class="masvideos-loop-movie__title  movie__title">Love, Simon</h3></a>
																														</div> 
																														<div class="movie__short-description">
																															<div>
																																<p>Simon Spier keeps a huge secret from his family, his friends and all of his classmates: he&#8217;s gay. When that secret is threatened, Simon must face everyone and come to terms with his identity.</p> 
																															</div> 
																														</div>
																														<div class="movie__actions"><a href="/react2/movie/love-simon/" class="movie-actions--link_watch">Watch Now</a> 
																															<div class="movie-actions--link_add-to-playlist dropdown">
																																<a class="dropdown-toggle" href="/react2/movie/love-simon/" data-toggle="dropdown">+ Playlist</a>
																																<div class="dropdown-menu">
																																	<a class="login-link" href="/login?movie-playlists/">Sign in to add this movie to a playlist.</a> 
																																</div>
																															</div>
																														</div>
																													</div>
																													<div class="movie__review-info"> <a href="/react2/movie/love-simon/#reviews" class="avg-rating">
																														<span class="rating-with-count">
																															<svg class="vodi-svg" width="40px" height="39px" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 39"><title>play</title><path fill-rule="evenodd" d="M19.633,-0.000 C21.509,0.035 21.530,1.174 22.167,2.414 C23.329,4.679 24.406,7.067 25.572,9.338 C25.853,9.886 26.431,11.640 26.918,11.834 C27.486,12.203 29.345,12.109 30.165,12.316 C32.170,12.825 34.489,12.860 36.500,13.364 C37.516,13.618 38.689,13.413 39.430,13.927 C39.689,14.107 39.770,14.504 39.984,14.732 C40.047,16.499 39.096,16.843 38.163,17.792 C36.473,19.509 34.784,21.227 33.095,22.944 C32.585,23.462 31.092,24.543 31.036,25.359 C31.423,25.951 31.307,27.455 31.511,28.258 C32.138,30.727 32.213,33.522 32.857,35.987 C33.142,37.078 33.016,38.241 32.303,38.724 C31.108,39.533 29.632,38.193 28.819,37.758 C26.695,36.623 24.601,35.624 22.483,34.457 C21.979,34.179 20.607,33.178 20.108,33.088 C19.748,33.023 18.163,34.107 17.812,34.296 C15.557,35.505 13.340,36.640 11.080,37.839 C10.548,38.120 9.180,39.226 8.309,38.966 C6.955,38.558 6.874,36.993 7.280,35.423 C7.716,33.733 7.697,31.880 8.151,30.109 C8.527,28.642 8.907,26.529 9.022,24.957 C8.092,24.344 7.202,23.107 6.408,22.300 C4.760,20.625 3.059,18.990 1.340,17.389 C0.646,16.742 -0.578,15.515 0.311,14.249 C0.915,13.388 2.364,13.656 3.557,13.364 C6.678,12.599 10.114,12.468 13.298,11.834 C14.186,9.747 15.306,7.711 16.307,5.716 C16.954,4.426 17.496,3.163 18.128,1.931 C18.334,1.531 18.358,1.093 18.603,0.724 C18.845,0.362 19.299,0.273 19.633,-0.000 Z" /></svg> <span class="avg-rating-number"> 8.0</span>
																														</span>
																														<span class="rating-number-with-text">
																															<span class="avg-rating-number"> 8.0</span>
																															<span class="avg-rating-text">
																																<span>1</span> Vote </span>
																															</span>
																														</a>
																														<div class="viewers-count">
																														</div>
																													</div>
																												</div>
																											</div>

																											<div class="post-304 movie type-movie status-publish has-post-thumbnail hentry movie_genre-action movie_genre-animation movie_tag-brother movie_tag-king movie_tag-premieres movie_tag-viking">
																												<span class="movie__badge"><span class="movie__badge--featured">Featured</span></span>
																												<div class="movie__poster"><a href="/react2/movie/gnome-alone/" class="masvideos-LoopMovie-link masvideos-loop-movie__link movie__link"><img width="300" height="450" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/27-300x450.jpg" class="movie__poster--image" alt="" srcset="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/27-300x450.jpg 300w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/27-66x98.jpg 66w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/27-200x300.jpg 200w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/27-600x900.jpg 600w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/27-150x225.jpg 150w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/27.jpg 668w" sizes="(max-width: 300px) 100vw, 300px" /></a>
																												</div>
																												<div class="movie__body">
																													<div class="movie__info">
																														<div class="movie__info--head">
																															<div class="movie__meta"><span class="movie__meta--release-year">2017</span><span class="movie__meta--genre"><a href="/react2/genre/movieaction/" rel="tag">Action</a>, <a href="/react2/genre/movieanimation/" rel="tag">Animation</a></span>
																															</div><a href="/react2/movie/gnome-alone/" class="masvideos-LoopMovie-link masvideos-loop-movie__link movie__link"><h3 class="masvideos-loop-movie__title  movie__title">Gnome Alone</h3></a>
																														</div> 
																														<div class="movie__short-description">
																															<div>
																																<p>When Chloe discovers that her new home&#8217;s garden gnomes are not what they seem, she must decide between the pursuit of a desired high school life and taking up the fight against the Troggs</p> 
																															</div> 
																														</div>
																														<div class="movie__actions"><a href="/react2/movie/gnome-alone/" class="movie-actions--link_watch">Watch Now</a> 
																															<div class="movie-actions--link_add-to-playlist dropdown">
																																<a class="dropdown-toggle" href="/react2/movie/gnome-alone/" data-toggle="dropdown">+ Playlist</a>
																																<div class="dropdown-menu">
																																	<a class="login-link" href="/login?movie-playlists/">Sign in to add this movie to a playlist.</a> 
																																</div>
																															</div>
																														</div>
																													</div>
																													<div class="movie__review-info"> <a href="/react2/movie/gnome-alone/#reviews" class="avg-rating">
																														<span class="rating-with-count">
																															<svg class="vodi-svg" width="40px" height="39px" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 39"><title>play</title><path fill-rule="evenodd" d="M19.633,-0.000 C21.509,0.035 21.530,1.174 22.167,2.414 C23.329,4.679 24.406,7.067 25.572,9.338 C25.853,9.886 26.431,11.640 26.918,11.834 C27.486,12.203 29.345,12.109 30.165,12.316 C32.170,12.825 34.489,12.860 36.500,13.364 C37.516,13.618 38.689,13.413 39.430,13.927 C39.689,14.107 39.770,14.504 39.984,14.732 C40.047,16.499 39.096,16.843 38.163,17.792 C36.473,19.509 34.784,21.227 33.095,22.944 C32.585,23.462 31.092,24.543 31.036,25.359 C31.423,25.951 31.307,27.455 31.511,28.258 C32.138,30.727 32.213,33.522 32.857,35.987 C33.142,37.078 33.016,38.241 32.303,38.724 C31.108,39.533 29.632,38.193 28.819,37.758 C26.695,36.623 24.601,35.624 22.483,34.457 C21.979,34.179 20.607,33.178 20.108,33.088 C19.748,33.023 18.163,34.107 17.812,34.296 C15.557,35.505 13.340,36.640 11.080,37.839 C10.548,38.120 9.180,39.226 8.309,38.966 C6.955,38.558 6.874,36.993 7.280,35.423 C7.716,33.733 7.697,31.880 8.151,30.109 C8.527,28.642 8.907,26.529 9.022,24.957 C8.092,24.344 7.202,23.107 6.408,22.300 C4.760,20.625 3.059,18.990 1.340,17.389 C0.646,16.742 -0.578,15.515 0.311,14.249 C0.915,13.388 2.364,13.656 3.557,13.364 C6.678,12.599 10.114,12.468 13.298,11.834 C14.186,9.747 15.306,7.711 16.307,5.716 C16.954,4.426 17.496,3.163 18.128,1.931 C18.334,1.531 18.358,1.093 18.603,0.724 C18.845,0.362 19.299,0.273 19.633,-0.000 Z" /></svg> <span class="avg-rating-number"> 8.0</span>
																														</span>
																														<span class="rating-number-with-text">
																															<span class="avg-rating-number"> 8.0</span>
																															<span class="avg-rating-text">
																																<span>1</span> Vote </span>
																															</span>
																														</a>
																														<div class="viewers-count">
																														</div>
																													</div>
																												</div>
																											</div>

																											<div class="post-300 movie type-movie status-publish has-post-thumbnail hentry movie_genre-comedy movie_genre-crime movie_genre-sci-fi">
																												<div class="movie__poster"><a href="/react2/movie/bad-genius/" class="masvideos-LoopMovie-link masvideos-loop-movie__link movie__link"><img width="300" height="450" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/25-300x450.jpg" class="movie__poster--image" alt="" srcset="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/25-300x450.jpg 300w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/25-66x98.jpg 66w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/25-200x300.jpg 200w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/25-600x900.jpg 600w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/25-150x225.jpg 150w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/25.jpg 668w" sizes="(max-width: 300px) 100vw, 300px" /></a>
																												</div>
																												<div class="movie__body">
																													<div class="movie__info">
																														<div class="movie__info--head">
																															<div class="movie__meta"><span class="movie__meta--release-year">2017</span><span class="movie__meta--genre"><a href="/react2/genre/moviecomedy/" rel="tag">Comedy</a>, <a href="/react2/genre/moviecrime/" rel="tag">Crime</a>, <a href="/react2/genre/moviesci-fi/" rel="tag">Sci-Fi</a></span>
																															</div><a href="/react2/movie/bad-genius/" class="masvideos-LoopMovie-link masvideos-loop-movie__link movie__link"><h3 class="masvideos-loop-movie__title  movie__title">Bad Genius</h3></a>
																														</div> 
																														<div class="movie__short-description">
																															<div>
																																<p>Lynn, a genius high school student who makes money by cheating tests, receives a new task that leads her to set foot on Sydney, Australia. In order to complete the millions-Baht task, Lynn and her classmates have to finish the international STIC(SAT) exam and deliver the answers back to her friends in Thailand before the exam takes place once again in her home country.</p> 
																															</div> 
																														</div>
																														<div class="movie__actions"><a href="/react2/movie/bad-genius/" class="movie-actions--link_watch">Watch Now</a> 
																															<div class="movie-actions--link_add-to-playlist dropdown">
																																<a class="dropdown-toggle" href="/react2/movie/bad-genius/" data-toggle="dropdown">+ Playlist</a>
																																<div class="dropdown-menu">
																																	<a class="login-link" href="/login?movie-playlists/">Sign in to add this movie to a playlist.</a> 
																																</div>
																															</div>

																														</div>
																													</div>
																													<div class="movie__review-info"> <a href="/react2/movie/bad-genius/#reviews" class="avg-rating">
																														<span class="rating-with-count">
																															<svg class="vodi-svg" width="40px" height="39px" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 39"><title>play</title><path fill-rule="evenodd" d="M19.633,-0.000 C21.509,0.035 21.530,1.174 22.167,2.414 C23.329,4.679 24.406,7.067 25.572,9.338 C25.853,9.886 26.431,11.640 26.918,11.834 C27.486,12.203 29.345,12.109 30.165,12.316 C32.170,12.825 34.489,12.860 36.500,13.364 C37.516,13.618 38.689,13.413 39.430,13.927 C39.689,14.107 39.770,14.504 39.984,14.732 C40.047,16.499 39.096,16.843 38.163,17.792 C36.473,19.509 34.784,21.227 33.095,22.944 C32.585,23.462 31.092,24.543 31.036,25.359 C31.423,25.951 31.307,27.455 31.511,28.258 C32.138,30.727 32.213,33.522 32.857,35.987 C33.142,37.078 33.016,38.241 32.303,38.724 C31.108,39.533 29.632,38.193 28.819,37.758 C26.695,36.623 24.601,35.624 22.483,34.457 C21.979,34.179 20.607,33.178 20.108,33.088 C19.748,33.023 18.163,34.107 17.812,34.296 C15.557,35.505 13.340,36.640 11.080,37.839 C10.548,38.120 9.180,39.226 8.309,38.966 C6.955,38.558 6.874,36.993 7.280,35.423 C7.716,33.733 7.697,31.880 8.151,30.109 C8.527,28.642 8.907,26.529 9.022,24.957 C8.092,24.344 7.202,23.107 6.408,22.300 C4.760,20.625 3.059,18.990 1.340,17.389 C0.646,16.742 -0.578,15.515 0.311,14.249 C0.915,13.388 2.364,13.656 3.557,13.364 C6.678,12.599 10.114,12.468 13.298,11.834 C14.186,9.747 15.306,7.711 16.307,5.716 C16.954,4.426 17.496,3.163 18.128,1.931 C18.334,1.531 18.358,1.093 18.603,0.724 C18.845,0.362 19.299,0.273 19.633,-0.000 Z" /></svg> <span class="avg-rating-number"> 8.0</span>
																														</span>
																														<span class="rating-number-with-text">
																															<span class="avg-rating-number"> 8.0</span>
																															<span class="avg-rating-text">
																																<span>1</span> Vote </span>
																															</span>
																														</a>
																														<div class="viewers-count">
																														</div>
																													</div>
																												</div>
																											</div>

																											<div class="post-280 movie type-movie status-publish has-post-thumbnail hentry movie_genre-action movie_genre-adventure movie_tag-brother movie_tag-king movie_tag-premieres movie_tag-viking">
																												<span class="movie__badge"><span class="movie__badge--featured">Featured</span></span>
																												<div class="movie__poster"><a href="/react2/movie/game-night/" class="masvideos-LoopMovie-link masvideos-loop-movie__link movie__link"><img width="300" height="450" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/15-300x450.jpg" class="movie__poster--image" alt="" srcset="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/15-300x450.jpg 300w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/15-66x98.jpg 66w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/15-200x300.jpg 200w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/15-600x900.jpg 600w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/15-150x225.jpg 150w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/15.jpg 668w" sizes="(max-width: 300px) 100vw, 300px" /></a>
																												</div>
																												<div class="movie__body">
																													<div class="movie__info">
																														<div class="movie__info--head">
																															<div class="movie__meta"><span class="movie__meta--release-year">2018</span><span class="movie__meta--genre"><a href="/react2/genre/movieaction/" rel="tag">Action</a>, <a href="/react2/genre/movieadventure/" rel="tag">Adventure</a></span>
																															</div><a href="/react2/movie/game-night/" class="masvideos-LoopMovie-link masvideos-loop-movie__link movie__link"><h3 class="masvideos-loop-movie__title  movie__title">Game Night</h3></a>
																														</div> 
																														<div class="movie__short-description">
																															<div>
																																<p>A group of friends who meet regularly for game nights find themselves entangled in a real-life mystery when the shady brother of one of them is seemingly kidnapped by dangerous gangsters.</p> 
																															</div> 
																														</div>
																														<div class="movie__actions"><a href="/react2/movie/game-night/" class="movie-actions--link_watch">Watch Now</a> 
																															<div class="movie-actions--link_add-to-playlist dropdown">
																																<a class="dropdown-toggle" href="/react2/movie/game-night/" data-toggle="dropdown">+ Playlist</a>
																																<div class="dropdown-menu">
																																	<a class="login-link" href="/login?movie-playlists/">Sign in to add this movie to a playlist.</a> 
																																</div>
																															</div>

																														</div>
																													</div>
																													<div class="movie__review-info"> <a href="/react2/movie/game-night/#reviews" class="avg-rating">
																														<span class="rating-with-count">
																															<svg class="vodi-svg" width="40px" height="39px" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 39"><title>play</title><path fill-rule="evenodd" d="M19.633,-0.000 C21.509,0.035 21.530,1.174 22.167,2.414 C23.329,4.679 24.406,7.067 25.572,9.338 C25.853,9.886 26.431,11.640 26.918,11.834 C27.486,12.203 29.345,12.109 30.165,12.316 C32.170,12.825 34.489,12.860 36.500,13.364 C37.516,13.618 38.689,13.413 39.430,13.927 C39.689,14.107 39.770,14.504 39.984,14.732 C40.047,16.499 39.096,16.843 38.163,17.792 C36.473,19.509 34.784,21.227 33.095,22.944 C32.585,23.462 31.092,24.543 31.036,25.359 C31.423,25.951 31.307,27.455 31.511,28.258 C32.138,30.727 32.213,33.522 32.857,35.987 C33.142,37.078 33.016,38.241 32.303,38.724 C31.108,39.533 29.632,38.193 28.819,37.758 C26.695,36.623 24.601,35.624 22.483,34.457 C21.979,34.179 20.607,33.178 20.108,33.088 C19.748,33.023 18.163,34.107 17.812,34.296 C15.557,35.505 13.340,36.640 11.080,37.839 C10.548,38.120 9.180,39.226 8.309,38.966 C6.955,38.558 6.874,36.993 7.280,35.423 C7.716,33.733 7.697,31.880 8.151,30.109 C8.527,28.642 8.907,26.529 9.022,24.957 C8.092,24.344 7.202,23.107 6.408,22.300 C4.760,20.625 3.059,18.990 1.340,17.389 C0.646,16.742 -0.578,15.515 0.311,14.249 C0.915,13.388 2.364,13.656 3.557,13.364 C6.678,12.599 10.114,12.468 13.298,11.834 C14.186,9.747 15.306,7.711 16.307,5.716 C16.954,4.426 17.496,3.163 18.128,1.931 C18.334,1.531 18.358,1.093 18.603,0.724 C18.845,0.362 19.299,0.273 19.633,-0.000 Z" /></svg> <span class="avg-rating-number"> 8.0</span>
																														</span>
																														<span class="rating-number-with-text">
																															<span class="avg-rating-number"> 8.0</span>
																															<span class="avg-rating-text">
																																<span>1</span> Vote </span>
																															</span>
																														</a>
																														<div class="viewers-count">
																														</div>
																													</div>
																												</div>
																											</div>

																										</div>
																									</div>
																								</div> 
																							</div>
																						</div>
																					</div>
																				</div>
																			</section>
																			<section class="home-section section-featured-tv-show" style="padding-bottom: 13px; background-image: url( https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/ep-bg.jpg );">
																				<div class="container">
																					<div class="section-featured-tv-show__inner">
																						<header class="featured-tv-show__header"><h2 class="featured-tv-show__header-title"><a href="/react2/tv_show/vikings/">Vikings</a></h2><span class="featured-tv-show__header-subtitle">New Season 5 just flow in. Watch and Debate</span></header>
																						<div class="featured-tv-show__content">
																							<div class="seasons"><ul class="nav"><li class="nav-item"><a href="#tab-5f87f9fc30e410" data-toggle="tab" class="nav-link active show">Season 5</a></li><li class="nav-item"><a href="#tab-5f87f9fc30e411" data-toggle="tab" class="nav-link">Season 4</a></li><li class="nav-item"><a href="#tab-5f87f9fc30e412" data-toggle="tab" class="nav-link">Season 3</a></li><li class="nav-item"><a href="#tab-5f87f9fc30e413" data-toggle="tab" class="nav-link">Season 2</a></li><li class="nav-item"><a href="#tab-5f87f9fc30e414" data-toggle="tab" class="nav-link">Season 1</a></li></ul>
																							</div>
																							<div class="tab-content">
																								<div id="tab-5f87f9fc30e410" class="tab-pane active show">
																									<div class="masvideos masvideos-episodes ">
																										<div class="episodes columns-5">
																											<div class="episodes__inner">
																												<div class="post-2336 episode type-episode status-publish has-post-thumbnail hentry">
																													<div class="episode__poster"><a href="/react2/episode/the-departed-part-1/" class="masvideos-LoopEpisode-link masvideos-loop-episode__link episode__link"><img width="970" height="550" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/14-1.jpg" class="episode__poster--image" alt="" srcset="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/14-1.jpg 970w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/14-1-270x153.jpg 270w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/14-1-220x125.jpg 220w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/14-1-324x184.jpg 324w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/14-1-272x155.jpg 272w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/14-1-214x122.jpg 214w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/14-1-244x138.jpg 244w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/14-1-339x192.jpg 339w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/14-1-327x185.jpg 327w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/14-1-300x170.jpg 300w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/14-1-768x435.jpg 768w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/14-1-120x67.jpg 120w" sizes="(max-width: 970px) 100vw, 970px" /></a>
																													</div>
																													<div class="episode__body"><a href="/react2/episode/the-departed-part-1/" class="masvideos-LoopEpisode-link masvideos-loop-episode__link episode__link"><span class="masvideos-loop-episode__number episode__number">S05E01</span><h3 class="masvideos-loop-episode__title  episode__title">The Departed Part 1</h3></a>
																													</div>
																												</div>
																												<div class="post-2337 episode type-episode status-publish has-post-thumbnail hentry">
																													<div class="episode__poster"><a href="/react2/episode/the-departed-part-2/" class="masvideos-LoopEpisode-link masvideos-loop-episode__link episode__link"><img width="970" height="550" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/15-1.jpg" class="episode__poster--image" alt="" srcset="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/15-1.jpg 970w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/15-1-270x153.jpg 270w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/15-1-220x125.jpg 220w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/15-1-324x184.jpg 324w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/15-1-272x155.jpg 272w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/15-1-214x122.jpg 214w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/15-1-244x138.jpg 244w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/15-1-339x192.jpg 339w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/15-1-327x185.jpg 327w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/15-1-300x170.jpg 300w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/15-1-768x435.jpg 768w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/15-1-120x67.jpg 120w" sizes="(max-width: 970px) 100vw, 970px" /></a>
																													</div>
																													<div class="episode__body"><a href="/react2/episode/the-departed-part-2/" class="masvideos-LoopEpisode-link masvideos-loop-episode__link episode__link"><span class="masvideos-loop-episode__number episode__number">S05E02</span><h3 class="masvideos-loop-episode__title  episode__title">The Departed Part 2</h3></a>
																													</div>
																												</div>
																												<div class="post-2338 episode type-episode status-publish has-post-thumbnail hentry">
																													<div class="episode__poster"><a href="/react2/episode/homeland/" class="masvideos-LoopEpisode-link masvideos-loop-episode__link episode__link"><img width="970" height="550" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/16-1.jpg" class="episode__poster--image" alt="" srcset="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/16-1.jpg 970w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/16-1-270x153.jpg 270w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/16-1-220x125.jpg 220w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/16-1-324x184.jpg 324w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/16-1-272x155.jpg 272w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/16-1-214x122.jpg 214w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/16-1-244x138.jpg 244w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/16-1-339x192.jpg 339w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/16-1-327x185.jpg 327w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/16-1-300x170.jpg 300w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/16-1-768x435.jpg 768w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/16-1-120x67.jpg 120w" sizes="(max-width: 970px) 100vw, 970px" /></a>
																													</div>
																													<div class="episode__body"><a href="/react2/episode/homeland/" class="masvideos-LoopEpisode-link masvideos-loop-episode__link episode__link"><span class="masvideos-loop-episode__number episode__number">S05E03</span><h3 class="masvideos-loop-episode__title  episode__title">Homeland</h3></a>
																													</div>
																												</div>
																												<div class="post-2339 episode type-episode status-publish has-post-thumbnail hentry">
																													<div class="episode__poster"><a href="/react2/episode/the-plan/" class="masvideos-LoopEpisode-link masvideos-loop-episode__link episode__link"><img width="970" height="550" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/17-1.jpg" class="episode__poster--image" alt="" srcset="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/17-1.jpg 970w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/17-1-270x153.jpg 270w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/17-1-220x125.jpg 220w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/17-1-324x184.jpg 324w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/17-1-272x155.jpg 272w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/17-1-214x122.jpg 214w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/17-1-244x138.jpg 244w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/17-1-339x192.jpg 339w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/17-1-327x185.jpg 327w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/17-1-300x170.jpg 300w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/17-1-768x435.jpg 768w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/17-1-120x67.jpg 120w" sizes="(max-width: 970px) 100vw, 970px" /></a>
																													</div>
																													<div class="episode__body"><a href="/react2/episode/the-plan/" class="masvideos-LoopEpisode-link masvideos-loop-episode__link episode__link"><span class="masvideos-loop-episode__number episode__number">S05E04</span><h3 class="masvideos-loop-episode__title  episode__title">The Plan</h3></a>
																													</div>
																												</div>
																												<div class="post-2340 episode type-episode status-publish has-post-thumbnail hentry">
																													<div class="episode__poster"><a href="/react2/episode/the-prisoner/" class="masvideos-LoopEpisode-link masvideos-loop-episode__link episode__link"><img width="970" height="550" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/18-baby-blue.jpg" class="episode__poster--image" alt="" srcset="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/18-baby-blue.jpg 970w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/18-baby-blue-270x153.jpg 270w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/18-baby-blue-220x125.jpg 220w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/18-baby-blue-324x184.jpg 324w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/18-baby-blue-272x155.jpg 272w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/18-baby-blue-214x122.jpg 214w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/18-baby-blue-244x138.jpg 244w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/18-baby-blue-339x192.jpg 339w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/18-baby-blue-327x185.jpg 327w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/18-baby-blue-300x170.jpg 300w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/18-baby-blue-768x435.jpg 768w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/18-baby-blue-120x67.jpg 120w" sizes="(max-width: 970px) 100vw, 970px" /></a>
																													</div>
																													<div class="episode__body"><a href="/react2/episode/the-prisoner/" class="masvideos-LoopEpisode-link masvideos-loop-episode__link episode__link"><span class="masvideos-loop-episode__number episode__number">S05E05</span><h3 class="masvideos-loop-episode__title  episode__title">The Prisoner</h3></a>
																													</div>
																												</div>

																											</div>
																										</div>
																									</div>
																								</div>
																								<div id="tab-5f87f9fc30e411" class="tab-pane">
																									<div class="masvideos masvideos-episodes ">
																										<div class="episodes columns-5">
																											<div class="episodes__inner">
																												<div class="post-2340 episode type-episode status-publish has-post-thumbnail hentry">
																													<div class="episode__poster"><a href="/react2/episode/the-prisoner/" class="masvideos-LoopEpisode-link masvideos-loop-episode__link episode__link"><img width="970" height="550" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/18-baby-blue.jpg" class="episode__poster--image" alt="" srcset="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/18-baby-blue.jpg 970w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/18-baby-blue-270x153.jpg 270w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/18-baby-blue-220x125.jpg 220w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/18-baby-blue-324x184.jpg 324w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/18-baby-blue-272x155.jpg 272w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/18-baby-blue-214x122.jpg 214w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/18-baby-blue-244x138.jpg 244w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/18-baby-blue-339x192.jpg 339w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/18-baby-blue-327x185.jpg 327w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/18-baby-blue-300x170.jpg 300w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/18-baby-blue-768x435.jpg 768w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/18-baby-blue-120x67.jpg 120w" sizes="(max-width: 970px) 100vw, 970px" /></a>
																													</div>
																													<div class="episode__body"><a href="/react2/episode/the-prisoner/" class="masvideos-LoopEpisode-link masvideos-loop-episode__link episode__link"><span class="masvideos-loop-episode__number episode__number">S05E05</span><h3 class="masvideos-loop-episode__title  episode__title">The Prisoner</h3></a>
																													</div>
																												</div>
																												<div class="post-2339 episode type-episode status-publish has-post-thumbnail hentry">
																													<div class="episode__poster"><a href="/react2/episode/the-plan/" class="masvideos-LoopEpisode-link masvideos-loop-episode__link episode__link"><img width="970" height="550" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/17-1.jpg" class="episode__poster--image" alt="" srcset="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/17-1.jpg 970w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/17-1-270x153.jpg 270w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/17-1-220x125.jpg 220w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/17-1-324x184.jpg 324w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/17-1-272x155.jpg 272w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/17-1-214x122.jpg 214w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/17-1-244x138.jpg 244w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/17-1-339x192.jpg 339w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/17-1-327x185.jpg 327w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/17-1-300x170.jpg 300w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/17-1-768x435.jpg 768w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/17-1-120x67.jpg 120w" sizes="(max-width: 970px) 100vw, 970px" /></a>
																													</div>
																													<div class="episode__body"><a href="/react2/episode/the-plan/" class="masvideos-LoopEpisode-link masvideos-loop-episode__link episode__link"><span class="masvideos-loop-episode__number episode__number">S05E04</span><h3 class="masvideos-loop-episode__title  episode__title">The Plan</h3></a>
																													</div>
																												</div>
																												<div class="post-2336 episode type-episode status-publish has-post-thumbnail hentry">
																													<div class="episode__poster"><a href="/react2/episode/the-departed-part-1/" class="masvideos-LoopEpisode-link masvideos-loop-episode__link episode__link"><img width="970" height="550" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/14-1.jpg" class="episode__poster--image" alt="" srcset="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/14-1.jpg 970w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/14-1-270x153.jpg 270w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/14-1-220x125.jpg 220w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/14-1-324x184.jpg 324w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/14-1-272x155.jpg 272w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/14-1-214x122.jpg 214w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/14-1-244x138.jpg 244w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/14-1-339x192.jpg 339w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/14-1-327x185.jpg 327w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/14-1-300x170.jpg 300w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/14-1-768x435.jpg 768w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/14-1-120x67.jpg 120w" sizes="(max-width: 970px) 100vw, 970px" /></a>
																													</div>
																													<div class="episode__body"><a href="/react2/episode/the-departed-part-1/" class="masvideos-LoopEpisode-link masvideos-loop-episode__link episode__link"><span class="masvideos-loop-episode__number episode__number">S05E01</span><h3 class="masvideos-loop-episode__title  episode__title">The Departed Part 1</h3></a>
																													</div>
																												</div>
																												<div class="post-2337 episode type-episode status-publish has-post-thumbnail hentry">
																													<div class="episode__poster"><a href="/react2/episode/the-departed-part-2/" class="masvideos-LoopEpisode-link masvideos-loop-episode__link episode__link"><img width="970" height="550" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/15-1.jpg" class="episode__poster--image" alt="" srcset="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/15-1.jpg 970w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/15-1-270x153.jpg 270w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/15-1-220x125.jpg 220w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/15-1-324x184.jpg 324w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/15-1-272x155.jpg 272w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/15-1-214x122.jpg 214w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/15-1-244x138.jpg 244w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/15-1-339x192.jpg 339w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/15-1-327x185.jpg 327w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/15-1-300x170.jpg 300w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/15-1-768x435.jpg 768w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/15-1-120x67.jpg 120w" sizes="(max-width: 970px) 100vw, 970px" /></a>
																													</div>
																													<div class="episode__body"><a href="/react2/episode/the-departed-part-2/" class="masvideos-LoopEpisode-link masvideos-loop-episode__link episode__link"><span class="masvideos-loop-episode__number episode__number">S05E02</span><h3 class="masvideos-loop-episode__title  episode__title">The Departed Part 2</h3></a>
																													</div>
																												</div>
																												<div class="post-2338 episode type-episode status-publish has-post-thumbnail hentry">
																													<div class="episode__poster"><a href="/react2/episode/homeland/" class="masvideos-LoopEpisode-link masvideos-loop-episode__link episode__link"><img width="970" height="550" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/16-1.jpg" class="episode__poster--image" alt="" srcset="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/16-1.jpg 970w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/16-1-270x153.jpg 270w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/16-1-220x125.jpg 220w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/16-1-324x184.jpg 324w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/16-1-272x155.jpg 272w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/16-1-214x122.jpg 214w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/16-1-244x138.jpg 244w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/16-1-339x192.jpg 339w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/16-1-327x185.jpg 327w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/16-1-300x170.jpg 300w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/16-1-768x435.jpg 768w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/16-1-120x67.jpg 120w" sizes="(max-width: 970px) 100vw, 970px" /></a>
																													</div>
																													<div class="episode__body"><a href="/react2/episode/homeland/" class="masvideos-LoopEpisode-link masvideos-loop-episode__link episode__link"><span class="masvideos-loop-episode__number episode__number">S05E03</span><h3 class="masvideos-loop-episode__title  episode__title">Homeland</h3></a>
																													</div>
																												</div>

																											</div>
																										</div>
																									</div>
																								</div>
																								<div id="tab-5f87f9fc30e412" class="tab-pane">
																									<div class="masvideos masvideos-episodes ">
																										<div class="episodes columns-5">
																											<div class="episodes__inner">
																												<div class="post-2336 episode type-episode status-publish has-post-thumbnail hentry">
																													<div class="episode__poster"><a href="/react2/episode/the-departed-part-1/" class="masvideos-LoopEpisode-link masvideos-loop-episode__link episode__link"><img width="970" height="550" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/14-1.jpg" class="episode__poster--image" alt="" srcset="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/14-1.jpg 970w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/14-1-270x153.jpg 270w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/14-1-220x125.jpg 220w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/14-1-324x184.jpg 324w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/14-1-272x155.jpg 272w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/14-1-214x122.jpg 214w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/14-1-244x138.jpg 244w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/14-1-339x192.jpg 339w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/14-1-327x185.jpg 327w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/14-1-300x170.jpg 300w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/14-1-768x435.jpg 768w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/14-1-120x67.jpg 120w" sizes="(max-width: 970px) 100vw, 970px" /></a>
																													</div>
																													<div class="episode__body"><a href="/react2/episode/the-departed-part-1/" class="masvideos-LoopEpisode-link masvideos-loop-episode__link episode__link"><span class="masvideos-loop-episode__number episode__number">S05E01</span><h3 class="masvideos-loop-episode__title  episode__title">The Departed Part 1</h3></a>
																													</div>
																												</div>
																												<div class="post-2337 episode type-episode status-publish has-post-thumbnail hentry">
																													<div class="episode__poster"><a href="/react2/episode/the-departed-part-2/" class="masvideos-LoopEpisode-link masvideos-loop-episode__link episode__link"><img width="970" height="550" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/15-1.jpg" class="episode__poster--image" alt="" srcset="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/15-1.jpg 970w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/15-1-270x153.jpg 270w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/15-1-220x125.jpg 220w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/15-1-324x184.jpg 324w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/15-1-272x155.jpg 272w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/15-1-214x122.jpg 214w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/15-1-244x138.jpg 244w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/15-1-339x192.jpg 339w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/15-1-327x185.jpg 327w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/15-1-300x170.jpg 300w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/15-1-768x435.jpg 768w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/15-1-120x67.jpg 120w" sizes="(max-width: 970px) 100vw, 970px" /></a>
																													</div>
																													<div class="episode__body"><a href="/react2/episode/the-departed-part-2/" class="masvideos-LoopEpisode-link masvideos-loop-episode__link episode__link"><span class="masvideos-loop-episode__number episode__number">S05E02</span><h3 class="masvideos-loop-episode__title  episode__title">The Departed Part 2</h3></a>
																													</div>
																												</div>
																												<div class="post-2338 episode type-episode status-publish has-post-thumbnail hentry">
																													<div class="episode__poster"><a href="/react2/episode/homeland/" class="masvideos-LoopEpisode-link masvideos-loop-episode__link episode__link"><img width="970" height="550" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/16-1.jpg" class="episode__poster--image" alt="" srcset="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/16-1.jpg 970w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/16-1-270x153.jpg 270w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/16-1-220x125.jpg 220w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/16-1-324x184.jpg 324w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/16-1-272x155.jpg 272w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/16-1-214x122.jpg 214w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/16-1-244x138.jpg 244w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/16-1-339x192.jpg 339w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/16-1-327x185.jpg 327w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/16-1-300x170.jpg 300w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/16-1-768x435.jpg 768w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/16-1-120x67.jpg 120w" sizes="(max-width: 970px) 100vw, 970px" /></a>
																													</div>
																													<div class="episode__body"><a href="/react2/episode/homeland/" class="masvideos-LoopEpisode-link masvideos-loop-episode__link episode__link"><span class="masvideos-loop-episode__number episode__number">S05E03</span><h3 class="masvideos-loop-episode__title  episode__title">Homeland</h3></a>
																													</div>
																												</div>
																												<div class="post-2339 episode type-episode status-publish has-post-thumbnail hentry">
																													<div class="episode__poster"><a href="/react2/episode/the-plan/" class="masvideos-LoopEpisode-link masvideos-loop-episode__link episode__link"><img width="970" height="550" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/17-1.jpg" class="episode__poster--image" alt="" srcset="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/17-1.jpg 970w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/17-1-270x153.jpg 270w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/17-1-220x125.jpg 220w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/17-1-324x184.jpg 324w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/17-1-272x155.jpg 272w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/17-1-214x122.jpg 214w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/17-1-244x138.jpg 244w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/17-1-339x192.jpg 339w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/17-1-327x185.jpg 327w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/17-1-300x170.jpg 300w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/17-1-768x435.jpg 768w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/17-1-120x67.jpg 120w" sizes="(max-width: 970px) 100vw, 970px" /></a>
																													</div>
																													<div class="episode__body"><a href="/react2/episode/the-plan/" class="masvideos-LoopEpisode-link masvideos-loop-episode__link episode__link"><span class="masvideos-loop-episode__number episode__number">S05E04</span><h3 class="masvideos-loop-episode__title  episode__title">The Plan</h3></a>
																													</div>
																												</div>
																												<div class="post-2340 episode type-episode status-publish has-post-thumbnail hentry">
																													<div class="episode__poster"><a href="/react2/episode/the-prisoner/" class="masvideos-LoopEpisode-link masvideos-loop-episode__link episode__link"><img width="970" height="550" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/18-baby-blue.jpg" class="episode__poster--image" alt="" srcset="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/18-baby-blue.jpg 970w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/18-baby-blue-270x153.jpg 270w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/18-baby-blue-220x125.jpg 220w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/18-baby-blue-324x184.jpg 324w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/18-baby-blue-272x155.jpg 272w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/18-baby-blue-214x122.jpg 214w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/18-baby-blue-244x138.jpg 244w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/18-baby-blue-339x192.jpg 339w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/18-baby-blue-327x185.jpg 327w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/18-baby-blue-300x170.jpg 300w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/18-baby-blue-768x435.jpg 768w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/18-baby-blue-120x67.jpg 120w" sizes="(max-width: 970px) 100vw, 970px" /></a>
																													</div>
																													<div class="episode__body"><a href="/react2/episode/the-prisoner/" class="masvideos-LoopEpisode-link masvideos-loop-episode__link episode__link"><span class="masvideos-loop-episode__number episode__number">S05E05</span><h3 class="masvideos-loop-episode__title  episode__title">The Prisoner</h3></a>
																													</div>
																												</div>

																											</div>
																										</div>
																									</div>
																								</div>
																								<div id="tab-5f87f9fc30e413" class="tab-pane">
																									<div class="masvideos masvideos-episodes ">
																										<div class="episodes columns-5">
																											<div class="episodes__inner">
																												<div class="post-2338 episode type-episode status-publish has-post-thumbnail hentry">
																													<div class="episode__poster"><a href="/react2/episode/homeland/" class="masvideos-LoopEpisode-link masvideos-loop-episode__link episode__link"><img width="970" height="550" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/16-1.jpg" class="episode__poster--image" alt="" srcset="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/16-1.jpg 970w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/16-1-270x153.jpg 270w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/16-1-220x125.jpg 220w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/16-1-324x184.jpg 324w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/16-1-272x155.jpg 272w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/16-1-214x122.jpg 214w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/16-1-244x138.jpg 244w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/16-1-339x192.jpg 339w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/16-1-327x185.jpg 327w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/16-1-300x170.jpg 300w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/16-1-768x435.jpg 768w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/16-1-120x67.jpg 120w" sizes="(max-width: 970px) 100vw, 970px" /></a>
																													</div>
																													<div class="episode__body"><a href="/react2/episode/homeland/" class="masvideos-LoopEpisode-link masvideos-loop-episode__link episode__link"><span class="masvideos-loop-episode__number episode__number">S05E03</span><h3 class="masvideos-loop-episode__title  episode__title">Homeland</h3></a>
																													</div>
																												</div>
																												<div class="post-2340 episode type-episode status-publish has-post-thumbnail hentry">
																													<div class="episode__poster"><a href="/react2/episode/the-prisoner/" class="masvideos-LoopEpisode-link masvideos-loop-episode__link episode__link"><img width="970" height="550" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/18-baby-blue.jpg" class="episode__poster--image" alt="" srcset="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/18-baby-blue.jpg 970w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/18-baby-blue-270x153.jpg 270w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/18-baby-blue-220x125.jpg 220w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/18-baby-blue-324x184.jpg 324w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/18-baby-blue-272x155.jpg 272w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/18-baby-blue-214x122.jpg 214w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/18-baby-blue-244x138.jpg 244w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/18-baby-blue-339x192.jpg 339w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/18-baby-blue-327x185.jpg 327w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/18-baby-blue-300x170.jpg 300w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/18-baby-blue-768x435.jpg 768w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/18-baby-blue-120x67.jpg 120w" sizes="(max-width: 970px) 100vw, 970px" /></a>
																													</div>
																													<div class="episode__body"><a href="/react2/episode/the-prisoner/" class="masvideos-LoopEpisode-link masvideos-loop-episode__link episode__link"><span class="masvideos-loop-episode__number episode__number">S05E05</span><h3 class="masvideos-loop-episode__title  episode__title">The Prisoner</h3></a>
																													</div>
																												</div>
																												<div class="post-2336 episode type-episode status-publish has-post-thumbnail hentry">
																													<div class="episode__poster"><a href="/react2/episode/the-departed-part-1/" class="masvideos-LoopEpisode-link masvideos-loop-episode__link episode__link"><img width="970" height="550" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/14-1.jpg" class="episode__poster--image" alt="" srcset="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/14-1.jpg 970w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/14-1-270x153.jpg 270w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/14-1-220x125.jpg 220w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/14-1-324x184.jpg 324w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/14-1-272x155.jpg 272w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/14-1-214x122.jpg 214w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/14-1-244x138.jpg 244w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/14-1-339x192.jpg 339w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/14-1-327x185.jpg 327w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/14-1-300x170.jpg 300w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/14-1-768x435.jpg 768w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/14-1-120x67.jpg 120w" sizes="(max-width: 970px) 100vw, 970px" /></a>
																													</div>
																													<div class="episode__body"><a href="/react2/episode/the-departed-part-1/" class="masvideos-LoopEpisode-link masvideos-loop-episode__link episode__link"><span class="masvideos-loop-episode__number episode__number">S05E01</span><h3 class="masvideos-loop-episode__title  episode__title">The Departed Part 1</h3></a>
																													</div>
																												</div>
																												<div class="post-2337 episode type-episode status-publish has-post-thumbnail hentry">
																													<div class="episode__poster"><a href="/react2/episode/the-departed-part-2/" class="masvideos-LoopEpisode-link masvideos-loop-episode__link episode__link"><img width="970" height="550" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/15-1.jpg" class="episode__poster--image" alt="" srcset="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/15-1.jpg 970w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/15-1-270x153.jpg 270w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/15-1-220x125.jpg 220w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/15-1-324x184.jpg 324w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/15-1-272x155.jpg 272w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/15-1-214x122.jpg 214w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/15-1-244x138.jpg 244w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/15-1-339x192.jpg 339w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/15-1-327x185.jpg 327w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/15-1-300x170.jpg 300w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/15-1-768x435.jpg 768w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/15-1-120x67.jpg 120w" sizes="(max-width: 970px) 100vw, 970px" /></a>
																													</div>
																													<div class="episode__body"><a href="/react2/episode/the-departed-part-2/" class="masvideos-LoopEpisode-link masvideos-loop-episode__link episode__link"><span class="masvideos-loop-episode__number episode__number">S05E02</span><h3 class="masvideos-loop-episode__title  episode__title">The Departed Part 2</h3></a>
																													</div>
																												</div>
																												<div class="post-2339 episode type-episode status-publish has-post-thumbnail hentry">
																													<div class="episode__poster"><a href="/react2/episode/the-plan/" class="masvideos-LoopEpisode-link masvideos-loop-episode__link episode__link"><img width="970" height="550" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/17-1.jpg" class="episode__poster--image" alt="" srcset="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/17-1.jpg 970w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/17-1-270x153.jpg 270w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/17-1-220x125.jpg 220w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/17-1-324x184.jpg 324w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/17-1-272x155.jpg 272w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/17-1-214x122.jpg 214w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/17-1-244x138.jpg 244w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/17-1-339x192.jpg 339w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/17-1-327x185.jpg 327w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/17-1-300x170.jpg 300w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/17-1-768x435.jpg 768w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/17-1-120x67.jpg 120w" sizes="(max-width: 970px) 100vw, 970px" /></a>
																													</div>
																													<div class="episode__body"><a href="/react2/episode/the-plan/" class="masvideos-LoopEpisode-link masvideos-loop-episode__link episode__link"><span class="masvideos-loop-episode__number episode__number">S05E04</span><h3 class="masvideos-loop-episode__title  episode__title">The Plan</h3></a>
																													</div>
																												</div>

																											</div>
																										</div>
																									</div>
																								</div>
																								<div id="tab-5f87f9fc30e414" class="tab-pane">
																									<div class="masvideos masvideos-episodes ">
																										<div class="episodes columns-5">
																											<div class="episodes__inner">
																												<div class="post-2340 episode type-episode status-publish has-post-thumbnail hentry">
																													<div class="episode__poster"><a href="/react2/episode/the-prisoner/" class="masvideos-LoopEpisode-link masvideos-loop-episode__link episode__link"><img width="970" height="550" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/18-baby-blue.jpg" class="episode__poster--image" alt="" srcset="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/18-baby-blue.jpg 970w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/18-baby-blue-270x153.jpg 270w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/18-baby-blue-220x125.jpg 220w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/18-baby-blue-324x184.jpg 324w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/18-baby-blue-272x155.jpg 272w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/18-baby-blue-214x122.jpg 214w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/18-baby-blue-244x138.jpg 244w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/18-baby-blue-339x192.jpg 339w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/18-baby-blue-327x185.jpg 327w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/18-baby-blue-300x170.jpg 300w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/18-baby-blue-768x435.jpg 768w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/18-baby-blue-120x67.jpg 120w" sizes="(max-width: 970px) 100vw, 970px" /></a>
																													</div>
																													<div class="episode__body"><a href="/react2/episode/the-prisoner/" class="masvideos-LoopEpisode-link masvideos-loop-episode__link episode__link"><span class="masvideos-loop-episode__number episode__number">S05E05</span><h3 class="masvideos-loop-episode__title  episode__title">The Prisoner</h3></a>
																													</div>
																												</div>
																												<div class="post-2339 episode type-episode status-publish has-post-thumbnail hentry">
																													<div class="episode__poster"><a href="/react2/episode/the-plan/" class="masvideos-LoopEpisode-link masvideos-loop-episode__link episode__link"><img width="970" height="550" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/17-1.jpg" class="episode__poster--image" alt="" srcset="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/17-1.jpg 970w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/17-1-270x153.jpg 270w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/17-1-220x125.jpg 220w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/17-1-324x184.jpg 324w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/17-1-272x155.jpg 272w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/17-1-214x122.jpg 214w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/17-1-244x138.jpg 244w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/17-1-339x192.jpg 339w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/17-1-327x185.jpg 327w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/17-1-300x170.jpg 300w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/17-1-768x435.jpg 768w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/17-1-120x67.jpg 120w" sizes="(max-width: 970px) 100vw, 970px" /></a>
																													</div>
																													<div class="episode__body"><a href="/react2/episode/the-plan/" class="masvideos-LoopEpisode-link masvideos-loop-episode__link episode__link"><span class="masvideos-loop-episode__number episode__number">S05E04</span><h3 class="masvideos-loop-episode__title  episode__title">The Plan</h3></a>
																													</div>
																												</div>
																												<div class="post-2336 episode type-episode status-publish has-post-thumbnail hentry">
																													<div class="episode__poster"><a href="/react2/episode/the-departed-part-1/" class="masvideos-LoopEpisode-link masvideos-loop-episode__link episode__link"><img width="970" height="550" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/14-1.jpg" class="episode__poster--image" alt="" srcset="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/14-1.jpg 970w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/14-1-270x153.jpg 270w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/14-1-220x125.jpg 220w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/14-1-324x184.jpg 324w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/14-1-272x155.jpg 272w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/14-1-214x122.jpg 214w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/14-1-244x138.jpg 244w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/14-1-339x192.jpg 339w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/14-1-327x185.jpg 327w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/14-1-300x170.jpg 300w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/14-1-768x435.jpg 768w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/14-1-120x67.jpg 120w" sizes="(max-width: 970px) 100vw, 970px" /></a>
																													</div>
																													<div class="episode__body"><a href="/react2/episode/the-departed-part-1/" class="masvideos-LoopEpisode-link masvideos-loop-episode__link episode__link"><span class="masvideos-loop-episode__number episode__number">S05E01</span><h3 class="masvideos-loop-episode__title  episode__title">The Departed Part 1</h3></a>
																													</div>
																												</div>
																												<div class="post-2337 episode type-episode status-publish has-post-thumbnail hentry">
																													<div class="episode__poster"><a href="/react2/episode/the-departed-part-2/" class="masvideos-LoopEpisode-link masvideos-loop-episode__link episode__link"><img width="970" height="550" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/15-1.jpg" class="episode__poster--image" alt="" srcset="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/15-1.jpg 970w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/15-1-270x153.jpg 270w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/15-1-220x125.jpg 220w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/15-1-324x184.jpg 324w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/15-1-272x155.jpg 272w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/15-1-214x122.jpg 214w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/15-1-244x138.jpg 244w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/15-1-339x192.jpg 339w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/15-1-327x185.jpg 327w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/15-1-300x170.jpg 300w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/15-1-768x435.jpg 768w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/15-1-120x67.jpg 120w" sizes="(max-width: 970px) 100vw, 970px" /></a>
																													</div>
																													<div class="episode__body"><a href="/react2/episode/the-departed-part-2/" class="masvideos-LoopEpisode-link masvideos-loop-episode__link episode__link"><span class="masvideos-loop-episode__number episode__number">S05E02</span><h3 class="masvideos-loop-episode__title  episode__title">The Departed Part 2</h3></a>
																													</div>
																												</div>
																												<div class="post-2338 episode type-episode status-publish has-post-thumbnail hentry">
																													<div class="episode__poster"><a href="/react2/episode/homeland/" class="masvideos-LoopEpisode-link masvideos-loop-episode__link episode__link"><img width="970" height="550" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/16-1.jpg" class="episode__poster--image" alt="" srcset="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/16-1.jpg 970w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/16-1-270x153.jpg 270w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/16-1-220x125.jpg 220w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/16-1-324x184.jpg 324w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/16-1-272x155.jpg 272w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/16-1-214x122.jpg 214w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/16-1-244x138.jpg 244w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/16-1-339x192.jpg 339w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/16-1-327x185.jpg 327w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/16-1-300x170.jpg 300w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/16-1-768x435.jpg 768w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/16-1-120x67.jpg 120w" sizes="(max-width: 970px) 100vw, 970px" /></a>
																													</div>
																													<div class="episode__body"><a href="/react2/episode/homeland/" class="masvideos-LoopEpisode-link masvideos-loop-episode__link episode__link"><span class="masvideos-loop-episode__number episode__number">S05E03</span><h3 class="masvideos-loop-episode__title  episode__title">Homeland</h3></a>
																													</div>
																												</div>

																											</div>
																										</div>
																									</div>
																								</div>
																							</div>
																						</div> 
																					</div>
																				</div>
																			</section>
																			<section class="home-section home-tv-show-section-aside-header has-section-header has-bg-color light more-light">
																				<div class="container">
																					<ul class="nav nav-tabs"><li class="nav-item"><a class="nav-link active" href="#">Today</a></li><li class="nav-item"><a class="nav-link" href="#">This week</a></li><li class="nav-item"><a class="nav-link" href="#">Last 30 days</a></li></ul> 
																					<div class="home-tv-show-section-aside-header__inner">
																						<div class="masvideos masvideos-tv-shows ">
																							<div class="tv-shows columns-6">
																								<div class="tv-shows__inner"><header class="home-section__header"><h2 class="home-section__title">Popular TV Series <br> Right Now</h2></header>
																									<div class="tv-show post-2571 tv_show type-tv_show status-publish has-post-thumbnail hentry tv_show_genre-action tv_show_genre-drama tv_show_tag-brother tv_show_tag-brother-relationship tv_show_tag-kings tv_show_tag-vikings">
																										<div class="tv-show__poster"><a href="/react2/tv_show/chicago-med-2/" class="masvideos-LoopTvShow-link masvideos-loop-tv-show__link tv-show__link"><img width="970" height="550" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/42-limit.jpg" class="tv-show__poster--image tv_show__poster--image" alt="" srcset="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/42-limit.jpg 970w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/42-limit-270x153.jpg 270w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/42-limit-220x125.jpg 220w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/42-limit-324x184.jpg 324w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/42-limit-272x155.jpg 272w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/42-limit-214x122.jpg 214w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/42-limit-244x138.jpg 244w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/42-limit-339x192.jpg 339w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/42-limit-327x185.jpg 327w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/42-limit-300x170.jpg 300w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/42-limit-768x435.jpg 768w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/42-limit-120x67.jpg 120w" sizes="(max-width: 970px) 100vw, 970px" /></a>
																										</div>
																										<div class="tv-show__body">
																											<div class="tv-show__info">
																												<div class="tv-show__info--head">
																													<div class="tv-show__meta"><span class="tv-show__meta--genre"><a href="https://demo3.madrasthemes.com/vodi-demos/main/tv-show-genre/action/" rel="tag">Action</a>, <a href="https://demo3.madrasthemes.com/vodi-demos/main/tv-show-genre/drama/" rel="tag">Drama</a></span><span class="tv-show__meta--release-year">2015 to 2016 &#8211; 2016</span>
																													</div><a href="/react2/tv_show/chicago-med-2/" class="masvideos-LoopTvShow-link masvideos-loop-tv-show__link tv-show__link"><h3 class="masvideos-loop-tv-show__title  tv-show__title">Chicago Med</h3></a>
																													<div class="tv-show__episode">Newest Episode: <a href="/react2/episode/brothers-keeper-2/" class="tv-show__episode--link">S01E04</a>
																													</div>
																												</div> 
																												<div class="tv-show__short-description">
																													<div>
																														<p>An emotional thrill ride through the day-to-day chaos of the city&#8217;s most explosive hospital and the courageous team of doctors who hold it together.</p> 
																													</div> 
																												</div>
																												<div class="tv-show__actions"><a href="/react2/tv_show/chicago-med-2/" class="tv-show-actions--link_watch">Watch Now</a> 
																													<div class="tv-show-actions--link_add-to-playlist dropdown">
																														<a class="dropdown-toggle" href="/react2/tv_show/chicago-med-2/" data-toggle="dropdown">+ Playlist</a>
																														<div class="dropdown-menu">
																															<a class="login-link" href="/login?tv-show-playlists/">Sign in to add this tv show to a playlist.</a> 
																														</div>
																													</div>

																												</div>
																											</div>
																											<div class="tv-show__review-info">
																												<div class="viewers-count">
																												</div>
																											</div>
																										</div> 
																										<div class="tv-show__hover-area">
																											<div class="tv-show__hover-area--inner">
																												<div class="tv-show__hover-area--poster">
																													<div class="tv-show__poster"><a href="/react2/tv_show/chicago-med-2/" class="masvideos-LoopTvShow-link masvideos-loop-tv-show__link tv-show__link"><img width="970" height="550" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/42-limit.jpg" class="tv-show__poster--image tv_show__poster--image" alt="" srcset="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/42-limit.jpg 970w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/42-limit-270x153.jpg 270w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/42-limit-220x125.jpg 220w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/42-limit-324x184.jpg 324w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/42-limit-272x155.jpg 272w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/42-limit-214x122.jpg 214w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/42-limit-244x138.jpg 244w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/42-limit-339x192.jpg 339w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/42-limit-327x185.jpg 327w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/42-limit-300x170.jpg 300w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/42-limit-768x435.jpg 768w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/42-limit-120x67.jpg 120w" sizes="(max-width: 970px) 100vw, 970px" /></a>
																													</div>
																													<div class="tv-show__info--head">
																														<div class="tv-show__meta"><span class="tv-show__meta--genre"><a href="https://demo3.madrasthemes.com/vodi-demos/main/tv-show-genre/action/" rel="tag">Action</a>, <a href="https://demo3.madrasthemes.com/vodi-demos/main/tv-show-genre/drama/" rel="tag">Drama</a></span><span class="tv-show__meta--release-year">2015 to 2016 &#8211; 2016</span>
																														</div><a href="/react2/tv_show/chicago-med-2/" class="masvideos-LoopTvShow-link masvideos-loop-tv-show__link tv-show__link"><h3 class="masvideos-loop-tv-show__title  tv-show__title">Chicago Med</h3></a>
																													</div>
																												</div>
																												<div class="tv-show__hover-area--body">
																													<div class="tv-show__season-info">
																														<div class="tv-show__seasons">Seasons #: <a href="/react2/tv_show/chicago-med-2/" class="tv-show__episode--link">Season 1</a><a href="/react2/tv_show/chicago-med-2/" class="tv-show__episode--link">Season 2</a>
																														</div>
																														<div class="tv-show__episode">Newest Episode: <a href="/react2/episode/brothers-keeper-2/" class="tv-show__episode--link">S01E04</a>
																														</div>
																													</div>
																													<div class="tv-show__review-info"> 
																														<div class="tv-show__avg-rating">
																														</div>
																														<div class="viewers-count">
																														</div>
																													</div>
																													<div class="tv-show__actions"><a href="/react2/tv_show/chicago-med-2/" class="tv-show-actions--link_watch">Watch Now</a> 
																														<div class="tv-show-actions--link_add-to-playlist dropdown">
																															<a class="dropdown-toggle" href="/react2/tv_show/chicago-med-2/" data-toggle="dropdown">+ Playlist</a>
																															<div class="dropdown-menu">
																																<a class="login-link" href="/login?tv-show-playlists/">Sign in to add this tv show to a playlist.</a> 
																															</div>
																														</div>
																													</div>
																												</div> 
																											</div>
																										</div>
																									</div>
																									<div class="tv-show post-2560 tv_show type-tv_show status-publish has-post-thumbnail hentry tv_show_genre-action tv_show_genre-comedy tv_show_genre-drama tv_show_tag-4k-ultra tv_show_tag-brother tv_show_tag-brother-relationship tv_show_tag-kings tv_show_tag-vikings">
																										<div class="tv-show__poster"><a href="/react2/tv_show/the-last-man-on-the-earth/" class="masvideos-LoopTvShow-link masvideos-loop-tv-show__link tv-show__link"><img width="970" height="550" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/41-light.jpg" class="tv-show__poster--image tv_show__poster--image" alt="" srcset="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/41-light.jpg 970w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/41-light-270x153.jpg 270w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/41-light-220x125.jpg 220w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/41-light-324x184.jpg 324w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/41-light-272x155.jpg 272w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/41-light-214x122.jpg 214w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/41-light-244x138.jpg 244w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/41-light-339x192.jpg 339w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/41-light-327x185.jpg 327w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/41-light-300x170.jpg 300w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/41-light-768x435.jpg 768w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/41-light-120x67.jpg 120w" sizes="(max-width: 970px) 100vw, 970px" /></a>
																										</div>
																										<div class="tv-show__body">
																											<div class="tv-show__info">
																												<div class="tv-show__info--head">
																													<div class="tv-show__meta"><span class="tv-show__meta--genre"><a href="https://demo3.madrasthemes.com/vodi-demos/main/tv-show-genre/action/" rel="tag">Action</a>, <a href="https://demo3.madrasthemes.com/vodi-demos/main/tv-show-genre/comedy/" rel="tag">Comedy</a>, <a href="https://demo3.madrasthemes.com/vodi-demos/main/tv-show-genre/drama/" rel="tag">Drama</a></span><span class="tv-show__meta--release-year">2015 &#8211; 2015</span>
																													</div><a href="/react2/tv_show/the-last-man-on-the-earth/" class="masvideos-LoopTvShow-link masvideos-loop-tv-show__link tv-show__link"><h3 class="masvideos-loop-tv-show__title  tv-show__title">The Last Man on the earth</h3></a>
																													<div class="tv-show__episode">Newest Episode: <a href="/react2/episode/crickets/" class="tv-show__episode--link">S02E05</a>
																													</div>
																												</div> 
																												<div class="tv-show__short-description">
																													<div>
																														<p>Almost two years after a virus wiped out most of the human race, Phil Miller only wishes for some company, but soon gets more than he bargained for when that company shows up in the form of other survivors.</p> 
																													</div> 
																												</div>
																												<div class="tv-show__actions"><a href="/react2/tv_show/the-last-man-on-the-earth/" class="tv-show-actions--link_watch">Watch Now</a> 
																													<div class="tv-show-actions--link_add-to-playlist dropdown">
																														<a class="dropdown-toggle" href="/react2/tv_show/the-last-man-on-the-earth/" data-toggle="dropdown">+ Playlist</a>
																														<div class="dropdown-menu">
																															<a class="login-link" href="/login?tv-show-playlists/">Sign in to add this tv show to a playlist.</a> 
																														</div>
																													</div>

																												</div>
																											</div>
																											<div class="tv-show__review-info"> <a href="/react2/tv_show/the-last-man-on-the-earth/#reviews" class="avg-rating">
																												<span class="rating-with-count">
																													<svg class="vodi-svg" width="40px" height="39px" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 39"><title>play</title><path fill-rule="evenodd" d="M19.633,-0.000 C21.509,0.035 21.530,1.174 22.167,2.414 C23.329,4.679 24.406,7.067 25.572,9.338 C25.853,9.886 26.431,11.640 26.918,11.834 C27.486,12.203 29.345,12.109 30.165,12.316 C32.170,12.825 34.489,12.860 36.500,13.364 C37.516,13.618 38.689,13.413 39.430,13.927 C39.689,14.107 39.770,14.504 39.984,14.732 C40.047,16.499 39.096,16.843 38.163,17.792 C36.473,19.509 34.784,21.227 33.095,22.944 C32.585,23.462 31.092,24.543 31.036,25.359 C31.423,25.951 31.307,27.455 31.511,28.258 C32.138,30.727 32.213,33.522 32.857,35.987 C33.142,37.078 33.016,38.241 32.303,38.724 C31.108,39.533 29.632,38.193 28.819,37.758 C26.695,36.623 24.601,35.624 22.483,34.457 C21.979,34.179 20.607,33.178 20.108,33.088 C19.748,33.023 18.163,34.107 17.812,34.296 C15.557,35.505 13.340,36.640 11.080,37.839 C10.548,38.120 9.180,39.226 8.309,38.966 C6.955,38.558 6.874,36.993 7.280,35.423 C7.716,33.733 7.697,31.880 8.151,30.109 C8.527,28.642 8.907,26.529 9.022,24.957 C8.092,24.344 7.202,23.107 6.408,22.300 C4.760,20.625 3.059,18.990 1.340,17.389 C0.646,16.742 -0.578,15.515 0.311,14.249 C0.915,13.388 2.364,13.656 3.557,13.364 C6.678,12.599 10.114,12.468 13.298,11.834 C14.186,9.747 15.306,7.711 16.307,5.716 C16.954,4.426 17.496,3.163 18.128,1.931 C18.334,1.531 18.358,1.093 18.603,0.724 C18.845,0.362 19.299,0.273 19.633,-0.000 Z" /></svg> <span class="avg-rating-number"> 6.0</span>
																												</span>
																												<span class="rating-number-with-text">
																													<span class="avg-rating-number"> 6.0</span>
																													<span class="avg-rating-text">
																														<span>1</span> Vote </span>
																													</span>
																												</a>
																												<div class="viewers-count">
																												</div>
																											</div>
																										</div> 
																										<div class="tv-show__hover-area">
																											<div class="tv-show__hover-area--inner">
																												<div class="tv-show__hover-area--poster">
																													<div class="tv-show__poster"><a href="/react2/tv_show/the-last-man-on-the-earth/" class="masvideos-LoopTvShow-link masvideos-loop-tv-show__link tv-show__link"><img width="970" height="550" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/41-light.jpg" class="tv-show__poster--image tv_show__poster--image" alt="" srcset="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/41-light.jpg 970w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/41-light-270x153.jpg 270w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/41-light-220x125.jpg 220w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/41-light-324x184.jpg 324w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/41-light-272x155.jpg 272w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/41-light-214x122.jpg 214w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/41-light-244x138.jpg 244w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/41-light-339x192.jpg 339w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/41-light-327x185.jpg 327w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/41-light-300x170.jpg 300w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/41-light-768x435.jpg 768w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/41-light-120x67.jpg 120w" sizes="(max-width: 970px) 100vw, 970px" /></a>
																													</div>
																													<div class="tv-show__info--head">
																														<div class="tv-show__meta"><span class="tv-show__meta--genre"><a href="https://demo3.madrasthemes.com/vodi-demos/main/tv-show-genre/action/" rel="tag">Action</a>, <a href="https://demo3.madrasthemes.com/vodi-demos/main/tv-show-genre/comedy/" rel="tag">Comedy</a>, <a href="https://demo3.madrasthemes.com/vodi-demos/main/tv-show-genre/drama/" rel="tag">Drama</a></span><span class="tv-show__meta--release-year">2015 &#8211; 2015</span>
																														</div><a href="/react2/tv_show/the-last-man-on-the-earth/" class="masvideos-LoopTvShow-link masvideos-loop-tv-show__link tv-show__link"><h3 class="masvideos-loop-tv-show__title  tv-show__title">The Last Man on the earth</h3></a>
																													</div>
																												</div>
																												<div class="tv-show__hover-area--body">
																													<div class="tv-show__season-info">
																														<div class="tv-show__seasons">Seasons #: <a href="/react2/tv_show/the-last-man-on-the-earth/" class="tv-show__episode--link">Season 1</a><a href="/react2/tv_show/the-last-man-on-the-earth/" class="tv-show__episode--link">Season 2</a>
																														</div>
																														<div class="tv-show__episode">Newest Episode: <a href="/react2/episode/crickets/" class="tv-show__episode--link">S02E05</a>
																														</div>
																													</div>
																													<div class="tv-show__review-info"> 
																														<div class="tv-show__avg-rating">
																															<a href="/react2/tv_show/the-last-man-on-the-earth/#reviews" class="avg-rating">
																																<div class="avg-rating__inner">
																																	<span class="avg-rating__number"> 6.0</span>
																																	<span class="avg-rating__text">
																																		<span>1</span> Vote </span>
																																	</div>
																																</a>
																															</div>
																															<div class="viewers-count">
																															</div>
																														</div>
																														<div class="tv-show__actions"><a href="/react2/tv_show/the-last-man-on-the-earth/" class="tv-show-actions--link_watch">Watch Now</a> 
																															<div class="tv-show-actions--link_add-to-playlist dropdown">
																																<a class="dropdown-toggle" href="/react2/tv_show/the-last-man-on-the-earth/" data-toggle="dropdown">+ Playlist</a>
																																<div class="dropdown-menu">
																																	<a class="login-link" href="/login?tv-show-playlists/">Sign in to add this tv show to a playlist.</a> 
																																</div>
																															</div>
																														</div>
																													</div> 
																												</div>
																											</div>
																										</div>
																										<div class="tv-show post-2547 tv_show type-tv_show status-publish has-post-thumbnail hentry tv_show_genre-comedy tv_show_genre-drama tv_show_tag-4k-ultra tv_show_tag-brother tv_show_tag-brother-relationship tv_show_tag-king tv_show_tag-vikings">
																											<div class="tv-show__poster"><a href="/react2/tv_show/unbreakable-kimmy-schmidt/" class="masvideos-LoopTvShow-link masvideos-loop-tv-show__link tv-show__link"><img width="970" height="550" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/40-keep-going.jpg" class="tv-show__poster--image tv_show__poster--image" alt="" srcset="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/40-keep-going.jpg 970w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/40-keep-going-270x153.jpg 270w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/40-keep-going-220x125.jpg 220w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/40-keep-going-324x184.jpg 324w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/40-keep-going-272x155.jpg 272w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/40-keep-going-214x122.jpg 214w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/40-keep-going-244x138.jpg 244w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/40-keep-going-339x192.jpg 339w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/40-keep-going-327x185.jpg 327w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/40-keep-going-300x170.jpg 300w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/40-keep-going-768x435.jpg 768w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/40-keep-going-120x67.jpg 120w" sizes="(max-width: 970px) 100vw, 970px" /></a>
																											</div>
																											<div class="tv-show__body">
																												<div class="tv-show__info">
																													<div class="tv-show__info--head">
																														<div class="tv-show__meta"><span class="tv-show__meta--genre"><a href="https://demo3.madrasthemes.com/vodi-demos/main/tv-show-genre/comedy/" rel="tag">Comedy</a>, <a href="https://demo3.madrasthemes.com/vodi-demos/main/tv-show-genre/drama/" rel="tag">Drama</a></span><span class="tv-show__meta--release-year">2015 &#8211; 2016</span>
																														</div><a href="/react2/tv_show/unbreakable-kimmy-schmidt/" class="masvideos-LoopTvShow-link masvideos-loop-tv-show__link tv-show__link"><h3 class="masvideos-loop-tv-show__title  tv-show__title">Unbreakable Kimmy Schmidt</h3></a>
																														<div class="tv-show__episode">Newest Episode: <a href="/react2/episode/kimmy-gives-up/" class="tv-show__episode--link">S02E05</a>
																														</div>
																													</div> 
																													<div class="tv-show__short-description">
																														<div>
																															<p>Praesent iaculis, purus ac vehicula mattis, arcu lorem blandit nisl, non laoreet dui mi eget elit. Donec porttitor ex vel augue maximus luctus. Vivamus finibus nibh eu nunc volutpat suscipit. Nam vulputate libero quis nisi euismod rhoncus. Sed eu euismod felis. Aenean ullamcorper dapibus odio ac tempor. Aliquam iaculis, quam vitae imperdiet consectetur, mi ante semper metus, ac efficitur nisi justo ut eros. Maecenas suscipit turpis fermentum elementum scelerisque. </p><p>Sed leo elit, volutpat quis aliquet eu, elementum eget arcu. Aenean ligula tellus, malesuada eu ultrices vel, vulputate sit amet metus. Donec tincidunt sapien ut enim feugiat, sed egestas dolor ornare.</p> 
																														</div> 
																													</div>
																													<div class="tv-show__actions"><a href="/react2/tv_show/unbreakable-kimmy-schmidt/" class="tv-show-actions--link_watch">Watch Now</a> 
																														<div class="tv-show-actions--link_add-to-playlist dropdown">
																															<a class="dropdown-toggle" href="/react2/tv_show/unbreakable-kimmy-schmidt/" data-toggle="dropdown">+ Playlist</a>
																															<div class="dropdown-menu">
																																<a class="login-link" href="/login?tv-show-playlists/">Sign in to add this tv show to a playlist.</a> 
																															</div>
																														</div>

																													</div>
																												</div>
																												<div class="tv-show__review-info">
																													<div class="viewers-count">
																													</div>
																												</div>
																											</div> 
																											<div class="tv-show__hover-area">
																												<div class="tv-show__hover-area--inner">
																													<div class="tv-show__hover-area--poster">
																														<div class="tv-show__poster"><a href="/react2/tv_show/unbreakable-kimmy-schmidt/" class="masvideos-LoopTvShow-link masvideos-loop-tv-show__link tv-show__link"><img width="970" height="550" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/40-keep-going.jpg" class="tv-show__poster--image tv_show__poster--image" alt="" srcset="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/40-keep-going.jpg 970w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/40-keep-going-270x153.jpg 270w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/40-keep-going-220x125.jpg 220w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/40-keep-going-324x184.jpg 324w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/40-keep-going-272x155.jpg 272w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/40-keep-going-214x122.jpg 214w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/40-keep-going-244x138.jpg 244w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/40-keep-going-339x192.jpg 339w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/40-keep-going-327x185.jpg 327w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/40-keep-going-300x170.jpg 300w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/40-keep-going-768x435.jpg 768w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/40-keep-going-120x67.jpg 120w" sizes="(max-width: 970px) 100vw, 970px" /></a>
																														</div>
																														<div class="tv-show__info--head">
																															<div class="tv-show__meta"><span class="tv-show__meta--genre"><a href="https://demo3.madrasthemes.com/vodi-demos/main/tv-show-genre/comedy/" rel="tag">Comedy</a>, <a href="https://demo3.madrasthemes.com/vodi-demos/main/tv-show-genre/drama/" rel="tag">Drama</a></span><span class="tv-show__meta--release-year">2015 &#8211; 2016</span>
																															</div><a href="/react2/tv_show/unbreakable-kimmy-schmidt/" class="masvideos-LoopTvShow-link masvideos-loop-tv-show__link tv-show__link"><h3 class="masvideos-loop-tv-show__title  tv-show__title">Unbreakable Kimmy Schmidt</h3></a>
																														</div>
																													</div>
																													<div class="tv-show__hover-area--body">
																														<div class="tv-show__season-info">
																															<div class="tv-show__seasons">Seasons #: <a href="/react2/tv_show/unbreakable-kimmy-schmidt/" class="tv-show__episode--link">Season 1</a><a href="/react2/tv_show/unbreakable-kimmy-schmidt/" class="tv-show__episode--link">Season 2</a>
																															</div>
																															<div class="tv-show__episode">Newest Episode: <a href="/react2/episode/kimmy-gives-up/" class="tv-show__episode--link">S02E05</a>
																															</div>
																														</div>
																														<div class="tv-show__review-info"> 
																															<div class="tv-show__avg-rating">
																															</div>
																															<div class="viewers-count">
																															</div>
																														</div>
																														<div class="tv-show__actions"><a href="/react2/tv_show/unbreakable-kimmy-schmidt/" class="tv-show-actions--link_watch">Watch Now</a> 
																															<div class="tv-show-actions--link_add-to-playlist dropdown">
																																<a class="dropdown-toggle" href="/react2/tv_show/unbreakable-kimmy-schmidt/" data-toggle="dropdown">+ Playlist</a>
																																<div class="dropdown-menu">
																																	<a class="login-link" href="/login?tv-show-playlists/">Sign in to add this tv show to a playlist.</a> 
																																</div>
																															</div>
																														</div>
																													</div> 
																												</div>
																											</div>
																										</div>
																										<div class="tv-show post-2534 tv_show type-tv_show status-publish has-post-thumbnail hentry tv_show_genre-action tv_show_genre-drama tv_show_tag-brother tv_show_tag-brother-relationship tv_show_tag-king tv_show_tag-premieres tv_show_tag-vikings">
																											<div class="tv-show__poster"><a href="/react2/tv_show/house-of-cards/" class="masvideos-LoopTvShow-link masvideos-loop-tv-show__link tv-show__link"><img width="970" height="550" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/39-journey.jpg" class="tv-show__poster--image tv_show__poster--image" alt="" srcset="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/39-journey.jpg 970w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/39-journey-270x153.jpg 270w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/39-journey-220x125.jpg 220w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/39-journey-324x184.jpg 324w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/39-journey-272x155.jpg 272w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/39-journey-214x122.jpg 214w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/39-journey-244x138.jpg 244w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/39-journey-339x192.jpg 339w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/39-journey-327x185.jpg 327w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/39-journey-300x170.jpg 300w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/39-journey-768x435.jpg 768w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/39-journey-120x67.jpg 120w" sizes="(max-width: 970px) 100vw, 970px" /></a>
																											</div>
																											<div class="tv-show__body">
																												<div class="tv-show__info">
																													<div class="tv-show__info--head">
																														<div class="tv-show__meta"><span class="tv-show__meta--genre"><a href="https://demo3.madrasthemes.com/vodi-demos/main/tv-show-genre/action/" rel="tag">Action</a>, <a href="https://demo3.madrasthemes.com/vodi-demos/main/tv-show-genre/drama/" rel="tag">Drama</a></span><span class="tv-show__meta--release-year">2013 &#8211; 2015</span>
																														</div><a href="/react2/tv_show/house-of-cards/" class="masvideos-LoopTvShow-link masvideos-loop-tv-show__link tv-show__link"><h3 class="masvideos-loop-tv-show__title  tv-show__title">House of cards</h3></a>
																														<div class="tv-show__episode">Newest Episode: <a href="/react2/episode/chapter-18/" class="tv-show__episode--link">S02E05</a>
																														</div>
																													</div> 
																													<div class="tv-show__short-description">
																														<div>
																															<p>A Congressman works with his equally conniving wife to exact revenge on the people who betrayed him.</p> 
																														</div> 
																													</div>
																													<div class="tv-show__actions"><a href="/react2/tv_show/house-of-cards/" class="tv-show-actions--link_watch">Watch Now</a> 
																														<div class="tv-show-actions--link_add-to-playlist dropdown">
																															<a class="dropdown-toggle" href="/react2/tv_show/house-of-cards/" data-toggle="dropdown">+ Playlist</a>
																															<div class="dropdown-menu">
																																<a class="login-link" href="/login?tv-show-playlists/">Sign in to add this tv show to a playlist.</a> 
																															</div>
																														</div>

																													</div>
																												</div>
																												<div class="tv-show__review-info"> <a href="/react2/tv_show/house-of-cards/#reviews" class="avg-rating">
																													<span class="rating-with-count">
																														<svg class="vodi-svg" width="40px" height="39px" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 39"><title>play</title><path fill-rule="evenodd" d="M19.633,-0.000 C21.509,0.035 21.530,1.174 22.167,2.414 C23.329,4.679 24.406,7.067 25.572,9.338 C25.853,9.886 26.431,11.640 26.918,11.834 C27.486,12.203 29.345,12.109 30.165,12.316 C32.170,12.825 34.489,12.860 36.500,13.364 C37.516,13.618 38.689,13.413 39.430,13.927 C39.689,14.107 39.770,14.504 39.984,14.732 C40.047,16.499 39.096,16.843 38.163,17.792 C36.473,19.509 34.784,21.227 33.095,22.944 C32.585,23.462 31.092,24.543 31.036,25.359 C31.423,25.951 31.307,27.455 31.511,28.258 C32.138,30.727 32.213,33.522 32.857,35.987 C33.142,37.078 33.016,38.241 32.303,38.724 C31.108,39.533 29.632,38.193 28.819,37.758 C26.695,36.623 24.601,35.624 22.483,34.457 C21.979,34.179 20.607,33.178 20.108,33.088 C19.748,33.023 18.163,34.107 17.812,34.296 C15.557,35.505 13.340,36.640 11.080,37.839 C10.548,38.120 9.180,39.226 8.309,38.966 C6.955,38.558 6.874,36.993 7.280,35.423 C7.716,33.733 7.697,31.880 8.151,30.109 C8.527,28.642 8.907,26.529 9.022,24.957 C8.092,24.344 7.202,23.107 6.408,22.300 C4.760,20.625 3.059,18.990 1.340,17.389 C0.646,16.742 -0.578,15.515 0.311,14.249 C0.915,13.388 2.364,13.656 3.557,13.364 C6.678,12.599 10.114,12.468 13.298,11.834 C14.186,9.747 15.306,7.711 16.307,5.716 C16.954,4.426 17.496,3.163 18.128,1.931 C18.334,1.531 18.358,1.093 18.603,0.724 C18.845,0.362 19.299,0.273 19.633,-0.000 Z" /></svg> <span class="avg-rating-number"> 8.0</span>
																													</span>
																													<span class="rating-number-with-text">
																														<span class="avg-rating-number"> 8.0</span>
																														<span class="avg-rating-text">
																															<span>1</span> Vote </span>
																														</span>
																													</a>
																													<div class="viewers-count">
																													</div>
																												</div>
																											</div> 
																											<div class="tv-show__hover-area">
																												<div class="tv-show__hover-area--inner">
																													<div class="tv-show__hover-area--poster">
																														<div class="tv-show__poster"><a href="/react2/tv_show/house-of-cards/" class="masvideos-LoopTvShow-link masvideos-loop-tv-show__link tv-show__link"><img width="970" height="550" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/39-journey.jpg" class="tv-show__poster--image tv_show__poster--image" alt="" srcset="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/39-journey.jpg 970w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/39-journey-270x153.jpg 270w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/39-journey-220x125.jpg 220w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/39-journey-324x184.jpg 324w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/39-journey-272x155.jpg 272w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/39-journey-214x122.jpg 214w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/39-journey-244x138.jpg 244w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/39-journey-339x192.jpg 339w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/39-journey-327x185.jpg 327w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/39-journey-300x170.jpg 300w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/39-journey-768x435.jpg 768w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/39-journey-120x67.jpg 120w" sizes="(max-width: 970px) 100vw, 970px" /></a>
																														</div>
																														<div class="tv-show__info--head">
																															<div class="tv-show__meta"><span class="tv-show__meta--genre"><a href="https://demo3.madrasthemes.com/vodi-demos/main/tv-show-genre/action/" rel="tag">Action</a>, <a href="https://demo3.madrasthemes.com/vodi-demos/main/tv-show-genre/drama/" rel="tag">Drama</a></span><span class="tv-show__meta--release-year">2013 &#8211; 2015</span>
																															</div><a href="/react2/tv_show/house-of-cards/" class="masvideos-LoopTvShow-link masvideos-loop-tv-show__link tv-show__link"><h3 class="masvideos-loop-tv-show__title  tv-show__title">House of cards</h3></a>
																														</div>
																													</div>
																													<div class="tv-show__hover-area--body">
																														<div class="tv-show__season-info">
																															<div class="tv-show__seasons">Seasons #: <a href="/react2/tv_show/house-of-cards/" class="tv-show__episode--link">Season 1</a><a href="/react2/tv_show/house-of-cards/" class="tv-show__episode--link">Season 2</a>
																															</div>
																															<div class="tv-show__episode">Newest Episode: <a href="/react2/episode/chapter-18/" class="tv-show__episode--link">S02E05</a>
																															</div>
																														</div>
																														<div class="tv-show__review-info"> 
																															<div class="tv-show__avg-rating">
																																<a href="/react2/tv_show/house-of-cards/#reviews" class="avg-rating">
																																	<div class="avg-rating__inner">
																																		<span class="avg-rating__number"> 8.0</span>
																																		<span class="avg-rating__text">
																																			<span>1</span> Vote </span>
																																		</div>
																																	</a>
																																</div>
																																<div class="viewers-count">
																																</div>
																															</div>
																															<div class="tv-show__actions"><a href="/react2/tv_show/house-of-cards/" class="tv-show-actions--link_watch">Watch Now</a> 
																																<div class="tv-show-actions--link_add-to-playlist dropdown">
																																	<a class="dropdown-toggle" href="/react2/tv_show/house-of-cards/" data-toggle="dropdown">+ Playlist</a>
																																	<div class="dropdown-menu">
																																		<a class="login-link" href="/login?tv-show-playlists/">Sign in to add this tv show to a playlist.</a> 
																																	</div>
																																</div>
																															</div>
																														</div> 
																													</div>
																												</div>
																											</div>
																											<div class="tv-show post-2521 tv_show type-tv_show status-publish has-post-thumbnail hentry tv_show_genre-drama tv_show_genre-romance tv_show_tag-brother tv_show_tag-brother-relationship tv_show_tag-kings tv_show_tag-vikings">
																												<div class="tv-show__poster"><a href="/react2/tv_show/greys-anatomy/" class="masvideos-LoopTvShow-link masvideos-loop-tv-show__link tv-show__link"><img width="970" height="550" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/38-holidays.jpg" class="tv-show__poster--image tv_show__poster--image" alt="" srcset="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/38-holidays.jpg 970w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/38-holidays-270x153.jpg 270w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/38-holidays-220x125.jpg 220w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/38-holidays-324x184.jpg 324w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/38-holidays-272x155.jpg 272w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/38-holidays-214x122.jpg 214w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/38-holidays-244x138.jpg 244w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/38-holidays-339x192.jpg 339w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/38-holidays-327x185.jpg 327w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/38-holidays-300x170.jpg 300w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/38-holidays-768x435.jpg 768w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/38-holidays-120x67.jpg 120w" sizes="(max-width: 970px) 100vw, 970px" /></a>
																												</div>
																												<div class="tv-show__body">
																													<div class="tv-show__info">
																														<div class="tv-show__info--head">
																															<div class="tv-show__meta"><span class="tv-show__meta--genre"><a href="https://demo3.madrasthemes.com/vodi-demos/main/tv-show-genre/drama/" rel="tag">Drama</a>, <a href="https://demo3.madrasthemes.com/vodi-demos/main/tv-show-genre/romance/" rel="tag">Romance</a></span><span class="tv-show__meta--release-year">2005 &#8211; 2005</span>
																															</div><a href="/react2/tv_show/greys-anatomy/" class="masvideos-LoopTvShow-link masvideos-loop-tv-show__link tv-show__link"><h3 class="masvideos-loop-tv-show__title  tv-show__title">Grey&#039;s anatomy</h3></a>
																															<div class="tv-show__episode">Newest Episode: <a href="/react2/episode/bring-the-pain/" class="tv-show__episode--link">S02E05</a>
																															</div>
																														</div> 
																														<div class="tv-show__short-description">
																															<div>
																																<p>A drama centered on the personal and professional lives of five surgical interns and their supervisors.</p> 
																															</div> 
																														</div>
																														<div class="tv-show__actions"><a href="/react2/tv_show/greys-anatomy/" class="tv-show-actions--link_watch">Watch Now</a> 
																															<div class="tv-show-actions--link_add-to-playlist dropdown">
																																<a class="dropdown-toggle" href="/react2/tv_show/greys-anatomy/" data-toggle="dropdown">+ Playlist</a>
																																<div class="dropdown-menu">
																																	<a class="login-link" href="/login?tv-show-playlists/">Sign in to add this tv show to a playlist.</a> 
																																</div>
																															</div>

																														</div>
																													</div>
																													<div class="tv-show__review-info"> <a href="/react2/tv_show/greys-anatomy/#reviews" class="avg-rating">
																														<span class="rating-with-count">
																															<svg class="vodi-svg" width="40px" height="39px" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 39"><title>play</title><path fill-rule="evenodd" d="M19.633,-0.000 C21.509,0.035 21.530,1.174 22.167,2.414 C23.329,4.679 24.406,7.067 25.572,9.338 C25.853,9.886 26.431,11.640 26.918,11.834 C27.486,12.203 29.345,12.109 30.165,12.316 C32.170,12.825 34.489,12.860 36.500,13.364 C37.516,13.618 38.689,13.413 39.430,13.927 C39.689,14.107 39.770,14.504 39.984,14.732 C40.047,16.499 39.096,16.843 38.163,17.792 C36.473,19.509 34.784,21.227 33.095,22.944 C32.585,23.462 31.092,24.543 31.036,25.359 C31.423,25.951 31.307,27.455 31.511,28.258 C32.138,30.727 32.213,33.522 32.857,35.987 C33.142,37.078 33.016,38.241 32.303,38.724 C31.108,39.533 29.632,38.193 28.819,37.758 C26.695,36.623 24.601,35.624 22.483,34.457 C21.979,34.179 20.607,33.178 20.108,33.088 C19.748,33.023 18.163,34.107 17.812,34.296 C15.557,35.505 13.340,36.640 11.080,37.839 C10.548,38.120 9.180,39.226 8.309,38.966 C6.955,38.558 6.874,36.993 7.280,35.423 C7.716,33.733 7.697,31.880 8.151,30.109 C8.527,28.642 8.907,26.529 9.022,24.957 C8.092,24.344 7.202,23.107 6.408,22.300 C4.760,20.625 3.059,18.990 1.340,17.389 C0.646,16.742 -0.578,15.515 0.311,14.249 C0.915,13.388 2.364,13.656 3.557,13.364 C6.678,12.599 10.114,12.468 13.298,11.834 C14.186,9.747 15.306,7.711 16.307,5.716 C16.954,4.426 17.496,3.163 18.128,1.931 C18.334,1.531 18.358,1.093 18.603,0.724 C18.845,0.362 19.299,0.273 19.633,-0.000 Z" /></svg> <span class="avg-rating-number"> 8.0</span>
																														</span>
																														<span class="rating-number-with-text">
																															<span class="avg-rating-number"> 8.0</span>
																															<span class="avg-rating-text">
																																<span>1</span> Vote </span>
																															</span>
																														</a>
																														<div class="viewers-count">
																														</div>
																													</div>
																												</div> 
																												<div class="tv-show__hover-area">
																													<div class="tv-show__hover-area--inner">
																														<div class="tv-show__hover-area--poster">
																															<div class="tv-show__poster"><a href="/react2/tv_show/greys-anatomy/" class="masvideos-LoopTvShow-link masvideos-loop-tv-show__link tv-show__link"><img width="970" height="550" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/38-holidays.jpg" class="tv-show__poster--image tv_show__poster--image" alt="" srcset="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/38-holidays.jpg 970w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/38-holidays-270x153.jpg 270w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/38-holidays-220x125.jpg 220w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/38-holidays-324x184.jpg 324w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/38-holidays-272x155.jpg 272w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/38-holidays-214x122.jpg 214w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/38-holidays-244x138.jpg 244w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/38-holidays-339x192.jpg 339w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/38-holidays-327x185.jpg 327w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/38-holidays-300x170.jpg 300w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/38-holidays-768x435.jpg 768w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/38-holidays-120x67.jpg 120w" sizes="(max-width: 970px) 100vw, 970px" /></a>
																															</div>
																															<div class="tv-show__info--head">
																																<div class="tv-show__meta"><span class="tv-show__meta--genre"><a href="https://demo3.madrasthemes.com/vodi-demos/main/tv-show-genre/drama/" rel="tag">Drama</a>, <a href="https://demo3.madrasthemes.com/vodi-demos/main/tv-show-genre/romance/" rel="tag">Romance</a></span><span class="tv-show__meta--release-year">2005 &#8211; 2005</span>
																																</div><a href="/react2/tv_show/greys-anatomy/" class="masvideos-LoopTvShow-link masvideos-loop-tv-show__link tv-show__link"><h3 class="masvideos-loop-tv-show__title  tv-show__title">Grey&#039;s anatomy</h3></a>
																															</div>
																														</div>
																														<div class="tv-show__hover-area--body">
																															<div class="tv-show__season-info">
																																<div class="tv-show__seasons">Seasons #: <a href="/react2/tv_show/greys-anatomy/" class="tv-show__episode--link">Season 1</a><a href="/react2/tv_show/greys-anatomy/" class="tv-show__episode--link">Season 2</a>
																																</div>
																																<div class="tv-show__episode">Newest Episode: <a href="/react2/episode/bring-the-pain/" class="tv-show__episode--link">S02E05</a>
																																</div>
																															</div>
																															<div class="tv-show__review-info"> 
																																<div class="tv-show__avg-rating">
																																	<a href="/react2/tv_show/greys-anatomy/#reviews" class="avg-rating">
																																		<div class="avg-rating__inner">
																																			<span class="avg-rating__number"> 8.0</span>
																																			<span class="avg-rating__text">
																																				<span>1</span> Vote </span>
																																			</div>
																																		</a>
																																	</div>
																																	<div class="viewers-count">
																																	</div>
																																</div>
																																<div class="tv-show__actions"><a href="/react2/tv_show/greys-anatomy/" class="tv-show-actions--link_watch">Watch Now</a> 
																																	<div class="tv-show-actions--link_add-to-playlist dropdown">
																																		<a class="dropdown-toggle" href="/react2/tv_show/greys-anatomy/" data-toggle="dropdown">+ Playlist</a>
																																		<div class="dropdown-menu">
																																			<a class="login-link" href="/login?tv-show-playlists/">Sign in to add this tv show to a playlist.</a> 
																																		</div>
																																	</div>
																																</div>
																															</div> 
																														</div>
																													</div>
																												</div>
																												<div class="tv-show post-2508 tv_show type-tv_show status-publish has-post-thumbnail hentry tv_show_genre-drama tv_show_genre-fantasy tv_show_tag-brother tv_show_tag-brother-relationship tv_show_tag-kings tv_show_tag-vikings">
																													<div class="tv-show__poster"><a href="/react2/tv_show/dom-grozypeeny-dreadful/" class="masvideos-LoopTvShow-link masvideos-loop-tv-show__link tv-show__link"><img width="970" height="550" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/37-hater.jpg" class="tv-show__poster--image tv_show__poster--image" alt="" srcset="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/37-hater.jpg 970w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/37-hater-270x153.jpg 270w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/37-hater-220x125.jpg 220w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/37-hater-324x184.jpg 324w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/37-hater-272x155.jpg 272w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/37-hater-214x122.jpg 214w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/37-hater-244x138.jpg 244w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/37-hater-339x192.jpg 339w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/37-hater-327x185.jpg 327w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/37-hater-300x170.jpg 300w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/37-hater-768x435.jpg 768w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/37-hater-120x67.jpg 120w" sizes="(max-width: 970px) 100vw, 970px" /></a>
																													</div>
																													<div class="tv-show__body">
																														<div class="tv-show__info">
																															<div class="tv-show__info--head">
																																<div class="tv-show__meta"><span class="tv-show__meta--genre"><a href="https://demo3.madrasthemes.com/vodi-demos/main/tv-show-genre/drama/" rel="tag">Drama</a>, <a href="https://demo3.madrasthemes.com/vodi-demos/main/tv-show-genre/fantasy/" rel="tag">Fantasy</a></span><span class="tv-show__meta--release-year">2014 &#8211; 2015</span>
																																</div><a href="/react2/tv_show/dom-grozypeeny-dreadful/" class="masvideos-LoopTvShow-link masvideos-loop-tv-show__link tv-show__link"><h3 class="masvideos-loop-tv-show__title  tv-show__title">Dom grozy(Peeny Dreadful)</h3></a>
																																<div class="tv-show__episode">Newest Episode: <a href="/react2/episode/little-scorpion/" class="tv-show__episode--link">S02E05</a>
																																</div>
																															</div> 
																															<div class="tv-show__short-description">
																																<div>
																																	<p>Explorer Sir Malcolm Murray, American gunslinger Ethan Chandler, scientist Victor Frankenstein, and medium Vanessa Ives unite to combat supernatural threats in Victorian London.</p> 
																																</div> 
																															</div>
																															<div class="tv-show__actions"><a href="/react2/tv_show/dom-grozypeeny-dreadful/" class="tv-show-actions--link_watch">Watch Now</a> 
																																<div class="tv-show-actions--link_add-to-playlist dropdown">
																																	<a class="dropdown-toggle" href="/react2/tv_show/dom-grozypeeny-dreadful/" data-toggle="dropdown">+ Playlist</a>
																																	<div class="dropdown-menu">
																																		<a class="login-link" href="/login?tv-show-playlists/">Sign in to add this tv show to a playlist.</a> 
																																	</div>
																																</div>

																															</div>
																														</div>
																														<div class="tv-show__review-info">
																															<div class="viewers-count">
																															</div>
																														</div>
																													</div> 
																													<div class="tv-show__hover-area">
																														<div class="tv-show__hover-area--inner">
																															<div class="tv-show__hover-area--poster">
																																<div class="tv-show__poster"><a href="/react2/tv_show/dom-grozypeeny-dreadful/" class="masvideos-LoopTvShow-link masvideos-loop-tv-show__link tv-show__link"><img width="970" height="550" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/37-hater.jpg" class="tv-show__poster--image tv_show__poster--image" alt="" srcset="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/37-hater.jpg 970w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/37-hater-270x153.jpg 270w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/37-hater-220x125.jpg 220w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/37-hater-324x184.jpg 324w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/37-hater-272x155.jpg 272w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/37-hater-214x122.jpg 214w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/37-hater-244x138.jpg 244w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/37-hater-339x192.jpg 339w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/37-hater-327x185.jpg 327w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/37-hater-300x170.jpg 300w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/37-hater-768x435.jpg 768w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/37-hater-120x67.jpg 120w" sizes="(max-width: 970px) 100vw, 970px" /></a>
																																</div>
																																<div class="tv-show__info--head">
																																	<div class="tv-show__meta"><span class="tv-show__meta--genre"><a href="https://demo3.madrasthemes.com/vodi-demos/main/tv-show-genre/drama/" rel="tag">Drama</a>, <a href="https://demo3.madrasthemes.com/vodi-demos/main/tv-show-genre/fantasy/" rel="tag">Fantasy</a></span><span class="tv-show__meta--release-year">2014 &#8211; 2015</span>
																																	</div><a href="/react2/tv_show/dom-grozypeeny-dreadful/" class="masvideos-LoopTvShow-link masvideos-loop-tv-show__link tv-show__link"><h3 class="masvideos-loop-tv-show__title  tv-show__title">Dom grozy(Peeny Dreadful)</h3></a>
																																</div>
																															</div>
																															<div class="tv-show__hover-area--body">
																																<div class="tv-show__season-info">
																																	<div class="tv-show__seasons">Seasons #: <a href="/react2/tv_show/dom-grozypeeny-dreadful/" class="tv-show__episode--link">Season 1</a><a href="/react2/tv_show/dom-grozypeeny-dreadful/" class="tv-show__episode--link">season 2</a>
																																	</div>
																																	<div class="tv-show__episode">Newest Episode: <a href="/react2/episode/little-scorpion/" class="tv-show__episode--link">S02E05</a>
																																	</div>
																																</div>
																																<div class="tv-show__review-info"> 
																																	<div class="tv-show__avg-rating">
																																	</div>
																																	<div class="viewers-count">
																																	</div>
																																</div>
																																<div class="tv-show__actions"><a href="/react2/tv_show/dom-grozypeeny-dreadful/" class="tv-show-actions--link_watch">Watch Now</a> 
																																	<div class="tv-show-actions--link_add-to-playlist dropdown">
																																		<a class="dropdown-toggle" href="/react2/tv_show/dom-grozypeeny-dreadful/" data-toggle="dropdown">+ Playlist</a>
																																		<div class="dropdown-menu">
																																			<a class="login-link" href="/login?tv-show-playlists/">Sign in to add this tv show to a playlist.</a> 
																																		</div>
																																	</div>
																																</div>
																															</div> 
																														</div>
																													</div>
																												</div>
																												<div class="tv-show post-2495 tv_show type-tv_show status-publish has-post-thumbnail hentry tv_show_genre-crime tv_show_genre-drama tv_show_tag-brother tv_show_tag-brother-relationship tv_show_tag-kings tv_show_tag-vikings">
																													<div class="tv-show__poster"><a href="/react2/tv_show/cardinal/" class="masvideos-LoopTvShow-link masvideos-loop-tv-show__link tv-show__link"><img width="970" height="550" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/36-garden.jpg" class="tv-show__poster--image tv_show__poster--image" alt="" srcset="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/36-garden.jpg 970w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/36-garden-270x153.jpg 270w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/36-garden-220x125.jpg 220w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/36-garden-324x184.jpg 324w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/36-garden-272x155.jpg 272w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/36-garden-214x122.jpg 214w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/36-garden-244x138.jpg 244w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/36-garden-339x192.jpg 339w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/36-garden-327x185.jpg 327w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/36-garden-300x170.jpg 300w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/36-garden-768x435.jpg 768w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/36-garden-120x67.jpg 120w" sizes="(max-width: 970px) 100vw, 970px" /></a>
																													</div>
																													<div class="tv-show__body">
																														<div class="tv-show__info">
																															<div class="tv-show__info--head">
																																<div class="tv-show__meta"><span class="tv-show__meta--genre"><a href="https://demo3.madrasthemes.com/vodi-demos/main/tv-show-genre/crime/" rel="tag">Crime</a>, <a href="https://demo3.madrasthemes.com/vodi-demos/main/tv-show-genre/drama/" rel="tag">Drama</a></span><span class="tv-show__meta--release-year">2017 &#8211; 2018</span>
																																</div><a href="/react2/tv_show/cardinal/" class="masvideos-LoopTvShow-link masvideos-loop-tv-show__link tv-show__link"><h3 class="masvideos-loop-tv-show__title  tv-show__title">Cardinal</h3></a>
																																<div class="tv-show__episode">Newest Episode: <a href="/react2/episode/northwind/" class="tv-show__episode--link">S02E05</a>
																																</div>
																															</div> 
																															<div class="tv-show__short-description">
																																<div>
																																	<p>Cardinal struggles to right past wrongs that could derail his investigation and end his career, as the case grows more violent and twisted, and the clock ticks down on the killer&#8217;s next victim.</p> 
																																</div> 
																															</div>
																															<div class="tv-show__actions"><a href="/react2/tv_show/cardinal/" class="tv-show-actions--link_watch">Watch Now</a> 
																																<div class="tv-show-actions--link_add-to-playlist dropdown">
																																	<a class="dropdown-toggle" href="/react2/tv_show/cardinal/" data-toggle="dropdown">+ Playlist</a>
																																	<div class="dropdown-menu">
																																		<a class="login-link" href="/login?tv-show-playlists/">Sign in to add this tv show to a playlist.</a> 
																																	</div>
																																</div>

																															</div>
																														</div>
																														<div class="tv-show__review-info"> <a href="/react2/tv_show/cardinal/#reviews" class="avg-rating">
																															<span class="rating-with-count">
																																<svg class="vodi-svg" width="40px" height="39px" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 39"><title>play</title><path fill-rule="evenodd" d="M19.633,-0.000 C21.509,0.035 21.530,1.174 22.167,2.414 C23.329,4.679 24.406,7.067 25.572,9.338 C25.853,9.886 26.431,11.640 26.918,11.834 C27.486,12.203 29.345,12.109 30.165,12.316 C32.170,12.825 34.489,12.860 36.500,13.364 C37.516,13.618 38.689,13.413 39.430,13.927 C39.689,14.107 39.770,14.504 39.984,14.732 C40.047,16.499 39.096,16.843 38.163,17.792 C36.473,19.509 34.784,21.227 33.095,22.944 C32.585,23.462 31.092,24.543 31.036,25.359 C31.423,25.951 31.307,27.455 31.511,28.258 C32.138,30.727 32.213,33.522 32.857,35.987 C33.142,37.078 33.016,38.241 32.303,38.724 C31.108,39.533 29.632,38.193 28.819,37.758 C26.695,36.623 24.601,35.624 22.483,34.457 C21.979,34.179 20.607,33.178 20.108,33.088 C19.748,33.023 18.163,34.107 17.812,34.296 C15.557,35.505 13.340,36.640 11.080,37.839 C10.548,38.120 9.180,39.226 8.309,38.966 C6.955,38.558 6.874,36.993 7.280,35.423 C7.716,33.733 7.697,31.880 8.151,30.109 C8.527,28.642 8.907,26.529 9.022,24.957 C8.092,24.344 7.202,23.107 6.408,22.300 C4.760,20.625 3.059,18.990 1.340,17.389 C0.646,16.742 -0.578,15.515 0.311,14.249 C0.915,13.388 2.364,13.656 3.557,13.364 C6.678,12.599 10.114,12.468 13.298,11.834 C14.186,9.747 15.306,7.711 16.307,5.716 C16.954,4.426 17.496,3.163 18.128,1.931 C18.334,1.531 18.358,1.093 18.603,0.724 C18.845,0.362 19.299,0.273 19.633,-0.000 Z" /></svg> <span class="avg-rating-number"> 8.0</span>
																															</span>
																															<span class="rating-number-with-text">
																																<span class="avg-rating-number"> 8.0</span>
																																<span class="avg-rating-text">
																																	<span>1</span> Vote </span>
																																</span>
																															</a>
																															<div class="viewers-count">
																															</div>
																														</div>
																													</div> 
																													<div class="tv-show__hover-area">
																														<div class="tv-show__hover-area--inner">
																															<div class="tv-show__hover-area--poster">
																																<div class="tv-show__poster">
																																	<a href="/react2/tv_show/cardinal/" class="masvideos-LoopTvShow-link masvideos-loop-tv-show__link tv-show__link"><img width="970" height="550" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/36-garden.jpg" class="tv-show__poster--image tv_show__poster--image" alt="" srcset="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/36-garden.jpg 970w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/36-garden-270x153.jpg 270w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/36-garden-220x125.jpg 220w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/36-garden-324x184.jpg 324w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/36-garden-272x155.jpg 272w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/36-garden-214x122.jpg 214w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/36-garden-244x138.jpg 244w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/36-garden-339x192.jpg 339w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/36-garden-327x185.jpg 327w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/36-garden-300x170.jpg 300w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/36-garden-768x435.jpg 768w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/36-garden-120x67.jpg 120w" sizes="(max-width: 970px) 100vw, 970px" /></a>
																																</div>
																																<div class="tv-show__info--head">
																																	<div class="tv-show__meta"><span class="tv-show__meta--genre"><a href="https://demo3.madrasthemes.com/vodi-demos/main/tv-show-genre/crime/" rel="tag">Crime</a>, <a href="https://demo3.madrasthemes.com/vodi-demos/main/tv-show-genre/drama/" rel="tag">Drama</a></span><span class="tv-show__meta--release-year">2017 &#8211; 2018</span>
																																	</div><a href="/react2/tv_show/cardinal/" class="masvideos-LoopTvShow-link masvideos-loop-tv-show__link tv-show__link"><h3 class="masvideos-loop-tv-show__title  tv-show__title">Cardinal</h3></a>
																																</div>
																															</div>
																															<div class="tv-show__hover-area--body">
																																<div class="tv-show__season-info">
																																	<div class="tv-show__seasons">Seasons #: <a href="/react2/tv_show/cardinal/" class="tv-show__episode--link">Season 1</a><a href="/react2/tv_show/cardinal/" class="tv-show__episode--link">Season 2</a>
																																	</div>
																																	<div class="tv-show__episode">Newest Episode: <a href="/react2/episode/northwind/" class="tv-show__episode--link">S02E05</a>
																																	</div>
																																</div>
																																<div class="tv-show__review-info"> 
																																	<div class="tv-show__avg-rating">
																																		<a href="/react2/tv_show/cardinal/#reviews" class="avg-rating">
																																			<div class="avg-rating__inner">
																																				<span class="avg-rating__number"> 8.0</span>
																																				<span class="avg-rating__text">
																																					<span>1</span> Vote </span>
																																				</div>
																																			</a>
																																		</div>
																																		<div class="viewers-count">
																																		</div>
																																	</div>
																																	<div class="tv-show__actions">
																																		<a href="/react2/tv_show/cardinal/" class="tv-show-actions--link_watch">Watch Now</a> 
																																		<div class="tv-show-actions--link_add-to-playlist dropdown">
																																			<a class="dropdown-toggle" href="/react2/tv_show/cardinal/" data-toggle="dropdown">+ Playlist</a>
																																			<div class="dropdown-menu">
																																				<a class="login-link" href="/login?tv-show-playlists/">Sign in to add this tv show to a playlist.</a> 
																																			</div>
																																		</div>
																																	</div>
																																</div> 
																															</div>
																														</div>
																													</div>
																													<div class="tv-show post-2482 tv_show type-tv_show status-publish has-post-thumbnail hentry tv_show_genre-comedy tv_show_genre-crime tv_show_genre-drama tv_show_tag-brother tv_show_tag-brother-relationship tv_show_tag-kings tv_show_tag-original tv_show_tag-vikings">
																														<div class="tv-show__poster">
																															<a href="/react2/tv_show/orange-is-the-new-black/" class="masvideos-LoopTvShow-link masvideos-loop-tv-show__link tv-show__link"><img width="970" height="550" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/35-fine-line.jpg" class="tv-show__poster--image tv_show__poster--image" alt="" srcset="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/35-fine-line.jpg 970w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/35-fine-line-270x153.jpg 270w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/35-fine-line-220x125.jpg 220w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/35-fine-line-324x184.jpg 324w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/35-fine-line-272x155.jpg 272w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/35-fine-line-214x122.jpg 214w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/35-fine-line-244x138.jpg 244w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/35-fine-line-339x192.jpg 339w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/35-fine-line-327x185.jpg 327w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/35-fine-line-300x170.jpg 300w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/35-fine-line-768x435.jpg 768w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/35-fine-line-120x67.jpg 120w" sizes="(max-width: 970px) 100vw, 970px" /></a>
																														</div>
																														<div class="tv-show__body">
																															<div class="tv-show__info">
																																<div class="tv-show__info--head">
																																	<div class="tv-show__meta"><span class="tv-show__meta--genre"><a href="https://demo3.madrasthemes.com/vodi-demos/main/tv-show-genre/comedy/" rel="tag">Comedy</a>, <a href="https://demo3.madrasthemes.com/vodi-demos/main/tv-show-genre/crime/" rel="tag">Crime</a>, <a href="https://demo3.madrasthemes.com/vodi-demos/main/tv-show-genre/drama/" rel="tag">Drama</a></span><span class="tv-show__meta--release-year">2013 &#8211; 2014</span>
																																	</div><a href="/react2/tv_show/orange-is-the-new-black/" class="masvideos-LoopTvShow-link masvideos-loop-tv-show__link tv-show__link"><h3 class="masvideos-loop-tv-show__title  tv-show__title">Orange is the New black</h3></a>
																																	<div class="tv-show__episode">Newest Episode: <a href="/react2/episode/low-self-esteem-city/" class="tv-show__episode--link">S01E05</a>
																																	</div>
																																</div> 
																																<div class="tv-show__short-description">
																																	<div>
																																		<p>Convicted of a decade old crime of transporting drug money to an ex-girlfriend, normally law-abiding Piper Chapman is sentenced to a year and a half behind bars to face the reality of how life-changing prison can really be.</p> 
																																	</div> 
																																</div>
																																<div class="tv-show__actions">
																																	<a href="/react2/tv_show/orange-is-the-new-black/" class="tv-show-actions--link_watch">Watch Now</a> 
																																	<div class="tv-show-actions--link_add-to-playlist dropdown">
																																		<a class="dropdown-toggle" href="/react2/tv_show/orange-is-the-new-black/" data-toggle="dropdown">+ Playlist</a>
																																		<div class="dropdown-menu">
																																			<a class="login-link" href="/login?tv-show-playlists/">Sign in to add this tv show to a playlist.</a> 
																																		</div>
																																	</div>

																																</div>
																															</div>
																															<div class="tv-show__review-info"> 
																																<a href="/react2/tv_show/orange-is-the-new-black/#reviews" class="avg-rating">
																																	<span class="rating-with-count">
																																		<svg class="vodi-svg" width="40px" height="39px" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 39"><title>play</title><path fill-rule="evenodd" d="M19.633,-0.000 C21.509,0.035 21.530,1.174 22.167,2.414 C23.329,4.679 24.406,7.067 25.572,9.338 C25.853,9.886 26.431,11.640 26.918,11.834 C27.486,12.203 29.345,12.109 30.165,12.316 C32.170,12.825 34.489,12.860 36.500,13.364 C37.516,13.618 38.689,13.413 39.430,13.927 C39.689,14.107 39.770,14.504 39.984,14.732 C40.047,16.499 39.096,16.843 38.163,17.792 C36.473,19.509 34.784,21.227 33.095,22.944 C32.585,23.462 31.092,24.543 31.036,25.359 C31.423,25.951 31.307,27.455 31.511,28.258 C32.138,30.727 32.213,33.522 32.857,35.987 C33.142,37.078 33.016,38.241 32.303,38.724 C31.108,39.533 29.632,38.193 28.819,37.758 C26.695,36.623 24.601,35.624 22.483,34.457 C21.979,34.179 20.607,33.178 20.108,33.088 C19.748,33.023 18.163,34.107 17.812,34.296 C15.557,35.505 13.340,36.640 11.080,37.839 C10.548,38.120 9.180,39.226 8.309,38.966 C6.955,38.558 6.874,36.993 7.280,35.423 C7.716,33.733 7.697,31.880 8.151,30.109 C8.527,28.642 8.907,26.529 9.022,24.957 C8.092,24.344 7.202,23.107 6.408,22.300 C4.760,20.625 3.059,18.990 1.340,17.389 C0.646,16.742 -0.578,15.515 0.311,14.249 C0.915,13.388 2.364,13.656 3.557,13.364 C6.678,12.599 10.114,12.468 13.298,11.834 C14.186,9.747 15.306,7.711 16.307,5.716 C16.954,4.426 17.496,3.163 18.128,1.931 C18.334,1.531 18.358,1.093 18.603,0.724 C18.845,0.362 19.299,0.273 19.633,-0.000 Z" /></svg> <span class="avg-rating-number"> 8.0</span>
																																	</span>
																																	<span class="rating-number-with-text">
																																		<span class="avg-rating-number"> 8.0</span>
																																		<span class="avg-rating-text">
																																			<span>1</span> Vote </span>
																																		</span>
																																	</a>
																																	<div class="viewers-count">
																																	</div>
																																</div>
																															</div> 
																															<div class="tv-show__hover-area">
																																<div class="tv-show__hover-area--inner">
																																	<div class="tv-show__hover-area--poster">
																																		<div class="tv-show__poster"><a href="/react2/tv_show/orange-is-the-new-black/" class="masvideos-LoopTvShow-link masvideos-loop-tv-show__link tv-show__link"><img width="970" height="550" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/35-fine-line.jpg" class="tv-show__poster--image tv_show__poster--image" alt="" srcset="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/35-fine-line.jpg 970w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/35-fine-line-270x153.jpg 270w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/35-fine-line-220x125.jpg 220w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/35-fine-line-324x184.jpg 324w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/35-fine-line-272x155.jpg 272w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/35-fine-line-214x122.jpg 214w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/35-fine-line-244x138.jpg 244w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/35-fine-line-339x192.jpg 339w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/35-fine-line-327x185.jpg 327w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/35-fine-line-300x170.jpg 300w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/35-fine-line-768x435.jpg 768w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/35-fine-line-120x67.jpg 120w" sizes="(max-width: 970px) 100vw, 970px" /></a>
																																		</div>
																																		<div class="tv-show__info--head">
																																			<div class="tv-show__meta"><span class="tv-show__meta--genre"><a href="https://demo3.madrasthemes.com/vodi-demos/main/tv-show-genre/comedy/" rel="tag">Comedy</a>, <a href="https://demo3.madrasthemes.com/vodi-demos/main/tv-show-genre/crime/" rel="tag">Crime</a>, <a href="https://demo3.madrasthemes.com/vodi-demos/main/tv-show-genre/drama/" rel="tag">Drama</a></span><span class="tv-show__meta--release-year">2013 &#8211; 2014</span>
																																			</div><a href="/react2/tv_show/orange-is-the-new-black/" class="masvideos-LoopTvShow-link masvideos-loop-tv-show__link tv-show__link"><h3 class="masvideos-loop-tv-show__title  tv-show__title">Orange is the New black</h3></a>
																																		</div>
																																	</div>
																																	<div class="tv-show__hover-area--body">
																																		<div class="tv-show__season-info">
																																			<div class="tv-show__seasons">Seasons #: <a href="/react2/tv_show/orange-is-the-new-black/" class="tv-show__episode--link">Season 1</a><a href="/react2/tv_show/orange-is-the-new-black/" class="tv-show__episode--link">Season 2</a>
																																			</div>
																																			<div class="tv-show__episode">Newest Episode: <a href="/react2/episode/low-self-esteem-city/" class="tv-show__episode--link">S01E05</a>
																																			</div>
																																		</div>
																																		<div class="tv-show__review-info"> 
																																			<div class="tv-show__avg-rating">
																																				<a href="/react2/tv_show/orange-is-the-new-black/#reviews" class="avg-rating">
																																					<div class="avg-rating__inner">
																																						<span class="avg-rating__number"> 8.0</span>
																																						<span class="avg-rating__text">
																																							<span>1</span> Vote </span>
																																						</div>
																																					</a>
																																				</div>
																																				<div class="viewers-count">
																																				</div>
																																			</div>
																																			<div class="tv-show__actions"><a href="/react2/tv_show/orange-is-the-new-black/" class="tv-show-actions--link_watch">Watch Now</a> 
																																				<div class="tv-show-actions--link_add-to-playlist dropdown">
																																					<a class="dropdown-toggle" href="/react2/tv_show/orange-is-the-new-black/" data-toggle="dropdown">+ Playlist</a>
																																					<div class="dropdown-menu">
																																						<a class="login-link" href="/login?tv-show-playlists/">Sign in to add this tv show to a playlist.</a> 
																																					</div>
																																				</div>
																																			</div>
																																		</div> 
																																	</div>
																																</div>
																															</div>
																															<div class="tv-show post-2469 tv_show type-tv_show status-publish has-post-thumbnail hentry tv_show_genre-biography tv_show_genre-crime tv_show_genre-drama tv_show_tag-brother tv_show_tag-brother-relationship tv_show_tag-kings tv_show_tag-vikings">
																																<div class="tv-show__poster">
																																	<a href="/react2/tv_show/narcos/" class="masvideos-LoopTvShow-link masvideos-loop-tv-show__link tv-show__link"><img width="970" height="550" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/34-fighter.jpg" class="tv-show__poster--image tv_show__poster--image" alt="" srcset="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/34-fighter.jpg 970w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/34-fighter-270x153.jpg 270w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/34-fighter-220x125.jpg 220w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/34-fighter-324x184.jpg 324w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/34-fighter-272x155.jpg 272w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/34-fighter-214x122.jpg 214w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/34-fighter-244x138.jpg 244w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/34-fighter-339x192.jpg 339w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/34-fighter-327x185.jpg 327w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/34-fighter-300x170.jpg 300w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/34-fighter-768x435.jpg 768w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/34-fighter-120x67.jpg 120w" sizes="(max-width: 970px) 100vw, 970px" /></a>
																																</div>
																																<div class="tv-show__body">
																																	<div class="tv-show__info">
																																		<div class="tv-show__info--head">
																																			<div class="tv-show__meta"><span class="tv-show__meta--genre"><a href="https://demo3.madrasthemes.com/vodi-demos/main/tv-show-genre/biography/" rel="tag">Biography</a>, <a href="https://demo3.madrasthemes.com/vodi-demos/main/tv-show-genre/crime/" rel="tag">Crime</a>, <a href="https://demo3.madrasthemes.com/vodi-demos/main/tv-show-genre/drama/" rel="tag">Drama</a></span><span class="tv-show__meta--release-year">2015 &#8211; 2016</span>
																																			</div><a href="/react2/tv_show/narcos/" class="masvideos-LoopTvShow-link masvideos-loop-tv-show__link tv-show__link"><h3 class="masvideos-loop-tv-show__title  tv-show__title">Narcos</h3></a>
																																			<div class="tv-show__episode">Newest Episode: <a href="/react2/episode/the-enemy-of-my-enemy-is-my-friend/" class="tv-show__episode--link">S01E05</a>
																																			</div>
																																		</div> 
																																		<div class="tv-show__short-description">
																																			<div>
																																				<p>A chronicled look at the criminal exploits of Colombian drug lord Pablo Escobar, as well as the many other drug kingpins who plagued the country through the years.</p> 
																																			</div> 
																																		</div>
																																		<div class="tv-show__actions">
																																			<a href="/react2/tv_show/narcos/" class="tv-show-actions--link_watch">Watch Now</a> 
																																			<div class="tv-show-actions--link_add-to-playlist dropdown">
																																				<a class="dropdown-toggle" href="/react2/tv_show/narcos/" data-toggle="dropdown">+ Playlist</a>
																																				<div class="dropdown-menu">
																																					<a class="login-link" href="/login?tv-show-playlists/">Sign in to add this tv show to a playlist.</a> 
																																				</div>
																																			</div>

																																		</div>
																																	</div>
																																	<div class="tv-show__review-info"> 
																																		<a href="/react2/tv_show/narcos/#reviews" class="avg-rating">
																																			<span class="rating-with-count">
																																				<svg class="vodi-svg" width="40px" height="39px" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 39"><title>play</title><path fill-rule="evenodd" d="M19.633,-0.000 C21.509,0.035 21.530,1.174 22.167,2.414 C23.329,4.679 24.406,7.067 25.572,9.338 C25.853,9.886 26.431,11.640 26.918,11.834 C27.486,12.203 29.345,12.109 30.165,12.316 C32.170,12.825 34.489,12.860 36.500,13.364 C37.516,13.618 38.689,13.413 39.430,13.927 C39.689,14.107 39.770,14.504 39.984,14.732 C40.047,16.499 39.096,16.843 38.163,17.792 C36.473,19.509 34.784,21.227 33.095,22.944 C32.585,23.462 31.092,24.543 31.036,25.359 C31.423,25.951 31.307,27.455 31.511,28.258 C32.138,30.727 32.213,33.522 32.857,35.987 C33.142,37.078 33.016,38.241 32.303,38.724 C31.108,39.533 29.632,38.193 28.819,37.758 C26.695,36.623 24.601,35.624 22.483,34.457 C21.979,34.179 20.607,33.178 20.108,33.088 C19.748,33.023 18.163,34.107 17.812,34.296 C15.557,35.505 13.340,36.640 11.080,37.839 C10.548,38.120 9.180,39.226 8.309,38.966 C6.955,38.558 6.874,36.993 7.280,35.423 C7.716,33.733 7.697,31.880 8.151,30.109 C8.527,28.642 8.907,26.529 9.022,24.957 C8.092,24.344 7.202,23.107 6.408,22.300 C4.760,20.625 3.059,18.990 1.340,17.389 C0.646,16.742 -0.578,15.515 0.311,14.249 C0.915,13.388 2.364,13.656 3.557,13.364 C6.678,12.599 10.114,12.468 13.298,11.834 C14.186,9.747 15.306,7.711 16.307,5.716 C16.954,4.426 17.496,3.163 18.128,1.931 C18.334,1.531 18.358,1.093 18.603,0.724 C18.845,0.362 19.299,0.273 19.633,-0.000 Z" /></svg> <span class="avg-rating-number"> 7.0</span>
																																			</span>
																																			<span class="rating-number-with-text">
																																				<span class="avg-rating-number"> 7.0</span>
																																				<span class="avg-rating-text">
																																					<span>1</span> Vote </span>
																																				</span>
																																			</a>
																																			<div class="viewers-count">
																																			</div>
																																		</div>
																																	</div> 
																																	<div class="tv-show__hover-area">
																																		<div class="tv-show__hover-area--inner">
																																			<div class="tv-show__hover-area--poster">
																																				<div class="tv-show__poster"><a href="/react2/tv_show/narcos/" class="masvideos-LoopTvShow-link masvideos-loop-tv-show__link tv-show__link">
																																					<img width="970" height="550" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/34-fighter.jpg" class="tv-show__poster--image tv_show__poster--image" alt="" srcset="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/34-fighter.jpg 970w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/34-fighter-270x153.jpg 270w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/34-fighter-220x125.jpg 220w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/34-fighter-324x184.jpg 324w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/34-fighter-272x155.jpg 272w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/34-fighter-214x122.jpg 214w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/34-fighter-244x138.jpg 244w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/34-fighter-339x192.jpg 339w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/34-fighter-327x185.jpg 327w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/34-fighter-300x170.jpg 300w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/34-fighter-768x435.jpg 768w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/34-fighter-120x67.jpg 120w" sizes="(max-width: 970px) 100vw, 970px" /></a>
																																				</div>
																																				<div class="tv-show__info--head">
																																					<div class="tv-show__meta"><span class="tv-show__meta--genre"><a href="https://demo3.madrasthemes.com/vodi-demos/main/tv-show-genre/biography/" rel="tag">Biography</a>, <a href="https://demo3.madrasthemes.com/vodi-demos/main/tv-show-genre/crime/" rel="tag">Crime</a>, <a href="https://demo3.madrasthemes.com/vodi-demos/main/tv-show-genre/drama/" rel="tag">Drama</a></span><span class="tv-show__meta--release-year">2015 &#8211; 2016</span>
																																					</div><a href="/react2/tv_show/narcos/" class="masvideos-LoopTvShow-link masvideos-loop-tv-show__link tv-show__link"><h3 class="masvideos-loop-tv-show__title  tv-show__title">Narcos</h3></a>
																																				</div>
																																			</div>
																																			<div class="tv-show__hover-area--body">
																																				<div class="tv-show__season-info">
																																					<div class="tv-show__seasons">Seasons #: <a href="/react2/tv_show/narcos/" class="tv-show__episode--link">Season 1</a><a href="/react2/tv_show/narcos/" class="tv-show__episode--link">Season 2</a>
																																					</div>
																																					<div class="tv-show__episode">Newest Episode: <a href="/react2/episode/the-enemy-of-my-enemy-is-my-friend/" class="tv-show__episode--link">S01E05</a>
																																					</div>
																																				</div>
																																				<div class="tv-show__review-info"> 
																																					<div class="tv-show__avg-rating">
																																						<a href="/react2/tv_show/narcos/#reviews" class="avg-rating">
																																							<div class="avg-rating__inner">
																																								<span class="avg-rating__number"> 7.0</span>
																																								<span class="avg-rating__text">
																																									<span>1</span> Vote </span>
																																								</div>
																																							</a>
																																						</div>
																																						<div class="viewers-count">
																																						</div>
																																					</div>
																																					<div class="tv-show__actions"><a href="/react2/tv_show/narcos/" class="tv-show-actions--link_watch">Watch Now</a> 
																																						<div class="tv-show-actions--link_add-to-playlist dropdown">
																																							<a class="dropdown-toggle" href="/react2/tv_show/narcos/" data-toggle="dropdown">+ Playlist</a>
																																							<div class="dropdown-menu">
																																								<a class="login-link" href="/login?tv-show-playlists/">Sign in to add this tv show to a playlist.</a> 
																																							</div>
																																						</div>
																																					</div>
																																				</div> 
																																			</div>
																																		</div>
																																	</div>
																																	<div class="tv-show post-2443 tv_show type-tv_show status-publish has-post-thumbnail hentry tv_show_genre-crime tv_show_genre-drama tv_show_tag-brother tv_show_tag-brother-relationship tv_show_tag-kings tv_show_tag-vikings">
																																		<div class="tv-show__poster"><a href="/react2/tv_show/better-call-saul/" class="masvideos-LoopTvShow-link masvideos-loop-tv-show__link tv-show__link"><img width="970" height="550" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/33-experience.jpg" class="tv-show__poster--image tv_show__poster--image" alt="" srcset="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/33-experience.jpg 970w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/33-experience-270x153.jpg 270w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/33-experience-220x125.jpg 220w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/33-experience-324x184.jpg 324w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/33-experience-272x155.jpg 272w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/33-experience-214x122.jpg 214w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/33-experience-244x138.jpg 244w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/33-experience-339x192.jpg 339w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/33-experience-327x185.jpg 327w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/33-experience-300x170.jpg 300w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/33-experience-768x435.jpg 768w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/33-experience-120x67.jpg 120w" sizes="(max-width: 970px) 100vw, 970px" /></a>
																																		</div>
																																		<div class="tv-show__body">
																																			<div class="tv-show__info">
																																				<div class="tv-show__info--head">
																																					<div class="tv-show__meta"><span class="tv-show__meta--genre"><a href="https://demo3.madrasthemes.com/vodi-demos/main/tv-show-genre/crime/" rel="tag">Crime</a>, <a href="https://demo3.madrasthemes.com/vodi-demos/main/tv-show-genre/drama/" rel="tag">Drama</a></span><span class="tv-show__meta--release-year">2015 &#8211; 2016</span>
																																					</div><a href="/react2/tv_show/better-call-saul/" class="masvideos-LoopTvShow-link masvideos-loop-tv-show__link tv-show__link"><h3 class="masvideos-loop-tv-show__title  tv-show__title">Better Call Saul</h3></a>
																																					<div class="tv-show__episode">Newest Episode: <a href="/react2/episode/rebecca/" class="tv-show__episode--link">S01E05</a>
																																					</div>
																																				</div> 
																																				<div class="tv-show__short-description">
																																					<div>
																																						<p>The trials and tribulations of criminal lawyer, Jimmy McGill, in the time leading up to establishing his strip-mall law office in Albuquerque, New Mexico.</p> 
																																					</div>
																																				</div>
																																				<div class="tv-show__actions"><a href="/react2/tv_show/better-call-saul/" class="tv-show-actions--link_watch">Watch Now</a> 
																																					<div class="tv-show-actions--link_add-to-playlist dropdown">
																																						<a class="dropdown-toggle" href="/react2/tv_show/better-call-saul/" data-toggle="dropdown">+ Playlist</a>
																																						<div class="dropdown-menu">
																																							<a class="login-link" href="/login?tv-show-playlists/">Sign in to add this tv show to a playlist.</a> 
																																						</div>
																																					</div>
																																				</div>
																																			</div>
																																			<div class="tv-show__review-info"> <a href="/react2/tv_show/better-call-saul/#reviews" class="avg-rating">
																																				<span class="rating-with-count">
																																					<svg class="vodi-svg" width="40px" height="39px" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 39"><title>play</title><path fill-rule="evenodd" d="M19.633,-0.000 C21.509,0.035 21.530,1.174 22.167,2.414 C23.329,4.679 24.406,7.067 25.572,9.338 C25.853,9.886 26.431,11.640 26.918,11.834 C27.486,12.203 29.345,12.109 30.165,12.316 C32.170,12.825 34.489,12.860 36.500,13.364 C37.516,13.618 38.689,13.413 39.430,13.927 C39.689,14.107 39.770,14.504 39.984,14.732 C40.047,16.499 39.096,16.843 38.163,17.792 C36.473,19.509 34.784,21.227 33.095,22.944 C32.585,23.462 31.092,24.543 31.036,25.359 C31.423,25.951 31.307,27.455 31.511,28.258 C32.138,30.727 32.213,33.522 32.857,35.987 C33.142,37.078 33.016,38.241 32.303,38.724 C31.108,39.533 29.632,38.193 28.819,37.758 C26.695,36.623 24.601,35.624 22.483,34.457 C21.979,34.179 20.607,33.178 20.108,33.088 C19.748,33.023 18.163,34.107 17.812,34.296 C15.557,35.505 13.340,36.640 11.080,37.839 C10.548,38.120 9.180,39.226 8.309,38.966 C6.955,38.558 6.874,36.993 7.280,35.423 C7.716,33.733 7.697,31.880 8.151,30.109 C8.527,28.642 8.907,26.529 9.022,24.957 C8.092,24.344 7.202,23.107 6.408,22.300 C4.760,20.625 3.059,18.990 1.340,17.389 C0.646,16.742 -0.578,15.515 0.311,14.249 C0.915,13.388 2.364,13.656 3.557,13.364 C6.678,12.599 10.114,12.468 13.298,11.834 C14.186,9.747 15.306,7.711 16.307,5.716 C16.954,4.426 17.496,3.163 18.128,1.931 C18.334,1.531 18.358,1.093 18.603,0.724 C18.845,0.362 19.299,0.273 19.633,-0.000 Z" /></svg> <span class="avg-rating-number"> 6.0</span>
																																				</span>
																																				<span class="rating-number-with-text">
																																					<span class="avg-rating-number"> 6.0</span>
																																					<span class="avg-rating-text">
																																						<span>1</span> Vote </span>
																																					</span>
																																				</a>
																																				<div class="viewers-count">
																																				</div>
																																			</div>
																																		</div> 
																																		<div class="tv-show__hover-area">
																																			<div class="tv-show__hover-area--inner">
																																				<div class="tv-show__hover-area--poster">
																																					<div class="tv-show__poster"><a href="/react2/tv_show/better-call-saul/" class="masvideos-LoopTvShow-link masvideos-loop-tv-show__link tv-show__link">
																																						<img width="970" height="550" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/33-experience.jpg" class="tv-show__poster--image tv_show__poster--image" alt="" srcset="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/33-experience.jpg 970w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/33-experience-270x153.jpg 270w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/33-experience-220x125.jpg 220w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/33-experience-324x184.jpg 324w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/33-experience-272x155.jpg 272w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/33-experience-214x122.jpg 214w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/33-experience-244x138.jpg 244w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/33-experience-339x192.jpg 339w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/33-experience-327x185.jpg 327w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/33-experience-300x170.jpg 300w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/33-experience-768x435.jpg 768w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/33-experience-120x67.jpg 120w" sizes="(max-width: 970px) 100vw, 970px" /></a>
																																					</div>
																																					<div class="tv-show__info--head">
																																						<div class="tv-show__meta"><span class="tv-show__meta--genre"><a href="https://demo3.madrasthemes.com/vodi-demos/main/tv-show-genre/crime/" rel="tag">Crime</a>, <a href="https://demo3.madrasthemes.com/vodi-demos/main/tv-show-genre/drama/" rel="tag">Drama</a></span><span class="tv-show__meta--release-year">2015 &#8211; 2016</span>
																																						</div><a href="/react2/tv_show/better-call-saul/" class="masvideos-LoopTvShow-link masvideos-loop-tv-show__link tv-show__link"><h3 class="masvideos-loop-tv-show__title  tv-show__title">Better Call Saul</h3></a>
																																					</div>
																																				</div>
																																				<div class="tv-show__hover-area--body">
																																					<div class="tv-show__season-info">
																																						<div class="tv-show__seasons">Seasons #: <a href="/react2/tv_show/better-call-saul/" class="tv-show__episode--link">Season 1</a><a href="/react2/tv_show/better-call-saul/" class="tv-show__episode--link">Season 2</a>
																																						</div>
																																						<div class="tv-show__episode">Newest Episode: <a href="/react2/episode/rebecca/" class="tv-show__episode--link">S01E05</a>
																																						</div>
																																					</div>
																																					<div class="tv-show__review-info"> 
																																						<div class="tv-show__avg-rating">
																																							<a href="/react2/tv_show/better-call-saul/#reviews" class="avg-rating">
																																								<div class="avg-rating__inner">
																																									<span class="avg-rating__number"> 6.0</span>
																																									<span class="avg-rating__text">
																																										<span>1</span> Vote 
																																									</span>
																																								</div>
																																							</a>
																																						</div>
																																						<div class="viewers-count">
																																						</div>
																																					</div>
																																					<div class="tv-show__actions">
																																						<a href="/react2/tv_show/better-call-saul/" class="tv-show-actions--link_watch">Watch Now</a> 
																																						<div class="tv-show-actions--link_add-to-playlist dropdown">
																																							<a class="dropdown-toggle" href="/react2/tv_show/better-call-saul/" data-toggle="dropdown">+ Playlist</a>
																																							<div class="dropdown-menu">
																																								<a class="login-link" href="/login?tv-show-playlists/">Sign in to add this tv show to a playlist.</a> 
																																							</div>
																																						</div>
																																					</div>
																																				</div> 
																																			</div>
																																		</div>
																																	</div>
																																</div>
																															</div>
																														</div> 
																													</div>
																												</div>
																											</section>
																										</div>
																									</article>
																								</main>
																							</div> 
																						</div>
																					</div>
																				</div>
																				@endsection
																				@section('scripts')

																				<script type="text/javascript">

																					fetch('https://khojapp.com/api/react/movies?limit=12')
																					.then(function (data) {
																						return data.text();
																					})
																					.then(function (html) {
																						html = JSON.parse(html)
																						htmlcontent = '<header class="home-section__header"><h2 class="home-section__title">Popular Movies <br> to Watch Now</h2><p class="home-section__subtitle">Most watched movies by days</p><div class="home-section__action"><a href="#" class="home-section__action-link">View all</a></div></header>';
																							console.log(html)
																							// console.log(html.contents.data)
																							// html = html.contents
																							if (html){
																								elementdata = html;
																								elementdata.forEach(function (item){
																									htmlcontent += '<div class="post-2930 movie type-movie status-publish has-post-thumbnail hentry movie_genre-action"><div class="movie__poster"><a href="/react2/movie/'+item.title+'/'+item.content_id+'" class="masvideos-LoopMovie-link masvideos-loop-movie__link movie__link"><img width="300" height="450" src="'+item.poster+'" class="movie__poster--image" alt="" srcset="'+item.poster+'" sizes="(max-width: 300px) 100vw, 300px" /></a></div><div class="movie__body">	<div class="movie__info">		<div class="movie__info--head"><div class="movie__meta"><span class="movie__meta--release-year">'+item.release_year+'</span><span class="movie__meta--genre"><a href="/react2/genre/movieaction/" rel="tag">Action</a></span></div><a href="/react2/movie/'+item.title+'/" class="masvideos-LoopMovie-link masvideos-loop-movie__link movie__link"><h3 class="masvideos-loop-movie__title  movie__title">'+item.title+'</h3></a></div><div class="movie__short-description"><div><p>'+item.short_description+'</p> </div> </div><div class="movie__actions"><a href="/react2/movie/'+item.title+'/'+item.content_id+'" class="movie-actions--link_watch">Watch Now</a><div class="movie-actions--link_add-to-playlist dropdown"><a class="dropdown-toggle" href="/react2/movie/delta-bravo/" data-toggle="dropdown">+ Playlist</a><div class="dropdown-menu">	<a class="login-link" href="/login?movie-playlists/">Sign in to add this movie to a playlist.</a> </div></div></div></div><div class="movie__review-info"> <a href="/react2/movie/delta-bravo/#reviews" class="avg-rating"><span class="rating-with-count"><svg class="vodi-svg" width="40px" height="39px" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 39"><title>play</title><path fill-rule="evenodd" d="M19.633,-0.000 C21.509,0.035 21.530,1.174 22.167,2.414 C23.329,4.679 24.406,7.067 25.572,9.338 C25.853,9.886 26.431,11.640 26.918,11.834 C27.486,12.203 29.345,12.109 30.165,12.316 C32.170,12.825 34.489,12.860 36.500,13.364 C37.516,13.618 38.689,13.413 39.430,13.927 C39.689,14.107 39.770,14.504 39.984,14.732 C40.047,16.499 39.096,16.843 38.163,17.792 C36.473,19.509 34.784,21.227 33.095,22.944 C32.585,23.462 31.092,24.543 31.036,25.359 C31.423,25.951 31.307,27.455 31.511,28.258 C32.138,30.727 32.213,33.522 32.857,35.987 C33.142,37.078 33.016,38.241 32.303,38.724 C31.108,39.533 29.632,38.193 28.819,37.758 C26.695,36.623 24.601,35.624 22.483,34.457 C21.979,34.179 20.607,33.178 20.108,33.088 C19.748,33.023 18.163,34.107 17.812,34.296 C15.557,35.505 13.340,36.640 11.080,37.839 C10.548,38.120 9.180,39.226 8.309,38.966 C6.955,38.558 6.874,36.993 7.280,35.423 C7.716,33.733 7.697,31.880 8.151,30.109 C8.527,28.642 8.907,26.529 9.022,24.957 C8.092,24.344 7.202,23.107 6.408,22.300 C4.760,20.625 3.059,18.990 1.340,17.389 C0.646,16.742 -0.578,15.515 0.311,14.249 C0.915,13.388 2.364,13.656 3.557,13.364 C6.678,12.599 10.114,12.468 13.298,11.834 C14.186,9.747 15.306,7.711 16.307,5.716 C16.954,4.426 17.496,3.163 18.128,1.931 C18.334,1.531 18.358,1.093 18.603,0.724 C18.845,0.362 19.299,0.273 19.633,-0.000 Z" /></svg> <span class="avg-rating-number"> 8.0</span></span><span class="rating-number-with-text"><span class="avg-rating-number"> 8.0</span><span class="avg-rating-text"><span>1</span> Vote </span></span></a><div class="viewers-count"></div></div></div></div>';

																								})
																							}




																							document.getElementById('moviessection').innerHTML = htmlcontent;
																							var scripts = document.getElementById("moviessection").querySelectorAll("script");
																							for (var i = 0; i < scripts.length; i++) {
																								if (scripts[i].innerText) {
																									eval(scripts[i].innerText);
																								} else {
																									fetch(scripts[i].src).then(function (data) {
																										data.text().then(function (r) {
																											eval(r);
																										})
																									});

																								}
    // To not repeat the element
    scripts[i].parentNode.removeChild(scripts[i]);
}
});

fetch('https://khojapp.com/api/trending?limit=12')
																					.then(function (data) {
																						return data.text();
																					})
																					.then(function (html) {
																						html = JSON.parse(html)
																						htmlcontent = '<header class="home-section__header section-movies-carousel-aside-header__header"><h2 class="home-section__title">Romantic for Valentines Day</h2><div class="section-movies-carousel-aside-header__custom-arrows"></div><div class="home-section__action"><a href="#" class="home-section__action-link">View All</a></div></header>';
																						htmlcontent += '<div class="section-movies-carousel__carousel"><div class="movies-carousel__inner" data-ride="vodi-slick-carousel" data-wrap=".movies__inner" data-slick="{&quot;slidesToShow&quot;:6,&quot;slidesToScroll&quot;:1,&quot;dots&quot;:false,&quot;arrows&quot;:true,&quot;autoplay&quot;:false,&quot;infinite&quot;:false,&quot;responsive&quot;:[{&quot;breakpoint&quot;:768,&quot;settings&quot;:{&quot;slidesToShow&quot;:2,&quot;slidesToScroll&quot;:1}},{&quot;breakpoint&quot;:992,&quot;settings&quot;:{&quot;slidesToShow&quot;:3,&quot;slidesToScroll&quot;:1}},{&quot;breakpoint&quot;:1200,&quot;settings&quot;:{&quot;slidesToShow&quot;:4,&quot;slidesToScroll&quot;:1}}],&quot;appendArrows&quot;:&quot;#section-movies-carousel-aside-header-5f87f9fc21c6a .section-movies-carousel-aside-header__custom-arrows&quot;}"><div class="masvideos masvideos-movies "><div class="movies columns-6"><div class="movies__inner">';
																							// console.log(html['contents'])
																							console.log(html.contents.data)
																							html = html.contents
																							if (html.data){
																								elementdata = html.data;
																								elementdata.forEach(function (item){
																									htmlcontent += '<div class="post-2930 movie type-movie status-publish has-post-thumbnail hentry movie_genre-action"><div class="movie__poster"><a href="/react2/movie/'+item.title+'/'+item.content_id+'" class="masvideos-LoopMovie-link masvideos-loop-movie__link movie__link"><img width="300" height="450" src="https://khojapp.com/poster/'+item.poster+'" class="movie__poster--image" alt="" sizes="(max-width: 300px) 100vw, 300px" /></a></div><div class="movie__body"><div class="movie__info"><div class="movie__info--head"><div class="movie__meta"><span class="movie__meta--release-year">'+item.cinema_release_date.split('-')[0]+'</span><span class="movie__meta--genre"><a href="/react2/genre/movieaction/" rel="tag">Action</a></span></div><a href="/react2/movie/delta-bravo/" class="masvideos-LoopMovie-link masvideos-loop-movie__link movie__link"><h3 class="masvideos-loop-movie__title  movie__title">'+item.title+'</h3></a></div><div class="movie__short-description"><div><p>'+item.short_description+'</p> </div></div><div class="movie__actions"><a href="/react2/movie/delta-bravo/" class="movie-actions--link_watch">Watch Now</a><div class="movie-actions--link_add-to-playlist dropdown"><a class="dropdown-toggle" href="/react2/movie/delta-bravo/" data-toggle="dropdown">+ Playlist</a><div class="dropdown-menu"><a class="login-link" href="/login?movie-playlists/">Sign in to add this movie to a playlist.</a> </div></div></div>=</div><div class="movie__review-info"> <a href="/react2/movie/delta-bravo/#reviews" class="avg-rating"><span class="rating-with-count"><svg class="vodi-svg" width="40px" height="39px" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 39"><title>play</title><path fill-rule="evenodd" d="M19.633,-0.000 C21.509,0.035 21.530,1.174 22.167,2.414 C23.329,4.679 24.406,7.067 25.572,9.338 C25.853,9.886 26.431,11.640 26.918,11.834 C27.486,12.203 29.345,12.109 30.165,12.316 C32.170,12.825 34.489,12.860 36.500,13.364 C37.516,13.618 38.689,13.413 39.430,13.927 C39.689,14.107 39.770,14.504 39.984,14.732 C40.047,16.499 39.096,16.843 38.163,17.792 C36.473,19.509 34.784,21.227 33.095,22.944 C32.585,23.462 31.092,24.543 31.036,25.359 C31.423,25.951 31.307,27.455 31.511,28.258 C32.138,30.727 32.213,33.522 32.857,35.987 C33.142,37.078 33.016,38.241 32.303,38.724 C31.108,39.533 29.632,38.193 28.819,37.758 C26.695,36.623 24.601,35.624 22.483,34.457 C21.979,34.179 20.607,33.178 20.108,33.088 C19.748,33.023 18.163,34.107 17.812,34.296 C15.557,35.505 13.340,36.640 11.080,37.839 C10.548,38.120 9.180,39.226 8.309,38.966 C6.955,38.558 6.874,36.993 7.280,35.423 C7.716,33.733 7.697,31.880 8.151,30.109 C8.527,28.642 8.907,26.529 9.022,24.957 C8.092,24.344 7.202,23.107 6.408,22.300 C4.760,20.625 3.059,18.990 1.340,17.389 C0.646,16.742 -0.578,15.515 0.311,14.249 C0.915,13.388 2.364,13.656 3.557,13.364 C6.678,12.599 10.114,12.468 13.298,11.834 C14.186,9.747 15.306,7.711 16.307,5.716 C16.954,4.426 17.496,3.163 18.128,1.931 C18.334,1.531 18.358,1.093 18.603,0.724 C18.845,0.362 19.299,0.273 19.633,-0.000 Z" /></svg> <span class="avg-rating-number"> 8.0</span></span><span class="rating-number-with-text"><span class="avg-rating-number"> 8.0</span><span class="avg-rating-text"><span>1</span> Vote </span></span></a><div class="viewers-count"></div></div></div></div>';

																								})
																							}




																							document.getElementById('specialevent').innerHTML = htmlcontent;
																							var scripts = document.getElementById("specialevent").querySelectorAll("script");
																							for (var i = 0; i < scripts.length; i++) {
																								if (scripts[i].innerText) {
																									eval(scripts[i].innerText);
																								} else {
																									fetch(scripts[i].src).then(function (data) {
																										data.text().then(function (r) {
																											eval(r);
																										})
																									});

																								}
    // To not repeat the element
    scripts[i].parentNode.removeChild(scripts[i]);
}
});


</script>

@endsection