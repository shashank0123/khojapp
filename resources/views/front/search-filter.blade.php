<?php
$show_count =0;
?>
<div class="row" id="dynamic-search">
				@if (isset($contents))
				@foreach ($contents as $element)
				<?php
				$show_count++;
				?>
				<div class="col-sm-3 search-response">
					<div class="movie-item">
						<div class="mv-img">
							@if(isset($element->poster_path))
							<img src="https://image.tmdb.org/t/p/w300{{$element->poster_path}}" alt="" style="width: 240px; height: 340px">
							@else
							<img src="/images/default.png" alt="">
							@endif
						</div>
						<div class="hvr-inner">
							<a  href="/movie-detail/{{$element->id}}">
								<i class="fa fa-play" aria-hidden="true"></i> </a>
							</div>
							<div class="title-in">
								@if($type == 'tv')
								<h6><a href="/movie-detail/{{$element->id}}">{{$element->name}}</a></h6>
								@else
								<h6><a href="/movie-detail/{{$element->id}}">{{$element->title}}</a></h6>
								@endif
								<p><i class="ion-android-star"></i><span>{{$element->vote_average}}</span> /10</p>
							</div>
						</div>
					</div>
					@endforeach
					@endif

					@if($show_count == 0)
					<h2 style="text-align: center;color: #fff">No Result Folund For <i>"{{$keyword}}"</i>
						<br><br></h2>
					@endif

				</div>






				{{-- <hr class="hrstyle"> --}}

			</div>