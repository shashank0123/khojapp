    @if (isset($contents))
    <?php $space = 0; $rat=0;?>
    @foreach ($contents as $element)
    <?php $flag = 1; $space++;?>
    
    <div class="col-sm-3 col-lg-2 col-xs-6 search-response dynamic">

                   
      <a  href="/detail/{{$element->content_type}}/{{$element->full_path}}">
       
        

          <div class="movie-item">
           <div class="mv-img">
            @if(isset($element->poster) && strpos($element->poster, 'poster/') == false)
            <img src="{{$element->poster}}" alt="Bootstrap Touch Slider"  class="slide-image web-image"/>
            @else
            <img src="/images/default.png" class="web-image" alt=""/>
            @endif
          </div>
          <div class="hvr-inner">
           {{-- <i class="fa fa-play" aria-hidden="true"></i>  --}}
         </div>
         <div class="title-in">
          
          <p>
           <i class="ion-android-star"></i>
           <span> @if($element->tmdb_score){{$element->tmdb_score}}@else{{'0'}}@endif</span>/10
         </p>
       </div>
       <div class="imdb">
        <div class="row">
        
          <h6><a href="/detail/{{$element->content_type}}/{{$element->full_path}}"> 
            <?php
            if(isset($element->title))
          echo substr($element->title,0,50); 
        elseif(isset($element->original_title))
          echo substr($element->original_title,0,50); 
          
          ?></a></h6>
         
        </div>
        <div class="row rate-row">
        <div class="col-sm-6 col-xs-6"><b>IMDB</b>: <span style="color: yellow">@if(isset($element->imdb_score)){{$element->imdb_score}}@else{{'N/A'}}@endif</span></div>
        <div class="col-sm-3 col-xs-3">RT</div>
        <div class="col-sm-3 col-xs-3">UA</div>
      </div>
      </div>
    </div>
  </a>
</div>
<?php $rat++; ?>
@endforeach

@endif