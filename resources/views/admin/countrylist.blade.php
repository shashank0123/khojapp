@extends('layouts.admin')


@section('content')
<h4 class="header-title m-t-0 m-b-30">Default Example</h4>
<h4 class="header-title m-t-0 m-b-30">Country List</h4>
@if(Session::has('message'))
   <div class="alert alert-success">{{ Session::get('message') }}</div>
@endif
<table id="datatable" class="table table-striped table-bordered">
    <thead>
        <tr>
            <th>id</th>
            <th>Country</th>
            
            <th>Action</th>
        </tr>
    </thead>

    <tbody>
    	@foreach ($countries as $country)
        <tr>
            <td>{{ $country->id }}</td>
            <td>{{ $country->country }}</td>
            <td>
                {{ link_to_route('country.edit','Edit',[$country->id], ['class'=> 'btn btn-primary btn-xs']) }}

                    {!! Form::open(['method'=>'delete','route'=>['country.destroy', $country->id]]) !!}

                {!! Form::submit('Delete',['class'=>'btn btn-danger btn-xs' ])!!}
           </td>
        </tr>
        @endforeach
       
    </tbody>
</table>                    
@endsection

