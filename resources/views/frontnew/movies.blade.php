
@extends('layouts.khoj_new')

<?php if (isset($_GET['page_id'])){
	$next = $_GET['page_id']+1;
	$prev = $_GET['page_id']-1;
}
else {
	$prev = '0';
	$next = '1';
}

$featureds = session()->get('featureds');
?>

@section('content')
<div class="movie-content">

	@if (isset($contents->results))
					@foreach ($contents->results as $element)
	<div class="col-sm-3 search-response">
				<a  href="/movie/{{$element->id}}">
		<div class="movie-item">
			<div class="mv-img">
				@if(isset($element->poster_path))
				<img src="https://image.tmdb.org/t/p/w300{{$element->poster_path}}" alt="Bootstrap Touch Slider"  class="slide-image" style="width: 100%; height: 250px"/>
         @else
				<img src="/asset/images/uploads/mv1.jpg" alt="" style="width: 100%; height: 250px" />
				@endif
			</div>
			<div class="hvr-inner">
					{{-- <i class="fa fa-play" aria-hidden="true"></i>  --}}
			</div>
			<div class="title-in">
				<h6><a href="/movie/{{$element->id}}"> {{$element->title}} </a></h6>
				<p>
					<i class="ion-android-star"></i>
					<span> {{$element->vote_average}}</span>/10
				</p>
			</div>
			<div class="imdb">
				<div class="col-sm-4 col-xs-4">IMDB</div>
				<div class="col-sm-4 col-xs-4">RT</div>
				<div class="col-sm-4 col-xs-4">UA</div>
			</div>
		</div>
				</a>
	</div>
	@endforeach
	@endif



@endsection 