<!DOCTYPE html>
<html lang="en" class="fsvs">

<head>
  <meta charset="UTF-8">
  <title>Skrn</title>
  
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/style.css">
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome.min.css">
	<link rel="stylesheet" href="slick/slick.css">
	<link rel="stylesheet" href="slick/slick-theme.css">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  
     <link rel="stylesheet" href="css/test.css">

  
</head>

<body>

<aside id="dashboard_menu" class="menu-d">
	<ul class="menu">
		<li>
		 	<a href="#" class="logo"><img src="img/SKRN logo.png" width="80" height="20" alt=""></a>			
		</li>
		<li>
		 	<a href="#" class=""><span class="menu-icon"><i class="fa fa-home"></i></span><br><p class="menu-text">TV Series</p></a>			
		</li>		
		<li><a href="#" class=""><span class="menu-icon"><i class="fa fa-film"></i></span><br><p class="menu-text">Movies</p></a></li>
		<li><a href="#" class=""><span class="menu-icon"><i class="fa fa-music"></i></span><br><p class="menu-text">Playlist</p></a></li>
		<li><a href="#" class=""><span class="menu-icon"><i class="fa fa-ticket"></i></span><br><p class="menu-text">New Arrival</p></a></li>
		<li><a href="#" class=""><span class="menu-icon"><i class="fa fa-bell"></i></span><br><p class="menu-text">Coming Soon</p></a></li>
		
	</ul>
</aside>	
<nav class="navbar navbar-inverse navbar-fixed-top">
		<ul class="nav navbar-nav navbar-right pull-right" style="margin-top: 15px;">
			<li>
				<a href="#" class="n_button">
				    <i class="fa fa-bell"></i>
				    <span class="button__badge">2</span>
				  </a>
			</li>
			<li class="pull-left dropdown">
				<a href="#" data-toggle="dropdown">Name &nbsp;
				<b class="caret"></b></a>
				<ul class="dropdown-menu">
					<li><a href="includes/logout.php">Log Out</a></li>
				</ul>
			</li>
		</ul>
</nav>
  <main id="fsvs-body" class="" style="width: 92%;">
	
	
	<section class="page">
			<div class="container">
				<div class="new_release col-md-12">
					<div class="col-md-6">
						<p>NEW RELEASES</p>
						<h1>Seven Days in Baaghi2</h1>
					</div>
				</div>
			</div>
	</section>
	
	<section class="page">
		<div class="set">
				<div class="row">
					<h2>Icon Set Name</h2>
					<p>A short description about out this set</p>
				
					
				</div>
			</div>
	</section>
	
	
	<section class="page">
	<div class="set special">
				<div class="row">
					
				</div>
			</div>
	</section>
	
	
	
	<section class="page">04</section>
	
	<section class="page">05</section>
	
</main>
  <script src='js/jquery.js'></script>
  <script type="text/javascript" src="js/bootstrap.min.js" ></script> 
	<script src="js/ripples.min.js"></script>
	<!-- <script src="js/material.min.js"></script> -->
    <script src="slick/slick.min.js"></script>
    <link href='https://fonts.googleapis.com/css?family=Ubuntu:400,700' rel='stylesheet' type='text/css'>
<script src='js/fsvc.js'></script>

  

    <script  src="js/index.js"></script>




</body>

</html>
