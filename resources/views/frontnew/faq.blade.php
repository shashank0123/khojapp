@extends('layouts.khoj_new')
@php $miny=1900; $maxy=date('Y'); $minr=0; $maxr=10; @endphp
@section('content')
<section id="page-bg-color">
	<div class="container">
		<div class="row">
			<div class="faq-sec1">
				<h2 class="head-title">FAQ</h2>
				<h4>FAQ Information</h4>
            <h4>Q: HOW IS YOUR CONTENT CREATED?</h4>
<p>We use an open database, themoviedb.com, as the main data source for our content (for trailers, synopsis, titles etc). To be able to provide up-to-date information on where movies and TV shows are available for viewing, we check the content of all the providers listed on Khoj daily and match this information with the correct movie/show.</p>

<h4>Q: I SEARCHED FOR A MOVIE/SHOW ON Khoj, BUT COULDN'T FIND IT. WHY?</h4>
<p>Either none of the providers we support has an offer for the movie/show currently, or it is not listed in our source database themoviedb.com.</p>

<h4>Q: IS NETFLIX'S CONTENT DIFFERENT IN EACH COUNTRY?</h4>
<p>Yes. We list Netflix content in all the countries where we are available. To see the Netflix content available in your country, make sure to choose the correct country on the Khoj startpage.</p>

<h4>Q: DO YOU KNOW WHEN A MOVIE/SHOW/SEASON BECOMES AVAILABLE?</h4>
<p>No. We at Khoj can't affect on what content the providers offer and when. We update the offers daily, so as soon as a movie/show becomes available, you will find the information on our page.</p>

<h4>Q: I HAD AN ISSUE WITH BUYING/RENTING/STREAMING THE MOVIE/SHOW ON THE PROVIDER'S PAGE. WHAT SHOULD I DO?</h4>
<p>Please contact the provider's customer support directly. We can't handle any issues outside our app/website.</p>

<h4>Q: THE PRICE YOU LIST FOR A MOVIE/SHOW/SEASON IS WRONG, E.G. IT'S MORE EXPENSIVE ON ITUNES. WHY?</h4>
<p>Please notice that there are usually different prices for HD and SD versions. "Best price" is usually for SD version, while many providers show the more expensive HD price by default. Make sure to use the correct filters to check prices.</p>

<h4>Q: A MOVIE/SHOW IS AVAILABLE ON "PROVIDER X", BUT YOU ARE MISSING THIS INFORMATION. WHY?</h4>
<p>If it's a provider already listed on Khoj, it's an error from our side and we are sorry about that. Please let us know the details and we will fix it as quickly as possible. If it's a provider we don't list currently, feel free to suggest this provider to us!</p>

<h4>Q: WHAT DOES IT MEAN WHEN A MOVIE/SHOW IS NOT AVAILABLE FOR STREAMING/BUYING/RENTING?</h4>
<p>It means (most likely) that none of the providers we support offers the title at the moment. We check the providers' pages daily for new content. If you know we are missing an availability information, please report it and we'll fix it!</p>

<h4>Q: I NOTICED A MOVIE/SHOW WASN'T AVAILABLE ON THE PROVIDER'S SERVICE, ALTHOUGH YOU SAID IT IS. WHY?</h4>
<p>We check our content's availability daily but sometimes due to technical issues we are not able to check if the offers are expired or not. Please let us know about the issue, and we will fix it quickly.</p>

<h4>Q: WHY WAS I REDIRECTED TO A DIFFERENT MOVIE/SHOW AFTER CLICKING A PROVIDER'S ICON?</h4>
<p>We most likely mixed up a wrong version or a similarly named movie/show, sorry about that. Let us know about it, and we will fix it asap!</p>

<h4>Q: I NOTICED THAT YOU DON'T LIST ALL POSSIBLE STREAMING/RENTING/BUYING PROVIDERS AVAILABLE IN MY COUNTRY. WHY?</h4>
<p>Our goal is to have all the providers that are important for our users and we choose the providers based on our users' request. Let us know about it, and we will fix it asap!</p>


			</div>
		</div>
	</div>
</section>


<script src="//code.jquery.com/jquery.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
@endsection