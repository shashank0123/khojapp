<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Setting;

class SettingController extends Controller
{
    public function getCompanyDetail(){
    	$data = array();

    	$name = Setting::where('key','website_name')->first();
    	$logo = Setting::where('key','website_logo')->first();
    	$area = Setting::where('key','area')->first();
    	$city = Setting::where('key','city')->first();
    	$state = Setting::where('key','state')->first();
    	$pincode = Setting::where('key','pincode')->first();
    	$country = Setting::where('key','country')->first();
    	$web_url = Setting::where('key','website_url')->first();
    	$contact = Setting::where('key','contact_number')->first();
    	$subsc_mail = Setting::where('key','subscription_email')->first();
    	$contact_mail = Setting::where('key','contact_us_email')->first();

    	$data['name'] = $name->value;
    	$data['logo'] = $logo->value;
    	$data['area'] = $area->value;
    	$data['city'] = $city->value;
    	$data['state'] = $state->value;
    	$data['pincode'] = $pincode->value;
    	$data['country'] = $country->value;
    	$data['web_url'] = $web_url->value;
    	$data['contact'] = $contact->value;
    	$data['subsc_mail'] = $subsc_mail->value;
    	$data['contact_mail'] = $contact_mail->value;

    	return view('admin.settings.website',compact('data'));
    }


     public function storeCompanyDetail(Request $request){
    	$data = $request->all();
    	$logo = "";

    		  if($request->hasFile('logo')) 
  {
   $file=$request->logo;
   $file->move(public_path(). '/asset/images/', time().$file->getClientOriginalName());   
   // $file->move(public_path(). '/thumbnail/', time().$file->getClientOriginalName());   
   $logo = time().$file->getClientOriginalName();
 }


 	if(!empty($data['logo'])){
 		
    	$web_logo = Setting::where('key','website_logo')->update(array('value'=>$logo));
 	}
    	$name = Setting::where('key','website_name')->update(array('value'=>$data['name']));
    	$area = Setting::where('key','area')->update(array('value'=>$data['area']));
    	$city = Setting::where('key','city')->update(array('value'=>$data['city']));
    	$state = Setting::where('key','state')->update(array('value'=>$data['state']));
    	$pincode = Setting::where('key','pincode')->update(array('value'=>$data['pincode']));
    	$country = Setting::where('key','country')->update(array('value'=>$data['country']));
    	$web_url = Setting::where('key','website_url')->update(array('value'=>$data['web_url']));
    	$contact = Setting::where('key','contact_number')->update(array('value'=>$data['contact']));
    	$subsc_mail = Setting::where('key','subscription_email')->update(array('value'=>$data['subsc_mail']));
    	$contact_mail = Setting::where('key','contact_us_email')->update(array('value'=>$data['contact_mail']));

    	return redirect()->back()->with('success','Data successfully inserted'); 
}


    
    public function getFacebookDetail(){
    	$data = array();
    	$client_id = Setting::where('key','facebook_client_id')->first();
    	$client_secret = Setting::where('key','facebook_client_secret')->first();
    	$callback_url = Setting::where('key','facebook_callback_url')->first();
    	$data['client_id'] = $client_id->value;
    	$data['client_secret'] = $client_secret->value;
    	$data['callback_url'] = $callback_url->value;
    	return view('admin.settings.facebook',compact('data'));
    }

    public function storeFacebookDetail(Request $request){
    	$data = $request->all();

    	$client_id = Setting::where('key','facebook_client_id')->update(array('value' => $data['client_id']));
    	$client_secret = Setting::where('key','facebook_client_secret')->update(array('value' => $data['client_secret']));
    	$callback_url = Setting::where('key','facebook_callback_url')->update(array('value' => $data['callback_url']));

    	return redirect()->back()->with('success','Data successfully inserted'); 
}

    public function getGoogleDetail(){
    	$data = array();
    	$client_id = Setting::where('key','google_client_id')->first();
    	$client_secret = Setting::where('key','google_client_secret')->first();
    	$callback_url = Setting::where('key','google_callback_url')->first();
    	$data['client_id'] = $client_id->value;
    	$data['client_secret'] = $client_secret->value;
    	$data['callback_url'] = $callback_url->value;
    	return view('admin.settings.google',compact('data'));
    }

    public function storeGoogleDetail(Request $request){
    	$data = $request->all();

    	$client_id = Setting::where('key','google_client_id')->update(array('value' => $data['client_id']));
    	$client_secret = Setting::where('key','google_client_secret')->update(array('value' => $data['client_secret']));
    	$callback_url = Setting::where('key','google_callback_url')->update(array('value' => $data['callback_url']));

    	return redirect()->back()->with('success','Data successfully inserted'); 
}
}
