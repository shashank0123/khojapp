<?php

namespace App\Http\Controllers;

set_time_limit(1000);
ini_set('max_execution_time', 120000);
ini_set('memory_limit', '1012M');

use Illuminate\Http\Request;
use DB;
use App\Content;
use App\ContentCasting;
use App\ContentGenre;
use App\ContentClip;



class JustWatchCrawlerController extends Controller
{
    public function crawl(Request $request)
    {
        $url = "https://apis.justwatch.com/content/titles/en_IN/popular?body=%7B%22age_certifications%22:[],%22content_types%22:[],%22genres%22:[],%22languages%22:null,%22min_price%22:null,%22max_price%22:null,%22monetization_types%22:[%22ads%22,%22buy%22,%22flatrate%22,%22free%22,%22rent%22],%22presentation_types%22:[],%22providers%22:[],%22release_year_from%22:null,%22release_year_until%22:null,%22scoring_filter_types%22:null,%22timeline_type%22:null,%22sort_by%22:null,%22sort_asc%22:null,%22page%22:2,%22page_size%22:1000%7D";

        $c = curl_init();
        curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($c, CURLOPT_URL, $url);
        curl_setopt($c, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($c, CURLOPT_SSL_VERIFYPEER, 0);

        $contents = curl_exec($c);

        $contents = json_decode($contents);

        $data = $contents->items;
        foreach ($data as $key => $value) {

            $db_content = DB::table('contents')->where('content_source_id', $value->id)->first();
            if (isset($db_content)) {
                $content_id = $db_content->content_id;
            } else {
                $db_data = DB::table('contents')->insert(
                    array(
                        'title' => $value->title,
                        'content_type' => 'movie',
                        'poster' => $value->poster ?? '',
                        'content_source' => 'justwatch',
                        'content_source_id' => $value->id
                    )
                );
            }
        }

        // dd($contents);
    }

    public function slugname(Request $request)
    {
        $moviename = Content::where('full_path', 'LIKE', "/in/%")->first();
        // $name = strtolower($moviename->title)
        if ($moviename) {
            $full_path = explode('in/', $moviename->full_path)[1];
            echo $moviename->content_id . " ";
            // $moviename->update();
            $moviename1 = DB::table('contents')->where('content_id', $moviename->content_id)->update(['full_path' => $full_path]);
        }
    }


    public function getdatasorted(Request $request)
    {


        $movie = Content::where('execute_status', '!=', '1')->where('content_type', 'shows')->orderBy('click_count', 'DESC')->first();
        if ($movie->content_source_id == null || $movie->content_source_id < 1) {
            $deleted = Content::where('content_id', $movie->content_id)->delete();
            echo $movie->content_id . " deleted ";
            die;
        }

        echo $movie->content_id . " ";
        if ($movie->content_source_id) {


            // =================================justwatch==============================

            $justwatch_urlnew = "https://apis.justwatch.com/content/titles/" . $movie->content_type . "/" . $movie->content_source_id . "/locale/en_IN?language=en";
            // $justwatch_urlnew = "https://apis.justwatch.com/content/titles/show/11188/locale/en_IN";

            $genrerequest = curl_init();
            curl_setopt($genrerequest, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($genrerequest, CURLOPT_URL, $justwatch_urlnew);
            curl_setopt($genrerequest, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($genrerequest, CURLOPT_SSL_VERIFYPEER, false);
            $contents = curl_exec($genrerequest);

            $contents = json_decode($contents);
            // var_dump($contents);
            $status1 = 1;
            $newdata = array();

            if (isset($contents->short_description))
                $newdata['short_description'] = $contents->short_description;

            if (isset($contents->original_release_year))
                $newdata['original_release_year'] = $contents->original_release_year;
            if (isset($contents->localized_release_date))
                $newdata['localized_release_date'] = $contents->localized_release_date;

            if (isset($contents->first_air_date)) {
                $newdata['localized_release_date'] = $contents->first_air_date;
                $newdata['cinema_release_date'] = $contents->first_air_date;
            }

            if (isset($contents->cinema_release_date))
                $newdata['cinema_release_date'] = $contents->cinema_release_date;

            if (isset($contents->original_title))
                $newdata['original_title'] = $contents->original_title;

            if (isset($contents->title))
                $newdata['title'] = $contents->title;

            if (isset($contents->tmdb_popularity))
                $newdata['tmdb_popularity'] = $contents->tmdb_popularity;

            if (isset($contents->runtime))
                $newdata['runtime'] = $contents->runtime;

            // if ($movie->poster != ''){
            // Remote image URL
            if (isset($contents->poster)) {
                $key1 = explode('/', $contents->poster)[2];
                $url1 = 'https://images.justwatch.com/poster/' . $key1 . '/s592';
                $url2 = 'https://images.justwatch.com/poster/' . $key1 . '/s166';
                $base_path = public_path();
                $keyname = $key1 . time() . ".jpg";
                $img1 = $base_path . '/poster/' . $keyname;
                $img2 = $base_path . '/thumbnail/' . $keyname;
                $newdata['execute_status'] = 1;
                $newdata['poster'] = $keyname;
                // echo $movie->content_id." ";
                $newmovie = DB::table('contents')->where('content_id', $movie->content_id)->update($newdata);

                try {
                    file_put_contents($img1, file_get_contents($url1));
                } catch (Exception $ex) {
                    //Process the exception
                }
                try {
                    file_put_contents($img2, file_get_contents($url2));
                } catch (Exception $ex) {
                    //Process the exception
                }
            }

            // $newmovie = DB::table('contents')->where('content_id', $movie->content_id)->update($newdata);
            // Image path
            // Save image
            // }




            // =================================justwatch==============================


            if ($movie) {
                $count = $movie->click_count + 1;
                $change_movie = DB::table('contents')
                    ->where('content_id', $movie->content_id)
                    ->update(array(
                        'click_count' => $count,
                    ));
            }

            $casts = DB::table('content_casting')
                ->where('content_id', $movie->content_id)
                ->get();

            if (count($casts) < 1 && isset($contents->credits)) {
                foreach ($contents->credits as $key => $value) {
                    $castingcheck = ContentCasting::where('person_id', $value->person_id)->where('content_id', $movie->content_id)->first();
                    if (!$castingcheck) {
                        $casting = array();
                        $casting['role'] = $value->role;
                        $casting['content_id'] = $movie->content_id;
                        if (isset($value->character_name))
                            $casting['character_name'] = $value->character_name;
                        if (isset($value->name))
                            $casting['name'] = $value->name;
                        $casting['person_id'] = $value->person_id;
                        $addcast = ContentCasting::insert($casting);
                    }
                }
            }
            if (isset($contents->clips)) {
                foreach ($contents->clips as $key => $value) {
                    $castingcheck = ContentClip::where('content_id', $movie->content_id)->first();
                    if (!$castingcheck) {
                        $genres = array();
                        $genres['content_id'] = $movie->content_id;
                        $genres['type'] = $value->type;
                        $genres['provider'] = $value->provider;
                        $genres['external_id'] = $value->external_id;
                        $genres['title'] = $value->name;
                        $addcast = ContentClip::insert($genres);
                    }
                }
            }



            $genres = DB::table('content_genres')
                ->leftJoin('genres', 'genres.genre_id', 'content_genres.genre_id')
                ->where('content_id', $movie->content_id)
                ->select('genres.genre_id', 'genres.title')
                ->distinct('genre_id')
                ->get();

            if (count($genres) < 1 && isset($contents->genre_ids)) {
                foreach ($contents->genre_ids as $key => $value) {
                    $castingcheck = ContentGenre::where('content_id', $movie->content_id)->first();
                    if (!$castingcheck) {
                        $genres = array();
                        $genres['content_id'] = $movie->content_id;

                        $genres['genre_id'] = $value;
                        $addcast = ContentGenre::insert($genres);
                    }
                }
            }
        } else {
            $newmovie = DB::table('contents')->where('content_id', $movie->content_id)->update(['execute_status' => '1']);
        }
    }


    public function getmoviedetails(Request $request)
    {
        $list = $db_content = DB::table('contents')->orderBy('click_count', 'DESC')->where('execute_status', '!=', '1')->get();
        foreach ($list as $key => $value) {
            $datacheck = DB::table('content_providers')->where('content_id', $value->content_id)->get();

            if (count($datacheck) < 1) {
                $details_url = "https://apis.justwatch.com/content/titles/movie/" . $value->content_source_id . "/locale/en_IN?language=en";

                $c = curl_init();
                curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($c, CURLOPT_URL, $details_url);
                curl_setopt($c, CURLOPT_SSL_VERIFYHOST, false);
                curl_setopt($c, CURLOPT_SSL_VERIFYPEER, false);
                $contents_gen = curl_exec($c);
                $contents_gen = json_decode($contents_gen);
                if (isset($contents_gen->offers)) {
                    $providerlist = $contents_gen->offers;
                    foreach ($providerlist as $key2 => $value2) {
                        // 		$provider_check = DB::table('content_providers')
                        // ->where('content_id',$value->content_id)
                        // ->get();

                        // if ($provider_check)
                        $db_data = DB::table('content_providers')->insert(
                            array(
                                'content_id' => $value->content_id,
                                'provider_id' => $value2->provider_id,
                                'video_quality' => $value2->presentation_type ?? '',
                                'monetization_type' => $value2->monetization_type ?? '',
                                'retail_price' => $value2->retail_price ?? '',
                                'web_url' => $value2->urls->standard_web ?? ""
                            )
                        );
                    }
                }
            }
        }
    }


    public function getmxplayerdata(Request $request)
    {
        $keyword = $request->keyword;
        $keyword = str_replace(' ', '%20', $keyword);
        $data = file_get_contents("https://api.mxplay.com/v1/web/search/result?query=" . $keyword . "&platform=com.mxplay.desktop&content-languages=hi,en");
        $data = json_decode($data);
        foreach ($data->sections as $key => $value) {
            if ($value->id == 'movie') {
                $dataadd = $this->addmxplayerdata($value, 'movie');
            } elseif ($value->id == 'shows') {
                $dataadd = $this->addmxplayerdata($value, 'shows');
            } else {
                continue;
            }
        }
        // dd($data);
    }


    public function addmxplayerdata($movies, $contenttype)
    {
        // dd($movies);
        if (count($movies->items) > 0) {
            foreach ($movies->items as $key1 => $value1) {
                // dd($value1);
                if (isset($value1->image)) {
                    foreach ($value1->image as $key => $image) {
                        if ($key == '2x3') {
                            $image = explode('/', $image);
                            $url1 = 'https://is-1.mxplay.com/media/images/' . $image[3] . '/2x3/2x/' . $image[4];
                            $base_path = public_path();
                            $keyname = time() . $image[4];
                            $img1 = $base_path . '/poster/' . $keyname;
                            $updateData['poster'] = $keyname;
                            // Save image
                            file_put_contents($img1, file_get_contents($url1));
                        }
                    }
                }
                $dbcheck = DB::table('contents')->where('title', $value1->title)->where('content_source', 'mxplayer')->where('content_type', $contenttype)->where('content_source_id', $value1->id)->first();
                if (!$dbcheck) {
                    $db_data1 = DB::table('contents')->insert(
                        array(
                            'title' => $value1->title,
                            'original_title' => $value1->title,
                            'content_type' => $contenttype,
                            'poster' => $updateData['poster'],
                            'content_source' => 'mxplayer',
                            'content_source_id' => $value1->id,
                            'full_path' => $value1->webUrl ?? "",
                            'tmdb_popularity' => 0 ?? '',
                            'original_release_year' => explode('-', $value1->releaseDate)[0] ?? '',
                            'cinema_release_date' => $value1->releaseDate ?? '',
                            'original_language' => $value1->language[0] ?? "",
                            'tmdb_score' => $tmdb_score ?? "",
                            'imdb_score' => $imdb_score ?? '',
                            'parent_id' => 0
                        )
                    );
                    $dbcheck = DB::table('contents')->where('title', $value1->title)->where('content_source', 'mxplayer')->where('content_type', $contenttype)->where('content_source_id', $value1->id)->first();
                    if ($dbcheck)
                        $db_data = DB::table('content_providers')->insert(
                            array(
                                'content_id' => $dbcheck->content_id,
                                'provider_id' => 700,
                                'video_quality' => 'SD',
                                'currency' => 'INR',
                                'monetization_type' => 'ads',
                                'retail_price' => '0',
                                'web_url' => 'https://www.mxplayer.in/'.$dbcheck->full_path
                            )
                        );
                }
                // dd($db_data1);
            }
        }
    }


    public function gettmdbdata(Request $request)
    {
        $keyword = $request->keyword;
        $keyword = str_replace(' ', '%20', $keyword);

        if (isset($request->year)){
            $url = "https://api.themoviedb.org/3/search/tv?api_key=f0dc06cd59a97bbddcebd05e902559e0&language=en-US&sort_by=popularity.desc&include_adult=false&query=$keyword&page=1&year=".$request->year;
        }
        else

        $url = "https://api.themoviedb.org/3/search/tv?api_key=f0dc06cd59a97bbddcebd05e902559e0&language=en-US&sort_by=popularity.desc&include_adult=false&query=$keyword&page=1";
        // $justwatch_urlnew = "https://apis.justwatch.com/content/titles/show/11188/locale/en_IN";
        // echo $url;
        $genrerequest = curl_init();
        curl_setopt($genrerequest, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($genrerequest, CURLOPT_URL, $url);
        curl_setopt($genrerequest, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($genrerequest, CURLOPT_SSL_VERIFYPEER, false);
        $data = curl_exec($genrerequest);

        $data = json_decode($data);
        if (isset($data->results))
            foreach ($data->results as $key => $value) {
                // dd($value);
                $dataadd = $this->addtmdbdata($value, 'show');
            }

        $url = "https://api.themoviedb.org/3/search/movie?api_key=f0dc06cd59a97bbddcebd05e902559e0&language=en-US&sort_by=popularity.desc&include_adult=false&query=$keyword&page=1";
        // $justwatch_urlnew = "https://apis.justwatch.com/content/titles/show/11188/locale/en_IN";

        // echo $url;
        $genrerequest = curl_init();
        curl_setopt($genrerequest, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($genrerequest, CURLOPT_URL, $url);
        curl_setopt($genrerequest, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($genrerequest, CURLOPT_SSL_VERIFYPEER, false);
        $data = curl_exec($genrerequest);

        $data = json_decode($data);


        // var_dump($data); die;

        if (isset($data->results))
            foreach ($data->results as $key => $value) {
                $dataadd = $this->addtmdbdata($value, 'movie');
            }
    }


    public function addtmdbdata($movie, $contenttype)
    {
        // if (isset($movie->title)){
        if ($contenttype == 'movie') {

            $releaseyear = '';
            $releasedate = '';

            if (!empty($release_date)) {
                $releaseyear = explode('-', $movie->release_date)[0];
                $releasedate = $movie->release_date;
            }

            $db_check = DB::table('contents')->where('title', $movie->title)->where('original_release_year',  $releaseyear)->orWhere('tmdb_id', $movie->id)->first();
            // echo "/n";
            if (!empty($db_check)) {
                $this->updatetmdbcast('movie', $db_check->content_id);
                $this->updatetmdbtrailor('movie', $db_check->content_id);
            }
        } elseif ($contenttype == 'show') {
            // if (isset($movie->original_title))
            $movie->title = $movie->name ?? '';
            $movie->original_title = $movie->original_name ?? '';
            // dd($movie);
            if (isset($movie->first_air_date)) {
                $releaseyear = explode('-', $movie->first_air_date)[0];
                $releasedate = $movie->first_air_date;
            } else {
                $releaseyear = "";
                $releasedate = "";
            }
            $db_check = DB::table('contents')->where('title', $movie->title)->where('original_release_year',  $releaseyear)->first();
            // var_dump($db_check);
            // echo "/n";
            if (!empty($db_check)) {
                $this->updatetmdbcast('show', $db_check->content_id);
                $this->updatetmdbtrailor('show', $db_check->content_id);
            }
        } else return 0;
        var_dump($db_check);
        // die();
        if (!$db_check) {
            $movie->poster_path = $this->storetmdbimage($movie->poster_path);
            $db_data = DB::table('contents')->insert(
                array(
                    'title' => $movie->title  ?? '',
                    'original_title' => $movie->original_title ?? '',
                    'content_type' => $contenttype ?? '',
                    'short_description' => $movie->overview ?? '',
                    'poster' => $movie->poster_path ?? '',
                    'content_source' => 'tmdb',
                    'content_source_id' => $movie->id,
                    'full_path' => $movie->full_path ?? "",
                    'tmdb_popularity' => $movie->popularity ?? '',
                    'tmdb_score' => 0,
                    'original_release_year' => $releaseyear ?? '',
                    'cinema_release_date' => $releasedate ?? '',
                    'original_language' => $movie->original_language ?? '',
                    'tmdb_id' => $movie->id ?? '0',
                )
            );
        }
        // }

    }


    public function storetmdbimage($path)
    {
        if (!empty($path)) {
            // $key1 = explode('/', $newdata->poster)[2];
            $url1 = 'https://image.tmdb.org/t/p/w500' . $path;
            $url2 = 'https://image.tmdb.org/t/p/w600_and_h900_bestv2' . $path;

            $base_path = public_path();
            $keyname = $path;
            $img1 = $base_path . '/poster' . $keyname;
            $img2 = $base_path . '/thumbnail' . $keyname;
            // echo $url1;
            // die();
            file_put_contents($img1, file_get_contents($url1));
            file_put_contents($img2, file_get_contents($url2));
            // $updatedata['poster'] = $keyname;
            return $path;
        }
    }

    public function addproviders($content_id)
    {
    }

    public function updatecastid()
    {
        $cast = DB::table('content_casting')->where('exec_status', 0)->first();
        if ($cast)
            // foreach ($cast as $key => $value) {
            $check = $this->getcastbyname($cast->name);
        # code...
        // }
    }
    public function getcastbyname($name)
    {
        $original = $name ?? '';

        echo $original;
        $name = str_replace(' ', '%20', $name);
        $url = "https://api.themoviedb.org/3/search/person?api_key=f0dc06cd59a97bbddcebd05e902559e0&language=en-US&query=" . $name . "&page=1&include_adult=false";
        $c = curl_init();
        curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($c, CURLOPT_URL, $url);
        curl_setopt($c, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($c, CURLOPT_SSL_VERIFYPEER, 0);

        $contents = curl_exec($c);

        $contents = json_decode($contents);
        // dd($contents);
        $updatedata = array();
        $updatedata['exec_status'] = 1;
        if (isset($contents->results)) {
            $data = $contents->results;
            foreach ($data as $key => $value) {
                // dd($name);

                $updatedata['tmdb_id'] = $value->id;
                if ($value->profile_path != null) {
                    $updatedata['poster'] = 'https://image.tmdb.org/t/p/original' . $value->profile_path;
                    // dd($value);
                }
                if ($value->name == $original) {
                    echo $value->id;
                    $dbupdate = DB::table('content_casting')
                        ->where('name', $original)
                        ->update($updatedata);
                    return 0;
                    // dd($value);
                }
            }
        }
        $dbupdate = DB::table('content_casting')
            ->where('name', $original)
            ->update($updatedata);
    }


    public function addmxprovider(Request $request)
    {
        $dbcheck = DB::table('contents')->where('content_source', 'mxplayer')->get();
        foreach ($dbcheck as $key => $value) {
            $db_data = DB::table('content_providers')->insert(
                array(
                    'content_id' => $value->content_id,
                    'provider_id' => 700,
                    'video_quality' => 'SD',
                    'currency' => 'INR',
                    'monetization_type' => 'ads',
                    'retail_price' => '0',
                    'web_url' => ""
                )
            );
        }
    }


    public function getulludata(Request $request)
    {
        $keyword = $request->data;
        $keyword = str_replace(' ', '%20', $keyword);
        $data = file_get_contents("https://ullu.app/ulluCore/api/v1/media/v2/client/search?titleText=" . $keyword . "&page=0&size=40");
        $data = json_decode($data);
        foreach ($data->content as $key => $value) {
            $dataadd = $this->addulludata($value, 'shows');
        }
    }

    public function addulludata($movies, $contenttype)
    {
        if (isset($movies->portraitPosterId)) {

            $url1 = 'https://d1dfc9w6nzu9oi.cloudfront.net/' . $movies->portraitPosterId;
            $url2 = 'https://d1dfc9w6nzu9oi.cloudfront.net/' . $movies->landscapePosterId;
            $base_path = public_path();
            $keyname = $movies->portraitPosterId;
            $img2 = $base_path . '/thumbnail/' . $movies->portraitPosterId;
            $img1 = $base_path . '/poster/' . $movies->portraitPosterId;
            $updateData['poster'] = $keyname;
            // Save image
            file_put_contents($img1, file_get_contents($url1));
            file_put_contents($img2, file_get_contents($url2));

            // }
        }
        $dbcheck = DB::table('contents')->where('content_source', 'ullu')->where('content_source_id', $movies->id)->first();
        if (!$dbcheck) {
            $db_data1 = DB::table('contents')->insert(
                array(
                    'title' => $movies->title,
                    'original_title' => $movies->title,
                    'content_type' => $contenttype,
                    'poster' => $updateData['poster'],
                    'content_source' => 'ullu',
                    'content_source_id' => $movies->id,
                    'full_path' => $movies->titleYearSlug ?? "",
                    'tmdb_popularity' => 0,
                    'original_release_year' => '',
                    'cinema_release_date' => date('Y-m-d'),
                    'original_language' => "",
                    'tmdb_score' => "",
                    'imdb_score' => '',
                    'parent_id' => 0
                )
            );
            $dbcheck = DB::table('contents')->where('title', $movies->title)->where('content_source', 'ullu')->where('content_type', $contenttype)->where('content_source_id', $movies->id)->first();
            if ($dbcheck)
                $db_data = DB::table('content_providers')->insert(
                    array(
                        'content_id' => $dbcheck->content_id,
                        'provider_id' => 701,
                        'video_quality' => 'SD',
                        'currency' => 'INR',
                        'monetization_type' => 'ads',
                        'retail_price' => '0',
                        'web_url' => ""
                    )
                );
        }
    }

    public function getairteldata(Request $request)
    {
        $keyword = $request->data;
        $keyword = str_replace(' ', '%20', $keyword);
        $url = "https://search.airtel.tv/app/v3/search/atv/query?appId=WEB&bn=18&count=200&dt=BROWSER&ln=hi&offSet=0&os=WEBOS&q=" . $keyword;
        $genrerequest = curl_init();
        curl_setopt($genrerequest, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($genrerequest, CURLOPT_URL, $url);
        curl_setopt($genrerequest, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($genrerequest, CURLOPT_SSL_VERIFYPEER, false);
        $contents = curl_exec($genrerequest);
        $count = 0;
        $contents = json_decode($contents);
        if (isset($contents->categories)) {
            foreach ($contents->categories as $key => $categories) {
                if ($categories->type == 'LIVETVCHANNEL') {
                    continue;
                } elseif ($categories->type == 'MOVIE') {
                    $data = $categories->contentResponseList;
                    echo count($data);
                    foreach ($data as $key => $value) {
                        echo $value->title;
                        if (isset($value->releaseYear) && $value->releaseYear > 0)
                            $return1 = $this->addairtelmovie($value, 'movie');
                        // dd($value);
                        $count++;
                    }
                } else {
                    // dd($categories);
                }
            }
        }
        // dd($contents->categories);
        return $count;
    }

    public function addairtelmovie($content, $content_type)
    {
        $moviepath = "https://www.airtelxstream.in/movies/" . strtolower(str_replace(' ', '-', $content->title)) . "/" . $content->id;
        $DBcheck = DB::table('contents')->where('title', $content->title)->where('original_release_year', $content->releaseYear)->where('content_type', $content_type)->first();
        if ($DBcheck) {
            $pro_check =  DB::table('content_providers')->where('content_id', $DBcheck->content_id)->where('provider_id', 702)->first();
            if (!$pro_check)
                $db_data = DB::table('content_providers')->insert(array(
                    'content_id' => $DBcheck->content_id,
                    'provider_id' => 702,
                    'video_quality' => 'SD',
                    'currency' => 'INR',
                    'monetization_type' => 'flatrate',
                    'retail_price' => '0',
                    'web_url' => $moviepath
                ));
        } else {
            // dd($content);
            $movie = array();
            $movie['title'] = $content->title;
            $movie['original_title'] = $content->title;
            $movie['content_type'] = $content_type;
            $movie['content_source'] = 'airtelxtream';
            $movie['content_source_id'] = $content->id;
            $movie['original_release_year'] = $content->releaseYear;
            $movie['poster'] = $content->images->PORTRAIT;

            $url1 = $movie['poster'];
            $base_path = public_path();
            $keyname = $content->id . '.jpg';
            $img1 = $base_path . '/poster/' . $keyname;
            $movie['poster'] = $keyname;

            $genrerequest = curl_init();
            curl_setopt($genrerequest, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($genrerequest, CURLOPT_URL, $url1);
            curl_setopt($genrerequest, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($genrerequest, CURLOPT_SSL_VERIFYPEER, false);
            $contents = curl_exec($genrerequest);
            file_put_contents($img1, file_get_contents($contents));
            $movie['runtime'] = $content->duration;
            $movie['full_path'] = $moviepath;
            $movie['original_language'] = implode(',', $content->languages);
            $db_data1 = DB::table('contents')->insert($movie);
            $dbcheck = DB::table('contents')->where('title', $content->title)->where('content_source', 'airtelxtream')->where('content_type', $content_type)->where('content_source_id', $content->id)->first();

            $db_data = DB::table('content_providers')->insert(array(
                'content_id' => $dbcheck->content_id,
                'provider_id' => 702,
                'video_quality' => 'SD',
                'currency' => 'INR',
                'monetization_type' => 'flatrate',
                'retail_price' => '0',
                'web_url' => $moviepath
            ));
        }
        return 1;
    }


    public function getvodafonedata(Request $request)
    {
        $keyword = $request->data;
        $keyword = str_replace(' ', '%20', $keyword);
        $url = "https://api.vodafoneplay.in/content/v7/search/?fields=generalInfo,contents,images&&level=&startIndex=1&count=1000&orderBy=releaseDate&orderMode=1&type=movie%2Cvodchannel%2Ctvshow%2Ctvseries&query=" . $keyword;
        $genrerequest = curl_init();
        curl_setopt($genrerequest, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($genrerequest, CURLOPT_URL, $url);
        curl_setopt($genrerequest, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($genrerequest, CURLOPT_SSL_VERIFYPEER, false);
        $contents = curl_exec($genrerequest);
        $count = 0;
        $contents = json_decode($contents);
        // if ($contents->results)
        // 	$contents = $contents->results;

        if (isset($contents->results)) {
            foreach ($contents->results as $key => $categories) {
                if ($categories->generalInfo->type == 'tvseries') {
                    continue;
                } elseif ($categories->generalInfo->type == 'movie') {
                    // dd($categories);
                    $basic = $categories->generalInfo;
                    $dates = $categories->content;
                    $images = $categories->images;
                    // dd($images);
                    $return1 = $this->addvodafonemovie($basic, $images, 'movie', $images);
                } else {
                    // dd($categories);
                    // dd($categories);
                }
            }
        }
        // dd($contents->categories);
        return $count;
    }


    public function storevodefoneimage($path)
    {

        foreach ($path as $key => $value) {
            // dd($value);
            if ($value->type == 'portraitcoverposter') {
                $url1 = $value->link;
                $url2 = $value->link;
                $temp = explode('/', $value->link);
                $keyname = $temp[count($temp) - 1];
            }
        }
        if ($url1) {
            $base_path = public_path();
            $img1 = $base_path . '/poster/' . $keyname;
            $img2 = $base_path . '/thumbnail/' . $keyname;
            // echo $url1;
            // die();
            file_put_contents($img1, file_get_contents($url1));
            file_put_contents($img2, file_get_contents($url2));
            // $updatedata['poster'] = $keyname;
            // echo $keyname;
            return $keyname;
        }
    }

    public function addvodafonemovie($basic, $dates, $content_type, $images = false)
    {
        if (isset($dates->releaseDate))
            $releaseyear = explode('-', $dates->releaseDate)[0];
        else
            $releaseyear = "";
        $moviepath = "https://www.vodafoneplay.in/" . $content_type . "s/detail/" . $basic->_id . "/" . strtolower(str_replace(' ', '-', $basic->title));

        $path = $this->storevodefoneimage($images->values);
        // echo $basic->title;
        // echo $releaseyear;
        $DBcheck = DB::table('contents')->where('title', $basic->title)->where('original_release_year', $releaseyear)->where('content_type', $content_type)->first();
        // dd($DBcheck);

        if ($DBcheck) {
            // echo $DBcheck->content_id;
            DB::table('contents')->where('content_id', $DBcheck->content_id)->update(['poster' => $path, 'execute_status' => 1]);
            $pro_check =  DB::table('content_providers')->where('content_id', $DBcheck->content_id)->where('provider_id', 703)->first();
            if (!$pro_check)
                $db_data = DB::table('content_providers')->insert(array(
                    'content_id' => $DBcheck->content_id,
                    'provider_id' => 703,
                    'video_quality' => 'SD',
                    'currency' => 'INR',
                    'monetization_type' => 'flatrate',
                    'retail_price' => '0',
                    'web_url' => $moviepath
                ));
        } else {
            // dd($dates);
            $movie = array();
            $movie['title'] = $basic->title;
            $movie['original_title'] = $basic->title;
            $movie['content_type'] = $content_type;
            $movie['content_source'] = 'vodafoneplay';
            $movie['content_source_id'] = $basic->_id;
            $movie['original_release_year'] = $releaseyear;
            $movie['short_description'] = $basic->briefDescription;
            $movie['poster'] = $path;

            if (isset($dates->duration)) {
                $time = explode(':', $dates->duration);
                $runtime = ($time[0] * 60) + ($time[1] * 1) + ($time[2] / 60);
            } else
                $runtime = "";
            $movie['runtime'] = $runtime;
            $movie['full_path'] = $moviepath;
            if (isset($dates->language))
                $movie['original_language'] = implode(',', $dates->language);
            $dbcheck = DB::table('contents')->where('content_source_id', $basic->_id)->where('content_type', $content_type)->first();
            if (!$dbcheck) {
                $db_data1 = DB::table('contents')->insert($movie);
                $dbcheck = DB::table('contents')->where('title', $basic->title)->where('content_source', 'vodafoneplay')->where('content_type', $content_type)->where('content_source_id', $basic->_id)->first();
            }

            $dbcheck1 = DB::table('content_providers')->where('content_id', $dbcheck->content_id)->where('provider_id', '703')->first();
            if (!$dbcheck1)
                $db_data = DB::table('content_providers')->insert(array(
                    'content_id' => $dbcheck->content_id,
                    'provider_id' => 703,
                    'video_quality' => 'SD',
                    'currency' => 'INR',
                    'monetization_type' => 'flatrate',
                    'retail_price' => '0',
                    'web_url' => $moviepath
                ));
        }
        return 1;
    }


    public function gettvfdata(Request $request)
    {
        $keyword = $request->data;
        $keyword = str_replace(' ', '%20', $keyword);
        $url = "https://webapi-services.tvfplay.com/v2/api/v2/search/w/autocomplete/" . $keyword;
        $genrerequest = curl_init();
        curl_setopt($genrerequest, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($genrerequest, CURLOPT_URL, $url);
        curl_setopt($genrerequest, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($genrerequest, CURLOPT_SSL_VERIFYPEER, false);
        $contents = curl_exec($genrerequest);
        $count = 0;
        $contents = json_decode($contents);
        // if ($contents->results)
        // 	$contents = $contents->results;
        dd($contents);
        if (isset($contents->results)) {
            foreach ($contents->results as $key => $categories) {
                if ($categories->generalInfo->type == 'tvseries') {
                    continue;
                } elseif ($categories->generalInfo->type == 'movie') {
                    // dd($categories);
                    $basic = $categories->generalInfo;
                    $dates = $categories->content;

                    $return1 = $this->addtvfmovie($basic, $dates, 'movie');
                } else {
                    // dd($categories);
                    // dd($categories);
                }
            }
        }
        // dd($contents->categories);
        return $count;
    }

    public function addtvfmovie($basic, $dates, $content_type)
    {
        $releaseyear = explode('-', $dates->releaseDate)[0];
        echo $moviepath = "https://www.vodafoneplay.in/" . $content_type . "s/detail/" . $basic->_id . "/" . strtolower(str_replace(' ', '-', $basic->title));
        echo $releaseyear;
        $DBcheck = DB::table('contents')->where('title', $basic->title)->where('original_release_year', $releaseyear)->where('content_type', $content_type)->first();

        if ($DBcheck) {
            $pro_check =  DB::table('content_providers')->where('content_id', $DBcheck->content_id)->where('provider_id', 703)->first();
            if (!$pro_check)
                $db_data = DB::table('content_providers')->insert(array(
                    'content_id' => $DBcheck->content_id,
                    'provider_id' => 703,
                    'video_quality' => 'SD',
                    'currency' => 'INR',
                    'monetization_type' => 'flatrate',
                    'retail_price' => '0',
                    'web_url' => $moviepath
                ));
        } else {
            // dd($dates);
            $movie = array();
            $movie['title'] = $basic->title;
            $movie['original_title'] = $basic->title;
            $movie['content_type'] = $content_type;
            $movie['content_source'] = 'vodafoneplay';
            $movie['content_source_id'] = $basic->_id;
            $movie['original_release_year'] = $releaseyear;
            $movie['short_description'] = $basic->briefDescription;
            // $movie['poster'] = $content->images->PORTRAIT;

            // $url1 = $movie['poster'];
            // $base_path = public_path();
            // $keyname = $content->id.'.jpg';
            // $img1 = $base_path.'/poster/'.$keyname;
            // $movie['poster'] = $keyname;

            // $genrerequest = curl_init();
            // curl_setopt($genrerequest,CURLOPT_RETURNTRANSFER,1);
            // curl_setopt($genrerequest, CURLOPT_URL, $url1);
            // curl_setopt($genrerequest, CURLOPT_SSL_VERIFYHOST, false);
            // curl_setopt($genrerequest, CURLOPT_SSL_VERIFYPEER, false);
            // $contents = curl_exec($genrerequest);
            // file_put_contents($img1, file_get_contents($contents));
            $time = explode(':', $dates->duration);
            $runtime = ($time[0] * 60) + ($time[1] * 1) + ($time[2] / 60);
            $movie['runtime'] = $runtime;
            $movie['full_path'] = $moviepath;
            $movie['original_language'] = implode(',', $dates->language);

            $db_data1 = DB::table('contents')->insert($movie);
            $dbcheck = DB::table('contents')->where('title', $basic->title)->where('content_source', 'vodafoneplay')->where('content_type', $content_type)->where('content_source_id', $basic->_id)->first();


            $db_data = DB::table('content_providers')->insert(array(
                'content_id' => $dbcheck->content_id,
                'provider_id' => 703,
                'video_quality' => 'SD',
                'currency' => 'INR',
                'monetization_type' => 'flatrate',
                'retail_price' => '0',
                'web_url' => $moviepath
            ));
        }
        return 1;
    }


    public function findduplicatedata(Request $request)
    {
        echo "<pre>";
        echo "hi";
        $results = \App\Content::select('*')->where('content_type', 'movie')->groupBy('title')->havingRaw('count(*) > 1')->get();
        echo "hi";
        count($results);
        dd($results);
        echo "hi";
        foreach ($results as $key => $value) {
            // echo $value->content_id." ";
            $db = \App\Content::where('title', $value->title)->where('content_type', 'movie')->where('content_id', '!=', $value->content_id)->first();
            if ($db && ($value->content_source == $db->content_source) && ($value->content_source_id == $db->content_source_id) && ($value->content_type == $db->content_type)) {
                print_r($db->content_id);
                $delete = \App\Content::where('content_id', $db->content_id)->delete();
            }
            if ($db && ($value->content_source != $db->content_source) && ($value->content_type != $db->content_type)) {
                echo $value->content_source_id . " " . $value->content_source_id . " " . $db->content_source . " " . $db->content_source . " " . "\n";
                if ($value->content_source == $db->content_source)
                    if ($db->content_source == 'mxplayer' && ($value->content_source == 'justwatch' || $value->content_source == 'admin')) {
                        echo "hi";
                    }
            } else
                continue;
        }
        dd($results);
        return $results;
    }



    public function testreleasedateupdate(Request $request)
    {
        $seasons = \DB::table('contents')->where('cinema_release_date', '')->where('content_type', 'movie')->orderBy('original_release_year', 'desc')->first();
        if ($seasons) {
            $seasons->original_title = urlencode($seasons->original_title);
            $url = "https://api.themoviedb.org/3/search/$seasons->content_type?api_key=f0dc06cd59a97bbddcebd05e902559e0&language=en-US&sort_by=popularity.desc&query=$seasons->original_title&page=1&year=$seasons->original_release_year";
            $c = curl_init();
            curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($c, CURLOPT_URL, $url);
            curl_setopt($c, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($c, CURLOPT_SSL_VERIFYPEER, 0);
            $contents = curl_exec($c);
            $contents = json_decode($contents);
            if (!empty($contents)) {
                // $total_pages = $contents->total_pages;
                $matches = count($contents->results);
                if ($matches == 1) {
                    $data = $contents->results[0];
                    $seasons1 = DB::table('contents')->where('content_id', $seasons->content_id)->update(['cinema_release_date' => $data->release_date]);
                } else
                    $seasons1 = DB::table('contents')->where('content_id', $seasons->content_id)->update(['cinema_release_date' => '1711-12-12']);
                echo $seasons1;
            }
        }
    }

    public function updatedata(Request $request)
    {
        $seasons = \DB::table('contents')->where('original_language', '!=', '')->first();
        // foreach ($season as $key => $seasons) {

        if ($seasons) {
            $seasons->original_title = urlencode($seasons->original_title);
            $url = "https://api.themoviedb.org/3/search/$seasons->content_type?api_key=f0dc06cd59a97bbddcebd05e902559e0&language=en-US&sort_by=popularity.desc&query=$seasons->original_title&page=1&year=$seasons->original_release_year";
            $c = curl_init();
            curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($c, CURLOPT_URL, $url);
            curl_setopt($c, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($c, CURLOPT_SSL_VERIFYPEER, 0);
            $contents = curl_exec($c);
            $contents = json_decode($contents);

            if (!empty($contents)) {
                // $total_pages = $contents->total_pages;
                if (isset($contents->results)) {
                    $matches = count($contents->results);
                    if ($matches == 1) {
                        $data = $contents->results[0];
                        $seasons1 = DB::table('contents')->where('content_id', $seasons->content_id)->update(['cinema_release_date' => $data->release_date]);
                    } else
                        $seasons1 = DB::table('contents')->where('content_id', $seasons->content_id)->update(['cinema_release_date' => '1711-12-12']);
                }
            }
        }
        // }
    }


    public function updatecast(Request $request)
    {
        $season =  \DB::table('content_casting')->where('poster', '!=', '')->where('exec_status', 0)->first();
        try {
            $url = $season->poster;
            $c = curl_init();
            curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($c, CURLOPT_URL, $url);
            curl_setopt($c, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($c, CURLOPT_SSL_VERIFYPEER, 0);
            $contents = curl_exec($c);

            if ($contents == "<h1>File not Found</h1>") {
                DB::table('content_casting')->where('person_id', $season->person_id)->update(['poster' => "", 'exec_status' => 1]);
                // echo "hi";
            } else {
                DB::table('content_casting')->where('person_id', $season->person_id)->update(['exec_status' => 1]);
                // echo "Bye";
            }
            // dd($contents);
        } catch (Exception $e) {

            DB::table('content_casting')->where('person_id', $season->person_id)->update(['poster' => "", 'exec_status' => 1]);
        }

        // dd($season);

    }


    public function updatevodafoneposter(Request $request)
    {
        $movie = \DB::table('contents')->where('content_source', 'vodafoneplay')->where('execute_status', 0)->orderBy('created_at', 'desc')->first();
        // dd($movie);
        $keyword = $movie->title;
        $keyword = str_replace(' ', '%20', $keyword);
        // echo $movie->content_id;
        $url = "https://api.vodafoneplay.in/content/v7/search/?fields=generalInfo,contents,images&&level=&startIndex=1&count=1000&orderBy=releaseDate&orderMode=1&type=movie%2Cvodchannel%2Ctvshow%2Ctvseries&query=" . $keyword;
        // dd($url);
        $genrerequest = curl_init();
        curl_setopt($genrerequest, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($genrerequest, CURLOPT_URL, $url);
        curl_setopt($genrerequest, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($genrerequest, CURLOPT_SSL_VERIFYPEER, false);
        $contents = curl_exec($genrerequest);
        $count = 0;
        $contents = json_decode($contents);
        DB::table('contents')->where('content_id', $movie->content_id)->update(['execute_status' => 1]);
        if (isset($contents->results)) {
            foreach ($contents->results as $key => $categories) {
                if ($categories->generalInfo->type == 'tvseries') {
                    continue;
                } elseif ($categories->generalInfo->type == 'movie') {
                    // dd($categories);
                    $basic = $categories->generalInfo;
                    $dates = $categories->content;
                    $images = $categories->images;

                    $return1 = $this->addvodafonemovie($basic, $dates, 'movie', $images);
                } else {
                    // dd($categories);
                    // dd($categories);
                }
            }
        }
        // dd($contents->categories);
        return $count;
    }

    public function updatetmdbid(Request $request)
    {

        // get first movie
        if (isset($request->id)) {

            $movie = \DB::table('contents')->where('content_id', $request->id)->first();
        } else
            $movie = \DB::table('contents')->where('tmdb_id', 0)->where('execute_status', 0)->orderBy('created_at', 'desc')->first();

        // var_dump($movie->title);

        // change the status to 1 so that no other job works on it
        $movie3 = \DB::table('contents')->where('content_id', $movie->content_id)->update(['execute_status' => 1]);


        //check if movie exists on tmdb
        $movie->title = str_replace(' ', '%20', $movie->title);
        // dd($movie);
        if ($movie->content_type == "movie") {
            $url = "https://api.themoviedb.org/3/search/movie?api_key=f0dc06cd59a97bbddcebd05e902559e0&language=en-US&sort_by=popularity.desc&query=$movie->title&page=1&year=$movie->original_release_year";
        } elseif ($movie->content_type == 'show') {
            $url = "https://api.themoviedb.org/3/search/tv?api_key=f0dc06cd59a97bbddcebd05e902559e0&language=en-US&sort_by=popularity.desc&query=$movie->title&page=1&year=$movie->original_release_year";
        }
        // echo $url;
        if (isset($url)) {
            $genrerequest = curl_init();
            // echo "hi";
            curl_setopt($genrerequest, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($genrerequest, CURLOPT_URL, $url);
            curl_setopt($genrerequest, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($genrerequest, CURLOPT_SSL_VERIFYPEER, false);
            $contents = curl_exec($genrerequest);
            $count = 0;
            $contents = json_decode($contents);
            // dd($contents);
            // dd($url);
            if (isset($contents->results) && count($contents->results) > 0) {
                foreach ($contents->results as $key => $value) {
                    $releaseyear = "";
                    if ($movie->content_type == "movie") {
                        if (isset($movie->release_date)) {
                            $releaseyear = explode('-', $value->release_date)[0];
                            $releasedate = $movie->release_date;
                        }
                    } elseif ($movie->content_type == 'show') {
                        $movie->title = $value->original_name;
                        $movie->original_title = $value->original_name;
                        // dd($movie);
                        if (isset($movie->first_air_date)) {
                            $releaseyear = explode('-', $value->first_air_date)[0];
                            $releasedate = $value->first_air_date;
                        }
                    } else return 0;

                    $updatedata = [];
                    // $updatedata['execute_status'] = 1;
                    $updatedata['tmdb_id'] = $value->id;
                    if ($movie->title == "" || $movie->title == null) {
                        $updatedata['title'] = $value->title;
                    }

                    if ($movie->original_title == "" || $movie->original_title == null) {
                        $updatedata['original_title'] = $value->original_title;
                    }
                    if ($movie->short_description == "" || $movie->short_description == null) {
                        $updatedata['short_description'] = $value->overview;
                    }

                    if ($movie->poster == "" || $movie->poster == null) {
                        $updatedata['poster'] = $this->storetmdbimage($value->poster_path);
                    }

                    if ($movie->original_language == "" || $movie->original_language == null) {
                        $updatedata['original_language'] = $value->original_language;
                    }


                    if (isset($value->genre_ids) && count($value->genre_ids) > 0) {
                        foreach ($value->genre_ids as $key1 => $value1) {
                            $checkdb = \DB::table('content_genres')->where('content_id', $movie->content_id)->where('genre_id', $value1)->first();
                            if (!$checkdb) {
                                \DB::table('content_genres')->insert(['content_id' => $movie->content_id, 'genre_id' => $value1]);
                            }
                        }
                    }


                    $check1 = \DB::table('contents')->where('content_id', $movie->content_id)->update($updatedata);
                    // var_dump($movie); die;
                    $this->updatetmdbcast($movie->content_type, $movie->content_id);
                    $this->updatetmdbtrailor($movie->content_type, $movie->content_id);
                    if ($check1) {
                        return 0;
                    }

                    // $data = array(

                    //                  'content_source_id' => $movie->id,
                    //                  'full_path' => $movie->full_path ?? "",
                    //                  'tmdb_popularity' => $movie->popularity,
                    //                  'tmdb_score' => 0,
                    //                  'original_release_year' => $releaseyear,
                    //                  'cinema_release_date' => $releasedate,
                    //                  'original_language' => $movie->original_language,
                    //                  'tmdb_score' => $movie->popularity,
                    //              )	;


                    // if ($releaseyear == $movie->original_release_year)


                }
            }
        }


        // die();

    }


    public function updatetmdbmovie(Request $request)
    {

        // get first movie

        if ($request->movie_id) {

            $movie = \DB::table('contents')->where('content_id', $request->movie_id)->where('content_type', 'movie')->orderBy('created_at', 'desc')->first();
        } else{
            $movie = \DB::table('contents')->where('tmdb_id', '!=', 0)->where('execute_status', 1)->where('content_type', 'movie')->orderBy('created_at', 'desc')->first();
            // var_dump($movie->title);

            // change the status to 1 so that no other job works on it
            $movie3 = \DB::table('contents')->where('content_id', $movie->content_id)->update(['execute_status' => 2]);
        }
        
            

        //check if movie exists on tmdb
        if ($movie){
        $movie->title = str_replace(' ', '%20', $movie->title);
        }else{
            return "movie not found";
        }
        if ($movie->tmdb_id > 0) {
            $tmdb_id = $movie->tmdb_id;
        }
        if ($movie->content_source == 'tmdb') {
            $tmdb_id = $movie->content_source_id;
        }
        // dd($movie);
        $url = "https://api.themoviedb.org/3/movie/$tmdb_id?api_key=f0dc06cd59a97bbddcebd05e902559e0&language=en-US&include_adult=true";
        // echo $url;
        if (isset($url)) {
            $genrerequest = curl_init();
            // echo "hi";
            curl_setopt($genrerequest, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($genrerequest, CURLOPT_URL, $url);
            curl_setopt($genrerequest, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($genrerequest, CURLOPT_SSL_VERIFYPEER, false);
            $contents = curl_exec($genrerequest);
            $count = 0;
            $contents = json_decode($contents);
            // dd($contents);
            // dd($url);
            if ($contents) {

                $releaseyear = "";
                // dd($contents);
                if (isset($contents->release_date)) {
                    $releaseyear = explode('-', $contents->release_date)[0];
                    $releasedate = $contents->release_date;
                } else return 0;

                $updatedata = [];
                $updatedata['execute_status'] = 2;
                $updatedata['tmdb_id'] = $contents->id;
                $updatedata['imdb_id'] = $contents->imdb_id;
                $updatedata['original_release_year'] = $releaseyear;
                $updatedata['cinema_release_date'] = $releasedate;

                // 'title' => $value23->name,
                // 'original_title' => $movie->title ?? ''." ".$value23->name ?? '',
                // 'content_type' => "tv-season",
                // 'poster' => $movie->poster ?? '',
                // 'short_description' => $value23->overview ?? '',
                // 'content_source' => 'tmdb',
                // 'content_source_id' => $value23->id,
                // 'tmdb_id' => $value23->id,
                // 'season_number' => $value23->season_number ?? '',
                // 'full_path' => $movie->full_path ?? "",
                // 'tmdb_popularity' => $movie->tmdb_popularity ?? '',
                // 'original_release_year' => explode('-',$value23->air_date)[0] ?? '',
                // 'cinema_release_date' => $value23->air_date ?? '',
                // 'original_language' => $movie->original_language ?? "",
                // 'tmdb_score' => $movie->tmdb_score ?? "",
                // 'imdb_score' => $movie->imdb_score ?? '',
                // 'parent_id' => $movie->content_id
                if ($movie->revenue == "" || $movie->revenue == null) {
                    $updatedata['revenue'] = $contents->revenue;
                }

                if ($movie->runtime == "" || $movie->runtime == null) {
                    $updatedata['runtime'] = $contents->runtime;
                }
                // dd($updatedata);

                $check1 = \DB::table('contents')->where('content_id', $movie->content_id)->update($updatedata);
                $movie = \DB::table('contents')->where('content_id', $movie->content_id)->first();
                if ($movie) {
                    $this->updatetmdbcast($movie->content_type, $movie->content_id);
                    $this->updatetmdbtrailor($movie->content_type, $movie->content_id);
                    return 0;
                }
            }
        }
    }


    public function updatetmdbtv(Request $request)
    {

        $tmdb_id = 0;

        // get first movie

        if (isset($request->tv_id)) {
            $movie = \DB::table('contents')->where('content_id', $request->tv_id)->where('content_type', 'show')->first();
            // dd($movie);

        } else
            $movie = \DB::table('contents')->where('execute_status', 1)->where('content_type', 'show')->first();
        // var_dump($movie->title); die;
        // if ($movie && $movie->tmdb_id == 0){
        if ($movie) {

            //check if movie exists on tmdb
            $movie->title = str_replace(' ', '%20', $movie->title);
            // dd($movie);

            echo $url = "https://api.themoviedb.org/3/search/tv?api_key=f0dc06cd59a97bbddcebd05e902559e0&language=en-US&sort_by=popularity.desc&query=$movie->title&page=1&year=$movie->original_release_year&include_adult=true";

            // echo $url;	die;
            if (isset($url)) {
                $genrerequest = curl_init();
                // echo "hi";
                curl_setopt($genrerequest, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($genrerequest, CURLOPT_URL, $url);
                curl_setopt($genrerequest, CURLOPT_SSL_VERIFYHOST, false);
                curl_setopt($genrerequest, CURLOPT_SSL_VERIFYPEER, false);
                $contents = curl_exec($genrerequest);
                $count = 0;
                $contents = json_decode($contents);
                // dd($contents);
                // dd($url);
                if (isset($contents->results) && count($contents->results) > 0) {

                    // var_dump($contents->results); die;
                    foreach ($contents->results as $key => $value) {
                        // var_dump($value); die;
                        $tmdb_id = $value->id;
                        $releaseyear = "";
                        $releasedate = '';
                        $movie->title = $value->original_name ?? '';
                        $movie->original_title = $value->original_name ?? '';
                        // dd($movie);
                        if (isset($movie->first_air_date)) {
                            $releaseyear = explode('-', $value->first_air_date)[0];
                            $releasedate = $value->first_air_date;

                            $updatedata = [];
                            // $updatedata['execute_status'] = 1;
                            $updatedata['tmdb_id'] = $value->id;
                            if ($movie->title == "" || $movie->title == null) {
                                $updatedata['title'] = $value->title;
                            }

                            if ($movie->original_title == "" || $movie->original_title == null) {
                                $updatedata['original_title'] = $value->original_name;
                            }
                            if ($movie->short_description == "" || $movie->short_description == null) {
                                $updatedata['short_description'] = $value->overview;
                            }

                            if ($movie->poster == "" || $movie->poster == null) {
                                $updatedata['poster'] = $this->storetmdbimage($value->poster_path);
                            }

                            if ($movie->original_language == "" || $movie->original_language == null) {
                                $updatedata['original_language'] = $value->original_language;
                            }


                            if (isset($value->genre_ids) && count($value->genre_ids) > 0) {
                                foreach ($value->genre_ids as $key1 => $value1) {
                                    $checkdb = \DB::table('content_genres')->where('content_id', $movie->content_id)->where('genre_id', $value1)->first();
                                    if (!$checkdb) {
                                        \DB::table('content_genres')->insert(['content_id' => $movie->content_id, 'genre_id' => $value1]);
                                    }
                                }
                            }


                            $check1 = \DB::table('contents')->where('content_id', $movie->content_id)->update($updatedata);
                        }


                        // echo $tmdb_id." ";


                        $movie1 = \DB::table('contents')->where('tmdb_id', $tmdb_id)->first();
                        // var_dump($movie); die;

                        $url = "https://api.themoviedb.org/3/tv/$tmdb_id?api_key=f0dc06cd59a97bbddcebd05e902559e0&language=en-US";

                        // echo $url; die;


                        if (isset($url)) {
                            $genrerequest = curl_init();

                            curl_setopt($genrerequest, CURLOPT_RETURNTRANSFER, 1);
                            curl_setopt($genrerequest, CURLOPT_URL, $url);
                            curl_setopt($genrerequest, CURLOPT_SSL_VERIFYHOST, false);
                            curl_setopt($genrerequest, CURLOPT_SSL_VERIFYPEER, false);
                            $contents = curl_exec($genrerequest);
                            $count = 0;
                            $contents = json_decode($contents);

                            // var_dump($contents); die;

                            if ($contents) {

                                $releaseyear = "";
                                if (isset($contents->first_air_date)) {
                                    $releaseyear = explode('-', $contents->first_air_date)[0];
                                    $releasedate = $contents->first_air_date;
                                }


                                $updatedata = [];
                                $updatedata['execute_status'] = 2;

                                // dd($movie);
                                if ($movie->total_episodes == "" || $movie->total_episodes == null) {
                                    $updatedata['total_episodes'] = $contents->number_of_episodes ?? '0';
                                }

                                if ($movie->total_seasons == "" || $movie->total_seasons == null) {
                                    $updatedata['total_seasons'] = $contents->number_of_seasons ?? '0';
                                }

                                if (($movie->runtime == "" || $movie->runtime == null) && isset($contents->episode_run_time[0])) {
                                    $updatedata['runtime'] = $contents->episode_run_time[0];
                                }

                                if (!empty($contents->networks)) {


                                    foreach ($contents->networks as $key32 => $value32) {
                                        $getOTT = \DB::table('providers')->where('technical_name', $value32->name)->first();
                                        if ($getOTT) {
                                            $provider_id = $getOTT->provider_id;
                                        } else {



                                            if (!empty($value32->logo_path)) {

                                                $url1 = 'https://image.tmdb.org/t/p/w200' . $value32->logo_path;

                                                $base_path = public_path();
                                                $keyname = '/providers' . $value32->logo_path;
                                                $img1 = $base_path . '/asset' . $keyname;

                                                file_put_contents($img1, file_get_contents($url1));
                                            } else {
                                                $keyname = "";
                                            }
                                            \DB::table('providers')->insert([
                                                'profile_id' => $value32->id,
                                                'title' => $value32->name ?? '',
                                                'technical_name' => $value32->name ?? '',
                                                'short_name' => $value32->name ?? '',
                                                'slug' => $value32->name ?? '',
                                                'icon_url' => $keyname,
                                                'content_source' => 'tmdb',
                                                'content_source_id' => $value32->id,
                                            ]);
                                            $getOTT = \DB::table('providers')->where('technical_name', $value32->name)->first();
                                            $provider_id = $getOTT->provider_id;
                                        }


                                        $dbcheck = DB::table('content_providers')->where('content_id', $movie->content_id)->where('provider_id', $provider_id)->first();


                                        if (!$dbcheck) {
                                            $db_data = DB::table('content_providers')->insert(array(
                                                'content_id' => $movie->content_id,
                                                'provider_id' => $provider_id,
                                                'video_quality' => 'SD',
                                                'currency' => 'INR',
                                                'monetization_type' => 'flatrate',
                                                'retail_price' => '0',
                                                'isLive' => 1
                                            ));
                                        }
                                    }
                                }
                            }

                            // echo $movie->content_id; die;

                            // $dbcheck = \DB::table('contents')->where('parent_id', $movie->content_id)->where('content_source','tmdb')->get();
                            // var_dump($contents->seasons); die;

                            if (!empty($contents->seasons)) {

                                // var_dump($contents->seasons); die;
                                foreach ($contents->seasons as $key32 => $value23) {
                                    // echo $movie->content_id; die;

                                    // echo $value23->id; die;


                                    $check_season = \DB::table('contents')->where('parent_id', $movie->content_id)->where('tmdb_id', $value23->id)->first();
                                    // var_dump($check_season); die;

                                    if (!empty($check_season)) {
                                        // echo 'Found';
                                        $db_data = DB::table('contents')->where('parent_id', $movie->content_id)->where('tmdb_id', $value23->id)->update(
                                            array(
                                                'title' => $value23->name,
                                                'original_title' => $movie->title ?? '' . " " . $value23->name ?? '',
                                                'content_type' => "tv-season",
                                                'poster' => $movie->poster ?? '',
                                                'short_description' => $value23->overview ?? '',
                                                'content_source' => 'tmdb',
                                                'content_source_id' => $value23->id,

                                                'season_number' => $value23->season_number ?? '',
                                                'full_path' => $movie->full_path ?? "",
                                                'tmdb_popularity' => $movie->tmdb_popularity ?? '',
                                                'original_release_year' => explode('-', $value23->air_date)[0] ?? '',
                                                'cinema_release_date' => $value23->air_date ?? '',
                                                'original_language' => $movie->original_language ?? "",
                                                'tmdb_score' => $movie->tmdb_score ?? "",
                                                'imdb_score' => $movie->imdb_score ?? '',

                                            )
                                        );
                                    } else {
                                        // echo "Not Found";

                                        $db_data = DB::table('contents')->insert(
                                            array(
                                                'title' => $value23->name,
                                                'original_title' => $movie->title ?? '' . " " . $value23->name ?? '',
                                                'content_type' => "tv-season",
                                                'poster' => $movie->poster ?? '',
                                                'short_description' => $value23->overview ?? '',
                                                'content_source' => 'tmdb',
                                                'content_source_id' => $value23->id,
                                                'tmdb_id' => $value23->id,
                                                'season_number' => $value23->season_number ?? '',
                                                'full_path' => $movie->full_path ?? "",
                                                'tmdb_popularity' => $movie->tmdb_popularity ?? '',
                                                'original_release_year' => explode('-', $value23->air_date)[0] ?? '',
                                                'cinema_release_date' => $value23->air_date ?? '',
                                                'original_language' => $movie->original_language ?? "",
                                                'tmdb_score' => $movie->tmdb_score ?? "",
                                                'imdb_score' => $movie->imdb_score ?? '',
                                                'parent_id' => $movie->content_id
                                            )
                                        );
                                    }
                                    // die;

                                    $dbcheck1 = \DB::table('contents')->where('tmdb_id', $value23->id)->where('parent_id', $movie->content_id)->first();
                                    // var_dump($dbcheck1); die;



                                    $data = $this->updateepisodestmdb($value23->season_number, $tmdb_id, $dbcheck1->content_id);
                                }
                            }


                            $check1 = \DB::table('contents')->where('content_id', $movie->content_id)->update($updatedata);

                            if ($check1)
                                return [$movie->content_id, $contents];
                        }
                    }
                }
            }
        } else
					if ($movie)
            $tmdb_id = $movie->tmdb_id;

        else
            return "no show found";










        // get first movie
        // $movie = \DB::table('contents')->where('execute_status', 1)->where('content_type', 'show')->orderBy('created_at', 'desc')->first();
        // var_dump($movie->title);
        // if ($movie){


        // 	$url = "https://api.themoviedb.org/3/tv/$tmdb_id?api_key=f0dc06cd59a97bbddcebd05e902559e0&language=en-US";


        // 	if (isset($url)){
        // 		$genrerequest = curl_init();

        // 		curl_setopt($genrerequest,CURLOPT_RETURNTRANSFER,1);
        // 		curl_setopt($genrerequest, CURLOPT_URL, $url);
        // 		curl_setopt($genrerequest, CURLOPT_SSL_VERIFYHOST, false);
        // 		curl_setopt($genrerequest, CURLOPT_SSL_VERIFYPEER, false);
        // 		$contents = curl_exec($genrerequest);
        // 		$count = 0;
        // 		$contents = json_decode($contents);

        // 		if ($contents){

        // 			$releaseyear = "";
        // 			if (isset($contents->first_air_date)){
        // 				$releaseyear = explode('-', $contents->first_air_date)[0];
        // 				$releasedate = $contents->first_air_date ;
        // 			}


        // 			$updatedata =[];
        // 			$updatedata['execute_status'] = 2;


        // 			if ($movie->total_episodes == "" || $movie->total_episodes == null)  {
        // 				$updatedata['total_episodes'] = $contents->number_of_episodes ?? '0';
        // 			}

        // 			if ($movie->total_seasons == "" || $movie->total_seasons == null)  {
        // 				$updatedata['total_seasons'] = $contents->number_of_seasons ?? '0';
        // 			}

        // 			if (($movie->runtime == "" || $movie->runtime == null) && isset($contents->episode_run_time[0]))  {
        // 				$updatedata['runtime'] = $contents->episode_run_time[0];
        // 			}

        // 			foreach ($contents->networks as $key32 => $value32) {
        // 				$getOTT = \DB::table('providers')->where('technical_name', $value32->name)->first();
        // 				if ($getOTT){
        // 					$provider_id = $getOTT->provider_id;
        // 				}
        // 				else{



        // 					if(!empty($value32->logo_path)){

        // 						$url1 = 'https://image.tmdb.org/t/p/w200'.$value32->logo_path;

        // 						$base_path = public_path();
        // 						$keyname = '/providers'.$value32->logo_path;
        // 						$img1 = $base_path.'/asset'.$keyname;

        // 						file_put_contents($img1, file_get_contents($url1));

        // 					}
        // 					else{
        // 						$keyname= "";
        // 					}
        // 					\DB::table('providers')->insert([
        // 						'profile_id' => $value32->id,
        // 						'title' => $value32->name ?? '',
        // 						'technical_name' => $value32->name ?? '',
        // 						'short_name' => $value32->name ?? '',
        // 						'slug' => $value32->name ?? '',
        // 						'icon_url' => $keyname,
        // 						'content_source' => 'tmdb',
        // 						'content_source_id' => $value32->id,
        // 					]);
        // 					$getOTT = \DB::table('providers')->where('technical_name', $value32->name)->first();
        // 					$provider_id = $getOTT->provider_id;
        // 				}


        // 				$dbcheck = DB::table('content_providers')->where('content_id', $movie->content_id)->where('provider_id', $provider_id)->first();


        // 				if (!$dbcheck){
        // 					$db_data = DB::table('content_providers')->insert(array(
        // 						'content_id' => $movie->content_id,
        // 						'provider_id' => $provider_id ,
        // 						'video_quality' => 'SD',
        // 						'currency' => 'INR',
        // 						'monetization_type' => 'flatrate',
        // 						'retail_price' => '0',
        // 						'isLive' => 1
        // 					));

        // 				}



        // 			}



        // 			$dbcheck = \DB::table('contents')->where('parent_id', $movie->content_id)->get();

        // 			if (count($dbcheck)<1){
        // 				foreach ($contents->seasons as $key32 => $value23) {

        // 					$db_data = DB::table('contents')->insert(
        // 						array(
        // 							'title' => $value23->name,
        // 							'original_title' => $movie->title ?? ''." ".$value23->name ?? '',
        // 							'content_type' => "tv-season",
        // 							'poster' => $movie->poster ?? '',
        // 							'short_description' => $value23->overview ?? '',
        // 							'content_source' => 'tmdb',
        // 							'content_source_id' => $value23->id,
        // 							'season_number' => $value23->season_number ?? '',
        // 							'full_path' => $movie->full_path ?? "",
        // 							'tmdb_popularity' => $movie->tmdb_popularity ?? '',
        // 							'original_release_year' => explode('-',$value23->air_date)[0] ?? '',
        // 							'cinema_release_date' => $value23->air_date ?? '',
        // 							'original_language' => $movie->original_language ?? "",
        // 							'tmdb_score' => $movie->tmdb_score ?? "",
        // 							'imdb_score' => $movie->imdb_score ?? '',
        // 							'parent_id' => $movie->content_id
        // 						));

        // 					$dbcheck1 = \DB::table('contents')->where('content_source_id', $value23->id)->where('content_source', 'tmdb')->where('parent_id', $movie->content_id)->first();



        // 					$data = $this->updateepisodestmdb($value23->season_number, $tmdb_id, $dbcheck1->content_id);
        // 				}
        // 			}
        // 			else
        // 			{
        // 				foreach ($dbcheck as $key => $value23) {
        // 					if ($value23->season_number !=""){
        // 						$data = $this->updateepisodestmdb($value23->season_number, $tmdb_id, $value23->content_id);

        // 					}
        // 				}
        // 			}

        // 			$check1 = \DB::table('contents')->where('content_id', $movie->content_id)->update($updatedata);

        // 			if ($check1)
        // 				return [$movie->content_id, $contents];
        // 		}
        // 	}
        // }
    }








    public function updatetmdbcast($type, $content_id)
    {

        $movie = \DB::table('contents')->where('content_id', $content_id)->first();
        if ($movie->tmdb_id > 0) {
            $tmdb_id = $movie->tmdb_id;
        }
        if ($movie->content_source == 'tmdb') {
            $tmdb_id = $movie->content_source_id;
        }
        // var_dump($movie);
        // die;
        if ($type == 'movie') {
            $url = "https://api.themoviedb.org/3/movie/$tmdb_id/credits?api_key=f0dc06cd59a97bbddcebd05e902559e0&language=en-US";
        } else {
            $url = "https://api.themoviedb.org/3/tv/$tmdb_id/credits?api_key=f0dc06cd59a97bbddcebd05e902559e0&language=en-US";
        }

        // echo $url;
        // die;
        if (isset($url)) {
            $castrequest = curl_init();
            // echo "hi";
            curl_setopt($castrequest, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($castrequest, CURLOPT_URL, $url);
            curl_setopt($castrequest, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($castrequest, CURLOPT_SSL_VERIFYPEER, false);
            $contents = curl_exec($castrequest);
            $count = 0;
            $contents = json_decode($contents);

            $deleteprevious = \DB::table('content_casting')->where('content_id', $content_id)->delete();

            if (!empty($contents) && !empty($contents->cast)) {
                foreach ($contents->cast as $cast) {
                    // var_dump($cast); die;
                    $check = \DB::table('content_casting')->where('person_id', $cast->cast_id ?? $cast->id)->where('content_id', $content_id)->where('name', $cast->name)->first();
                    // var_dump($check);
                    // die;
                    if (!$check) {
                        $insert = \DB::table('content_casting')->insert([
                            'content_id' => $content_id,
                            'person_id' => $cast->cast_id ?? $cast->id,
                            'character_name' => $cast->character ?? '',
                            'name' => $cast->name ?? '',
                            'priority' => $cast->order ?? '',
                            'poster' => $cast->profile_path
                        ]);
                    } else {
                        \DB::table('content_casting')->where('person_id', $cast->cast_id ?? $cast->id)->where('content_id', $content_id)->where('name', $cast->name)->update(array(
                            'character_name' => $cast->character ?? '',
                            'name' => $cast->name ?? '',
                            'priority' => $cast->order ?? '',
                            'poster' => $cast->profile_path
                        ));
                    }
                    $this->getcastbyname($cast->name);
                }
            }
        }
    }


    public function updatetmdbtrailor($type, $content_id)
    {

        $movie = \DB::table('contents')->where('content_id', $content_id)->first();
        // var_dump($movie);
        if ($movie->tmdb_id > 0) {
            $tmdb_id = $movie->tmdb_id;
        }
        if ($movie->content_source == 'tmdb') {
            $tmdb_id = $movie->content_source_id;
        }
        // die;
        if ($type == 'movie') {
            $url = "https://api.themoviedb.org/3/movie/$tmdb_id/videos?api_key=f0dc06cd59a97bbddcebd05e902559e0&language=en-US";
        } else {
            $url = "https://api.themoviedb.org/3/tv/$tmdb_id/videos?api_key=f0dc06cd59a97bbddcebd05e902559e0&language=en-US";
        }

        // echo $url;
        // die;
        if (isset($url)) {
            $trailorrequest = curl_init();
            // echo "hi";
            curl_setopt($trailorrequest, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($trailorrequest, CURLOPT_URL, $url);
            curl_setopt($trailorrequest, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($trailorrequest, CURLOPT_SSL_VERIFYPEER, false);
            $contents = curl_exec($trailorrequest);
            $count = 0;
            $contents = json_decode($contents);

            // var_dump($contents->results); die;
            if (!empty($contents) && !empty($contents->results)) {
                foreach ($contents->results as $trailor) {
                    // var_dump($cast); die;
                    $check = \DB::table('content_clips')->where('external_id', $trailor->key)->where('content_id', $content_id)->first();
                    // var_dump($check);
                    // die;
                    if (!$check) {
                        $newData = DB::table('content_clips')->insert(array(
                            'content_id' => $content_id,
                            'title' => $trailor->name ?? $movie->title,
                            'display_priority' => '1',
                            'external_id' => $trailor->key ?? '',
                            'type' => $trailor->type ?? '',
                            'provider' => $trailor->site ?? '',

                        ));
                    } else {
                        \DB::table('content_clips')->where('external_id', $trailor->key)->where('content_id', $content_id)->update(array(

                            'title' => $trailor->name ?? $movie->title,
                            'display_priority' => '1',
                            'external_id' => $trailor->key ?? '',
                            'type' => $trailor->type ?? '',
                            'provider' => $trailor->site ?? '',
                        ));
                    }
                }
            }
        }
    }


    public function updatetmdbmoviecast(Request $request)
    {

        // get first movie
        $movie = \DB::table('contents')->where('tmdb_id', '!=', 0)->where('execute_status', 2)->where('content_type', 'movie')->orderBy('created_at', 'desc')->first();
        // var_dump($movie->title);

        // change the status to 1 so that no other job works on it
        $movie3 = \DB::table('contents')->where('content_id', $movie->content_id)->update(['execute_status' => 3]);


        //check if movie exists on tmdb
        $movie->title = str_replace(' ', '%20', $movie->title);
        // dd($movie);
        $url = "https://api.themoviedb.org/3/movie/$movie->tmdb_id/credits?api_key=f0dc06cd59a97bbddcebd05e902559e0&language=en-US";
        // echo $url;
        if (isset($url)) {
            $genrerequest = curl_init();
            // echo "hi";
            curl_setopt($genrerequest, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($genrerequest, CURLOPT_URL, $url);
            curl_setopt($genrerequest, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($genrerequest, CURLOPT_SSL_VERIFYPEER, false);
            $contents = curl_exec($genrerequest);
            $count = 0;
            $contents = json_decode($contents);
            // dd($contents);
            // dd($url);
            if ($contents) {
                if (isset($contents->cast))

                    foreach ($contents->cast as $key => $value) {
                        $castcheck = \DB::table('content_casting')->where('content_id', $movie->content_id)->where('person_id', $value->cast_id)->first();
                        if (!$castcheck) {
                            $insert = \DB::table('content_casting')->insert([
                                'content_id' => $movie->content_id,
                                'person_id' => $value->cast_id,
                                'character_name' => $value->character ?? '',
                                'name' => $value->name ?? '',
                                'priority' => $value->order ?? '',
                                'poster' => $value->profile_path
                            ]);
                        }
                    }
            }
        }
    }


    public function updateepisodestmdb($season_number, $tmdb_id, $content_id)
    {
        echo $tmdb_id . " " . $season_number;
        $url = "https://api.themoviedb.org/3/tv/$tmdb_id/season/$season_number?api_key=f0dc06cd59a97bbddcebd05e902559e0&language=en-US";

        // echo $url; die;

        if (isset($url)) {
            $genrerequest = curl_init();
            // echo "hi";
            curl_setopt($genrerequest, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($genrerequest, CURLOPT_URL, $url);
            curl_setopt($genrerequest, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($genrerequest, CURLOPT_SSL_VERIFYPEER, false);
            $contents = curl_exec($genrerequest);
            $count = 0;
            $contents = json_decode($contents);
            // dd($contents);

            if (isset($contents->episodes)) {

                foreach ($contents->episodes as $key => $episode) {
                    $dbcheck = \DB::table('contents')->where('content_source_id', $episode->id)->where('content_source', 'tmdb')->where('content_source_id', $episode->id)->first();
                    // $dbcheck = \DB::table('contents')->where('content_source_id', $episode->id)->where('content_source', 'tmdb')->where('parent_id', $content_id)->first();
                    // dd($dbcheck);
                    if (!$dbcheck) {
                        $db_data1 = DB::table('contents')->insert(
                            array(
                                'title' => $episode->name ?? '',
                                'original_title' => $episode->name ?? '',
                                'content_type' => "tv-episode",
                                'poster' => '',
                                'content_source' => 'tmdb',
                                'content_source_id' => $episode->id,
                                'short_description' => $episode->overview ?? "",
                                'episode_number' => $episode->episode_number ?? '',
                                'season_number' => $episode->season_number ?? '',
                                'original_release_year' => $episode->original_release_year ?? '',
                                'cinema_release_date' => $episode->air_date ?? '',
                                'parent_id' => $content_id
                            )
                        );
                    } else {
                        $dbcheck = \DB::table('contents')->where('content_source_id', $episode->id)->where('content_source', 'tmdb')->update(array(
                            'title' => $episode->name ?? '',
                            'original_title' => $episode->name ?? '',
                            'content_type' => "tv-episode",
                            'poster' => '',
                            'content_source' => 'tmdb',
                            'content_source_id' => $episode->id,
                            'short_description' => $episode->overview ?? "",
                            'episode_number' => $episode->episode_number ?? '',
                            'season_number' => $episode->season_number ?? '',
                            'original_release_year' => $episode->original_release_year ?? '',
                            'cinema_release_date' => $episode->air_date ?? '',
                            'parent_id' => $content_id
                        ));
                    }
                }
            }
        }
    }





    public function changeMovieStatus(Request $request)
    {
        if (isset($request->content_id)){
            $contents = Content::where('content_id', $request->content_id)->get();
        }
        else
        $contents = Content::where('status', 0)->get();

        foreach ($contents as $key => $value) {


            if ($value->content_source == 'justwatch'){
                $this->verifyfromjustwatch($value);
                $checkagain = \DB::table('contents')->where('content_id', $value->content_id)->first();

                if ($checkagain){
                    if ($checkagain->title != "" && $checkagain->poster != "" && $checkagain->short_description != ""){
                        $checkagain = \DB::table('contents')->where('content_id', $value->content_id)->update(['status' => 1]);
                    }
                }
                // var_dump($checkagain);
            }

            if ($value->content_source == 'mxplayer'){
                $this->verifyfrommxplayer($value);
                $checkagain = \DB::table('contents')->where('content_id', $value->content_id)->first();

                if ($checkagain){
                    if ($checkagain->title != "" && $checkagain->poster != "" && $checkagain->short_description != ""){
                        $checkagain = \DB::table('contents')->where('content_id', $value->content_id)->update(['status' => 1]);
                    }
                }
                // var_dump($checkagain);
            }

            // dd($value->content_source);




            // dd($value->title);
        }
        echo count($contents);
        // dd($contents);
    }
}
