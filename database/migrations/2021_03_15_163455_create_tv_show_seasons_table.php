<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTvShowSeasonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tv_show_seasons', function (Blueprint $table) {
            $table->increments('id');
            $table->string('season_name');
            $table->string('season_number');
            $table->string('show_id');
            $table->string('start_date')->nullable();
            $table->string('backdrop_path')->nullable();
            $table->string('poster');
            $table->string('episode_count');
            $table->string('tmdb_id');
            $table->string('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tv_show_seasons');
    }
}
