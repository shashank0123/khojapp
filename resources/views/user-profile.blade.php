<?php 

if (isset(auth()->user()->profile_pic))
$profile_pic = '/images/user/'.auth()->user()->profile_pic;
else {
	$profile_pic = '/images/default_user.png';
}

$username = explode(' ',Auth::user()->name);

?>


@extends('layouts.khoj_new')

@section('content')
<style>
	 #clickBro { display: none; }
#file { display: none; }
.breadcumb li { display: inline-block !important; }
</style>

<div class="hero user-hero">
	@if($errors->any())
	<div class="alert alert-danger" style="position: absolute; top: 2px">
		@foreach($errors->all() as $error)
		<li style="color: #fff">{{ $error }}</li>
		@endforeach
	</div>
	@endif

	@if($message = Session::get('message'))
	<div class="btn btn-success" style="width: 100%; position: absolute; top: 2px">
		<p style="color: #fff">{{ $message }}</p>
	</div>
	@endif
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="hero-ct">
					<h1>{{$user->name}}</h1>
					<ul class="breadcumb">
						<li class="active"><a href="/">Home</a></li>
						<li> <span class="ion-ios-arrow-right"></span>Profile</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="page-single">
	<div class="container">
		<div class="row ipad-width">
			<div class="col-md-3 col-sm-12 col-xs-12">
				<div class="user-information">
					<div class="user-img">

						<form method="post" action="/upload-image" enctype="multipart/form-data" id="upload_image">
							@csrf
							<input type="hidden" name="member_id" id="uid" value="{{auth()->user()->id}}">
						<a href="#"><img src="{{$profile_pic}}" alt="" style="width: 80%; margin: auto"><br></a>
							{{-- @if(!empty($profile->profimage))
							<span id="uploaded_image">
								<img src="/assetsss/images/AdminProduct/{{$profile->profimage}}" width="150px" height="150px">
							</span>
							@else
							<span id="uploaded_image"></span>
							@endif --}}
							<br>
							{{-- <img src="{{ asset('images/product/fashion-12.jpg') }}" style="width:100px;height:100px;border-radius:50%;margin: auto;display: block; margin-bottom:15px"> --}}
							<label for="file"  class="redbtn">Change Pic</label>
							<input type="file" name="file" id="file" onchange='submitMe()'/>
							<input type="submit" name="submit" id="clickBro" />
						</form>
						
						{{-- <a href="#" class="redbtn">Change avatar</a> --}}





						{{-- <a href="#"><img src="{{$profile_pic}}" alt="" style="width: 80%; margin: auto"><br></a> --}}
					</div>
					<div class="user-fav">
						<p onclick="showdropdown1()" style="cursor: pointer;">Account Details&nbsp;&nbsp;<i class="fa fa-angle-down"></i></p>
						<ul style="display: none;" id="dropdown1">
							<li><a href="/favourites/{{auth()->user()->id}}">&nbsp;&nbsp;&nbsp;Favourite movies</a></li>
							{{-- <li><a href="/playlists/{{auth()->user()->id}}">&nbsp;&nbsp;&nbsp;My Playlist</a></li> --}}
							<li><a href="/watchlists/{{auth()->user()->id}}">&nbsp;&nbsp;&nbsp;My WatchList</a></li>
						</ul>
					</div>
					<div class="user-fav">
						<p onclick="showdropdown2()" style="cursor: pointer;">Settings&nbsp;&nbsp;<i class="fa fa-angle-down"></i></p>
						<ul style="display: none;" id="dropdown2">
							<li class="active"><a href="/profile">&nbsp;&nbsp;&nbsp;Profile</a></li>
							<li><a href="/reset-password">&nbsp;&nbsp;&nbsp;Change password</a></li>
							
						</ul>
					</div>

					<div class="user-fav">
						{{-- <p>Settings</p> --}}
						<ul>
							<li><a href="{{ route('logout') }}"
								onclick="event.preventDefault();
								document.getElementById('logout-form').submit();">Log out</a>
								<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
									{{ csrf_field() }}
								</form>
							</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="col-md-9 col-sm-12 col-xs-12">
				<div class="form-style-1 user-pro" action="#">
					<div class="row">
					<form action="/update-profile" method="get" class="user">
						<h4>Profile details</h4>
						@csrf
						<input type="hidden" name="uid" value="{{auth()->user()->id}}">
						<div class="row">
							<div class="col-md-6 form-it">
								<label>Username</label>
								<input type="text" placeholder="edwardkennedy" name="username" value="{{ Auth::user()->name }}">
							</div>
							<div class="col-md-6 form-it">
								<label>Email Address</label>
								<input type="text" placeholder="edward@kennedy.com" name="email" value="{{ Auth::user()->email }}" readonly style="color: #040404">
							</div>
						</div>
						
						<div class="row">
							<div class="col-md-6 form-it">
								<label>Gender</label>
								<select name="gender" id="gender">							
									<option value="Female" @if(!empty($profile)) @if($profile->gender == 'Female'){{'selected'}}@endif @endif>Female</option>
									<option value="Male" @if(!empty($profile)) @if($profile->gender == 'Male'){{'selected'}}@endif @endif>Male</option>						
								</select>
							</div>
							<div class="col-md-6 form-it">
								<label>Mobile</label>
								<input type="text" placeholder="*******999" name="contact" value="{{auth()->user()->mobile}}">
								
							</div>
						</div>
						<div class="row">
							<div class="col-md-6 form-it">
								<label>Address</label>
								<input type="text" placeholder="Enter Your Address" name="address" value="<?php if(!empty($profile))echo $profile->address; ?>">
								
							</div>
							<div class="col-md-6 form-it">
								<label>Country</label>
								<select name="country" id="country">
									
									<option @if (isset($profile->country) && $profile->country=='0')
		 								  	{{ 'selected' }}
		 								  @endif value="0">All Countries</option>
		 								  <option @if (isset($profile->country) && $profile->country=='1')
		 								  	{{ 'selected' }}
		 								  @endif value="1">Argentina</option>
		 								  <option @if (isset($profile->country) && $profile->country=='2')
		 								  	{{ 'selected' }}
		 								  @endif value="2">Australia</option>
		 								  <option @if (isset($profile->country) && $profile->country=='3')
		 								  	{{ 'selected' }}
		 								  @endif value="3">Bahamas</option>
		 								  <option @if (isset($profile->country) && $profile->country=='4')
		 								  	{{ 'selected' }}
		 								  @endif value="4">Belgium</option>
		 								  <option @if (isset($profile->country) && $profile->country=='5')
		 								  	{{ 'selected' }}
		 								  @endif value="5">Brazil</option>
		 								  <option @if (isset($profile->country) && $profile->country=='6')
		 								  	{{ 'selected' }}
		 								  @endif value="6">Canada</option>
		 								  <option @if (isset($profile->country) && $profile->country=='7')
		 								  	{{ 'selected' }}
		 								  @endif value="7">Chile</option>
		 								  <option @if (isset($profile->country) && $profile->country=='8')
		 								  	{{ 'selected' }}
		 								  @endif value="8">China</option>
		 								  <option @if (isset($profile->country) && $profile->country=='9')
		 								  	{{ 'selected' }}
		 								  @endif value="9">Denmark</option>
		 								  <option @if (isset($profile->country) && $profile->country=='10')
		 								  	{{ 'selected' }}
		 								  @endif value="10">Ecuador</option>
		 								  <option @if (isset($profile->country) && $profile->country=='11')
		 								  	{{ 'selected' }}
		 								  @endif value="11">France</option>
		 								  <option @if (isset($profile->country) && $profile->country=='12')
		 								  	{{ 'selected' }}
		 								  @endif value="12">Germany</option>
		 								  <option @if (isset($profile->country) && $profile->country=='13')
		 								  	{{ 'selected' }}
		 								  @endif value="13">Greece</option>
		 								  <option @if (isset($profile->country) && $profile->country=='14')
		 								  	{{ 'selected' }}
		 								  @endif value="14">Guatemala</option>
		 								  <option @if (isset($profile->country) && $profile->country=='15')
		 								  	{{ 'selected' }}
		 								  @endif value="15">Italy</option>
		 								  <option @if (isset($profile->country) && $profile->country=='27')
		 								  	{{ 'selected' }}
		 								  @endif value="27">India</option>
		 								  <option @if (isset($profile->country) && $profile->country=='16')
		 								  	{{ 'selected' }}
		 								  @endif value="16">Japan</option>
		 								  <option @if (isset($profile->country) && $profile->country=='17')
		 								  	{{ 'selected' }}
		 								  @endif value="17">asdfasdf</option>
		 								  <option @if (isset($profile->country) && $profile->country=='18')
		 								  	{{ 'selected' }}
		 								  @endif value="18">Korea</option>
		 								  <option @if (isset($profile->country) && $profile->country=='19')
		 								  	{{ 'selected' }}
		 								  @endif value="19">Malaysia</option>
		 								  <option @if (isset($profile->country) && $profile->country=='20')
		 								  	{{ 'selected' }}
		 								  @endif value="20">Monaco</option>
		 								  <option @if (isset($profile->country) && $profile->country=='21')
		 								  	{{ 'selected' }}
		 								  @endif value="21">Morocco</option>
		 								  <option @if (isset($profile->country) && $profile->country=='22')
		 								  	{{ 'selected' }}
		 								  @endif value="22">New Zealand</option>
		 								  <option @if (isset($profile->country) && $profile->country=='23')
		 								  	{{ 'selected' }}
		 								  @endif value="23">Panama</option>
		 								  <option @if (isset($profile->country) && $profile->country=='24')
		 								  	{{ 'selected' }}
		 								  @endif value="24">Portugal</option>
		 								  <option @if (isset($profile->country) && $profile->country=='25')
		 								  	{{ 'selected' }}
		 								  @endif value="25">Russia</option>
		 								  <option @if (isset($profile->country) && $profile->country=='26')
		 								  	{{ 'selected' }}
		 								  @endif value="26">United Kingdom</option>
		 								  
									{{-- @if(isset($countries))
									@foreach($countries as $country)
									<option value="{{$country->country}}"  @if(!empty($profile)) @if($profile->country == $country->country){{'selected'}}@endif @endif>{{$country->country}}</option>
									@endforeach
									@endif --}}
									
								</select>
							</div>
						</div>
						<div class="row">
							<div class="col-md-2">
								<input class="submit" type="submit" value="save">
							</div>
						</div>	
					</form>
					
				</div>

				</div>
			</div>
		</div>
	</div>
</div>
<!-- footer section-->

<script>
	function submitMe() {
    $('#file').click();
    $("#clickBro").click();
  }

  function showdropdown1() {
    $('#dropdown1').toggle();
   
  }

  function showdropdown2() {
    $('#dropdown2').toggle();
   
  }

  

</script>

@endsection