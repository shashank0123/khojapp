<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFeautreToMoviesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
 

public function up()
    {
        Schema::table('movies', function (Blueprint $table) {
            $table->string('featured')->nullable();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
