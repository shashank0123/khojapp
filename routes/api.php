<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::get('/register', 'AuthController@register');

Route::get('/login', 'AuthController@login');
Route::get('/logout', 'AuthController@logout');
Route::get('/trending', 'KhojApiController@trending');
Route::get('/upcoming', 'KhojApiController@upcoming');
Route::get('/newest', 'KhojApiController@newest');
Route::get('/viral', 'KhojApiController@viral_videos');
Route::get('/latest', 'KhojApiController@latest');
Route::get('/free_videos', 'KhojApiController@free_videos');
Route::get('/search', 'KhojApiController@dynamic_search');
Route::get('/movie_details', 'KhojApiController@movie_details');
Route::get('/similar_movie', 'KhojApiController@similar_movie');
Route::get('/show_seasons', 'KhojApiController@show_seasons');
Route::get('/fullsearch', 'KhojApiController@getSearch');
Route::get('/casts', 'KhojApiController@getCastDetail');
Route::get('/banners', 'KhojApiController@getbanners');
Route::get('/get_filters', 'KhojApiController@get_filters');
Route::get('/submitfeedback', 'KhojApiController@submitFeedback');






Route::get('/react/slider', 'KhojVodiController@getSlider');
Route::get('/react/movies', 'KhojVodiController@getMovies');
Route::get('/react/tvshows', 'KhojVodiController@getShows');
Route::get('/react/movies/genre/{genre}', 'KhojVodiController@getMovieByGenre');
Route::get('/react/tvshows/genre/{genre}', 'KhojVodiController@getShowByGenre');
Route::get('/react/movies/featured', 'KhojVodiController@getMovieByFeatured');
Route::get('/react/tvshows/featured', 'KhojVodiController@getShowByFeatured');
Route::get('/react/genre/{content_id}', 'KhojVodiController@getGenreByContentId');
Route::get('/react/watchlist/{content_id}', 'KhojVodiController@getOTTByContentId');
Route::get('/react/trailer/{content_id}', 'KhojVodiController@getTrailerByContentId');







Route::group([    
    'namespace' => 'Auth',    
    'middleware' => 'api',    
    'prefix' => 'password'
], function () {    
    Route::post('create', 'ResetPasswordController@create');
    Route::get('find/{token}', 'ResetPasswordController@find');
    Route::post('reset', 'ResetPasswordController@reset');
});

Route::group(['middleware' => ['jwt.auth']], function() {  
	Route::get('/add_to_favourite', 'KhojApiController@add_to_favourite');
	Route::get('/add_to_watchlist', 'KhojApiController@add_to_watchlist');
	Route::get('/favourite', 'KhojApiController@favourite');
	Route::get('/watchlist', 'KhojApiController@watchlist');
	Route::get('/savefilter', 'KhojApiController@watchlist');
	Route::get('/getfilter', 'KhojApiController@watchlist');

	});