<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB, StdClass;
use App\Content;
use App\Favourite;
use App\WatchList;
use App\FeaturedImage;
use App\SearchKeyword;
use App\Feedback;;
use App\Genre;
use JWTFactory;
use JWTAuth;

class KhojApiController extends Controller
{
	function trending(Request $request){ 

		if (isset($request->limit) && $request->limit > 0){
			$limit = $request->limit;
		}
		else
			$limit = 30;

		$date = date('Y-m-d');
	$page = 0;
	$langs = "";
	$rated = "";
	$sort = "";
	$field = "";
	$sort_type = "";
	$provider = "";
	$genre = "";
	$minr=0;
	$maxr=10; 
	$type="all";
	$tab = 'Trending';

  // $miny = DB::table('contents')->where('original_release_year','>','1900')->min('original_release_year');
	$miny = '0000-00-00';
	$maxy = date('y-m-d');;

	if(isset($_GET['page'])){
		$page = $_GET['page']-1;
	}  

	if(isset($_GET['lang'])){  $langs = $_GET['lang'];  }
	if(isset($_GET['provider'])){  $provider = explode('_',$_GET['provider'])[1];  }

	if(isset($_GET['sort'])){
		$sort = $_GET['sort'];
		if($sort == "alpha"){
			$field = 'title';
			$sort_type = 'ASC';
		}
		elseif($sort == 'rate'){
			$field = 'tmdb_score';
			$sort_type = 'DESC';
		}
		elseif($sort == 'year'){
			$field = 'cinema_release_date';
			$sort_type = 'DESC';
		}
		elseif($sort == 'imdb_score'){
			$field = 'imdb_score';
			$sort_type = 'DESC';
		}
		else{
			$field = 'content_id';
			$sort_type = 'DESC';
		}
	}

	if((isset($_GET['sort']) && isset($_GET['genre'])) || (isset($_GET['sort']) && isset($_GET['provider']))){
		$sort = $_GET['sort'];
		if($sort == "alpha"){
			$field = 'contents.title';
			$sort_type = 'ASC';
		}
		elseif($sort == 'rate'){
			$field = 'contents.tmdb_score';
			$sort_type = 'DESC';
		}
		elseif($sort == 'year'){
			$field = 'contents.cinema_release_date';
			$sort_type = 'DESC';
		}
		elseif($sort == 'imdb_score'){
			$field = 'contents.imdb_score';
			$sort_type = 'DESC';
		}
		else{
			$field = 'contents.content_id';
			$sort_type = 'DESC';
		}
	}

	if(isset($_GET['rated'])){
		$rated = $_GET['rated'];
	}

	if(isset($_GET['searchtype'])){
		$type = $_GET['searchtype'];
	}

	if(isset($_GET['genre'])){
		$genre = $_GET['genre'];
	}

	if(isset($_GET['minr'])){
		$minr = $_GET['minr'];
	}
	if(isset($_GET['maxr'])){
		$maxr = $_GET['maxr'];    
	}

	if(isset($_GET['miny'])){
		$miny = $_GET['miny']."-01-01";
	}
	// if(isset($_GET['maxy'])){
	// 	$maxy = $_GET['maxy']."-12-31";
	// }


	if($type == 'all'){

		if($provider == ""){

			if($sort == ""){


				if($langs!="" && $rated!="" && $genre!=""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('content_genres')
						->leftJoin('contents','content_genres.content_id','contents.content_id')
						->where('content_genres.genre_id',$genre)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.age_certification',$rated)
						->where('contents.original_language',$langs)
						->where('contents.content_type','!=','tv-show')
						->orderBy('contents.click_count','DESC')
						->paginate($limit);
					}
					else{
						$contents = DB::table('content_genres')
						->leftJoin('contents','content_genres.content_id','contents.content_id')
						->where('content_genres.genre_id',$genre)
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.age_certification',$rated)
						->where('contents.original_language',$langs)
						->where('contents.content_type','!=','tv-show')
						->orderBy('contents.click_count','DESC')
						->paginate($limit);
					}   
				}
				elseif($langs!="" && $rated != ""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('contents')
						->where('original_language',$langs)
						->where('age_certification',$rated)
						->where('cinema_release_date','<=',$maxy)
						->where('cinema_release_date','>=',$miny)     
						->where('content_type','!=','tv-show')                  
						->orderBy('click_count','DESC')
						->paginate($limit);
					}
					else{
						$contents = DB::table('contents')
						->where('original_language',$langs)
						->where('age_certification',$rated)
						->where('tmdb_score','<=',$maxr)
						->where('tmdb_score','>=',$minr)
						->where('cinema_release_date','<=',$maxy)
						->where('cinema_release_date','>=',$miny)     
						->where('content_type','!=','tv-show')                  
						->orderBy('click_count','DESC')
						->paginate($limit);
					}           
				}
				elseif($rated != "" && $genre!=""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('content_genres')
						->leftJoin('contents','content_genres.content_id','contents.content_id')
						->where('content_genres.genre_id',$genre)
						->where('contents.age_certification',$rated)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)    
						->where('contents.content_type','!=','tv-show')   
						->orderBy('contents.click_count','DESC')
						->paginate($limit);
					}
					else{
						$contents = DB::table('content_genres')
						->leftJoin('contents','content_genres.content_id','contents.content_id')
						->where('content_genres.genre_id',$genre)
						->where('contents.age_certification',$rated)
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)    
						->where('contents.content_type','!=','tv-show')   
						->orderBy('contents.click_count','DESC')
						->paginate($limit);
					}   
				}
				elseif($langs != "" && $genre!=""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('content_genres')
						->leftJoin('contents','content_genres.content_id','contents.content_id')
						->where('content_genres.genre_id',$genre)
						->where('contents.original_language',$langs)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.content_type','!=','tv-show')
						->orderBy('contents.click_count','DESC')
						->paginate($limit);
					}
					else{
						$contents = DB::table('content_genres')
						->leftJoin('contents','content_genres.content_id','contents.content_id')
						->where('content_genres.genre_id',$genre)
						->where('contents.original_language',$langs)
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.content_type','!=','tv-show')
						->orderBy('contents.click_count','DESC')
						->paginate($limit);
					}            
				}
				elseif($langs != ""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('contents')
						->where('original_language',$langs)
						->where('cinema_release_date','<=',$maxy)
						->where('cinema_release_date','>=',$miny)
						->where('content_type','!=','tv-show')
						->orderBy('click_count','DESC')
						->paginate($limit);
					}
					else{
						$contents = DB::table('contents')
						->where('original_language',$langs)
						->where('tmdb_score','<=',$maxr)
						->where('tmdb_score','>=',$minr)
						->where('cinema_release_date','<=',$maxy)
						->where('cinema_release_date','>=',$miny)
						->where('content_type','!=','tv-show')
						->orderBy('click_count','DESC')
						->paginate($limit);
					}           
				}
				elseif($genre!=""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('content_genres')
						->leftJoin('contents','content_genres.content_id','contents.content_id')
						->where('content_genres.genre_id',$genre)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.content_type','!=','tv-show')
						->orderBy('contents.click_count','DESC')
						->paginate($limit);
					}
					else{
						$contents = DB::table('content_genres')
						->leftJoin('contents','content_genres.content_id','contents.content_id')
						->where('content_genres.genre_id',$genre)
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.content_type','!=','tv-show')
						->orderBy('contents.click_count','DESC')
						->paginate($limit);
					}            
				}
				elseif($rated != ""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('contents')
						->where('age_certification',$rated)                
						->where('cinema_release_date','<=',$maxy)
						->where('cinema_release_date','>=',$miny)
						->where('content_type','!=','tv-show')
						->orderBy('click_count','DESC')
						->paginate($limit);
					}
					else{
						$contents = DB::table('contents')
						->where('age_certification',$rated)                
						->where('tmdb_score','<=',$maxr)
						->where('tmdb_score','>=',$minr)
						->where('cinema_release_date','<=',$maxy)
						->where('cinema_release_date','>=',$miny)
						->where('content_type','!=','tv-show')
						->orderBy('click_count','DESC')
						->paginate($limit);
					}            
				}
				else{
					if($minr==0 && $maxr==10){
						$contents = DB::table('contents')
						->where('cinema_release_date','<=',$maxy)
						->where('cinema_release_date','>=',$miny)
						->where('content_type','!=','tv-show')
						->orderBy('click_count','DESC')
						->paginate($limit);
					}
					else{
						$contents = DB::table('contents')
						->where('tmdb_score','<=',$maxr)
						->where('tmdb_score','>=',$minr)
						->where('cinema_release_date','<=',$maxy)
						->where('cinema_release_date','>=',$miny)
						->where('content_type','!=','tv-show')
						->orderBy('click_count','DESC')
						->paginate($limit);
					}  
				}
			}
			else{

				if($langs!="" && $rated!="" && $genre!=""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('content_genres')
						->leftJoin('contents','content_genres.content_id','contents.content_id')
						->where('content_genres.genre_id',$genre)
						->where('contents.age_certification',$rated)
						->where('contents.original_language',$langs)
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.content_type','!=','tv-show')
						->orderBy('contents.click_count','DESC')
						->orderBy($field,$sort_type)
						->paginate($limit);
					}
					else{
						$contents = DB::table('content_genres')
						->leftJoin('contents','content_genres.content_id','contents.content_id')
						->where('content_genres.genre_id',$genre)
						->where('contents.age_certification',$rated)
						->where('contents.original_language',$langs)
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.content_type','!=','tv-show')
						->orderBy('contents.click_count','DESC')
						->orderBy($field,$sort_type)
						->paginate($limit);
					}          
				}
				elseif($langs!="" && $rated != ""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('contents')
						->where('original_language',$langs)
						->where('age_certification',$rated)                
						->where('cinema_release_date','<=',$maxy)
						->where('cinema_release_date','>=',$miny)
						->where('content_type','!=','tv-show')
						->orderBy('click_count','DESC')
						->orderBy($field,$sort_type)
						->paginate($limit); 
					}
					else{
						$contents = DB::table('contents')
						->where('original_language',$langs)
						->where('age_certification',$rated)                
						->where('tmdb_score','<=',$maxr)
						->where('tmdb_score','>=',$minr)
						->where('cinema_release_date','<=',$maxy)
						->where('cinema_release_date','>=',$miny)
						->where('content_type','!=','tv-show')
						->orderBy('click_count','DESC')
						->orderBy($field,$sort_type)
						->paginate($limit); 
					}        
				}
				elseif($rated != "" && $genre!=""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('content_genres')
						->leftJoin('contents','content_genres.content_id','contents.content_id')
						->where('content_genres.genre_id',$genre)
						->where('contents.age_certification',$rated)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.content_type','!=','tv-show')
						->orderBy('contents.click_count','DESC')
						->orderBy($field,$sort_type)
						->paginate($limit);
					}
					else{
						$contents = DB::table('content_genres')
						->leftJoin('contents','content_genres.content_id','contents.content_id')
						->where('content_genres.genre_id',$genre)
						->where('contents.age_certification',$rated)
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.content_type','!=','tv-show')
						->orderBy('contents.click_count','DESC')
						->orderBy($field,$sort_type)
						->paginate($limit);
					}          
				}
				elseif($langs != "" && $genre!=""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('content_genres')
						->leftJoin('contents','content_genres.content_id','contents.content_id')
						->where('content_genres.genre_id',$genre)
						->where('contents.original_language',$langs)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.content_type','!=','tv-show')
						->orderBy('contents.click_count','DESC')
						->orderBy($field,$sort_type)
						->paginate($limit);
					}
					else{
						$contents = DB::table('content_genres')
						->leftJoin('contents','content_genres.content_id','contents.content_id')
						->where('content_genres.genre_id',$genre)
						->where('contents.original_language',$langs)
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.content_type','!=','tv-show')
						->orderBy('contents.click_count','DESC')
						->orderBy($field,$sort_type)
						->paginate($limit);
					}          
				}
				elseif($langs != ""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('contents')
						->where('original_language',$langs)
						->where('cinema_release_date','<=',$maxy)
						->where('cinema_release_date','>=',$miny)
						->where('content_type','!=','tv-show')
						->orderBy('click_count','DESC')
						->orderBy($field,$sort_type)
						->paginate($limit);
					}
					else{
						$contents = DB::table('contents')
						->where('original_language',$langs)
						->where('tmdb_score','<=',$maxr)
						->where('tmdb_score','>=',$minr)
						->where('cinema_release_date','<=',$maxy)
						->where('cinema_release_date','>=',$miny)
						->where('content_type','!=','tv-show')
						->orderBy('click_count','DESC')
						->orderBy($field,$sort_type)
						->paginate($limit);
					}         
				}
				elseif($genre!=""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('content_genres')
						->leftJoin('contents','content_genres.content_id','contents.content_id')
						->where('content_genres.genre_id',$genre)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.content_type','!=','tv-show')
						->orderBy('contents.click_count','DESC')
						->orderBy($field,$sort_type)
						->paginate($limit);
					}
					else{
						$contents = DB::table('content_genres')
						->leftJoin('contents','content_genres.content_id','contents.content_id')
						->where('content_genres.genre_id',$genre)
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.content_type','!=','tv-show')
						->orderBy('contents.click_count','DESC')
						->orderBy($field,$sort_type)
						->paginate($limit);
					}   
				}
				elseif($rated != ""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('contents')
						->where('age_certification',$rated)                
						->where('cinema_release_date','<=',$maxy)
						->where('cinema_release_date','>=',$miny)
						->where('content_type','!=','tv-show')
						->orderBy('click_count','DESC')
						->orderBy($field,$sort_type)
						->paginate($limit);
					}
					else{
						$contents = DB::table('contents')
						->where('age_certification',$rated)                
						->where('tmdb_score','<=',$maxr)
						->where('tmdb_score','>=',$minr)
						->where('cinema_release_date','<=',$maxy)
						->where('cinema_release_date','>=',$miny)
						->where('content_type','!=','tv-show')
						->orderBy('click_count','DESC')
						->orderBy($field,$sort_type)
						->paginate($limit);
					}          
				}
				else{
					if($minr==0 && $maxr==10){
						$contents = DB::table('contents')
						->where('cinema_release_date','<=',$maxy)
						->where('cinema_release_date','>=',$miny)
						->where('content_type','!=','tv-show')
						->orderBy('click_count','DESC')
						->orderBy($field,$sort_type)
						->paginate($limit);  
					}
					else{
						$contents = DB::table('contents')
						->where('tmdb_score','<=',$maxr)
						->where('tmdb_score','>=',$minr)
						->where('cinema_release_date','<=',$maxy)
						->where('cinema_release_date','>=',$miny)
						->where('content_type','!=','tv-show')
						->orderBy('click_count','DESC')
						->orderBy($field,$sort_type)
						->paginate($limit);  
					}
				}
			}
		}

  //When Provider Not NULL
		else{
			if($sort == ""){
				if($langs!="" && $rated!="" && $genre!=""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->leftJoin('content_genres','content_genres.content_id','contents.content_id')
						->where('content_providers.provider_id',$provider)
						->where('content_genres.genre_id',$genre)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.age_certification',$rated)
						->where('contents.original_language',$langs)
						->where('contents.content_type','!=','tv-show')
						->orderBy('contents.click_count','DESC')
						->orderBy('content_genres.content_id','DESC')
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate($limit);
					}
					else{
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->leftJoin('content_genres','content_genres.content_id','contents.content_id')
						->where('content_providers.provider_id',$provider)
						->where('content_genres.genre_id',$genre)
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.age_certification',$rated)
						->where('contents.original_language',$langs)
						->where('contents.content_type','!=','tv-show')
						->orderBy('contents.click_count','DESC')
						->orderBy('content_genres.content_id','DESC')
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate($limit);
					}   
				}
				elseif($langs!="" && $rated != ""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->where('content_providers.provider_id',$provider)
						->where('contents.original_language',$langs)
						->where('contents.age_certification',$rated)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)     
						->where('contents.content_type','!=','tv-show')  
						->orderBy('contents.click_count','DESC')
						->select('contents.*')
						->distinct('contents.content_id')               
						->paginate($limit);
					}
					else{
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->where('content_providers.provider_id',$provider)
						->where('contents.original_language',$langs)
						->where('contents.age_certification',$rated)
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)     
						->where('contents.content_type','!=','tv-show')   
						->orderBy('contents.click_count','DESC')
						->select('contents.*')
						->distinct('contents.content_id')              
						->paginate($limit);
					}           
				}
				elseif($rated != "" && $genre!=""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->leftJoin('content_genres','content_genres.content_id','contents.content_id')
						->where('content_providers.provider_id',$provider)
						->where('content_genres.genre_id',$genre)
						->where('contents.age_certification',$rated)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.content_type','!=','tv-show')
						->orderBy('contents.click_count','DESC')
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate($limit);
					}
					else{
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->leftJoin('content_genres','content_genres.content_id','contents.content_id')
						->where('content_providers.provider_id',$provider)
						->where('content_genres.genre_id',$genre)
						->where('contents.age_certification',$rated)
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.content_type','!=','tv-show')
						->orderBy('contents.click_count','DESC')
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate($limit);
					}   
				}
				elseif($langs != "" && $genre!=""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->leftJoin('content_genres','content_genres.content_id','contents.content_id')
						->where('content_providers.provider_id',$provider)
						->where('content_genres.genre_id',$genre)
						->where('contents.original_language',$langs)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.content_type','!=','tv-show')
						->orderBy('contents.click_count','DESC')
						->orderBy('content_genres.content_id','DESC')
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate($limit);
					}
					else{
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->leftJoin('content_genres','content_genres.content_id','contents.content_id')
						->where('content_providers.provider_id',$provider)
						->where('content_genres.genre_id',$genre)
						->where('contents.original_language',$langs)
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.content_type','!=','tv-show')
						->orderBy('contents.click_count','DESC')
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate($limit);
					}            
				}
				elseif($langs != ""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->where('content_providers.provider_id',$provider)
						->where('contents.original_language',$langs)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)     
						->where('contents.content_type','!=','tv-show')     
						->orderBy('contents.click_count','DESC')
						->select('contents.*')
						->distinct('contents.content_id')            
						->paginate($limit);
					}
					else{
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->where('content_providers.provider_id',$provider)
						->where('contents.original_language',$langs)
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)     
						->where('contents.content_type','!=','tv-show')    
						->orderBy('contents.click_count','DESC')
						->select('contents.*')
						->distinct('contents.content_id')             
						->paginate($limit);
					}           
				}
				elseif($genre!=""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->leftJoin('content_genres','content_genres.content_id','contents.content_id')
						->where('content_providers.provider_id',$provider)
						->where('content_genres.genre_id',$genre)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.content_type','!=','tv-show')
						->orderBy('contents.click_count','DESC')
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate($limit);
					}
					else{
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->leftJoin('content_genres','content_genres.content_id','contents.content_id')
						->where('content_providers.provider_id',$provider)
						->where('content_genres.genre_id',$genre)
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.content_type','!=','tv-show')
						->orderBy('contents.click_count','DESC')
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate($limit);
					}            
				}
				elseif($rated != ""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->where('content_providers.provider_id',$provider)
						->where('contents.age_certification',$rated)                
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)     
						->where('contents.content_type','!=','tv-show')    
						->orderBy('contents.click_count','DESC')
						->select('contents.*')
						->distinct('contents.content_id')             
						->paginate($limit);
					}
					else{
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->where('content_providers.provider_id',$provider)
						->where('contents.age_certification',$rated)                
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)     
						->where('contents.content_type','!=','tv-show')    
						->orderBy('contents.click_count','DESC')
						->select('contents.*')
						->distinct('contents.content_id')             
						->paginate($limit);
					}            
				}
				else{
					if($minr==0 && $maxr==10){
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->where('content_providers.provider_id',$provider)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)     
						->where('contents.content_type','!=','tv-show')    
						->orderBy('contents.click_count','DESC')
						->select('contents.*')
						->distinct('contents.content_id')             
						->paginate($limit);
					}
					else{
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->where('content_providers.provider_id',$provider)
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)     
						->where('contents.content_type','!=','tv-show')                 
						->orderBy('contents.click_count','DESC')
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate($limit);
					}  
				}
			}
			else{

				if($langs!="" && $rated!="" && $genre!=""){

					if($minr==0 && $maxr==10){
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->leftJoin('content_genres','content_genres.content_id','contents.content_id')
						->where('content_providers.provider_id',$provider)
						->where('content_genres.genre_id',$genre)
						->where('contents.age_certification',$rated)
						->where('contents.original_language',$langs)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.content_type','!=','tv-show')
						->orderBy('contents.click_count','DESC')
						->orderBy($field,$sort_type)
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate($limit);
					}
					else{
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->leftJoin('content_genres','content_genres.content_id','contents.content_id')
						->where('content_providers.provider_id',$provider)
						->where('content_genres.genre_id',$genre)
						->where('contents.age_certification',$rated)
						->where('contents.original_language',$langs)
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.content_type','!=','tv-show')
						->orderBy('contents.click_count','DESC')
						->orderBy($field,$sort_type)
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate($limit);
					}          
				}
				elseif($langs!="" && $rated != ""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->where('content_providers.provider_id',$provider)
						->where('contents.original_language',$langs)
						->where('contents.age_certification',$rated)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)     
						->where('contents.content_type','!=','tv-show')                 
						->orderBy('contents.click_count','DESC')
						->orderBy($field,$sort_type)
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate($limit); 
					}
					else{
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->where('content_providers.provider_id',$provider)
						->where('contents.original_language',$langs)
						->where('contents.age_certification',$rated)
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)     
						->where('contents.content_type','!=','tv-show')                 
						->orderBy('contents.click_count','DESC')
						->orderBy($field,$sort_type)
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate($limit); 
					}        
				}
				elseif($rated != "" && $genre!=""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->leftJoin('content_genres','content_genres.content_id','contents.content_id')
						->where('content_providers.provider_id',$provider)
						->where('content_genres.genre_id',$genre)
						->where('contents.age_certification',$rated)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.content_type','!=','tv-show')
						->orderBy('contents.click_count','DESC')
						->orderBy($field,$sort_type)
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate($limit);
					}
					else{
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->leftJoin('content_genres','content_genres.content_id','contents.content_id')
						->where('content_providers.provider_id',$provider)
						->where('content_genres.genre_id',$genre)
						->where('contents.age_certification',$rated)
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.content_type','!=','tv-show')
						->orderBy('contents.click_count','DESC')
						->orderBy($field,$sort_type)
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate($limit);
					}          
				}
				elseif($langs != "" && $genre!=""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->leftJoin('content_genres','content_genres.content_id','contents.content_id')
						->where('content_providers.provider_id',$provider)
						->where('content_genres.genre_id',$genre)
						->where('contents.original_language',$langs)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.content_type','!=','tv-show')
						->orderBy('contents.click_count','DESC')
						->orderBy($field,$sort_type)
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate($limit);
					}
					else{
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->leftJoin('content_genres','content_genres.content_id','contents.content_id')
						->where('content_providers.provider_id',$provider)
						->where('content_genres.genre_id',$genre)
						->where('contents.original_language',$langs)
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.content_type','!=','tv-show')
						->orderBy('contents.click_count','DESC')
						->orderBy($field,$sort_type)
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate($limit);
					}          
				}
				elseif($langs != ""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->where('content_providers.provider_id',$provider)
						->where('contents.original_language',$langs)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)     
						->where('contents.content_type','!=','tv-show')                 
						->orderBy('contents.click_count','DESC')
						->orderBy($field,$sort_type)
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate($limit);
					}
					else{
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->where('content_providers.provider_id',$provider)
						->where('contents.original_language',$langs)
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)     
						->where('contents.content_type','!=','tv-show')                 
						->orderBy('contents.click_count','DESC')
						->orderBy($field,$sort_type)
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate($limit);
					}         
				}
				elseif($genre!=""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->leftJoin('content_genres','content_genres.content_id','contents.content_id')
						->where('content_providers.provider_id',$provider)
						->where('content_genres.genre_id',$genre)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.content_type','!=','tv-show')
						->orderBy('contents.click_count','DESC')
						->orderBy($field,$sort_type)
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate($limit);
					}
					else{
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->leftJoin('content_genres','content_genres.content_id','contents.content_id')
						->where('content_providers.provider_id',$provider)
						->where('content_genres.genre_id',$genre)
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.content_type','!=','tv-show')
						->orderBy('contents.click_count','DESC')
						->orderBy($field,$sort_type)
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate($limit);
					}   
				}
				elseif($rated != ""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->where('content_providers.provider_id',$provider)
						->where('contents.age_certification',$rated)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)     
						->where('contents.content_type','!=','tv-show')                 
						->orderBy('contents.click_count','DESC')
						->orderBy($field,$sort_type)
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate($limit);
					}
					else{
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->where('content_providers.provider_id',$provider)
						->where('contents.age_certification',$rated)
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)     
						->where('contents.content_type','!=','tv-show')                 
						->orderBy('contents.click_count','DESC')
						->orderBy($field,$sort_type)
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate($limit);
					}          
				}
				else{
					if($minr==0 && $maxr==10){
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->where('content_providers.provider_id',$provider)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)     
						->where('contents.content_type','!=','tv-show')                 
						->orderBy('contents.click_count','DESC')
						->orderBy($field,$sort_type)
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate($limit);
					}
					else{
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->where('content_providers.provider_id',$provider)
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)     
						->where('contents.content_type','!=','tv-show')                 
						->orderBy('contents.click_count','DESC')
						->orderBy($field,$sort_type)
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate($limit);
					}
				}
			}

		}
	}

	else{

		$filter_type = array();
		if($type == 'movie'){
			$filter_type = ['movie'];
		}
		else{
			$filter_type = ['shows','tv-show','show'];        
		}

		if($provider == ""){

			if($sort == ""){
				if($langs!="" && $rated!="" && $genre!=""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('content_genres')
						->leftJoin('contents','content_genres.content_id','contents.content_id')
						->where('content_genres.genre_id',$genre)
						->whereIn('contents.content_type',$filter_type)
						->where('contents.age_certification',$rated)
						->where('contents.original_language',$langs)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->orderBy('contents.click_count','DESC')
						->paginate($limit);
					}
					else{
						$contents = DB::table('content_genres')
						->leftJoin('contents','content_genres.content_id','contents.content_id')
						->where('content_genres.genre_id',$genre)
						->whereIn('contents.content_type',$filter_type)
						->where('contents.age_certification',$rated)
						->where('contents.original_language',$langs)
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->orderBy('contents.click_count','DESC')
						->paginate($limit);
					}        
				}
				elseif($langs!="" && $rated != ""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('contents')
						->whereIn('content_type',$filter_type)
						->where('original_language',$langs)
						->where('age_certification',$rated)                
						->where('cinema_release_date','<=',$maxy)
						->where('cinema_release_date','>=',$miny)
						->orderBy('click_count','DESC')
						->paginate($limit);
					}
					else{$contents = DB::table('contents')
						->whereIn('content_type',$filter_type)
					->where('original_language',$langs)
					->where('age_certification',$rated)                
					->where('tmdb_score','<=',$maxr)
					->where('tmdb_score','>=',$minr)
					->where('cinema_release_date','<=',$maxy)
					->where('cinema_release_date','>=',$miny)
					->orderBy('click_count','DESC')
					->paginate($limit);
				}       
			}
			elseif($rated != "" && $genre!=""){
				if($minr==0 && $maxr==10){
					$contents = DB::table('content_genres')
					->leftJoin('contents','content_genres.content_id','contents.content_id')
					->where('content_genres.genre_id',$genre)
					->whereIn('contents.content_type',$filter_type)
					->where('contents.age_certification',$rated)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>=',$miny)
					->orderBy('contents.click_count','DESC')
					->paginate($limit);
				}
				else{
					$contents = DB::table('content_genres')
					->leftJoin('contents','content_genres.content_id','contents.content_id')
					->where('content_genres.genre_id',$genre)
					->whereIn('contents.content_type',$filter_type)
					->where('contents.age_certification',$rated)
					->where('contents.tmdb_score','<=',$maxr)
					->where('contents.tmdb_score','>=',$minr)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>=',$miny)
					->orderBy('contents.click_count','DESC')
					->paginate($limit);
				}        
			}
			elseif($langs != "" && $genre!=""){
				if($minr==0 && $maxr==10){
					$contents = DB::table('content_genres')
					->leftJoin('contents','content_genres.content_id','contents.content_id')
					->where('content_genres.genre_id',$genre)
					->whereIn('contents.content_type',$filter_type)
					->where('contents.original_language',$langs)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>=',$miny)
					->orderBy('contents.click_count','DESC')
					->paginate($limit); 
				}
				else{
					$contents = DB::table('content_genres')
					->leftJoin('contents','content_genres.content_id','contents.content_id')
					->where('content_genres.genre_id',$genre)
					->whereIn('contents.content_type',$filter_type)
					->where('contents.original_language',$langs)
					->where('contents.tmdb_score','<=',$maxr)
					->where('contents.tmdb_score','>=',$minr)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>=',$miny)
					->orderBy('contents.click_count','DESC')
					->paginate($limit); 
				}       
			}
			elseif($langs != ""){
				if($minr==0 && $maxr==10){
					$contents = DB::table('contents')
					->whereIn('content_type',$filter_type)
					->where('original_language',$langs)
					->where('cinema_release_date','<=',$maxy)
					->where('cinema_release_date','>=',$miny)
					->orderBy('click_count','DESC')
					->paginate($limit);
				}
				else{
					$contents = DB::table('contents')
					->whereIn('content_type',$filter_type)
					->where('original_language',$langs)
					->where('tmdb_score','<=',$maxr)
					->where('tmdb_score','>=',$minr)
					->where('cinema_release_date','<=',$maxy)
					->where('cinema_release_date','>=',$miny)
					->orderBy('click_count','DESC')
					->paginate($limit);
				}       
			}
			elseif($genre!=""){
				if($minr==0 && $maxr==10){
					$contents = DB::table('content_genres')
					->leftJoin('contents','content_genres.content_id','contents.content_id')
					->where('content_genres.genre_id',$genre)
					->whereIn('contents.content_type',$filter_type)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>=',$miny)
					->orderBy('contents.click_count','DESC')
					->paginate($limit); 
				}
				else{
					$contents = DB::table('content_genres')
					->leftJoin('contents','content_genres.content_id','contents.content_id')
					->where('content_genres.genre_id',$genre)
					->whereIn('contents.content_type',$filter_type)
					->where('contents.tmdb_score','<=',$maxr)
					->where('contents.tmdb_score','>=',$minr)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>=',$miny)
					->orderBy('contents.click_count','DESC')
					->paginate($limit); 
				}  
			}
			elseif($rated != ""){
				if($minr==0 && $maxr==10){
					$contents = DB::table('contents')
					->whereIn('content_type',$filter_type)
					->where('age_certification',$rated)                
					->where('cinema_release_date','<=',$maxy)
					->where('cinema_release_date','>=',$miny)
					->orderBy('click_count','DESC')
					->paginate($limit);  
				}
				else{
					$contents = DB::table('contents')
					->whereIn('content_type',$filter_type)
					->where('age_certification',$rated)                
					->where('tmdb_score','<=',$maxr)
					->where('tmdb_score','>=',$minr)
					->where('cinema_release_date','<=',$maxy)
					->where('cinema_release_date','>=',$miny)
					->orderBy('click_count','DESC')
					->paginate($limit);  
				} 
			}
			else{
				if($minr==0 && $maxr==10){
					$contents = DB::table('contents')
					->whereIn('content_type',$filter_type)
					->where('cinema_release_date','<=',$maxy)
					->where('cinema_release_date','>=',$miny)
					->orderBy('click_count','DESC')
					->paginate($limit);  
				}
				else{
					$contents = DB::table('contents')
					->whereIn('content_type',$filter_type)
					->where('tmdb_score','<=',$maxr)
					->where('tmdb_score','>=',$minr)
					->where('cinema_release_date','<=',$maxy)
					->where('cinema_release_date','>=',$miny)
					->orderBy('click_count','DESC')
					->paginate($limit); 
				}
			}
		}
		else{

			if($langs!="" && $rated!="" && $genre!=""){
				if($minr==0 && $maxr==10){
					$contents = DB::table('content_genres')
					->leftJoin('contents','content_genres.content_id','contents.content_id')
					->where('content_genres.genre_id',$genre)
					->whereIn('contents.content_type',$filter_type)
					->where('contents.age_certification',$rated)
					->where('contents.original_language',$langs)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>=',$miny)
					->orderBy('contents.click_count','DESC')
					->orderBy($field,$sort_type)
					->paginate($limit);
				}
				else{
					$contents = DB::table('content_genres')
					->leftJoin('contents','content_genres.content_id','contents.content_id')
					->where('content_genres.genre_id',$genre)
					->whereIn('contents.content_type',$filter_type)
					->where('contents.age_certification',$rated)
					->where('contents.original_language',$langs)
					->where('contents.tmdb_score','<=',$maxr)
					->where('contents.tmdb_score','>=',$minr)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>=',$miny)
					->orderBy('contents.click_count','DESC')
					->orderBy($field,$sort_type)
					->paginate($limit);
				}   
			}
			elseif($langs!="" && $rated != ""){
				if($minr==0 && $maxr==10){
					$contents = DB::table('contents')
					->whereIn('content_type',$filter_type)
					->where('original_language',$langs)
					->where('age_certification',$rated)                
					->where('cinema_release_date','<=',$maxy)
					->where('cinema_release_date','>=',$miny)
					->orderBy('click_count','DESC')
					->orderBy($field,$sort_type)
					->paginate($limit);  
				}
				else{
					$contents = DB::table('contents')
					->whereIn('content_type',$filter_type)
					->where('original_language',$langs)
					->where('age_certification',$rated)                
					->where('tmdb_score','<=',$maxr)
					->where('tmdb_score','>=',$minr)
					->where('cinema_release_date','<=',$maxy)
					->where('cinema_release_date','>=',$miny)
					->orderBy('click_count','DESC')
					->orderBy($field,$sort_type)
					->paginate($limit);  
				}

			}
			elseif($rated != "" && $genre!=""){
				if($minr==0 && $maxr==10){
					$contents = DB::table('content_genres')
					->leftJoin('contents','content_genres.content_id','contents.content_id')
					->where('content_genres.genre_id',$genre)
					->whereIn('contents.content_type',$filter_type)
					->where('contents.age_certification',$rated)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>=',$miny)
					->orderBy('contents.click_count','DESC')
					->orderBy($field,$sort_type)
					->paginate($limit);
				}
				else{
					$contents = DB::table('content_genres')
					->leftJoin('contents','content_genres.content_id','contents.content_id')
					->where('content_genres.genre_id',$genre)
					->whereIn('contents.content_type',$filter_type)
					->where('contents.age_certification',$rated)
					->where('contents.tmdb_score','<=',$maxr)
					->where('contents.tmdb_score','>=',$minr)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>=',$miny)
					->orderBy('contents.click_count','DESC')
					->orderBy($field,$sort_type)
					->paginate($limit);
				}      
			}
			elseif($langs != "" && $genre!=""){
				if($minr==0 && $maxr==10){
					$contents = DB::table('content_genres')
					->leftJoin('contents','content_genres.content_id','contents.content_id')
					->where('content_genres.genre_id',$genre)
					->whereIn('contents.content_type',$filter_type)
					->where('contents.original_language',$langs)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>=',$miny)
					->orderBy('contents.click_count','DESC')
					->orderBy($field,$sort_type)
					->paginate($limit);
				}
				else{
					$contents = DB::table('content_genres')
					->leftJoin('contents','content_genres.content_id','contents.content_id')
					->where('content_genres.genre_id',$genre)
					->whereIn('contents.content_type',$filter_type)
					->where('contents.original_language',$langs)
					->where('contents.tmdb_score','<=',$maxr)
					->where('contents.tmdb_score','>=',$minr)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>=',$miny)
					->orderBy('contents.click_count','DESC')
					->orderBy($field,$sort_type)
					->paginate($limit);
				}   
			}
			elseif($langs != ""){
				if($minr==0 && $maxr==10){
					$contents = DB::table('contents')
					->whereIn('content_type',$filter_type)
					->where('original_language',$langs)
					->where('cinema_release_date','<=',$maxy)
					->where('cinema_release_date','>=',$miny)
					->orderBy('click_count','DESC')
					->orderBy($field,$sort_type)
					->paginate($limit);
				}
				else{
					$contents = DB::table('contents')
					->whereIn('content_type',$filter_type)
					->where('original_language',$langs)
					->where('tmdb_score','<=',$maxr)
					->where('tmdb_score','>=',$minr)
					->where('cinema_release_date','<=',$maxy)
					->where('cinema_release_date','>=',$miny)
					->orderBy('click_count','DESC')
					->orderBy($field,$sort_type)
					->paginate($limit);
				}     
			}
			elseif($genre!=""){
				if($minr==0 && $maxr==10){
					$contents = DB::table('content_genres')
					->leftJoin('contents','content_genres.content_id','contents.content_id')
					->where('content_genres.genre_id',$genre)
					->whereIn('contents.content_type',$filter_type)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>=',$miny)
					->orderBy('contents.click_count','DESC')
					->orderBy($field,$sort_type)
					->paginate($limit); 
				}
				else{
					$contents = DB::table('content_genres')
					->leftJoin('contents','content_genres.content_id','contents.content_id')
					->where('content_genres.genre_id',$genre)
					->whereIn('contents.content_type',$filter_type)
					->where('contents.tmdb_score','<=',$maxr)
					->where('contents.tmdb_score','>=',$minr)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>=',$miny)
					->orderBy('contents.click_count','DESC')
					->orderBy($field,$sort_type)
					->paginate($limit); 
				}  
			}
			elseif($rated != ""){
				if($minr==0 && $maxr==10){
					$contents = DB::table('contents')
					->whereIn('content_type',$filter_type)
					->where('age_certification',$rated)                
					->where('cinema_release_date','<=',$maxy)
					->where('cinema_release_date','>=',$miny)
					->orderBy('click_count','DESC')
					->orderBy($field,$sort_type)
					->paginate($limit);
				}
				else{
					$contents = DB::table('contents')
					->whereIn('content_type',$filter_type)
					->where('age_certification',$rated)                
					->where('tmdb_score','<=',$maxr)
					->where('tmdb_score','>=',$minr)
					->where('cinema_release_date','<=',$maxy)
					->where('cinema_release_date','>=',$miny)
					->orderBy('click_count','DESC')
					->orderBy($field,$sort_type)
					->paginate($limit);
				}   
			}
			else{
				if($minr==0 && $maxr==10){
					$contents = DB::table('contents')
					->whereIn('content_type',$filter_type)
					->where('cinema_release_date','<=',$maxy)
					->where('cinema_release_date','>=',$miny)
					->orderBy('click_count','DESC')
					->orderBy($field,$sort_type)
					->paginate($limit);  
				}
				else{
					$contents = DB::table('contents')
					->whereIn('content_type',$filter_type)
					->where('tmdb_score','<=',$maxr)
					->where('tmdb_score','>=',$minr)
					->where('cinema_release_date','<=',$maxy)
					->where('cinema_release_date','>=',$miny)
					->orderBy('click_count','DESC')
					->orderBy($field,$sort_type)
					->paginate($limit);  
				}  
			}
		}
	}



	else{

		if($sort == ""){
			if($langs!="" && $rated!="" && $genre!=""){
				if($minr==0 && $maxr==10){
					$contents = DB::table('content_providers')
					->leftJoin('contents','contents.content_id','content_providers.content_id')
					->leftJoin('content_genres','content_genres.content_id','contents.content_id')
					->where('content_providers.provider_id',$provider)
					->where('content_genres.genre_id',$genre)
					->where('contents.age_certification',$rated)
					->whereIn('contents.content_type',$filter_type)
					->where('contents.original_language',$langs)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>=',$miny)
					->orderBy('contents.click_count','DESC')
					->select('contents.*')
					->distinct('contents.content_id')
					->paginate($limit);
				}
				else{
					$contents = DB::table('content_providers')
					->leftJoin('contents','contents.content_id','content_providers.content_id')
					->leftJoin('content_genres','content_genres.content_id','contents.content_id')
					->where('content_providers.provider_id',$provider)
					->where('content_genres.genre_id',$genre)
					->whereIn('contents.content_type',$filter_type)
					->where('contents.age_certification',$rated)
					->where('contents.original_language',$langs)
					->where('contents.tmdb_score','<=',$maxr)
					->where('contents.tmdb_score','>=',$minr)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>=',$miny)
					->orderBy('contents.click_count','DESC')
					->select('contents.*')
					->distinct('contents.content_id')
					->paginate($limit);
				}        
			}
			elseif($langs!="" && $rated != ""){
				if($minr==0 && $maxr==10){
					$contents = DB::table('content_providers')
					->leftJoin('contents','contents.content_id','content_providers.content_id')
					->where('content_providers.provider_id',$provider)
					->whereIn('contents.content_type',$filter_type)
					->where('contents.original_language',$langs)
					->where('contents.age_certification',$rated)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>=',$miny)     
					->orderBy('contents.click_count','DESC')
					->select('contents.*')
					->distinct('contents.content_id')
					->paginate($limit);
				}
				else{
					$contents = DB::table('content_providers')
					->leftJoin('contents','contents.content_id','content_providers.content_id')
					->where('content_providers.provider_id',$provider)
					->whereIn('contents.content_type',$filter_type)
					->where('contents.original_language',$langs)
					->where('contents.age_certification',$rated)
					->where('contents.tmdb_score','<=',$maxr)
					->where('contents.tmdb_score','>=',$minr)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>=',$miny)     
					->orderBy('contents.click_count','DESC')
					->select('contents.*')
					->distinct('contents.content_id')
					->paginate($limit);
				}       
			}
			elseif($rated != "" && $genre!=""){
				if($minr==0 && $maxr==10){
					$contents = DB::table('content_providers')
					->leftJoin('contents','contents.content_id','content_providers.content_id')
					->leftJoin('content_genres','content_genres.content_id','contents.content_id')
					->where('content_providers.provider_id',$provider)
					->where('content_genres.genre_id',$genre)
					->where('contents.age_certification',$rated)
					->whereIn('contents.content_type',$filter_type)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>=',$miny)
					->orderBy('contents.click_count','DESC')
					->select('contents.*')
					->distinct('contents.content_id')
					->paginate($limit);
				}
				else{
					$contents = DB::table('content_providers')
					->leftJoin('contents','contents.content_id','content_providers.content_id')
					->leftJoin('content_genres','content_genres.content_id','contents.content_id')
					->where('content_providers.provider_id',$provider)
					->where('content_genres.genre_id',$genre)
					->where('contents.age_certification',$rated)
					->whereIn('contents.content_type',$filter_type)
					->where('contents.tmdb_score','<=',$maxr)
					->where('contents.tmdb_score','>=',$minr)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>=',$miny)
					->orderBy('contents.click_count','DESC')
					->select('contents.*')
					->distinct('contents.content_id')
					->paginate($limit);
				}        
			}
			elseif($langs != "" && $genre!=""){
				if($minr==0 && $maxr==10){
					$contents = DB::table('content_providers')
					->leftJoin('contents','contents.content_id','content_providers.content_id')
					->leftJoin('content_genres','content_genres.content_id','contents.content_id')
					->where('content_providers.provider_id',$provider)
					->where('content_genres.genre_id',$genre)
					->whereIn('contents.content_type',$filter_type)
					->where('contents.original_language',$langs)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>=',$miny)
					->orderBy('contents.click_count','DESC')
					->select('contents.*')
					->distinct('contents.content_id')
					->paginate($limit);
				}
				else{
					$contents = DB::table('content_providers')
					->leftJoin('contents','contents.content_id','content_providers.content_id')
					->leftJoin('content_genres','content_genres.content_id','contents.content_id')
					->where('content_providers.provider_id',$provider)
					->where('content_genres.genre_id',$genre)
					->whereIn('contents.content_type',$filter_type)
					->where('contents.original_language',$langs)
					->where('contents.tmdb_score','<=',$maxr)
					->where('contents.tmdb_score','>=',$minr)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>=',$miny)
					->orderBy('contents.click_count','DESC')
					->select('contents.*')
					->distinct('contents.content_id')
					->paginate($limit);
				}       
			}
			elseif($langs != ""){
				if($minr==0 && $maxr==10){
					$contents = DB::table('content_providers')
					->leftJoin('contents','contents.content_id','content_providers.content_id')
					->where('content_providers.provider_id',$provider)
					->whereIn('contents.content_type',$filter_type)
					->where('contents.original_language',$langs)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>=',$miny)     
					->orderBy('contents.click_count','DESC')
					->select('contents.*')
					->distinct('contents.content_id')
					->paginate($limit);
				}
				else{
					$contents = DB::table('content_providers')
					->leftJoin('contents','contents.content_id','content_providers.content_id')
					->where('content_providers.provider_id',$provider)
					->whereIn('contents.content_type',$filter_type)
					->where('contents.original_language',$langs)
					->where('contents.tmdb_score','<=',$maxr)
					->where('contents.tmdb_score','>=',$minr)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>=',$miny)     
					->orderBy('contents.click_count','DESC')
					->select('contents.*')
					->distinct('contents.content_id')
					->paginate($limit);
				}       
			}
			elseif($genre!=""){
				if($minr==0 && $maxr==10){
					$contents = DB::table('content_providers')
					->leftJoin('contents','contents.content_id','content_providers.content_id')
					->leftJoin('content_genres','content_genres.content_id','contents.content_id')
					->where('content_providers.provider_id',$provider)
					->where('content_genres.genre_id',$genre)
					->whereIn('contents.content_type',$filter_type)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>=',$miny)
					->orderBy('contents.click_count','DESC')
					->select('contents.*')
					->distinct('contents.content_id')
					->paginate($limit);
				}
				else{
					$contents = DB::table('content_providers')
					->leftJoin('contents','contents.content_id','content_providers.content_id')
					->leftJoin('content_genres','content_genres.content_id','contents.content_id')
					->where('content_providers.provider_id',$provider)
					->where('content_genres.genre_id',$genre)
					->whereIn('contents.content_type',$filter_type)
					->where('contents.tmdb_score','<=',$maxr)
					->where('contents.tmdb_score','>=',$minr)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>=',$miny)
					->orderBy('contents.click_count','DESC')
					->select('contents.*')
					->distinct('contents.content_id')
					->paginate($limit);
				}  
			}
			elseif($rated != ""){


				if($minr==0 && $maxr==10){
					$contents = DB::table('content_providers')
					->leftJoin('contents','contents.content_id','content_providers.content_id')
					->where('content_providers.provider_id',$provider)
					->whereIn('contents.content_type',$filter_type)
					->where('contents.age_certification',$rated)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>=',$miny)     
					->orderBy('contents.click_count','DESC')
					->select('contents.*')
					->distinct('contents.content_id')
					->paginate($limit);  
				}
				else{
					$contents = DB::table('content_providers')
					->leftJoin('contents','contents.content_id','content_providers.content_id')
					->where('content_providers.provider_id',$provider)
					->whereIn('contents.content_type',$filter_type)
					->where('contents.age_certification',$rated)
					->where('contents.tmdb_score','<=',$maxr)
					->where('contents.tmdb_score','>=',$minr)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>=',$miny)     
					->orderBy('contents.click_count','DESC')
					->select('contents.*')
					->distinct('contents.content_id')
					->paginate($limit);
				} 
			}
			else{
				if($minr==0 && $maxr==10){
					$contents = DB::table('content_providers')
					->leftJoin('contents','contents.content_id','content_providers.content_id')
					->where('content_providers.provider_id',$provider)
					->whereIn('contents.content_type',$filter_type)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>=',$miny)     
					->orderBy('contents.click_count','DESC')
					->select('contents.*')
					->distinct('contents.content_id')
					->paginate($limit);
				}
				else{
					$contents = DB::table('content_providers')
					->leftJoin('contents','contents.content_id','content_providers.content_id')
					->where('content_providers.provider_id',$provider)
					->whereIn('contents.content_type',$filter_type)
					->where('contents.tmdb_score','<=',$maxr)
					->where('contents.tmdb_score','>=',$minr)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>=',$miny)     
					->orderBy('contents.click_count','DESC')
					->select('contents.*')
					->distinct('contents.content_id')
					->paginate($limit);
				}
			}
		}
		else{

			if($langs!="" && $rated!="" && $genre!=""){
				if($minr==0 && $maxr==10){
					$contents = DB::table('content_providers')
					->leftJoin('contents','contents.content_id','content_providers.content_id')
					->leftJoin('content_genres','content_genres.content_id','contents.content_id')
					->where('content_providers.provider_id',$provider)
					->where('content_genres.genre_id',$genre)
					->where('contents.age_certification',$rated)
					->whereIn('contents.content_type',$filter_type)
					->where('contents.original_language',$langs)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>=',$miny)
					->orderBy('contents.click_count','DESC')
					->orderBy($field,$sort_type)
					->select('contents.*')
					->distinct('contents.content_id')
					->paginate($limit);
				}
				else{
					$contents = DB::table('content_genres')
					->leftJoin('contents','content_genres.content_id','contents.content_id')
					->where('content_genres.genre_id',$genre)
					->whereIn('contents.content_type',$filter_type)
					->where('contents.age_certification',$rated)
					->where('contents.original_language',$langs)
					->where('contents.tmdb_score','<=',$maxr)
					->where('contents.tmdb_score','>=',$minr)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>=',$miny)
					->orderBy('contents.click_count','DESC')
					->orderBy($field,$sort_type)
					->paginate($limit);
				}   
			}
			elseif($langs!="" && $rated != ""){
				if($minr==0 && $maxr==10){
					$contents = DB::table('content_providers')
					->leftJoin('contents','contents.content_id','content_providers.content_id')
					->where('content_providers.provider_id',$provider)
					->whereIn('contents.content_type',$filter_type)
					->where('contents.original_language',$langs)
					->where('contents.age_certification',$rated)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>=',$miny)     
					->orderBy('contents.click_count','DESC')
					->orderBy($field,$sort_type)
					->select('contents.*')
					->distinct('contents.content_id')
					->paginate($limit); 
				}
				else{
					$contents = DB::table('content_providers')
					->leftJoin('contents','contents.content_id','content_providers.content_id')
					->where('content_providers.provider_id',$provider)
					->whereIn('contents.content_type',$filter_type)
					->where('contents.original_language',$langs)
					->where('contents.age_certification',$rated)
					->where('contents.tmdb_score','<=',$maxr)
					->where('contents.tmdb_score','>=',$minr)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>=',$miny)     
					->orderBy('contents.click_count','DESC')
					->orderBy($field,$sort_type)
					->select('contents.*')
					->distinct('contents.content_id')
					->paginate($limit); 
				}

			}
			elseif($rated != "" && $genre!=""){
				if($minr==0 && $maxr==10){
					$contents = DB::table('content_providers')
					->leftJoin('contents','contents.content_id','content_providers.content_id')
					->leftJoin('content_genres','content_genres.content_id','contents.content_id')
					->where('content_providers.provider_id',$provider)
					->where('content_genres.genre_id',$genre)
					->where('contents.age_certification',$rated)
					->whereIn('contents.content_type',$filter_type)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>=',$miny)
					->orderBy('contents.click_count','DESC')
					->orderBy($field,$sort_type)
					->select('contents.*')
					->distinct('contents.content_id')
					->paginate($limit);
				}
				else{
					$contents = DB::table('content_providers')
					->leftJoin('contents','contents.content_id','content_providers.content_id')
					->leftJoin('content_genres','content_genres.content_id','contents.content_id')
					->where('content_providers.provider_id',$provider)
					->where('content_genres.genre_id',$genre)
					->where('contents.age_certification',$rated)
					->whereIn('contents.content_type',$filter_type)
					->where('contents.tmdb_score','<=',$maxr)
					->where('contents.tmdb_score','>=',$minr)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>=',$miny)
					->orderBy('contents.click_count','DESC')
					->orderBy($field,$sort_type)
					->select('contents.*')
					->distinct('contents.content_id')
					->paginate($limit);
				}      
			}
			elseif($langs != "" && $genre!=""){
				if($minr==0 && $maxr==10){
					$contents = DB::table('content_providers')
					->leftJoin('contents','contents.content_id','content_providers.content_id')
					->leftJoin('content_genres','content_genres.content_id','contents.content_id')
					->where('content_providers.provider_id',$provider)
					->where('content_genres.genre_id',$genre)
					->whereIn('contents.content_type',$filter_type)
					->where('contents.original_language',$langs)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>=',$miny)
					->orderBy('contents.click_count','DESC')
					->orderBy($field,$sort_type)
					->select('contents.*')
					->distinct('contents.content_id')
					->paginate($limit);
				}
				else{
					$contents = DB::table('content_providers')
					->leftJoin('contents','contents.content_id','content_providers.content_id')
					->leftJoin('content_genres','content_genres.content_id','contents.content_id')
					->where('content_providers.provider_id',$provider)
					->where('content_genres.genre_id',$genre)
					->whereIn('contents.content_type',$filter_type)
					->where('contents.original_language',$langs)
					->where('contents.tmdb_score','>=',$minr)
					->where('contents.tmdb_score','<=',$maxr)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>=',$miny)
					->orderBy('contents.click_count','DESC')
					->orderBy($field,$sort_type)
					->select('contents.*')
					->distinct('contents.content_id')
					->paginate($limit);
				}   
			}
			elseif($langs != ""){
				if($minr==0 && $maxr==10){
					$contents = DB::table('content_providers')
					->leftJoin('contents','contents.content_id','content_providers.content_id')
					->where('content_providers.provider_id',$provider)
					->whereIn('contents.content_type',$filter_type)
					->where('contents.original_language',$langs)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>=',$miny)     
					->orderBy('contents.click_count','DESC')
					->orderBy($field,$sort_type)
					->select('contents.*')
					->distinct('contents.content_id')
					->paginate($limit);
				}
				else{
					$contents = DB::table('content_providers')
					->leftJoin('contents','contents.content_id','content_providers.content_id')
					->where('content_providers.provider_id',$provider)
					->whereIn('contents.content_type',$filter_type)
					->where('contents.original_language',$langs)
					->where('contents.tmdb_score','<=',$maxr)
					->where('contents.tmdb_score','>=',$minr)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>=',$miny)     
					->orderBy('contents.click_count','DESC')
					->orderBy($field,$sort_type)
					->select('contents.*')
					->distinct('contents.content_id')
					->paginate($limit);
				}     
			}
			elseif($genre!=""){
				if($minr==0 && $maxr==10){
					$contents = DB::table('content_providers')
					->leftJoin('contents','contents.content_id','content_providers.content_id')
					->leftJoin('content_genres','content_genres.content_id','contents.content_id')
					->where('content_providers.provider_id',$provider)
					->where('content_genres.genre_id',$genre)
					->whereIn('contents.content_type',$filter_type)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>=',$miny)
					->orderBy('contents.click_count','DESC')
					->orderBy($field,$sort_type)
					->select('contents.*')
					->distinct('contents.content_id')
					->paginate($limit);
				}
				else{
					$contents = DB::table('content_providers')
					->leftJoin('contents','contents.content_id','content_providers.content_id')
					->leftJoin('content_genres','content_genres.content_id','contents.content_id')
					->where('content_providers.provider_id',$provider)
					->where('content_genres.genre_id',$genre)
					->whereIn('contents.content_type',$filter_type)
					->where('contents.tmdb_score','>=',$minr)
					->where('contents.tmdb_score','<=',$maxr)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>=',$miny)
					->orderBy('contents.click_count','DESC')
					->orderBy($field,$sort_type)
					->select('contents.*')
					->distinct('contents.content_id')
					->paginate($limit);
				}  
			}
			elseif($rated != ""){
				if($minr==0 && $maxr==10){
					$contents = DB::table('content_providers')
					->leftJoin('contents','contents.content_id','content_providers.content_id')
					->where('content_providers.provider_id',$provider)
					->whereIn('contents.content_type',$filter_type)
					->where('contents.age_certification',$rated)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>=',$miny)     
					->orderBy('contents.click_count','DESC')
					->orderBy($field,$sort_type)
					->select('contents.*')
					->distinct('contents.content_id')
					->paginate($limit);
				}
				else{
					$contents = DB::table('content_providers')
					->leftJoin('contents','contents.content_id','content_providers.content_id')
					->where('content_providers.provider_id',$provider)
					->whereIn('contents.content_type',$filter_type)
					->where('contents.original_language',$langs)
					->where('contents.tmdb_score','<=',$maxr)
					->where('contents.tmdb_score','>=',$minr)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>=',$miny)     
					->orderBy('contents.click_count','DESC')
					->orderBy($field,$sort_type)
					->select('contents.*')
					->distinct('contents.content_id')
					->paginate($limit);
				}   
			}
			else{
				if($minr==0 && $maxr==10){
					$contents = DB::table('content_providers')
					->leftJoin('contents','contents.content_id','content_providers.content_id')
					->where('content_providers.provider_id',$provider)
					->whereIn('contents.content_type',$filter_type)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>=',$miny)     
					->orderBy('contents.click_count','DESC')
					->orderBy($field,$sort_type)
					->select('contents.*')
					->distinct('contents.content_id')
					->paginate($limit);
				}
				else{
					$contents = DB::table('content_providers')
					->leftJoin('contents','contents.content_id','content_providers.content_id')
					->where('content_providers.provider_id',$provider)
					->whereIn('contents.content_type',$filter_type)
					->where('contents.tmdb_score','<=',$maxr)
					->where('contents.tmdb_score','>=',$minr)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>=',$miny)     
					->orderBy('contents.click_count','DESC')
					->orderBy($field,$sort_type)
					->select('contents.*')
					->distinct('contents.content_id')
					->paginate($limit);
				}  
			}
		}

	}

}
$miny = explode('-',$miny)[0];
$maxy = explode('-',$maxy)[0];

		if(isset($_GET['provider'])){  $provider = $_GET['provider'];  }
		foreach ($contents as $key => $value) {
			if (!file_exists(public_path().'/poster/'.$value->poster)){
				$contents[$key]->poster = 'default.png';
			}
		}
		return response()->json(compact('contents'));
	      // return view('khoj.home',);
	}

	function upcoming(Request $request){ 

		if (isset($request->limit) && $request->limit > 0){
			$limit = $request->limit;
		}
		else
			$limit = 30;

		$date=date('Y-m-d');
	$page = 0;
	$langs = "";
	$rated = "";
	$sort = "";
	$field = "";
	$sort_type = "";
	$provider = "";
	$genre = "";
	$minr=0;
	$maxr=10; 
	$type="all";
	$tab = 'Upcoming';

	$miny = $date;
	$maxy = DB::table('contents')->orderBy('cinema_release_date','DESC')->first();
	$maxy = $maxy->cinema_release_date;

	if(isset($_GET['page'])){
		$page = $_GET['page']-1;
	}  

	if(isset($_GET['lang'])){  $langs = $_GET['lang'];  }
	if(isset($_GET['provider'])){  $provider = explode('_',$_GET['provider'])[1];  }

	if(isset($_GET['sort'])){
		$sort = $_GET['sort'];
		if($sort == "alpha"){
			$field = 'title';
			$sort_type = 'ASC';
		}
		elseif($sort == 'rate'){
			$field = 'tmdb_score';
			$sort_type = 'DESC';
		}
		elseif($sort == 'year'){
			$field = 'cinema_release_date';
			$sort_type = 'DESC';
		}
		elseif($sort == 'imdb_score'){
			$field = 'imdb_score';
			$sort_type = 'DESC';
		}
		else{
			$field = 'content_id';
			$sort_type = 'DESC';
		}
	}

	if( (isset($_GET['sort']) && isset($_GET['genre'])) || (isset($_GET['sort']) && isset($_GET['provider']))){
		$sort = $_GET['sort'];
		if($sort == "alpha"){
			$field = 'contents.title';
			$sort_type = 'ASC';
		}
		elseif($sort == 'rate'){
			$field = 'contents.tmdb_score';
			$sort_type = 'DESC';
		}
		elseif($sort == 'year'){
			$field = 'contents.cinema_release_date';
			$sort_type = 'DESC';
		}
		elseif($sort == 'imdb_score'){
			$field = 'contents.imdb_score';
			$sort_type = 'DESC';
		}
		else{
			$field = 'contents.content_id';
			$sort_type = 'DESC';
		}
	}

	if(isset($_GET['rated'])){
		$rated = $_GET['rated'];
	}

	if(isset($_GET['searchtype'])){
		$type = $_GET['searchtype'];
	}

	if(isset($_GET['genre'])){
		$genre = $_GET['genre'];
	}

	if(isset($_GET['minr'])){
		$minr = $_GET['minr'];
	}
	if(isset($_GET['maxr'])){
		$maxr = $_GET['maxr'];    
	}

	if(isset($_GET['miny'])){
		$miny = $_GET['miny']."-01-01";
	}
	if(isset($_GET['maxy'])){
		$maxy = $_GET['maxy']."-12-31";
	}

	if($type == 'all'){
		if($provider == ""){
			if($sort == ""){

				if($langs!="" && $rated!="" && $genre!=""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('content_genres')
						->leftJoin('contents','content_genres.content_id','contents.content_id')
						->where('content_genres.genre_id',$genre)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>',$miny)
						->where('contents.age_certification',$rated)
						->where('contents.original_language',$langs)
						->where('contents.content_type','!=','tv-show')
						->orderBy('content_genres.content_id','DESC')
						->paginate($limit);
					}
					else{
						$contents = DB::table('content_genres')
						->leftJoin('contents','content_genres.content_id','contents.content_id')
						->where('content_genres.genre_id',$genre)
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>',$miny)
						->where('contents.age_certification',$rated)
						->where('contents.original_language',$langs)
						->where('contents.content_type','!=','tv-show')
						->orderBy('content_genres.content_id','DESC')
						->paginate($limit);
					}   
				}
				elseif($langs!="" && $rated != ""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('contents')
						->where('original_language',$langs)
						->where('age_certification',$rated)
						->where('cinema_release_date','<=',$maxy)
						->where('cinema_release_date','>',$miny)     
						->where('content_type','!=','tv-show')                  
						->paginate($limit);
					}
					else{
						$contents = DB::table('contents')
						->where('original_language',$langs)
						->where('age_certification',$rated)
						->where('tmdb_score','<=',$maxr)
						->where('tmdb_score','>=',$minr)
						->where('cinema_release_date','<=',$maxy)
						->where('cinema_release_date','>',$miny)     
						->where('content_type','!=','tv-show')                  
						->paginate($limit);
					}           
				}
				elseif($rated != "" && $genre!=""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('content_genres')
						->leftJoin('contents','content_genres.content_id','contents.content_id')
						->where('content_genres.genre_id',$genre)
						->where('contents.age_certification',$rated)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>',$miny)    
						->where('contents.content_type','!=','tv-show')   
						->orderBy('content_genres.content_id','DESC')
						->paginate($limit);
					}
					else{
						$contents = DB::table('content_genres')
						->leftJoin('contents','content_genres.content_id','contents.content_id')
						->where('content_genres.genre_id',$genre)
						->where('contents.age_certification',$rated)
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>',$miny)    
						->where('contents.content_type','!=','tv-show')   
						->orderBy('content_genres.content_id','DESC')
						->paginate($limit);
					}   
				}
				elseif($langs != "" && $genre!=""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('content_genres')
						->leftJoin('contents','content_genres.content_id','contents.content_id')
						->where('content_genres.genre_id',$genre)
						->where('contents.original_language',$langs)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>',$miny)
						->where('contents.content_type','!=','tv-show')
						->orderBy('content_genres.content_id','DESC')
						->paginate($limit);
					}
					else{
						$contents = DB::table('content_genres')
						->leftJoin('contents','content_genres.content_id','contents.content_id')
						->where('content_genres.genre_id',$genre)
						->where('contents.original_language',$langs)
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>',$miny)
						->where('contents.content_type','!=','tv-show')
						->orderBy('content_genres.content_id','DESC')
						->paginate($limit);
					}            
				}
				elseif($langs != ""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('contents')
						->where('original_language',$langs)
						->where('cinema_release_date','<=',$maxy)
						->where('cinema_release_date','>',$miny)
						->where('content_type','!=','tv-show')
						->paginate($limit);
					}
					else{
						$contents = DB::table('contents')
						->where('original_language',$langs)
						->where('tmdb_score','<=',$maxr)
						->where('tmdb_score','>=',$minr)
						->where('cinema_release_date','<=',$maxy)
						->where('cinema_release_date','>',$miny)
						->where('content_type','!=','tv-show')
						->paginate($limit);
					}           
				}
				elseif($genre!=""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('content_genres')
						->leftJoin('contents','content_genres.content_id','contents.content_id')
						->where('content_genres.genre_id',$genre)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>',$miny)
						->where('contents.content_type','!=','tv-show')
						->orderBy('content_genres.content_id','DESC')
						->paginate($limit);
					}
					else{
						$contents = DB::table('content_genres')
						->leftJoin('contents','content_genres.content_id','contents.content_id')
						->where('content_genres.genre_id',$genre)
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>',$miny)
						->where('contents.content_type','!=','tv-show')
						->orderBy('content_genres.content_id','DESC')
						->paginate($limit);
					}            
				}
				elseif($rated != ""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('contents')
						->where('age_certification',$rated)                
						->where('cinema_release_date','<=',$maxy)
						->where('cinema_release_date','>',$miny)
						->where('content_type','!=','tv-show')
						->paginate($limit);
					}
					else{
						$contents = DB::table('contents')
						->where('age_certification',$rated)                
						->where('tmdb_score','<=',$maxr)
						->where('tmdb_score','>=',$minr)
						->where('cinema_release_date','<=',$maxy)
						->where('cinema_release_date','>',$miny)
						->where('content_type','!=','tv-show')
						->paginate($limit);
					}            
				}
				else{
					if($minr==0 && $maxr==10){
						$contents = DB::table('contents')
						->where('cinema_release_date','<=',$maxy)
						->where('cinema_release_date','>',$miny)
						->where('content_type','!=','tv-show')
						->orderBy('content_id','DESC')
						->paginate($limit);
					}
					else{
						$contents = DB::table('contents')
						->where('tmdb_score','<=',$maxr)
						->where('tmdb_score','>=',$minr)
						->where('cinema_release_date','<=',$maxy)
						->where('cinema_release_date','>',$miny)
						->where('content_type','!=','tv-show')
						->orderBy('content_id','DESC')
						->paginate($limit);
					}  
				}
			}
			else{

				if($langs!="" && $rated!="" && $genre!=""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('content_genres')
						->leftJoin('contents','content_genres.content_id','contents.content_id')
						->where('content_genres.genre_id',$genre)
						->where('contents.age_certification',$rated)
						->where('contents.original_language',$langs)
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>',$miny)
						->where('contents.content_type','!=','tv-show')
						->orderBy($field,$sort_type)
						->paginate($limit);
					}
					else{
						$contents = DB::table('content_genres')
						->leftJoin('contents','content_genres.content_id','contents.content_id')
						->where('content_genres.genre_id',$genre)
						->where('contents.age_certification',$rated)
						->where('contents.original_language',$langs)
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>',$miny)
						->where('contents.content_type','!=','tv-show')
						->orderBy($field,$sort_type)
						->paginate($limit);
					}          
				}
				elseif($langs!="" && $rated != ""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('contents')
						->where('original_language',$langs)
						->where('age_certification',$rated)                
						->where('cinema_release_date','<=',$maxy)
						->where('cinema_release_date','>',$miny)
						->where('content_type','!=','tv-show')
						->orderBy($field,$sort_type)
						->paginate($limit); 
					}
					else{
						$contents = DB::table('contents')
						->where('original_language',$langs)
						->where('age_certification',$rated)                
						->where('tmdb_score','<=',$maxr)
						->where('tmdb_score','>=',$minr)
						->where('cinema_release_date','<=',$maxy)
						->where('cinema_release_date','>',$miny)
						->where('content_type','!=','tv-show')
						->orderBy($field,$sort_type)
						->paginate($limit); 
					}        
				}
				elseif($rated != "" && $genre!=""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('content_genres')
						->leftJoin('contents','content_genres.content_id','contents.content_id')
						->where('content_genres.genre_id',$genre)
						->where('contents.age_certification',$rated)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>',$miny)
						->where('contents.content_type','!=','tv-show')
						->orderBy($field,$sort_type)
						->paginate($limit);
					}
					else{
						$contents = DB::table('content_genres')
						->leftJoin('contents','content_genres.content_id','contents.content_id')
						->where('content_genres.genre_id',$genre)
						->where('contents.age_certification',$rated)
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>',$miny)
						->where('contents.content_type','!=','tv-show')
						->orderBy($field,$sort_type)
						->paginate($limit);
					}          
				}
				elseif($langs != "" && $genre!=""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('content_genres')
						->leftJoin('contents','content_genres.content_id','contents.content_id')
						->where('content_genres.genre_id',$genre)
						->where('contents.original_language',$langs)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>',$miny)
						->where('contents.content_type','!=','tv-show')
						->orderBy($field,$sort_type)
						->paginate($limit);
					}
					else{
						$contents = DB::table('content_genres')
						->leftJoin('contents','content_genres.content_id','contents.content_id')
						->where('content_genres.genre_id',$genre)
						->where('contents.original_language',$langs)
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>',$miny)
						->where('contents.content_type','!=','tv-show')
						->orderBy($field,$sort_type)
						->paginate($limit);
					}          
				}
				elseif($langs != ""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('contents')
						->where('original_language',$langs)
						->where('cinema_release_date','<=',$maxy)
						->where('cinema_release_date','>',$miny)
						->where('content_type','!=','tv-show')
						->orderBy($field,$sort_type)
						->paginate($limit);
					}
					else{
						$contents = DB::table('contents')
						->where('original_language',$langs)
						->where('tmdb_score','<=',$maxr)
						->where('tmdb_score','>=',$minr)
						->where('cinema_release_date','<=',$maxy)
						->where('cinema_release_date','>',$miny)
						->where('content_type','!=','tv-show')
						->orderBy($field,$sort_type)
						->paginate($limit);
					}         
				}
				elseif($genre!=""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('content_genres')
						->leftJoin('contents','content_genres.content_id','contents.content_id')
						->where('content_genres.genre_id',$genre)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>',$miny)
						->where('contents.content_type','!=','tv-show')
						->orderBy($field,$sort_type)
						->paginate($limit);
					}
					else{
						$contents = DB::table('content_genres')
						->leftJoin('contents','content_genres.content_id','contents.content_id')
						->where('content_genres.genre_id',$genre)
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>',$miny)
						->where('contents.content_type','!=','tv-show')
						->orderBy($field,$sort_type)
						->paginate($limit);
					}   
				}
				elseif($rated != ""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('contents')
						->where('age_certification',$rated)                
						->where('cinema_release_date','<=',$maxy)
						->where('cinema_release_date','>',$miny)
						->where('content_type','!=','tv-show')
						->orderBy($field,$sort_type)
						->paginate($limit);
					}
					else{
						$contents = DB::table('contents')
						->where('age_certification',$rated)                
						->where('tmdb_score','<=',$maxr)
						->where('tmdb_score','>=',$minr)
						->where('cinema_release_date','<=',$maxy)
						->where('cinema_release_date','>',$miny)
						->where('content_type','!=','tv-show')
						->orderBy($field,$sort_type)
						->paginate($limit);
					}          
				}
				else{
					if($minr==0 && $maxr==10){
						$contents = DB::table('contents')
						->where('cinema_release_date','<=',$maxy)
						->where('cinema_release_date','>',$miny)
						->where('content_type','!=','tv-show')
						->orderBy($field,$sort_type)
						->paginate($limit);  
					}
					else{
						$contents = DB::table('contents')
						->where('tmdb_score','<=',$maxr)
						->where('tmdb_score','>=',$minr)
						->where('cinema_release_date','<=',$maxy)
						->where('cinema_release_date','>',$miny)
						->where('content_type','!=','tv-show')
						->orderBy($field,$sort_type)
						->paginate($limit);  
					}
				}
			}
		}

  //When Provider Not NULL
		else{
			if($sort == ""){
				if($langs!="" && $rated!="" && $genre!=""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->leftJoin('content_genres','content_genres.content_id','contents.content_id')
						->where('content_providers.provider_id',$provider)
						->where('content_genres.genre_id',$genre)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>',$miny)
						->where('contents.age_certification',$rated)
						->where('contents.original_language',$langs)
						->where('contents.content_type','!=','tv-show')
						->orderBy('content_genres.content_id','DESC')
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate($limit);
					}
					else{
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->leftJoin('content_genres','content_genres.content_id','contents.content_id')
						->where('content_providers.provider_id',$provider)
						->where('content_genres.genre_id',$genre)
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>',$miny)
						->where('contents.age_certification',$rated)
						->where('contents.original_language',$langs)
						->where('contents.content_type','!=','tv-show')
						->orderBy('content_genres.content_id','DESC')
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate($limit);
					}   
				}
				elseif($langs!="" && $rated != ""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->where('content_providers.provider_id',$provider)
						->where('contents.original_language',$langs)
						->where('contents.age_certification',$rated)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>',$miny)     
						->where('contents.content_type','!=','tv-show')  
						->select('contents.*')
						->distinct('contents.content_id')               
						->paginate($limit);
					}
					else{
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->where('content_providers.provider_id',$provider)
						->where('contents.original_language',$langs)
						->where('contents.age_certification',$rated)
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>',$miny)     
						->where('contents.content_type','!=','tv-show')   
						->select('contents.*')
						->distinct('contents.content_id')              
						->paginate($limit);
					}           
				}
				elseif($rated != "" && $genre!=""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->leftJoin('content_genres','content_genres.content_id','contents.content_id')
						->where('content_providers.provider_id',$provider)
						->where('content_genres.genre_id',$genre)
						->where('contents.age_certification',$rated)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>',$miny)
						->where('contents.content_type','!=','tv-show')
						->orderBy('content_genres.content_id','DESC')
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate($limit);
					}
					else{
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->leftJoin('content_genres','content_genres.content_id','contents.content_id')
						->where('content_providers.provider_id',$provider)
						->where('content_genres.genre_id',$genre)
						->where('contents.age_certification',$rated)
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>',$miny)
						->where('contents.content_type','!=','tv-show')
						->orderBy('content_genres.content_id','DESC')
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate($limit);
					}   
				}
				elseif($langs != "" && $genre!=""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->leftJoin('content_genres','content_genres.content_id','contents.content_id')
						->where('content_providers.provider_id',$provider)
						->where('content_genres.genre_id',$genre)
						->where('contents.original_language',$langs)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>',$miny)
						->where('contents.content_type','!=','tv-show')
						->orderBy('content_genres.content_id','DESC')
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate($limit);
					}
					else{
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->leftJoin('content_genres','content_genres.content_id','contents.content_id')
						->where('content_providers.provider_id',$provider)
						->where('content_genres.genre_id',$genre)
						->where('contents.original_language',$langs)
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>',$miny)
						->where('contents.content_type','!=','tv-show')
						->orderBy('content_genres.content_id','DESC')
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate($limit);
					}            
				}
				elseif($langs != ""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->where('content_providers.provider_id',$provider)
						->where('contents.original_language',$langs)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>',$miny)     
						->where('contents.content_type','!=','tv-show')     
						->select('contents.*')
						->distinct('contents.content_id')            
						->paginate($limit);
					}
					else{
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->where('content_providers.provider_id',$provider)
						->where('contents.original_language',$langs)
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>',$miny)     
						->where('contents.content_type','!=','tv-show')    
						->select('contents.*')
						->distinct('contents.content_id')             
						->paginate($limit);
					}           
				}
				elseif($genre!=""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->leftJoin('content_genres','content_genres.content_id','contents.content_id')
						->where('content_providers.provider_id',$provider)
						->where('content_genres.genre_id',$genre)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>',$miny)
						->where('contents.content_type','!=','tv-show')
						->orderBy('content_genres.content_id','DESC')
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate($limit);
					}
					else{
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->leftJoin('content_genres','content_genres.content_id','contents.content_id')
						->where('content_providers.provider_id',$provider)
						->where('content_genres.genre_id',$genre)
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>',$miny)
						->where('contents.content_type','!=','tv-show')
						->orderBy('content_genres.content_id','DESC')
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate($limit);
					}            
				}
				elseif($rated != ""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->where('content_providers.provider_id',$provider)
						->where('contents.age_certification',$rated)                
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>',$miny)     
						->where('contents.content_type','!=','tv-show')    
						->select('contents.*')
						->distinct('contents.content_id')             
						->paginate($limit);
					}
					else{
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->where('content_providers.provider_id',$provider)
						->where('contents.age_certification',$rated)                
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>',$miny)     
						->where('contents.content_type','!=','tv-show')    
						->select('contents.*')
						->distinct('contents.content_id')             
						->paginate($limit);
					}            
				}
				else{
					if($minr==0 && $maxr==10){
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->where('content_providers.provider_id',$provider)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>',$miny)     
						->where('contents.content_type','!=','tv-show')    
						->select('contents.*')
						->distinct('contents.content_id')             
						->paginate($limit);
					}
					else{
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->where('content_providers.provider_id',$provider)
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>',$miny)     
						->where('contents.content_type','!=','tv-show')                 
						->orderBy('contents.content_id','DESC')
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate($limit);
					}  
				}
			}
			else{

				if($langs!="" && $rated!="" && $genre!=""){

					if($minr==0 && $maxr==10){
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->leftJoin('content_genres','content_genres.content_id','contents.content_id')
						->where('content_providers.provider_id',$provider)
						->where('content_genres.genre_id',$genre)
						->where('contents.age_certification',$rated)
						->where('contents.original_language',$langs)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>',$miny)
						->where('contents.content_type','!=','tv-show')
						->orderBy($field,$sort_type)
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate($limit);
					}
					else{
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->leftJoin('content_genres','content_genres.content_id','contents.content_id')
						->where('content_providers.provider_id',$provider)
						->where('content_genres.genre_id',$genre)
						->where('contents.age_certification',$rated)
						->where('contents.original_language',$langs)
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>',$miny)
						->where('contents.content_type','!=','tv-show')
						->orderBy($field,$sort_type)
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate($limit);
					}          
				}
				elseif($langs!="" && $rated != ""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->where('content_providers.provider_id',$provider)
						->where('contents.original_language',$langs)
						->where('contents.age_certification',$rated)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>',$miny)     
						->where('contents.content_type','!=','tv-show')                 
						->orderBy('contents.content_id','DESC')
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate($limit); 
					}
					else{
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->where('content_providers.provider_id',$provider)
						->where('contents.original_language',$langs)
						->where('contents.age_certification',$rated)
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>',$miny)     
						->where('contents.content_type','!=','tv-show')                 
						->orderBy('contents.content_id','DESC')
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate($limit); 
					}        
				}
				elseif($rated != "" && $genre!=""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->leftJoin('content_genres','content_genres.content_id','contents.content_id')
						->where('content_providers.provider_id',$provider)
						->where('content_genres.genre_id',$genre)
						->where('contents.age_certification',$rated)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>',$miny)
						->where('contents.content_type','!=','tv-show')
						->orderBy($field,$sort_type)
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate($limit);
					}
					else{
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->leftJoin('content_genres','content_genres.content_id','contents.content_id')
						->where('content_providers.provider_id',$provider)
						->where('content_genres.genre_id',$genre)
						->where('contents.age_certification',$rated)
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>',$miny)
						->where('contents.content_type','!=','tv-show')
						->orderBy($field,$sort_type)
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate($limit);
					}          
				}
				elseif($langs != "" && $genre!=""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->leftJoin('content_genres','content_genres.content_id','contents.content_id')
						->where('content_providers.provider_id',$provider)
						->where('content_genres.genre_id',$genre)
						->where('contents.original_language',$langs)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>',$miny)
						->where('contents.content_type','!=','tv-show')
						->orderBy($field,$sort_type)
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate($limit);
					}
					else{
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->leftJoin('content_genres','content_genres.content_id','contents.content_id')
						->where('content_providers.provider_id',$provider)
						->where('content_genres.genre_id',$genre)
						->where('contents.original_language',$langs)
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>',$miny)
						->where('contents.content_type','!=','tv-show')
						->orderBy($field,$sort_type)
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate($limit);
					}          
				}
				elseif($langs != ""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->where('content_providers.provider_id',$provider)
						->where('contents.original_language',$langs)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>',$miny)     
						->where('contents.content_type','!=','tv-show')                 
						->orderBy($field,$sort_type)
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate($limit);
					}
					else{
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->where('content_providers.provider_id',$provider)
						->where('contents.original_language',$langs)
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>',$miny)     
						->where('contents.content_type','!=','tv-show')                 
						->orderBy($field,$sort_type)
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate($limit);
					}         
				}
				elseif($genre!=""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->leftJoin('content_genres','content_genres.content_id','contents.content_id')
						->where('content_providers.provider_id',$provider)
						->where('content_genres.genre_id',$genre)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>',$miny)
						->where('contents.content_type','!=','tv-show')
						->orderBy($field,$sort_type)
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate($limit);
					}
					else{
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->leftJoin('content_genres','content_genres.content_id','contents.content_id')
						->where('content_providers.provider_id',$provider)
						->where('content_genres.genre_id',$genre)
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>',$miny)
						->where('contents.content_type','!=','tv-show')
						->orderBy($field,$sort_type)
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate($limit);
					}   
				}
				elseif($rated != ""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->where('content_providers.provider_id',$provider)
						->where('contents.age_certification',$rated)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>',$miny)     
						->where('contents.content_type','!=','tv-show')                 
						->orderBy($field,$sort_type)
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate($limit);
					}
					else{
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->where('content_providers.provider_id',$provider)
						->where('contents.age_certification',$rated)
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>',$miny)     
						->where('contents.content_type','!=','tv-show')                 
						->orderBy($field,$sort_type)
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate($limit);
					}          
				}
				else{
					if($minr==0 && $maxr==10){
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->where('content_providers.provider_id',$provider)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>',$miny)     
						->where('contents.content_type','!=','tv-show')                 
						->orderBy($field,$sort_type)
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate($limit);
					}
					else{
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->where('content_providers.provider_id',$provider)
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>',$miny)     
						->where('contents.content_type','!=','tv-show')                 
						->orderBy($field,$sort_type)
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate($limit);
					}
				}
			}

		}
	}

	else{

		$filter_type = array();
		if($type == 'movie'){
			$filter_type = ['movie'];
		}
		else{
			$filter_type = ['shows','tv-show','show'];        
		}

		if($provider == ""){

			if($sort == ""){
				if($langs!="" && $rated!="" && $genre!=""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('content_genres')
						->leftJoin('contents','content_genres.content_id','contents.content_id')
						->where('content_genres.genre_id',$genre)
						->whereIn('contents.content_type',$filter_type)
						->where('contents.age_certification',$rated)
						->where('contents.original_language',$langs)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>',$miny)
						->orderBy('content_genres.content_id','DESC')
						->paginate($limit);
					}
					else{
						$contents = DB::table('content_genres')
						->leftJoin('contents','content_genres.content_id','contents.content_id')
						->where('content_genres.genre_id',$genre)
						->whereIn('contents.content_type',$filter_type)
						->where('contents.age_certification',$rated)
						->where('contents.original_language',$langs)
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>',$miny)
						->orderBy('content_genres.content_id','DESC')
						->paginate($limit);
					}        
				}
				elseif($langs!="" && $rated != ""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('contents')
						->whereIn('content_type',$filter_type)
						->where('original_language',$langs)
						->where('age_certification',$rated)                
						->where('cinema_release_date','<=',$maxy)
						->where('cinema_release_date','>',$miny)
						->paginate($limit);
					}
					else{$contents = DB::table('contents')
						->whereIn('content_type',$filter_type)
					->where('original_language',$langs)
					->where('age_certification',$rated)                
					->where('tmdb_score','<=',$maxr)
					->where('tmdb_score','>=',$minr)
					->where('cinema_release_date','<=',$maxy)
					->where('cinema_release_date','>',$miny)
					->paginate($limit);
				}       
			}
			elseif($rated != "" && $genre!=""){
				if($minr==0 && $maxr==10){
					$contents = DB::table('content_genres')
					->leftJoin('contents','content_genres.content_id','contents.content_id')
					->where('content_genres.genre_id',$genre)
					->whereIn('contents.content_type',$filter_type)
					->where('contents.age_certification',$rated)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>',$miny)
					->orderBy('content_genres.content_id','DESC')
					->paginate($limit);
				}
				else{
					$contents = DB::table('content_genres')
					->leftJoin('contents','content_genres.content_id','contents.content_id')
					->where('content_genres.genre_id',$genre)
					->whereIn('contents.content_type',$filter_type)
					->where('contents.age_certification',$rated)
					->where('contents.tmdb_score','<=',$maxr)
					->where('contents.tmdb_score','>=',$minr)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>',$miny)
					->orderBy('content_genres.content_id','DESC')
					->paginate($limit);
				}        
			}
			elseif($langs != "" && $genre!=""){
				if($minr==0 && $maxr==10){
					$contents = DB::table('content_genres')
					->leftJoin('contents','content_genres.content_id','contents.content_id')
					->where('content_genres.genre_id',$genre)
					->whereIn('contents.content_type',$filter_type)
					->where('contents.original_language',$langs)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>',$miny)
					->orderBy('content_genres.content_id','DESC')
					->paginate($limit); 
				}
				else{
					$contents = DB::table('content_genres')
					->leftJoin('contents','content_genres.content_id','contents.content_id')
					->where('content_genres.genre_id',$genre)
					->whereIn('contents.content_type',$filter_type)
					->where('contents.original_language',$langs)
					->where('contents.tmdb_score','<=',$maxr)
					->where('contents.tmdb_score','>=',$minr)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>',$miny)
					->orderBy('content_genres.content_id','DESC')
					->paginate($limit); 
				}       
			}
			elseif($langs != ""){
				if($minr==0 && $maxr==10){
					$contents = DB::table('contents')
					->whereIn('content_type',$filter_type)
					->where('original_language',$langs)
					->where('cinema_release_date','<=',$maxy)
					->where('cinema_release_date','>',$miny)
					->paginate($limit);
				}
				else{
					$contents = DB::table('contents')
					->whereIn('content_type',$filter_type)
					->where('original_language',$langs)
					->where('tmdb_score','<=',$maxr)
					->where('tmdb_score','>=',$minr)
					->where('cinema_release_date','<=',$maxy)
					->where('cinema_release_date','>',$miny)
					->paginate($limit);
				}       
			}
			elseif($genre!=""){
				if($minr==0 && $maxr==10){
					$contents = DB::table('content_genres')
					->leftJoin('contents','content_genres.content_id','contents.content_id')
					->where('content_genres.genre_id',$genre)
					->whereIn('contents.content_type',$filter_type)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>',$miny)
					->orderBy('content_genres.content_id','DESC')
					->paginate($limit); 
				}
				else{
					$contents = DB::table('content_genres')
					->leftJoin('contents','content_genres.content_id','contents.content_id')
					->where('content_genres.genre_id',$genre)
					->whereIn('contents.content_type',$filter_type)
					->where('contents.tmdb_score','<=',$maxr)
					->where('contents.tmdb_score','>=',$minr)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>',$miny)
					->orderBy('content_genres.content_id','DESC')
					->paginate($limit); 
				}  
			}
			elseif($rated != ""){
				if($minr==0 && $maxr==10){
					$contents = DB::table('contents')
					->whereIn('content_type',$filter_type)
					->where('age_certification',$rated)                
					->where('cinema_release_date','<=',$maxy)
					->where('cinema_release_date','>',$miny)
					->paginate($limit);  
				}
				else{
					$contents = DB::table('contents')
					->whereIn('content_type',$filter_type)
					->where('age_certification',$rated)                
					->where('tmdb_score','<=',$maxr)
					->where('tmdb_score','>=',$minr)
					->where('cinema_release_date','<=',$maxy)
					->where('cinema_release_date','>',$miny)
					->paginate($limit);  
				} 
			}
			else{
				if($minr==0 && $maxr==10){
					$contents = DB::table('contents')
					->whereIn('content_type',$filter_type)
					->where('cinema_release_date','<=',$maxy)
					->where('cinema_release_date','>',$miny)
					->paginate($limit);  
				}
				else{
					$contents = DB::table('contents')
					->whereIn('content_type',$filter_type)
					->where('tmdb_score','<=',$maxr)
					->where('tmdb_score','>=',$minr)
					->where('cinema_release_date','<=',$maxy)
					->where('cinema_release_date','>',$miny)
					->paginate($limit); 
				}
			}
		}
		else{

			if($langs!="" && $rated!="" && $genre!=""){
				if($minr==0 && $maxr==10){
					$contents = DB::table('content_genres')
					->leftJoin('contents','content_genres.content_id','contents.content_id')
					->where('content_genres.genre_id',$genre)
					->whereIn('contents.content_type',$filter_type)
					->where('contents.age_certification',$rated)
					->where('contents.original_language',$langs)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>',$miny)
					->orderBy($field,$sort_type)
					->paginate($limit);
				}
				else{
					$contents = DB::table('content_genres')
					->leftJoin('contents','content_genres.content_id','contents.content_id')
					->where('content_genres.genre_id',$genre)
					->whereIn('contents.content_type',$filter_type)
					->where('contents.age_certification',$rated)
					->where('contents.original_language',$langs)
					->where('contents.tmdb_score','<=',$maxr)
					->where('contents.tmdb_score','>=',$minr)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>',$miny)
					->orderBy($field,$sort_type)
					->paginate($limit);
				}   
			}
			elseif($langs!="" && $rated != ""){
				if($minr==0 && $maxr==10){
					$contents = DB::table('contents')
					->whereIn('content_type',$filter_type)
					->where('original_language',$langs)
					->where('age_certification',$rated)                
					->where('cinema_release_date','<=',$maxy)
					->where('cinema_release_date','>',$miny)
					->orderBy($field,$sort_type)
					->paginate($limit);  
				}
				else{
					$contents = DB::table('contents')
					->whereIn('content_type',$filter_type)
					->where('original_language',$langs)
					->where('age_certification',$rated)                
					->where('tmdb_score','<=',$maxr)
					->where('tmdb_score','>=',$minr)
					->where('cinema_release_date','<=',$maxy)
					->where('cinema_release_date','>',$miny)
					->orderBy($field,$sort_type)
					->paginate($limit);  
				}

			}
			elseif($rated != "" && $genre!=""){
				if($minr==0 && $maxr==10){
					$contents = DB::table('content_genres')
					->leftJoin('contents','content_genres.content_id','contents.content_id')
					->where('content_genres.genre_id',$genre)
					->whereIn('contents.content_type',$filter_type)
					->where('contents.age_certification',$rated)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>',$miny)
					->orderBy($field,$sort_type)
					->paginate($limit);
				}
				else{
					$contents = DB::table('content_genres')
					->leftJoin('contents','content_genres.content_id','contents.content_id')
					->where('content_genres.genre_id',$genre)
					->whereIn('contents.content_type',$filter_type)
					->where('contents.age_certification',$rated)
					->where('contents.tmdb_score','<=',$maxr)
					->where('contents.tmdb_score','>=',$minr)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>',$miny)
					->orderBy($field,$sort_type)
					->paginate($limit);
				}      
			}
			elseif($langs != "" && $genre!=""){
				if($minr==0 && $maxr==10){
					$contents = DB::table('content_genres')
					->leftJoin('contents','content_genres.content_id','contents.content_id')
					->where('content_genres.genre_id',$genre)
					->whereIn('contents.content_type',$filter_type)
					->where('contents.original_language',$langs)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>',$miny)
					->orderBy($field,$sort_type)
					->paginate($limit);
				}
				else{
					$contents = DB::table('content_genres')
					->leftJoin('contents','content_genres.content_id','contents.content_id')
					->where('content_genres.genre_id',$genre)
					->whereIn('contents.content_type',$filter_type)
					->where('contents.original_language',$langs)
					->where('contents.tmdb_score','<=',$maxr)
					->where('contents.tmdb_score','>=',$minr)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>',$miny)
					->orderBy($field,$sort_type)
					->paginate($limit);
				}   
			}
			elseif($langs != ""){
				if($minr==0 && $maxr==10){
					$contents = DB::table('contents')
					->whereIn('content_type',$filter_type)
					->where('original_language',$langs)
					->where('cinema_release_date','<=',$maxy)
					->where('cinema_release_date','>',$miny)
					->orderBy($field,$sort_type)
					->paginate($limit);
				}
				else{
					$contents = DB::table('contents')
					->whereIn('content_type',$filter_type)
					->where('original_language',$langs)
					->where('tmdb_score','<=',$maxr)
					->where('tmdb_score','>=',$minr)
					->where('cinema_release_date','<=',$maxy)
					->where('cinema_release_date','>',$miny)
					->orderBy($field,$sort_type)
					->paginate($limit);
				}     
			}
			elseif($genre!=""){
				if($minr==0 && $maxr==10){
					$contents = DB::table('content_genres')
					->leftJoin('contents','content_genres.content_id','contents.content_id')
					->where('content_genres.genre_id',$genre)
					->whereIn('contents.content_type',$filter_type)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>',$miny)
					->orderBy('content_genres.content_id','DESC')
					->orderBy($field,$sort_type)
					->paginate($limit); 
				}
				else{
					$contents = DB::table('content_genres')
					->leftJoin('contents','content_genres.content_id','contents.content_id')
					->where('content_genres.genre_id',$genre)
					->whereIn('contents.content_type',$filter_type)
					->where('contents.tmdb_score','<=',$maxr)
					->where('contents.tmdb_score','>=',$minr)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>',$miny)
					->orderBy('content_genres.content_id','DESC')
					->orderBy($field,$sort_type)
					->paginate($limit); 
				}  
			}
			elseif($rated != ""){
				if($minr==0 && $maxr==10){
					$contents = DB::table('contents')
					->whereIn('content_type',$filter_type)
					->where('age_certification',$rated)                
					->where('cinema_release_date','<=',$maxy)
					->where('cinema_release_date','>',$miny)
					->orderBy($field,$sort_type)
					->paginate($limit);
				}
				else{
					$contents = DB::table('contents')
					->whereIn('content_type',$filter_type)
					->where('age_certification',$rated)                
					->where('tmdb_score','<=',$maxr)
					->where('tmdb_score','>=',$minr)
					->where('cinema_release_date','<=',$maxy)
					->where('cinema_release_date','>',$miny)
					->orderBy($field,$sort_type)
					->paginate($limit);
				}   
			}
			else{
				if($minr==0 && $maxr==10){
					$contents = DB::table('contents')
					->whereIn('content_type',$filter_type)
					->where('cinema_release_date','<=',$maxy)
					->where('cinema_release_date','>',$miny)
					->orderBy($field,$sort_type)
					->paginate($limit);  
				}
				else{
					$contents = DB::table('contents')
					->whereIn('content_type',$filter_type)
					->where('tmdb_score','<=',$maxr)
					->where('tmdb_score','>=',$minr)
					->where('cinema_release_date','<=',$maxy)
					->where('cinema_release_date','>',$miny)
					->orderBy($field,$sort_type)
					->paginate($limit);  
				}  
			}
		}
	}



	else{

		if($sort == ""){
			if($langs!="" && $rated!="" && $genre!=""){
				if($minr==0 && $maxr==10){
					$contents = DB::table('content_providers')
					->leftJoin('contents','contents.content_id','content_providers.content_id')
					->leftJoin('content_genres','content_genres.content_id','contents.content_id')
					->where('content_providers.provider_id',$provider)
					->where('content_genres.genre_id',$genre)
					->where('contents.age_certification',$rated)
					->whereIn('contents.content_type',$filter_type)
					->where('contents.original_language',$langs)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>',$miny)
					->orderBy('content_genres.content_id','DESC')
					->select('contents.*')
					->distinct('contents.content_id')
					->paginate($limit);
				}
				else{
					$contents = DB::table('content_providers')
					->leftJoin('contents','contents.content_id','content_providers.content_id')
					->leftJoin('content_genres','content_genres.content_id','contents.content_id')
					->where('content_providers.provider_id',$provider)
					->where('content_genres.genre_id',$genre)
					->whereIn('contents.content_type',$filter_type)
					->where('contents.age_certification',$rated)
					->where('contents.original_language',$langs)
					->where('contents.tmdb_score','<=',$maxr)
					->where('contents.tmdb_score','>=',$minr)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>',$miny)
					->orderBy('content_genres.content_id','DESC')
					->select('contents.*')
					->distinct('contents.content_id')
					->paginate($limit);
				}        
			}
			elseif($langs!="" && $rated != ""){
				if($minr==0 && $maxr==10){
					$contents = DB::table('content_providers')
					->leftJoin('contents','contents.content_id','content_providers.content_id')
					->where('content_providers.provider_id',$provider)
					->whereIn('contents.content_type',$filter_type)
					->where('contents.original_language',$langs)
					->where('contents.age_certification',$rated)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>',$miny)     
					->select('contents.*')
					->distinct('contents.content_id')
					->paginate($limit);
				}
				else{
					$contents = DB::table('content_providers')
					->leftJoin('contents','contents.content_id','content_providers.content_id')
					->where('content_providers.provider_id',$provider)
					->whereIn('contents.content_type',$filter_type)
					->where('contents.original_language',$langs)
					->where('contents.age_certification',$rated)
					->where('contents.tmdb_score','<=',$maxr)
					->where('contents.tmdb_score','>=',$minr)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>',$miny)     
					->select('contents.*')
					->distinct('contents.content_id')
					->paginate($limit);
				}       
			}
			elseif($rated != "" && $genre!=""){
				if($minr==0 && $maxr==10){
					$contents = DB::table('content_providers')
					->leftJoin('contents','contents.content_id','content_providers.content_id')
					->leftJoin('content_genres','content_genres.content_id','contents.content_id')
					->where('content_providers.provider_id',$provider)
					->where('content_genres.genre_id',$genre)
					->where('contents.age_certification',$rated)
					->whereIn('contents.content_type',$filter_type)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>',$miny)
					->orderBy('content_genres.content_id','DESC')
					->select('contents.*')
					->distinct('contents.content_id')
					->paginate($limit);
				}
				else{
					$contents = DB::table('content_providers')
					->leftJoin('contents','contents.content_id','content_providers.content_id')
					->leftJoin('content_genres','content_genres.content_id','contents.content_id')
					->where('content_providers.provider_id',$provider)
					->where('content_genres.genre_id',$genre)
					->where('contents.age_certification',$rated)
					->whereIn('contents.content_type',$filter_type)
					->where('contents.tmdb_score','<=',$maxr)
					->where('contents.tmdb_score','>=',$minr)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>',$miny)
					->orderBy('content_genres.content_id','DESC')
					->select('contents.*')
					->distinct('contents.content_id')
					->paginate($limit);
				}        
			}
			elseif($langs != "" && $genre!=""){
				if($minr==0 && $maxr==10){
					$contents = DB::table('content_providers')
					->leftJoin('contents','contents.content_id','content_providers.content_id')
					->leftJoin('content_genres','content_genres.content_id','contents.content_id')
					->where('content_providers.provider_id',$provider)
					->where('content_genres.genre_id',$genre)
					->whereIn('contents.content_type',$filter_type)
					->where('contents.original_language',$langs)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>',$miny)
					->orderBy('content_genres.content_id','DESC')
					->select('contents.*')
					->distinct('contents.content_id')
					->paginate($limit);
				}
				else{
					$contents = DB::table('content_providers')
					->leftJoin('contents','contents.content_id','content_providers.content_id')
					->leftJoin('content_genres','content_genres.content_id','contents.content_id')
					->where('content_providers.provider_id',$provider)
					->where('content_genres.genre_id',$genre)
					->whereIn('contents.content_type',$filter_type)
					->where('contents.original_language',$langs)
					->where('contents.tmdb_score','<=',$maxr)
					->where('contents.tmdb_score','>=',$minr)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>',$miny)
					->orderBy('content_genres.content_id','DESC')
					->select('contents.*')
					->distinct('contents.content_id')
					->paginate($limit);
				}       
			}
			elseif($langs != ""){
				if($minr==0 && $maxr==10){
					$contents = DB::table('content_providers')
					->leftJoin('contents','contents.content_id','content_providers.content_id')
					->where('content_providers.provider_id',$provider)
					->whereIn('contents.content_type',$filter_type)
					->where('contents.original_language',$langs)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>',$miny)     
					->select('contents.*')
					->distinct('contents.content_id')
					->paginate($limit);
				}
				else{
					$contents = DB::table('content_providers')
					->leftJoin('contents','contents.content_id','content_providers.content_id')
					->where('content_providers.provider_id',$provider)
					->whereIn('contents.content_type',$filter_type)
					->where('contents.original_language',$langs)
					->where('contents.tmdb_score','<=',$maxr)
					->where('contents.tmdb_score','>=',$minr)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>',$miny)     
					->select('contents.*')
					->distinct('contents.content_id')
					->paginate($limit);
				}       
			}
			elseif($genre!=""){
				if($minr==0 && $maxr==10){
					$contents = DB::table('content_providers')
					->leftJoin('contents','contents.content_id','content_providers.content_id')
					->leftJoin('content_genres','content_genres.content_id','contents.content_id')
					->where('content_providers.provider_id',$provider)
					->where('content_genres.genre_id',$genre)
					->whereIn('contents.content_type',$filter_type)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>',$miny)
					->orderBy('content_genres.content_id','DESC')
					->select('contents.*')
					->distinct('contents.content_id')
					->paginate($limit);
				}
				else{
					$contents = DB::table('content_providers')
					->leftJoin('contents','contents.content_id','content_providers.content_id')
					->leftJoin('content_genres','content_genres.content_id','contents.content_id')
					->where('content_providers.provider_id',$provider)
					->where('content_genres.genre_id',$genre)
					->whereIn('contents.content_type',$filter_type)
					->where('contents.tmdb_score','<=',$maxr)
					->where('contents.tmdb_score','>=',$minr)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>',$miny)
					->orderBy('content_genres.content_id','DESC')
					->select('contents.*')
					->distinct('contents.content_id')
					->paginate($limit);
				}  
			}
			elseif($rated != ""){


				if($minr==0 && $maxr==10){
					$contents = DB::table('content_providers')
					->leftJoin('contents','contents.content_id','content_providers.content_id')
					->where('content_providers.provider_id',$provider)
					->whereIn('contents.content_type',$filter_type)
					->where('contents.age_certification',$rated)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>',$miny)     
					->select('contents.*')
					->distinct('contents.content_id')
					->paginate($limit);  
				}
				else{
					$contents = DB::table('content_providers')
					->leftJoin('contents','contents.content_id','content_providers.content_id')
					->where('content_providers.provider_id',$provider)
					->whereIn('contents.content_type',$filter_type)
					->where('contents.age_certification',$rated)
					->where('contents.tmdb_score','<=',$maxr)
					->where('contents.tmdb_score','>=',$minr)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>',$miny)     
					->select('contents.*')
					->distinct('contents.content_id')
					->paginate($limit);
				} 
			}
			else{
				if($minr==0 && $maxr==10){
					$contents = DB::table('content_providers')
					->leftJoin('contents','contents.content_id','content_providers.content_id')
					->where('content_providers.provider_id',$provider)
					->whereIn('contents.content_type',$filter_type)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>',$miny)     
					->select('contents.*')
					->distinct('contents.content_id')
					->paginate($limit);
				}
				else{
					$contents = DB::table('content_providers')
					->leftJoin('contents','contents.content_id','content_providers.content_id')
					->where('content_providers.provider_id',$provider)
					->whereIn('contents.content_type',$filter_type)
					->where('contents.tmdb_score','<=',$maxr)
					->where('contents.tmdb_score','>=',$minr)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>',$miny)     
					->select('contents.*')
					->distinct('contents.content_id')
					->paginate($limit);
				}
			}
		}
		else{

			if($langs!="" && $rated!="" && $genre!=""){
				if($minr==0 && $maxr==10){
					$contents = DB::table('content_providers')
					->leftJoin('contents','contents.content_id','content_providers.content_id')
					->leftJoin('content_genres','content_genres.content_id','contents.content_id')
					->where('content_providers.provider_id',$provider)
					->where('content_genres.genre_id',$genre)
					->where('contents.age_certification',$rated)
					->whereIn('contents.content_type',$filter_type)
					->where('contents.original_language',$langs)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>',$miny)
					->orderBy($field,$sort_type)
					->select('contents.*')
					->distinct('contents.content_id')
					->paginate($limit);
				}
				else{
					$contents = DB::table('content_genres')
					->leftJoin('contents','content_genres.content_id','contents.content_id')
					->where('content_genres.genre_id',$genre)
					->whereIn('contents.content_type',$filter_type)
					->where('contents.age_certification',$rated)
					->where('contents.original_language',$langs)
					->where('contents.tmdb_score','<=',$maxr)
					->where('contents.tmdb_score','>=',$minr)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>',$miny)
					->orderBy($field,$sort_type)
					->paginate($limit);
				}   
			}
			elseif($langs!="" && $rated != ""){
				if($minr==0 && $maxr==10){
					$contents = DB::table('content_providers')
					->leftJoin('contents','contents.content_id','content_providers.content_id')
					->where('content_providers.provider_id',$provider)
					->whereIn('contents.content_type',$filter_type)
					->where('contents.original_language',$langs)
					->where('contents.age_certification',$rated)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>',$miny)     
					->orderBy($field,$sort_type)
					->select('contents.*')
					->distinct('contents.content_id')
					->paginate($limit); 
				}
				else{
					$contents = DB::table('content_providers')
					->leftJoin('contents','contents.content_id','content_providers.content_id')
					->where('content_providers.provider_id',$provider)
					->whereIn('contents.content_type',$filter_type)
					->where('contents.original_language',$langs)
					->where('contents.age_certification',$rated)
					->where('contents.tmdb_score','<=',$maxr)
					->where('contents.tmdb_score','>=',$minr)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>',$miny)     
					->orderBy($field,$sort_type)
					->select('contents.*')
					->distinct('contents.content_id')
					->paginate($limit); 
				}

			}
			elseif($rated != "" && $genre!=""){
				if($minr==0 && $maxr==10){
					$contents = DB::table('content_providers')
					->leftJoin('contents','contents.content_id','content_providers.content_id')
					->leftJoin('content_genres','content_genres.content_id','contents.content_id')
					->where('content_providers.provider_id',$provider)
					->where('content_genres.genre_id',$genre)
					->where('contents.age_certification',$rated)
					->whereIn('contents.content_type',$filter_type)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>',$miny)
					->orderBy($field,$sort_type)
					->select('contents.*')
					->distinct('contents.content_id')
					->paginate($limit);
				}
				else{
					$contents = DB::table('content_providers')
					->leftJoin('contents','contents.content_id','content_providers.content_id')
					->leftJoin('content_genres','content_genres.content_id','contents.content_id')
					->where('content_providers.provider_id',$provider)
					->where('content_genres.genre_id',$genre)
					->where('contents.age_certification',$rated)
					->whereIn('contents.content_type',$filter_type)
					->where('contents.tmdb_score','<=',$maxr)
					->where('contents.tmdb_score','>=',$minr)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>',$miny)
					->orderBy($field,$sort_type)
					->select('contents.*')
					->distinct('contents.content_id')
					->paginate($limit);
				}      
			}
			elseif($langs != "" && $genre!=""){
				if($minr==0 && $maxr==10){
					$contents = DB::table('content_providers')
					->leftJoin('contents','contents.content_id','content_providers.content_id')
					->leftJoin('content_genres','content_genres.content_id','contents.content_id')
					->where('content_providers.provider_id',$provider)
					->where('content_genres.genre_id',$genre)
					->whereIn('contents.content_type',$filter_type)
					->where('contents.original_language',$langs)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>',$miny)
					->orderBy($field,$sort_type)
					->select('contents.*')
					->distinct('contents.content_id')
					->paginate($limit);
				}
				else{
					$contents = DB::table('content_providers')
					->leftJoin('contents','contents.content_id','content_providers.content_id')
					->leftJoin('content_genres','content_genres.content_id','contents.content_id')
					->where('content_providers.provider_id',$provider)
					->where('content_genres.genre_id',$genre)
					->whereIn('contents.content_type',$filter_type)
					->where('contents.original_language',$langs)
					->where('contents.tmdb_score','>=',$minr)
					->where('contents.tmdb_score','<=',$maxr)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>',$miny)
					->orderBy($field,$sort_type)
					->select('contents.*')
					->distinct('contents.content_id')
					->paginate($limit);
				}   
			}
			elseif($langs != ""){
				if($minr==0 && $maxr==10){
					$contents = DB::table('content_providers')
					->leftJoin('contents','contents.content_id','content_providers.content_id')
					->where('content_providers.provider_id',$provider)
					->whereIn('contents.content_type',$filter_type)
					->where('contents.original_language',$langs)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>',$miny)     
					->orderBy($field,$sort_type)
					->select('contents.*')
					->distinct('contents.content_id')
					->paginate($limit);
				}
				else{
					$contents = DB::table('content_providers')
					->leftJoin('contents','contents.content_id','content_providers.content_id')
					->where('content_providers.provider_id',$provider)
					->whereIn('contents.content_type',$filter_type)
					->where('contents.original_language',$langs)
					->where('contents.tmdb_score','<=',$maxr)
					->where('contents.tmdb_score','>=',$minr)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>',$miny)     
					->orderBy($field,$sort_type)
					->select('contents.*')
					->distinct('contents.content_id')
					->paginate($limit);
				}     
			}
			elseif($genre!=""){
				if($minr==0 && $maxr==10){
					$contents = DB::table('content_providers')
					->leftJoin('contents','contents.content_id','content_providers.content_id')
					->leftJoin('content_genres','content_genres.content_id','contents.content_id')
					->where('content_providers.provider_id',$provider)
					->where('content_genres.genre_id',$genre)
					->whereIn('contents.content_type',$filter_type)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>',$miny)
					->orderBy($field,$sort_type)
					->select('contents.*')
					->distinct('contents.content_id')
					->paginate($limit);
				}
				else{
					$contents = DB::table('content_providers')
					->leftJoin('contents','contents.content_id','content_providers.content_id')
					->leftJoin('content_genres','content_genres.content_id','contents.content_id')
					->where('content_providers.provider_id',$provider)
					->where('content_genres.genre_id',$genre)
					->whereIn('contents.content_type',$filter_type)
					->where('contents.tmdb_score','>=',$minr)
					->where('contents.tmdb_score','<=',$maxr)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>',$miny)
					->orderBy($field,$sort_type)
					->select('contents.*')
					->distinct('contents.content_id')
					->paginate($limit);
				}  
			}
			elseif($rated != ""){
				if($minr==0 && $maxr==10){
					$contents = DB::table('content_providers')
					->leftJoin('contents','contents.content_id','content_providers.content_id')
					->where('content_providers.provider_id',$provider)
					->whereIn('contents.content_type',$filter_type)
					->where('contents.age_certification',$rated)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>',$miny)     
					->orderBy($field,$sort_type)
					->select('contents.*')
					->distinct('contents.content_id')
					->paginate($limit);
				}
				else{
					$contents = DB::table('content_providers')
					->leftJoin('contents','contents.content_id','content_providers.content_id')
					->where('content_providers.provider_id',$provider)
					->whereIn('contents.content_type',$filter_type)
					->where('contents.original_language',$langs)
					->where('contents.tmdb_score','<=',$maxr)
					->where('contents.tmdb_score','>=',$minr)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>',$miny)     
					->orderBy($field,$sort_type)
					->select('contents.*')
					->distinct('contents.content_id')
					->paginate($limit);
				}   
			}
			else{
				if($minr==0 && $maxr==10){
					$contents = DB::table('content_providers')
					->leftJoin('contents','contents.content_id','content_providers.content_id')
					->where('content_providers.provider_id',$provider)
					->whereIn('contents.content_type',$filter_type)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>',$miny)     
					->orderBy($field,$sort_type)
					->select('contents.*')
					->distinct('contents.content_id')
					->paginate($limit);
				}
				else{
					$contents = DB::table('content_providers')
					->leftJoin('contents','contents.content_id','content_providers.content_id')
					->where('content_providers.provider_id',$provider)
					->whereIn('contents.content_type',$filter_type)
					->where('contents.tmdb_score','<=',$maxr)
					->where('contents.tmdb_score','>=',$minr)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>',$miny)     
					->orderBy($field,$sort_type)
					->select('contents.*')
					->distinct('contents.content_id')
					->paginate($limit);
				}  
			}
		}

	}

}
$miny = explode('-',$miny)[0];
$maxy = explode('-',$maxy)[0];

		if(isset($_GET['provider'])){  $provider = $_GET['provider'];  }
		foreach ($contents as $key => $value) {
			if (!file_exists(public_path().'/poster/'.$value->poster)){
				$contents[$key]->poster = 'default.png';
			}
		}
		return response()->json(compact('contents'));
	      // return view('khoj.home',);
	}


	function newest(Request $request){ 
		if (isset($request->limit) && $request->limit > 0){
			$limit = $request->limit;
		}
		else
			$limit = 30;

		$date = date('Y-m-d');
	$page = 0;
	$langs = "";
	$rated = "";
	$sort = "";
	$field = "";
	$sort_type = "";
	$provider = "";
	$genre = "";
	$minr=0;
	$maxr=10; 
	$type="all";
	$tab = 'New';

  // $miny = DB::table('contents')->where('original_release_year','>','1900')->min('original_release_year');
	$miny = '0000-00-00';
	$maxy = $date;

	if(isset($_GET['page'])){
		$page = $_GET['page']-1;
	}  

	if(isset($_GET['lang'])){  $langs = $_GET['lang'];  }
	if(isset($_GET['provider'])){  $provider = explode('_',$_GET['provider'])[1];  }

	if(isset($_GET['sort'])){
		$sort = $_GET['sort'];
		if($sort == "alpha"){
			$field = 'title';
			$sort_type = 'ASC';
		}
		elseif($sort == 'rate'){
			$field = 'tmdb_score';
			$sort_type = 'DESC';
		}
		elseif($sort == 'year'){
			$field = 'cinema_release_date';
			$sort_type = 'DESC';
		}
		elseif($sort == 'imdb_score'){
			$field = 'imdb_score';
			$sort_type = 'DESC';
		}
		else{
			$field = 'content_id';
			$sort_type = 'DESC';
		}
	}

	if( (isset($_GET['sort']) && isset($_GET['genre'])) || (isset($_GET['sort']) && isset($_GET['provider']))){
		$sort = $_GET['sort'];
		if($sort == "alpha"){
			$field = 'contents.title';
			$sort_type = 'ASC';
		}
		elseif($sort == 'rate'){
			$field = 'contents.tmdb_score';
			$sort_type = 'DESC';
		}
		elseif($sort == 'year'){
			$field = 'contents.cinema_release_date';
			$sort_type = 'DESC';
		}
		elseif($sort == 'imdb_score'){
			$field = 'contents.imdb_score';
			$sort_type = 'DESC';
		}
		else{
			$field = 'contents.content_id';
			$sort_type = 'DESC';
		}
	}

	if(isset($_GET['rated'])){
		$rated = $_GET['rated'];
	}

	if(isset($_GET['searchtype'])){
		$type = $_GET['searchtype'];
	}

	if(isset($_GET['genre'])){
		$genre = $_GET['genre'];
	}

	if(isset($_GET['minr'])){
		$minr = $_GET['minr'];
	}
	if(isset($_GET['maxr'])){
		$maxr = $_GET['maxr'];    
	}

	if(isset($_GET['miny'])){
		$miny = $_GET['miny'].'-01-01';
	}
	if(isset($_GET['maxy'])){
		$maxy = $_GET['maxy']."-12-31";
	}

	if($type == 'all'){
		if($provider == ""){
			if($sort == ""){

				if($langs!="" && $rated!="" && $genre!=""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('content_genres')
						->leftJoin('contents','content_genres.content_id','contents.content_id')
						->where('content_genres.genre_id',$genre)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.age_certification',$rated)
						->where('contents.original_language',$langs)
						->where('contents.parent_id','0')
						->orderBy('contents.cinema_release_date','DESC')
						->orderBy('contents.localized_release_date','DESC')
						->paginate($limit);
					}
					else{
						$contents = DB::table('content_genres')
						->leftJoin('contents','content_genres.content_id','contents.content_id')
						->where('content_genres.genre_id',$genre)
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.age_certification',$rated)
						->where('contents.original_language',$langs)
						->where('contents.parent_id','0')
						->orderBy('contents.cinema_release_date','DESC')
						->orderBy('contents.localized_release_date','DESC')
						->paginate($limit);
					}   
				}
				elseif($langs!="" && $rated != ""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('contents')
						->where('original_language',$langs)
						->where('age_certification',$rated)
						->where('cinema_release_date','<=',$maxy)
						->where('cinema_release_date','>=',$miny)     
						->where('parent_id','0')                  
						->orderBy('cinema_release_date','DESC')
						->orderBy('localized_release_date','DESC')
						->paginate($limit);
					}
					else{
						$contents = DB::table('contents')
						->where('original_language',$langs)
						->where('age_certification',$rated)
						->where('tmdb_score','<=',$maxr)
						->where('tmdb_score','>=',$minr)
						->where('cinema_release_date','<=',$maxy)
						->where('cinema_release_date','>=',$miny)     
						->where('parent_id','0')                  
						->orderBy('cinema_release_date','DESC')
						->orderBy('localized_release_date','DESC')
						->paginate($limit);
					}           
				}
				elseif($rated != "" && $genre!=""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('content_genres')
						->leftJoin('contents','content_genres.content_id','contents.content_id')
						->where('content_genres.genre_id',$genre)
						->where('contents.age_certification',$rated)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)    
						->where('contents.parent_id','0')
						->orderBy('contents.cinema_release_date','DESC')
						->orderBy('contents.localized_release_date','DESC')
						->paginate($limit);
					}
					else{
						$contents = DB::table('content_genres')
						->leftJoin('contents','content_genres.content_id','contents.content_id')
						->where('content_genres.genre_id',$genre)
						->where('contents.age_certification',$rated)
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)    
						->where('contents.parent_id','0')   
						->orderBy('contents.cinema_release_date','DESC')
						->orderBy('contents.localized_release_date','DESC')
						->paginate($limit);
					}   
				}
				elseif($langs != "" && $genre!=""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('content_genres')
						->leftJoin('contents','content_genres.content_id','contents.content_id')
						->where('content_genres.genre_id',$genre)
						->where('contents.original_language',$langs)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.parent_id','0')
						->orderBy('contents.cinema_release_date','DESC')
						->orderBy('contents.localized_release_date','DESC')
						->paginate($limit);
					}
					else{
						$contents = DB::table('content_genres')
						->leftJoin('contents','content_genres.content_id','contents.content_id')
						->where('content_genres.genre_id',$genre)
						->where('contents.original_language',$langs)
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.parent_id','0')
						->orderBy('contents.cinema_release_date','DESC')
						->orderBy('contents.localized_release_date','DESC')
						->paginate($limit);
					}            
				}
				elseif($langs != ""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('contents')
						->where('original_language',$langs)
						->where('cinema_release_date','<=',$maxy)
						->where('cinema_release_date','>=',$miny)
						->where('parent_id','0')
						->orderBy('cinema_release_date','DESC')
						->orderBy('localized_release_date','DESC')
						->paginate($limit);
					}
					else{
						$contents = DB::table('contents')
						->where('original_language',$langs)
						->where('tmdb_score','<=',$maxr)
						->where('tmdb_score','>=',$minr)
						->where('cinema_release_date','<=',$maxy)
						->where('cinema_release_date','>=',$miny)
						->where('parent_id','0')
						->orderBy('cinema_release_date','DESC')
						->orderBy('localized_release_date','DESC')
						->paginate($limit);
					}           
				}
				elseif($genre!=""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('content_genres')
						->leftJoin('contents','content_genres.content_id','contents.content_id')
						->where('content_genres.genre_id',$genre)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.parent_id','0')
						->orderBy('content_genres.content_id','DESC')
						->orderBy('contents.cinema_release_date','DESC')
						->orderBy('contents.localized_release_date','DESC')
						->paginate($limit);
					}
					else{
						$contents = DB::table('content_genres')
						->leftJoin('contents','content_genres.content_id','contents.content_id')
						->where('content_genres.genre_id',$genre)
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.parent_id','0')
						->orderBy('contents.cinema_release_date','DESC')
						->orderBy('contents.localized_release_date','DESC')
						->paginate($limit);
					}            
				}
				elseif($rated != ""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('contents')
						->where('age_certification',$rated)                
						->where('cinema_release_date','<=',$maxy)
						->where('cinema_release_date','>=',$miny)
						->where('parent_id','0')
						->orderBy('cinema_release_date','DESC')
						->orderBy('localized_release_date','DESC')
						->paginate($limit);
					}
					else{
						$contents = DB::table('contents')
						->where('age_certification',$rated)                
						->where('tmdb_score','<=',$maxr)
						->where('tmdb_score','>=',$minr)
						->where('cinema_release_date','<=',$maxy)
						->where('cinema_release_date','>=',$miny)
						->where('parent_id','0')
						->orderBy('cinema_release_date','DESC')
						->orderBy('localized_release_date','DESC')
						->paginate($limit);
					}            
				}
				else{
					if($minr==0 && $maxr==10){
						$contents = DB::table('contents')
						->where('cinema_release_date','<=',$maxy)
						->where('cinema_release_date','>=',$miny)
						->where('parent_id','0')
						->orderBy('content_id','DESC')
						->orderBy('cinema_release_date','DESC')
						->orderBy('localized_release_date','DESC')
						->paginate($limit);
					}
					else{
						$contents = DB::table('contents')
						->where('tmdb_score','<=',$maxr)
						->where('tmdb_score','>=',$minr)
						->where('cinema_release_date','<=',$maxy)
						->where('cinema_release_date','>=',$miny)
						->where('parent_id','0')
						->orderBy('content_id','DESC')
						->orderBy('cinema_release_date','DESC')
						->orderBy('localized_release_date','DESC')
						->paginate($limit);
					}  
				}
			}
			else{

				if($langs!="" && $rated!="" && $genre!=""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('content_genres')
						->leftJoin('contents','content_genres.content_id','contents.content_id')
						->where('content_genres.genre_id',$genre)
						->where('contents.age_certification',$rated)
						->where('contents.original_language',$langs)
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.parent_id','0')
						->orderBy('contents.cinema_release_date','DESC')
						->orderBy('contents.localized_release_date','DESC')
						->orderBy($field,$sort_type)
						->paginate($limit);
					}
					else{
						$contents = DB::table('content_genres')
						->leftJoin('contents','content_genres.content_id','contents.content_id')
						->where('content_genres.genre_id',$genre)
						->where('contents.age_certification',$rated)
						->where('contents.original_language',$langs)
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.parent_id','0')
						->orderBy('contents.cinema_release_date','DESC')
						->orderBy('contents.localized_release_date','DESC')
						->orderBy($field,$sort_type)
						->paginate($limit);
					}          
				}
				elseif($langs!="" && $rated != ""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('contents')
						->where('original_language',$langs)
						->where('age_certification',$rated)                
						->where('cinema_release_date','<=',$maxy)
						->where('cinema_release_date','>=',$miny)
						->where('parent_id','0')
						->orderBy('cinema_release_date','DESC')
						->orderBy('localized_release_date','DESC')
						->orderBy($field,$sort_type)
						->paginate($limit); 
					}
					else{
						$contents = DB::table('contents')
						->where('original_language',$langs)
						->where('age_certification',$rated)                
						->where('tmdb_score','<=',$maxr)
						->where('tmdb_score','>=',$minr)
						->where('cinema_release_date','<=',$maxy)
						->where('cinema_release_date','>=',$miny)
						->where('parent_id','0')
						->orderBy('cinema_release_date','DESC')
						->orderBy('localized_release_date','DESC')
						->orderBy($field,$sort_type)
						->paginate($limit); 
					}        
				}
				elseif($rated != "" && $genre!=""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('content_genres')
						->leftJoin('contents','content_genres.content_id','contents.content_id')
						->where('content_genres.genre_id',$genre)
						->where('contents.age_certification',$rated)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.parent_id','0')
						->orderBy('contents.cinema_release_date','DESC')
						->orderBy('contents.localized_release_date','DESC')
						->orderBy($field,$sort_type)
						->paginate($limit);
					}
					else{
						$contents = DB::table('content_genres')
						->leftJoin('contents','content_genres.content_id','contents.content_id')
						->where('content_genres.genre_id',$genre)
						->where('contents.age_certification',$rated)
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.parent_id','0')
						->orderBy('contents.cinema_release_date','DESC')
						->orderBy('contents.localized_release_date','DESC')
						->orderBy($field,$sort_type)
						->paginate($limit);
					}          
				}
				elseif($langs != "" && $genre!=""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('content_genres')
						->leftJoin('contents','content_genres.content_id','contents.content_id')
						->where('content_genres.genre_id',$genre)
						->where('contents.original_language',$langs)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.parent_id','0')
						->orderBy('contents.cinema_release_date','DESC')
						->orderBy('contents.localized_release_date','DESC')
						->orderBy($field,$sort_type)
						->paginate($limit);
					}
					else{
						$contents = DB::table('content_genres')
						->leftJoin('contents','content_genres.content_id','contents.content_id')
						->where('content_genres.genre_id',$genre)
						->where('contents.original_language',$langs)
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.parent_id','0')
						->orderBy('contents.cinema_release_date','DESC')
						->orderBy('contents.localized_release_date','DESC')
						->orderBy($field,$sort_type)
						->paginate($limit);
					}          
				}
				elseif($langs != ""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('contents')
						->where('original_language',$langs)
						->where('cinema_release_date','<=',$maxy)
						->where('cinema_release_date','>=',$miny)
						->where('parent_id','0')
						->orderBy('cinema_release_date','DESC')
						->orderBy('localized_release_date','DESC')
						->orderBy($field,$sort_type)
						->paginate($limit);
					}
					else{
						$contents = DB::table('contents')
						->where('original_language',$langs)
						->where('tmdb_score','<=',$maxr)
						->where('tmdb_score','>=',$minr)
						->where('cinema_release_date','<=',$maxy)
						->where('cinema_release_date','>=',$miny)
						->where('parent_id','0')
						->orderBy('cinema_release_date','DESC')
						->orderBy('localized_release_date','DESC')
						->orderBy($field,$sort_type)
						->paginate($limit);
					}         
				}
				elseif($genre!=""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('content_genres')
						->leftJoin('contents','content_genres.content_id','contents.content_id')
						->where('content_genres.genre_id',$genre)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.parent_id','0')
						->orderBy('contents.cinema_release_date','DESC')
						->orderBy('contents.localized_release_date','DESC')
						->orderBy($field,$sort_type)
						->paginate($limit);
					}
					else{
						$contents = DB::table('content_genres')
						->leftJoin('contents','content_genres.content_id','contents.content_id')
						->where('content_genres.genre_id',$genre)
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.parent_id','0')
						->orderBy('contents.cinema_release_date','DESC')
						->orderBy('contents.localized_release_date','DESC')
						->orderBy($field,$sort_type)
						->paginate($limit);
					}   
				}
				elseif($rated != ""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('contents')
						->where('age_certification',$rated)                
						->where('cinema_release_date','<=',$maxy)
						->where('cinema_release_date','>=',$miny)
						->where('parent_id','0')
						->orderBy('cinema_release_date','DESC')
						->orderBy('localized_release_date','DESC')
						->orderBy($field,$sort_type)
						->paginate($limit);
					}
					else{
						$contents = DB::table('contents')
						->where('age_certification',$rated)                
						->where('tmdb_score','<=',$maxr)
						->where('tmdb_score','>=',$minr)
						->where('cinema_release_date','<=',$maxy)
						->where('cinema_release_date','>=',$miny)
						->where('parent_id','0')
						->orderBy('cinema_release_date','DESC')
						->orderBy('localized_release_date','DESC')
						->orderBy($field,$sort_type)
						->paginate($limit);
					}          
				}
				else{
					if($minr==0 && $maxr==10){
						$contents = DB::table('contents')
						->where('cinema_release_date','<=',$maxy)
						->where('cinema_release_date','>=',$miny)
						->where('parent_id','0')
						->orderBy('cinema_release_date','DESC')
						->orderBy('localized_release_date','DESC')
						->orderBy($field,$sort_type)
						->paginate($limit);  
					}
					else{
						$contents = DB::table('contents')
						->where('tmdb_score','<=',$maxr)
						->where('tmdb_score','>=',$minr)
						->where('cinema_release_date','<=',$maxy)
						->where('cinema_release_date','>=',$miny)
						->where('parent_id','0')
						->orderBy('cinema_release_date','DESC')
						->orderBy('localized_release_date','DESC')
						->orderBy($field,$sort_type)
						->paginate($limit);  
					}
				}
			}
		}

  //When Provider Not NULL
		else{
			if($sort == ""){
				if($langs!="" && $rated!="" && $genre!=""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->leftJoin('content_genres','content_genres.content_id','contents.content_id')
						->where('content_providers.provider_id',$provider)
						->where('content_genres.genre_id',$genre)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.age_certification',$rated)
						->where('contents.original_language',$langs)
						->where('contents.parent_id','0')
						->orderBy('contents.cinema_release_date','DESC')
						->orderBy('contents.localized_release_date','DESC')
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate($limit);
					}
					else{
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->leftJoin('content_genres','content_genres.content_id','contents.content_id')
						->where('content_providers.provider_id',$provider)
						->where('content_genres.genre_id',$genre)
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.age_certification',$rated)
						->where('contents.original_language',$langs)
						->where('contents.parent_id','0')
						->orderBy('contents.cinema_release_date','DESC')
						->orderBy('contents.localized_release_date','DESC')
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate($limit);
					}   
				}
				elseif($langs!="" && $rated != ""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->where('content_providers.provider_id',$provider)
						->where('contents.original_language',$langs)
						->where('contents.age_certification',$rated)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)     
						->where('contents.parent_id','0')  
						->orderBy('contents.cinema_release_date','DESC')
						->orderBy('contents.localized_release_date','DESC')
						->select('contents.*')
						->distinct('contents.content_id')               
						->paginate($limit);
					}
					else{
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->where('content_providers.provider_id',$provider)
						->where('contents.original_language',$langs)
						->where('contents.age_certification',$rated)
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)     
						->where('contents.parent_id','0')   
						->orderBy('contents.cinema_release_date','DESC')
						->orderBy('contents.localized_release_date','DESC')
						->select('contents.*')
						->distinct('contents.content_id')              
						->paginate($limit);
					}           
				}
				elseif($rated != "" && $genre!=""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->leftJoin('content_genres','content_genres.content_id','contents.content_id')
						->where('content_providers.provider_id',$provider)
						->where('content_genres.genre_id',$genre)
						->where('contents.age_certification',$rated)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.parent_id','0')
						->orderBy('contents.cinema_release_date','DESC')
						->orderBy('contents.localized_release_date','DESC')
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate($limit);
					}
					else{
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->leftJoin('content_genres','content_genres.content_id','contents.content_id')
						->where('content_providers.provider_id',$provider)
						->where('content_genres.genre_id',$genre)
						->where('contents.age_certification',$rated)
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.parent_id','0')
						->orderBy('contents.cinema_release_date','DESC')
						->orderBy('contents.localized_release_date','DESC')
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate($limit);
					}   
				}
				elseif($langs != "" && $genre!=""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->leftJoin('content_genres','content_genres.content_id','contents.content_id')
						->where('content_providers.provider_id',$provider)
						->where('content_genres.genre_id',$genre)
						->where('contents.original_language',$langs)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.parent_id','0')
						->orderBy('contents.cinema_release_date','DESC')
						->orderBy('contents.localized_release_date','DESC')
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate($limit);
					}
					else{
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->leftJoin('content_genres','content_genres.content_id','contents.content_id')
						->where('content_providers.provider_id',$provider)
						->where('content_genres.genre_id',$genre)
						->where('contents.original_language',$langs)
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.parent_id','0')
						->orderBy('contents.cinema_release_date','DESC')
						->orderBy('contents.localized_release_date','DESC')
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate($limit);
					}            
				}
				elseif($langs != ""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->where('content_providers.provider_id',$provider)
						->where('contents.original_language',$langs)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)     
						->where('contents.parent_id','0')     
						->orderBy('contents.cinema_release_date','DESC')
						->orderBy('contents.localized_release_date','DESC')
						->select('contents.*')
						->distinct('contents.content_id')            
						->paginate($limit);
					}
					else{
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->where('content_providers.provider_id',$provider)
						->where('contents.original_language',$langs)
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)     
						->where('contents.parent_id','0')    
						->orderBy('contents.cinema_release_date','DESC')
						->orderBy('contents.localized_release_date','DESC')
						->select('contents.*')
						->distinct('contents.content_id')             
						->paginate($limit);
					}           
				}
				elseif($genre!=""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->leftJoin('content_genres','content_genres.content_id','contents.content_id')
						->where('content_providers.provider_id',$provider)
						->where('content_genres.genre_id',$genre)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.parent_id','0')
						->orderBy('contents.cinema_release_date','DESC')
						->orderBy('contents.localized_release_date','DESC')
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate($limit);
					}
					else{
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->leftJoin('content_genres','content_genres.content_id','contents.content_id')
						->where('content_providers.provider_id',$provider)
						->where('content_genres.genre_id',$genre)
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.parent_id','0')
						->orderBy('contents.cinema_release_date','DESC')
						->orderBy('contents.localized_release_date','DESC')
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate($limit);
					}            
				}
				elseif($rated != ""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->where('content_providers.provider_id',$provider)
						->where('contents.age_certification',$rated)                
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)     
						->where('contents.parent_id','0')    
						->orderBy('contents.cinema_release_date','DESC')
						->orderBy('contents.localized_release_date','DESC')
						->select('contents.*')
						->distinct('contents.content_id')             
						->paginate($limit);
					}
					else{
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->where('content_providers.provider_id',$provider)
						->where('contents.age_certification',$rated)                
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)     
						->where('contents.parent_id','0')    
						->orderBy('contents.cinema_release_date','DESC')
						->orderBy('contents.localized_release_date','DESC')
						->select('contents.*')
						->distinct('contents.content_id')             
						->paginate($limit);
					}            
				}
				else{
					if($minr==0 && $maxr==10){
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->where('content_providers.provider_id',$provider)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)     
						->where('contents.parent_id','0')    
						->orderBy('contents.cinema_release_date','DESC')
						->orderBy('contents.localized_release_date','DESC')
						->select('contents.*')
						->distinct('contents.content_id')             
						->paginate($limit);
					}
					else{
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->where('content_providers.provider_id',$provider)
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)     
						->where('contents.parent_id','0')                 
						->orderBy('contents.cinema_release_date','DESC')
						->orderBy('contents.localized_release_date','DESC')
						->orderBy($field,$sort_type)
						->orderBy($field,$sort_type)
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate($limit);
					}  
				}
			}
			else{

				if($langs!="" && $rated!="" && $genre!=""){

					if($minr==0 && $maxr==10){
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->leftJoin('content_genres','content_genres.content_id','contents.content_id')
						->where('content_providers.provider_id',$provider)
						->where('content_genres.genre_id',$genre)
						->where('contents.age_certification',$rated)
						->where('contents.original_language',$langs)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.parent_id','0')
						->orderBy('contents.cinema_release_date','DESC')
						->orderBy('contents.localized_release_date','DESC')
						->orderBy($field,$sort_type)
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate($limit);
					}
					else{
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->leftJoin('content_genres','content_genres.content_id','contents.content_id')
						->where('content_providers.provider_id',$provider)
						->where('content_genres.genre_id',$genre)
						->where('contents.age_certification',$rated)
						->where('contents.original_language',$langs)
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.parent_id','0')
						->orderBy('contents.cinema_release_date','DESC')
						->orderBy('contents.localized_release_date','DESC')
						->orderBy($field,$sort_type)
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate($limit);
					}          
				}
				elseif($langs!="" && $rated != ""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->where('content_providers.provider_id',$provider)
						->where('contents.original_language',$langs)
						->where('contents.age_certification',$rated)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)     
						->where('contents.parent_id','0')                 
						->orderBy('contents.cinema_release_date','DESC')
						->orderBy('contents.localized_release_date','DESC')
						->orderBy($field,$sort_type)
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate($limit); 
					}
					else{
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->where('content_providers.provider_id',$provider)
						->where('contents.original_language',$langs)
						->where('contents.age_certification',$rated)
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)     
						->where('contents.parent_id','0')                 
						->orderBy('contents.cinema_release_date','DESC')
						->orderBy('contents.localized_release_date','DESC')
						->orderBy($field,$sort_type)
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate($limit); 
					}        
				}
				elseif($rated != "" && $genre!=""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->leftJoin('content_genres','content_genres.content_id','contents.content_id')
						->where('content_providers.provider_id',$provider)
						->where('content_genres.genre_id',$genre)
						->where('contents.age_certification',$rated)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.parent_id','0')
						->orderBy('contents.cinema_release_date','DESC')
						->orderBy('contents.localized_release_date','DESC')
						->orderBy($field,$sort_type)
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate($limit);
					}
					else{
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->leftJoin('content_genres','content_genres.content_id','contents.content_id')
						->where('content_providers.provider_id',$provider)
						->where('content_genres.genre_id',$genre)
						->where('contents.age_certification',$rated)
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.parent_id','0')
						->orderBy('contents.cinema_release_date','DESC')
						->orderBy('contents.localized_release_date','DESC')
						->orderBy($field,$sort_type)
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate($limit);
					}          
				}
				elseif($langs != "" && $genre!=""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->leftJoin('content_genres','content_genres.content_id','contents.content_id')
						->where('content_providers.provider_id',$provider)
						->where('content_genres.genre_id',$genre)
						->where('contents.original_language',$langs)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.parent_id','0')
						->orderBy('contents.cinema_release_date','DESC')
						->orderBy('contents.localized_release_date','DESC')
						->orderBy($field,$sort_type)
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate($limit);
					}
					else{
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->leftJoin('content_genres','content_genres.content_id','contents.content_id')
						->where('content_providers.provider_id',$provider)
						->where('content_genres.genre_id',$genre)
						->where('contents.original_language',$langs)
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.parent_id','0')
						->orderBy('contents.cinema_release_date','DESC')
						->orderBy('contents.localized_release_date','DESC')
						->orderBy($field,$sort_type)
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate($limit);
					}          
				}
				elseif($langs != ""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->where('content_providers.provider_id',$provider)
						->where('contents.original_language',$langs)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)     
						->where('contents.parent_id','0')                 
						->orderBy('contents.cinema_release_date','DESC')
						->orderBy('contents.localized_release_date','DESC')
						->orderBy($field,$sort_type)
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate($limit);
					}
					else{
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->where('content_providers.provider_id',$provider)
						->where('contents.original_language',$langs)
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)     
						->where('contents.parent_id','0')                 
						->orderBy('contents.cinema_release_date','DESC')
						->orderBy('contents.localized_release_date','DESC')
						->orderBy($field,$sort_type)
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate($limit);
					}         
				}
				elseif($genre!=""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->leftJoin('content_genres','content_genres.content_id','contents.content_id')
						->where('content_providers.provider_id',$provider)
						->where('content_genres.genre_id',$genre)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.parent_id','0')
						->orderBy('contents.cinema_release_date','DESC')
						->orderBy('contents.localized_release_date','DESC')
						->orderBy($field,$sort_type)
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate($limit);
					}
					else{
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->leftJoin('content_genres','content_genres.content_id','contents.content_id')
						->where('content_providers.provider_id',$provider)
						->where('content_genres.genre_id',$genre)
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.parent_id','0')
						->orderBy('contents.cinema_release_date','DESC')
						->orderBy('contents.localized_release_date','DESC')
						->orderBy($field,$sort_type)
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate($limit);
					}   
				}
				elseif($rated != ""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->where('content_providers.provider_id',$provider)
						->where('contents.age_certification',$rated)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)     
						->where('contents.parent_id','0')                 
						->orderBy('contents.cinema_release_date','DESC')
						->orderBy('contents.localized_release_date','DESC')
						->orderBy($field,$sort_type)
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate($limit);
					}
					else{
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->where('content_providers.provider_id',$provider)
						->where('contents.age_certification',$rated)
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)     
						->where('contents.parent_id','0')                 
						->orderBy('contents.cinema_release_date','DESC')
						->orderBy('contents.localized_release_date','DESC')
						->orderBy($field,$sort_type)
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate($limit);
					}          
				}
				else{
					if($minr==0 && $maxr==10){
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->where('content_providers.provider_id',$provider)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)     
						->where('contents.parent_id','0')                 
						->orderBy('contents.cinema_release_date','DESC')
						->orderBy('contents.localized_release_date','DESC')
						->orderBy($field,$sort_type)
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate($limit);
					}
					else{
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->where('content_providers.provider_id',$provider)
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)     
						->where('contents.parent_id','0')                 
						->orderBy('contents.cinema_release_date','DESC')
						->orderBy('contents.localized_release_date','DESC')
						->orderBy($field,$sort_type)
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate($limit);
					}
				}
			}

		}
	}

	else{

		$filter_type = array();
		if($type == 'movie'){
			$filter_type = ['movie'];
		}
		else{
			$filter_type = ['shows','tv-show','show'];        
		}
		if($provider == ""){

			if($sort == ""){
				if($langs!="" && $rated!="" && $genre!=""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('content_genres')
						->leftJoin('contents','content_genres.content_id','contents.content_id')
						->where('content_genres.genre_id',$genre)
						->whereIn('contents.content_type',$filter_type)
						->where('contents.age_certification',$rated)
						->where('contents.original_language',$langs)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->orderBy('contents.cinema_release_date','DESC')
						->orderBy('contents.localized_release_date','DESC')
						->paginate($limit);
					}
					else{
						$contents = DB::table('content_genres')
						->leftJoin('contents','content_genres.content_id','contents.content_id')
						->where('content_genres.genre_id',$genre)
						->whereIn('contents.content_type',$filter_type)
						->where('contents.age_certification',$rated)
						->where('contents.original_language',$langs)
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->orderBy('contents.cinema_release_date','DESC')
						->orderBy('contents.localized_release_date','DESC')
						->paginate($limit);
					}        
				}
				elseif($langs!="" && $rated != ""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('contents')
						->whereIn('content_type',$filter_type)
						->where('original_language',$langs)
						->where('age_certification',$rated)                
						->where('cinema_release_date','<=',$maxy)
						->where('cinema_release_date','>=',$miny)
						->orderBy('cinema_release_date','DESC')
						->orderBy('localized_release_date','DESC')
						->paginate($limit);
					}
					else{$contents = DB::table('contents')
						->whereIn('content_type',$filter_type)
					->where('original_language',$langs)
					->where('age_certification',$rated)                
					->where('tmdb_score','<=',$maxr)
					->where('tmdb_score','>=',$minr)
					->where('cinema_release_date','<=',$maxy)
					->where('cinema_release_date','>=',$miny)
					->orderBy('cinema_release_date','DESC')
					->orderBy('localized_release_date','DESC')
					->paginate($limit);
				}       
			}
			elseif($rated != "" && $genre!=""){
				if($minr==0 && $maxr==10){
					$contents = DB::table('content_genres')
					->leftJoin('contents','content_genres.content_id','contents.content_id')
					->where('content_genres.genre_id',$genre)
					->whereIn('contents.content_type',$filter_type)
					->where('contents.age_certification',$rated)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>=',$miny)
					->orderBy('contents.cinema_release_date','DESC')
					->orderBy('contents.localized_release_date','DESC')
					->paginate($limit);
				}
				else{
					$contents = DB::table('content_genres')
					->leftJoin('contents','content_genres.content_id','contents.content_id')
					->where('content_genres.genre_id',$genre)
					->whereIn('contents.content_type',$filter_type)
					->where('contents.age_certification',$rated)
					->where('contents.tmdb_score','<=',$maxr)
					->where('contents.tmdb_score','>=',$minr)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>=',$miny)
					->orderBy('contents.cinema_release_date','DESC')
					->orderBy('contents.localized_release_date','DESC')
					->paginate($limit);
				}        
			}
			elseif($langs != "" && $genre!=""){
				if($minr==0 && $maxr==10){
					$contents = DB::table('content_genres')
					->leftJoin('contents','content_genres.content_id','contents.content_id')
					->where('content_genres.genre_id',$genre)
					->whereIn('contents.content_type',$filter_type)
					->where('contents.original_language',$langs)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>=',$miny)
					->orderBy('contents.cinema_release_date','DESC')
					->orderBy('contents.localized_release_date','DESC')
					->paginate($limit); 
				}
				else{
					$contents = DB::table('content_genres')
					->leftJoin('contents','content_genres.content_id','contents.content_id')
					->where('content_genres.genre_id',$genre)
					->whereIn('contents.content_type',$filter_type)
					->where('contents.original_language',$langs)
					->where('contents.tmdb_score','<=',$maxr)
					->where('contents.tmdb_score','>=',$minr)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>=',$miny)
					->orderBy('contents.cinema_release_date','DESC')
					->orderBy('contents.localized_release_date','DESC')
					->paginate($limit); 
				}       
			}
			elseif($langs != ""){
				if($minr==0 && $maxr==10){
					$contents = DB::table('contents')
					->whereIn('content_type',$filter_type)
					->where('original_language',$langs)
					->where('cinema_release_date','<=',$maxy)
					->where('cinema_release_date','>=',$miny)
					->orderBy('cinema_release_date','DESC')
					->orderBy('localized_release_date','DESC')
					->paginate($limit);
				}
				else{
					$contents = DB::table('contents')
					->whereIn('content_type',$filter_type)
					->where('original_language',$langs)
					->where('tmdb_score','<=',$maxr)
					->where('tmdb_score','>=',$minr)
					->where('cinema_release_date','<=',$maxy)
					->where('cinema_release_date','>=',$miny)
					->orderBy('cinema_release_date','DESC')
					->orderBy('localized_release_date','DESC')
					->paginate($limit);
				}       
			}
			elseif($genre!=""){
				if($minr==0 && $maxr==10){
					$contents = DB::table('content_genres')
					->leftJoin('contents','content_genres.content_id','contents.content_id')
					->where('content_genres.genre_id',$genre)
					->whereIn('contents.content_type',$filter_type)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>=',$miny)
					->orderBy('contents.cinema_release_date','DESC')
					->orderBy('contents.localized_release_date','DESC')
					->paginate($limit); 
				}
				else{
					$contents = DB::table('content_genres')
					->leftJoin('contents','content_genres.content_id','contents.content_id')
					->where('content_genres.genre_id',$genre)
					->whereIn('contents.content_type',$filter_type)
					->where('contents.tmdb_score','<=',$maxr)
					->where('contents.tmdb_score','>=',$minr)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>=',$miny)
					->orderBy('contents.cinema_release_date','DESC')
					->orderBy('contents.localized_release_date','DESC')
					->paginate($limit); 
				}  
			}
			elseif($rated != ""){
				if($minr==0 && $maxr==10){
					$contents = DB::table('contents')
					->whereIn('content_type',$filter_type)
					->where('age_certification',$rated)                
					->where('cinema_release_date','<=',$maxy)
					->where('cinema_release_date','>=',$miny)
					->orderBy('cinema_release_date','DESC')
					->orderBy('localized_release_date','DESC')
					->paginate($limit);  
				}
				else{
					$contents = DB::table('contents')
					->whereIn('content_type',$filter_type)
					->where('age_certification',$rated)                
					->where('tmdb_score','<=',$maxr)
					->where('tmdb_score','>=',$minr)
					->where('cinema_release_date','<=',$maxy)
					->where('cinema_release_date','>=',$miny)
					->orderBy('cinema_release_date','DESC')
					->orderBy('localized_release_date','DESC')
					->paginate($limit);  
				} 
			}
			else{
				if($minr==0 && $maxr==10){
					$contents = DB::table('contents')
					->whereIn('content_type',$filter_type)
					->where('cinema_release_date','<=',$maxy)
					->where('cinema_release_date','>=',$miny)
					->orderBy('cinema_release_date','DESC')
					->orderBy('localized_release_date','DESC')
					->paginate($limit);  
				}
				else{
					$contents = DB::table('contents')
					->whereIn('content_type',$filter_type)
					->where('tmdb_score','<=',$maxr)
					->where('tmdb_score','>=',$minr)
					->where('cinema_release_date','<=',$maxy)
					->where('cinema_release_date','>=',$miny)
					->orderBy('cinema_release_date','DESC')
					->orderBy('localized_release_date','DESC')
					->paginate($limit); 
				}
			}
		}
		else{

			if($langs!="" && $rated!="" && $genre!=""){
				if($minr==0 && $maxr==10){
					$contents = DB::table('content_genres')
					->leftJoin('contents','content_genres.content_id','contents.content_id')
					->where('content_genres.genre_id',$genre)
					->whereIn('contents.content_type',$filter_type)
					->where('contents.age_certification',$rated)
					->where('contents.original_language',$langs)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>=',$miny)
					->orderBy('contents.cinema_release_date','DESC')
					->orderBy('contents.localized_release_date','DESC')
					->orderBy($field,$sort_type)
					->paginate($limit);
				}
				else{
					$contents = DB::table('content_genres')
					->leftJoin('contents','content_genres.content_id','contents.content_id')
					->where('content_genres.genre_id',$genre)
					->whereIn('contents.content_type',$filter_type)
					->where('contents.age_certification',$rated)
					->where('contents.original_language',$langs)
					->where('contents.tmdb_score','<=',$maxr)
					->where('contents.tmdb_score','>=',$minr)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>=',$miny)
					->orderBy('contents.cinema_release_date','DESC')
					->orderBy('contents.localized_release_date','DESC')
					->orderBy($field,$sort_type)
					->paginate($limit);
				}   
			}
			elseif($langs!="" && $rated != ""){
				if($minr==0 && $maxr==10){
					$contents = DB::table('contents')
					->whereIn('content_type',$filter_type)
					->where('original_language',$langs)
					->where('age_certification',$rated)                
					->where('cinema_release_date','<=',$maxy)
					->where('cinema_release_date','>=',$miny)
					->orderBy('cinema_release_date','DESC')
					->orderBy('localized_release_date','DESC')
					->orderBy($field,$sort_type)
					->paginate($limit);  
				}
				else{
					$contents = DB::table('contents')
					->whereIn('content_type',$filter_type)
					->where('original_language',$langs)
					->where('age_certification',$rated)                
					->where('tmdb_score','<=',$maxr)
					->where('tmdb_score','>=',$minr)
					->where('cinema_release_date','<=',$maxy)
					->where('cinema_release_date','>=',$miny)
					->orderBy('cinema_release_date','DESC')
					->orderBy('localized_release_date','DESC')
					->orderBy($field,$sort_type)
					->paginate($limit);  
				}

			}
			elseif($rated != "" && $genre!=""){
				if($minr==0 && $maxr==10){
					$contents = DB::table('content_genres')
					->leftJoin('contents','content_genres.content_id','contents.content_id')
					->where('content_genres.genre_id',$genre)
					->whereIn('contents.content_type',$filter_type)
					->where('contents.age_certification',$rated)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>=',$miny)
					->orderBy('contents.cinema_release_date','DESC')
					->orderBy('contents.localized_release_date','DESC')
					->orderBy($field,$sort_type)
					->paginate($limit);
				}
				else{
					$contents = DB::table('content_genres')
					->leftJoin('contents','content_genres.content_id','contents.content_id')
					->where('content_genres.genre_id',$genre)
					->whereIn('contents.content_type',$filter_type)
					->where('contents.age_certification',$rated)
					->where('contents.tmdb_score','<=',$maxr)
					->where('contents.tmdb_score','>=',$minr)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>=',$miny)
					->orderBy('contents.cinema_release_date','DESC')
					->orderBy('contents.localized_release_date','DESC')
					->orderBy($field,$sort_type)
					->paginate($limit);
				}      
			}
			elseif($langs != "" && $genre!=""){
				if($minr==0 && $maxr==10){
					$contents = DB::table('content_genres')
					->leftJoin('contents','content_genres.content_id','contents.content_id')
					->where('content_genres.genre_id',$genre)
					->whereIn('contents.content_type',$filter_type)
					->where('contents.original_language',$langs)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>=',$miny)
					->orderBy('contents.cinema_release_date','DESC')
					->orderBy('contents.localized_release_date','DESC')
					->orderBy($field,$sort_type)
					->paginate($limit);
				}
				else{
					$contents = DB::table('content_genres')
					->leftJoin('contents','content_genres.content_id','contents.content_id')
					->where('content_genres.genre_id',$genre)
					->whereIn('contents.content_type',$filter_type)
					->where('contents.original_language',$langs)
					->where('contents.tmdb_score','<=',$maxr)
					->where('contents.tmdb_score','>=',$minr)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>=',$miny)
					->orderBy('contents.cinema_release_date','DESC')
					->orderBy('contents.localized_release_date','DESC')
					->orderBy($field,$sort_type)
					->paginate($limit);
				}   
			}
			elseif($langs != ""){
				if($minr==0 && $maxr==10){
					$contents = DB::table('contents')
					->whereIn('content_type',$filter_type)
					->where('original_language',$langs)
					->where('cinema_release_date','<=',$maxy)
					->where('cinema_release_date','>=',$miny)
					->orderBy('cinema_release_date','DESC')
					->orderBy('localized_release_date','DESC')
					->orderBy($field,$sort_type)
					->paginate($limit);
				}
				else{
					$contents = DB::table('contents')
					->whereIn('content_type',$filter_type)
					->where('original_language',$langs)
					->where('tmdb_score','<=',$maxr)
					->where('tmdb_score','>=',$minr)
					->where('cinema_release_date','<=',$maxy)
					->where('cinema_release_date','>=',$miny)
					->orderBy('cinema_release_date','DESC')
					->orderBy('localized_release_date','DESC')
					->orderBy($field,$sort_type)
					->paginate($limit);
				}     
			}
			elseif($genre!=""){
				if($minr==0 && $maxr==10){
					$contents = DB::table('content_genres')
					->leftJoin('contents','content_genres.content_id','contents.content_id')
					->where('content_genres.genre_id',$genre)
					->whereIn('contents.content_type',$filter_type)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>=',$miny)
					->orderBy('contents.cinema_release_date','DESC')
					->orderBy('contents.localized_release_date','DESC')
					->orderBy($field,$sort_type)
					->paginate($limit); 
				}
				else{
					$contents = DB::table('content_genres')
					->leftJoin('contents','content_genres.content_id','contents.content_id')
					->where('content_genres.genre_id',$genre)
					->whereIn('contents.content_type',$filter_type)
					->where('contents.tmdb_score','<=',$maxr)
					->where('contents.tmdb_score','>=',$minr)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>=',$miny)
					->orderBy('contents.cinema_release_date','DESC')
					->orderBy('contents.localized_release_date','DESC')
					->orderBy($field,$sort_type)
					->paginate($limit); 
				}  
			}
			elseif($rated != ""){
				if($minr==0 && $maxr==10){
					$contents = DB::table('contents')
					->whereIn('content_type',$filter_type)
					->where('age_certification',$rated)                
					->where('cinema_release_date','<=',$maxy)
					->where('cinema_release_date','>=',$miny)
					->orderBy('cinema_release_date','DESC')
					->orderBy('localized_release_date','DESC')
					->orderBy($field,$sort_type)
					->paginate($limit);
				}
				else{
					$contents = DB::table('contents')
					->whereIn('content_type',$filter_type)
					->where('age_certification',$rated)                
					->where('tmdb_score','<=',$maxr)
					->where('tmdb_score','>=',$minr)
					->where('cinema_release_date','<=',$maxy)
					->where('cinema_release_date','>=',$miny)
					->orderBy('cinema_release_date','DESC')
					->orderBy('localized_release_date','DESC')
					->orderBy($field,$sort_type)
					->paginate($limit);
				}   
			}
			else{
				if($minr==0 && $maxr==10){
					$contents = DB::table('contents')
					->whereIn('content_type',$filter_type)
					->where('cinema_release_date','<=',$maxy)
					->where('cinema_release_date','>=',$miny)
					->orderBy('contents.cinema_release_date','DESC')
					->orderBy('contents.localized_release_date','DESC')
					->orderBy($field,$sort_type)
					->paginate($limit);  
				}
				else{
					$contents = DB::table('contents')
					->whereIn('content_type',$filter_type)
					->where('tmdb_score','<=',$maxr)
					->where('tmdb_score','>=',$minr)
					->where('cinema_release_date','<=',$maxy)
					->where('cinema_release_date','>=',$miny)
					->orderBy('contents.cinema_release_date','DESC')
					->orderBy('contents.localized_release_date','DESC')
					->orderBy($field,$sort_type)
					->paginate($limit);  
				}  
			}
		}
	}



	else{

		if($sort == ""){
			if($langs!="" && $rated!="" && $genre!=""){
				if($minr==0 && $maxr==10){
					$contents = DB::table('content_providers')
					->leftJoin('contents','contents.content_id','content_providers.content_id')
					->leftJoin('content_genres','content_genres.content_id','contents.content_id')
					->where('content_providers.provider_id',$provider)
					->where('content_genres.genre_id',$genre)
					->where('contents.age_certification',$rated)
					->whereIn('contents.content_type',$filter_type)
					->where('contents.original_language',$langs)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>=',$miny)
					->orderBy('contents.cinema_release_date','DESC')
					->orderBy('contents.localized_release_date','DESC')
					->select('contents.*')
					->distinct('contents.content_id')
					->paginate($limit);
				}
				else{
					$contents = DB::table('content_providers')
					->leftJoin('contents','contents.content_id','content_providers.content_id')
					->leftJoin('content_genres','content_genres.content_id','contents.content_id')
					->where('content_providers.provider_id',$provider)
					->where('content_genres.genre_id',$genre)
					->whereIn('contents.content_type',$filter_type)
					->where('contents.age_certification',$rated)
					->where('contents.original_language',$langs)
					->where('contents.tmdb_score','<=',$maxr)
					->where('contents.tmdb_score','>=',$minr)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>=',$miny)
					->orderBy('contents.cinema_release_date','DESC')
					->orderBy('contents.localized_release_date','DESC')
					->select('contents.*')
					->distinct('contents.content_id')
					->paginate($limit);
				}        
			}
			elseif($langs!="" && $rated != ""){
				if($minr==0 && $maxr==10){
					$contents = DB::table('content_providers')
					->leftJoin('contents','contents.content_id','content_providers.content_id')
					->where('content_providers.provider_id',$provider)
					->whereIn('contents.content_type',$filter_type)
					->where('contents.original_language',$langs)
					->where('contents.age_certification',$rated)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>=',$miny)     
					->orderBy('contents.cinema_release_date','DESC')
					->orderBy('contents.localized_release_date','DESC')
					->select('contents.*')
					->distinct('contents.content_id')
					->paginate($limit);
				}
				else{
					$contents = DB::table('content_providers')
					->leftJoin('contents','contents.content_id','content_providers.content_id')
					->where('content_providers.provider_id',$provider)
					->whereIn('contents.content_type',$filter_type)
					->where('contents.original_language',$langs)
					->where('contents.age_certification',$rated)
					->where('contents.tmdb_score','<=',$maxr)
					->where('contents.tmdb_score','>=',$minr)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>=',$miny)     
					->orderBy('contents.cinema_release_date','DESC')
					->orderBy('contents.localized_release_date','DESC')
					->select('contents.*')
					->distinct('contents.content_id')
					->paginate($limit);
				}       
			}
			elseif($rated != "" && $genre!=""){
				if($minr==0 && $maxr==10){
					$contents = DB::table('content_providers')
					->leftJoin('contents','contents.content_id','content_providers.content_id')
					->leftJoin('content_genres','content_genres.content_id','contents.content_id')
					->where('content_providers.provider_id',$provider)
					->where('content_genres.genre_id',$genre)
					->where('contents.age_certification',$rated)
					->whereIn('contents.content_type',$filter_type)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>=',$miny)
					->orderBy('contents.cinema_release_date','DESC')
					->orderBy('contents.localized_release_date','DESC')
					->select('contents.*')
					->distinct('contents.content_id')
					->paginate($limit);
				}
				else{
					$contents = DB::table('content_providers')
					->leftJoin('contents','contents.content_id','content_providers.content_id')
					->leftJoin('content_genres','content_genres.content_id','contents.content_id')
					->where('content_providers.provider_id',$provider)
					->where('content_genres.genre_id',$genre)
					->where('contents.age_certification',$rated)
					->whereIn('contents.content_type',$filter_type)
					->where('contents.tmdb_score','<=',$maxr)
					->where('contents.tmdb_score','>=',$minr)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>=',$miny)
					->orderBy('contents.cinema_release_date','DESC')
					->orderBy('contents.localized_release_date','DESC')
					->select('contents.*')
					->distinct('contents.content_id')
					->paginate($limit);
				}        
			}
			elseif($langs != "" && $genre!=""){
				if($minr==0 && $maxr==10){
					$contents = DB::table('content_providers')
					->leftJoin('contents','contents.content_id','content_providers.content_id')
					->leftJoin('content_genres','content_genres.content_id','contents.content_id')
					->where('content_providers.provider_id',$provider)
					->where('content_genres.genre_id',$genre)
					->whereIn('contents.content_type',$filter_type)
					->where('contents.original_language',$langs)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>=',$miny)
					->orderBy('contents.cinema_release_date','DESC')
					->orderBy('contents.localized_release_date','DESC')
					->select('contents.*')
					->distinct('contents.content_id')
					->paginate($limit);
				}
				else{
					$contents = DB::table('content_providers')
					->leftJoin('contents','contents.content_id','content_providers.content_id')
					->leftJoin('content_genres','content_genres.content_id','contents.content_id')
					->where('content_providers.provider_id',$provider)
					->where('content_genres.genre_id',$genre)
					->whereIn('contents.content_type',$filter_type)
					->where('contents.original_language',$langs)
					->where('contents.tmdb_score','<=',$maxr)
					->where('contents.tmdb_score','>=',$minr)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>=',$miny)
					->orderBy('contents.cinema_release_date','DESC')
					->orderBy('contents.localized_release_date','DESC')
					->select('contents.*')
					->distinct('contents.content_id')
					->paginate($limit);
				}       
			}
			elseif($langs != ""){
				if($minr==0 && $maxr==10){
					$contents = DB::table('content_providers')
					->leftJoin('contents','contents.content_id','content_providers.content_id')
					->where('content_providers.provider_id',$provider)
					->whereIn('contents.content_type',$filter_type)
					->where('contents.original_language',$langs)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>=',$miny)     
					->orderBy('contents.cinema_release_date','DESC')
					->orderBy('contents.localized_release_date','DESC')
					->select('contents.*')
					->distinct('contents.content_id')
					->paginate($limit);
				}
				else{
					$contents = DB::table('content_providers')
					->leftJoin('contents','contents.content_id','content_providers.content_id')
					->where('content_providers.provider_id',$provider)
					->whereIn('contents.content_type',$filter_type)
					->where('contents.original_language',$langs)
					->where('contents.tmdb_score','<=',$maxr)
					->where('contents.tmdb_score','>=',$minr)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>=',$miny)     
					->orderBy('contents.cinema_release_date','DESC')
					->orderBy('contents.localized_release_date','DESC')
					->select('contents.*')
					->distinct('contents.content_id')
					->paginate($limit);
				}       
			}
			elseif($genre!=""){
				if($minr==0 && $maxr==10){
					$contents = DB::table('content_providers')
					->leftJoin('contents','contents.content_id','content_providers.content_id')
					->leftJoin('content_genres','content_genres.content_id','contents.content_id')
					->where('content_providers.provider_id',$provider)
					->where('content_genres.genre_id',$genre)
					->whereIn('contents.content_type',$filter_type)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>=',$miny)
					->orderBy('contents.cinema_release_date','DESC')
					->orderBy('contents.localized_release_date','DESC')
					->select('contents.*')
					->distinct('contents.content_id')
					->paginate($limit);
				}
				else{
					$contents = DB::table('content_providers')
					->leftJoin('contents','contents.content_id','content_providers.content_id')
					->leftJoin('content_genres','content_genres.content_id','contents.content_id')
					->where('content_providers.provider_id',$provider)
					->where('content_genres.genre_id',$genre)
					->whereIn('contents.content_type',$filter_type)
					->where('contents.tmdb_score','<=',$maxr)
					->where('contents.tmdb_score','>=',$minr)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>=',$miny)
					->orderBy('contents.cinema_release_date','DESC')
					->orderBy('contents.localized_release_date','DESC')
					->select('contents.*')
					->distinct('contents.content_id')
					->paginate($limit);
				}  
			}
			elseif($rated != ""){


				if($minr==0 && $maxr==10){
					$contents = DB::table('content_providers')
					->leftJoin('contents','contents.content_id','content_providers.content_id')
					->where('content_providers.provider_id',$provider)
					->whereIn('contents.content_type',$filter_type)
					->where('contents.age_certification',$rated)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>=',$miny)     
					->orderBy('contents.cinema_release_date','DESC')
					->orderBy('contents.localized_release_date','DESC')
					->select('contents.*')
					->distinct('contents.content_id')
					->paginate($limit);  
				}
				else{
					$contents = DB::table('content_providers')
					->leftJoin('contents','contents.content_id','content_providers.content_id')
					->where('content_providers.provider_id',$provider)
					->whereIn('contents.content_type',$filter_type)
					->where('contents.age_certification',$rated)
					->where('contents.tmdb_score','<=',$maxr)
					->where('contents.tmdb_score','>=',$minr)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>=',$miny)     
					->orderBy('contents.cinema_release_date','DESC')
					->orderBy('contents.localized_release_date','DESC')
					->select('contents.*')
					->distinct('contents.content_id')
					->paginate($limit);
				} 
			}
			else{
				if($minr==0 && $maxr==10){
					$contents = DB::table('content_providers')
					->leftJoin('contents','contents.content_id','content_providers.content_id')
					->where('content_providers.provider_id',$provider)
					->whereIn('contents.content_type',$filter_type)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>=',$miny)     
					->orderBy('contents.cinema_release_date','DESC')
					->orderBy('contents.localized_release_date','DESC')
					->select('contents.*')
					->distinct('contents.content_id')
					->paginate($limit);
				}
				else{
					$contents = DB::table('content_providers')
					->leftJoin('contents','contents.content_id','content_providers.content_id')
					->where('content_providers.provider_id',$provider)
					->whereIn('contents.content_type',$filter_type)
					->where('contents.tmdb_score','<=',$maxr)
					->where('contents.tmdb_score','>=',$minr)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>=',$miny)     
					->orderBy('contents.cinema_release_date','DESC')
					->orderBy('contents.localized_release_date','DESC')
					->select('contents.*')
					->distinct('contents.content_id')
					->paginate($limit);
				}
			}
		}
		else{

			if($langs!="" && $rated!="" && $genre!=""){
				if($minr==0 && $maxr==10){
					$contents = DB::table('content_providers')
					->leftJoin('contents','contents.content_id','content_providers.content_id')
					->leftJoin('content_genres','content_genres.content_id','contents.content_id')
					->where('content_providers.provider_id',$provider)
					->where('content_genres.genre_id',$genre)
					->where('contents.age_certification',$rated)
					->whereIn('contents.content_type',$filter_type)
					->where('contents.original_language',$langs)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>=',$miny)
					->orderBy('contents.cinema_release_date','DESC')
					->orderBy('contents.localized_release_date','DESC')
					->orderBy($field,$sort_type)
					->select('contents.*')
					->distinct('contents.content_id')
					->paginate($limit);
				}
				else{
					$contents = DB::table('content_genres')
					->leftJoin('contents','content_genres.content_id','contents.content_id')
					->where('content_genres.genre_id',$genre)
					->whereIn('contents.content_type',$filter_type)
					->where('contents.age_certification',$rated)
					->where('contents.original_language',$langs)
					->where('contents.tmdb_score','<=',$maxr)
					->where('contents.tmdb_score','>=',$minr)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>=',$miny)
					->orderBy('contents.cinema_release_date','DESC')
					->orderBy('contents.localized_release_date','DESC')
					->orderBy($field,$sort_type)
					->paginate($limit);
				}   
			}
			elseif($langs!="" && $rated != ""){
				if($minr==0 && $maxr==10){
					$contents = DB::table('content_providers')
					->leftJoin('contents','contents.content_id','content_providers.content_id')
					->where('content_providers.provider_id',$provider)
					->whereIn('contents.content_type',$filter_type)
					->where('contents.original_language',$langs)
					->where('contents.age_certification',$rated)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>=',$miny)     
					->orderBy('contents.cinema_release_date','DESC')
					->orderBy('contents.localized_release_date','DESC')
					->orderBy($field,$sort_type)
					->select('contents.*')
					->distinct('contents.content_id')
					->paginate($limit); 
				}
				else{
					$contents = DB::table('content_providers')
					->leftJoin('contents','contents.content_id','content_providers.content_id')
					->where('content_providers.provider_id',$provider)
					->whereIn('contents.content_type',$filter_type)
					->where('contents.original_language',$langs)
					->where('contents.age_certification',$rated)
					->where('contents.tmdb_score','<=',$maxr)
					->where('contents.tmdb_score','>=',$minr)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>=',$miny)     
					->orderBy('contents.cinema_release_date','DESC')
					->orderBy('contents.localized_release_date','DESC')
					->orderBy($field,$sort_type)
					->select('contents.*')
					->distinct('contents.content_id')
					->paginate($limit); 
				}

			}
			elseif($rated != "" && $genre!=""){
				if($minr==0 && $maxr==10){
					$contents = DB::table('content_providers')
					->leftJoin('contents','contents.content_id','content_providers.content_id')
					->leftJoin('content_genres','content_genres.content_id','contents.content_id')
					->where('content_providers.provider_id',$provider)
					->where('content_genres.genre_id',$genre)
					->where('contents.age_certification',$rated)
					->whereIn('contents.content_type',$filter_type)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>=',$miny)
					->orderBy('contents.cinema_release_date','DESC')
					->orderBy('contents.localized_release_date','DESC')
					->orderBy($field,$sort_type)
					->select('contents.*')
					->distinct('contents.content_id')
					->paginate($limit);
				}
				else{
					$contents = DB::table('content_providers')
					->leftJoin('contents','contents.content_id','content_providers.content_id')
					->leftJoin('content_genres','content_genres.content_id','contents.content_id')
					->where('content_providers.provider_id',$provider)
					->where('content_genres.genre_id',$genre)
					->where('contents.age_certification',$rated)
					->whereIn('contents.content_type',$filter_type)
					->where('contents.tmdb_score','<=',$maxr)
					->where('contents.tmdb_score','>=',$minr)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>=',$miny)
					->orderBy('contents.cinema_release_date','DESC')
					->orderBy('contents.localized_release_date','DESC')
					->orderBy($field,$sort_type)
					->select('contents.*')
					->distinct('contents.content_id')
					->paginate($limit);
				}      
			}
			elseif($langs != "" && $genre!=""){
				if($minr==0 && $maxr==10){
					$contents = DB::table('content_providers')
					->leftJoin('contents','contents.content_id','content_providers.content_id')
					->leftJoin('content_genres','content_genres.content_id','contents.content_id')
					->where('content_providers.provider_id',$provider)
					->where('content_genres.genre_id',$genre)
					->whereIn('contents.content_type',$filter_type)
					->where('contents.original_language',$langs)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>=',$miny)
					->orderBy('contents.cinema_release_date','DESC')
					->orderBy('contents.localized_release_date','DESC')
					->orderBy($field,$sort_type)
					->select('contents.*')
					->distinct('contents.content_id')
					->paginate($limit);
				}
				else{
					$contents = DB::table('content_providers')
					->leftJoin('contents','contents.content_id','content_providers.content_id')
					->leftJoin('content_genres','content_genres.content_id','contents.content_id')
					->where('content_providers.provider_id',$provider)
					->where('content_genres.genre_id',$genre)
					->whereIn('contents.content_type',$filter_type)
					->where('contents.original_language',$langs)
					->where('contents.tmdb_score','>=',$minr)
					->where('contents.tmdb_score','<=',$maxr)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>=',$miny)
					->orderBy('contents.cinema_release_date','DESC')
					->orderBy('contents.localized_release_date','DESC')
					->orderBy($field,$sort_type)
					->select('contents.*')
					->distinct('contents.content_id')
					->paginate($limit);
				}   
			}
			elseif($langs != ""){
				if($minr==0 && $maxr==10){
					$contents = DB::table('content_providers')
					->leftJoin('contents','contents.content_id','content_providers.content_id')
					->where('content_providers.provider_id',$provider)
					->whereIn('contents.content_type',$filter_type)
					->where('contents.original_language',$langs)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>=',$miny)     
					->orderBy('contents.cinema_release_date','DESC')
					->orderBy('contents.localized_release_date','DESC')
					->orderBy($field,$sort_type)
					->select('contents.*')
					->distinct('contents.content_id')
					->paginate($limit);
				}
				else{
					$contents = DB::table('content_providers')
					->leftJoin('contents','contents.content_id','content_providers.content_id')
					->where('content_providers.provider_id',$provider)
					->whereIn('contents.content_type',$filter_type)
					->where('contents.original_language',$langs)
					->where('contents.tmdb_score','<=',$maxr)
					->where('contents.tmdb_score','>=',$minr)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>=',$miny)     
					->orderBy('contents.cinema_release_date','DESC')
					->orderBy('contents.localized_release_date','DESC')
					->orderBy($field,$sort_type)
					->select('contents.*')
					->distinct('contents.content_id')
					->paginate($limit);
				}     
			}
			elseif($genre!=""){
				if($minr==0 && $maxr==10){
					$contents = DB::table('content_providers')
					->leftJoin('contents','contents.content_id','content_providers.content_id')
					->leftJoin('content_genres','content_genres.content_id','contents.content_id')
					->where('content_providers.provider_id',$provider)
					->where('content_genres.genre_id',$genre)
					->whereIn('contents.content_type',$filter_type)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>=',$miny)
					->orderBy('contents.cinema_release_date','DESC')
					->orderBy('contents.localized_release_date','DESC')
					->orderBy($field,$sort_type)
					->select('contents.*')
					->distinct('contents.content_id')
					->paginate($limit);
				}
				else{
					$contents = DB::table('content_providers')
					->leftJoin('contents','contents.content_id','content_providers.content_id')
					->leftJoin('content_genres','content_genres.content_id','contents.content_id')
					->where('content_providers.provider_id',$provider)
					->where('content_genres.genre_id',$genre)
					->whereIn('contents.content_type',$filter_type)
					->where('contents.tmdb_score','>=',$minr)
					->where('contents.tmdb_score','<=',$maxr)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>=',$miny)
					->orderBy('contents.cinema_release_date','DESC')
					->orderBy('contents.localized_release_date','DESC')
					->orderBy($field,$sort_type)
					->select('contents.*')
					->distinct('contents.content_id')
					->paginate($limit);
				}  
			}
			elseif($rated != ""){
				if($minr==0 && $maxr==10){
					$contents = DB::table('content_providers')
					->leftJoin('contents','contents.content_id','content_providers.content_id')
					->where('content_providers.provider_id',$provider)
					->whereIn('contents.content_type',$filter_type)
					->where('contents.age_certification',$rated)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>=',$miny)     
					->orderBy('contents.cinema_release_date','DESC')
					->orderBy('contents.localized_release_date','DESC')
					->orderBy($field,$sort_type)
					->select('contents.*')
					->distinct('contents.content_id')
					->paginate($limit);
				}
				else{
					$contents = DB::table('content_providers')
					->leftJoin('contents','contents.content_id','content_providers.content_id')
					->where('content_providers.provider_id',$provider)
					->whereIn('contents.content_type',$filter_type)
					->where('contents.original_language',$langs)
					->where('contents.tmdb_score','<=',$maxr)
					->where('contents.tmdb_score','>=',$minr)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>=',$miny)     
					->orderBy('contents.cinema_release_date','DESC')->orderBy($field,$sort_type)
					->select('contents.*')
					->distinct('contents.content_id')
					->paginate($limit);
				}   
			}
			else{
				if($minr==0 && $maxr==10){
					$contents = DB::table('content_providers')
					->leftJoin('contents','contents.content_id','content_providers.content_id')
					->where('content_providers.provider_id',$provider)
					->whereIn('contents.content_type',$filter_type)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>=',$miny)     
					->orderBy('contents.localized_release_date','DESC')->orderBy($field,$sort_type)
					->select('contents.*')
					->distinct('contents.content_id')
					->paginate($limit);
				}
				else{
					$contents = DB::table('content_providers')
					->leftJoin('contents','contents.content_id','content_providers.content_id')
					->where('content_providers.provider_id',$provider)
					->whereIn('contents.content_type',$filter_type)
					->where('contents.tmdb_score','<=',$maxr)
					->where('contents.tmdb_score','>=',$minr)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>=',$miny)     
					->orderBy($field,$sort_type)
					->select('contents.*')
					->distinct('contents.content_id')
					->paginate($limit);
				}  
			}
		}

	}

}
$miny = explode('-',$miny)[0];
$maxy = explode('-',$maxy)[0];

		if(isset($_GET['provider'])){  $provider = $_GET['provider'];  }
		foreach ($contents as $key => $value) {
			if (!file_exists(public_path().'/poster/'.$value->poster)){
				$contents[$key]->poster = 'default.png';
			}
		}
		return response()->json(compact('contents'));
	      // return view('khoj.home',);
	}

	function viral_videos(Request $request){ 

		if (isset($request->limit) && $request->limit > 0){
			$limit = $request->limit;
		}
		else
			$limit = 30;

		$page = 0;
	$langs = "";
	$rated = "";
	$sort = "";
	$field = "";
	$sort_type = "";
	$genre = "";
	$provider = "";
	$minr=0;
	$maxr=10; 
	$type="viral";
	$tab = 'Viral';

	$maxy = DB::table('contents')->orderBy('cinema_release_date','DESC')->first();
	$maxy = $maxy->cinema_release_date;

	$miny = '0000-00-00';    

	if(isset($_GET['page'])){
		$page = $_GET['page']-1;
	}  

	if(isset($_GET['lang'])){  $langs = $_GET['lang'];  }
	if(isset($_GET['provider'])){  $provider = explode('_',$_GET['provider'])[1];  }

	if(isset($_GET['sort'])){
		$sort = $_GET['sort'];
		if($sort == "alpha"){
			$field = 'title';
			$sort_type = 'ASC';
		}
		elseif($sort == 'rate'){
			$field = 'tmdb_score';
			$sort_type = 'DESC';
		}
		elseif($sort == 'year'){
			$field = 'cinema_release_date';
			$sort_type = 'DESC';
		}
		elseif($sort == 'imdb_score'){
			$field = 'imdb_score';
			$sort_type = 'DESC';
		}
		else{
			$field = 'content_id';
			$sort_type = 'DESC';
		}      
	}

	if((isset($_GET['sort']) && isset($_GET['genre'])) || (isset($_GET['sort']) && isset($_GET['provider']))){
		$sort = $_GET['sort'];
		if($sort == "alpha"){
			$field = 'contents.title';
			$sort_type = 'ASC';
		}
		elseif($sort == 'rate'){
			$field = 'contents.tmdb_score';
			$sort_type = 'DESC';
		}
		elseif($sort == 'year'){
			$field = 'contents.cinema_release_date';
			$sort_type = 'DESC';
		}
		elseif($sort == 'imdb_score'){
			$field = 'contents.imdb_score';
			$sort_type = 'DESC';
		}
		else{
			$field = 'contents.content_id';
			$sort_type = 'DESC';
		}

	}

	if(isset($_GET['rated'])){
		$rated = $_GET['rated'];
	}

	if(isset($_GET['searchtype'])){
		$type = $_GET['searchtype'];
	}

	if(isset($_GET['genre'])){
		$genre = $_GET['genre'];
	}

	if(isset($_GET['minr'])){
		$minr = $_GET['minr'];
	}
	if(isset($_GET['maxr'])){
		$maxr = $_GET['maxr'];    
	}

	if(isset($_GET['miny'])){
		$miny = $_GET['miny']."-01-01";
	}
	if(isset($_GET['maxy'])){
		$maxy = $_GET['maxy']."-12-31";
	}


	if($provider == ""){

		if($sort == ""){
			if($langs!="" && $rated!="" && $genre!=""){
				if($minr==0 && $maxr==10){
					$contents = DB::table('content_genres')
					->leftJoin('contents','content_genres.content_id','contents.content_id')
					->where('content_genres.genre_id',$genre)
					->where('contents.content_type',$type)
					->where('contents.age_certification',$rated)
					->where('contents.original_language',$langs)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>=',$miny)
					->where('contents.parent_id','0')
					->orderBy('content_genres.content_id','DESC')
					->paginate($limit);
				}
				else{
					$contents = DB::table('content_genres')
					->leftJoin('contents','content_genres.content_id','contents.content_id')
					->where('content_genres.genre_id',$genre)
					->where('contents.content_type',$type)
					->where('contents.age_certification',$rated)
					->where('contents.original_language',$langs)
					->where('contents.tmdb_score','<=',$maxr)
					->where('contents.tmdb_score','>=',$minr)
					->where('contents.cinema_release_date','<=',$maxy)
					->where('contents.cinema_release_date','>=',$miny)
					->where('contents.parent_id','0')
					->orderBy('content_genres.content_id','DESC')
					->paginate($limit);
				}        
			}
			elseif($langs!="" && $rated != ""){
				if($minr==0 && $maxr==10){
					$contents = DB::table('contents')
					->where('content_type',$type)
					->where('original_language',$langs)
					->where('age_certification',$rated)                
					->where('cinema_release_date','<=',$maxy)
					->where('cinema_release_date','>=',$miny)
					->where('parent_id','0')
					->paginate($limit);
				}
				else{$contents = DB::table('contents')
					->where('content_type',$type)
				->where('original_language',$langs)
				->where('age_certification',$rated)                
				->where('tmdb_score','<=',$maxr)
				->where('tmdb_score','>=',$minr)
				->where('cinema_release_date','<=',$maxy)
				->where('cinema_release_date','>=',$miny)
				->where('parent_id','0')
				->paginate($limit);
			}       
		}
		elseif($rated != "" && $genre!=""){
			if($minr==0 && $maxr==10){
				$contents = DB::table('content_genres')
				->leftJoin('contents','content_genres.content_id','contents.content_id')
				->where('content_genres.genre_id',$genre)
				->where('contents.content_type',$type)
				->where('contents.age_certification',$rated)
				->where('contents.cinema_release_date','<=',$maxy)
				->where('contents.cinema_release_date','>=',$miny)
				->where('contents.parent_id','0')
				->orderBy('content_genres.content_id','DESC')
				->paginate($limit);
			}
			else{
				$contents = DB::table('content_genres')
				->leftJoin('contents','content_genres.content_id','contents.content_id')
				->where('content_genres.genre_id',$genre)
				->where('contents.content_type',$type)
				->where('contents.age_certification',$rated)
				->where('contents.tmdb_score','<=',$maxr)
				->where('contents.tmdb_score','>=',$minr)
				->where('contents.cinema_release_date','<=',$maxy)
				->where('contents.cinema_release_date','>=',$miny)
				->where('contents.parent_id','0')
				->orderBy('content_genres.content_id','DESC')
				->paginate($limit);
			}        
		}
		elseif($langs != "" && $genre!=""){
			if($minr==0 && $maxr==10){
				$contents = DB::table('content_genres')
				->leftJoin('contents','content_genres.content_id','contents.content_id')
				->where('content_genres.genre_id',$genre)
				->where('contents.content_type',$type)
				->where('contents.original_language',$langs)
				->where('contents.cinema_release_date','<=',$maxy)
				->where('contents.cinema_release_date','>=',$miny)
				->where('contents.parent_id','0')
				->orderBy('content_genres.content_id','DESC')
				->paginate($limit); 
			}
			else{
				$contents = DB::table('content_genres')
				->leftJoin('contents','content_genres.content_id','contents.content_id')
				->where('content_genres.genre_id',$genre)
				->where('contents.content_type',$type)
				->where('contents.original_language',$langs)
				->where('contents.tmdb_score','<=',$maxr)
				->where('contents.tmdb_score','>=',$minr)
				->where('contents.cinema_release_date','<=',$maxy)
				->where('contents.cinema_release_date','>=',$miny)
				->where('contents.parent_id','0')
				->orderBy('content_genres.content_id','DESC')
				->paginate($limit); 
			}       
		}
		elseif($langs != ""){
			if($minr==0 && $maxr==10){
				$contents = DB::table('contents')
				->where('content_type',$type)
				->where('original_language',$langs)
				->where('cinema_release_date','<=',$maxy)
				->where('cinema_release_date','>=',$miny)
				->where('parent_id','0')
				->paginate($limit);
			}
			else{
				$contents = DB::table('contents')
				->where('content_type',$type)
				->where('original_language',$langs)
				->where('tmdb_score','<=',$maxr)
				->where('tmdb_score','>=',$minr)
				->where('cinema_release_date','<=',$maxy)
				->where('cinema_release_date','>=',$miny)
				->where('parent_id','0')
				->paginate($limit);
			}       
		}
		elseif($genre!=""){
			if($minr==0 && $maxr==10){
				$contents = DB::table('content_genres')
				->leftJoin('contents','content_genres.content_id','contents.content_id')
				->where('content_genres.genre_id',$genre)
				->where('contents.content_type',$type)
				->where('contents.cinema_release_date','<=',$maxy)
				->where('contents.cinema_release_date','>=',$miny)
				->where('contents.parent_id','0')
				->orderBy('content_genres.content_id','DESC')
				->paginate($limit); 
			}
			else{
				$contents = DB::table('content_genres')
				->leftJoin('contents','content_genres.content_id','contents.content_id')
				->where('content_genres.genre_id',$genre)
				->where('contents.content_type',$type)
				->where('contents.tmdb_score','<=',$maxr)
				->where('contents.tmdb_score','>=',$minr)
				->where('contents.cinema_release_date','<=',$maxy)
				->where('contents.cinema_release_date','>=',$miny)
				->where('contents.parent_id','0')
				->orderBy('content_genres.content_id','DESC')
				->paginate($limit); 
			}  
		}
		elseif($rated != ""){
			if($minr==0 && $maxr==10){
				$contents = DB::table('contents')
				->where('content_type',$type)
				->where('age_certification',$rated)                
				->where('cinema_release_date','<=',$maxy)
				->where('cinema_release_date','>=',$miny)
				->where('parent_id','0')
				->paginate($limit);  
			}
			else{
				$contents = DB::table('contents')
				->where('content_type',$type)
				->where('age_certification',$rated)                
				->where('tmdb_score','<=',$maxr)
				->where('tmdb_score','>=',$minr)
				->where('cinema_release_date','<=',$maxy)
				->where('cinema_release_date','>=',$miny)
				->where('parent_id','0')
				->paginate($limit);  
			} 
		}
		else{
			if($minr==0 && $maxr==10){
				$contents = DB::table('contents')
				->where('content_type',$type)
				->where('cinema_release_date','<=',$maxy)
				->where('cinema_release_date','>=',$miny)
				->where('parent_id','0')
				->paginate($limit);  
			}
			else{
				$contents = DB::table('contents')
				->where('content_type',$type)
				->where('tmdb_score','<=',$maxr)
				->where('tmdb_score','>=',$minr)
				->where('cinema_release_date','<=',$maxy)
				->where('cinema_release_date','>=',$miny)
				->where('parent_id','0')
				->paginate($limit); 
			}
		}
	}
	else{

		if($langs!="" && $rated!="" && $genre!=""){
			if($minr==0 && $maxr==10){
				$contents = DB::table('content_genres')
				->leftJoin('contents','content_genres.content_id','contents.content_id')
				->where('content_genres.genre_id',$genre)
				->where('contents.content_type',$type)
				->where('contents.age_certification',$rated)
				->where('contents.original_language',$langs)
				->where('contents.cinema_release_date','<=',$maxy)
				->where('contents.cinema_release_date','>=',$miny)
				->where('contents.parent_id','0')
				->orderBy($field,$sort_type)
				->paginate($limit);
			}
			else{
				$contents = DB::table('content_genres')
				->leftJoin('contents','content_genres.content_id','contents.content_id')
				->where('content_genres.genre_id',$genre)
				->where('contents.content_type',$type)
				->where('contents.age_certification',$rated)
				->where('contents.original_language',$langs)
				->where('contents.tmdb_score','<=',$maxr)
				->where('contents.tmdb_score','>=',$minr)
				->where('contents.cinema_release_date','<=',$maxy)
				->where('contents.cinema_release_date','>=',$miny)
				->where('contents.parent_id','0')
				->orderBy($field,$sort_type)
				->paginate($limit);
			}   
		}
		elseif($langs!="" && $rated != ""){
			if($minr==0 && $maxr==10){
				$contents = DB::table('contents')
				->where('content_type',$type)
				->where('original_language',$langs)
				->where('age_certification',$rated)                
				->where('cinema_release_date','<=',$maxy)
				->where('cinema_release_date','>=',$miny)
				->where('parent_id','0')
				->orderBy($field,$sort_type)
				->paginate($limit);  
			}
			else{
				$contents = DB::table('contents')
				->where('content_type',$type)
				->where('original_language',$langs)
				->where('age_certification',$rated)                
				->where('tmdb_score','<=',$maxr)
				->where('tmdb_score','>=',$minr)
				->where('cinema_release_date','<=',$maxy)
				->where('cinema_release_date','>=',$miny)
				->where('parent_id','0')
				->orderBy($field,$sort_type)
				->paginate($limit);  
			}

		}
		elseif($rated != "" && $genre!=""){
			if($minr==0 && $maxr==10){
				$contents = DB::table('content_genres')
				->leftJoin('contents','content_genres.content_id','contents.content_id')
				->where('content_genres.genre_id',$genre)
				->where('contents.content_type',$type)
				->where('contents.age_certification',$rated)
				->where('contents.cinema_release_date','<=',$maxy)
				->where('contents.cinema_release_date','>=',$miny)
				->where('contents.parent_id','0')
				->orderBy($field,$sort_type)
				->paginate($limit);
			}
			else{
				$contents = DB::table('content_genres')
				->leftJoin('contents','content_genres.content_id','contents.content_id')
				->where('content_genres.genre_id',$genre)
				->where('contents.content_type',$type)
				->where('contents.age_certification',$rated)
				->where('contents.tmdb_score','<=',$maxr)
				->where('contents.tmdb_score','>=',$minr)
				->where('contents.cinema_release_date','<=',$maxy)
				->where('contents.cinema_release_date','>=',$miny)
				->where('contents.parent_id','0')
				->orderBy($field,$sort_type)
				->paginate($limit);
			}      
		}
		elseif($langs != "" && $genre!=""){
			if($minr==0 && $maxr==10){
				$contents = DB::table('content_genres')
				->leftJoin('contents','content_genres.content_id','contents.content_id')
				->where('content_genres.genre_id',$genre)
				->where('contents.content_type',$type)
				->where('contents.original_language',$langs)
				->where('contents.cinema_release_date','<=',$maxy)
				->where('contents.cinema_release_date','>=',$miny)
				->where('contents.parent_id','0')
				->orderBy($field,$sort_type)
				->paginate($limit);
			}
			else{
				$contents = DB::table('content_genres')
				->leftJoin('contents','content_genres.content_id','contents.content_id')
				->where('content_genres.genre_id',$genre)
				->where('contents.content_type',$type)
				->where('contents.original_language',$langs)
				->where('contents.tmdb_score','<=',$maxr)
				->where('contents.tmdb_score','>=',$minr)
				->where('contents.cinema_release_date','<=',$maxy)
				->where('contents.cinema_release_date','>=',$miny)
				->where('contents.parent_id','0')
				->orderBy($field,$sort_type)
				->paginate($limit);
			}   
		}
		elseif($langs != ""){
			if($minr==0 && $maxr==10){
				$contents = DB::table('contents')
				->where('content_type',$type)
				->where('original_language',$langs)
				->where('cinema_release_date','<=',$maxy)
				->where('cinema_release_date','>=',$miny)
				->where('parent_id','0')
				->orderBy($field,$sort_type)
				->paginate($limit);
			}
			else{
				$contents = DB::table('contents')
				->where('content_type',$type)
				->where('original_language',$langs)
				->where('tmdb_score','<=',$maxr)
				->where('tmdb_score','>=',$minr)
				->where('cinema_release_date','<=',$maxy)
				->where('cinema_release_date','>=',$miny)
				->where('parent_id','0')
				->orderBy($field,$sort_type)
				->paginate($limit);
			}     
		}
		elseif($genre!=""){
			if($minr==0 && $maxr==10){
				$contents = DB::table('content_genres')
				->leftJoin('contents','content_genres.content_id','contents.content_id')
				->where('content_genres.genre_id',$genre)
				->where('contents.content_type',$type)
				->where('contents.cinema_release_date','<=',$maxy)
				->where('contents.cinema_release_date','>=',$miny)
				->where('contents.parent_id','0')
				->orderBy('content_genres.content_id','DESC')
				->orderBy($field,$sort_type)
				->paginate($limit); 
			}
			else{
				$contents = DB::table('content_genres')
				->leftJoin('contents','content_genres.content_id','contents.content_id')
				->where('content_genres.genre_id',$genre)
				->where('contents.content_type',$type)
				->where('contents.tmdb_score','<=',$maxr)
				->where('contents.tmdb_score','>=',$minr)
				->where('contents.cinema_release_date','<=',$maxy)
				->where('contents.cinema_release_date','>=',$miny)
				->where('contents.parent_id','0')
				->orderBy('content_genres.content_id','DESC')
				->orderBy($field,$sort_type)
				->paginate($limit); 
			}  
		}
		elseif($rated != ""){
			if($minr==0 && $maxr==10){
				$contents = DB::table('contents')
				->where('content_type',$type)
				->where('age_certification',$rated)                
				->where('cinema_release_date','<=',$maxy)
				->where('cinema_release_date','>=',$miny)
				->where('parent_id','0')
				->orderBy($field,$sort_type)
				->paginate($limit);
			}
			else{
				$contents = DB::table('contents')
				->where('content_type',$type)
				->where('age_certification',$rated)                
				->where('tmdb_score','<=',$maxr)
				->where('tmdb_score','>=',$minr)
				->where('cinema_release_date','<=',$maxy)
				->where('cinema_release_date','>=',$miny)
				->where('parent_id','0')
				->orderBy($field,$sort_type)
				->paginate($limit);
			}   
		}
		else{
			if($minr==0 && $maxr==10){
				$contents = DB::table('contents')
				->where('content_type',$type)
				->where('cinema_release_date','<=',$maxy)
				->where('cinema_release_date','>=',$miny)
				->where('parent_id','0')
				->orderBy($field,$sort_type)
				->paginate($limit);  
			}
			else{
				$contents = DB::table('contents')
				->where('content_type',$type)
				->where('tmdb_score','<=',$maxr)
				->where('tmdb_score','>=',$minr)
				->where('cinema_release_date','<=',$maxy)
				->where('cinema_release_date','>=',$miny)
				->where('parent_id','0')
				->orderBy($field,$sort_type)
				->paginate($limit);  
			}  
		}
	}
}



else{

	if($sort == ""){
		if($langs!="" && $rated!="" && $genre!=""){
			if($minr==0 && $maxr==10){
				$contents = DB::table('content_providers')
				->leftJoin('contents','contents.content_id','content_providers.content_id')
				->leftJoin('content_genres','content_genres.content_id','contents.content_id')
				->where('content_providers.provider_id',$provider)
				->where('content_genres.genre_id',$genre)
				->where('contents.age_certification',$rated)
				->where('contents.content_type',$type)
				->where('contents.original_language',$langs)
				->where('contents.cinema_release_date','<=',$maxy)
				->where('contents.cinema_release_date','>=',$miny)
				->where('contents.parent_id','0')
				->orderBy('content_genres.content_id','DESC')
				->select('contents.*')
				->distinct('contents.content_id')
				->paginate($limit);
			}
			else{
				$contents = DB::table('content_providers')
				->leftJoin('contents','contents.content_id','content_providers.content_id')
				->leftJoin('content_genres','content_genres.content_id','contents.content_id')
				->where('content_providers.provider_id',$provider)
				->where('content_genres.genre_id',$genre)
				->where('contents.content_type',$type)
				->where('contents.age_certification',$rated)
				->where('contents.original_language',$langs)
				->where('contents.tmdb_score','<=',$maxr)
				->where('contents.tmdb_score','>=',$minr)
				->where('contents.cinema_release_date','<=',$maxy)
				->where('contents.cinema_release_date','>=',$miny)
				->where('contents.parent_id','0')
				->orderBy('content_genres.content_id','DESC')
				->select('contents.*')
				->distinct('contents.content_id')
				->paginate($limit);
			}        
		}
		elseif($langs!="" && $rated != ""){
			if($minr==0 && $maxr==10){
				$contents = DB::table('content_providers')
				->leftJoin('contents','contents.content_id','content_providers.content_id')
				->where('content_providers.provider_id',$provider)
				->where('contents.content_type',$type)
				->where('contents.original_language',$langs)
				->where('contents.age_certification',$rated)
				->where('contents.cinema_release_date','<=',$maxy)
				->where('contents.cinema_release_date','>=',$miny)     
				->where('contents.parent_id','0')
				->select('contents.*')
				->distinct('contents.content_id')
				->paginate($limit);
			}
			else{
				$contents = DB::table('content_providers')
				->leftJoin('contents','contents.content_id','content_providers.content_id')
				->where('content_providers.provider_id',$provider)
				->where('contents.content_type',$type)
				->where('contents.original_language',$langs)
				->where('contents.age_certification',$rated)
				->where('contents.tmdb_score','<=',$maxr)
				->where('contents.tmdb_score','>=',$minr)
				->where('contents.cinema_release_date','<=',$maxy)
				->where('contents.cinema_release_date','>=',$miny)     
				->where('contents.parent_id','0')
				->select('contents.*')
				->distinct('contents.content_id')
				->paginate($limit);
			}       
		}
		elseif($rated != "" && $genre!=""){
			if($minr==0 && $maxr==10){
				$contents = DB::table('content_providers')
				->leftJoin('contents','contents.content_id','content_providers.content_id')
				->leftJoin('content_genres','content_genres.content_id','contents.content_id')
				->where('content_providers.provider_id',$provider)
				->where('content_genres.genre_id',$genre)
				->where('contents.age_certification',$rated)
				->where('contents.content_type',$type)
				->where('contents.cinema_release_date','<=',$maxy)
				->where('contents.cinema_release_date','>=',$miny)
				->where('contents.parent_id','0')
				->orderBy('content_genres.content_id','DESC')
				->select('contents.*')
				->distinct('contents.content_id')
				->paginate($limit);
			}
			else{
				$contents = DB::table('content_providers')
				->leftJoin('contents','contents.content_id','content_providers.content_id')
				->leftJoin('content_genres','content_genres.content_id','contents.content_id')
				->where('content_providers.provider_id',$provider)
				->where('content_genres.genre_id',$genre)
				->where('contents.age_certification',$rated)
				->where('contents.content_type',$type)
				->where('contents.tmdb_score','<=',$maxr)
				->where('contents.tmdb_score','>=',$minr)
				->where('contents.cinema_release_date','<=',$maxy)
				->where('contents.cinema_release_date','>=',$miny)
				->where('contents.parent_id','0')
				->orderBy('content_genres.content_id','DESC')
				->select('contents.*')
				->distinct('contents.content_id')
				->paginate($limit);
			}        
		}
		elseif($langs != "" && $genre!=""){
			if($minr==0 && $maxr==10){
				$contents = DB::table('content_providers')
				->leftJoin('contents','contents.content_id','content_providers.content_id')
				->leftJoin('content_genres','content_genres.content_id','contents.content_id')
				->where('content_providers.provider_id',$provider)
				->where('content_genres.genre_id',$genre)
				->where('contents.content_type',$type)
				->where('contents.original_language',$langs)
				->where('contents.cinema_release_date','<=',$maxy)
				->where('contents.cinema_release_date','>=',$miny)
				->where('contents.parent_id','0')
				->orderBy('content_genres.content_id','DESC')
				->select('contents.*')
				->distinct('contents.content_id')
				->paginate($limit);
			}
			else{
				$contents = DB::table('content_providers')
				->leftJoin('contents','contents.content_id','content_providers.content_id')
				->leftJoin('content_genres','content_genres.content_id','contents.content_id')
				->where('content_providers.provider_id',$provider)
				->where('content_genres.genre_id',$genre)
				->where('contents.content_type',$type)
				->where('contents.original_language',$langs)
				->where('contents.tmdb_score','<=',$maxr)
				->where('contents.tmdb_score','>=',$minr)
				->where('contents.cinema_release_date','<=',$maxy)
				->where('contents.cinema_release_date','>=',$miny)
				->where('contents.parent_id','0')
				->orderBy('content_genres.content_id','DESC')
				->select('contents.*')
				->distinct('contents.content_id')
				->paginate($limit);
			}       
		}
		elseif($langs != ""){
			if($minr==0 && $maxr==10){
				$contents = DB::table('content_providers')
				->leftJoin('contents','contents.content_id','content_providers.content_id')
				->where('content_providers.provider_id',$provider)
				->where('contents.content_type',$type)
				->where('contents.original_language',$langs)
				->where('contents.cinema_release_date','<=',$maxy)
				->where('contents.cinema_release_date','>=',$miny)     
				->where('contents.parent_id','0')
				->select('contents.*')
				->distinct('contents.content_id')
				->paginate($limit);
			}
			else{
				$contents = DB::table('content_providers')
				->leftJoin('contents','contents.content_id','content_providers.content_id')
				->where('content_providers.provider_id',$provider)
				->where('contents.content_type',$type)
				->where('contents.original_language',$langs)
				->where('contents.tmdb_score','<=',$maxr)
				->where('contents.tmdb_score','>=',$minr)
				->where('contents.cinema_release_date','<=',$maxy)
				->where('contents.parent_id','0')
				->where('contents.cinema_release_date','>=',$miny)     
				->select('contents.*')
				->distinct('contents.content_id')
				->paginate($limit);
			}       
		}
		elseif($genre!=""){
			if($minr==0 && $maxr==10){
				$contents = DB::table('content_providers')
				->leftJoin('contents','contents.content_id','content_providers.content_id')
				->leftJoin('content_genres','content_genres.content_id','contents.content_id')
				->where('content_providers.provider_id',$provider)
				->where('content_genres.genre_id',$genre)
				->where('contents.content_type',$type)
				->where('contents.cinema_release_date','<=',$maxy)
				->where('contents.cinema_release_date','>=',$miny)
				->where('contents.parent_id','0')
				->orderBy('content_genres.content_id','DESC')
				->select('contents.*')
				->distinct('contents.content_id')
				->paginate($limit);
			}
			else{
				$contents = DB::table('content_providers')
				->leftJoin('contents','contents.content_id','content_providers.content_id')
				->leftJoin('content_genres','content_genres.content_id','contents.content_id')
				->where('content_providers.provider_id',$provider)
				->where('content_genres.genre_id',$genre)
				->where('contents.content_type',$type)
				->where('contents.tmdb_score','<=',$maxr)
				->where('contents.tmdb_score','>=',$minr)
				->where('contents.cinema_release_date','<=',$maxy)
				->where('contents.cinema_release_date','>=',$miny)
				->where('contents.parent_id','0')
				->orderBy('content_genres.content_id','DESC')
				->select('contents.*')
				->distinct('contents.content_id')
				->paginate($limit);
			}  
		}
		elseif($rated != ""){


			if($minr==0 && $maxr==10){
				$contents = DB::table('content_providers')
				->leftJoin('contents','contents.content_id','content_providers.content_id')
				->where('content_providers.provider_id',$provider)
				->where('contents.content_type',$type)
				->where('contents.age_certification',$rated)
				->where('contents.cinema_release_date','<=',$maxy)
				->where('contents.cinema_release_date','>=',$miny)     
				->where('contents.parent_id','0')
				->select('contents.*')
				->distinct('contents.content_id')
				->paginate($limit);  
			}
			else{
				$contents = DB::table('content_providers')
				->leftJoin('contents','contents.content_id','content_providers.content_id')
				->where('content_providers.provider_id',$provider)
				->where('contents.content_type',$type)
				->where('contents.age_certification',$rated)
				->where('contents.tmdb_score','<=',$maxr)
				->where('contents.tmdb_score','>=',$minr)
				->where('contents.cinema_release_date','<=',$maxy)
				->where('contents.cinema_release_date','>=',$miny)     
				->where('contents.parent_id','0')
				->select('contents.*')
				->distinct('contents.content_id')
				->paginate($limit);
			} 
		}
		else{
			if($minr==0 && $maxr==10){
				$contents = DB::table('content_providers')
				->leftJoin('contents','contents.content_id','content_providers.content_id')
				->where('content_providers.provider_id',$provider)
				->where('contents.content_type',$type)
				->where('contents.cinema_release_date','<=',$maxy)
				->where('contents.cinema_release_date','>=',$miny)     
				->where('contents.parent_id','0')
				->select('contents.*')
				->distinct('contents.content_id')
				->paginate($limit);
			}
			else{
				$contents = DB::table('content_providers')
				->leftJoin('contents','contents.content_id','content_providers.content_id')
				->where('content_providers.provider_id',$provider)
				->where('contents.content_type',$type)
				->where('contents.tmdb_score','<=',$maxr)
				->where('contents.tmdb_score','>=',$minr)
				->where('contents.cinema_release_date','<=',$maxy)
				->where('contents.cinema_release_date','>=',$miny)     
				->where('contents.parent_id','0')
				->select('contents.*')
				->distinct('contents.content_id')
				->paginate($limit);
			}
		}
	}
	else{

		if($langs!="" && $rated!="" && $genre!=""){
			if($minr==0 && $maxr==10){
				$contents = DB::table('content_providers')
				->leftJoin('contents','contents.content_id','content_providers.content_id')
				->leftJoin('content_genres','content_genres.content_id','contents.content_id')
				->where('content_providers.provider_id',$provider)
				->where('content_genres.genre_id',$genre)
				->where('contents.age_certification',$rated)
				->where('contents.content_type',$type)
				->where('contents.original_language',$langs)
				->where('contents.cinema_release_date','<=',$maxy)
				->where('contents.cinema_release_date','>=',$miny)
				->where('contents.parent_id','0')
				->orderBy($field,$sort_type)
				->select('contents.*')
				->distinct('contents.content_id')
				->paginate($limit);
			}
			else{
				$contents = DB::table('content_genres')
				->leftJoin('contents','content_genres.content_id','contents.content_id')
				->where('content_genres.genre_id',$genre)
				->where('contents.content_type',$type)
				->where('contents.age_certification',$rated)
				->where('contents.original_language',$langs)
				->where('contents.tmdb_score','<=',$maxr)
				->where('contents.tmdb_score','>=',$minr)
				->where('contents.cinema_release_date','<=',$maxy)
				->where('contents.cinema_release_date','>=',$miny)
				->where('contents.parent_id','0')
				->orderBy($field,$sort_type)
				->paginate($limit);
			}   
		}
		elseif($langs!="" && $rated != ""){
			if($minr==0 && $maxr==10){
				$contents = DB::table('content_providers')
				->leftJoin('contents','contents.content_id','content_providers.content_id')
				->where('content_providers.provider_id',$provider)
				->where('contents.content_type',$type)
				->where('contents.original_language',$langs)
				->where('contents.age_certification',$rated)
				->where('contents.cinema_release_date','<=',$maxy)
				->where('contents.cinema_release_date','>=',$miny)     
				->where('contents.parent_id','0')
				->orderBy($field,$sort_type)
				->select('contents.*')
				->distinct('contents.content_id')
				->paginate($limit); 
			}
			else{
				$contents = DB::table('content_providers')
				->leftJoin('contents','contents.content_id','content_providers.content_id')
				->where('content_providers.provider_id',$provider)
				->where('contents.content_type',$type)
				->where('contents.original_language',$langs)
				->where('contents.age_certification',$rated)
				->where('contents.tmdb_score','<=',$maxr)
				->where('contents.tmdb_score','>=',$minr)
				->where('contents.cinema_release_date','<=',$maxy)
				->where('contents.cinema_release_date','>=',$miny)     
				->where('contents.parent_id','0')
				->orderBy($field,$sort_type)
				->select('contents.*')
				->distinct('contents.content_id')
				->paginate($limit); 
			}

		}
		elseif($rated != "" && $genre!=""){
			if($minr==0 && $maxr==10){
				$contents = DB::table('content_providers')
				->leftJoin('contents','contents.content_id','content_providers.content_id')
				->leftJoin('content_genres','content_genres.content_id','contents.content_id')
				->where('content_providers.provider_id',$provider)
				->where('content_genres.genre_id',$genre)
				->where('contents.age_certification',$rated)
				->where('contents.content_type',$type)
				->where('contents.cinema_release_date','<=',$maxy)
				->where('contents.cinema_release_date','>=',$miny)
				->where('contents.parent_id','0')
				->orderBy($field,$sort_type)
				->select('contents.*')
				->distinct('contents.content_id')
				->paginate($limit);
			}
			else{
				$contents = DB::table('content_providers')
				->leftJoin('contents','contents.content_id','content_providers.content_id')
				->leftJoin('content_genres','content_genres.content_id','contents.content_id')
				->where('content_providers.provider_id',$provider)
				->where('content_genres.genre_id',$genre)
				->where('contents.age_certification',$rated)
				->where('contents.content_type',$type)
				->where('contents.tmdb_score','<=',$maxr)
				->where('contents.tmdb_score','>=',$minr)
				->where('contents.cinema_release_date','<=',$maxy)
				->where('contents.cinema_release_date','>=',$miny)
				->where('contents.parent_id','0')
				->orderBy($field,$sort_type)
				->select('contents.*')
				->distinct('contents.content_id')
				->paginate($limit);
			}      
		}
		elseif($langs != "" && $genre!=""){
			if($minr==0 && $maxr==10){
				$contents = DB::table('content_providers')
				->leftJoin('contents','contents.content_id','content_providers.content_id')
				->leftJoin('content_genres','content_genres.content_id','contents.content_id')
				->where('content_providers.provider_id',$provider)
				->where('content_genres.genre_id',$genre)
				->where('contents.content_type',$type)
				->where('contents.original_language',$langs)
				->where('contents.cinema_release_date','<=',$maxy)
				->where('contents.cinema_release_date','>=',$miny)
				->where('contents.parent_id','0')
				->orderBy($field,$sort_type)
				->select('contents.*')
				->distinct('contents.content_id')
				->paginate($limit);
			}
			else{
				$contents = DB::table('content_providers')
				->leftJoin('contents','contents.content_id','content_providers.content_id')
				->leftJoin('content_genres','content_genres.content_id','contents.content_id')
				->where('content_providers.provider_id',$provider)
				->where('content_genres.genre_id',$genre)
				->where('contents.content_type',$type)
				->where('contents.original_language',$langs)
				->where('contents.tmdb_score','>=',$minr)
				->where('contents.tmdb_score','<=',$maxr)
				->where('contents.cinema_release_date','<=',$maxy)
				->where('contents.cinema_release_date','>=',$miny)
				->where('contents.parent_id','0')
				->orderBy($field,$sort_type)
				->select('contents.*')
				->distinct('contents.content_id')
				->paginate($limit);
			}   
		}
		elseif($langs != ""){
			if($minr==0 && $maxr==10){
				$contents = DB::table('content_providers')
				->leftJoin('contents','contents.content_id','content_providers.content_id')
				->where('content_providers.provider_id',$provider)
				->where('contents.content_type',$type)
				->where('contents.original_language',$langs)
				->where('contents.cinema_release_date','<=',$maxy)
				->where('contents.cinema_release_date','>=',$miny)     
				->where('contents.parent_id','0')
				->orderBy($field,$sort_type)
				->select('contents.*')
				->distinct('contents.content_id')
				->paginate($limit);
			}
			else{
				$contents = DB::table('content_providers')
				->leftJoin('contents','contents.content_id','content_providers.content_id')
				->where('content_providers.provider_id',$provider)
				->where('contents.content_type',$type)
				->where('contents.original_language',$langs)
				->where('contents.tmdb_score','<=',$maxr)
				->where('contents.tmdb_score','>=',$minr)
				->where('contents.cinema_release_date','<=',$maxy)
				->where('contents.cinema_release_date','>=',$miny)     
				->where('contents.parent_id','0')
				->orderBy($field,$sort_type)
				->select('contents.*')
				->distinct('contents.content_id')
				->paginate($limit);
			}     
		}
		elseif($genre!=""){
			if($minr==0 && $maxr==10){
				$contents = DB::table('content_providers')
				->leftJoin('contents','contents.content_id','content_providers.content_id')
				->leftJoin('content_genres','content_genres.content_id','contents.content_id')
				->where('content_providers.provider_id',$provider)
				->where('content_genres.genre_id',$genre)
				->where('contents.content_type',$type)
				->where('contents.cinema_release_date','<=',$maxy)
				->where('contents.cinema_release_date','>=',$miny)
				->where('contents.parent_id','0')
				->orderBy($field,$sort_type)
				->select('contents.*')
				->distinct('contents.content_id')
				->paginate($limit);
			}
			else{
				$contents = DB::table('content_providers')
				->leftJoin('contents','contents.content_id','content_providers.content_id')
				->leftJoin('content_genres','content_genres.content_id','contents.content_id')
				->where('content_providers.provider_id',$provider)
				->where('content_genres.genre_id',$genre)
				->where('contents.content_type',$type)
				->where('contents.tmdb_score','>=',$minr)
				->where('contents.tmdb_score','<=',$maxr)
				->where('contents.cinema_release_date','<=',$maxy)
				->where('contents.cinema_release_date','>=',$miny)
				->where('contents.parent_id','0')
				->orderBy($field,$sort_type)
				->select('contents.*')
				->distinct('contents.content_id')
				->paginate($limit);
			}  
		}
		elseif($rated != ""){
			if($minr==0 && $maxr==10){
				$contents = DB::table('content_providers')
				->leftJoin('contents','contents.content_id','content_providers.content_id')
				->where('content_providers.provider_id',$provider)
				->where('contents.content_type',$type)
				->where('contents.age_certification',$rated)
				->where('contents.cinema_release_date','<=',$maxy)
				->where('contents.cinema_release_date','>=',$miny)     
				->where('contents.parent_id','0')
				->orderBy($field,$sort_type)
				->select('contents.*')
				->distinct('contents.content_id')
				->paginate($limit);
			}
			else{
				$contents = DB::table('content_providers')
				->leftJoin('contents','contents.content_id','content_providers.content_id')
				->where('content_providers.provider_id',$provider)
				->where('contents.content_type',$type)
				->where('contents.original_language',$langs)
				->where('contents.tmdb_score','<=',$maxr)
				->where('contents.tmdb_score','>=',$minr)
				->where('contents.cinema_release_date','<=',$maxy)
				->where('contents.cinema_release_date','>=',$miny)     
				->where('contents.parent_id','0')
				->orderBy($field,$sort_type)
				->select('contents.*')
				->distinct('contents.content_id')
				->paginate($limit);
			}   
		}
		else{
			if($minr==0 && $maxr==10){
				$contents = DB::table('content_providers')
				->leftJoin('contents','contents.content_id','content_providers.content_id')
				->where('content_providers.provider_id',$provider)
				->where('contents.content_type',$type)
				->where('contents.cinema_release_date','<=',$maxy)
				->where('contents.cinema_release_date','>=',$miny)     
				->where('contents.parent_id','0')
				->orderBy($field,$sort_type)
				->select('contents.*')
				->distinct('contents.content_id')
				->paginate($limit);
			}
			else{
				$contents = DB::table('content_providers')
				->leftJoin('contents','contents.content_id','content_providers.content_id')
				->where('content_providers.provider_id',$provider)
				->where('contents.content_type',$type)
				->where('contents.tmdb_score','<=',$maxr)
				->where('contents.tmdb_score','>=',$minr)
				->where('contents.cinema_release_date','<=',$maxy)
				->where('contents.cinema_release_date','>=',$miny)     
				->where('contents.parent_id','0')
				->orderBy($field,$sort_type)
				->select('contents.*')
				->distinct('contents.content_id')
				->paginate($limit);
			}  
		}
	}
}
$miny = explode('-',$miny)[0];
$maxy = explode('-',$maxy)[0];

if(isset($_GET['provider'])){  $provider = $_GET['provider'];  }


		if(isset($_GET['provider'])){  $provider = $_GET['provider'];  }
		foreach ($contents as $key => $value) {
			if (!file_exists(public_path().'/poster/'.$value->poster)){
				$contents[$key]->poster = 'default.png';
			}
		}
		return response()->json(compact('contents'));
	      // return view('khoj.home',);
	
	}

	function latest(Request $request){ 

		$page = 0;
		$langs = "";
		$rated = "";
		$sort = "";
		$field = "";
		$sort_type = "";
		$genre = 0;
		$provider_id = "";
		$minr=0;
		$maxr=10; 
		$type="all";
		$tab = 'Home';

		$maxy = date('Y');;
		// $maxy = $maxy->cinema_release_date;
		$miny = '1920';      

		if(isset($_GET['page'])){
			$page = $_GET['page'];
		}  

		if(isset($_GET['lang'])){  $langs = $_GET['lang'];  }
		if(isset($_GET['provider'])){  $provider_id = explode('_',$_GET['provider'])[1];  }

		if(isset($_GET['sort'])){
			$sort = $_GET['sort'];
			if($sort == "alpha"){
				$field = 'title';
				$sort_type = 'ASC';
			}
			elseif($sort == 'rate'){
				$field = 'tmdb_score';
				$sort_type = 'DESC';
			}
			elseif($sort == 'year'){
				$field = 'cinema_release_date';
				$sort_type = 'DESC';
			}
			elseif($sort == 'imdb_score'){
				$field = 'imdb_score';
				$sort_type = 'DESC';
			}
			else{
				$field = 'content_id';
				$sort_type = 'DESC';
			}      
		}

		if((isset($_GET['sort']) && isset($_GET['genre'])) || (isset($_GET['sort']) && isset($_GET['provider']))){
			$sort = $_GET['sort'];
			if($sort == "alpha"){
				$field = 'contents.title';
				$sort_type = 'ASC';
			}
			elseif($sort == 'rate'){
				$field = 'contents.tmdb_score';
				$sort_type = 'DESC';
			}
			elseif($sort == 'year'){
				$field = 'contents.cinema_release_date';
				$sort_type = 'DESC';
			}
			elseif($sort == 'imdb_score'){
				$field = 'contents.imdb_score';
				$sort_type = 'DESC';
			}
			else{
				$field = 'contents.content_id';
				$sort_type = 'DESC';
			}

		}

		if(isset($_GET['rated'])){
			$rated = $_GET['rated'];
		}

		if(isset($_GET['searchtype'])){
			$type = $_GET['searchtype'];
		}

		if(isset($_GET['genre'])){
			$genre = $_GET['genre'];
		}

		if(isset($_GET['minr'])){
			$minr = $_GET['minr'];
		}
		if(isset($_GET['maxr'])){
			$maxr = $_GET['maxr'];    
		}

		if(isset($_GET['miny'])){
			$miny = $_GET['miny']."-01-01";
		}
		if(isset($_GET['maxy'])){
			$maxy = $_GET['maxy']."-12-31";
		}

		$sort_type = "relevance";
	if (isset($_GET['sort_by'])){
		$sort_type = $_GET['sort_by'];
	}

	$orderBy = "DESC";
	if (isset($_GET['order'])){
		$orderBy = $_GET['order'];
	} 

		$search23 = "CALL ret_home(".$genre.", '".$type."','".$miny."', '".$maxy."', '".$rated."', '".$langs."', ".$page.", $limit, '".$provider_id."', '".$sort_type."', '".$orderBy."')";
	// die();
		$contents1 = DB::select($search23);
		$miny = explode('-',$miny)[0];
		$maxy = explode('-',$maxy)[0];


		if(isset($_GET['provider'])){  $provider = $_GET['provider'];  }
		foreach ($contents1 as $key => $value) {
			if (!file_exists(public_path().'/poster/'.$value->poster)){
				$contents1[$key]->poster = 'default.png';
			}
		}
		$contents = new StdClass;
		$contents->current_page = $page;
		$contents->data = $contents1;
		return response()->json(compact('contents'));
	      // return view('khoj.home',);
	}

	function free_videos(Request $request){ 
		if (isset($request->limit) && $request->limit > 0){
			$limit = $request->limit;
		}
		else
			$limit = 30;

		$page = 0;
	$langs = "";
	$rated = "";
	$sort = "";
	$field = "";
	$sort_type = "";
	$provider = "";
	$genre = "";
	$minr=0;
	$maxr=10; 
	$type="all";
	$tab = 'Free';

	$miny = '0000-00-00';
	$maxy = date('Y-m-d');

  // $miny = DB::table('contents')->where('original_release_year','>','1900')->min('original_release_year');
  // $maxy = DB::table('contents')->max('original_release_year');

	if(isset($_GET['page'])){
		$page = $_GET['page']-1;
	}  

	if(isset($_GET['lang'])){ $langs = $_GET['lang']; }
	if(isset($_GET['provider'])){ $provider = explode('_',$_GET['provider'])[1]; }

	if(isset($_GET['sort'])){
		$sort = $_GET['sort'];
		if($sort == "alpha"){
			$field = 'title';
			$sort_type = 'ASC';
		}
		elseif($sort == 'rate'){
			$field = 'tmdb_score';
			$sort_type = 'DESC';
		}
		elseif($sort == 'year'){
			$field = 'cinema_release_date';
			$sort_type = 'DESC';
		}
		elseif($sort == 'imdb_score'){
			$field = 'imdb_score';
			$sort_type = 'DESC';
		}
		else{
			$field = 'content_id';
			$sort_type = 'DESC';
		}

	}

	if( (isset($_GET['sort']) && isset($_GET['genre'])) || (isset($_GET['sort']) && isset($_GET['provider'])) ){
		$sort = $_GET['sort'];
		if($sort == "alpha"){
			$field = 'contents.title';
			$sort_type = 'ASC';
		}
		elseif($sort == 'rate'){
			$field = 'contents.tmdb_score';
			$sort_type = 'DESC';
		}
		elseif($sort == 'year'){
			$field = 'contents.cinema_release_date';
			$sort_type = 'DESC';
		}
		elseif($sort == 'imdb_score'){
			$field = 'contents.imdb_score';
			$sort_type = 'DESC';
		}
		else{
			$field = 'contents.content_id';
			$sort_type = 'DESC';
		}
	}

	if(isset($_GET['rated'])){
		$rated = $_GET['rated'];
	}

	if(isset($_GET['searchtype'])){
		$type = $_GET['searchtype'];
	}

	if(isset($_GET['genre'])){
		$genre = $_GET['genre'];
	}

	if(isset($_GET['minr'])){
		$minr = $_GET['minr'];
	}
	if(isset($_GET['maxr'])){
		$maxr = $_GET['maxr'];    
	}

	if(isset($_GET['miny'])){
		$miny = $_GET['miny']."-01-01";
	}
	if(isset($_GET['maxy'])){
		$maxy = $_GET['maxy']."-12-31";
	}

	if($type == 'all'){

		if($provider == ""){
			if($sort == ""){
				if($langs!="" && $rated!="" && $genre!=""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->leftJoin('content_genres','content_genres.content_id','contents.content_id')
						->where('content_providers.retail_price','0.00')
						->where('content_providers.monetization_type',"!=",'flatrate')
						->where('content_genres.genre_id',$genre)
						->where('contents.age_certification',$rated)
						->where('contents.original_language',$langs)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.content_type','!=','tv-show')
						->orderBy('contents.cinema_release_date','DESC')
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate('20');
					}
					else{
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->leftJoin('content_genres','content_genres.content_id','contents.content_id')
						->where('content_providers.retail_price','0.00')
						->where('content_providers.monetization_type',"!=",'flatrate')
						->where('content_genres.genre_id',$genre)
						->where('contents.age_certification',$rated)
						->where('contents.original_language',$langs)
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.content_type','!=','tv-show')
						->orderBy('contents.cinema_release_date','DESC')
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate('20');
					}
				}
				elseif($langs!="" && $rated != ""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->where('content_providers.retail_price','0.00')
						->where('content_providers.monetization_type',"!=",'flatrate')
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.age_certification',$rated)
						->where('contents.original_language',$langs)
						->where('contents.content_type','!=','tv-show')
						->orderBy('contents.cinema_release_date','DESC')
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate('20');
					}
					else{
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->where('content_providers.retail_price','0.00')
						->where('content_providers.monetization_type',"!=",'flatrate')
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.age_certification',$rated)
						->where('contents.original_language',$langs)
						->where('contents.content_type','!=','tv-show')
						->orderBy('contents.cinema_release_date','DESC')
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate('20');  
					}
				}
				elseif($rated != "" && $genre!=""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->leftJoin('content_genres','content_genres.content_id','contents.content_id')
						->where('content_providers.retail_price','0.00')
						->where('content_providers.monetization_type',"!=",'flatrate')
						->where('content_genres.genre_id',$genre)
						->where('contents.age_certification',$rated)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.content_type','!=','tv-show')
						->orderBy('contents.cinema_release_date','DESC')
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate('20');
					}
					else{
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->leftJoin('content_genres','content_genres.content_id','contents.content_id')
						->where('content_providers.retail_price','0.00')
						->where('content_providers.monetization_type',"!=",'flatrate')
						->where('content_genres.genre_id',$genre)
						->where('contents.age_certification',$rated)
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.content_type','!=','tv-show')
						->orderBy('contents.cinema_release_date','DESC')
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate('20');
					}
				}
				elseif($langs != "" && $genre!=""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->leftJoin('content_genres','content_genres.content_id','contents.content_id')
						->where('content_providers.retail_price','0.00')
						->where('content_providers.monetization_type',"!=",'flatrate')
						->where('content_genres.genre_id',$genre)
						->where('contents.original_language',$langs)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.content_type','!=','tv-show')
						->orderBy('contents.cinema_release_date','DESC')
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate('20');
					}
					else{
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->leftJoin('content_genres','content_genres.content_id','contents.content_id')
						->where('content_providers.retail_price','0.00')
						->where('content_providers.monetization_type',"!=",'flatrate')
						->where('content_genres.genre_id',$genre)
						->where('contents.original_language',$langs)
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.content_type','!=','tv-show')
						->orderBy('contents.cinema_release_date','DESC')
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate('20');
					}
				}
				elseif($langs != ""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->where('content_providers.retail_price','0.00')
						->where('content_providers.monetization_type',"!=",'flatrate')
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.original_language',$langs)
						->where('contents.content_type','!=','tv-show')
						->orderBy('contents.cinema_release_date','DESC')
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate('20');
					}
					else{
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->where('content_providers.retail_price','0.00')
						->where('content_providers.monetization_type',"!=",'flatrate')
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.original_language',$langs)
						->where('contents.content_type','!=','tv-show')
						->orderBy('contents.cinema_release_date','DESC')
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate('20');
					}
				}
				elseif($genre!=""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->leftJoin('content_genres','content_genres.content_id','contents.content_id')
						->where('content_providers.retail_price','0.00')
						->where('content_providers.provider_id','!=','701')
						->where('content_providers.monetization_type',"!=",'flatrate')
						->where('content_genres.genre_id',$genre)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.content_type','!=','tv-show')
						->orderBy('contents.cinema_release_date','DESC')
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate('20');
					}
					else{
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->leftJoin('content_genres','content_genres.content_id','contents.content_id')
						->where('content_providers.retail_price','0.00')
						->where('content_providers.provider_id','!=','701')
						->where('content_providers.monetization_type',"!=",'flatrate')
						->where('content_genres.genre_id',$genre)
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.content_type','!=','tv-show')
						->orderBy('contents.cinema_release_date','DESC')
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate('20');
					}
				}
				elseif($rated != ""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->where('content_providers.retail_price','0.00')
						->where('content_providers.provider_id','!=','701')
						->where('content_providers.monetization_type',"!=",'flatrate')
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.age_certification',$rated)
						->where('contents.content_type','!=','tv-show')
						->orderBy('contents.cinema_release_date','DESC')
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate('20');
					}
					else{
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->where('content_providers.retail_price','0.00')
						->where('content_providers.provider_id','!=','701')
						->where('content_providers.monetization_type',"!=",'flatrate')
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.age_certification',$rated)
						->where('contents.content_type','!=','tv-show')
						->orderBy('contents.cinema_release_date','DESC')
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate('20');  
					}
				}
				else{
					if($minr==0 && $maxr==10){
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->where('content_providers.retail_price','0.00')
						->where('content_providers.provider_id','!=','701')
						->where('content_providers.monetization_type',"!=",'flatrate')
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.content_type','!=','tv-show')
						->orderBy('contents.cinema_release_date','DESC')
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate('20');
					}
					else{
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->where('content_providers.retail_price','0.00')
						->where('content_providers.provider_id','!=','701')
						->where('content_providers.monetization_type',"!=",'flatrate')
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.content_type','!=','tv-show')
						->orderBy('contents.cinema_release_date','DESC')
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate('20');
					}
				}
			}
			else{

				if($langs!="" && $rated!="" && $genre!=""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->leftJoin('content_genres','content_genres.content_id','contents.content_id')
						->where('content_providers.retail_price','0.00')
						->where('content_providers.provider_id','!=','701')
						->where('content_providers.monetization_type',"!=",'flatrate')
						->where('content_genres.genre_id',$genre)
						->where('contents.age_certification',$rated)
						->where('contents.original_language',$langs)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.content_type','!=','tv-show')
						->orderBy($field,$sort_type)
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate('20');
					}
					else{
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->leftJoin('content_genres','content_genres.content_id','contents.content_id')
						->where('content_providers.retail_price','0.00')
						->where('content_providers.provider_id','!=','701')
						->where('content_providers.monetization_type',"!=",'flatrate')
						->where('content_genres.genre_id',$genre)
						->where('contents.age_certification',$rated)
						->where('contents.original_language',$langs)
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.content_type','!=','tv-show')
						->orderBy($field,$sort_type)
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate('20');
					}
				}
				elseif($langs!="" && $rated != ""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->where('content_providers.retail_price','0.00')
						->where('content_providers.provider_id','!=','701')
						->where('content_providers.monetization_type',"!=",'flatrate')
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.age_certification',$rated)
						->where('contents.original_language',$langs)
						->where('contents.content_type','!=','tv-show')
						->orderBy($field,$sort_type)
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate('20');
					}
					else{
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->where('content_providers.retail_price','0.00')
						->where('content_providers.provider_id','!=','701')
						->where('content_providers.monetization_type',"!=",'flatrate')
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.age_certification',$rated)
						->where('contents.original_language',$langs)
						->where('contents.content_type','!=','tv-show')
						->orderBy($field,$sort_type)
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate('20');  
					}
				}
				elseif($rated != "" && $genre!=""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->leftJoin('content_genres','content_genres.content_id','contents.content_id')
						->where('content_providers.retail_price','0.00')
						->where('content_providers.provider_id','!=','701')
						->where('content_providers.monetization_type',"!=",'flatrate')
						->where('content_genres.genre_id',$genre)
						->where('contents.age_certification',$rated)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.content_type','!=','tv-show')
						->orderBy($field,$sort_type)
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate('20');
					}
					else{
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->leftJoin('content_genres','content_genres.content_id','contents.content_id')
						->where('content_providers.retail_price','0.00')
						->where('content_providers.provider_id','!=','701')
						->where('content_providers.monetization_type',"!=",'flatrate')
						->where('content_genres.genre_id',$genre)
						->where('contents.age_certification',$rated)
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.content_type','!=','tv-show')
						->orderBy($field,$sort_type)
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate('20');
					}
				}
				elseif($langs != "" && $genre!=""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->leftJoin('content_genres','content_genres.content_id','contents.content_id')
						->where('content_providers.retail_price','0.00')
						->where('content_providers.monetization_type',"!=",'flatrate')
						->where('content_genres.genre_id',$genre)
						->where('contents.original_language',$langs)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.content_type','!=','tv-show')
						->orderBy($field,$sort_type)
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate('20');
					}
					else{
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->leftJoin('content_genres','content_genres.content_id','contents.content_id')
						->where('content_providers.retail_price','0.00')
						->where('content_providers.monetization_type',"!=",'flatrate')
						->where('content_genres.genre_id',$genre)
						->where('contents.original_language',$langs)
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.content_type','!=','tv-show')
						->orderBy($field,$sort_type)
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate('20');
					}
				}
				elseif($langs != ""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->where('content_providers.retail_price','0.00')
						->where('content_providers.monetization_type',"!=",'flatrate')
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.original_language',$langs)
						->where('contents.content_type','!=','tv-show')
						->orderBy($field,$sort_type)
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate('20');
					}
					else{
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->where('content_providers.retail_price','0.00')
						->where('content_providers.monetization_type',"!=",'flatrate')
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.original_language',$langs)
						->where('contents.content_type','!=','tv-show')
						->orderBy($field,$sort_type)
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate('20');
					}
				}
				elseif($genre!=""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->leftJoin('content_genres','content_genres.content_id','contents.content_id')
						->where('content_providers.retail_price','0.00')
						->where('content_providers.monetization_type',"!=",'flatrate')
						->where('content_genres.genre_id',$genre)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.content_type','!=','tv-show')
						->orderBy($field,$sort_type)
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate('20');
					}
					else{
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->leftJoin('content_genres','content_genres.content_id','contents.content_id')
						->where('content_providers.retail_price','0.00')
						->where('content_providers.monetization_type',"!=",'flatrate')
						->where('content_genres.genre_id',$genre)
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.content_type','!=','tv-show')
						->orderBy($field,$sort_type)
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate('20');
					}
				}
				elseif($rated != ""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->where('content_providers.retail_price','0.00')
						->where('content_providers.monetization_type',"!=",'flatrate')
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.age_certification',$rated)
						->where('contents.content_type','!=','tv-show')
						->orderBy($field,$sort_type)
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate('20');
					}
					else{
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->where('content_providers.retail_price','0.00')
						->where('content_providers.monetization_type',"!=",'flatrate')
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.age_certification',$rated)
						->where('contents.content_type','!=','tv-show')
						->orderBy($field,$sort_type)
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate('20');  
					}
				}
				else{
					if($minr==0 && $maxr==10){
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->where('content_providers.retail_price','0.00')
						->where('content_providers.monetization_type',"!=",'flatrate')
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.content_type','!=','tv-show')
						->orderBy($field,$sort_type)
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate('20');
					}
					else{
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->where('content_providers.retail_price','0.00')
						->where('content_providers.monetization_type',"!=",'flatrate')
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.content_type','!=','tv-show')
						->orderBy($field,$sort_type)
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate('20');
					}
				}
			}
		}
		else{
			if($sort == ""){
				if($langs!="" && $rated!="" && $genre!=""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->leftJoin('content_genres','content_genres.content_id','contents.content_id')
						->where('content_providers.retail_price','0.00')
						->where('content_providers.monetization_type',"!=",'flatrate')
						->where('content_providers.provider_id',$provider)
						->where('content_genres.genre_id',$genre)
						->where('contents.age_certification',$rated)
						->where('contents.original_language',$langs)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.content_type','!=','tv-show')
						->orderBy('contents.cinema_release_date','DESC')
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate('20');
					}
					else{
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->leftJoin('content_genres','content_genres.content_id','contents.content_id')
						->where('content_providers.retail_price','0.00')
						->where('content_providers.monetization_type',"!=",'flatrate')
						->where('content_providers.provider_id',$provider)
						->where('content_genres.genre_id',$genre)
						->where('contents.age_certification',$rated)
						->where('contents.original_language',$langs)
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.content_type','!=','tv-show')
						->orderBy('contents.cinema_release_date','DESC')
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate('20');
					}
				}
				elseif($langs!="" && $rated != ""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->where('content_providers.retail_price','0.00')
						->where('content_providers.monetization_type',"!=",'flatrate')
						->where('content_providers.provider_id',$provider)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.age_certification',$rated)
						->where('contents.original_language',$langs)
						->where('contents.content_type','!=','tv-show')
						->orderBy('contents.cinema_release_date','DESC')
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate('20');
					}
					else{
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->where('content_providers.retail_price','0.00')
						->where('content_providers.monetization_type',"!=",'flatrate')
						->where('content_providers.provider_id',$provider)
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.age_certification',$rated)
						->where('contents.original_language',$langs)
						->where('contents.content_type','!=','tv-show')
						->orderBy('contents.cinema_release_date','DESC')
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate('20');  
					}
				}
				elseif($rated != "" && $genre!=""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->leftJoin('content_genres','content_genres.content_id','contents.content_id')
						->where('content_providers.retail_price','0.00')
						->where('content_providers.monetization_type',"!=",'flatrate')
						->where('content_providers.provider_id',$provider)
						->where('content_genres.genre_id',$genre)
						->where('contents.age_certification',$rated)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.content_type','!=','tv-show')
						->orderBy('contents.cinema_release_date','DESC')
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate('20');
					}
					else{
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->leftJoin('content_genres','content_genres.content_id','contents.content_id')
						->where('content_providers.retail_price','0.00')
						->where('content_providers.monetization_type',"!=",'flatrate')
						->where('content_providers.provider_id',$provider)
						->where('content_genres.genre_id',$genre)
						->where('contents.age_certification',$rated)
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.content_type','!=','tv-show')
						->orderBy('contents.cinema_release_date','DESC')
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate('20');
					}
				}
				elseif($langs != "" && $genre!=""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->leftJoin('content_genres','content_genres.content_id','contents.content_id')
						->where('content_providers.retail_price','0.00')
						->where('content_providers.monetization_type',"!=",'flatrate')
						->where('content_providers.provider_id',$provider)
						->where('content_genres.genre_id',$genre)
						->where('contents.original_language',$langs)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.content_type','!=','tv-show')
						->orderBy('contents.cinema_release_date','DESC')
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate('20');
					}
					else{
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->leftJoin('content_genres','content_genres.content_id','contents.content_id')
						->where('content_providers.retail_price','0.00')
						->where('content_providers.monetization_type',"!=",'flatrate')
						->where('content_providers.provider_id',$provider)
						->where('content_genres.genre_id',$genre)
						->where('contents.original_language',$langs)
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.content_type','!=','tv-show')
						->orderBy('contents.cinema_release_date','DESC')
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate('20');
					}
				}
				elseif($langs != ""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->where('content_providers.retail_price','0.00')
						->where('content_providers.monetization_type',"!=",'flatrate')
						->where('content_providers.provider_id',$provider)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.original_language',$langs)
						->where('contents.content_type','!=','tv-show')
						->orderBy('contents.cinema_release_date','DESC')
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate('20');
					}
					else{
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->where('content_providers.retail_price','0.00')
						->where('content_providers.monetization_type',"!=",'flatrate')
						->where('content_providers.provider_id',$provider)
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.original_language',$langs)
						->where('contents.content_type','!=','tv-show')
						->orderBy('contents.cinema_release_date','DESC')
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate('20');
					}
				}
				elseif($genre!=""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->leftJoin('content_genres','content_genres.content_id','contents.content_id')
						->where('content_providers.retail_price','0.00')
						->where('content_providers.monetization_type',"!=",'flatrate')
						->where('content_providers.provider_id',$provider)
						->where('content_genres.genre_id',$genre)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.content_type','!=','tv-show')
						->orderBy('contents.cinema_release_date','DESC')
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate('20');
					}
					else{
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->leftJoin('content_genres','content_genres.content_id','contents.content_id')
						->where('content_providers.retail_price','0.00')
						->where('content_providers.monetization_type',"!=",'flatrate')
						->where('content_providers.provider_id',$provider)
						->where('content_genres.genre_id',$genre)
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.content_type','!=','tv-show')
						->orderBy('contents.cinema_release_date','DESC')
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate('20');
					}
				}
				elseif($rated != ""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->where('content_providers.retail_price','0.00')
						->where('content_providers.monetization_type',"!=",'flatrate')
						->where('content_providers.provider_id',$provider)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.age_certification',$rated)
						->where('contents.content_type','!=','tv-show')
						->orderBy('contents.cinema_release_date','DESC')
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate('20');
					}
					else{
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->where('content_providers.retail_price','0.00')
						->where('content_providers.monetization_type',"!=",'flatrate')
						->where('content_providers.provider_id',$provider)
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.age_certification',$rated)
						->where('contents.content_type','!=','tv-show')
						->orderBy('contents.cinema_release_date','DESC')
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate('20');  
					}
				}
				else{
					if($minr==0 && $maxr==10){
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->where('content_providers.retail_price','0.00')
						->where('content_providers.monetization_type',"!=",'flatrate')
						->where('content_providers.provider_id',$provider)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.content_type','!=','tv-show')
						->orderBy('contents.cinema_release_date','DESC')
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate('20');
					}
					else{
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->where('content_providers.retail_price','0.00')
						->where('content_providers.monetization_type',"!=",'flatrate')
						->where('content_providers.provider_id',$provider)
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.content_type','!=','tv-show')
						->orderBy('contents.cinema_release_date','DESC')
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate('20');
					}
				}
			}
			else{

				if($langs!="" && $rated!="" && $genre!=""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->leftJoin('content_genres','content_genres.content_id','contents.content_id')
						->where('content_providers.retail_price','0.00')
						->where('content_providers.monetization_type',"!=",'flatrate')
						->where('content_providers.provider_id',$provider)
						->where('content_genres.genre_id',$genre)
						->where('contents.age_certification',$rated)
						->where('contents.original_language',$langs)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.content_type','!=','tv-show')
						->orderBy($field,$sort_type)
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate('20');
					}
					else{
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->leftJoin('content_genres','content_genres.content_id','contents.content_id')
						->where('content_providers.retail_price','0.00')
						->where('content_providers.monetization_type',"!=",'flatrate')
						->where('content_providers.provider_id',$provider)
						->where('content_genres.genre_id',$genre)
						->where('contents.age_certification',$rated)
						->where('contents.original_language',$langs)
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.content_type','!=','tv-show')
						->orderBy($field,$sort_type)
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate('20');
					}
				}
				elseif($langs!="" && $rated != ""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->where('content_providers.retail_price','0.00')
						->where('content_providers.monetization_type',"!=",'flatrate')
						->where('content_providers.provider_id',$provider)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.age_certification',$rated)
						->where('contents.original_language',$langs)
						->where('contents.content_type','!=','tv-show')
						->orderBy($field,$sort_type)
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate('20');
					}
					else{
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->where('content_providers.retail_price','0.00')
						->where('content_providers.monetization_type',"!=",'flatrate')
						->where('content_providers.provider_id',$provider)
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.age_certification',$rated)
						->where('contents.original_language',$langs)
						->where('contents.content_type','!=','tv-show')
						->orderBy($field,$sort_type)
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate('20');  
					}
				}
				elseif($rated != "" && $genre!=""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->leftJoin('content_genres','content_genres.content_id','contents.content_id')
						->where('content_providers.retail_price','0.00')
						->where('content_providers.monetization_type',"!=",'flatrate')
						->where('content_providers.provider_id',$provider)
						->where('content_genres.genre_id',$genre)
						->where('contents.age_certification',$rated)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.content_type','!=','tv-show')
						->orderBy($field,$sort_type)
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate('20');
					}
					else{
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->leftJoin('content_genres','content_genres.content_id','contents.content_id')
						->where('content_providers.retail_price','0.00')
						->where('content_providers.monetization_type',"!=",'flatrate')
						->where('content_providers.provider_id',$provider)
						->where('content_genres.genre_id',$genre)
						->where('contents.age_certification',$rated)
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.content_type','!=','tv-show')
						->orderBy($field,$sort_type)
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate('20');
					}
				}
				elseif($langs != "" && $genre!=""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->leftJoin('content_genres','content_genres.content_id','contents.content_id')
						->where('content_providers.provider_id',$provider)
						->where('content_providers.retail_price','0.00')
						->where('content_providers.monetization_type',"!=",'flatrate')
						->where('content_genres.genre_id',$genre)
						->where('contents.original_language',$langs)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.content_type','!=','tv-show')
						->orderBy($field,$sort_type)
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate('20');
					}
					else{
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->leftJoin('content_genres','content_genres.content_id','contents.content_id')
						->where('content_providers.retail_price','0.00')
						->where('content_providers.monetization_type',"!=",'flatrate')
						->where('content_providers.provider_id',$provider)
						->where('content_genres.genre_id',$genre)
						->where('contents.original_language',$langs)
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.content_type','!=','tv-show')
						->orderBy($field,$sort_type)
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate('20');
					}
				}
				elseif($langs != ""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->where('content_providers.retail_price','0.00')
						->where('content_providers.monetization_type',"!=",'flatrate')
						->where('content_providers.provider_id',$provider)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.original_language',$langs)
						->where('contents.content_type','!=','tv-show')
						->orderBy($field,$sort_type)
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate('20');
					}
					else{
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->where('content_providers.retail_price','0.00')
						->where('content_providers.monetization_type',"!=",'flatrate')
						->where('content_providers.provider_id',$provider)
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.original_language',$langs)
						->where('contents.content_type','!=','tv-show')
						->orderBy($field,$sort_type)
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate('20');
					}
				}
				elseif($genre!=""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->leftJoin('content_genres','content_genres.content_id','contents.content_id')
						->where('content_providers.retail_price','0.00')
						->where('content_providers.monetization_type',"!=",'flatrate')
						->where('content_providers.provider_id',$provider)
						->where('content_genres.genre_id',$genre)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.content_type','!=','tv-show')
						->orderBy($field,$sort_type)
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate('20');
					}
					else{
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->leftJoin('content_genres','content_genres.content_id','contents.content_id')
						->where('content_providers.retail_price','0.00')
						->where('content_providers.monetization_type',"!=",'flatrate')
						->where('content_providers.provider_id',$provider)
						->where('content_genres.genre_id',$genre)
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.content_type','!=','tv-show')
						->orderBy($field,$sort_type)
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate('20');
					}
				}
				elseif($rated != ""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->where('content_providers.retail_price','0.00')
						->where('content_providers.monetization_type',"!=",'flatrate')
						->where('content_providers.provider_id',$provider)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.age_certification',$rated)
						->where('contents.content_type','!=','tv-show')
						->orderBy($field,$sort_type)
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate('20');
					}
					else{
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->where('content_providers.retail_price','0.00')
						->where('content_providers.monetization_type',"!=",'flatrate')
						->where('content_providers.provider_id',$provider)
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.age_certification',$rated)
						->where('contents.content_type','!=','tv-show')
						->orderBy($field,$sort_type)
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate('20');  
					}
				}
				else{
					if($minr==0 && $maxr==10){
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->where('content_providers.retail_price','0.00')
						->where('content_providers.monetization_type',"!=",'flatrate')
						->where('content_providers.provider_id',$provider)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.content_type','!=','tv-show')
						->orderBy($field,$sort_type)
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate('20');
					}
					else{
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->where('content_providers.retail_price','0.00')
						->where('content_providers.monetization_type',"!=",'flatrate')
						->where('content_providers.provider_id',$provider)
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.content_type','!=','tv-show')
						->orderBy($field,$sort_type)
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate('20');
					}
				}
			}
		}
	}
	else{

		$filter_type = array();
		if($type == 'movie'){
			$filter_type = ['movie'];
		}
		else{
			$filter_type = ['shows','tv-show','show'];        
		}

		if($provider == ""){

			if($sort == ""){
				if($langs!="" && $rated!="" && $genre!=""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->leftJoin('content_genres','content_genres.content_id','contents.content_id')
						->where('content_providers.retail_price','0.00')
						->where('content_providers.monetization_type',"!=",'flatrate')
						->where('content_genres.genre_id',$genre)
						->whereIn('contents.content_type',$filter_type)
						->where('contents.age_certification',$rated)
						->where('contents.original_language',$langs)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->orderBy('contents.cinema_release_date','DESC')
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate('20');
					}
					else{
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->leftJoin('content_genres','content_genres.content_id','contents.content_id')
						->where('content_providers.retail_price','0.00')
						->where('content_providers.monetization_type',"!=",'flatrate')
						->where('content_genres.genre_id',$genre)
						->whereIn('contents.content_type',$filter_type)
						->where('contents.age_certification',$rated)
						->where('contents.original_language',$langs)
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->orderBy('contents.cinema_release_date','DESC')
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate('20');
					}
				}
				elseif($langs!="" && $rated != ""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->where('content_providers.retail_price','0.00')
						->where('content_providers.monetization_type',"!=",'flatrate')
						->whereIn('contents.content_type',$filter_type)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.age_certification',$rated)
						->where('contents.original_language',$langs)
						->orderBy('contents.cinema_release_date','DESC')
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate('20'); 
					}
					else{
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->where('content_providers.retail_price','0.00')
						->where('content_providers.monetization_type',"!=",'flatrate')
						->whereIn('contents.content_type',$filter_type)
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.age_certification',$rated)
						->where('contents.original_language',$langs)
						->orderBy('contents.cinema_release_date','DESC')
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate('20');  
					}
				}
				elseif($rated != "" && $genre!=""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->leftJoin('content_genres','content_genres.content_id','contents.content_id')
						->where('content_providers.retail_price','0.00')
						->where('content_providers.monetization_type',"!=",'flatrate')
						->whereIn('contents.content_type',$filter_type)
						->where('content_genres.genre_id',$genre)
						->where('contents.age_certification',$rated)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->orderBy('contents.cinema_release_date','DESC')
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate('20');
					}
					else{
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->leftJoin('content_genres','content_genres.content_id','contents.content_id')
						->where('content_providers.retail_price','0.00')
						->where('content_providers.monetization_type',"!=",'flatrate')
						->whereIn('contents.content_type',$filter_type)
						->where('content_genres.genre_id',$genre)
						->where('contents.age_certification',$rated)
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->orderBy('contents.cinema_release_date','DESC')
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate('20');
					}
				}
				elseif($langs != "" && $genre!=""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->leftJoin('content_genres','content_genres.content_id','contents.content_id')
						->where('content_providers.retail_price','0.00')
						->where('content_providers.monetization_type',"!=",'flatrate')
						->whereIn('contents.content_type',$filter_type)
						->where('content_genres.genre_id',$genre)
						->where('contents.original_language',$langs)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->orderBy('contents.cinema_release_date','DESC')
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate('20');
					}
					else{
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->leftJoin('content_genres','content_genres.content_id','contents.content_id')
						->where('content_providers.retail_price','0.00')
						->where('content_providers.monetization_type',"!=",'flatrate')
						->whereIn('contents.content_type',$filter_type)
						->where('content_genres.genre_id',$genre)
						->where('contents.original_language',$langs)
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->orderBy('contents.cinema_release_date','DESC')
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate('20');
					}
				}
				elseif($langs != ""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->where('content_providers.retail_price','0.00')
						->where('content_providers.monetization_type',"!=",'flatrate')
						->whereIn('contents.content_type',$filter_type)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.original_language',$langs)
						->orderBy('contents.cinema_release_date','DESC')
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate('20');
					}
					else{
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->where('content_providers.retail_price','0.00')
						->where('content_providers.monetization_type',"!=",'flatrate')
						->whereIn('contents.content_type',$filter_type)
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.original_language',$langs)
						->orderBy('contents.cinema_release_date','DESC')
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate('20');
					}
				}
				elseif($genre!=""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->leftJoin('content_genres','content_genres.content_id','contents.content_id')
						->where('content_providers.retail_price','0.00')
						->where('content_providers.monetization_type',"!=",'flatrate')
						->whereIn('contents.content_type',$filter_type)
						->where('content_genres.genre_id',$genre)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->orderBy('contents.cinema_release_date','DESC')
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate('20');
					}
					else{
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->leftJoin('content_genres','content_genres.content_id','contents.content_id')
						->where('content_providers.retail_price','0.00')
						->where('content_providers.monetization_type',"!=",'flatrate')
						->whereIn('contents.content_type',$filter_type)
						->where('content_genres.genre_id',$genre)
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->orderBy('contents.cinema_release_date','DESC')
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate('20');
					}
				}
				elseif($rated != ""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->where('content_providers.retail_price','0.00')
						->where('content_providers.monetization_type',"!=",'flatrate')
						->whereIn('contents.content_type',$filter_type)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.age_certification',$rated)
						->orderBy('contents.cinema_release_date','DESC')
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate('20'); 
					}
					else{
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->where('content_providers.retail_price','0.00')
						->where('content_providers.monetization_type',"!=",'flatrate')
						->whereIn('contents.content_type',$filter_type)
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.age_certification',$rated)
						->orderBy('contents.cinema_release_date','DESC')
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate('20');  
					}
				}
				else{
					if($minr==0 && $maxr==10){
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->where('content_providers.retail_price','0.00')
						->where('content_providers.monetization_type',"!=",'flatrate')
						->whereIn('contents.content_type',$filter_type)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->orderBy('contents.cinema_release_date','DESC')
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate('20');
					}
					else{
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->where('content_providers.retail_price','0.00')
						->where('content_providers.monetization_type',"!=",'flatrate')
						->whereIn('contents.content_type',$filter_type)
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->orderBy('contents.cinema_release_date','DESC')
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate('20');
					}
				}
			}
			else{

				if($langs!="" && $rated!="" && $genre!=""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->leftJoin('content_genres','content_genres.content_id','contents.content_id')
						->where('content_providers.retail_price','0.00')
						->where('content_providers.monetization_type',"!=",'flatrate')
						->whereIn('contents.content_type',$filter_type)
						->where('content_genres.genre_id',$genre)
						->where('contents.age_certification',$rated)
						->where('contents.original_language',$langs)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->orderBy($field,$sort_type)
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate('20');
					}
					else{
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->leftJoin('content_genres','content_genres.content_id','contents.content_id')
						->where('content_providers.retail_price','0.00')
						->where('content_providers.monetization_type',"!=",'flatrate')
						->whereIn('contents.content_type',$filter_type)
						->where('content_genres.genre_id',$genre)
						->where('contents.age_certification',$rated)
						->where('contents.original_language',$langs)
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->orderBy($field,$sort_type)
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate('20');
					}
				}
				elseif($langs!="" && $rated != ""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->where('content_providers.retail_price','0.00')
						->where('content_providers.monetization_type',"!=",'flatrate')
						->whereIn('contents.content_type',$filter_type)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.age_certification',$rated)
						->where('contents.original_language',$langs)
						->orderBy($field,$sort_type)
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate('20'); 
					}
					else{
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->where('content_providers.retail_price','0.00')
						->where('content_providers.monetization_type',"!=",'flatrate')
						->whereIn('contents.content_type',$filter_type)
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.age_certification',$rated)
						->where('contents.original_language',$langs)
						->orderBy($field,$sort_type)
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate('20');  
					}
				}
				elseif($rated != "" && $genre!=""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->leftJoin('content_genres','content_genres.content_id','contents.content_id')
						->where('content_providers.retail_price','0.00')
						->where('content_providers.monetization_type',"!=",'flatrate')
						->whereIn('contents.content_type',$filter_type)
						->where('content_genres.genre_id',$genre)
						->where('contents.age_certification',$rated)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->orderBy($field,$sort_type)
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate('20');
					}
					else{
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->leftJoin('content_genres','content_genres.content_id','contents.content_id')
						->where('content_providers.retail_price','0.00')
						->where('content_providers.monetization_type',"!=",'flatrate')
						->whereIn('contents.content_type',$filter_type)
						->where('content_genres.genre_id',$genre)
						->where('contents.age_certification',$rated)
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->orderBy($field,$sort_type)
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate('20');
					}
				}
				elseif($langs != "" && $genre!=""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->leftJoin('content_genres','content_genres.content_id','contents.content_id')
						->where('content_providers.retail_price','0.00')
						->where('content_providers.monetization_type',"!=",'flatrate')
						->where('content_genres.genre_id',$genre)
						->whereIn('contents.content_type',$filter_type)
						->where('contents.original_language',$langs)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->orderBy($field,$sort_type)
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate('20');
					}
					else{
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->leftJoin('content_genres','content_genres.content_id','contents.content_id')
						->where('content_providers.retail_price','0.00')
						->where('content_providers.monetization_type',"!=",'flatrate')
						->where('content_genres.genre_id',$genre)
						->whereIn('contents.content_type',$filter_type)
						->where('contents.original_language',$langs)
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->orderBy($field,$sort_type)
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate('20');
					}
				}
				elseif($langs != ""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->where('content_providers.retail_price','0.00')
						->where('content_providers.monetization_type',"!=",'flatrate')
						->whereIn('contents.content_type',$filter_type)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.original_language',$langs)
						->orderBy($field,$sort_type)
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate('20');
					}
					else{
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->where('content_providers.retail_price','0.00')
						->where('content_providers.monetization_type',"!=",'flatrate')
						->whereIn('contents.content_type',$filter_type)
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.original_language',$langs)
						->orderBy($field,$sort_type)
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate('20');
					}
				}
				elseif($genre!=""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->leftJoin('content_genres','content_genres.content_id','contents.content_id')
						->where('content_providers.retail_price','0.00')
						->where('content_providers.monetization_type',"!=",'flatrate')
						->whereIn('contents.content_type',$filter_type)
						->where('content_genres.genre_id',$genre)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->orderBy($field,$sort_type)
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate('20');
					}
					else{
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->leftJoin('content_genres','content_genres.content_id','contents.content_id')
						->where('content_providers.retail_price','0.00')
						->where('content_providers.monetization_type',"!=",'flatrate')
						->whereIn('contents.content_type',$filter_type)
						->where('content_genres.genre_id',$genre)
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->orderBy($field,$sort_type)
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate('20');
					}
				}
				elseif($rated != ""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->where('content_providers.retail_price','0.00')
						->where('content_providers.monetization_type',"!=",'flatrate')
						->whereIn('contents.content_type',$filter_type)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.age_certification',$rated)
						->orderBy($field,$sort_type)
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate('20');  
					}
					else{
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->where('content_providers.retail_price','0.00')
						->where('content_providers.monetization_type',"!=",'flatrate')
						->whereIn('contents.content_type',$filter_type)
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.age_certification',$rated)
						->orderBy($field,$sort_type)
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate('20');  
					}
				}
				else{
					if($minr==0 && $maxr==10){
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->where('content_providers.retail_price','0.00')
						->where('content_providers.monetization_type',"!=",'flatrate')
						->whereIn('contents.content_type',$filter_type)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->orderBy($field,$sort_type)
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate('20');
					}
					else{
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->where('content_providers.retail_price','0.00')
						->where('content_providers.monetization_type',"!=",'flatrate')
						->whereIn('contents.content_type',$filter_type)
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->orderBy($field,$sort_type)
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate('20');
					}
				}
			}
		}





		else{
			if($sort == ""){
				if($langs!="" && $rated!="" && $genre!=""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->leftJoin('content_genres','content_genres.content_id','contents.content_id')
						->where('content_providers.retail_price','0.00')
						->where('content_providers.monetization_type',"!=",'flatrate')
						->where('content_providers.provider_id',$provider)
						->where('content_genres.genre_id',$genre)
						->whereIn('contents.content_type',$filter_type)
						->where('contents.age_certification',$rated)
						->where('contents.original_language',$langs)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->orderBy('contents.cinema_release_date','DESC')
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate('20');
					}
					else{
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->leftJoin('content_genres','content_genres.content_id','contents.content_id')
						->where('content_providers.retail_price','0.00')
						->where('content_providers.monetization_type',"!=",'flatrate')
						->where('content_providers.provider_id',$provider)
						->where('content_genres.genre_id',$genre)
						->whereIn('contents.content_type',$filter_type)
						->where('contents.age_certification',$rated)
						->where('contents.original_language',$langs)
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->orderBy('contents.cinema_release_date','DESC')
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate('20');
					}
				}
				elseif($langs!="" && $rated != ""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->where('content_providers.retail_price','0.00')
						->where('content_providers.monetization_type',"!=",'flatrate')
						->where('content_providers.provider_id',$provider)
						->whereIn('contents.content_type',$filter_type)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.age_certification',$rated)
						->where('contents.original_language',$langs)
						->orderBy('contents.cinema_release_date','DESC')
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate('20'); 
					}
					else{
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->where('content_providers.retail_price','0.00')
						->where('content_providers.monetization_type',"!=",'flatrate')
						->where('content_providers.provider_id',$provider)
						->whereIn('contents.content_type',$filter_type)
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.age_certification',$rated)
						->where('contents.original_language',$langs)
						->orderBy('contents.cinema_release_date','DESC')
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate('20');  
					}
				}
				elseif($rated != "" && $genre!=""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->leftJoin('content_genres','content_genres.content_id','contents.content_id')
						->where('content_providers.retail_price','0.00')
						->where('content_providers.monetization_type',"!=",'flatrate')
						->where('content_providers.provider_id',$provider)
						->whereIn('contents.content_type',$filter_type)
						->where('content_genres.genre_id',$genre)
						->where('contents.age_certification',$rated)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->orderBy('contents.cinema_release_date','DESC')
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate('20');
					}
					else{
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->leftJoin('content_genres','content_genres.content_id','contents.content_id')
						->where('content_providers.retail_price','0.00')
						->where('content_providers.monetization_type',"!=",'flatrate')
						->where('content_providers.provider_id',$provider)
						->whereIn('contents.content_type',$filter_type)
						->where('content_genres.genre_id',$genre)
						->where('contents.age_certification',$rated)
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->orderBy('contents.cinema_release_date','DESC')
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate('20');
					}
				}
				elseif($langs != "" && $genre!=""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->leftJoin('content_genres','content_genres.content_id','contents.content_id')
						->where('content_providers.retail_price','0.00')
						->where('content_providers.monetization_type',"!=",'flatrate')
						->where('content_providers.provider_id',$provider)
						->whereIn('contents.content_type',$filter_type)
						->where('content_genres.genre_id',$genre)
						->where('contents.original_language',$langs)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->orderBy('contents.cinema_release_date','DESC')
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate('20');
					}
					else{
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->leftJoin('content_genres','content_genres.content_id','contents.content_id')
						->where('content_providers.retail_price','0.00')
						->where('content_providers.monetization_type',"!=",'flatrate')
						->where('content_providers.provider_id',$provider)
						->whereIn('contents.content_type',$filter_type)
						->where('content_genres.genre_id',$genre)
						->where('contents.original_language',$langs)
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->orderBy('contents.cinema_release_date','DESC')
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate('20');
					}
				}
				elseif($langs != ""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->where('content_providers.retail_price','0.00')
						->where('content_providers.monetization_type',"!=",'flatrate')
						->where('content_providers.provider_id',$provider)
						->whereIn('contents.content_type',$filter_type)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.original_language',$langs)
						->orderBy('contents.cinema_release_date','DESC')
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate('20');
					}
					else{
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->where('content_providers.retail_price','0.00')
						->where('content_providers.monetization_type',"!=",'flatrate')
						->where('content_providers.provider_id',$provider)
						->whereIn('contents.content_type',$filter_type)
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.original_language',$langs)
						->orderBy('contents.cinema_release_date','DESC')
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate('20');
					}
				}
				elseif($genre!=""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->leftJoin('content_genres','content_genres.content_id','contents.content_id')
						->where('content_providers.retail_price','0.00')
						->where('content_providers.monetization_type',"!=",'flatrate')
						->where('content_providers.provider_id',$provider)
						->whereIn('contents.content_type',$filter_type)
						->where('content_genres.genre_id',$genre)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->orderBy('contents.cinema_release_date','DESC')
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate('20');
					}
					else{
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->leftJoin('content_genres','content_genres.content_id','contents.content_id')
						->where('content_providers.retail_price','0.00')
						->where('content_providers.monetization_type',"!=",'flatrate')
						->where('content_providers.provider_id',$provider)
						->whereIn('contents.content_type',$filter_type)
						->where('content_genres.genre_id',$genre)
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->orderBy('contents.cinema_release_date','DESC')
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate('20');
					}
				}
				elseif($rated != ""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->where('content_providers.retail_price','0.00')
						->where('content_providers.monetization_type',"!=",'flatrate')
						->where('content_providers.provider_id',$provider)
						->whereIn('contents.content_type',$filter_type)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.age_certification',$rated)
						->orderBy('contents.cinema_release_date','DESC')
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate('20'); 
					}
					else{
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->where('content_providers.retail_price','0.00')
						->where('content_providers.monetization_type',"!=",'flatrate')
						->where('content_providers.provider_id',$provider)
						->whereIn('contents.content_type',$filter_type)
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.age_certification',$rated)
						->orderBy('contents.cinema_release_date','DESC')
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate('20');  
					}
				}
				else{
					if($minr==0 && $maxr==10){
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->where('content_providers.retail_price','0.00')
						->where('content_providers.monetization_type',"!=",'flatrate')
						->where('content_providers.provider_id',$provider)
						->whereIn('contents.content_type',$filter_type)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->orderBy('contents.cinema_release_date','DESC')
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate('20');
					}
					else{
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->where('content_providers.retail_price','0.00')
						->where('content_providers.monetization_type',"!=",'flatrate')
						->whereIn('contents.content_type',$filter_type)
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->orderBy('contents.cinema_release_date','DESC')
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate('20');
					}
				}
			}
			else{

				if($langs!="" && $rated!="" && $genre!=""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->leftJoin('content_genres','content_genres.content_id','contents.content_id')
						->where('content_providers.retail_price','0.00')
						->where('content_providers.monetization_type',"!=",'flatrate')
						->where('content_providers.provider_id',$provider)
						->whereIn('contents.content_type',$filter_type)
						->where('content_genres.genre_id',$genre)
						->where('contents.age_certification',$rated)
						->where('contents.original_language',$langs)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->orderBy($field,$sort_type)
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate('20');
					}
					else{
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->leftJoin('content_genres','content_genres.content_id','contents.content_id')
						->where('content_providers.retail_price','0.00')
						->where('content_providers.monetization_type',"!=",'flatrate')
						->where('content_providers.provider_id',$provider)
						->whereIn('contents.content_type',$filter_type)
						->where('content_genres.genre_id',$genre)
						->where('contents.age_certification',$rated)
						->where('contents.original_language',$langs)
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->orderBy($field,$sort_type)
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate('20');
					}
				}
				elseif($langs!="" && $rated != ""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->where('content_providers.retail_price','0.00')
						->where('content_providers.monetization_type',"!=",'flatrate')
						->where('content_providers.provider_id',$provider)
						->whereIn('contents.content_type',$filter_type)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.age_certification',$rated)
						->where('contents.original_language',$langs)
						->orderBy($field,$sort_type)
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate('20'); 
					}
					else{
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->where('content_providers.retail_price','0.00')
						->where('content_providers.monetization_type',"!=",'flatrate')
						->where('content_providers.provider_id',$provider)
						->whereIn('contents.content_type',$filter_type)
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.age_certification',$rated)
						->where('contents.original_language',$langs)
						->orderBy($field,$sort_type)
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate('20');  
					}
				}
				elseif($rated != "" && $genre!=""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->leftJoin('content_genres','content_genres.content_id','contents.content_id')
						->where('content_providers.retail_price','0.00')
						->where('content_providers.monetization_type',"!=",'flatrate')
						->where('content_providers.provider_id',$provider)
						->whereIn('contents.content_type',$filter_type)
						->where('content_genres.genre_id',$genre)
						->where('contents.age_certification',$rated)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->orderBy($field,$sort_type)
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate('20');
					}
					else{
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->leftJoin('content_genres','content_genres.content_id','contents.content_id')
						->where('content_providers.retail_price','0.00')
						->where('content_providers.monetization_type',"!=",'flatrate')
						->where('content_providers.provider_id',$provider)
						->whereIn('contents.content_type',$filter_type)
						->where('content_genres.genre_id',$genre)
						->where('contents.age_certification',$rated)
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->orderBy($field,$sort_type)
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate('20');
					}
				}
				elseif($langs != "" && $genre!=""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->leftJoin('content_genres','content_genres.content_id','contents.content_id')
						->where('content_providers.retail_price','0.00')
						->where('content_providers.monetization_type',"!=",'flatrate')
						->where('content_providers.provider_id',$provider)
						->where('content_genres.genre_id',$genre)
						->whereIn('contents.content_type',$filter_type)
						->where('contents.original_language',$langs)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->orderBy($field,$sort_type)
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate('20');
					}
					else{
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->leftJoin('content_genres','content_genres.content_id','contents.content_id')
						->where('content_providers.retail_price','0.00')
						->where('content_providers.monetization_type',"!=",'flatrate')
						->where('content_providers.provider_id',$provider)
						->where('content_genres.genre_id',$genre)
						->whereIn('contents.content_type',$filter_type)
						->where('contents.original_language',$langs)
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->orderBy($field,$sort_type)
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate('20');
					}
				}
				elseif($langs != ""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->where('content_providers.retail_price','0.00')
						->where('content_providers.monetization_type',"!=",'flatrate')
						->where('content_providers.provider_id',$provider)
						->whereIn('contents.content_type',$filter_type)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.original_language',$langs)
						->orderBy($field,$sort_type)
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate('20');
					}
					else{
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->where('content_providers.retail_price','0.00')
						->where('content_providers.monetization_type',"!=",'flatrate')
						->where('content_providers.provider_id',$provider)
						->whereIn('contents.content_type',$filter_type)
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.original_language',$langs)
						->orderBy($field,$sort_type)
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate('20');
					}
				}
				elseif($genre!=""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->leftJoin('content_genres','content_genres.content_id','contents.content_id')
						->where('content_providers.retail_price','0.00')
						->where('content_providers.monetization_type',"!=",'flatrate')
						->where('content_providers.provider_id',$provider)
						->whereIn('contents.content_type',$filter_type)
						->where('content_genres.genre_id',$genre)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->orderBy($field,$sort_type)
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate('20');
					}
					else{
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->leftJoin('content_genres','content_genres.content_id','contents.content_id')
						->where('content_providers.retail_price','0.00')
						->where('content_providers.monetization_type',"!=",'flatrate')
						->where('content_providers.provider_id',$provider)
						->whereIn('contents.content_type',$filter_type)
						->where('content_genres.genre_id',$genre)
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->orderBy($field,$sort_type)
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate('20');
					}
				}
				elseif($rated != ""){
					if($minr==0 && $maxr==10){
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->where('content_providers.retail_price','0.00')
						->where('content_providers.monetization_type',"!=",'flatrate')
						->where('content_providers.provider_id',$provider)
						->whereIn('contents.content_type',$filter_type)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.age_certification',$rated)
						->orderBy($field,$sort_type)
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate('20');  
					}
					else{
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->where('content_providers.retail_price','0.00')
						->where('content_providers.monetization_type',"!=",'flatrate')
						->where('content_providers.provider_id',$provider)
						->whereIn('contents.content_type',$filter_type)
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->where('contents.age_certification',$rated)
						->orderBy($field,$sort_type)
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate('20');  
					}
				}
				else{
					if($minr==0 && $maxr==10){
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->where('content_providers.retail_price','0.00')
						->where('content_providers.monetization_type',"!=",'flatrate')
						->where('content_providers.provider_id',$provider)
						->whereIn('contents.content_type',$filter_type)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->orderBy($field,$sort_type)
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate('20');
					}
					else{
						$contents = DB::table('content_providers')
						->leftJoin('contents','contents.content_id','content_providers.content_id')
						->where('content_providers.retail_price','0.00')
						->where('content_providers.monetization_type',"!=",'flatrate')
						->where('content_providers.provider_id',$provider)
						->whereIn('contents.content_type',$filter_type)
						->where('contents.tmdb_score','<=',$maxr)
						->where('contents.tmdb_score','>=',$minr)
						->where('contents.cinema_release_date','<=',$maxy)
						->where('contents.cinema_release_date','>=',$miny)
						->orderBy($field,$sort_type)
						->select('contents.*')
						->distinct('contents.content_id')
						->paginate('20');
					}
				}
			}
		}
	}

	$miny = explode('-',$miny)[0];
	$maxy = explode('-',$maxy)[0];

		if(isset($_GET['provider'])){  $provider = $_GET['provider'];  }
		foreach ($contents as $key => $value) {
			if (!file_exists(public_path().'/poster/'.$value->poster)){
				$contents[$key]->poster = 'default.png';
			}
		}
		return response()->json(compact('contents'));
	      // return view('khoj.home',);
	}


public function dynamic_search(Request $request)
{       
	$response = new StdClass;
	$response->status = 200;
	$response->message = "data received";
	$keyword = $request->data;
	$search = $request->search; 
      //$keyword = str_replace(' ', '+', $keyword);
      // $search = str_replace(' ', '+', $search);
      // $this->searchjustwatch($request);
	$content = DB::select("CALL ret_search_help('".$keyword."','ALL' ,5)");
      // dd($content);
	$contents = array();
	$casts = array();
	foreach ($content as $key => $value) {

		$value = explode(',', $value->result);
		$obj = new StdClass;
		if ($value[0] == 'content'){
			$obj->content_id = $value[1];
			$obj->title = $value[2];
			$obj->original_title = $value[3];
			$obj->original_release_year = $value[4];
			$obj->poster = $value[5];
			$obj->content_type = $value[6];
			array_push($contents, $obj);
		}
		elseif ($value[0] == 'person'){
          // dd($value);
			$obj->person_id = $value[1];
			$obj->name = $value[2];
			array_push($casts, $obj);
		}


	}
	foreach ($contents as $key => $value) {
		if (!file_exists(public_path().'/poster/'.$value->poster)){
			$contents[$key]->poster = 'default.png';
		}
	}
	$response->movies = $contents;
	$response->cast = $casts;
	return response()->json($response);
              // return view('khoj.dynamic-search', compact('contents', 'keyword','casts'));
}

function movie_details(Request $request){

	$response = new StdClass;
	$response->status = 200;
	$response->message = "data received";
	$type = 'movie';
	$type = $request->type;
	$id = $request->id;
	$related_movies = new StdClass;
	$title="";
	$movie = Content::where('content_id',$id)->first();
      // dd($movie);

	if ($movie){
        // if ($movie->content_source == 'justwatch'){
        //   $justwatchcheck = $this->verifyfromjustwatch($movie);
        // }
        // elseif ($movie->content_source == 'mxplayer'){
        //   $id = $justwatchcheck = $this->verifyfrommxplayer($movie);
        //   $movie = Content::where('content_id',$id)->first();
        // }

        // elseif ($movie->content_source == 'ullu'){
        //   $justwatchcheck = $this->verifyfromullu($movie);
        // }
        // =================================justwatch==============================

		if($movie && isset($movie->title)){
			$title  = $movie->title;
		}
		elseif(isset($movie->original_title)){
			$title = $movie->original_title;
		}
	}

      // $related_movies = DB::table('contents')
      //                   ->where('title','LIKE','%'.$title.'%')
      //                   ->where('content_id','!=',$id)
      //                   ->where('content_type','!=','tv-season')
      //                   ->where('content_type','!=','tv-episode')
      //                   ->limit(20)
      //                   ->get();

	$movie = Content::where('content_id',$id)->first();
	$season_id = 0;

	if($movie) {
		$casts = DB::table('content_casting')
		->where('content_id',$id)
		->orderBy('role')
		->get();
		if ($casts){
			$castlist = "";
			foreach ($casts as $key => $value) {
				$castlist .= $value->name.",";
			}

			$movie->casts = $castlist;
		}
		else
			$movie->casts = "";
		$genres = DB::table('content_genres')
		->leftJoin('genres','genres.genre_id','content_genres.genre_id')
		->where('content_genres.content_id',$id)
		->select('genres.genre_id','genres.title')
		->distinct('genre_id')
		->get();
		$genrelist = "";

		foreach ($genres as $key => $value) {
			$genrelist .= $value->title.",";
		}

		$movie->genres = $genrelist;

		$clips = DB::table('content_clips')
		->where('content_id',$id)
		->get();
		$movie->clips = $clips;
		$movie->favourite = 1;
		$movie->watchlist = 0;
		$movie->clips = $clips;

		$review = new StdClass;
		$review->name = "shashank";
		$review->review = "This is a very good movie. Must watch";
		$movie->reviews = [$review, $review];
		$movie->share_url = "download the app to see the movie from http://google.com or visit url http://khojapp.com";
		$providers = DB::table('content_providers')
		->leftJoin('providers', 'providers.provider_id', 'content_providers.provider_id')
		->where('content_providers.content_id',$id)
		->select('content_providers.content_id','content_providers.provider_id','content_providers.video_quality','content_providers.monetization_type','content_providers.retail_price','content_providers.currency','content_providers.web_url', 'providers.icon_url')
		->distinct('content_providers.provider_id')
		->orderBy('content_providers.retail_price', 'desc')
		->orderBy('content_providers.video_quality', 'desc')
		->get();
		$count = $movie->click_count+1;
		$change_movie = DB::table('contents')
		->where('content_id',$id)
		->update(array(
			'click_count' => $count,
		));

		$episodes = array();
		$season_id = 0;
		$season_one = "";
		$seasons = DB::table('contents')->where('parent_id',$movie->content_id)->where('content_type','tv-season')->orderBy('cinema_release_date','DESC')->orderBy('content_id','DESC')->get();
		if ($seasons)
			$movie->seasons = count($seasons);
      // // dd($seasons);
      // if(!empty($seasons) && !empty($seasons[0])){
      //   $season_id = $seasons[0]->content_id ?? '';
      //   $season_one = $seasons[0]->title ?? '';
      // }
      // else{
      //   $season_id = $movie->content_id;
      //   $season_one = $movie->title ?? '';
      // }
		$banners = DB::table('content_banners')->where('content_id',$movie->content_id)->get();
		$movie->banners = $banners;
	}




      // if($season_id>0){
      //   $episodes = DB::table('contents')->where('parent_id',$season_id)->where('content_type','tv-episode')->orderBy('cinema_release_date','DESC')->orderBy('content_id','DESC')->get();
      // }


	if (isset($movie->content_type))
		$type = $movie->content_type;
	else
		$type = "";

	$movie->providers = $providers;
	$response->movie = $movie;
	return response()->json($response);
	return view(compact('movie','casts','genres','banners','clips','providers','related_movies','type','seasons','episodes','season_one','banners'));

}

function similar_movie(Request $request){
	if (isset($request->limit) && $request->limit > 0){
			$limit = $request->limit;
		}
		else
			$limit = 30;
	$response = new StdClass;
	$response->status = 200;
	$response->message = "data received";
	$type = 'movie';
	$type = $request->type;
	$id = $request->id;
	$related_movies = new StdClass;
	$title="";
	$movie = Content::where('content_id',$id)->first();
      // dd($movie);

	if ($movie){
		if($movie && isset($movie->title)){
			$title  = $movie->title;
		}
		elseif(isset($movie->original_title)){
			$title = $movie->original_title;
		}
	}

	$related_movies = DB::table('contents')
	->where('title','LIKE','%'.$title.'%')
	->where('content_id','!=',$id)
	->where('content_type','!=','tv-season')
	->where('content_type','!=','tv-episode')
	->limit(20)
	->get();


	$response->similar_movie = $related_movies;
	return response()->json($response);

}

public function show_seasons(Request $request)
{
	$content_id = $request->show_id;
	$seasons = DB::table('contents')->where('parent_id',$content_id)->where('content_type','tv-season')->orderBy('cinema_release_date','DESC')->select('content_id', 'content_type', 'title', 'original_title')->orderBy('content_id','DESC')->get();
	$response = new stdClass;
	$response->status = 200;
	$response->message = "Data received";
	$response->seasons = $seasons;
	return response()->json($response);
}

public function getbanners(Request $request)
{
	$featureds = FeaturedImage::where('status','Active')->where('start_date','<=',date('Y-m-d'))->where('end_date','>=',date('Y-m-d'))->orderBy('display_priority', 'asc')->get();
	$response = new stdClass;
	$response->status = 200;
	$response->message = "Data received";
	$response->banners = $featureds;
	return response()->json($response);
}

public function add_to_favourite(Request $request)
{
	$movie_id = $request->movie_id;
	$movie = Content::where('content_id',$movie_id)->first();
	$response = new stdClass;
	$response->status = 400;
	$response->message = "Data received";
	$response->user_id = auth()->user()->id;
	$message = 'Something Went Wrong';
	if ($movie){
		if (isset(auth()->user()->id)){
			$favourite = Favourite::where('movie_id', $movie_id)->where('user_id', auth()->user()->id)->first();
			if ($favourite){
          // return redirect('/movie_detail/'.$id);
				$message = "Already in your watchlist";

			}
			else{
				$favourite = new Favourite;
				$favourite->movie_id = $movie_id;
				$favourite->type = $movie->content_type;
				$favourite->user_id = auth()->user()->id;
				$favourite->save();
				$response->status = 200;
				$message = "Added to watchlist successfully";

          // return redirect('movie_detail/'.$id);
			}
		}
		else
			$message = "Login again";
	}
	else{
		$message = "No movie with this name";
	}
	$response->message = $message;
	return response()->json($response);
}


public function add_to_watchlist(Request $request)
{
	$movie_id = $request->movie_id;
	$movie = Content::where('content_id',$movie_id)->first();
	$response = new stdClass;
	$response->status = 400;
	$response->message = "Data received";
	$response->user_id = auth()->user()->id;
	$message = 'Something Went Wrong';
	if ($movie){
		if (isset(auth()->user()->id)){
			$favourite = WatchList::where('movie_id', $movie_id)->where('user_id', auth()->user()->id)->first();
			if ($favourite){
          // return redirect('/movie_detail/'.$id);
				$message = "Already in your favourite list";

			}
			else{
				$favourite = new Watchlist;
				$favourite->movie_id = $movie_id;
				$favourite->type = $movie->content_type;
				$favourite->user_id = auth()->user()->id;
				$favourite->save();
				$response->status = 200;
				$message = "Added to favourite list successfully";

          // return redirect('movie_detail/'.$id);
			}
		}
		else
			$message = "Login again";
	}
	else{
		$message = "No movie with this name";
	}
	$response->message = $message;
	return response()->json($response);
}


public function favourite(){
	$response = new stdClass;
	$response->status = 400;
	$message = "Data received";
	$response->user_id = auth()->user()->id;
	$message = 'Something Went Wrong';

	$contents = Favourite::join('contents', 'contents.content_id', '=', 'favourites.movie_id')->where('favourites.user_id', auth()->user()->id)->get();
	if ($contents){
		$response->list = $contents;
		$message = "data received";
	}
	$response->message = $message;
	return response()->json($response);
}

public function watchlist(){
	$response = new stdClass;
	$response->status = 400;
	$message = "Data received";
	$response->user_id = auth()->user()->id;
	$message = 'Something Went Wrong';


	$contents = WatchList::join('contents', 'contents.content_id', '=', 'watch_lists.movie_id')->where('watch_lists.user_id', auth()->user()->id)->get();
	if ($contents){
		$response->list = $contents;
		$message = "data received";
	}
	$response->message = $message;
	return response()->json($response);
}


public function get_filters(Request $request)
{
	$response = new stdClass;
      // $providers1 = DB::table('providers')->first(); 
	$languages = DB::table('languages')
	->leftJoin('contents','contents.original_language','languages.code')
	->where('contents.original_language',"!=",NULL)
	->where('languages.status','Active')
	->select('contents.original_language','languages.english_name')
	->orderBy('languages.display_priority','ASC')
	->distinct('contents.original_language')
	->get();
	$categories = ['all', 'movies', 'TV Shows', 'Viral Video'];

	$all_providers = DB::table('providers')->where('display_priority', '>', 0)->select('provider_id', 'title', 'icon_url')->orderBy('display_priority','ASC')->get();
	$all_genres = Genre::select('genre_id','title')->distinct('genre_id')->get();
	$flag = 0;

      // $featureds = session()->get('featureds');
	$featureds = FeaturedImage::where('status','Active')->where('start_date','<=',date('Y-m-d'))->where('end_date','>=',date('Y-m-d'))->orderBy('display_priority', 'asc')->get();


	$minyr = 1900;
	$maxyr = 2020;
	$release_year = [$minyr, $maxyr];
	$rating = [0, 1];
	$obj = array(
		'categories' => $categories,
		'providers' => $all_providers,
		'langauge' => $languages,
		'genres' => $all_genres,
		'release_year' => $release_year,
		'rating' => $rating,
	);

	$response->status = 200;
	$response->message = "Data received";
	$response->filters = $obj;
	return response()->json($response);
}

public function getSearch(Request $request)
{   
	if (isset($request->limit) && $request->limit > 0){
			$limit = $request->limit;
		}
		else
			$limit = 30;
	$keyword = $request->data;
	$search = $request->data; 
	$request->data = $request->data ;
	$response = new stdClass;
	$response->status = 400;
	$message = 'Something Went Wrong';
    // $response->user_id = auth()->user()->id;


      // $this->getmxplayerdata($request);
      // $this->searchjustwatch($request);
      // $this->getulludata($request);



	$page = 0;
	$langs = 'ALL';
	$rated = "ALL";
	$sort = "";
	$field = "title";
	$sort_type = "ASC";
	$genre = 0;
	$minr=0;
	$maxr=10; 

	$tab= 'search';
	$provider_id = 0;
	if (isset($request->provider)){
		$provider_id = explode('_', $request->provider)[1];
	}

	$miny = '0000-00-00';
	$maxy = '2020-12-12';

	$maxy = $maxy->cinema_release_date;
  // $miny = DB::table('contents')->where('original_release_year','>','1900')->min('original_release_year');
  // $maxy = DB::table('contents')->max('original_release_year');

	if(isset($_GET['page'])){
		$page = $_GET['page']-1;
	}  

	$keyword = $request->data;
	if ($request->searchtype)
		$type = $request->searchtype;
	else
		$type = 'ALL';
	$searchtype = $type;
	if ($keyword != ""){
		$findKeyword = SearchKeyword::where('keyword',$keyword)->first();

		if(empty($findKeyword)){
			$search_keyword = new  SearchKeyword;
			if (isset(auth()->user()->id)){
				$search_keyword->user_id = auth()->user()->id;
			}
			$search_keyword->keyword = $keyword;
			$search_keyword->save();
		}     
	}
	else{
		$response->message = $message;
		return response()->json($response);
	}

	if(isset($_GET['lang'])){  $langs = $_GET['lang'];  }

	if(isset($_GET['sort'])){
		$sort = $_GET['sort'];
		if($sort == "alpha"){
			$field = 'contents.title';
			$sort_type = 'ASC';
		}
		elseif($sort == 'rate'){
			$field = 'contents.tmdb_score';
			$sort_type = 'DESC';
		}
		elseif($sort == 'year'){
			$field = 'contents.cinema_release_date';
			$sort_type = 'DESC';
		}
		elseif($sort == 'imdb_score'){
			$field = 'contents.imdb_score';
			$sort_type = 'DESC';
		}
		else{
			$field = 'contents.title';
			$sort_type = 'ASC';
		}
	}

	if(isset($_GET['sort']) && isset($_GET['genre'])){
		$sort = $_GET['sort'];
		if($sort == "alpha"){
			$field = 'contents.title';
			$sort_type = 'ASC';
		}
		elseif($sort == 'rate'){
			$field = 'contents.tmdb_score';
			$sort_type = 'DESC';
		}
		elseif($sort == 'year'){
			$field = 'contents.cinema_release_date';
			$sort_type = 'DESC';
		}
		elseif($sort == 'imdb_score'){
			$field = 'contents.imdb_score';
			$sort_type = 'DESC';
		}
		else{
			$field = 'contents.title';
			$sort_type = 'ASC';
		}
	}

	if(isset($_GET['rated'])){
		$rated = $_GET['rated'];
	}

	if(isset($_GET['genre'])){
		$genre = $_GET['genre'];
	}

	if(isset($_GET['minr'])){
		$minr = $_GET['minr'];
	}
	if(isset($_GET['maxr'])){
		$maxr = $_GET['maxr'];    
	}

	if(isset($_GET['miny'])){
		$miny = $_GET['miny']."-01-01";
	}
	if(isset($_GET['maxy'])){
		$maxy = $_GET['maxy']."-12-31";
	}

    // var_dump($keyword);
    // var_dump($genre);
    // var_dump($type);
    // var_dump($rated);
    // var_dump($langs);

    // die();
	$contents = DB::select("CALL ret_search('".$keyword."', ".$genre.", '".$type."','1968', '2020', '".$rated."', '".$langs."', 0, $limit, 0, '".$provider_id."')");
    // dd($contents);
	if (count($contents)>0){
		$response->contents = $contents;
		$message = "Data received";
	}



	$miny = explode('-',$miny)[0];
	$maxy = explode('-',$maxy)[0];


  // $contents = $contents->appends(\Input::except('page'));
	$response->message = $message;
	return response()->json($response);
	return view('khoj.home',compact('contents','page','keyword','type','miny','maxy','searchtype','langs','genre','rated','minr','maxr','tab'));

}
public function submitFeedback( Request $request){
	$data = $request->all();
	$newData = new Feedback;

	$newData->name = $request->name;
	$newData->email = $request->email;
	$newData->subject = $request->subject;
	$newData->message = $request->message;
	$newData->status = 'Active';

	$newData->save();
	$response = new stdClass;
	$response->status = 200;
	$response->message = "Data received";

	return response()->json($response);
}

public function getCastDetail(Request $request){
	$cast_id =$request->cast_id;
	$response = new stdClass;
	$response->status = 200;
	$response->message = "Data received";

	if ($cast_id == 0){
		$find_id = DB::table('content_casting')->where('name', str_replace('_', ' ', $keyword))->where('person_id', '!=', 0)->first();
		if ($find_id){
			$cast_id = $find_id->person_id;
		}
	}

    // $cast_id;
    // $cast = DB::table('content_casting')->where('person_id',$cast_id)->first();
    // $contents = DB::table('content_casting')->leftJoin('contents','contents.content_id','content_casting.content_id')->where('content_casting.person_id',$cast_id)->orderBy('content_casting.role','ASC')->orderBy('contents.click_count','DESC')->orderBy('contents.imdb_score','DESC')->orderBy('contents.original_release_year','DESC')->select('contents.*')->paginate($limit);
    // $type="all";

	$page = 0;
	$langs = 'ALL';
	$rated = "ALL";
	$sort = "";
	$field = "title";
	$sort_type = "ASC";
	$genre = 0;
	$minr=0;
	$maxr=10; 

	$tab= 'search';

	$miny = '0000-00-00';
	$maxy = date('y-m-d');

	// $maxy = $maxy->cinema_release_date;
  // $miny = DB::table('contents')->where('original_release_year','>','1900')->min('original_release_year');
  // $maxy = DB::table('contents')->max('original_release_year');

	if(isset($_GET['page'])){
		$page = ($_GET['page']-1)*$limit;
	}  

	$keyword = $request->search;
	if ($request->searchtype)
		$type = $request->searchtype;
	else
		$type = 'ALL';
	$searchtype = $type;


	if(isset($_GET['lang'])){  $langs = $_GET['lang'];  }

	if(isset($_GET['sort'])){
		$sort = $_GET['sort'];
		if($sort == "alpha"){
			$field = 'contents.title';
			$sort_type = 'ASC';
		}
		elseif($sort == 'rate'){
			$field = 'contents.tmdb_score';
			$sort_type = 'DESC';
		}
		elseif($sort == 'year'){
			$field = 'contents.cinema_release_date';
			$sort_type = 'DESC';
		}
		elseif($sort == 'imdb_score'){
			$field = 'contents.imdb_score';
			$sort_type = 'DESC';
		}
		else{
			$field = 'contents.title';
			$sort_type = 'ASC';
		}
	}

	if(isset($_GET['sort']) && isset($_GET['genre'])){
		$sort = $_GET['sort'];
		if($sort == "alpha"){
			$field = 'contents.title';
			$sort_type = 'ASC';
		}
		elseif($sort == 'rate'){
			$field = 'contents.tmdb_score';
			$sort_type = 'DESC';
		}
		elseif($sort == 'year'){
			$field = 'contents.cinema_release_date';
			$sort_type = 'DESC';
		}
		elseif($sort == 'imdb_score'){
			$field = 'contents.imdb_score';
			$sort_type = 'DESC';
		}
		else{
			$field = 'contents.title';
			$sort_type = 'ASC';
		}
	}

	if(isset($_GET['rated'])){
		$rated = $_GET['rated'];
	}

	if(isset($_GET['genre'])){
		$genre = $_GET['genre'];
	}

	if(isset($_GET['minr'])){
		$minr = $_GET['minr'];
	}
	if(isset($_GET['maxr'])){
		$maxr = $_GET['maxr'];    
	}

	if(isset($_GET['miny'])){
		$miny = $_GET['miny']."-01-01";
	}
	if(isset($_GET['maxy'])){
		$maxy = $_GET['maxy']."-12-31";
	}

    // var_dump($keyword);
    // var_dump($genre);
    // var_dump($type);
    // var_dump($rated);
    // var_dump($langs);
    // die();
	$provider_id = 0;
	if (isset($request->provider)){
		$provider_id = explode('_', $request->provider)[1];
	}
	$searchcrit = "CALL ret_search('".$keyword."', ".$genre.", '".$type."','1968', '2020', '".$rated."', '".$langs."', $page, $limit, $cast_id, '".$provider_id."')";
	$contents = DB::select($searchcrit);
    // dd($contents);


	$miny = explode('-',$miny)[0];
	$maxy = explode('-',$maxy)[0];
	$response->contents = $contents;
	return response()->json($response);
	return view('khoj.home',compact('contents','page','keyword','type','miny','maxy','searchtype','langs','genre','rated','minr','maxr','tab'));
    // return view('khoj.cast_movies',compact('cast','contents','type'));
}

}
