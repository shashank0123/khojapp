<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\SearchKeyword;
use DB;

class MovieSync extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'khoj:moviesync';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
	   \Log::info('Cron Job Started');
        $keywords = SearchKeyword::where('exec_status', 0)->orderBy('created_at', 'desc')->get();
        
        foreach ($keywords as $key => $value) {
            $keywords1 = SearchKeyword::where('id', $value->id)->update(['exec_status' => '1']);
            \Log::info($value->keyword.'Cron Job Started');
            try {
            $this->getmxplayerdata($value->keyword);
                
            } catch (Exception $e) {
                
            }
            try {
            $this->getairteldata($value->keyword);
                
            } catch (Exception $e) {
                
            }
            try {
            $this->getvodafonedata($value->keyword);
                
            } catch (Exception $e) {
                
            }
            try {
            $this->getulludata($value->keyword);
                
            } catch (Exception $e) {
                
            }
            try {
            $this->gettmdbdata($value->keyword);
                
            } catch (Exception $e) {
                
            }
            
        }
    }

    public function getmxplayerdata($keyword)
    {
        $keyword = $keyword;
        $dataurl = "https://api.mxplay.com/v1/web/search/result?query=".$keyword."&platform=com.mxplay.desktop&content-languages=hi,en";
        $keyword = str_replace(' ', '%20', $keyword);
        if (filter_var($dataurl, FILTER_VALIDATE_URL)) 
            $data = file_get_contents($dataurl);
        else
            return 0;
        $data = json_decode($data);
        foreach ($data->sections as $key => $value) {
            if ($value->id == 'movie'){
                $dataadd = $this->addmxplayerdata($value, 'movie');
            }
            elseif ($value->id == 'shows'){
                $dataadd = $this->addmxplayerdata($value, 'show');
            }
            else{
                continue;
            }
        }
        // dd($data);
    }


    public function gettmdbdata($keyword)
    {
        // return 1;
        $keyword = $keyword;
        $url = "https://api.themoviedb.org/3/search/tv?api_key=f0dc06cd59a97bbddcebd05e902559e0&language=en-US&sort_by=popularity.desc&include_adult=false&query=$keyword&page=1";
        // $justwatch_urlnew = "https://apis.justwatch.com/content/titles/show/11188/locale/en_IN";

            $genrerequest = curl_init();
            curl_setopt($genrerequest,CURLOPT_RETURNTRANSFER,1);
            curl_setopt($genrerequest, CURLOPT_URL, $url);
            curl_setopt($genrerequest, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($genrerequest, CURLOPT_SSL_VERIFYPEER, false);
            $data = curl_exec($genrerequest);

            $data = json_decode($data);
            if (isset($data->results))
        foreach ($data->results as $key => $value) {
            $dataadd = $this->addtmdbdata($value, 'show');            
        }

        $url = "https://api.themoviedb.org/3/search/movie?api_key=f0dc06cd59a97bbddcebd05e902559e0&language=en-US&sort_by=popularity.desc&include_adult=false&query=$keyword&page=1";
        // $justwatch_urlnew = "https://apis.justwatch.com/content/titles/show/11188/locale/en_IN";

            $genrerequest = curl_init();
            curl_setopt($genrerequest,CURLOPT_RETURNTRANSFER,1);
            curl_setopt($genrerequest, CURLOPT_URL, $url);
            curl_setopt($genrerequest, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($genrerequest, CURLOPT_SSL_VERIFYPEER, false);
            $data = curl_exec($genrerequest);

            $data = json_decode($data);
            
            if (isset($data->results))
        foreach ($data->results as $key => $value) {
            $dataadd = $this->addtmdbdata($value, 'movie');            
        }
        // dd($data);
    }


    public function addtmdbdata($movie, $contenttype)
    {
        if ($movie->id == 469299){
            return 0;
        }
        if ($contenttype == 'movie'){
            print_r($movie);
            $releaseyear =explode('-', $movie->release_date)[0];
            $releasedate = $movie->release_date;
            $db_check = DB::table('contents')->where('title', $movie->title)->where('original_release_year',  explode('-', $movie->release_date)[0])->first();
            }
            elseif ($contenttype == 'show'){
                // if (isset($movie->original_title))
                $movie->title = $movie->original_name ?? '';

                $movie->original_title = $movie->original_name ?? '';
                // dd($movie);
                if (isset($movie->first_air_date)){
                    $releaseyear =explode('-', $movie->first_air_date)[0];
                    $releasedate = $movie->first_air_date;
                }
                else {
                    $releaseyear = "";
                    $releasedate = "";
                }
            $db_check = DB::table('contents')->where('title', $movie->title)->where('original_release_year',  $releaseyear)->first();
            }
            else return 0;
            if (!$db_check){
                $movie->poster_path = $this->storetmdbimage($movie->poster_path);
                $db_data = DB::table('contents')->insert(
                        array(
                            'title' => $movie->title ?? '',
                            'original_title' => $movie->original_title ?? '',
                            'content_type' => $contenttype ?? '',
                            'short_description' => $movie->overview ?? '',
                            'poster' => $movie->poster_path ?? '',
                            'content_source' => 'tmdb',
                            'content_source_id' => $movie->id,
                            'full_path' => $movie->full_path ?? "",
                            'tmdb_popularity' => $movie->popularity ?? '',
                            'tmdb_score' => 0,
                            'original_release_year' => $releaseyear ?? '',
                            'cinema_release_date' => $releasedate ?? '',
                            'original_language' => $movie->original_language ?? '',
                            'tmdb_score' => $movie->popularity ?? '',
                        ));
            }
        
    }


    public function storetmdbimage($path)
{
    if(!empty($path)){
        // $key1 = explode('/', $newdata->poster)[2];
        $url1 = 'https://image.tmdb.org/t/p/w500'.$path;
        $url2 = 'https://image.tmdb.org/t/p/w600_and_h900_bestv2'.$path;

        $base_path = public_path();
        $keyname = $path;
        $img1 = $base_path.'/poster'.$keyname;
        $img2 = $base_path.'/thumbnail'.$keyname;
        // echo $url1;
        // die();
        file_put_contents($img1, file_get_contents($url1));
        file_put_contents($img2, file_get_contents($url2));
        // $updatedata['poster'] = $keyname;
        return $path;
        
    }
}


function url_exists($url) {
    return curl_init($url) !== false;
}

    public function addmxplayerdata($movies, $contenttype)
    {
        if (count($movies->items)>0){
            foreach ($movies->items as $key1 => $value1) {
                // dd($value1);
                if (isset($value1->image)){                 
                    foreach ($value1->image as $key => $image) {
                        if ($key == '2x3'){
                            $image = explode('/', $image);
                            $url1 = 'https://is-1.mxplay.com/media/images/'.$image[3].'/2x3/2x/'.$image[4];
                            $base_path = public_path();
                            $keyname = time().$image[4];
                            $img1 = $base_path.'/poster/'.$keyname;
                            $updateData['poster'] = $keyname;
                            \Log::info($url1);
                            echo $url1;
                            if (url_exists($url1))
                                file_put_contents($img1, file_get_contents($url1));
                        // Save image 
                        }                       
                    }           

                }
                // echo $value1->title;
                $dbcheck = DB::table('contents')->where('content_source_id', $value1->id)->first();
                if (!$dbcheck){
                    $db_data1 = DB::table('contents')->insert(
                        array(
                            'title' => $value1->title,
                            'original_title' => $value1->title,
                            'content_type' => $contenttype,
                            'poster' => $updateData['poster'],
                            'content_source' => 'mxplayer',
                            'content_source_id' => $value1->id,
                            'full_path' => $movies->webUrl ?? "",
                            'tmdb_popularity' => 0 ?? '',
                            'original_release_year' => explode('-', $value1->releaseDate)[0] ?? '',
                            'cinema_release_date' => $value1->releaseDate ?? '',
                            'original_language' => $value1->language[0] ?? "",
                            'tmdb_score' => $tmdb_score ?? "",
                            'imdb_score' => $imdb_score ?? '',
                            'parent_id' => 0
                        ));
                    $dbcheck = DB::table('contents')->where('content_source_id', $value1->id)->first();
                    if ($dbcheck)
                        $db_data = DB::table('content_providers')->insert(
                                    array(
                                        'content_id' => $dbcheck->content_id,
                                        'provider_id' => 700 ,
                                        'video_quality' => 'SD',
                                        'currency' => 'INR',
                                        'monetization_type' => 'ads',
                                        'retail_price' => '0',
                                        'web_url' => ""
                                    ));
                }
                // dd($db_data1);
            }
        }
    }

    
    

    public function updatecastid(){
        $cast = DB::table('content_casting')->where('exec_status', 0)->first();
        if ($cast)
            // foreach ($cast as $key => $value) {
            $check = $this->getcastbyname($cast->name);
                # code...
            // }
    }
    public function getcastbyname($name)
    {
        $original = $name;

        echo $original;
        $name = str_replace(' ', '%20', $name);
        $url = "https://api.themoviedb.org/3/search/person?api_key=f0dc06cd59a97bbddcebd05e902559e0&language=en-US&query=".$name."&page=1&include_adult=false";
        $c = curl_init();
        curl_setopt($c,CURLOPT_RETURNTRANSFER,1);
        curl_setopt($c, CURLOPT_URL, $url);
        curl_setopt($c, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($c, CURLOPT_SSL_VERIFYPEER, 0);

        $contents = curl_exec($c);

        $contents = json_decode($contents);
        // dd($contents);
                $updatedata = array();
                $updatedata['exec_status'] = 1;
        if (isset($contents->results)){
            $data = $contents->results;
            foreach ($data as $key => $value) {
                // dd($name);
                    
                $updatedata['tmdb_id'] = $value->id;
                if ($value->profile_path != null){
                    $updatedata['poster'] = 'https://image.tmdb.org/t/p/original'.$value->profile_path;
                    // dd($value);
                }
                if ($value->name == $original){
                    echo $value->id;
                    $dbupdate = DB::table('content_casting')
                    ->where('name', $original)
                    ->update($updatedata);
                    return 0;
                    // dd($value);
                }
            }
        }
        $dbupdate = DB::table('content_casting')
                    ->where('name', $original)
                    ->update($updatedata);
    }


    public function addmxprovider($keyword)
    {
        $dbcheck = DB::table('contents')->where('content_source', 'mxplayer')->get();
        foreach ($dbcheck as $key => $value) {
            $db_data = DB::table('content_providers')->insert(
                array(
                    'content_id' => $value->content_id,
                    'provider_id' => 700 ,
                    'video_quality' => 'SD',
                    'currency' => 'INR',
                    'monetization_type' => 'ads',
                    'retail_price' => '0',
                    'web_url' => ""
                ));
        }
    }


    public function getulludata($keyword)
    {
        $keyword = $keyword;
        $keyword = str_replace(' ', '%20', $keyword);
        $data = file_get_contents("https://ullu.app/ulluCore/api/v1/media/v2/client/search?titleText=".$keyword."&page=0&size=40");
        $data = json_decode($data);
        foreach ($data->content as $key => $value) {        
            $dataadd = $this->addulludata($value, 'show'); 
        }
    }

    public function addulludata($movies, $contenttype)
    {
        if (isset($movies->portraitPosterId)){
            
            $url1 = 'https://d1dfc9w6nzu9oi.cloudfront.net/'.$movies->portraitPosterId;
            $url2 = 'https://d1dfc9w6nzu9oi.cloudfront.net/'.$movies->landscapePosterId;
            $base_path = public_path();
            $keyname = $movies->portraitPosterId;
            $img2 = $base_path.'/thumbnail/'.$movies->portraitPosterId;
            $img1 = $base_path.'/poster/'.$movies->portraitPosterId;
            $updateData['poster'] = $keyname;
            // Save image 
            if (filter_var($url1, FILTER_VALIDATE_URL)) 
                file_put_contents($img1, file_get_contents($url1));
            if (filter_var($url2, FILTER_VALIDATE_URL)) 
                file_put_contents($img2, file_get_contents($url2));

            // }
        }
        $dbcheck = DB::table('contents')->where('content_source', 'ullu')->where('content_source_id', $movies->id)->first();
        if (!$dbcheck){
            $db_data1 = DB::table('contents')->insert(
                array(
                    'title' => $movies->title,
                    'original_title' => $movies->title,
                    'content_type' => $contenttype,
                    'poster' => $updateData['poster'],
                    'content_source' => 'ullu',
                    'content_source_id' => $movies->id,
                    'full_path' => $movies->titleYearSlug ?? "",
                    'tmdb_popularity' => 0 ,
                    'original_release_year' => '',
                    'cinema_release_date' => date('Y-m-d'),
                    'original_language' => "",
                    'tmdb_score' => "",
                    'imdb_score' => '',
                    'parent_id' => 0
                )
            );
            $dbcheck = DB::table('contents')->where('title', $movies->title)->where('content_source', 'ullu')->where('content_type', $contenttype)->where('content_source_id', $movies->id)->first();
                    if ($dbcheck)
                        $db_data = DB::table('content_providers')->insert(
                                    array(
                                        'content_id' => $dbcheck->content_id,
                                        'provider_id' => 701 ,
                                        'video_quality' => 'SD',
                                        'currency' => 'INR',
                                        'monetization_type' => 'ads',
                                        'retail_price' => '0',
                                        'web_url' => ""
                                    ));
        }
    }

    public function getairteldata($keyword)
    {
        $keyword = $keyword;
        $keyword = str_replace(' ', '%20', $keyword);
        $url = "https://search.airtel.tv/app/v3/search/atv/query?appId=WEB&bn=18&count=200&dt=BROWSER&ln=hi&offSet=0&os=WEBOS&q=".$keyword;
        $genrerequest = curl_init();
        curl_setopt($genrerequest,CURLOPT_RETURNTRANSFER,1);
        curl_setopt($genrerequest, CURLOPT_URL, $url);
        curl_setopt($genrerequest, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($genrerequest, CURLOPT_SSL_VERIFYPEER, false);
        $contents = curl_exec($genrerequest);
        $count = 0;
        $contents = json_decode($contents);
        if (isset($contents->categories)){
            foreach ($contents->categories as $key => $categories) {
                if ($categories->type == 'LIVETVCHANNEL'){
                    continue;
                }
                elseif ($categories->type == 'MOVIE'){
                    $data = $categories->contentResponseList;
                    echo count($data);
                    foreach ($data as $key => $value) {
                        echo $value->title;
                        if (isset($value->releaseYear) && $value->releaseYear > 0)
                            $return1 = $this->addairtelmovie($value, 'movie');
                        // dd($value);
                        $count++;
                    }
                }
                else {
                    // dd($categories);
                }
            }
        }
        // dd($contents->categories);
        return $count;
    }

    public function addairtelmovie($content, $content_type)
    {
        $moviepath = "https://www.airtelxstream.in/movies/".strtolower(str_replace(' ', '-', $content->title))."/".$content->id;
        $DBcheck = DB::table('contents')->where('title', $content->title)->where('original_release_year', $content->releaseYear)->where('content_type', $content_type)->first();
        if ($DBcheck){
            $pro_check =  DB::table('content_providers')->where('content_id', $DBcheck->content_id)->where('provider_id', 702)->first();
            if (!$pro_check)
                $db_data = DB::table('content_providers')->insert(array(
                                        'content_id' => $DBcheck->content_id,
                                        'provider_id' => 702 ,
                                        'video_quality' => 'SD',
                                        'currency' => 'INR',
                                        'monetization_type' => 'flatrate',
                                        'retail_price' => '0',
                                        'web_url' => $moviepath
                                    ));

        }
        else{
            // dd($content);
            $movie = array();
            $movie['title'] = $content->title;
            $movie['original_title'] = $content->title;
            $movie['content_type'] = $content_type;
            $movie['content_source'] = 'airtelxtream';
            $movie['content_source_id'] = $content->id;
            $movie['original_release_year'] = $content->releaseYear;
            $movie['poster'] = $content->images->PORTRAIT;

            $url1 = $movie['poster'];
            $base_path = public_path();
            $keyname = $content->id.'.jpg';
            $img1 = $base_path.'/poster/'.$keyname;
            $movie['poster'] = $keyname;
            
            $genrerequest = curl_init();
            curl_setopt($genrerequest,CURLOPT_RETURNTRANSFER,1);
            curl_setopt($genrerequest, CURLOPT_URL, $url1);
            curl_setopt($genrerequest, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($genrerequest, CURLOPT_SSL_VERIFYPEER, false);
            $contents = curl_exec($genrerequest);

            
            if (!is_string($contents) && $img1)
                file_put_contents($img1, file_get_contents($contents));
            $movie['runtime'] = $content->duration;
            $movie['full_path'] = $moviepath;
            $movie['original_language'] = implode(',', $content->languages);
            $db_data1 = DB::table('contents')->insert($movie);
            $dbcheck = DB::table('contents')->where('title', $content->title)->where('content_source', 'airtelxtream')->where('content_type', $content_type)->where('content_source_id', $content->id)->first();
            
            $db_data = DB::table('content_providers')->insert(array(
                                        'content_id' => $dbcheck->content_id,
                                        'provider_id' => 702 ,
                                        'video_quality' => 'SD',
                                        'currency' => 'INR',
                                        'monetization_type' => 'flatrate',
                                        'retail_price' => '0',
                                        'web_url' => $moviepath
                                    ));
        }
        return 1;
    }


    public function getvodafonedata($keyword)
    {
        $keyword = $keyword;
        $keyword = str_replace(' ', '%20', $keyword);
        $url = "https://api.vodafoneplay.in/content/v7/search/?fields=generalInfo,contents,images&&level=&startIndex=1&count=1000&orderBy=releaseDate&orderMode=1&type=movie%2Cvodchannel%2Ctvshow%2Ctvseries&query=".$keyword;
        $genrerequest = curl_init();
        curl_setopt($genrerequest,CURLOPT_RETURNTRANSFER,1);
        curl_setopt($genrerequest, CURLOPT_URL, $url);
        curl_setopt($genrerequest, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($genrerequest, CURLOPT_SSL_VERIFYPEER, false);
        $contents = curl_exec($genrerequest);
        $count = 0;
        $contents = json_decode($contents);
        // if ($contents->results)
        //  $contents = $contents->results;
        
        if (isset($contents->results)){
            foreach ($contents->results as $key => $categories) {
                if ($categories->generalInfo->type == 'tvseries'){
                    continue;
                }
                elseif ($categories->generalInfo->type == 'movie'){
                    // dd($categories);
                    $basic = $categories->generalInfo;
                    $dates = $categories->content;
                    $images = $categories->images;
                                    
                    $return1 = $this->addvodafonemovie($basic, $dates, 'movie', $images);
                    
                }
                else {
                    // dd($categories);
                    // dd($categories);
                }
            }
        }
        // dd($contents->categories);
        return $count;
    }

     public function storevodefoneimage($path)
    {
        try {
            
            foreach ($path as $key => $value) {
                if ($value->type == 'portraitcoverposter'){
                    $url1 = $value->link;
                    $url2= $value->link;
                    $temp = explode('/', $value->link);
                    $keyname = $temp[count($temp)-1];
                }
            }
            if ($url1){
                $base_path = public_path();
                $img1 = $base_path.'/poster/'.$keyname;
                $img2 = $base_path.'/thumbnail/'.$keyname;
                // echo $url1;
                // die();
                // $img1;
                $check1 = file_put_contents($img1, file_get_contents($url1));
                $check2 = file_put_contents($img2, file_get_contents($url2));
                // $updatedata['poster'] = $keyname;
                return $keyname;
            }
            
        } catch (Exception $e) {
            return "";    
        }
    }

    public function addvodafonemovie($basic, $dates, $content_type, $images = false)
    {
        if (isset($dates->releaseDate))
            $releaseyear = explode('-', $dates->releaseDate)[0];
        else
            $releaseyear = "";
        $moviepath = "https://www.vodafoneplay.in/".$content_type."s/detail/".$basic->_id."/".strtolower(str_replace(' ', '-', $basic->title));
        // echo $releaseyear;
        if (isset($images->values))
        $path = $this->storevodefoneimage($images->values);
        else $path = "";
        $DBcheck = DB::table('contents')->where('title', $basic->title)->where('original_release_year', $releaseyear)->where('content_type', $content_type)->first();
        
        if ($DBcheck){
            $pro_check =  DB::table('content_providers')->where('content_id', $DBcheck->content_id)->where('provider_id', 703)->first();
            if (!$pro_check)
                $db_data = DB::table('content_providers')->insert(array(
                                        'content_id' => $DBcheck->content_id,
                                        'provider_id' => 703 ,
                                        'video_quality' => 'SD',
                                        'currency' => 'INR',
                                        'monetization_type' => 'flatrate',
                                        'retail_price' => '0',
                                        'web_url' => $moviepath
                                    ));

        }
        else{
            // dd($dates);
            $movie = array();
            $movie['title'] = $basic->title;
            $movie['original_title'] = $basic->title;
            $movie['content_type'] = $content_type;
            $movie['content_source'] = 'vodafoneplay';
            $movie['content_source_id'] = $basic->_id;
            $movie['original_release_year'] = $releaseyear;
            $movie['poster'] = $path;
            $movie['short_description'] = $basic->briefDescription;
            
            if (isset($dates->duration)){
                $time = explode(':', $dates->duration);
                $runtime = ($time[0]*60)+($time[1]*1)+($time[2]/60);
            }
            else
                $runtime = "";
            $movie['runtime'] = $runtime;
            $movie['full_path'] = $moviepath;
            if (isset($dates->language))
                $movie['original_language'] = implode(',', $dates->language);
            $dbcheck = DB::table('contents')->where('content_source_id', $basic->_id)->where('content_type', $content_type)->first();
            if (!$dbcheck){
                $db_data1 = DB::table('contents')->insert($movie);
                $dbcheck = DB::table('contents')->where('title', $basic->title)->where('content_source', 'vodafoneplay')->where('content_type', $content_type)->where('content_source_id', $basic->_id)->first();
            }
            
             $pro_check =  DB::table('content_providers')->where('content_id', $dbcheck->content_id)->where('provider_id', 703)->first();
            if (!$pro_check)
                $db_data = DB::table('content_providers')->insert(array(
                                        'content_id' => $dbcheck->content_id,
                                        'provider_id' => 703 ,
                                        'video_quality' => 'SD',
                                        'currency' => 'INR',
                                        'monetization_type' => 'flatrate',
                                        'retail_price' => '0',
                                        'web_url' => $moviepath
                                    ));
        }
        return 1;
    }


    public function gettvfdata($keyword)
    {
        $keyword = $keyword;
        $keyword = str_replace(' ', '%20', $keyword);
        $url = "https://webapi-services.tvfplay.com/v2/api/v2/search/w/autocomplete/".$keyword;
        $genrerequest = curl_init();
        curl_setopt($genrerequest,CURLOPT_RETURNTRANSFER,1);
        curl_setopt($genrerequest, CURLOPT_URL, $url);
        curl_setopt($genrerequest, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($genrerequest, CURLOPT_SSL_VERIFYPEER, false);
        $contents = curl_exec($genrerequest);
        $count = 0;
        $contents = json_decode($contents);
        // if ($contents->results)
        //  $contents = $contents->results;
        dd($contents);
        if (isset($contents->results)){
            foreach ($contents->results as $key => $categories) {
                if ($categories->generalInfo->type == 'tvseries'){
                    continue;
                }
                elseif ($categories->generalInfo->type == 'movie'){
                    // dd($categories);
                    $basic = $categories->generalInfo;
                    $dates = $categories->content;
                                    
                    $return1 = $this->addtvfmovie($basic, $dates, 'movie');
                    
                }
                else {
                    // dd($categories);
                    // dd($categories);
                }
            }
        }
        // dd($contents->categories);
        return $count;
    }

    public function addtvfmovie($basic, $dates, $content_type)
    {
        $releaseyear = explode('-', $dates->releaseDate)[0];
        echo $moviepath = "https://www.vodafoneplay.in/".$content_type."s/detail/".$basic->_id."/".strtolower(str_replace(' ', '-', $basic->title));
        echo $releaseyear;
        $DBcheck = DB::table('contents')->where('title', $basic->title)->where('original_release_year', $releaseyear)->where('content_type', $content_type)->first();
        
        if ($DBcheck){
            $pro_check =  DB::table('content_providers')->where('content_id', $DBcheck->content_id)->where('provider_id', 703)->first();
            if (!$pro_check)
                $db_data = DB::table('content_providers')->insert(array(
                                        'content_id' => $DBcheck->content_id,
                                        'provider_id' => 703 ,
                                        'video_quality' => 'SD',
                                        'currency' => 'INR',
                                        'monetization_type' => 'flatrate',
                                        'retail_price' => '0',
                                        'web_url' => $moviepath
                                    ));

        }
        else{
            // dd($dates);
            $movie = array();
            $movie['title'] = $basic->title;
            $movie['original_title'] = $basic->title;
            $movie['content_type'] = $content_type;
            $movie['content_source'] = 'vodafoneplay';
            $movie['content_source_id'] = $basic->_id;
            $movie['original_release_year'] = $releaseyear;
            $movie['short_description'] = $basic->briefDescription;
            
            $time = explode(':', $dates->duration);
            $runtime = ($time[0]*60)+($time[1]*1)+($time[2]/60);
            $movie['runtime'] = $runtime;
            $movie['full_path'] = $moviepath;
            $movie['original_language'] = implode(',', $dates->language);
            
            $db_data1 = DB::table('contents')->insert($movie);
            $dbcheck = DB::table('contents')->where('title', $basic->title)->where('content_source', 'vodafoneplay')->where('content_type', $content_type)->where('content_source_id', $basic->_id)->first();
            
             $pro_check =  DB::table('content_providers')->where('content_id', $DBcheck->content_id)->where('provider_id', 703)->first();
            if (!$pro_check)
                $db_data = DB::table('content_providers')->insert(array(
                                        'content_id' => $dbcheck->content_id,
                                        'provider_id' => 703 ,
                                        'video_quality' => 'SD',
                                        'currency' => 'INR',
                                        'monetization_type' => 'flatrate',
                                        'retail_price' => '0',
                                        'web_url' => $moviepath
                                    ));
        }
        return 1;
    }
}
