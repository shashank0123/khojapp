<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class MovieUpdate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'movie:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
         $season = \DB::table('contents')->where('content_type','!=' ,'movie')->limit(30)->get();
        foreach ($season as $key => $seasons) {
            
        if ($seasons){
            $seasons->original_title = urlencode($seasons->original_title);
            $url = "https://api.themoviedb.org/3/search/$seasons->content_type?api_key=f0dc06cd59a97bbddcebd05e902559e0&language=en-US&sort_by=popularity.desc&query=$seasons->original_title&page=1&year=$seasons->original_release_year";
            \Log::info($url);
            $c = curl_init();
            curl_setopt($c,CURLOPT_RETURNTRANSFER,1);
            curl_setopt($c, CURLOPT_URL, $url);
            curl_setopt($c, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($c, CURLOPT_SSL_VERIFYPEER, 0);
            $contents = curl_exec($c);
            $contents = json_decode($contents);
            if(!empty($contents)){
                 // $total_pages = $contents->total_pages;
                if (isset($contents->results)){
                 $matches = count($contents->results);
                 if ($matches == 1){
                    $data = $contents->results[0];
                    $seasons1 = DB::table('contents')->where('content_id', $seasons->content_id)->update(['cinema_release_date' => $data->release_date]);
                    
                 }
                 else
                    $seasons1 = DB::table('contents')->where('content_id', $seasons->content_id)->update(['cinema_release_date' => '1711-12-12']);
            }
            }
        }
        }
    }
}
