<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;
use Socialite;
use Illuminate\Http\Request;
use App\Notification;
use App\User;
use App\Profile;
use App\Recommendation;
use App\WatchList;
use App\Playlist;
use App\Favourite;
use App\Country;
use App\Rating;
use Input;
use StdClass;
use Hash;
use Mail;
use Sentinel;
use Reminder;

class UserController extends Controller
{
	    use AuthenticatesUsers;

	public function profile(Request $request)
	{
		$notifications = Notification::where('user_id')->get();
		$count = 0;
		$moviewatched = 0;
		$serieswatched = 0;
		$docwatched = 0;
		$countries = Country::all();
		if ($notifications){
			$count = count($notifications);
		}

		$recommended = Recommendation::join('contents', 'contents.content_id', '=', 'recommendations.movie_id')->where('recommendations.user_id', auth()->user()->id)->get();
		$user = User::find(auth()->user()->id);
		$profile = Profile::where('user_id', auth()->user()->id)->first();
		
		if (!$profile){
			$profile = new Profile;
			$profile->user_id = auth()->user()->id;
			$profile->save();
			$genres = array();
		}
		else{
			$genres = $profile->genres;
			if (strlen($genres)>0){
				$genres = explode(',', $genres);
			}
		}

		$type='Movie';
		
		return view('user-profile', compact('count', 'profile', 'user', 'moviewatched', 'serieswatched','docwatched', 'recommended', 'genres', 'countries' , 'type'));
	}


	public function getResetPassword(Request $request)
	{
		$user = User::find(auth()->user()->id);
		$profile = Profile::where('user_id', auth()->user()->id)->orderBy('created_at','DESC')->first();

		$type='Movie';
		return view('frontnew.password', compact('user', 'profile' , 'type'));
	}


	public function account(Request $request)
	{
		$user = User::find(auth()->user()->id);
		$profile = Profile::where('user_id', auth()->user()->id)->orderBy('created_at','DESC')->first();

		return view('user_account', compact('user', 'profile'));
	}

	// public function saveaccount(Request $request)
	// {
	// 	$user = User::find(auth()->user()->id);
	// 	$profile = Profile::where('user_id', auth()->user()->id)->first();
	// 	if (!$profile){
	// 		$profile = new Profile;
	// 		$profile->user_id = auth()->user()->id;
	// 		if ($request->hasfile('profile_pic')) {
	// 			$file=$request->file('profile_pic');
	// 			$file->move(public_path(). '/images/user', $file->getClientOriginalName());
	// 			$user->profile_pic=$file->getClientOriginalName();
	// 		}
	// 		$profile->country = $request->country;
	// 		$profile->genres = $request->genres;
	// 		$profile->save();
	// 	}
	// 	else{
	// 		$profile->country = $request->country;
	// 		if ($request->hasfile('profile_pic')) {
	// 			$file=$request->file('profile_pic');
	// 			$file->move(public_path(). '/images/user', $file->getClientOriginalName());
	// 			$profile->profile_pic=$file->getClientOriginalName();
	// 		}
	// 		$profile->update();

	// 	}
	// 	$user->name = $request->name;
	// 	$user->update();

	// 	return redirect('account');


	// }

	public function saveRating(Request $request)
	{
		$id = auth()->user()->id;
		$movie_id = $request->movie_id;
		$rating = $request->rating;

		$ratings = Rating::where('user_id', $id)->where('movie_id', $movie_id)->first();
		if ($ratings){
			$ratings ->rating = $rating;
			$ratings->update();
		}
		else
		{
			$ratings = new Rating;
			$ratings->movie_id = $movie_id;
			$ratings->user_id = $id;
			$ratings->rating = $rating;
			$ratings->save();
		}
		echo 'success;';
	}


	public function savefavourite(Request $request, $type, $id)
	{		
		$message = 'Something Went Wrong';
		if (isset(auth()->user()->id)){
			$favourite = Favourite::where('movie_id', $id)->where('user_id', auth()->user()->id)->first();
			if ($favourite){
				// return redirect('/movie_detail/'.$id);
				$message = "Already in your favourite list";
				return redirect()->back()->with('message',$message);

			}
			else{
				$favourite = new Favourite;
				$favourite->movie_id = $id;
				$favourite->type = $type;
				$favourite->user_id = auth()->user()->id;
				$favourite->save();
				$message = "Added to favourite list successfully";
				return redirect()->back()->with('message',$message);
				// return redirect('movie_detail/'.$id);
			}
		}
		else
			return redirect('login');
	}

	public function savewatchlist(Request $request, $type, $id)
	{
		$message = 'Something Went Wrong';
		if (isset(auth()->user()->id)){
			$favourite = WatchList::where('movie_id', $id)->where('user_id', auth()->user()->id)->first();
			if ($favourite){
				$message = "Already in your WatchList list";
				return redirect()->back()->with('message',$message);
			}
			else{
				$favourite = new WatchList;
				$favourite->movie_id = $id;
				$favourite->user_id = auth()->user()->id;
				$favourite->type = $type;
				$favourite->save();
				$message = "Added to WatchList list successfully";
				return redirect()->back()->with('message',$message);
			}
		}
		else
			return redirect('login');
	}


	public function saveplaylist(Request $request, $type, $id)
	{
		$message = 'Something Went Wrong';
		if (isset(auth()->user()->id)){
			$favourite = Playlist::where('movie_id', $id)->where('user_id', auth()->user()->id)->first();
			if ($favourite){
				$message = "Already in your Playlist list";
				return redirect()->back()->with('message',$message);
				// return redirect('/movie_detail/'.$id);

			}
			else{
				$favourite = new Playlist;
				$favourite->movie_id = $id;
				$favourite->type = $type;
				$favourite->user_id = auth()->user()->id;
				$favourite->save();
				$message = "Added to Playlist list successfully";
				return redirect()->back()->with('message',$message);				
			}
		}
		else
			return redirect('login');
	}

	public function passwordUpdate(Request $request,$id){
		$message = 'Something Went Wrong';
		$user = User::where('id',$id)->first();

		if($request->new_password == $request->confirm_password){
			$user->password = Hash::make($request->new_password);
			$user->update();
			$message = 'Password Updated Successfully.';			
		}
		else{
			$message = "Password didn't match.";
		}

		return redirect()->back()->with('message',$message);		
	}


	public function uplaodProfileImage(Request $request)
	{
		$response = new StdClass;
		$message = "Something went wrong";
		$user_id = $request->member_id; 
		$profimage = $request->file ;   
		$member = User::find($user_id);   

		if (Input::file('file')) 
		{
			$file=Input::file('file');
			$file->move(public_path(). '/images/user/', time().$file->getClientOriginalName());           
			$member->profile_pic=time().$file->getClientOriginalName();  
			$member->update();
			$img = $member->profile_pic;
			$response->image  = $img ;
			$message = "Image Uploaded";
		}
		else
		{
			$message = "Request not completed";
		}
		return redirect()->back();
	}

	public function favourite(){
		$key = 'My Favourite List';
		$keyword = 'Favourite';
		$contents = Favourite::join('contents', 'contents.content_id', '=', 'favourites.movie_id')->where('favourites.user_id', auth()->user()->id)->get();
		


		$type='Movie';
		return view('frontnew.favourite',compact('contents','key','keyword','type'));
	}

	public function watchlist(){
		$key = 'My WatchList';
		$keyword = 'WatchList';
		$contents = WatchList::join('contents', 'contents.content_id', '=', 'watch_lists.movie_id')->where('watch_lists.user_id', auth()->user()->id)->get();

		$type='Movie';
		return view('frontnew.favourite',compact('contents','key','keyword','type'));
	}

	public function playlist(){
		$key = 'My Playlist';
		$keyword = 'PlayList';
		$contents = Playlist::join('contents', 'contents.content_id', '=', 'playlists.movie_id')->where('playlists.user_id', auth()->user()->id)->get();

		$type='Movie';
		return view('frontnew.favourite',compact('contents','key','keyword','type'));
	}

	public function deleteFavourite(Request $request){
		$msg = 'Somethong went wrong';
		$key = $request->key;

		if($key == 'Favourite')
			$favourte = Favourite::where('favourite_id',$request->id)->delete();

		if($key=='WatchList')
			$favourte = WatchList::where('watchlist_id',$request->id)->delete();

		if($key == 'PlayList')
			$favourte = Playlist::where('playlist_id',$request->id)->delete();


		return response()->json(['msg' => $msg]);
	}

	public function updateProfile (Request $request){

		$user =  User::find($request->uid);

		$profile = Profile::where('user_id',$user->id)->first();

		if(isset($profile)){
			$profile->gender = $request->gender;
			$profile->country = $request->country;
			$profile->contact = $request->contact;
			$profile->address = $request->address;
			$profile->update();
		}	

		return redirect()->back()->with('message','Profile updated successfully');
	}

	public function getForgetPassword(Request $request)
	{
		return view('frontnew.reset-password');
	}

	public function sendResetPasswordLink(Request $request)
	{
		
		$message = 'Something Went Wrong';
		$email = $request->email;

		$user = User::where('email', $email)->first();
		$result = Null;

		if(!empty($user)){
			$message = 'This Email is not registered with us';
		return response()->json(['message'=>$message]);
		}

		$user = Sentinel::findById($user->id);
		$reminder = Reminder::exists($user) ? : Reminder::create($user);

		$this->sendEmail($user, $reminder->code);

		$message = "Password Reset code is send to your mail.";
		return response()->json(['message'=>$message]);
	}


	public function sendEmail($user,$code){
		Mail::send(
			'auth.passwords.forgot',
			['user' => $user , 'code' => $code], 
			 function ($message) use ($user) {
			$message->to($user->email);
			$message->subject("$user->name, reset your password. ");
		});
	}

}
