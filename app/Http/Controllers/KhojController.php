<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Crew;
use App\Movie;
use App\Cast;
use App\WatchList;
use App\Playlist;
use App\Favourite;
use App\Country;
use App\Newsletter;
use App\Genre;
use App\Rating;
use App\SearchKeyword;
use DB,StdClass;

class KhojController extends Controller
{
	public function __construct()
  {
    $genre = Genre::all();
    session()->put('genre', $genre);
    $featureds = Movie::where('featured', 'yes')->get();
    foreach ($featureds as $key => $value) {
      $id = $value->movie_id;
      $favourite = 0;
      $watchlist = 0;
      $playlist = 0;
      if (isset(auth()->user()->id)){
        $favourite = Favourite::where('movie_id', $id)->where('user_id', auth()->user()->id)->first();
        if ($favourite){
          $featureds[$key]->favourite = 1;
        }
        $watchlist = WatchList::where('movie_id', $id)->where('user_id', auth()->user()->id)->first();
        if ($watchlist){
          $featureds[$key]->watchlist = 1;
        }

        $playlist = Playlist::where('movie_id', $id)->where('user_id', auth()->user()->id)->first();
        if ($playlist){
          $featureds[$key]->playlist = 1;
        }
      }
      $casts = Crew::join('casts', 'casts.id' , '=', 'crews.cast_id')->where('movie_id', $id)->where('job', '!=', 'Director')->get()->take(6);
      $featureds[$key]->crews = $casts;

    }
    session()->put('featureds', $featureds);
  }


  function getHome(){ 

    $total_pages = 0;
    $imdb_rating = [];
    $predict ="home";
    if (isset($request->page_id) && $request->page_id >1){
      $page = $request->page_id;
    }   
    else
      $page = 1;
    $url = "https://api.themoviedb.org/3/discover/movie?api_key=f0dc06cd59a97bbddcebd05e902559e0&language=en-US&sort_by=popularity.desc&include_adult=false&include_video=false&page=$page";
    $genre_url = "https://api.themoviedb.org/3/genre/movie/list?api_key=f0dc06cd59a97bbddcebd05e902559e0&language=en-US";
    $c = curl_init();
    curl_setopt($c,CURLOPT_RETURNTRANSFER,1);
    curl_setopt($c, CURLOPT_URL, $url);
    curl_setopt($c, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($c, CURLOPT_SSL_VERIFYPEER, false);
    $contents = curl_exec($c);

    $genrerequest = curl_init();
    curl_setopt($genrerequest,CURLOPT_RETURNTRANSFER,1);
    curl_setopt($genrerequest, CURLOPT_URL, $genre_url);
    curl_setopt($genrerequest, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($genrerequest, CURLOPT_SSL_VERIFYPEER, false);
    $genres = curl_exec($genrerequest);
    
    $contents = json_decode($contents);
    if (isset($contents)){
      $genres = json_decode($genres);
      $total_pages = $contents->total_pages;

      foreach($contents->results as $con){

        $url = "https://api.themoviedb.org/3/movie/$con->id?api_key=f0dc06cd59a97bbddcebd05e902559e0&language=en-US";

        $c = curl_init();
        curl_setopt($c,CURLOPT_RETURNTRANSFER,1);
        curl_setopt($c, CURLOPT_URL, $url);
        curl_setopt($c, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($c, CURLOPT_SSL_VERIFYPEER, false);
        $content = curl_exec($c);
        $content = json_decode($content);

        $movie_imdb = $content->imdb_id;

        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://movie-database-imdb-alternative.p.rapidapi.com/?i=$movie_imdb&r=json",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_SSL_VERIFYPEER => false,
          CURLOPT_SSL_VERIFYHOST => false,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_HTTPHEADER => array(
            "x-rapidapi-host: movie-database-imdb-alternative.p.rapidapi.com",
            "x-rapidapi-key: 49763b4616mshf6f6b0ff05d1434p132176jsn30326869077d"
          ),
        ));

        $response = curl_exec($curl);

        $response = json_decode($response);

        $err = curl_error($curl);

        if ($err) {
          echo "cURL Error #:" . $err;
        } else {
          if(isset($response->Response) && $response->Response == 'True'){
            if(isset($response->imdbRating)){
              $imdb_rating[] = $response->imdbRating;
            }
            else{
              $imdb_rating[] = 'N/A';
            }
          }
          else{
            $imdb_rating[] = 'N/A';
          }
        }
      }
    }
    
    if (isset($contents) && isset($genres))
      $genres = $genres->genres;
    else 
      $genres = array();
    $type = 'Movies';
    $side = 'All Movies';
    
    return view('frontnew.home', compact('contents', 'genres', 'type','total_pages','predict','side','imdb_rating'));
  }


  function getMovie(Request $request, $id){

    $favourite = 0;
    $watchlist = 0;
    $playlist = 0;
    $user_rating = 0;
    $contents = "";
    $related_movies = [];
    $gen_type = "";
    $imdb_rating = 0;
    $release_year = 0;
    $db_content = "";
    $path = "";
    $content_id = "";

    if (isset(auth()->user()->id)){
      $favourite = Favourite::where('movie_id', $id)->where('user_id', auth()->user()->id)->first();
      if ($favourite){
        $favourite = 1;
      }
      $watchlist = WatchList::where('movie_id', $id)->where('user_id', auth()->user()->id)->first();
      if ($watchlist){
        $watchlist = 1;
      }

      $playlist = Playlist::where('movie_id', $id)->where('user_id', auth()->user()->id)->first();
      if ($playlist){
        $playlist = 1;
      }
      $user_rating = Rating::where('user_id', auth()->user()->id)->where('movie_id', $id)->first();
    }
    $casts = Crew::join('casts', 'casts.id' , '=', 'crews.cast_id')->where('casts.movie_id', $id)->where('casts.type','movie')->where(function ($query) {
      $query->where('job', '!=', 'Director')->orWhere('job', '!=' , 'Story');
    })->distinct()->get();

    $crews = $casts;      

    $movie = Movie::where('movie_id', $id)->where('type','Movie')->first();
    
    if (!isset($movie)){
      $url = "https://api.themoviedb.org/3/movie/$id?api_key=f0dc06cd59a97bbddcebd05e902559e0&language=en-US";

      $c = curl_init();
      curl_setopt($c,CURLOPT_RETURNTRANSFER,1);
      curl_setopt($c, CURLOPT_URL, $url);
      curl_setopt($c, CURLOPT_SSL_VERIFYHOST, false);
      curl_setopt($c, CURLOPT_SSL_VERIFYPEER, false);
      $contents = curl_exec($c);
      $contents = json_decode($contents);

      $movie = new Movie;
      $movie->movie_id = $id; 
      $movie->name = $contents->original_title; 
      $movie->runtime = $contents->runtime; 
      $movie->release_date = $contents->release_date; 
      $movie->poster_path = $contents->poster_path; 
      $movie->genre_type = $gen_type; 
      $movie->vote_average = $contents->vote_average; 
      $movie->overview = $contents->overview; 
      $movie->type = 'Movie'; 

      $path = "/poster/".$movie->poster_path;

      $movie->save();

      $db_content = DB::table('contents')->where('title',$movie->name)->first();
    //If Not data available for this movie in contents table
      if(isset($db_content)){
        $content_id = $db_content->content_id;
      }
      else{

        $db_data = DB::table('contents')->insert(
          array(
            'title' => $movie->title,
            'content_type' => 'movie',
            'poster' => $path,
            'short_description' => $movie->overview,
            'original_release_year' => $release_year,
            'original_title' => $contents->original_title,
            'original_language' => $contents->original_language,
            'runtime' => $contents->runtime,
            'imdb_score' => $imdb_rating,
          ));

        $db_data = DB::table('contents')
        ->orderBy('content_id','DESC')
        ->limit(1)
        ->first();

        $content_id = $db_data->content_id; 

        $gene = [];
        $gene = $contents->genres;
        if(isset($gene)){
          foreach($gene as $g){
            $db_gen = DB::table('content_genres')->insert(
              array(
                'content_id' => $content_id,
                'genre_id' => $g->id
              )
            );
          }
        }

      }
    }
    $release_year = date('Y',strtotime($movie->release_date));
    $db_content = DB::table('contents')
    ->where('title',$movie->name)
    ->where('content_type','movie')
    ->first();

    if(isset($db_content)){
      $content_id = $db_content->content_id;
      $justwatch_id = $db_content->content_source_id;
    }
    else{
      $db_data = DB::table('contents')->insert(
        array(
          'title' => $movie->name,
          'content_type' => 'movie',
          'poster' => $path,
          'short_description' => $movie->overview,
          'original_release_year' => $release_year,
          'original_title' => $movie->name,
          'runtime' => $movie->runtime,
          'imdb_score' => 'N/A',
        ));
    }

    if (count($casts)<1){

       //Cast. Crews & Directors in Content_casting

      $db_casts = DB::table('content_casting')
      ->where('content_id',$content_id)
      ->get();

      $castlist = "https://api.themoviedb.org/3/movie/$id/credits?api_key=f0dc06cd59a97bbddcebd05e902559e0";

      $creditcurl = curl_init();
      curl_setopt($creditcurl,CURLOPT_RETURNTRANSFER,1);
      curl_setopt($creditcurl, CURLOPT_URL, $castlist);
      curl_setopt($creditcurl, CURLOPT_SSL_VERIFYHOST, false);
      curl_setopt($creditcurl, CURLOPT_SSL_VERIFYPEER, false);
      $castlist = curl_exec($creditcurl);
      $castlist = json_decode($castlist);

      if(isset($castlist)){
        if(isset($castlist->crew))
          $crews = $castlist->crew;
        if(isset($castlist->cast))
          $casts = $castlist->cast;}
        if(isset($casts)){
          foreach ($casts as $key => $value) {
            if(isset($value->character)){
              $character = $value->character;
            }
            else{
              $character = "";
            }

            if(isset($value->department)){
              $department = strtoupper($value->department);
            }
            else{
              $department = "";
            }

            $cast = Cast::where('movie_id', $id)->get();
            if (!$cast){
              $cast = new Cast;
              $cast->name = $value->name;
              $cast->id = $value->id;
              $cast->movie_id = $id;
              $cast->type = 'movie';
              $cast->profile_path = $value->profile_path;
              $cast->save();
            }

            if(!$db_casts){
              $add_cast = DB::table('content_casting')
              ->insert(array(
                'content_id' => $content_id,
                'person_id' => $value->id,
                'name' => $value->name,
                'character_name' => $character ,
                'role' => $department,

              ));
            }

            $crew = Crew::where('movie_id', $id)->get();
            if (!$crew){
              $crew = new Crew;
              $crew->movie_id = $id;
              $crew->cast_id = $cast->id;
              $crew->type = 'movie';
              $crew->job = $value->character;
              $crew->save();
            }
          }
        }

      }
      if (isset($crews)){
        $directors = array();
        $writers = array();

        foreach ($crews as $key => $value) {
          if ($value->job == 'Director'){
            $castcheck = Cast::where('name', $value->name)->first();
            if ($castcheck){
              $crewcheck = Crew::where('cast_id', $castcheck->id)->first();
              if (!$crewcheck){
                $crew = new Crew;
                $crew->movie_id = $id;
                $crew->cast_id = $castcheck->id;
                $crew->job = $value->job;
                $crew->save();
              }
            }
            else
            {
              $cast = new Cast;
              $cast->name = $value->name;
              $cast->id = $value->id;
              $cast->profile_path = $value->profile_path;
              $cast->save();
            }
            array_push($directors, $value->name);
          }
          if ($value->job == 'Story'){
            $castcheck = Cast::where('name', $value->name)->first();
            if ($castcheck){
              $crewcheck = Crew::where('cast_id', $castcheck->id)->first();
              if (!$crewcheck){
                $crew = new Crew;
                $crew->movie_id = $id;
                $crew->cast_id = $castcheck->id;
                $crew->job = $value->job;
                $crew->save();
              }
            }
            else
            {
              $cast = new Cast;
              $cast->name = $value->name;
              $cast->id = $value->id;
              $cast->profile_path = $value->profile_path;
              $cast->save();
            }
            array_push($writers, $value->name);
          }
        }
      }
      else{
        $directors = Crew::join('casts', 'casts.id' , '=' ,'crews.cast_id')
        ->select('casts.name')
        ->where('crews.movie_id', $id)
        ->where('crews.job', 'Director')
        ->distinct()->get();
        $writers = Crew::join('casts', 'casts.id' , '=' ,'crews.cast_id')
        ->select('casts.name')
        ->where('crews.movie_id', $id)
        ->where('crews.job', 'Story')
        ->distinct()->get();
      }

      $trailorurl = "https://api.themoviedb.org/3/movie/$id/videos?api_key=f0dc06cd59a97bbddcebd05e902559e0&language=en-US";
      $trailorcurl = curl_init();
      curl_setopt($trailorcurl,CURLOPT_RETURNTRANSFER,1);
      curl_setopt($trailorcurl, CURLOPT_URL, $trailorurl);
      curl_setopt($trailorcurl, CURLOPT_SSL_VERIFYHOST, false);
      curl_setopt($trailorcurl, CURLOPT_SSL_VERIFYPEER, false);
      $trailors = curl_exec($trailorcurl);
      $trailors = json_decode($trailors);
      // $trailorslist = array();
      if(isset($trailors)){
        if(isset($trailors->results))
          $trailorslist = $trailors->results;
      }

      
      // if (isset($trailors->results))
        // foreach ($trailors->results as $key => $value) {
        //   if ($value->type == "Trailer")
        //     array_push($trailorslist, $value->key);
        // }
      // echo urlencode($movie->name);
      // die;

    //     $handle = curl_init();
    //     $urlcani = 'http://www.canistream.it/services/search/?movieName=' . urlencode($movie->name);

    //     curl_setopt( $handle, CURLOPT_URL, $urlcani );
    //     curl_setopt( $handle, CURLOPT_USERAGENT, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.94 Safari/537.36' );
    //     curl_setopt($handle, CURLOPT_RETURNTRANSFER, 1 );
    //     curl_setopt($handle, CURLOPT_SSL_VERIFYHOST, false);
    //     curl_setopt($handle, CURLOPT_SSL_VERIFYPEER, false);
    // $result = curl_exec( $handle );
    // curl_close( $handle );
    // $streamat = array();
    // $result = json_decode($result);

    // var_dump($result);
    // die;

      if (isset($result[0]->affiliates)){
        foreach ($result[0]->affiliates as $key => $value) {
          if (isset($value->url) ){
            $streamat[$key] = $value;
          }
        }
      }

      $reviewurl = "https://api.themoviedb.org/3/movie/$id/reviews?api_key=f0dc06cd59a97bbddcebd05e902559e0&language=en-US&page=1";
      $reviewcurl = curl_init();
      curl_setopt($reviewcurl,CURLOPT_RETURNTRANSFER,1);
      curl_setopt($reviewcurl, CURLOPT_URL, $reviewurl);
      curl_setopt($reviewcurl, CURLOPT_SSL_VERIFYHOST, false);
      curl_setopt($reviewcurl, CURLOPT_SSL_VERIFYPEER, false);
      $reviews = curl_exec($reviewcurl);
      $reviews = json_decode($reviews);
      if(isset($reviews->results))
        $reviewslist = $reviews->results;

      $genre_url = "https://api.themoviedb.org/3/genre/movie/list?api_key=f0dc06cd59a97bbddcebd05e902559e0&language=en-US";
      $genrerequest = curl_init();
      curl_setopt($genrerequest,CURLOPT_RETURNTRANSFER,1);
      curl_setopt($genrerequest, CURLOPT_SSL_VERIFYHOST, 0);
      curl_setopt($genrerequest, CURLOPT_SSL_VERIFYPEER, 0);
      curl_setopt($genrerequest, CURLOPT_URL, $genre_url);
      $genres = curl_exec($genrerequest);    

      $genres = json_decode($genres);
      $genres = $genres->genres;

      $similar_url = "https://api.themoviedb.org/3/movie/$id/similar?api_key=f0dc06cd59a97bbddcebd05e902559e0&language=en-US&page=1";

      $c = curl_init();
      curl_setopt($c,CURLOPT_RETURNTRANSFER,1);
      curl_setopt($c, CURLOPT_URL, $similar_url);
      curl_setopt($c, CURLOPT_SSL_VERIFYHOST, false);
      curl_setopt($c, CURLOPT_SSL_VERIFYPEER, false);
      $contents_gen = curl_exec($c);    
      $contents_gen = json_decode($contents_gen);

      $related_movies = $contents_gen->results;
      $type = 'Movies';

      $content_provide = DB::table('content_providers')
      ->where('content_id',$content_id)
      ->get();

      if (!$content_provide  && isset($justwatch_id)){

        $details_url = "https://apis.justwatch.com/content/titles/movie/".$value->content_source_id."/locale/en_IN?language=en";

        $c = curl_init();
        curl_setopt($c,CURLOPT_RETURNTRANSFER,1);
        curl_setopt($c, CURLOPT_URL, $details_url);
        curl_setopt($c, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($c, CURLOPT_SSL_VERIFYPEER, false);
        $contents_gen = curl_exec($c);    
        $contents_gen = json_decode($contents_gen);
        if (isset($contents_gen->offers)){
          $providerlist = $contents_gen->offers;
          foreach ($providerlist as $key2 => $value2) {
            DB::table('content_providers')
            ->where('content_id',$content_id)
            ->get();
            $db_data = DB::table('content_providers')->insert(
              array(
                'content_id' => $value->content_id,
                'provider_id' => $value2->provider_id ,
                'video_quality' => $value2->presentation_type ?? '',
                'monetization_type' => $value2->monetization_type ?? '',
                'retail_price' => $value2->retail_price ?? '',
                'web_url' => $value2->urls->standard_web ?? ""
              ));

          }



        }

      }
      $content_provide = DB::table('content_providers')
      ->where('content_id',$content_id)
      ->get();


//For IMDB Rating
      $url = "https://api.themoviedb.org/3/movie/$id?api_key=f0dc06cd59a97bbddcebd05e902559e0&language=en-US";

      $c = curl_init();
      curl_setopt($c,CURLOPT_RETURNTRANSFER,1);
      curl_setopt($c, CURLOPT_URL, $url);
      curl_setopt($c, CURLOPT_SSL_VERIFYHOST, false);
      curl_setopt($c, CURLOPT_SSL_VERIFYPEER, false);
      $content = curl_exec($c);
      $content = json_decode($content);

      if(isset($content->imdb_id)){
      //For IMDB Rating
        $movie_imdb = $content->imdb_id;

        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://movie-database-imdb-alternative.p.rapidapi.com/?i=$movie_imdb&r=json",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_SSL_VERIFYPEER => false,
          CURLOPT_SSL_VERIFYHOST => false,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_HTTPHEADER => array(
            "x-rapidapi-host: movie-database-imdb-alternative.p.rapidapi.com",
            "x-rapidapi-key: 49763b4616mshf6f6b0ff05d1434p132176jsn30326869077d"
          ),
        ));

        $response = curl_exec($curl);

        $response = json_decode($response);
        $err = curl_error($curl);

        if ($err) {
          echo "cURL Error #:" . $err;
        } else {
          if(isset($response->Response) && $response->Response == 'True'){
            if(isset($response->imdbRating)){
              $imdb_rating = $response->imdbRating;
            }
            else{
              $imdb_rating = 'N/A';
            }
          }
          else{
            $imdb_rating = 'N/A';
          }
        }
      }
      else{
        $imdb_rating = 'N/A';  
      }

      $predict = 'Single';
// echo "http://api.rottentomatoes.com/api/public/v1.0/lists/movies/in_theaters.json?page_limit=1&page=1&country=us&apikey=49763b4616mshf6f6b0ff05d1434p132176jsn30326869077d";
// die;

      return view('frontnew.movie-single', compact('movie','directors','writers','trailorslist','reviewslist', 'casts', 'crews', 'watchlist', 'playlist', 'favourite', 'user_rating', 'genres', 'related_movies' , 'type' , 'imdb_rating','content_provide','predict'));
    }

    public function getTrendingMovies(Request $request)
    { 
      $total_pages = 0;
      $predict ="Trending";
      $date = date('Y-m-d');
      $imdb_rating = [];

      if (isset($request->page_id) && $request->page_id >1){
        $page = $request->page_id;
      }   
      else
        $page = 1;

      // $url = "https://api.themoviedb.org/3/movie/popular?api_key=f0dc06cd59a97bbddcebd05e902559e0&language=en-US&sort_by=&primary_release_date.lte=$date&include_adult=false&include_video=false&page=$page";

      $url = "https://api.themoviedb.org/3/discover/movie?api_key=f0dc06cd59a97bbddcebd05e902559e0&language=en-US&sort_by=release_date.desc&amp;region=US&primary_release_date.lte=$date&include_adult=false&include_video=false&page=$page";
      
      $genre_url = "https://api.themoviedb.org/3/genre/movie/list?api_key=f0dc06cd59a97bbddcebd05e902559e0&language=en-US";
      $c = curl_init();
      curl_setopt($c,CURLOPT_RETURNTRANSFER,1);
      curl_setopt($c, CURLOPT_URL, $url);
      curl_setopt($c, CURLOPT_SSL_VERIFYHOST, false);
      curl_setopt($c, CURLOPT_SSL_VERIFYPEER, false);
      $contents = curl_exec($c);

      $genrerequest = curl_init();
      curl_setopt($genrerequest,CURLOPT_RETURNTRANSFER,1);
      curl_setopt($genrerequest, CURLOPT_URL, $genre_url);
      curl_setopt($genrerequest, CURLOPT_SSL_VERIFYHOST, false);
      curl_setopt($genrerequest, CURLOPT_SSL_VERIFYPEER, false);
      $genres = curl_exec($genrerequest);

      $contents = json_decode($contents);
      if (isset($contents)){
        $total_pages = $contents->total_pages;
        $genres = json_decode($genres);

        foreach($contents->results as $con){

          $url = "https://api.themoviedb.org/3/movie/$con->id?api_key=f0dc06cd59a97bbddcebd05e902559e0&language=en-US";

          $c = curl_init();
          curl_setopt($c,CURLOPT_RETURNTRANSFER,1);
          curl_setopt($c, CURLOPT_URL, $url);
          curl_setopt($c, CURLOPT_SSL_VERIFYHOST, false);
          curl_setopt($c, CURLOPT_SSL_VERIFYPEER, false);
          $content = curl_exec($c);
          $content = json_decode($content);

          $movie_imdb = $content->imdb_id;

          $curl = curl_init();

          curl_setopt_array($curl, array(
            CURLOPT_URL => "https://movie-database-imdb-alternative.p.rapidapi.com/?i=$movie_imdb&r=json",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_SSL_VERIFYHOST => false,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
              "x-rapidapi-host: movie-database-imdb-alternative.p.rapidapi.com",
              "x-rapidapi-key: 49763b4616mshf6f6b0ff05d1434p132176jsn30326869077d"
            ),
          ));

          $response = curl_exec($curl);

          $response = json_decode($response);

          $err = curl_error($curl);

          if ($err) {
            echo "cURL Error #:" . $err;
          } else {
            if(isset($response->Response) && $response->Response == 'True'){
              if(isset($response->imdbRating)){
                $imdb_rating[] = $response->imdbRating;
              }
              else{
                $imdb_rating[] = 'N/A';
              }
            }
            else{
              $imdb_rating[] = 'N/A';
            }
          }
        }
      }
      if (isset($contents) && isset($genres))
        $genres = $genres->genres;
      else 
        $genres = array();

      $side = "Trending Movies"; 
      $type = 'Movies';

      return view('frontnew.home', compact('contents', 'genres','side', 'type' , 'total_pages','predict','imdb_rating'));
    }

    public function getNewMovies(Request $request)
    { 
      $total_pages = 0;
      $predict ="New";
      $imdb_rating = [];
      $date = date('Y-m-d');
      $min = (date('Y'))."-".(date('m')-1)."-".(date('d')-1);      

      if (isset($request->page_id) && $request->page_id >1){
        $page = $request->page_id;
      }   
      else
        $page = 1;
      $url = "https://api.themoviedb.org/3/movie/popular?api_key=f0dc06cd59a97bbddcebd05e902559e0&language=en-US&sort_by=popularity.asc&primary_release_date.lte=$date&primary_release_date.gte=$min&include_adult=false&include_video=false&page=$page";

      $genre_url = "https://api.themoviedb.org/3/genre/movie/list?api_key=f0dc06cd59a97bbddcebd05e902559e0&language=en-US";
      $c = curl_init();
      curl_setopt($c,CURLOPT_RETURNTRANSFER,1);
      curl_setopt($c, CURLOPT_URL, $url);
      curl_setopt($c, CURLOPT_SSL_VERIFYHOST, false);
      curl_setopt($c, CURLOPT_SSL_VERIFYPEER, false);
      $contents = curl_exec($c);

      $genrerequest = curl_init();
      curl_setopt($genrerequest,CURLOPT_RETURNTRANSFER,1);
      curl_setopt($genrerequest, CURLOPT_URL, $genre_url);
      curl_setopt($genrerequest, CURLOPT_SSL_VERIFYHOST, false);
      curl_setopt($genrerequest, CURLOPT_SSL_VERIFYPEER, false);
      $genres = curl_exec($genrerequest);

      $contents = json_decode($contents);


      if (isset($contents)){
        $total_pages = $contents->total_pages;
        $genres = json_decode($genres);

        foreach($contents->results as $con){

          $url = "https://api.themoviedb.org/3/movie/$con->id?api_key=f0dc06cd59a97bbddcebd05e902559e0&language=en-US";

          $c = curl_init();
          curl_setopt($c,CURLOPT_RETURNTRANSFER,1);
          curl_setopt($c, CURLOPT_URL, $url);
          curl_setopt($c, CURLOPT_SSL_VERIFYHOST, false);
          curl_setopt($c, CURLOPT_SSL_VERIFYPEER, false);
          $content = curl_exec($c);
          $content = json_decode($content);

          $movie_imdb = $content->imdb_id;

          $curl = curl_init();

          curl_setopt_array($curl, array(
            CURLOPT_URL => "https://movie-database-imdb-alternative.p.rapidapi.com/?i=$movie_imdb&r=json",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_SSL_VERIFYHOST => false,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
              "x-rapidapi-host: movie-database-imdb-alternative.p.rapidapi.com",
              "x-rapidapi-key: 49763b4616mshf6f6b0ff05d1434p132176jsn30326869077d"
            ),
          ));

          $response = curl_exec($curl);

          $response = json_decode($response);

          $err = curl_error($curl);

          if ($err) {
            echo "cURL Error #:" . $err;
          } else {
            if(isset($response->Response) && $response->Response == 'True'){
              if(isset($response->imdbRating)){
                $imdb_rating[] = $response->imdbRating;
              }
              else{
                $imdb_rating[] = 'N/A';
              }
            }
            else{
              $imdb_rating[] = 'N/A';
            }
          }
        }

      }
      if (isset($contents) && isset($genres))
        $genres = $genres->genres;
      else 
        $genres = array();

      $side = "Trending Movies"; 
      $type = 'Movies';

      return view('frontnew.home', compact('contents', 'genres','side', 'type' , 'total_pages','predict','imdb_rating'));
    }



//     public function getSearch(Request $request)
//     {       
      
//       $keyword = $request->search;
//       $search = $request->searchtype;
//       $imdb_rating = [];
//       $url = "";
//       $contents = "";
//       $predict = "Search";

//       if($search == 'tv')
//         $search = 'show';
      
//       $total_pages = 0;
//       $page = 1;

//       // $countries = Country::all();
//       $check = SearchKeyword::where('keyword',$keyword)
//               ->first();

//       $check_cast = DB::table('content_casting')
//                     ->where('name','LIKE','%'.$keyword.'%')->first();

//       if($check){
//           $data = DB::table('contents')
//                   ->where('title','LIKE','%'.$keyword.'%')
//                   ->where('content_type',$search)
//                   ->orWhere('original_title','LIKE','%'.$keyword.'%')
//                   ->get();
//       }

//       else{
//         $search_key = new SearchKeyword;
//         if (isset(auth()->user()->id)){
//           $search_key->user_id = auth()->user()->id;
//         }
//         $search_key->keyword = $keyword;
//         $search_key->save();

//         $url = "https://api.themoviedb.org/3/search/$search?api_key=f0dc06cd59a97bbddcebd05e902559e0&language=en-US&sort_by=popularity.desc&include_adult=false&query=$keyword&page=$page";
        
//     $c = curl_init();
//     curl_setopt($c,CURLOPT_RETURNTRANSFER,1);
//     curl_setopt($c, CURLOPT_URL, $url);
//     curl_setopt($c, CURLOPT_SSL_VERIFYHOST, 0);
//     curl_setopt($c, CURLOPT_SSL_VERIFYPEER, 0);
//     $contents = curl_exec($c);
//     $contents = json_decode($contents);

//     if(!empty($contents)){
//      $total_pages = $contents->total_pages;

//      foreach($contents->results as $con){

//       $db_content = DB::table('contents')
//                     ->insert(
//                       array(
//                         'content_type' => $search,
//                         'poster' => $con->poster_path ?? '',
//                         'title' => $con->title ?? '' , 
//                         'short_description' => $con->overview ?? '',
//                         'original_language' => $con->original_language,
//                         'original_title' =>$con->original_title ?? '',
//                         'content_source_id' => $con->id,
//                         'runtime' => $con->runtime ?? '0',
//                       ));

//                     $db_check = DB::table('contents')
//                           ->orderBy('content_id','DESC')
//                           ->first();
//                           $content_id = $db_check->content_id;

//                     if(isset($con->genre_ids)){
//                       $gens = $con->genre_ids;
                     
//                       foreach ($gens as $value) {
//                         $db_genre = DB::table('content_genres')
//                                     ->insert(
//                                       array(
//                                         'content_id' => $content_id,
//                                         'genre_id' => $value
//                                         ));

//                       }
//                     }

                    

//       $url = "https://api.themoviedb.org/3/movie/$con->id?api_key=f0dc06cd59a97bbddcebd05e902559e0&language=en-US";

//       $c = curl_init();
//       curl_setopt($c,CURLOPT_RETURNTRANSFER,1);
//       curl_setopt($c, CURLOPT_URL, $url);
//       curl_setopt($c, CURLOPT_SSL_VERIFYHOST, false);
//       curl_setopt($c, CURLOPT_SSL_VERIFYPEER, false);
//       $content = curl_exec($c);
//       $content = json_decode($content);

//       $movie_imdb = $content->imdb_id;

//       $curl = curl_init();

//       curl_setopt_array($curl, array(
//         CURLOPT_URL => "https://movie-database-imdb-alternative.p.rapidapi.com/?i=$movie_imdb&r=json",
//         CURLOPT_RETURNTRANSFER => true,
//         CURLOPT_FOLLOWLOCATION => true,
//         CURLOPT_ENCODING => "",
//         CURLOPT_MAXREDIRS => 10,
//         CURLOPT_SSL_VERIFYPEER => false,
//         CURLOPT_SSL_VERIFYHOST => false,
//         CURLOPT_TIMEOUT => 30,
//         CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
//         CURLOPT_CUSTOMREQUEST => "GET",
//         CURLOPT_HTTPHEADER => array(
//           "x-rapidapi-host: movie-database-imdb-alternative.p.rapidapi.com",
//           "x-rapidapi-key: 49763b4616mshf6f6b0ff05d1434p132176jsn30326869077d"
//         ),
//       ));

//       $response = curl_exec($curl);

//       $response = json_decode($response);

//       $err = curl_error($curl);

//       if ($err) {
//         echo "cURL Error #:" . $err;
//       } else {
//         if(isset($response->Response) && $response->Response == 'True'){
//           if(isset($response->imdbRating)){
//             $imdb_rating[] = $response->imdbRating;
//           }
//           else{
//             $imdb_rating[] = 'N/A';
//           }
//         }
//         else{
//           $imdb_rating[] = 'N/A';
//         }
//       }
//     }
//   }
// }

//   $genre_url = "https://api.themoviedb.org/3/genre/movie/list?api_key=f0dc06cd59a97bbddcebd05e902559e0&language=en-US";
//   $genrerequest = curl_init();
//   curl_setopt($genrerequest,CURLOPT_RETURNTRANSFER,1);
//   curl_setopt($genrerequest, CURLOPT_SSL_VERIFYHOST, 0);
//   curl_setopt($genrerequest, CURLOPT_SSL_VERIFYPEER, 0);
//   curl_setopt($genrerequest, CURLOPT_URL, $genre_url);
//   $genres = curl_exec($genrerequest);

//   $genres = json_decode($genres);
//   $genres = $genres->genres;

//   if($search == 'movie')
//     $type="Movies";
//   elseif($search == 'tv')
//     $type="TV Serials";
//   else
//     $type="Web Series";    

//   $side = "Search";
//   return view('frontnew.home', compact('contents', 'genres', 'keyword', 'countries', 'type', 'total_pages', 'predict','side','imdb_rating'));
// }




    public function getSearch(Request $request)
    {       

      $keyword = $request->search;
      $imdb_rating = [];
      $predict = "Search";

      $search = new SearchKeyword;
      if (isset(auth()->user()->id)){
        $search->user_id = auth()->user()->id;
      }
      $search->keyword = $keyword;
      $search->save();

      $check_cast = Cast::where('name','LIKE','%'.$keyword.'%')->first();

      $url = "";

      $total_pages = 0;
      $page = 1;
      if (isset($request->page_id) && $request->page_id >1){
        $page = $request->page_id;
      }   
      else
        $page = 1;
      $search = $request->searchtype;    
      $contents = "";

      $countries = Country::all();

      if(!$check_cast){
        $url = "https://api.themoviedb.org/3/search/$search?api_key=f0dc06cd59a97bbddcebd05e902559e0&language=en-US&sort_by=popularity.desc&include_adult=false&query=$keyword&page=$page";
        $c = curl_init();
        curl_setopt($c,CURLOPT_RETURNTRANSFER,1);
        curl_setopt($c, CURLOPT_URL, $url);
        curl_setopt($c, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($c, CURLOPT_SSL_VERIFYPEER, 0);
        $contents = curl_exec($c);
        $contents = json_decode($contents);
        if(!empty($contents))
         $total_pages = $contents->total_pages;
     }
     else{
       $url = "https://api.themoviedb.org/3/person/$check_cast->id/movie_credits?api_key=f0dc06cd59a97bbddcebd05e902559e0&language=en-US";
       $c = curl_init();
       curl_setopt($c,CURLOPT_RETURNTRANSFER,1);
       curl_setopt($c, CURLOPT_URL, $url);
       curl_setopt($c, CURLOPT_SSL_VERIFYHOST, 0);
       curl_setopt($c, CURLOPT_SSL_VERIFYPEER, 0);
       $people_contents = curl_exec($c);
       $people_contents = json_decode($people_contents);
       if(!empty($people_contents->cast))   
        $people_contents->results = $people_contents->cast;
      $total_pages = 0;

    }

    $url = "https://api.themoviedb.org/3/search/$search?api_key=f0dc06cd59a97bbddcebd05e902559e0&language=en-US&sort_by=popularity.desc&include_adult=false&query=$keyword&page=$page";
    $c = curl_init();
    curl_setopt($c,CURLOPT_RETURNTRANSFER,1);
    curl_setopt($c, CURLOPT_URL, $url);
    curl_setopt($c, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($c, CURLOPT_SSL_VERIFYPEER, 0);
    $contents = curl_exec($c);
    $contents = json_decode($contents);
    if(!empty($contents)){
     $total_pages = $contents->total_pages;

       foreach($contents->results as $con){

      $url = "https://api.themoviedb.org/3/movie/$con->id?api_key=f0dc06cd59a97bbddcebd05e902559e0&language=en-US";

      $c = curl_init();
      curl_setopt($c,CURLOPT_RETURNTRANSFER,1);
      curl_setopt($c, CURLOPT_URL, $url);
      curl_setopt($c, CURLOPT_SSL_VERIFYHOST, false);
      curl_setopt($c, CURLOPT_SSL_VERIFYPEER, false);
      $content = curl_exec($c);
      $content = json_decode($content);

      $movie_imdb = $content->imdb_id;

        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://movie-database-imdb-alternative.p.rapidapi.com/?i=$movie_imdb&r=json",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_SSL_VERIFYPEER => false,
          CURLOPT_SSL_VERIFYHOST => false,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_HTTPHEADER => array(
            "x-rapidapi-host: movie-database-imdb-alternative.p.rapidapi.com",
            "x-rapidapi-key: 49763b4616mshf6f6b0ff05d1434p132176jsn30326869077d"
          ),
        ));

        $response = curl_exec($curl);

        $response = json_decode($response);

        $err = curl_error($curl);

        if ($err) {
          echo "cURL Error #:" . $err;
        } else {
          if(isset($response->Response) && $response->Response == 'True'){
            if(isset($response->imdbRating)){
              $imdb_rating[] = $response->imdbRating;
            }
            else{
              $imdb_rating[] = 'N/A';
            }
          }
          else{
            $imdb_rating[] = 'N/A';
          }
        }
      }
    }

   $genre_url = "https://api.themoviedb.org/3/genre/movie/list?api_key=f0dc06cd59a97bbddcebd05e902559e0&language=en-US";
   $genrerequest = curl_init();
   curl_setopt($genrerequest,CURLOPT_RETURNTRANSFER,1);
   curl_setopt($genrerequest, CURLOPT_SSL_VERIFYHOST, 0);
   curl_setopt($genrerequest, CURLOPT_SSL_VERIFYPEER, 0);
   curl_setopt($genrerequest, CURLOPT_URL, $genre_url);
   $genres = curl_exec($genrerequest);

   $genres = json_decode($genres);
   $genres = $genres->genres;

   if($search == 'movie')
    $type="Movies";
  elseif($search == 'tv')
    $type="TV Serials";
  else
    $type="Web Series";    

  $side = "Search";
  return view('frontnew.home', compact('contents', 'genres', 'keyword', 'countries', 'type', 'total_pages', 'predict','side','imdb_rating'));
}


public function getUpcomingMovies(Request $request)
{           
$total_pages = 0;
$predict ="Upcoming";
$imdb_rating = [];

$min_date = date('Y-m-d');

if (isset($request->page_id) && $request->page_id >1){
  $page = $request->page_id;
}   
else
  $page = 1;      

$url = "https://api.themoviedb.org/3/discover/movie?api_key=f0dc06cd59a97bbddcebd05e902559e0&language=en-US&amp;region=US&primary_release_date.gte=$min_date&sort_by=release_date.asc&include_adult=false&include_video=false&page=$page";

$genre_url = "https://api.themoviedb.org/3/genre/movie/list?api_key=f0dc06cd59a97bbddcebd05e902559e0&language=en-US";
$c = curl_init();
curl_setopt($c,CURLOPT_RETURNTRANSFER,1);
curl_setopt($c, CURLOPT_URL, $url);
curl_setopt($c, CURLOPT_SSL_VERIFYHOST, 0);
curl_setopt($c, CURLOPT_SSL_VERIFYPEER, 0);
$contents = curl_exec($c);
$genrerequest = curl_init();
curl_setopt($genrerequest,CURLOPT_RETURNTRANSFER,1);
curl_setopt($genrerequest, CURLOPT_URL, $genre_url);
curl_setopt($genrerequest, CURLOPT_SSL_VERIFYHOST, false);
curl_setopt($genrerequest, CURLOPT_SSL_VERIFYPEER, false);
$genres = curl_exec($genrerequest);

$contents = json_decode($contents);
if(isset($contents)){
  $total_pages = $contents->total_pages;

  foreach($contents->results as $con){

    $url = "https://api.themoviedb.org/3/movie/$con->id?api_key=f0dc06cd59a97bbddcebd05e902559e0&language=en-US";

    $c = curl_init();
    curl_setopt($c,CURLOPT_RETURNTRANSFER,1);
    curl_setopt($c, CURLOPT_URL, $url);
    curl_setopt($c, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($c, CURLOPT_SSL_VERIFYPEER, false);
    $content = curl_exec($c);
    $content = json_decode($content);

    $movie_imdb = $content->imdb_id;

    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => "https://movie-database-imdb-alternative.p.rapidapi.com/?i=$movie_imdb&r=json",
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_SSL_VERIFYPEER => false,
      CURLOPT_SSL_VERIFYHOST => false,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "GET",
      CURLOPT_HTTPHEADER => array(
        "x-rapidapi-host: movie-database-imdb-alternative.p.rapidapi.com",
        "x-rapidapi-key: 49763b4616mshf6f6b0ff05d1434p132176jsn30326869077d"
      ),
    ));

    $response = curl_exec($curl);

    $response = json_decode($response);

    $err = curl_error($curl);

    if ($err) {
      echo "cURL Error #:" . $err;
    } else {
      if(isset($response->Response) && $response->Response == 'True'){
        if(isset($response->imdbRating)){
          $imdb_rating[] = $response->imdbRating;
        }
        else{
          $imdb_rating[] = 'N/A';
        }
      }
      else{
        $imdb_rating[] = 'N/A';
      }
    }
  }
}
$genres = json_decode($genres);
$genres = $genres->genres;

$side = "Upcoming Movies";
$type="Movies";
  return view('frontnew.home', compact('contents', 'genres', 'side', 'type','total_pages','predict','imdb_rating'));
}

public function getFreeMovies(Request $request)
{           
  $total_pages = 0;
  $predict ="Free";
  $results=[];
  $contents= new StdClass;
  
  $min_date = date('Y-m-d');

  $getCost = DB::table('content_providers')
              ->where('retail_price',0)
              ->select('content_id')
              ->distinct('content_id')
              ->paginate(20);

              if($getCost){
                foreach($getCost as $cost){
                  $content = new StdClass;
                  // $contents->results[] = DB::table('contents')->where('content_id',$cost->content_id)->first();
                  $data = DB::table('contents')->where('content_id',$cost->content_id)->first();

                  if($data){
                  $content->id = $data->content_source_id;
                  $content->title = $data->title;
                  $content->poster_path = $data->poster;
                  $content->vote_average = $data->tmdb_score;

                  $results[] = $content;
                  $imdb_rating[] = $data->imdb_score;
                }


                }
              }           

$total_pages = DB::table('content_providers')
              ->where('retail_price',0)
              ->select('content_id')
              ->distinct('content_id')
              ->count();

              $total_pages = ceil($total_pages/20);

             
$contents->results = $results;

  $side = "Free Movies";
  $type = "Movies";
  return view('frontnew.home', compact('contents', 'genres', 'side', 'type','total_pages','predict'));
}

public function getLatestMovies(Request $request)
{           
  $total_pages = 0;
  $movies = array();
  if (isset($request->page_id) && $request->page_id >1){
    $page = $request->page_id;
  }   
  else
    $page = 1;    

  $url = "https://api.themoviedb.org/3/discover/movie?api_key=f0dc06cd59a97bbddcebd05e902559e0&language=en-US&sort_by=release_date.desc&include_adult=false&include_video=false&page=$page";

  $genre_url = "https://api.themoviedb.org/3/genre/movie/list?api_key=f0dc06cd59a97bbddcebd05e902559e0&language=en-US";
    // die();
  $c = curl_init();
  curl_setopt($c,CURLOPT_RETURNTRANSFER,1);
  curl_setopt($c, CURLOPT_URL, $url);

  curl_setopt($c, CURLOPT_SSL_VERIFYHOST, 0);
  curl_setopt($c, CURLOPT_SSL_VERIFYPEER, 0);
  $contents = curl_exec($c);
  $contents = json_decode($contents);
  if (isset($contents)){
    $movies = $contents;
  }
  else
    $movies = [];
  $genrerequest = curl_init();
  curl_setopt($genrerequest,CURLOPT_RETURNTRANSFER,1);
  curl_setopt($genrerequest, CURLOPT_URL, $genre_url);
  $genres = curl_exec($genrerequest);
  $genres = json_decode($genres);

  if (isset($contents) && isset($genres)){
    $total_pages = $contents->total_pages;
    $genres = $genres->genres;
  }
  else 
    $genres = array();
  if(isset($movies))
    $contents = $movies;

  $side = "New Arrival";
  $type = 'Movies';
  return view('frontnew.home', compact('contents', 'genres', 'side', 'type','total_pages'));
}

public function getTvSeries(Request $request){
  $type = $request->type; 

  if($type == 'Tv'){
    $url = "https://api.themoviedb.org/3/discover/tv?api_key=f0dc06cd59a97bbddcebd05e902559e0&language=en-US&sort_by=popularity.desc&include_adult=false&include_video=false";
    $genre_url = "https://api.themoviedb.org/3/genre/movie/list?api_key=f0dc06cd59a97bbddcebd05e902559e0&language=en-US";
    $c = curl_init();
    curl_setopt($c,CURLOPT_RETURNTRANSFER,1);
    curl_setopt($c, CURLOPT_URL, $url);
    curl_setopt($c, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($c, CURLOPT_SSL_VERIFYPEER, 0);
    $contents = curl_exec($c);
    $genrerequest = curl_init();
    curl_setopt($genrerequest,CURLOPT_RETURNTRANSFER,1);
    curl_setopt($genrerequest, CURLOPT_URL, $genre_url);
    curl_setopt($genrerequest, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($genrerequest, CURLOPT_SSL_VERIFYPEER, false);
    $genres = curl_exec($genrerequest);

    $contents = json_decode($contents);
    $contents = $contents->results;
    $genres = json_decode($genres);

    $side = 'TV Shows';

    return view('frontnew.search-filter',compact('contents', 'genres','type','total_pages','side'));
  }
}


public function getSingleTseries(Request $request,$id)
{         
  $favourite = 0;
  $watchlist = 0;
  $playlist = 0;
  $contents = "";      
  $predict = 'Single';
  $seasons = "";

  $imdb_rating = 0;
  $release_year = 0;

  $db_content = "";
  $path = "";
  $content_id = "";

  if (isset(auth()->user()->id)){
    $favourite = Favourite::where('movie_id', $id)->where('user_id', auth()->user()->id)->first();
    if ($favourite){
      $favourite = 1;
    }
    $watchlist = WatchList::where('movie_id', $id)->where('user_id', auth()->user()->id)->first();
    if ($watchlist){
      $watchlist = 1;
    }

    $playlist = Playlist::where('movie_id', $id)->where('user_id', auth()->user()->id)->first();
    if ($playlist){
      $playlist = 1;
    }
  }
  $movie = Movie::where('movie_id', $id)->where('type','Series')->first();

  $casts = Crew::join('casts', 'casts.id' , '=', 'crews.cast_id')->where('casts.movie_id',$id)->where('casts.type','tvseries')->where('job', '!=', 'Director')->get();

  $crews = $casts;

  if (!isset($movie)){

    $url = "https://api.themoviedb.org/3/tv/$id?api_key=f0dc06cd59a97bbddcebd05e902559e0&language=en-US";
    
    $c = curl_init();
    curl_setopt($c,CURLOPT_RETURNTRANSFER,1);
    curl_setopt($c, CURLOPT_URL, $url);
    curl_setopt($c, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($c, CURLOPT_SSL_VERIFYPEER, 0);

    $contents = curl_exec($c);

    $contents = json_decode($contents);


    $movie = new Movie;
    $movie->movie_id = $id; 
    $movie->name = $contents->original_name; 
    if(isset($contents->episode_run_time[0]))
      $movie->runtime = $contents->episode_run_time[0]; 
    else 
      $movie->runtime = '0'; 
    $movie->release_date = $contents->first_air_date; 
    $movie->poster_path = $contents->poster_path; 
    $movie->vote_average = $contents->vote_average; 
    $movie->overview = $contents->overview; 
    $movie->type = 'Series'; 

    $path = "/poster/".$contents->poster_path;

    $movie->save(); 

    $db_content = DB::table('contents')
    ->where('title',$movie->name)
    ->where('content_type','show')
    ->first();

    //If Not data available for this movie in contents table
    if(isset($db_content)){
      $content_id = $db_content->content_id;
    }
    else{ 

     $release_year = date('Y',strtotime($contents->first_air_date));



     $db_data = DB::table('contents')->insert(
      array(
        'title' => $movie->title,
        'content_type' => 'show',
        'poster' => $path,
        'short_description' => $movie->overview,
        'original_release_year' => $release_year,
        'original_title' => $contents->original_name,
        'original_language' => $contents->original_language,
        'runtime' => $contents->episode_run_time[0],
        'tmdb_popularity' => $contents->popularity,
        'total_seasons' => $contents->number_of_seasons,
        'total_episodes' => $contents->number_of_episodes,
      ));

     $db_data = DB::table('contents')
     ->orderBy('content_id','DESC')
     ->limit(1)
     ->first();

     $content_id = $db_data->content_id;

     $gene = [];
     $gene = $contents->genres;
     if(isset($gene)){
      foreach($gene as $g){
        $db_gen = DB::table('content_genres')->insert(
          array(
            'content_id' => $content_id,
            'genre_id' => $g->id
          )
        );
      }
    }
  }
}


$release_year = date('Y',strtotime($movie->release_date));
$db_content = DB::table('contents')
->where('title',$movie->name)
->where('content_type','show')
->first();

if(isset($db_content)){
  $content_id = $db_content->content_id;
}
else{
  $db_data = DB::table('contents')->insert(
    array(
      'title' => $movie->name,
      'content_type' => 'movie',
      'poster' => $path,
      'short_description' => $movie->overview,
      'original_release_year' => $release_year,
      'original_title' => $movie->name,
      'runtime' => $movie->runtime,
    ));
}

if (count($casts)<1){
  $castlist = "https://api.themoviedb.org/3/tv/$id/credits?api_key=f0dc06cd59a97bbddcebd05e902559e0";
  $creditcurl = curl_init();
  curl_setopt($creditcurl,CURLOPT_RETURNTRANSFER,1);
  curl_setopt($creditcurl, CURLOPT_URL, $castlist);
  curl_setopt($creditcurl, CURLOPT_SSL_VERIFYHOST, 0);
  curl_setopt($creditcurl, CURLOPT_SSL_VERIFYPEER, 0);
  $castlist = curl_exec($creditcurl);
  $castlist = json_decode($castlist);

  if(!empty($castlist)){
    $crews = $castlist->crew;
    $casts = $castlist->cast;
  }
  foreach ($casts as $key => $value) {
    $castcheck = Cast::where('name', $value->name)->first();
    if ($castcheck){
      $crewcheck = Crew::where('cast_id', $castcheck->id)->first();
      if (!$crewcheck){
        $crew = new Crew;
        $crew->movie_id = $id;
        $crew->type = 'tvseries';
        $crew->cast_id = $castcheck->id;
        $crew->job = $value->character;
        $crew->save();
      }
    }
    else
    {
      $cast = new Cast;
      $cast->name = $value->name;
      $cast->movie_id = $id;
      $cast->type = 'tvseries';
      $cast->id = $value->id;
      $cast->profile_path = $value->profile_path;
      $cast->save();
    }

    $db_casts = DB::table('content_casting')
    ->where('content_id',$content_id)
    ->get();

    if(!isset($db_casts)){
      $add_cast = DB::table('content_casting')
      ->insert(array(
        'content_id' => $content_id,
        'person_id' => $value->id,
        'name' => $value->name,
        'character_name' => $character ,
        'role' => $department,

      ));
    }
  }
}  

if (isset($crews)){
  $directors = array();
  $writers = array();

  foreach ($crews as $key => $value) {
    if ($value->job == 'Director'){
      $castcheck = Cast::where('name', $value->name)->first();
      if ($castcheck){
        $crewcheck = Crew::where('cast_id', $castcheck->id)->first();
        if (!$crewcheck){
          $crew = new Crew;
          $crew->movie_id = $id;
          $crew->cast_id = $castcheck->id;
          $crew->job = $value->job;
          $crew->type = 'tvseries';
          $crew->save();
        }
      }
      else
      {
        $cast = new Cast;
        $cast->movie_id = $id;
        $cast->name = $value->name;
        $cast->id = $value->id;
        $cast->profile_path = $value->profile_path;
        $cast->type = 'tvseries';
        $cast->save();
      }
      array_push($directors, $value->name);
    }
    if ($value->job == 'Story'){
      $castcheck = Cast::where('name', $value->name)->first();
      if ($castcheck){
        $crewcheck = Crew::where('cast_id', $castcheck->id)->first();
        if (!$crewcheck){
          $crew = new Crew;
          $crew->movie_id = $id;
          $crew->cast_id = $castcheck->id;
          $crew->job = $value->job;
          $crew->type = 'tvseries';
          $crew->save();
        }
      }
      else
      {
        $cast = new Cast;
        $cast->movie_id = $id;
        $cast->id = $value->id;
        $cast->name = $value->name;
        $cast->type = 'tvseries';
        $cast->profile_path = $value->profile_path;
        $cast->save();
      }
      array_push($writers, $value->name);
    }
  }
}
else{
  $directors = Crew::join('casts', 'casts.id' , '=' ,'crews.cast_id')
  ->select('casts.name')
  ->where('crews.movie_id', $id)
  ->where('crews.job', 'Director')
  ->get();
  $writers = Crew::join('casts', 'casts.id' , '=' ,'crews.cast_id')
  ->select('casts.name')
  ->where('crews.movie_id', $id)
  ->where('crews.job', 'Story')
  ->get();
}

$trailorurl = "https://api.themoviedb.org/3/tv/$id/videos?api_key=f0dc06cd59a97bbddcebd05e902559e0&language=en-US";
$trailorcurl = curl_init();
curl_setopt($trailorcurl,CURLOPT_RETURNTRANSFER,1);
curl_setopt($trailorcurl, CURLOPT_URL, $trailorurl);
curl_setopt($trailorcurl, CURLOPT_SSL_VERIFYHOST, false);
curl_setopt($trailorcurl, CURLOPT_SSL_VERIFYPEER, false);
$trailors = curl_exec($trailorcurl);
$trailors = json_decode($trailors);
      // $trailorslist = array();
if(isset($trailors)){
  if(isset($trailors->results))
    $trailorslist = $trailors->results;
}


      //   $handle = curl_init();
      //   curl_setopt( $handle, CURLOPT_URL, 'http://www.canistream.it/services/search/?movieName=' . urlencode($movie->name) );
      //   curl_setopt( $handle, CURLOPT_USERAGENT, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.94 Safari/537.36' );
      //   curl_setopt($handle, CURLOPT_RETURNTRANSFER, 1 );
      // // $result = curl_exec( $handle );
      //   $result = [];
      // // curl_close( $handle );
      //   $streamat = array();
      // // $result = json_decode($result);

      //   if (isset($result[0]->affiliates)){
      //     foreach ($result[0]->affiliates as $key => $value) {
      //       if (isset($value->url) ){
      //         $streamat[$key] = $value;
      //       }
      //       if (isset($value->SD->url))
      //         $streamat[$key] = $value->SD;
      //       if (isset($value->HD->url))
      //         $streamat[$key] = $value->SD;
      //     }

      //   }

$reviewurl = "https://api.themoviedb.org/3/movie/$id/reviews?api_key=f0dc06cd59a97bbddcebd05e902559e0&language=en-US&page=1";
$reviewcurl = curl_init();
curl_setopt($reviewcurl,CURLOPT_RETURNTRANSFER,1);
curl_setopt($reviewcurl, CURLOPT_URL, $reviewurl);
$reviews = curl_exec($reviewcurl);
$reviews = json_decode($reviews);
$reviewslist = array();

if(isset($reviews))
  if(isset($reviews->results))
    $reviewslist = $reviews->results;


  $genre_url = "https://api.themoviedb.org/3/genre/movie/list?api_key=f0dc06cd59a97bbddcebd05e902559e0&language=en-US";
  $genrerequest = curl_init();
  curl_setopt($genrerequest,CURLOPT_RETURNTRANSFER,1);
  curl_setopt($genrerequest, CURLOPT_SSL_VERIFYHOST, 0);
  curl_setopt($genrerequest, CURLOPT_SSL_VERIFYPEER, 0);
  curl_setopt($genrerequest, CURLOPT_URL, $genre_url);
  $genres = curl_exec($genrerequest);
  $genres = json_decode($genres);
  $genres = $genres->genres;

  $similar_url = "https://api.themoviedb.org/3/tv/$id/similar?api_key=f0dc06cd59a97bbddcebd05e902559e0&language=en-US&page=1";

  $c = curl_init();
  curl_setopt($c,CURLOPT_RETURNTRANSFER,1);
  curl_setopt($c, CURLOPT_URL, $similar_url);
  curl_setopt($c, CURLOPT_SSL_VERIFYHOST, false);
  curl_setopt($c, CURLOPT_SSL_VERIFYPEER, false);
  $contents_gen = curl_exec($c);    
  $contents_gen = json_decode($contents_gen);
  if(!empty($contents_gen))
    $related_movies = $contents_gen->results;

  $url = "https://api.themoviedb.org/3/tv/$id?api_key=f0dc06cd59a97bbddcebd05e902559e0&language=en-US";
  $c = curl_init();
  curl_setopt($c,CURLOPT_RETURNTRANSFER,1);
  curl_setopt($c, CURLOPT_URL, $url);
  curl_setopt($c, CURLOPT_SSL_VERIFYHOST, 0);
  curl_setopt($c, CURLOPT_SSL_VERIFYPEER, 0);

  $contents = curl_exec($c);

  $contents = json_decode($contents);

  $seasons = $contents->seasons;
  $type = 'Movies';

  $url = "https://api.themoviedb.org/3/tv/$id?api_key=f0dc06cd59a97bbddcebd05e902559e0&language=en-US";
  $c = curl_init();
  curl_setopt($c,CURLOPT_RETURNTRANSFER,1);
  curl_setopt($c, CURLOPT_URL, $url);
  curl_setopt($c, CURLOPT_SSL_VERIFYHOST, 0);
  curl_setopt($c, CURLOPT_SSL_VERIFYPEER, 0);

  $contents = curl_exec($c);

  $contents = json_decode($contents);

  $content_provide = DB::table('content_providers')
  ->where('content_id',$content_id)
  ->get();


//For IMDB Rating
  $url = "https://api.themoviedb.org/3/tv/$id?api_key=f0dc06cd59a97bbddcebd05e902559e0&language=en-US";

  $c = curl_init();
  curl_setopt($c,CURLOPT_RETURNTRANSFER,1);
  curl_setopt($c, CURLOPT_URL, $url);
  curl_setopt($c, CURLOPT_SSL_VERIFYHOST, false);
  curl_setopt($c, CURLOPT_SSL_VERIFYPEER, false);
  $content = curl_exec($c);
  $content = json_decode($content);


  if(isset($content->imdb_id)){
      //For IMDB Rating
    $movie_imdb = $content->imdb_id;

    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => "https://movie-database-imdb-alternative.p.rapidapi.com/?i=$movie_imdb&r=json",
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_SSL_VERIFYPEER => false,
      CURLOPT_SSL_VERIFYHOST => false,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "GET",
      CURLOPT_HTTPHEADER => array(
        "x-rapidapi-host: movie-database-imdb-alternative.p.rapidapi.com",
        "x-rapidapi-key: 49763b4616mshf6f6b0ff05d1434p132176jsn30326869077d"
      ),
    ));

    $response = curl_exec($curl);

    $response = json_decode($response);
    $err = curl_error($curl);

    if ($err) {
      echo "cURL Error #:" . $err;
    } else {
      if(isset($response->Response) && $response->Response == 'True'){
        if(isset($response->imdbRating)){
          $imdb_rating = $response->imdbRating;
        }
        else{
          $imdb_rating = 'N/A';
        }
      }
      else{
        $imdb_rating = 'N/A';
      }
    }
  }
  else{
    $imdb_rating = 'N/A';  
  }

  return view('frontnew.tseries-single', compact('movie','directors','writers','trailorslist','reviewslist', 'casts', 'crews', 'watchlist', 'playlist', 'favourite','streamat', 'genres','related_movies','seasons','type','content_provide','predict'));
}

public function submitNewsletter(Request $request){
  $check = Newsletter::where('email',$request->email)->first();
  $message = 'Something Went Wrong';
  if(!empty($check)){
    $message = 'This email is already registered.';
  }
  else{
    $news = new Newsletter;
    $news->email = $request->email;
    $news->save();
    $message = 'Thank You. We will reply you soon.';
  }
  return response()->json(['message'=>$message]);
}

public function getSearchFilter(Request $request)
{       
 $message = 'Something Went Wrong';
 $sort = $request->sort;
 $rated = $request->rated;
 $page = $request->page;
 $keyword = $request->search;
 $genre = $request->genre;
 $min_y = $request->min_y;
 $max_y = $request->max_y;
 $min_r = $request->min_r;
 $max_r = $request->max_r;
 $type = $request->type;
 $predict = $request->predict;
 $side = $request->side;
 $lang = $request->lang;
 $imdb_rating = [];
 $total_pages = 0;

 $contents ="";

 $countries = Country::all();
 if($type){}
  else{ $type="Movies";}

if($type=="Web"){
  return '<h4 style="margin: 100px; text-align: center; "> No result Found </h4>';
}

$genre_url = "https://api.themoviedb.org/3/genre/movie/list?api_key=f0dc06cd59a97bbddcebd05e902559e0&language=en-US";

$genrerequest = curl_init();
curl_setopt($genrerequest,CURLOPT_RETURNTRANSFER,1);
curl_setopt($genrerequest, CURLOPT_SSL_VERIFYHOST, 0);
curl_setopt($genrerequest, CURLOPT_SSL_VERIFYPEER, 0);
curl_setopt($genrerequest, CURLOPT_URL, $genre_url);
$genres = curl_exec($genrerequest);
$genres = json_decode($genres);
$genres = $genres->genres;

$result = [];
$i=0;

if($keyword){

  $url = $this->filterByKeyword($side,$keyword,$type,$page,$sort,$lang);

  $c = curl_init();
  curl_setopt($c,CURLOPT_RETURNTRANSFER,1);
  curl_setopt($c, CURLOPT_URL, $url);
  curl_setopt($c, CURLOPT_SSL_VERIFYHOST, 0);
  curl_setopt($c, CURLOPT_SSL_VERIFYPEER, 0);
  $contents = curl_exec($c);
  $contents = json_decode($contents);
  if(isset($contents->results)){

    foreach($contents->results as $con){

      $url = "https://api.themoviedb.org/3/movie/$con->id?api_key=f0dc06cd59a97bbddcebd05e902559e0&language=en-US";

      $c = curl_init();
      curl_setopt($c,CURLOPT_RETURNTRANSFER,1);
      curl_setopt($c, CURLOPT_URL, $url);
      curl_setopt($c, CURLOPT_SSL_VERIFYHOST, false);
      curl_setopt($c, CURLOPT_SSL_VERIFYPEER, false);
      $content = curl_exec($c);
      $content = json_decode($content);

      $movie_imdb = $content->imdb_id;

      $curl = curl_init();

      curl_setopt_array($curl, array(
        CURLOPT_URL => "https://movie-database-imdb-alternative.p.rapidapi.com/?i=$movie_imdb&r=json",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_SSL_VERIFYPEER => false,
        CURLOPT_SSL_VERIFYHOST => false,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
          "x-rapidapi-host: movie-database-imdb-alternative.p.rapidapi.com",
          "x-rapidapi-key: 49763b4616mshf6f6b0ff05d1434p132176jsn30326869077d"
        ),
      ));

      $response = curl_exec($curl);

      $response = json_decode($response);

      $err = curl_error($curl);

      if ($err) {
        echo "cURL Error #:" . $err;
      } else {
        if(isset($response->Response) && $response->Response == 'True'){
          if(isset($response->imdbRating)){
            $imdb_rating[] = $response->imdbRating;
          }
          else{
            $imdb_rating[] = 'N/A';
          }
        }
        else{
          $imdb_rating[] = 'N/A';
        }
      }
    }
    $contents = $contents->results;
  }
  else
    $contents = "";
}

else{

  if($predict == 'Upcoming'){
   $min_y = date('Y-m-d');

   if($type == 'Tv'){
    $url = $this->filterUpcomingTv($genre,$rated,$min_r,$max_r,$min_y,$sort,$page,$lang); 
  }
  elseif($type == 'Movies'){
    $url = $this->filterUpcomingMovie($genre,$rated,$min_r,$max_r,$min_y,$sort,$page,$lang);
  }
  else{ return "No Result Found";}
}

elseif($predict == 'Trending') {
 $max_y = date('Y-m-d');

 if($type == 'Tv'){
  $url = $this->filterTrendingTv($genre,$rated,$min_r,$max_r,$max_y,$sort,$page,$lang);   

}
elseif($type == 'Movies'){
  $url = $this->filterTrendingMovie($genre,$rated,$min_r,$max_r,$max_y,$sort,$page,$lang);    

}
else{ return "No Result Found";}
}
elseif($predict == 'New'){
  $max_y = date('Y-m-d');
  $min_y = (date('Y'))."-".(date('m')-1)."-".(date('d')-1);

  if($type == 'Tv'){
    $url = $this->filterNewTv($genre,$rated,$min_r,$max_r,$min_y,$max_y,$sort,$page,$lang);   

  }
  elseif($type == 'Movies'){
    $url = $this->filterNewMovie($genre,$rated,$min_r,$max_r,$min_y,$max_y,$sort,$page,$lang);    

  }
  else{ return "No Result Found";}
}
else {
 $min_y = $min_y."-01-01";
 $max_y = $max_y."-12-12";

 if($type == 'Tv'){
  $url = $this->filterByTypeTv($genre,$rated,$min_r,$max_r,$min_y,$max_y,$sort,$page,$lang);   

}
elseif($type == 'Movies'){
  $url = $this->filterByTypeMovie($genre,$rated,$min_r,$max_r,$min_y,$max_y,$sort,$page,$lang);    

}
else{ return "No Result Found";}
}


$c = curl_init();
curl_setopt($c,CURLOPT_RETURNTRANSFER,1);
curl_setopt($c, CURLOPT_URL, $url);
curl_setopt($c, CURLOPT_SSL_VERIFYHOST, 0);
curl_setopt($c, CURLOPT_SSL_VERIFYPEER, 0);
$contents = curl_exec($c);
$contents = json_decode($contents);

if(!empty($contents))
{
  if(!empty($contents->total_pages)){
    $total_pages = $contents->total_pages;
  }
  if(!empty($contents->results)){
    foreach($contents->results as $con){

      $url = "https://api.themoviedb.org/3/movie/$con->id?api_key=f0dc06cd59a97bbddcebd05e902559e0&language=en-US";

      $c = curl_init();
      curl_setopt($c,CURLOPT_RETURNTRANSFER,1);
      curl_setopt($c, CURLOPT_URL, $url);
      curl_setopt($c, CURLOPT_SSL_VERIFYHOST, false);
      curl_setopt($c, CURLOPT_SSL_VERIFYPEER, false);
      $content = curl_exec($c);
      $content = json_decode($content);

      $movie_imdb = $content->imdb_id;

      $curl = curl_init();

      curl_setopt_array($curl, array(
        CURLOPT_URL => "https://movie-database-imdb-alternative.p.rapidapi.com/?i=$movie_imdb&r=json",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_SSL_VERIFYPEER => false,
        CURLOPT_SSL_VERIFYHOST => false,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
          "x-rapidapi-host: movie-database-imdb-alternative.p.rapidapi.com",
          "x-rapidapi-key: 49763b4616mshf6f6b0ff05d1434p132176jsn30326869077d"
        ),
      ));

      $response = curl_exec($curl);

      $response = json_decode($response);

      $err = curl_error($curl);

      if ($err) {
        echo "cURL Error #:" . $err;
      } else {
        if(isset($response->Response) && $response->Response == 'True'){
          if(isset($response->imdbRating)){
            $imdb_rating[] = $response->imdbRating;
          }
          else{
            $imdb_rating[] = 'N/A';
          }
        }
        else{
          $imdb_rating[] = 'N/A';
        }
      }
    }
    $contents = $contents->results;


  }
  else{
    $contents = "";    
  }
}
else{
  $contents = "";
}
}

return view('frontnew.search-filter', compact('total_pages','contents', 'genres', 'keyword', 'countries','type','message','predict','side','imdb_rating'));
}

//Filter for Movies
public function filterByTypeMovie($genre,$rated,$min_r,$max_r,$min_y,$max_y,$sort,$page,$lang){
  $url ="";

  if($lang == 'Holywood'){
    if($genre!=null && $rated!=null){
      $url = "https://api.themoviedb.org/3/discover/movie?api_key=f0dc06cd59a97bbddcebd05e902559e0&language=en-US&sort_by=$sort&include_adult=false&include_video=false&with_genres=$genre&vote_average.gte=$min_r&vote_average.lte=$max_r&certification_country=IN&certification=$rated&primary_release_date.gte=$min_y&primary_release_date.lte=$max_y&page=$page";
    }
    elseif($genre!=null){
      $url = "https://api.themoviedb.org/3/discover/movie?api_key=f0dc06cd59a97bbddcebd05e902559e0&language=en-US&sort_by=$sort&include_adult=false&include_video=false&with_genres=$genre&vote_average.gte=$min_r&vote_average.lte=$max_r&primary_release_date.gte=$min_y&primary_release_date.lte=$max_y&page=$page";
    }
    elseif($rated!=null){
      $url = "https://api.themoviedb.org/3/discover/movie?api_key=f0dc06cd59a97bbddcebd05e902559e0&language=en-US&sort_by=$sort&include_adult=false&include_video=false&vote_average.gte=$min_r&vote_average.lte=$max_r&certification_country=IN&certification=$rated&primary_release_date.gte=$min_y&primary_release_date.lte=$max_y&page=$page";
    }
    else{
      $url = "https://api.themoviedb.org/3/discover/movie?api_key=f0dc06cd59a97bbddcebd05e902559e0&language=en-US&sort_by=$sort&include_adult=false&include_video=false&vote_average.gte=$min_r&vote_average.lte=$max_r&primary_release_date.gte=$min_y&primary_release_date.lte=$max_y&page=$page";
    }
  }
  else{
    if($genre!=null && $rated!=null){
      $url = "https://api.themoviedb.org/3/discover/movie?api_key=f0dc06cd59a97bbddcebd05e902559e0&amp;region=IN&language=hi-IN&with_original_language=hi&sort_by=$sort&include_adult=false&include_video=false&with_genres=$genre&vote_average.gte=$min_r&vote_average.lte=$max_r&certification_country=IN&certification=$rated&primary_release_date.gte=$min_y&primary_release_date.lte=$max_y&page=$page";
    }
    elseif($genre!=null){
      $url = "https://api.themoviedb.org/3/discover/movie?api_key=f0dc06cd59a97bbddcebd05e902559e0&amp;region=IN&language=hi-IN&with_original_language=hi&sort_by=$sort&include_adult=false&include_video=false&with_genres=$genre&vote_average.gte=$min_r&vote_average.lte=$max_r&certification_country=IN&primary_release_date.gte=$min_y&primary_release_date.lte=$max_y&page=$page";
    }
    elseif($rated!=null){
      $url = "https://api.themoviedb.org/3/discover/movie?api_key=f0dc06cd59a97bbddcebd05e902559e0&amp;region=IN&language=hi-IN&with_original_language=hi&sort_by=$sort&include_adult=false&include_video=false&vote_average.gte=$min_r&vote_average.lte=$max_r&certification_country=IN&certification=$rated&primary_release_date.gte=$min_y&primary_release_date.lte=$max_y&page=$page";
    }
    else{
      $url = "https://api.themoviedb.org/3/discover/movie?api_key=f0dc06cd59a97bbddcebd05e902559e0&amp;region=IN&language=hi-IN&with_original_language=hi&sort_by=$sort&include_adult=false&include_video=false&vote_average.gte=$min_r&vote_average.lte=$max_r&certification_country=IN&primary_release_date.gte=$min_y&primary_release_date.lte=$max_y&page=$page";
    }
  }
  return $url;    
}

//Filter For TV shows
public function filterByTypeTv($genre,$rated,$min_r,$max_r,$min_y,$max_y,$sort,$page,$lang){
  $url ="";

  if($lang=="Holywood"){

    if($genre!=null && $rated!=null){
      $url = "https://api.themoviedb.org/3/discover/tv?api_key=f0dc06cd59a97bbddcebd05e902559e0&language=en-US&sort_by=$sort&include_adult=false&include_video=false&with_genres=$genre&vote_average.gte=$min_r&vote_average.lte=$max_r&certification_country=IN&certification=$rated&first_air_date.gte=$min_y&first_air_date.lte=$max_y&page=$page";
    }
    elseif($rated!=null){
      $url = "https://api.themoviedb.org/3/discover/tv?api_key=f0dc06cd59a97bbddcebd05e902559e0&language=en-US&sort_by=$sort&include_adult=false&include_video=false&vote_average.gte=$min_r&vote_average.lte=$max_r&certification_country=IN&certification=$rated&first_air_date.gte=$min_y&first_air_date.lte=$max_y&page=$page";
    }
    elseif($genre!=null){
      $url = "https://api.themoviedb.org/3/discover/tv?api_key=f0dc06cd59a97bbddcebd05e902559e0&language=en-US&sort_by=$sort&include_adult=false&include_video=false&with_genres=$genre&vote_average.gte=$min_r&vote_average.lte=$max_r&first_air_date.gte=$min_y&first_air_date.lte=$max_y&page=$page";
    }
    else{
      $url = "https://api.themoviedb.org/3/discover/tv?api_key=f0dc06cd59a97bbddcebd05e902559e0&language=en-US&sort_by=$sort&include_adult=false&include_video=false&vote_average.gte=$min_r&vote_average.lte=$max_r&first_air_date.gte=$min_y&first_air_date.lte=$max_y&page=$page";
    }
  }
  else{
    if($genre!=null && $rated!=null){
      $url = "https://api.themoviedb.org/3/discover/tv?api_key=f0dc06cd59a97bbddcebd05e902559e0&amp;region=IN&language=hi-IN&with_original_language=hi&sort_by=$sort&include_adult=false&include_video=false&with_genres=$genre&vote_average.gte=$min_r&vote_average.lte=$max_r&certification_country=IN&certification=$rated&first_air_date.gte=$min_y&first_air_date.lte=$max_y&page=$page";
    }
    elseif($rated!=null){
      $url = "https://api.themoviedb.org/3/discover/tv?api_key=f0dc06cd59a97bbddcebd05e902559e0&amp;region=IN&language=hi-IN&with_original_language=hi&sort_by=$sort&include_adult=false&include_video=false&vote_average.gte=$min_r&vote_average.lte=$max_r&certification_country=IN&certification=$rated&first_air_date.gte=$min_y&first_air_date.lte=$max_y&page=$page";
    }
    elseif($genre!=null){
      $url = "https://api.themoviedb.org/3/discover/tv?api_key=f0dc06cd59a97bbddcebd05e902559e0&amp;region=IN&language=hi-IN&with_original_language=hi&sort_by=$sort&include_adult=false&include_video=false&with_genres=$genre&vote_average.gte=$min_r&vote_average.lte=$max_r&first_air_date.gte=$min_y&first_air_date.lte=$max_y&page=$page";
    }
    else{
      $url = "https://api.themoviedb.org/3/discover/tv?api_key=f0dc06cd59a97bbddcebd05e902559e0&amp;region=IN&language=hi-IN&with_original_language=hi&sort_by=$sort&include_adult=false&include_video=false&vote_average.gte=$min_r&vote_average.lte=$max_r&first_air_date.gte=$min_y&first_air_date.lte=$max_y&page=$page";
    } 
  }
  return $url;
}

//By Keyword
public function filterByKeyword($side,$keyword,$type,$page,$sort,$lang){
  $url ="";
  if($lang == 'Holywood'){
    if($type == 'Tv'){
      $url = "https://api.themoviedb.org/3/search/tv?sort_by=$sort&api_key=f0dc06cd59a97bbddcebd05e902559e0&language=en-US&page=1&include_adult=false&query=$keyword&page=$page";
    }
    else if($type=='Movies'){
      $url = "https://api.themoviedb.org/3/search/movie?sort_by=$sort&api_key=f0dc06cd59a97bbddcebd05e902559e0&language=en-US&sort_by=$sort.desc&include_adult=false&query=$keyword&page=$page";
    }
    else if ($type=='All'){
      $url = "https://api.themoviedb.org/3/search/multi?sort_by=$sort&api_key=f0dc06cd59a97bbddcebd05e902559e0&language=en-US&sort_by=$sort.desc&include_adult=false&query=$keyword&page=$page";
    }
  }
  else{
    if($type == 'Tv'){
      $url = "https://api.themoviedb.org/3/search/tv?sort_by=$sort&api_key=f0dc06cd59a97bbddcebd05e902559e0&amp;region=IN&language=hi-IN&with_original_language=hi&page=1&include_adult=false&query=$keyword&page=$page";
    }
    else if($type=='Movies'){
      $url = "https://api.themoviedb.org/3/search/movie?sort_by=$sort&api_key=f0dc06cd59a97bbddcebd05e902559e0&amp;region=IN&language=hi-IN&with_original_language=hi&sort_by=$sort.desc&include_adult=false&query=$keyword&page=$page";
    }
    else if ($type=='All'){
      $url = "https://api.themoviedb.org/3/search/multi?sort_by=$sort&api_key=f0dc06cd59a97bbddcebd05e902559e0&amp;region=IN&language=hi-IN&with_original_language=hi&sort_by=$sort.desc&include_adult=false&query=$keyword&page=$page";
    }
  }

  return $url;
}

//Filter for Upcoming Movies
public function filterUpcomingMovie($genre,$rated,$min_r,$max_r,$min_y,$sort,$page,$lang){
  $url ="";
  if($lang == "Holywood"){
    if($genre!=null && $rated!=null){
      $url = "https://api.themoviedb.org/3/discover/movie?api_key=f0dc06cd59a97bbddcebd05e902559e0&language=en-US&sort_by=$sort&include_adult=false&include_video=false&with_genres=$genre&vote_average.gte=$min_r&vote_average.lte=$max_r&certification_country=IN&certification=$rated&primary_release_date.gte=$min_y&page=$page";
    }
    elseif($genre!=null){
      $url = "https://api.themoviedb.org/3/discover/movie?api_key=f0dc06cd59a97bbddcebd05e902559e0&language=en-US&sort_by=$sort&include_adult=false&include_video=false&with_genres=$genre&vote_average.gte=$min_r&vote_average.lte=$max_r&primary_release_date.gte=$min_y&page=$page";
    }
    elseif($rated!=null){
      $url = "https://api.themoviedb.org/3/discover/movie?api_key=f0dc06cd59a97bbddcebd05e902559e0&language=en-US&sort_by=$sort&include_adult=false&include_video=false&vote_average.gte=$min_r&vote_average.lte=$max_r&certification_country=IN&certification=$rated&primary_release_date.gte=$min_y&page=$page";
    }
    else{
      $url = "https://api.themoviedb.org/3/discover/movie?api_key=f0dc06cd59a97bbddcebd05e902559e0&language=en-US&sort_by=$sort&include_adult=false&include_video=false&vote_average.gte=$min_r&vote_average.lte=$max_r&primary_release_date.gte=$min_y&page=$page";
    }
  }
  else{
    if($genre!=null && $rated!=null){
      $url = "https://api.themoviedb.org/3/discover/movie?api_key=f0dc06cd59a97bbddcebd05e902559e0&amp;region=IN&language=hi-IN&with_original_language=hi&sort_by=$sort&include_adult=false&include_video=false&with_genres=$genre&vote_average.gte=$min_r&vote_average.lte=$max_r&certification_country=IN&certification=$rated&primary_release_date.gte=$min_y&page=$page";
    }
    elseif($genre!=null){
      $url = "https://api.themoviedb.org/3/discover/movie?api_key=f0dc06cd59a97bbddcebd05e902559e0&amp;region=IN&language=hi-IN&with_original_language=hi&sort_by=$sort&include_adult=false&include_video=false&with_genres=$genre&vote_average.gte=$min_r&vote_average.lte=$max_r&primary_release_date.gte=$min_y&page=$page";
    }
    elseif($rated!=null){
      $url = "https://api.themoviedb.org/3/discover/movie?api_key=f0dc06cd59a97bbddcebd05e902559e0&amp;region=IN&language=hi-IN&with_original_language=hi&sort_by=$sort&include_adult=false&include_video=false&vote_average.gte=$min_r&vote_average.lte=$max_r&certification_country=IN&certification=$rated&primary_release_date.gte=$min_y&page=$page";
    }
    else{
      $url = "https://api.themoviedb.org/3/discover/movie?api_key=f0dc06cd59a97bbddcebd05e902559e0&amp;region=IN&language=hi-IN&with_original_language=hi&sort_by=$sort&include_adult=false&include_video=false&vote_average.gte=$min_r&vote_average.lte=$max_r&primary_release_date.gte=$min_y&page=$page";
    }
  }
  return $url;

}

public function filterUpcomingTv($genre,$rated,$min_r,$max_r,$min_y,$sort,$page,$lang){
  $url = "";

  if($lang == 'Holywood'){
    if($genre!=null && $rated!=null){
      $url = "https://api.themoviedb.org/3/discover/tv?api_key=f0dc06cd59a97bbddcebd05e902559e0&language=en-US&sort_by=$sort&include_adult=false&include_video=false&with_genres=$genre&vote_average.gte=$min_r&vote_average.lte=$max_r&certification_country=IN&certification=$rated&first_air_date.gte=$min_y&page=$page";
    }
    elseif($rated!=null){
      $url = "https://api.themoviedb.org/3/discover/tv?api_key=f0dc06cd59a97bbddcebd05e902559e0&language=en-US&sort_by=$sort&include_adult=false&include_video=false&vote_average.gte=$min_r&vote_average.lte=$max_r&certification_country=IN&certification=$rated&first_air_date.gte=$min_y&page=$page";
    }
    elseif($genre!=null){
      $url = "https://api.themoviedb.org/3/discover/tv?api_key=f0dc06cd59a97bbddcebd05e902559e0&language=en-US&sort_by=$sort&include_adult=false&include_video=false&with_genres=$genre&vote_average.gte=$min_r&vote_average.lte=$max_r&first_air_date.gte=$min_y&page=$page";
    }
    else{
      $url = "https://api.themoviedb.org/3/discover/tv?api_key=f0dc06cd59a97bbddcebd05e902559e0&language=en-US&sort_by=$sort&include_adult=false&include_video=false&vote_average.gte=$min_r&vote_average.lte=$max_r&first_air_date.gte=$min_y&page=$page";
    }
  }
  else{
    if($genre!=null && $rated!=null){
      $url = "https://api.themoviedb.org/3/discover/tv?api_key=f0dc06cd59a97bbddcebd05e902559e0&amp;region=IN&language=hi-IN&with_original_language=hi&sort_by=$sort&include_adult=false&include_video=false&with_genres=$genre&vote_average.gte=$min_r&vote_average.lte=$max_r&certification_country=IN&certification=$rated&first_air_date.gte=$min_y&page=$page";
    }
    elseif($rated!=null){
      $url = "https://api.themoviedb.org/3/discover/tv?api_key=f0dc06cd59a97bbddcebd05e902559e0&amp;region=IN&language=hi-IN&with_original_language=hi&sort_by=$sort&include_adult=false&include_video=false&vote_average.gte=$min_r&vote_average.lte=$max_r&certification_country=IN&certification=$rated&first_air_date.gte=$min_y&page=$page";
    }
    elseif($genre!=null){
      $url = "https://api.themoviedb.org/3/discover/tv?api_key=f0dc06cd59a97bbddcebd05e902559e0&amp;region=IN&language=hi-IN&with_original_language=hi&sort_by=$sort&include_adult=false&include_video=false&with_genres=$genre&vote_average.gte=$min_r&vote_average.lte=$max_r&first_air_date.gte=$min_y&page=$page";
    }
    else{
      $url = "https://api.themoviedb.org/3/discover/tv?api_key=f0dc06cd59a97bbddcebd05e902559e0&amp;region=IN&language=hi-IN&with_original_language=hi&sort_by=$sort&include_adult=false&include_video=false&vote_average.gte=$min_r&vote_average.lte=$max_r&first_air_date.gte=$min_y&page=$page";
    }
  }
  return $url;
}


//Filter for Trending Movies
public function filterTrendingMovie($genre,$rated,$min_r,$max_r,$max_y,$sort,$page,$lang){
  $url ="";
  if($lang == 'Holywood'){
    if($genre!=null && $rated!=null){
      $url = "https://api.themoviedb.org/3/discover/movie?api_key=f0dc06cd59a97bbddcebd05e902559e0&language=en-US&sort_by=$sort&include_adult=false&include_video=false&with_genres=$genre&vote_average.gte=$min_r&vote_average.lte=$max_r&certification_country=IN&certification=$rated&primary_release_date.lte=$max_y&page=$page";
    }
    elseif($genre!=null){
      $url = "https://api.themoviedb.org/3/discover/movie?api_key=f0dc06cd59a97bbddcebd05e902559e0&language=en-US&sort_by=$sort&include_adult=false&include_video=false&with_genres=$genre&vote_average.gte=$min_r&vote_average.lte=$max_r&primary_release_date.lte=$max_y&page=$page";
    }
    elseif($rated!=null){
      $url = "https://api.themoviedb.org/3/discover/movie?api_key=f0dc06cd59a97bbddcebd05e902559e0&language=en-US&sort_by=$sort&include_adult=false&include_video=false&vote_average.gte=$min_r&vote_average.lte=$max_r&certification_country=IN&certification=$rated&primary_release_date.lte=$max_y&page=$page";
    }
    else{
      $url = "https://api.themoviedb.org/3/discover/movie?api_key=f0dc06cd59a97bbddcebd05e902559e0&language=en-US&sort_by=$sort&include_adult=false&include_video=false&vote_average.gte=$min_r&vote_average.lte=$max_r&primary_release_date.lte=$max_y&page=$page";
    }
  }
  else{
    if($genre!=null && $rated!=null){
      $url = "https://api.themoviedb.org/3/discover/movie?api_key=f0dc06cd59a97bbddcebd05e902559e0&amp;region=IN&language=hi-IN&with_original_language=hi&sort_by=$sort&include_adult=false&include_video=false&with_genres=$genre&vote_average.gte=$min_r&vote_average.lte=$max_r&certification_country=IN&certification=$rated&primary_release_date.lte=$max_y&page=$page";
    }
    elseif($genre!=null){
      $url = "https://api.themoviedb.org/3/discover/movie?api_key=f0dc06cd59a97bbddcebd05e902559e0&amp;region=IN&language=hi-IN&with_original_language=hi&sort_by=$sort&include_adult=false&include_video=false&with_genres=$genre&vote_average.gte=$min_r&vote_average.lte=$max_r&primary_release_date.lte=$max_y&page=$page";
    }
    elseif($rated!=null){
      $url = "https://api.themoviedb.org/3/discover/movie?api_key=f0dc06cd59a97bbddcebd05e902559e0&amp;region=IN&language=hi-IN&with_original_language=hi&sort_by=$sort&include_adult=false&include_video=false&vote_average.gte=$min_r&vote_average.lte=$max_r&certification_country=IN&certification=$rated&primary_release_date.lte=$max_y&page=$page";
    }
    else{
      $url = "https://api.themoviedb.org/3/discover/movie?api_key=f0dc06cd59a97bbddcebd05e902559e0&amp;region=IN&language=hi-IN&with_original_language=hi&sort_by=$sort&include_adult=false&include_video=false&vote_average.gte=$min_r&vote_average.lte=$max_r&primary_release_date.lte=$max_y&page=$page";
    }
  }
  return $url;
}

public function filterTrendingTv($genre,$rated,$min_r,$max_r,$max_y,$sort,$page,$lang){
  $url ="";

  if($lang == 'Holywood'){
    if($genre!=null && $rated!=null){
      $url = "https://api.themoviedb.org/3/discover/tv?api_key=f0dc06cd59a97bbddcebd05e902559e0&language=en-US&sort_by=$sort&include_adult=false&include_video=false&with_genres=$genre&vote_average.gte=$min_r&vote_average.lte=$max_r&certification_country=IN&certification=$rated&first_air_date.lte=$max_y&page=$page";
    }
    elseif($rated!=null){
      $url = "https://api.themoviedb.org/3/discover/tv?api_key=f0dc06cd59a97bbddcebd05e902559e0&language=en-US&sort_by=$sort&include_adult=false&include_video=false&vote_average.gte=$min_r&vote_average.lte=$max_r&certification_country=IN&certification=$rated&first_air_date.lte=$max_y&page=$page";
    }
    elseif($genre!=null){
      $url = "https://api.themoviedb.org/3/discover/tv?api_key=f0dc06cd59a97bbddcebd05e902559e0&language=en-US&sort_by=$sort&include_adult=false&include_video=false&with_genres=$genre&vote_average.gte=$min_r&vote_average.lte=$max_r&first_air_date.lte=$max_y&page=$page";
    }
    else{
      $url = "https://api.themoviedb.org/3/discover/tv?api_key=f0dc06cd59a97bbddcebd05e902559e0&language=en-US&sort_by=$sort&include_adult=false&include_video=false&vote_average.gte=$min_r&vote_average.lte=$max_r&first_air_date.lte=$max_y&page=$page";
    }
  }
  else{
    if($genre!=null && $rated!=null){
      $url = "https://api.themoviedb.org/3/discover/tv?api_key=f0dc06cd59a97bbddcebd05e902559e0&amp;region=IN&language=hi-IN&with_original_language=hi&sort_by=$sort&include_adult=false&include_video=false&with_genres=$genre&vote_average.gte=$min_r&vote_average.lte=$max_r&certification_country=IN&certification=$rated&first_air_date.lte=$max_y&page=$page";
    }
    elseif($rated!=null){
      $url = "https://api.themoviedb.org/3/discover/tv?api_key=f0dc06cd59a97bbddcebd05e902559e0&amp;region=IN&language=hi-IN&with_original_language=hi&sort_by=$sort&include_adult=false&include_video=false&vote_average.gte=$min_r&vote_average.lte=$max_r&certification_country=IN&certification=$rated&first_air_date.lte=$max_y&page=$page";
    }
    elseif($genre!=null){
      $url = "https://api.themoviedb.org/3/discover/tv?api_key=f0dc06cd59a97bbddcebd05e902559e0&amp;region=IN&language=hi-IN&with_original_language=hi&sort_by=$sort&include_adult=false&include_video=false&with_genres=$genre&vote_average.gte=$min_r&vote_average.lte=$max_r&first_air_date.lte=$max_y&page=$page";
    }
    else{
      $url = "https://api.themoviedb.org/3/discover/tv?api_key=f0dc06cd59a97bbddcebd05e902559e0&amp;region=IN&language=hi-IN&with_original_language=hi&sort_by=$sort&include_adult=false&include_video=false&vote_average.gte=$min_r&vote_average.lte=$max_r&first_air_date.lte=$max_y&page=$page";
    }
  }
  return $url;
}


//Filter for New Movies
public function filterNewMovie($genre,$rated,$min_r,$max_r,$min_y,$max_y,$sort,$page,$lang){
  $url ="";

  if($lang == 'Holywood'){
    if($genre!=null && $rated!=null){
      $url = "https://api.themoviedb.org/3/discover/movie?api_key=f0dc06cd59a97bbddcebd05e902559e0&language=en-US&sort_by=$sort&include_adult=false&include_video=false&with_genres=$genre&vote_average.gte=$min_r&vote_average.lte=$max_r&certification_country=IN&certification=$rated&primary_release_date.lte=$max_y&primary_release_date.gte=$min_y&page=$page";
    }
    elseif($genre!=null){
      $url = "https://api.themoviedb.org/3/discover/movie?api_key=f0dc06cd59a97bbddcebd05e902559e0&language=en-US&sort_by=$sort&include_adult=false&include_video=false&with_genres=$genre&vote_average.gte=$min_r&vote_average.lte=$max_r&primary_release_date.lte=$max_y&primary_release_date.gte=$min_y&page=$page";
    }
    elseif($rated!=null){
      $url = "https://api.themoviedb.org/3/discover/movie?api_key=f0dc06cd59a97bbddcebd05e902559e0&language=en-US&sort_by=$sort&include_adult=false&include_video=false&vote_average.gte=$min_r&vote_average.lte=$max_r&certification_country=IN&certification=$rated&primary_release_date.lte=$max_y&primary_release_date.gte=$min_y&page=$page";
    }
    else{
      $url = "https://api.themoviedb.org/3/discover/movie?api_key=f0dc06cd59a97bbddcebd05e902559e0&language=en-US&sort_by=$sort&include_adult=false&include_video=false&vote_average.gte=$min_r&vote_average.lte=$max_r&primary_release_date.lte=$max_y&primary_release_date.gte=$min_y&page=$page";
    }
  }
  else{
    if($genre!=null && $rated!=null){
      $url = "https://api.themoviedb.org/3/discover/movie?api_key=f0dc06cd59a97bbddcebd05e902559e0&amp;region=IN&language=hi-IN&with_original_language=hi&sort_by=$sort&include_adult=false&include_video=false&with_genres=$genre&vote_average.gte=$min_r&vote_average.lte=$max_r&certification_country=IN&certification=$rated&primary_release_date.lte=$max_y&primary_release_date.gte=$min_y&page=$page";
    }
    elseif($genre!=null){
      $url = "https://api.themoviedb.org/3/discover/movie?api_key=f0dc06cd59a97bbddcebd05e902559e0&amp;region=IN&language=hi-IN&with_original_language=hi&sort_by=$sort&include_adult=false&include_video=false&with_genres=$genre&vote_average.gte=$min_r&vote_average.lte=$max_r&primary_release_date.lte=$max_y&primary_release_date.gte=$min_y&page=$page";
    }
    elseif($rated!=null){
      $url = "https://api.themoviedb.org/3/discover/movie?api_key=f0dc06cd59a97bbddcebd05e902559e0&amp;region=IN&language=hi-IN&with_original_language=hi&sort_by=$sort&include_adult=false&include_video=false&vote_average.gte=$min_r&vote_average.lte=$max_r&certification_country=IN&certification=$rated&primary_release_date.lte=$max_y&primary_release_date.gte=$min_y&page=$page";
    }
    else{
      $url = "https://api.themoviedb.org/3/discover/movie?api_key=f0dc06cd59a97bbddcebd05e902559e0&amp;region=IN&language=hi-IN&with_original_language=hi&sort_by=$sort&include_adult=false&include_video=false&vote_average.gte=$min_r&vote_average.lte=$max_r&primary_release_date.lte=$max_y&primary_release_date.gte=$min_y&page=$page";
    }
  }
  return $url;
}

public function filterNewTv($genre,$rated,$min_r,$max_r,$min_y,$max_y,$sort,$page,$lang){
  $url ="";

  if($lang == 'Holywood'){
    if($genre!=null && $rated!=null){
      $url = "https://api.themoviedb.org/3/discover/tv?api_key=f0dc06cd59a97bbddcebd05e902559e0&language=en-US&sort_by=$sort&include_adult=false&include_video=false&with_genres=$genre&vote_average.gte=$min_r&vote_average.lte=$max_r&certification_country=IN&certification=$rated&first_air_date.gte=$min_y&first_air_date.lte=$max_y&page=$page";
    }
    elseif($rated!=null){
      $url = "https://api.themoviedb.org/3/discover/tv?api_key=f0dc06cd59a97bbddcebd05e902559e0&language=en-US&sort_by=$sort&include_adult=false&include_video=false&vote_average.gte=$min_r&vote_average.lte=$max_r&certification_country=IN&certification=$rated&first_air_date.gte=$min_y&first_air_date.lte=$max_y&page=$page";
    }
    elseif($genre!=null){
      $url = "https://api.themoviedb.org/3/discover/tv?api_key=f0dc06cd59a97bbddcebd05e902559e0&language=en-US&sort_by=$sort&include_adult=false&include_video=false&with_genres=$genre&vote_average.gte=$min_r&vote_average.lte=$max_r&first_air_date.gte=$min_y&first_air_date.lte=$max_y&page=$page";
    }
    else{
      $url = "https://api.themoviedb.org/3/discover/tv?api_key=f0dc06cd59a97bbddcebd05e902559e0&language=en-US&sort_by=$sort&include_adult=false&include_video=false&vote_average.gte=$min_r&vote_average.lte=$max_r&first_air_date.gte=$min_y&first_air_date.lte=$max_y&page=$page";
    }
  }
  else{
    if($genre!=null && $rated!=null){
      $url = "https://api.themoviedb.org/3/discover/tv?api_key=f0dc06cd59a97bbddcebd05e902559e0&amp;region=IN&language=hi-IN&with_original_language=hi&sort_by=$sort&include_adult=false&include_video=false&with_genres=$genre&vote_average.gte=$min_r&vote_average.lte=$max_r&certification_country=IN&certification=$rated&first_air_date.gte=$min_y&first_air_date.lte=$max_y&page=$page";
    }
    elseif($rated!=null){
      $url = "https://api.themoviedb.org/3/discover/tv?api_key=f0dc06cd59a97bbddcebd05e902559e0&amp;region=IN&language=hi-IN&with_original_language=hi&sort_by=$sort&include_adult=false&include_video=false&vote_average.gte=$min_r&vote_average.lte=$max_r&certification_country=IN&certification=$rated&first_air_date.gte=$min_y&first_air_date.lte=$max_y&page=$page";
    }
    elseif($genre!=null){
      $url = "https://api.themoviedb.org/3/discover/tv?api_key=f0dc06cd59a97bbddcebd05e902559e0&amp;region=IN&language=hi-IN&with_original_language=hi&sort_by=$sort&include_adult=false&include_video=false&with_genres=$genre&vote_average.gte=$min_r&vote_average.lte=$max_r&first_air_date.gte=$min_y&first_air_date.lte=$max_y&page=$page";
    }
    else{
      $url = "https://api.themoviedb.org/3/discover/tv?api_key=f0dc06cd59a97bbddcebd05e902559e0&amp;region=IN&language=hi-IN&with_original_language=hi&sort_by=$sort&include_adult=false&include_video=false&vote_average.gte=$min_r&vote_average.lte=$max_r&first_air_date.gte=$min_y&first_air_date.lte=$max_y&page=$page";
    }
  }
  return $url;
}

public function getDynamicSearch(Request $request)
{       
  if (isset($request->page_id) && $request->page_id >1){
    $page = $request->page_id;
  }   
  else
    $page = 1;
  $keyword = $request->data;
  $search = $request->search;    

  $total_pages = 0;

  $check_cast = Cast::where('name','LIKE','%'.$keyword.'%')->get();

  $countries = Country::all();

  $url = "https://api.themoviedb.org/3/search/$search?api_key=f0dc06cd59a97bbddcebd05e902559e0&language=en-US&sort_by=popularity.desc&include_adult=false&query=$keyword";

  $genre_url = "https://api.themoviedb.org/3/genre/movie/list?api_key=f0dc06cd59a97bbddcebd05e902559e0&language=en-US";
  $c = curl_init();
  curl_setopt($c,CURLOPT_RETURNTRANSFER,1);
  curl_setopt($c, CURLOPT_URL, $url);
  curl_setopt($c, CURLOPT_SSL_VERIFYHOST, 0);
  curl_setopt($c, CURLOPT_SSL_VERIFYPEER, 0);
  $contents = curl_exec($c);
  $genrerequest = curl_init();
  curl_setopt($genrerequest,CURLOPT_RETURNTRANSFER,1);
  curl_setopt($genrerequest, CURLOPT_SSL_VERIFYHOST, 0);
  curl_setopt($genrerequest, CURLOPT_SSL_VERIFYPEER, 0);
  curl_setopt($genrerequest, CURLOPT_URL, $genre_url);
  $genres = curl_exec($genrerequest);

  $contents = json_decode($contents);
  $genres = json_decode($genres);
  $genres = $genres->genres;

  // ==============Justwatch==============

  $justwatch_url = "https://apis.justwatch.com/content/titles/en_IN/popular?body=%7B%22page_size%22:5,%22page%22:1,%22query%22:%22".$keyword."%22,%22content_types%22:[%22show%22,%22movie%22]%7D";

  $c = curl_init();
  curl_setopt($c,CURLOPT_RETURNTRANSFER,1);
  curl_setopt($c, CURLOPT_URL, $justwatch_url);
  curl_setopt($c, CURLOPT_SSL_VERIFYHOST, 0);
  curl_setopt($c, CURLOPT_SSL_VERIFYPEER, 0);
  $justwatch = curl_exec($c);
  
  $justwatch = json_decode($justwatch);

  foreach ($justwatch->items as $key => $value) {

    $db_content = DB::table('contents')->where('content_source_id',$value->id)->first();
        //If Not data available for this movie in contents table
    if(isset($db_content)){
      $content_id = $db_content->content_id;
    }
    else{
     $db_data = DB::table('contents')->insert(
      array(
        'title' => $value->title,
        'content_type' => 'movie',
        'poster' => $value->poster ?? '',
        'content_source' => 'justwatch',
        'content_source_id' => $value->id
      ));
     if (isset($value->offers)){
      $providerlist = $value->offers;
      foreach ($providerlist as $key2 => $value2) {

        $db_data = DB::table('content_providers')->insert(
          array(
            'content_id' => $value->id,
            'provider_id' => $value2->provider_id ,
            'video_quality' => $value2->presentation_type ?? '',
            'monetization_type' => $value2->monetization_type ?? '',
            'retail_price' => $value2->retail_price ?? '',
            'web_url' => $value2->urls->standard_web ?? ""
          ));

      }
    }
  }
          // $db_content = DB::table('contents')->where('title',$value->title)->first();
}
  // ==============Justwatch==============




if($search == 'movie')
  $type="Movies";
elseif($search == 'tv')
  $type="TV Serials";
else
  $type="Web Series";

if(!empty($contents) && !empty($contents->total_pages))
  $total_pages = $contents->total_pages;    

return view('frontnew.dynamic-search', compact('contents', 'genres', 'keyword', 'countries', 'type','total_pages','check_cast'));
}

}