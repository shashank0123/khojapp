<?php 
							$rate = floor($movie->vote_average);
							?>
@extends('layouts.new_front')
<meta name="csrf-token" content="{{ csrf_token() }}">

@section('content')


<style>
iframe {
 width:100%; height:480px;
}
</style>
<div class="page-single movie-single movie_single">
	<div class="container-fluid">
		<div class="row ipad-width2">
			<div class="col-md-12 col-sm-12 col-xs-12">
				@if($errors->any())
          <div class="alert alert-danger" style="position: absolute; top: 2px">
            @foreach($errors->all() as $error)
            <li style="color: #fff">{{ $error }}</li>
            @endforeach
          </div>
          @endif

          @if($message = Session::get('message'))
          <div class="btn btn-success" style="width: 100%; position: absolute; top: 2px">
            <p style="color: #fff">{{ $message }}</p>
          </div>
          @endif
				<div class="movie-img ">
              {{-- <iframe width="100%" height="315" src="https://www.youtube.com/embed/mX_vuEm6t3I?start=29" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> --}}
              <iframe width="100%" height="315" src="https://www.youtube.com/embed/@if(isset($trailorslist[0])){{$trailorslist[0]}}@endif" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-2 col-sm-12 col-xs-12"></div>
			<div class="col-md-8 col-sm-12 col-xs-12">
				<div class="movie-single-ct main-content">
					<h1 class="bd-hd">{{$movie->name}} <span>
						<?php
						$release_date = explode('-',$movie->release_date);
						?>{{$release_date[0]}}</span></h1>
					<div class="social-btn">
						@if ($favourite)
						<a href="#" class="parent-btn"><i class="ion-heart"></i> Add to Favorite</a>
						@else
						<a href="/favourite/{{'Series'}}/{{$movie->movie_id}}" class="parent-btn"><i class="ion-heart"></i> Add to Favorite</a>
						@endif

						@if ($watchlist)						
						<a href="#" class="parent-btn"><i class="ion-eye"></i> Add to WatchList</a>
						@else
						<a href="/watchlist/{{'Series'}}/{{$movie->movie_id}}" class="parent-btn"><i class="ion-eye"></i> Add to WatchList</a>
						@endif

						@if ($playlist)						
						<a href="#" class="parent-btn"><i class="ion-play"></i> Add to PlayList</a>
						@else
						<a href="/playlist/{{'Series'}}/{{$movie->movie_id}}" class="parent-btn"><i class="ion-play"></i> Add to PlayList</a>
						@endif
						<div class="hover-bnt">
							<a href="#" class="parent-btn"><i class="ion-android-share-alt"></i>share</a>
							<div class="hvr-item">
								<a href="#" class="hvr-grow"><i class="ion-social-facebook"></i></a>
								<a href="#" class="hvr-grow"><i class="ion-social-twitter"></i></a>
								<a href="#" class="hvr-grow"><i class="ion-social-googleplus"></i></a>
								<a href="#" class="hvr-grow"><i class="ion-social-youtube"></i></a>
							</div>
						</div>		
					</div>
					<div class="movie-rate">
						<div class="rate">
							<i class="ion-android-star"></i>
							<p><span>{{$rate}}</span> /10<br>
								<span class="rv"><?php echo count($reviewslist); ?> @if(count($reviewslist) < 2){{'Review'}}@else{{'Reviews'}}@endif</span>
							</p>
						</div>
						<div class="rate-star">
							<p>Rate This Movie:  </p>
							
							@for($i=0;$i<10;$i++)
							@if($i<$rate)
							<i class="ion-ios-star"></i>
							@else
							<i class="ion-ios-star-outline"></i>
							@endif
							@endfor							
						</div>
					</div>
					<div class="movie-tabs">
						<div class="tabs">
							<ul class="tab-links tabs-mv">
								<li class="active"><a href="#overview">Overview</a></li>
								<li><a href="#reviews"> Reviews</a></li>
								<li><a href="#cast">  Cast & Crew </a></li>
								{{-- <li><a href="#media"> Media</a></li>  --}}
								<li><a href="#moviesrelated"> Related Movies</a></li>                        
							</ul>
						    <div class="tab-content">
						        <div id="overview" class="tab active">
						            <div class="row">
						            	<div class="col-md-8 col-sm-12 col-xs-12">
						            		<p>{{$movie->overview}}</p>
						            		<div class="title-hd-sm">
												<h4>Videos & Photos</h4>
												<a href="#" class="time">All 5 Videos & 245 Photos <i class="ion-ios-arrow-right"></i></a>
											</div>
											<div class="mvsingle-item ov-item">
												<a class="img-lightbox"  data-fancybox-group="gallery" href="/asset/images/uploads/image11.jpg" ><img src="/asset/images/uploads/image1.jpg" alt=""></a>
												<a class="img-lightbox"  data-fancybox-group="gallery" href="/asset/images/uploads/image21.jpg" ><img src="/asset/images/uploads/image2.jpg" alt=""></a>
												<a class="img-lightbox"  data-fancybox-group="gallery" href="/asset/images/uploads/image31.jpg" ><img src="/asset/images/uploads/image3.jpg" alt=""></a>
												<div class="vd-it">
													<img class="vd-img" src="/asset/images/uploads/image4.jpg" alt="">
													<a class="fancybox-media hvr-grow" href="https://www.youtube.com/embed/o-0hcF97wy0"><img src="/asset/images/uploads/play-vd.png" alt=""></a>
												</div>
											</div>
											<div class="title-hd-sm">
												<h4>cast</h4>
												<a href="#" class="time">Full Cast & Crew  <i class="ion-ios-arrow-right"></i></a>
											</div>
											<!-- movie cast -->
											<div class="mvcast-item">							
											@foreach ($casts as $cast)
						@if (!$cast->profile_path)
							<?php continue;?>{{-- expr --}}
						@endif				
												<div class="cast-it">
													<div class="cast-left">
														<img src="https://image.tmdb.org/t/p/w300{{$cast->profile_path}}" alt="" style="width:40px; height:40px">
														<a href="#">{{$cast->name}}</a>
													</div>
													<p>... <?php if (isset($cast->job)){?>{{$cast->job}}<?php } else { ?>{{$cast->character}}<?php } ?></p>
												</div>
												@endforeach
						
												
											</div>
											<div class="title-hd-sm">
												<h4>User reviews</h4>
												<a href="#" class="time">See All <?php echo count($reviewslist); ?> Reviews <i class="ion-ios-arrow-right"></i></a>
											</div>
											<!-- movie user review -->
											@if(!empty($reviewslist))
										@foreach ($reviewslist as $review)

											<div class="mv-user-review-item">
												<h3>{{ ucfirst($review->author)}}</h3>
												{{-- <div class="no-star">
													<i class="ion-android-star"></i>
													<i class="ion-android-star"></i>
													<i class="ion-android-star"></i>
													<i class="ion-android-star"></i>
													<i class="ion-android-star"></i>
													<i class="ion-android-star"></i>
													<i class="ion-android-star"></i>
													<i class="ion-android-star"></i>
													<i class="ion-android-star"></i>
													<i class="ion-android-star last"></i>
												</div>
												<p class="time">
													17 December 2016 by <a href="#"> hawaiipierson</a>
												</p> --}}
												<p>{{ (strlen($review->content) > 112) ? substr($review->content,0,112).'...' : $review->content}}</p>
											</div>
											@endforeach
											@endif
						            	</div>
						            	<div class="col-md-4 col-xs-12 col-sm-12">
						            		<div class="sb-it">
						            			<h6>Director: </h6>

						            			@foreach ($directors as $director)

						            			<p><a href="#"><?php if (isset($director->name)){?>{{$director->name}}<?php } else { ?>{{$director}}<?php } ?></a></p>
					
				@endforeach

						            			
						            		</div>
						            		<div class="sb-it">
						            			<h6>Writer: </h6>
						            			@foreach ($writers as $writer)
						            			<p><a href="#"><?php if (isset($director->name)){?>{{$writer->name}}<?php } else { ?>{{$writer}}<?php } ?></a> </p>
						            			@endforeach
						            		</div>
						            		<div class="sb-it">
						            			<h6>Stars: </h6>
						            			<p><a href="#">Robert Downey Jr,</a> <a href="#">Chris Evans,</a> <a href="#">Mark Ruffalo,</a><a href="#"> Scarlett Johansson</a></p>
						            		</div>
						            		<div class="sb-it">
						            			<h6>Genres:</h6>
						            			<p><a href="#">Action, </a> <a href="#"> Sci-Fi,</a> <a href="#">Adventure</a></p>
						            		</div>
						            		<div class="sb-it">
						            			<h6>Release Date:</h6>
						            			<p>{{Carbon\Carbon::parse($movie->release_date)->format('d M Y')}}</p>
						            		</div>
						            		<div class="sb-it">
						            			<h6>Run Time:</h6>
						            			<p>{{$movie->runtime}} mins</p>
						            		</div>
						            		<div class="sb-it">
						            			<h6>MMPA Rating:</h6>
						            			<p>PG-13</p>
						            		</div>
						            		{{-- <div class="sb-it">
						            			<h6>Plot Keywords:</h6>
						            			<p class="tags">
						            				<span class="time"><a href="#">superhero</a></span>
													<span class="time"><a href="#">marvel universe</a></span>
													<span class="time"><a href="#">comic</a></span>
													<span class="time"><a href="#">blockbuster</a></span>
													<span class="time"><a href="#">final battle</a></span>
						            			</p>
						            		</div> --}}
						            		<div class="ads">
						            			@if(isset($movie->poster_path))
												<img src="https://image.tmdb.org/t/p/w300{{$movie->poster_path}}" alt="">
												@else
												<img src="/images/default.png" alt="">
												@endif
											</div>
						            	</div>
						            </div>
						        </div>
						        <div id="reviews" class="tab review">
						           <div class="row">
						            	<div class="rv-hd">
						            		<div class="div">
							            		<h3>Related Movies To</h3>
						       	 				<h2>{{$movie->name}}</h2>
							            	</div>
							            	<a href="#" class="redbtn">Write Review</a>
						            	</div>
						            	<div class="topbar-filter">
											<p>Found <span><?php echo count($reviewslist); ?> reviews</span> in total</p>
											<label>Filter by:</label>
											<select>
												<option value="popularity">Popularity Descending</option>
												<option value="popularity">Popularity Ascending</option>
												<option value="rating">Rating Descending</option>
												<option value="rating">Rating Ascending</option>
												<option value="date">Release date Descending</option>
												<option value="date">Release date Ascending</option>
											</select>
										</div>

										@if(!empty($reviewslist))
										@foreach ($reviewslist as $review)

										<div class="mv-user-review-item">
											<div class="user-infor">
												<img src="/asset/images/uploads/userava1.jpg" alt="">
												<div>
													<h3>{{ $review->author}}</h3>
													{{-- <div class="no-star">
														<i class="ion-android-star"></i>
														<i class="ion-android-star"></i>
														<i class="ion-android-star"></i>
														<i class="ion-android-star"></i>
														<i class="ion-android-star"></i>
														<i class="ion-android-star"></i>
														<i class="ion-android-star"></i>
														<i class="ion-android-star"></i>
														<i class="ion-android-star"></i>
														<i class="ion-android-star last"></i>
													</div>
													<p class="time">
														17 December 2016 by <a href="#"> hawaiipierson</a>
													</p> --}}
												</div>
											</div>
											<p>{{ $review->content }}</p>
										</div>

										@endforeach	
										@endif								
										
										
										
										<div class="topbar-filter">
											<label>Reviews per page:</label>
											<select>
												<option value="range">5 Reviews</option>
												<option value="saab">10 Reviews</option>
											</select>
											<div class="pagination2">
												<span>Page 1 of 6:</span>
												<a class="active" href="#">1</a>
												<a href="#">2</a>
												<a href="#">3</a>
												<a href="#">4</a>
												<a href="#">5</a>
												<a href="#">6</a>
												<a href="#"><i class="ion-arrow-right-b"></i></a>
											</div>
										</div>
						            </div>
						        </div>
						        <div id="cast" class="tab">
						        	<div class="row">
						            	<h3>Cast & Crew of</h3>
					       	 			<h2>{{$movie->name}}</h2>
										<!-- //== -->
					       	 			{{-- <div class="title-hd-sm">
											<h4>Directors, Writers & Producers</h4>
										</div>
										<div class="mvcast-item">													
											
											<div class="cast-it">
												<div class="cast-left">
													<h4></h4>
													<a href="#"></a>
												</div>
												<p>...  Director</p>
											</div>
											
											
										</div> --}}
										<!-- //== -->
										{{-- <div class="title-hd-sm">
											<h4>Directors & Credit Writers</h4>
										</div>
										<div class="mvcast-item">											
											<div class="cast-it">
												<div class="cast-left">
													<h4>SL</h4>
													<a href="#">Stan Lee</a>
												</div>
												<p>...  (based on Marvel comics)</p>
											</div>
											<div class="cast-it">
												<div class="cast-left">
													<h4>JK</h4>
													<a href="#">Jack Kirby</a>
												</div>
												<p>...  (based on Marvel comics)</p>
											</div>
											<div class="cast-it">
												<div class="cast-left">
													<h4>JS</h4>
													<a href="#">Joe Simon</a>
												</div>
												<p>...  (character created by: Captain America)</p>
											</div>
											<div class="cast-it">
												<div class="cast-left">
													<h4>JS</h4>
													<a href="#">Joe Simon</a>
												</div>
												<p>...  (character created by: Thanos)</p>
											</div>
											<div class="cast-it">
												<div class="cast-left">
													<h4>RT</h4>
													<a href="#">Roy Thomas</a>
												</div>
												<p>...  (character created by: Ultron, Vision)</p>
											</div>
											<div class="cast-it">
												<div class="cast-left">
													<h4>JB</h4>
													<a href="#">John Buscema</a>
												</div>
												<p>...  (character created by: Ultron, Vision)</p>
											</div>
										</div> --}}
										<!-- //== -->
										<div class="title-hd-sm">
											<h4>Cast</h4>
										</div>
										<div class="mvcast-item">											@foreach ($casts as $cast)
						@if (!$cast->profile_path)
							<?php continue;?>{{-- expr --}}
						@endif				
											<div class="cast-it">
												<div class="cast-left">
													<img src="https://image.tmdb.org/t/p/w300{{$cast->profile_path}}" alt="" style="width: 40px; height: 40px">
													<a href="#">{{$cast->name}}.</a>
												</div>
												<p>...  <?php if (isset($cast->job)){?>{{$cast->job}}<?php } else { ?>{{$cast->character}}<?php } ?>.</p>
											</div>
											@endforeach

											
										</div>
										<!-- //== -->
										{{-- <div class="title-hd-sm">
											<h4>Produced by</h4>
										</div>
										<div class="mvcast-item">											
											<div class="cast-it">
												<div class="cast-left">
													<h4>VA</h4>
													<a href="#">Victoria Alonso</a>
												</div>
												<p>...  executive producer</p>
											</div>
											<div class="cast-it">
												<div class="cast-left">
													<h4>MB</h4>
													<a href="#">Mitchel Bell</a>
												</div>
												<p>...  co-producer (as Mitch Bell)</p>
											</div>
											<div class="cast-it">
												<div class="cast-left">
													<h4>JC</h4>
													<a href="#">Jamie Christopher</a>
												</div>
												<p>...  associate producer</p>
											</div>
											<div class="cast-it">
												<div class="cast-left">
													<h4>LD</h4>
													<a href="#">Louis D’Esposito</a>
												</div>
												<p>...  executive producer</p>
											</div>
											<div class="cast-it">
												<div class="cast-left">
													<h4>JF</h4>
													<a href="#">Jon Favreau</a>
												</div>
												<p>...  executive producer</p>
											</div>
											<div class="cast-it">
												<div class="cast-left">
													<h4>KF</h4>
													<a href="#">Kevin Feige</a>
												</div>
												<p>...  producer</p>
											</div>
											<div class="cast-it">
												<div class="cast-left">
													<h4>AF</h4>
													<a href="#">Alan Fine</a>
												</div>
												<p>...  executive producer</p>
											</div>
											<div class="cast-it">
												<div class="cast-left">
													<h4>JF</h4>
													<a href="#">Jeffrey Ford</a>
												</div>
												<p>...  associate producer</p>
											</div>
										</div> --}}
						            </div>
					       	 	</div>
					       	 	<div id="media" class="tab">
						        	<div class="row">
						        		<div class="rv-hd">
						            		<div>
						            			<h3>Videos & Photos of</h3>
					       	 					<h2>{{$movie->name}}</h2>
						            		</div>
						            	</div>
						            	<div class="title-hd-sm">
											<h4>Videos <span>(8)</span></h4>
										</div>
										<div class="mvsingle-item media-item">
											<div class="vd-item">
												<div class="vd-it">
													<img class="vd-img" src="/asset/images/uploads/vd-item1.jpg" alt="">
													<a class="fancybox-media hvr-grow"  href="https://www.youtube.com/embed/o-0hcF97wy0"><img src="/asset/images/uploads/play-vd.png" alt=""></a>
												</div>
												<div class="vd-infor">
													<h6> <a href="#">Trailer:  Watch New Scenes</a></h6>
													<p class="time"> 1: 31</p>
												</div>
											</div>
											<div class="vd-item">
												<div class="vd-it">
													<img class="vd-img" src="/asset/images/uploads/vd-item2.jpg" alt="">
													<a class="fancybox-media hvr-grow" href="https://www.youtube.com/embed/o-0hcF97wy0"><img src="/asset/images/uploads/play-vd.png" alt=""></a>
												</div>
												<div class="vd-infor">
													<h6> <a href="#">Featurette: “Avengers Re-Assembled</a></h6>
													<p class="time"> 1: 03</p>
												</div>
											</div>
											<div class="vd-item">
												<div class="vd-it">
													<img class="vd-img" src="/asset/images/uploads/vd-item3.jpg" alt="">
													<a class="fancybox-media hvr-grow" href="https://www.youtube.com/embed/o-0hcF97wy0"><img src="/asset/images/uploads/play-vd.png" alt=""></a>
												</div>
												<div class="vd-infor">
													<h6> <a href="#">Interview: Robert Downey Jr</a></h6>
													<p class="time"> 3:27</p>
												</div>
											</div>
											<div class="vd-item">
												<div class="vd-it">
													<img class="vd-img" src="/asset/images/uploads/vd-item4.jpg" alt="">
													<a class="fancybox-media hvr-grow" href="https://www.youtube.com/embed/o-0hcF97wy0"><img src="/asset/images/uploads/play-vd.png" alt=""></a>
												</div>
												<div class="vd-infor">
													<h6> <a href="#">Interview: Scarlett Johansson</a></h6>
													<p class="time"> 3:27</p>
												</div>
											</div>
											<div class="vd-item">
												<div class="vd-it">
													<img class="vd-img" src="/asset/images/uploads/vd-item1.jpg" alt="">
													<a class="fancybox-media hvr-grow" href="https://www.youtube.com/embed/o-0hcF97wy0"><img src="/asset/images/uploads/play-vd.png" alt=""></a>
												</div>
												<div class="vd-infor">
													<h6> <a href="#">Featurette: Meet Quicksilver & The Scarlet Witch</a></h6>
													<p class="time"> 1: 31</p>
												</div>
											</div>
											<div class="vd-item">
												<div class="vd-it">
													<img class="vd-img" src="/asset/images/uploads/vd-item2.jpg" alt="">
													<a class="fancybox-media hvr-grow" href="https://www.youtube.com/embed/o-0hcF97wy0"><img src="/asset/images/uploads/play-vd.png" alt=""></a>
												</div>
												<div class="vd-infor">
													<h6> <a href="#">Interview: Director Joss Whedon</a></h6>
													<p class="time"> 1: 03</p>
												</div>
											</div>
											<div class="vd-item">
												<div class="vd-it">
													<img class="vd-img" src="/asset/images/uploads/vd-item3.jpg" alt="">
													<a class="fancybox-media hvr-grow" href="https://www.youtube.com/embed/o-0hcF97wy0"><img src="/asset/images/uploads/play-vd.png" alt=""></a>
												</div>
												<div class="vd-infor">
													<h6> <a href="#">Interview: Mark Ruffalo</a></h6>
													<p class="time"> 3:27</p>
												</div>
											</div>
											<div class="vd-item">
												<div class="vd-it">
													<img class="vd-img" src="/asset/images/uploads/vd-item4.jpg" alt="">
													<a class="fancybox-media hvr-grow" href="https://www.youtube.com/embed/o-0hcF97wy0"><img src="/asset/images/uploads/play-vd.png" alt=""></a>
												</div>
												<div class="vd-infor">
													<h6> <a href="#">Official Trailer #2</a></h6>
													<p class="time"> 3:27</p>
												</div>
											</div>
										</div>
										<div class="title-hd-sm">
											<h4>Photos <span> (21)</span></h4>
										</div>
										<div class="mvsingle-item">
											<a class="img-lightbox"  data-fancybox-group="gallery" href="/asset/images/uploads/image11.jpg" ><img src="/asset/images/uploads/image1.jpg" alt=""></a>
											<a class="img-lightbox"  data-fancybox-group="gallery"  href="/asset/images/uploads/image21.jpg" ><img src="/asset/images/uploads/image2.jpg" alt=""></a>
											<a class="img-lightbox"  data-fancybox-group="gallery" href="/asset/images/uploads/image31.jpg" ><img src="/asset/images/uploads/image3.jpg" alt=""></a>
											<a class="img-lightbox"  data-fancybox-group="gallery" href="/asset/images/uploads/image41.jpg" ><img src="/asset/images/uploads/image4.jpg" alt=""></a>
											<a class="img-lightbox"  data-fancybox-group="gallery" href="/asset/images/uploads/image51.jpg" ><img src="/asset/images/uploads/image5.jpg" alt=""></a>
											<a class="img-lightbox"  data-fancybox-group="gallery" href="/asset/images/uploads/image61.jpg" ><img src="/asset/images/uploads/image6.jpg" alt=""></a>
											<a class="img-lightbox"  data-fancybox-group="gallery" href="/asset/images/uploads/image71.jpg" ><img src="/asset/images/uploads/image7.jpg" alt=""></a>
											<a class="img-lightbox"  data-fancybox-group="gallery" href="/asset/images/uploads/image81.jpg" ><img src="/asset/images/uploads/image8.jpg" alt=""></a>
											<a class="img-lightbox"  data-fancybox-group="gallery" href="/asset/images/uploads/image91.jpg" ><img src="/asset/images/uploads/image9.jpg" alt=""></a>
											<a class="img-lightbox"  data-fancybox-group="gallery" href="/asset/images/uploads/image101.jpg" ><img src="/asset/images/uploads/image10.jpg" alt=""></a>
											<a class="img-lightbox"  data-fancybox-group="gallery" href="/asset/images/uploads/image111.jpg" ><img src="/asset/images/uploads/image1-1.jpg" alt=""></a>
											<a class="img-lightbox"  data-fancybox-group="gallery" href="/asset/images/uploads/image121.jpg" ><img src="/asset/images/uploads/image12.jpg" alt=""></a>
											<a class="img-lightbox"  data-fancybox-group="gallery" href="/asset/images/uploads/image131.jpg" ><img src="/asset/images/uploads/image13.jpg" alt=""></a>
											<a class="img-lightbox"  data-fancybox-group="gallery" href="/asset/images/uploads/image141.jpg" ><img src="/asset/images/uploads/image14.jpg" alt=""></a>
											<a class="img-lightbox"  data-fancybox-group="gallery" href="/asset/images/uploads/image151.jpg" ><img src="/asset/images/uploads/image15.jpg" alt=""></a>
											<a class="img-lightbox"  data-fancybox-group="gallery" href="/asset/images/uploads/image161.jpg" ><img src="/asset/images/uploads/image16.jpg" alt=""></a>
											<a class="img-lightbox"  data-fancybox-group="gallery" href="/asset/images/uploads/image171.jpg" ><img src="/asset/images/uploads/image17.jpg" alt=""></a>
											<a class="img-lightbox"  data-fancybox-group="gallery" href="/asset/images/uploads/image181.jpg" ><img src="/asset/images/uploads/image18.jpg" alt=""></a>
											<a class="img-lightbox"  data-fancybox-group="gallery" href="/asset/images/uploads/image191.jpg" ><img src="/asset/images/uploads/image19.jpg" alt=""></a>
											<a class="img-lightbox"  data-fancybox-group="gallery" href="/asset/images/uploads/image201.jpg" ><img src="/asset/images/uploads/image20.jpg" alt=""></a>
											<a class="img-lightbox"  data-fancybox-group="gallery" href="/asset/images/uploads/image211.jpg" ><img src="/asset/images/uploads/image2-1.jpg" alt=""></a>
										</div>
						        	</div>
					       	 	</div>
					       	 	<div id="moviesrelated" class="tab">
									<div class="row">
										<h3>Related Movies To</h3>
										<h2>{{ucfirst($movie->name)}}</h2>
										<div class="topbar-filter">
											<p>Found <span>12 movies</span> in total</p>
											<label>Sort by:</label>
											{{-- <select>
												<option value="popularity">Popularity Descending</option>
												<option value="popularity">Popularity Ascending</option>
												<option value="rating">Rating Descending</option>
												<option value="rating">Rating Ascending</option>
												<option value="date">Release date Descending</option>
												<option value="date">Release date Ascending</option>
											</select> --}}
										</div>
										@if(isset($related_movies))
										@foreach($related_movies as $movie)
										<?php
										$release_date = explode('-',$movie->first_air_date);
										?>
										<div class="movie-item-style-2">

											@if(isset($movie->poster_path))
						            			<img src="https://image.tmdb.org/t/p/w300{{$movie->poster_path}}" alt="" style="width: 150px; height: 250px">
						            			@else
						            			<img src="/images/default.png" alt="" style="width: 150px; height: 250px">
						            			@endif

											<div class="mv-item-infor">
												<h6><a href="/movie-detail/{{$movie->id}}">{{$movie->name}} <span>({{$release_date[0]}})</span></a></h6>
												<p class="rate"><i class="ion-android-star"></i><span>{{$movie->vote_average}}</span> /10</p>
												<p class="describe">{{substr($movie->overview,50)}}...</p>
												{{-- <p class="run-time"> Run Time: 2h21’    .     <span>MMPA: PG-13 </span>    .     <span>Release: 1 May 2015</span></p> --}}
												{{-- <p>Director: <a href="#">Joss Whedon</a></p> --}}
												{{-- <p>Stars: <a href="#">Robert Downey Jr.,</a> <a href="#">Chris Evans,</a> <a href="#">  Chris Hemsworth</a></p> --}}
											</div>
										</div>
										@endforeach
										@endif
										
									</div>
								</div>
						    </div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-2 col-sm-12 col-xs-12"></div>
		</div>
	</div>
</div>


@endsection