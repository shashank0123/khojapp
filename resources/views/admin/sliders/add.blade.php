@extends('layouts.admin')


@section('content')
<h4 class="header-title m-t-0 m-b-30">Default Example</h4>
<h4 class="header-title m-t-0 m-b-30">Add Slider </h4>
@if(Session::get('success'))
<div class="alert alert-success"> {{Session::get('success')}} </div>
@endif
@if (count($errors) > 0)
<div class="alert alert-danger">
	<ul>
		@foreach ($errors->all() as $error)
		<li>{{ $error }}</li>
		@endforeach
	</ul>
</div>
@endif

<form method="POST" action="{{url('admin/add_slider')}}" enctype="multipart/form-data">
	@csrf
	

	<div class="row input-control">
		<label class="col-sm-3">Page Name</label>

		<div class="col-sm-6">
			<select name='page_title' id="page_title" class="form-control" required="required">
				<option value=""> -- SELECT PAGE NAME -- </option>		
				<option value="default">Default Image</option>		
				<option value="Home">Home</option>		
				<option value="Trending">Trending</option>	
				<option value="Upcoming">Upcoming</option>	
				<option value="New">New Release</option>	
				<option value="Free">Free</option>	
				<option value="Viral">Viral Videos</option>	
				<option value="Recent">Recently Added</option>	
				<option value="homeside">Home Sidebar</option>	
				<option value="movielist">Home Movielist</option>	
				<option value="moviedetailstitle">Side of Title Movie Details</option>	
				<option value="moviedetailscast">Below Cast Movie Details</option>	
				<option value="roadblock">Road Block</option>	
			</select>
		</div>
	</div>

	<div class="row input-control">
		<label class="col-sm-3">Banner Title</label>
		<div class="col-sm-6">
			
			<input type="text" name="image_title" id="image_title" class="form-control">

		</div>
	</div>

	<div class="row input-control">
		<label class="col-sm-3">Banner Image</label>
		<div class="col-sm-6">
			
			<input type="file" name="poster" id="poster" class="form-control">
			<h6>Banner size should be 1920*500 </h6>
		</div>
	</div>

	<div class="row input-control">
		<label class="col-sm-3">Redirect Url</label>
		<div class="col-sm-6">
			
			<input type="text" name="redirect_url" id="redirect_url" class="form-control">

		</div>
	</div>

	<div class="row input-control">
		<label class="col-sm-3">Priorty</label>
		<div class="col-sm-6">
			
			<input type="text" name="display_priority" id="display_priority" class="form-control">

		</div>
	</div>

	<div class="row input-control">
		<label class="col-sm-3">Start Date</label>
		<div class="col-sm-6">
			
			<input type="date" name="start_date" id="start_date" class="form-control" required="required">

		</div>
	</div><div class="row input-control">
		<label class="col-sm-3">End Date</label>
		<div class="col-sm-6">
			
			<input type="date" name="end_date" id="end_date" class="form-control" required="required">

		</div>
	</div>

	<div class="row input-control">
		<label class="col-sm-3">Status</label>

		<div class="col-sm-6">
			
			<select name='status' id="status" class="form-control">
				<option value="Active">Active</option>		
				<option value="Inactive">Inactive</option>	
			</select>
		</div>
	</div>
	

	<div class="row form-group">
		<div class="col-sm-6 col-sm-offset-5">
			<button type="submit" name="save" class="btn btn-primary">Save</button> 
		</div>
	</div>



</form>


@endsection
