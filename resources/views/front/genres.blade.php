@extends('layouts.new_front')

@section('content')
<style>
	#genre-list li{ display: inline-block; padding: 15px; text-align: center; }
	#genre-list li h6{ color: #fff; margin-top: 10px }
	#genre-list li h6:hover { color: #dcf836 }
	#genre-list .active h6 { color: #dcf836 }


</style>
<?php if (isset($_GET['page_id'])){
	$next = $_GET['page_id']+1;
	$prev = $_GET['page_id']-1;
}
else {
	$prev = '0';
	$next = '1';
}

?>
<?php $featureds = session()->get('featured');
?>		
<div class="page-single movie-single movie_single">
	<div class="container-fluid">

		<div class="flexslider progression-studios-dashboard-slider">
			<ul class="slides">
				@if(!empty($featureds))
				@foreach ($featureds as $featured)
				{{-- expr --}}

				<li class="progression_studios_animate_left">
					@if(isset($featured->poster_path))
					<div class="progression-studios-slider-dashboard-image-background" style="background-image:url(https://image.tmdb.org/t/p/w500{{$featured->poster_path}});">
						@else
					<div class="progression-studios-slider-dashboard-image-background" style="background-image:url(/images/default.png);">
						@endif
						<div class="progression-studios-slider-display-table">
							<div class="progression-studios-slider-vertical-align">							
								<div class="container">
									<a class="progression-studios-slider-play-btn afterglow" href="#VideoLightbox-1">
										<i class="fas fa-play"></i>
									</a>
									<video id="VideoLightbox-1" poster="../../cdn.plyr.io/static/demo/View_From_A_Blue_Moon_Trailer-HD6654.jpg?v1" width="960" height="540">
										<source src="https://cdn.plyr.io/static/demo/View_From_A_Blue_Moon_Trailer-HD.mp4" type="video/mp4">
											<source src="https://cdn.plyr.io/static/demo/View_From_A_Blue_Moon_Trailer-HD.webm" type="video/webm">
											</video>

											<div
											class="circle-rating-pro"
											data-value="0.86"
											data-animation-start-value="0.86"
											data-size="70"
											data-thickness="6"
											data-fill="{
											&quot;color&quot;: &quot;#42b740&quot;
										}"
										data-empty-fill="#def6de"
										data-reverse="true"
										><span style="color:#42b740;">8.6</span></div>

										<div class="progression-studios-slider-dashboard-caption-width">
											<div class="progression-studios-slider-caption-align">
												<h6>{{$featured->type}}</h6>
												<ul class="progression-studios-slider-rating">
													<li>PG-13</li><li>HD</li>
												</ul>
												<h2><a href="dashboard-movie-profile">{{$featured->name}}</a></h2>
												<ul class="progression-studios-slider-meta">
													<li>{{Carbon\Carbon::parse($featured->release_date)->format('d M Y')}} (UK)</li>
													<li>{{$featured->runtime}} min.</li>
													{{-- <li>Documentary</li> --}}
												</ul>
												<p class="progression-studios-slider-description">{{$featured->overview}} 
												</p>

												<a class="btn btn-green-pro btn-slider-pro btn-shadow-pro afterglow" href="<?php if ($featured->type == 'Series'){?>{{'tv'}}<?php } else echo 'movie';?>_detail/{{$featured->movie_id}}"><i class="fas fa-play"></i> Watch Trailer</a>
											{{-- <video class="vid" autoplay loop>
											    <source src="video.mp4" type='video/mp4' />
											    <img id="alternative" src="alternative.jpg" />
											</video>
											<iframe class="vid" id="yt" src="https://www.youtube.com/embed/qObSFfdfe7I" frameborder="0" allowfullscreen></iframe>
											<div id="content">
											    <p>Title</p>
											    <center>
											        <button class="btn btn-green-pro btn-slider-pro btn-shadow-pro afterglow">Click</button>
											    </center>
											</div> --}}


											<div class="progression-studios-slider-more-options">
												<ul>
													@if ($featured->favourite)
													<li><a href="#">Added to Favorites</a></li>
													@else
													<li><a href="/favourite/{{$featured->movie_id}}">Add to Favorites</a></li>
													@endif
													@if ($featured->watchlist)
													<li><a href="#">Added to Watchlist</a></li>
													@else
													<li><a href="/watchlist/{{$featured->movie_id}}">Add to Watchlist</a></li>
													@endif
													@if ($featured->playlist)
													<li><a href="#">Added to Playlist</a></li>
													@else
													<li><a href="/playlist/{{$featured->movie_id}}">Add to Playlist</a></li>
													@endif
													<li><a href="#!">Share...</a></li>
													<li><a href="#!">Write A Review</a></li>
												</ul>
												<i class="fas fa-ellipsis-h"></i>
											</div>
											<div class="clearfix"></div>      

											@if ($featured->crews)
											<h5>Starring</h5>
											<ul class="progression-studios-staring-slider">
												{{-- expr --}}
												@foreach ($featured->crews as $element)
												<li><a href="#!"><img src="https://image.tmdb.org/t/p/w300{{$element->profile_path}}" alt="{{$element->name}}"></a></li>
												@endforeach

											</ul>
											@endif

										</div><!-- close .progression-studios-slider-caption-align -->
									</div><!-- close .progression-studios-slider-caption-width -->
									
								</div><!-- close .container -->
								
							</div><!-- close .progression-studios-slider-vertical-align -->
						</div><!-- close .progression-studios-slider-display-table -->
						
						<div class="progression-studios-slider-mobile-background-cover"></div>
					</div><!-- close .progression-studios-slider-image-background -->
				</li>
				@endforeach			
				@endif	
			</ul>
		</div><!-- close .progression-studios-slider - See /js/script.js file for options -->

		<ul class="container"  id="genre-list">
			<li @if ($key == 'Drama')
			class="active"
			@endif >
			<a href="/genres/drama">
				<img src="/final/images/genres/drama.png" alt="Drama" style="width: 45px;height: 45px">
				<h6>Drama</h6>
			</a>
		</li>
		<li  @if ($key == 'Comedy')
		class="active"
		@endif >
		<a href="/genres/comedy">
			<img src="/final/images/genres/comedy.png" alt="Comedy" style="width: 45px;height: 45px">
			<h6>Comedy</h6>
		</a>
	</li>
	<li @if ($key == 'Action')
	class="active"
	@endif>
	<a href="/genres/action">
		<img src="/final/images/genres/action.png" alt="Action" style="width: 45px;height: 45px">
		<h6>Action</h6>
	</a>
</li>
<li @if ($key == 'Romance')
class="active"
@endif>
<a href="/genres/romance">
	<img src="/final/images/genres/romance.png" alt="Romance" style="width: 45px;height: 45px">
	<h6>Romance</h6>
</a>
</li>
<li @if ($key == 'Horror')
class="active"
@endif >
<a href="/genres/horror">
	<img src="/final/images/genres/horror.png" alt="Horror" style="width: 45px;height: 45px">
	<h6>Horror</h6>
</a>
</li>
<li @if ($key == 'Fantasy')
class="active"
@endif >
<a href="/genres/fantasy">
	<img src="/final/images/genres/fantasy.png" alt="Fantasy" style="width: 45px;height: 45px">
	<h6>Fantasy</h6>
</a>
</li>
<li @if ($key == 'Science Fiction')
class="active"
@endif >
<a href="/genres/science_fiction">
	<img src="/final/images/genres/sci-fi.png" alt="Sci-Fi" style="width: 45px;height: 45px">
	<h6>Sci-Fi</h6>
</a>
</li>
<li @if ($key == 'Thriller')
class="active"
@endif >
<a href="/genres/thriller">
	<img src="/final/images/genres/thriller.png" alt="Thriller" style="width: 45px;height: 45px">
	<h6>Thriller</h6>
</a>
</li>
<li @if ($key == 'Western')
class="active"
@endif >
<a href="/genres/western">
	<img src="/final/images/genres/western.png" alt="Western" style="width: 45px;height: 45px">
	<h6>Western</h6>
</a>
</li>
<li @if ($key == 'Adventure')
class="active"
@endif >
<a href="/genres/adventure">
	<img src="/final/images/genres/adventure.png" alt="Adventure" style="width: 45px;height: 45px">
	<h6>Adventure</h6>
</a>
</li>
<li @if ($key == 'Animation')
class="active"
@endif >
<a href="/genres/animation">
	<img src="/final/images/genres/animation.png" alt="Animation" style="width: 45px;height: 45px">
	<h6>Animation</h6>
</a>
</li>
<li @if ($key == 'Documentary')
class="active"
@endif >
<a href="/genres/documentary">
	<img src="/final/images/genres/documentary.png" alt="Documentary" style="width: 45px;height: 45px">
	<h6>Documentary</h6>
</a>
</li>
</ul>


<div class="clearfix"></div>


<div class="movie-items  full-width">
	<div class="row">
		<div class="col-md-12">
<div class="row">
	@if (isset($contents->results))
	@foreach ($contents->results as $element)
	<div class="col-sm-3 search-response">
		<div class="movie-item">
			<div class="mv-img">
				@if(isset($element->poster_path))
				<img src="https://image.tmdb.org/t/p/w300{{$element->poster_path}}" alt=""  style="width: 240px; height: 340px">
				@else
				<img src="/images/default.png" alt="" style="width: 240px; height: 340px">
				@endif
			</div>
			<div class="hvr-inner">
				<a  href="/movie-detail/{{$element->id}}">
					<i class="fa fa-play" aria-hidden="true"></i> 
				</a>
			</div>
			<div class="title-in">
				<h6>
					<a href="/movie-detail/{{$element->id}}">{{$element->title}}</a>
				</h6>
				<p>
					<i class="ion-android-star"></i><span>{{$element->vote_average}}</span> /10
				</p>
			</div>
		</div>
	</div>
	@endforeach
	@endif
</div>
</div>
	</div>
	
</div>







</div>
</div>

@endsection
