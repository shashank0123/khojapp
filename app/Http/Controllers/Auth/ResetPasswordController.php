<?php
namespace App\Http\Controllers\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;








use App\User;

use Sentinel;
use Reminder;
use Mail,Hash,Auth;

class ResetPasswordController extends Controller
{
     public function password(Request $request){
        $user = User::where('email',$request->email)->first();
        // var_dump($user);
        // die;

        if($user){
            $user = Sentinel::findById($user->id);
            $reminder = Reminder::exists($user) ? : Reminder::create($user);

            $this->sendEmail($user,$reminder);

            return redirect()->back()->with('message','Reset password link send to your Email.');
        }
        else{
            // echo 'Yes'; die;
            return redirect()->back()->with('fail','Email does not exist.');
        }
    } 

    public function sendEmail($user,$code){
        Mail::send(
            'auth.passwords.forgot', 
            ['user' => $user, 'code' => $code],
            function ($message) use ($user) {
                $message->to($user->email);
                $message->subject("$user->name, reset your password.");
            });
    }


    public function getResetPage($email,$token){
        return view('auth.passwords.reset',compact('email','token'));
    }


    public function create(Request $request){
        $data = $request->all();

        // var_dump($data['token']); die;

if($data['password'] == $data['password_confirmation']){
    $user = User::where('email',$data['email'])->first();

    // var_dump($user); die;
     $code = Reminder::where('user_id',$user->id)->where('completed',0)->get()->last();
    // var_dump($code); die;

     if(!empty($code)){
        $user->password = Hash::make($data['password']);
        $user->update();

$user = User::where('email',$data['email'])->first();
        Auth::login($user);
        return redirect()->route('home');
     }
     else{
        return redirect()->back()->with('message','Something went wrong.');
     }
}
else{
    return redirect()->back()->with('message','Password did not match.');
}
       

    }

}