<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Crew;
use App\Movie;
use App\Cast;
use App\WatchList;
use App\Playlist;
use App\Favourite;
use App\Country;
use App\Newsletter;
use App\Genre;
use App\Feedback;
use App\Rating;
use App\Content;
use App\ContentCasting;
use App\PublicPlaylist;
use App\ContentGenre;
use App\ContentClip;
use App\SearchKeyword;
use App\User;
use DB, StdClass;
use Intervention\Image\ImageManagerStatic as Image;

class KhojNewController extends Controller
{
    public function __construct()
    {
        $genre = \Cache::rememberForever('all_genres2', function () {
                            return $contents = $genre = Genre::all();

                        });
        
        session()->put('genre', $genre);
        $featureds = \Cache::rememberForever('featured22', function () {
                            return $contents = Movie::where('featured', 'yes')->get();;

                        });
        
        foreach ($featureds as $key => $value) {
            $id = $value->movie_id;
            $favourite = 0;
            $watchlist = 0;
            $playlist = 0;
            if (isset(auth()->user()->id)) {
                $favourite = Favourite::where('movie_id', $id)->where('user_id', auth()->user()->id)->first();
                if ($favourite) {
                    $featureds[$key]->favourite = 1;
                }
                $watchlist = WatchList::where('movie_id', $id)->where('user_id', auth()->user()->id)->first();
                if ($watchlist) {
                    $featureds[$key]->watchlist = 1;
                }

                $playlist = Playlist::where('movie_id', $id)->where('user_id', auth()->user()->id)->first();
                if ($playlist) {
                    $featureds[$key]->playlist = 1;
                }
            }
            $casts = \Cache::rememberForever('all_catss', function () {
                            return $contents = Crew::join('casts', 'casts.id', '=', 'crews.cast_id')->where('movie_id', $id)->where('job', '!=', 'Director')->get()->take(6);

                        });
            
            $featureds[$key]->crews = $casts;
        }
        session()->put('featureds', $featureds);
    }


    






    public function getCountContent()
    {
        $contents = DB::table('content_providers')
            ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
            ->where('content_providers.retail_price', '0.00')
            ->where('content_providers.provider_id', '!=', '701')
            ->where('content_providers.monetization_type', "!=", 'flatrate')
            // ->where('contents.cinema_release_date','<=',$maxy)
            // ->where('contents.cinema_release_date','>=',$miny)
            ->where('contents.content_type', '!=', 'tv-show')
            ->where('contents.parent_id', '0')
            ->orderBy('contents.cinema_release_date', 'DESC')
            ->select('contents.*')
            ->distinct('contents.content_id')
            ->count();
        echo $contents;
    }





    public function resizeImage($filename)
    {
        $img = 0;
        $img2 = 1;
        if ($filename == "") {
            return 0;
        }
        // echo public_path('poster/'.$filename)."<br>";

        try {
            if (file_exists(public_path('poster/' . $filename)))
                $img = Image::make(public_path('poster/' . $filename));
        } catch (NotReadableException $e) {
            // If error, stop and continue looping to next iteration
            // return 0;
        }
        try {
            if (file_exists(public_path('thumbnail/' . $filename)))
                return 0;
        } catch (NotReadableException $e) {
            // If error, stop and continue looping to next iteration
            // continue;
        }

        if ($img && !$img2) {
            $image_resize->resize(166, 236);
            $image_resize->save(public_path('thumbnail/' . $filename));
        }
    }

    public static function prepareurl($web_url)
    {
        $campaigncode = "utm_source=khojapp&utm_campaign=site&utm_medium=moviedetails";


        if (isset(explode('?', $web_url)[1])) {
            $web_url = $web_url . "&" . $campaigncode;
        } else {

            $web_url = $web_url . "?" . $campaigncode;
        }

        return $web_url;
    }



    // For Homepage
    function getHome(Request $request)
    {
        // return 0;
        $page = 0;
        $langs = "";
        $rated = "";
        $sort = "";
        $field = "";
        $sort_type = "";
        $genre = "";
        $provider = "";
        $minr = 0;
        $maxr = 10;
        $type = "ALL";
        $tab = 'Home';

        $maxy = date('y-m-d');;
        // $maxy = $maxy->cinema_release_date;

        $miny = '0000';

        if (isset($_GET['page'])) {
            $page = ($_GET['page'] - 1) * 28;
        }
        $rated = 'all';
        $langs = 'all';

        $provider_id = 0;
        $genre = 0;

        if (isset($_GET['lang'])) {
            $langs = $_GET['lang'];
        }
        if (isset($_GET['provider'])) {
            $provider_id = explode('_', $_GET['provider'])[1];
        }

        if (isset($_GET['sort'])) {
            $sort = $_GET['sort'];
            if ($sort == "alpha") {
                $field = 'title';
                $sort_type = 'ASC';
            } elseif ($sort == 'rate') {
                $field = 'tmdb_score';
                $sort_type = 'DESC';
            } elseif ($sort == 'year') {
                $field = 'cinema_release_date';
                $sort_type = 'DESC';
            } elseif ($sort == 'imdb_score') {
                $field = 'imdb_score';
                $sort_type = 'DESC';
            } else {
                $field = 'content_id';
                $sort_type = 'DESC';
            }
        }

        if ((isset($_GET['sort']) && isset($_GET['genre'])) || (isset($_GET['sort']) && isset($_GET['provider']))) {
            $sort = $_GET['sort'];
            if ($sort == "asc") {
                $field = 'contents.cinema_release_date';
                $sort_type = 'ASC';
            } elseif ($sort == 'desc') {
                $field = 'contents.cinema_release_date';
                $sort_type = 'DESC';
            } elseif ($sort == 'rand') {
                $field = 'contents.cinema_release_date';
                $sort_type = 'DESC';
            } elseif ($sort == 'imdb_score') {
                $field = 'contents.imdb_score';
                $sort_type = 'DESC';
            } else {
                $field = 'contents.content_id';
                $sort_type = 'DESC';
            }
        }

        if (isset($_GET['rated'])) {
            $rated = $_GET['rated'];
        }

        if (isset($_GET['searchtype'])) {
            $type = $_GET['searchtype'];
        }

        if (isset($_GET['genre'])) {
            $genre = $_GET['genre'];
        }

        if (isset($_GET['minr'])) {
            $minr = $_GET['minr'];
        }
        if (isset($_GET['maxr'])) {
            $maxr = $_GET['maxr'];
        }
        $miny = 1920;
        $maxy = date('Y');
        if (isset($_GET['miny'])) {
            $miny = $_GET['miny'];
        }
        if (isset($_GET['maxy'])) {
            $maxy = $_GET['maxy'];
        }

        $sort_type = "relevance";
        if (isset($_GET['sort_by'])) {
            $sort_type = $_GET['sort_by'];
        }

        $orderBy = "DESC";
        if (isset($_GET['order'])) {
            $orderBy = $_GET['order'];
        }

        echo $search23 = "CALL ret_home(" . $genre . ", '" . $type . "','" . $miny . "', '" . $maxy . "', '" . $rated . "', '" . $langs . "', " . $page . ", 28, '" . $provider_id . "', '" . $sort_type . "', '" . $orderBy . "')";
        // die();
        $contents = \Cache::rememberForever($search23, function () use ($search23) {
        return  DB::select($search23);
    });
        // $contents = DB::select($search23);
        // $miny = explode('-',$miny)[0];
        // $maxy = explode('-',$maxy)[0];

        if ($miny == '') {
            $miny = '1920';
        }

        if (isset($_GET['provider'])) {
            $provider = $_GET['provider'];
        }
        // $contents = $contents->appends(\Input::except('page'));
        foreach ($contents as $key => $value) {
            $this->resizeImage($value->poster);
        }
        return view('khoj.home', compact('contents', 'page', 'type', 'miny', 'maxy', 'langs', 'genre', 'rated', 'minr', 'maxr', 'sort', 'tab', 'provider'));
    }




    function selectbasicmovie(Request $request)
    {
        // return 0;
        $contents = Content::where('content_type', 'movie')->distinct('contents.content_id')->orderBy('original_release_year', 'desc')->limit(28)->get();
        $tab = "Select Your favourite";


        return view('khoj.preference_page', compact('contents', 'tab'));
    }

    function selectbasicott(Request $request)
    {
        // return 0;
        $contents = DB::table('providers')->get();
        $tab = "Select Your Subscribed OTT";


        return view('khoj.ott_page', compact('contents', 'tab'));
    }

    function updateuserpreference(Request $request)
    {
        // return 0;
        echo "alert";
        die();
        $contents = Content::where('content_type', 'movie')->distinct('contents.content_id')->orderBy('original_release_year', 'desc')->limit(28)->get();
        $tab = "Select Your favourite";


        return view('khoj.preference_page', compact('contents', 'tab'));
    }





    public function getViralMovies()
    {


        $page = 0;
        $langs = "";
        $rated = "";
        $sort = "";
        $field = "";
        $sort_type = "";
        $genre = "";
        $provider = "";
        $minr = 0;
        $maxr = 10;
        $type = "viral";
        $tab = 'Viral';

        // $maxy = DB::table('contents')->orderBy('cinema_release_date','DESC')->first();
        $maxy = date('y-m-d');;

        $miny = '0000-00-00';

        if (isset($_GET['page'])) {
            $page = $_GET['page'] - 1;
        }

        if (isset($_GET['lang'])) {
            $langs = $_GET['lang'];
        }
        if (isset($_GET['provider'])) {
            $provider = explode('_', $_GET['provider'])[1];
        }

        if (isset($_GET['sort'])) {
            $sort = $_GET['sort'];
            if ($sort == "alpha") {
                $field = 'title';
                $sort_type = 'ASC';
            } elseif ($sort == 'rate') {
                $field = 'tmdb_score';
                $sort_type = 'DESC';
            } elseif ($sort == 'year') {
                $field = 'cinema_release_date';
                $sort_type = 'DESC';
            } elseif ($sort == 'imdb_score') {
                $field = 'imdb_score';
                $sort_type = 'DESC';
            } else {
                $field = 'content_id';
                $sort_type = 'DESC';
            }
        }

        if ((isset($_GET['sort']) && isset($_GET['genre'])) || (isset($_GET['sort']) && isset($_GET['provider']))) {
            $sort = $_GET['sort'];
            if ($sort == "alpha") {
                $field = 'contents.title';
                $sort_type = 'ASC';
            } elseif ($sort == 'rate') {
                $field = 'contents.tmdb_score';
                $sort_type = 'DESC';
            } elseif ($sort == 'year') {
                $field = 'contents.cinema_release_date';
                $sort_type = 'DESC';
            } elseif ($sort == 'imdb_score') {
                $field = 'contents.imdb_score';
                $sort_type = 'DESC';
            } else {
                $field = 'contents.content_id';
                $sort_type = 'DESC';
            }
        }

        if (isset($_GET['rated'])) {
            $rated = $_GET['rated'];
        }

        if (isset($_GET['searchtype'])) {
            $type = $_GET['searchtype'];
        }

        if (isset($_GET['genre'])) {
            $genre = $_GET['genre'];
        }

        if (isset($_GET['minr'])) {
            $minr = $_GET['minr'];
        }
        if (isset($_GET['maxr'])) {
            $maxr = $_GET['maxr'];
        }

        if (isset($_GET['miny'])) {
            $miny = $_GET['miny'] . "-01-01";
        }
        if (isset($_GET['maxy'])) {
            $maxy = $_GET['maxy'] . "-12-31";
        }


        if ($provider == "") {

            if ($sort == "") {
                if ($langs != "" && $rated != "" && $genre != "") {
                    if ($minr == 0 && $maxr == 10) {
                        $contents = DB::table('content_genres')
                            ->leftJoin('contents', 'content_genres.content_id', 'contents.content_id')
                            ->where('content_genres.genre_id', $genre)
                            ->where('contents.content_type', $type)
                            ->where('contents.age_certification', $rated)
                            ->where('contents.original_language', $langs)
                            ->where('contents.cinema_release_date', '<=', $maxy)
                            ->where('contents.cinema_release_date', '>=', $miny)
                            ->where('contents.parent_id', '0')
                            ->orderBy('content_genres.content_id', 'DESC')
                            ->paginate(28);
                    } else {
                        $contents = DB::table('content_genres')
                            ->leftJoin('contents', 'content_genres.content_id', 'contents.content_id')
                            ->where('content_genres.genre_id', $genre)
                            ->where('contents.content_type', $type)
                            ->where('contents.age_certification', $rated)
                            ->where('contents.original_language', $langs)
                            ->where('contents.tmdb_score', '<=', $maxr)
                            ->where('contents.tmdb_score', '>=', $minr)
                            ->where('contents.cinema_release_date', '<=', $maxy)
                            ->where('contents.cinema_release_date', '>=', $miny)
                            ->where('contents.parent_id', '0')
                            ->orderBy('content_genres.content_id', 'DESC')
                            ->paginate(28);
                    }
                } elseif ($langs != "" && $rated != "") {
                    if ($minr == 0 && $maxr == 10) {
                        $contents = DB::table('contents')
                            ->where('content_type', $type)
                            ->where('original_language', $langs)
                            ->where('age_certification', $rated)
                            ->where('cinema_release_date', '<=', $maxy)
                            ->where('cinema_release_date', '>=', $miny)
                            ->where('parent_id', '0')
                            ->paginate(28);
                    } else {
                        $contents = DB::table('contents')
                            ->where('content_type', $type)
                            ->where('original_language', $langs)
                            ->where('age_certification', $rated)
                            ->where('tmdb_score', '<=', $maxr)
                            ->where('tmdb_score', '>=', $minr)
                            ->where('cinema_release_date', '<=', $maxy)
                            ->where('cinema_release_date', '>=', $miny)
                            ->where('parent_id', '0')
                            ->paginate(28);
                    }
                } elseif ($rated != "" && $genre != "") {
                    if ($minr == 0 && $maxr == 10) {
                        $contents = DB::table('content_genres')
                            ->leftJoin('contents', 'content_genres.content_id', 'contents.content_id')
                            ->where('content_genres.genre_id', $genre)
                            ->where('contents.content_type', $type)
                            ->where('contents.age_certification', $rated)
                            ->where('contents.cinema_release_date', '<=', $maxy)
                            ->where('contents.cinema_release_date', '>=', $miny)
                            ->where('contents.parent_id', '0')
                            ->orderBy('content_genres.content_id', 'DESC')
                            ->paginate(28);
                    } else {
                        $contents = DB::table('content_genres')
                            ->leftJoin('contents', 'content_genres.content_id', 'contents.content_id')
                            ->where('content_genres.genre_id', $genre)
                            ->where('contents.content_type', $type)
                            ->where('contents.age_certification', $rated)
                            ->where('contents.tmdb_score', '<=', $maxr)
                            ->where('contents.tmdb_score', '>=', $minr)
                            ->where('contents.cinema_release_date', '<=', $maxy)
                            ->where('contents.cinema_release_date', '>=', $miny)
                            ->where('contents.parent_id', '0')
                            ->orderBy('content_genres.content_id', 'DESC')
                            ->paginate(28);
                    }
                } elseif ($langs != "" && $genre != "") {
                    if ($minr == 0 && $maxr == 10) {
                        $contents = DB::table('content_genres')
                            ->leftJoin('contents', 'content_genres.content_id', 'contents.content_id')
                            ->where('content_genres.genre_id', $genre)
                            ->where('contents.content_type', $type)
                            ->where('contents.original_language', $langs)
                            ->where('contents.cinema_release_date', '<=', $maxy)
                            ->where('contents.cinema_release_date', '>=', $miny)
                            ->where('contents.parent_id', '0')
                            ->orderBy('content_genres.content_id', 'DESC')
                            ->paginate(28);
                    } else {
                        $contents = DB::table('content_genres')
                            ->leftJoin('contents', 'content_genres.content_id', 'contents.content_id')
                            ->where('content_genres.genre_id', $genre)
                            ->where('contents.content_type', $type)
                            ->where('contents.original_language', $langs)
                            ->where('contents.tmdb_score', '<=', $maxr)
                            ->where('contents.tmdb_score', '>=', $minr)
                            ->where('contents.cinema_release_date', '<=', $maxy)
                            ->where('contents.cinema_release_date', '>=', $miny)
                            ->where('contents.parent_id', '0')
                            ->orderBy('content_genres.content_id', 'DESC')
                            ->paginate(28);
                    }
                } elseif ($langs != "") {
                    if ($minr == 0 && $maxr == 10) {
                        $contents = DB::table('contents')
                            ->where('content_type', $type)
                            ->where('original_language', $langs)
                            ->where('cinema_release_date', '<=', $maxy)
                            ->where('cinema_release_date', '>=', $miny)
                            ->where('parent_id', '0')
                            ->paginate(28);
                    } else {
                        $contents = DB::table('contents')
                            ->where('content_type', $type)
                            ->where('original_language', $langs)
                            ->where('tmdb_score', '<=', $maxr)
                            ->where('tmdb_score', '>=', $minr)
                            ->where('cinema_release_date', '<=', $maxy)
                            ->where('cinema_release_date', '>=', $miny)
                            ->where('parent_id', '0')
                            ->paginate(28);
                    }
                } elseif ($genre != "") {
                    if ($minr == 0 && $maxr == 10) {
                        $contents = DB::table('content_genres')
                            ->leftJoin('contents', 'content_genres.content_id', 'contents.content_id')
                            ->where('content_genres.genre_id', $genre)
                            ->where('contents.content_type', $type)
                            ->where('contents.cinema_release_date', '<=', $maxy)
                            ->where('contents.cinema_release_date', '>=', $miny)
                            ->where('contents.parent_id', '0')
                            ->orderBy('content_genres.content_id', 'DESC')
                            ->paginate(28);
                    } else {
                        $contents = DB::table('content_genres')
                            ->leftJoin('contents', 'content_genres.content_id', 'contents.content_id')
                            ->where('content_genres.genre_id', $genre)
                            ->where('contents.content_type', $type)
                            ->where('contents.tmdb_score', '<=', $maxr)
                            ->where('contents.tmdb_score', '>=', $minr)
                            ->where('contents.cinema_release_date', '<=', $maxy)
                            ->where('contents.cinema_release_date', '>=', $miny)
                            ->where('contents.parent_id', '0')
                            ->orderBy('content_genres.content_id', 'DESC')
                            ->paginate(28);
                    }
                } elseif ($rated != "") {
                    if ($minr == 0 && $maxr == 10) {
                        $contents = DB::table('contents')
                            ->where('content_type', $type)
                            ->where('age_certification', $rated)
                            ->where('cinema_release_date', '<=', $maxy)
                            ->where('cinema_release_date', '>=', $miny)
                            ->where('parent_id', '0')
                            ->paginate(28);
                    } else {
                        $contents = DB::table('contents')
                            ->where('content_type', $type)
                            ->where('age_certification', $rated)
                            ->where('tmdb_score', '<=', $maxr)
                            ->where('tmdb_score', '>=', $minr)
                            ->where('cinema_release_date', '<=', $maxy)
                            ->where('cinema_release_date', '>=', $miny)
                            ->where('parent_id', '0')
                            ->paginate(28);
                    }
                } else {
                    if ($minr == 0 && $maxr == 10) {
                        $contents = DB::table('contents')
                            ->where('content_type', $type)
                            ->where('cinema_release_date', '<=', $maxy)
                            ->where('cinema_release_date', '>=', $miny)
                            ->where('parent_id', '0')
                            ->paginate(28);
                    } else {
                        $contents = DB::table('contents')
                            ->where('content_type', $type)
                            ->where('tmdb_score', '<=', $maxr)
                            ->where('tmdb_score', '>=', $minr)
                            ->where('cinema_release_date', '<=', $maxy)
                            ->where('cinema_release_date', '>=', $miny)
                            ->where('parent_id', '0')
                            ->paginate(28);
                    }
                }
            } else {

                if ($langs != "" && $rated != "" && $genre != "") {
                    if ($minr == 0 && $maxr == 10) {
                        $contents = DB::table('content_genres')
                            ->leftJoin('contents', 'content_genres.content_id', 'contents.content_id')
                            ->where('content_genres.genre_id', $genre)
                            ->where('contents.content_type', $type)
                            ->where('contents.age_certification', $rated)
                            ->where('contents.original_language', $langs)
                            ->where('contents.cinema_release_date', '<=', $maxy)
                            ->where('contents.cinema_release_date', '>=', $miny)
                            ->where('contents.parent_id', '0')
                            ->orderBy($field, $sort_type)
                            ->paginate(28);
                    } else {
                        $contents = DB::table('content_genres')
                            ->leftJoin('contents', 'content_genres.content_id', 'contents.content_id')
                            ->where('content_genres.genre_id', $genre)
                            ->where('contents.content_type', $type)
                            ->where('contents.age_certification', $rated)
                            ->where('contents.original_language', $langs)
                            ->where('contents.tmdb_score', '<=', $maxr)
                            ->where('contents.tmdb_score', '>=', $minr)
                            ->where('contents.cinema_release_date', '<=', $maxy)
                            ->where('contents.cinema_release_date', '>=', $miny)
                            ->where('contents.parent_id', '0')
                            ->orderBy($field, $sort_type)
                            ->paginate(28);
                    }
                } elseif ($langs != "" && $rated != "") {
                    if ($minr == 0 && $maxr == 10) {
                        $contents = DB::table('contents')
                            ->where('content_type', $type)
                            ->where('original_language', $langs)
                            ->where('age_certification', $rated)
                            ->where('cinema_release_date', '<=', $maxy)
                            ->where('cinema_release_date', '>=', $miny)
                            ->where('parent_id', '0')
                            ->orderBy($field, $sort_type)
                            ->paginate(28);
                    } else {
                        $contents = DB::table('contents')
                            ->where('content_type', $type)
                            ->where('original_language', $langs)
                            ->where('age_certification', $rated)
                            ->where('tmdb_score', '<=', $maxr)
                            ->where('tmdb_score', '>=', $minr)
                            ->where('cinema_release_date', '<=', $maxy)
                            ->where('cinema_release_date', '>=', $miny)
                            ->where('parent_id', '0')
                            ->orderBy($field, $sort_type)
                            ->paginate(28);
                    }
                } elseif ($rated != "" && $genre != "") {
                    if ($minr == 0 && $maxr == 10) {
                        $contents = DB::table('content_genres')
                            ->leftJoin('contents', 'content_genres.content_id', 'contents.content_id')
                            ->where('content_genres.genre_id', $genre)
                            ->where('contents.content_type', $type)
                            ->where('contents.age_certification', $rated)
                            ->where('contents.cinema_release_date', '<=', $maxy)
                            ->where('contents.cinema_release_date', '>=', $miny)
                            ->where('contents.parent_id', '0')
                            ->orderBy($field, $sort_type)
                            ->paginate(28);
                    } else {
                        $contents = DB::table('content_genres')
                            ->leftJoin('contents', 'content_genres.content_id', 'contents.content_id')
                            ->where('content_genres.genre_id', $genre)
                            ->where('contents.content_type', $type)
                            ->where('contents.age_certification', $rated)
                            ->where('contents.tmdb_score', '<=', $maxr)
                            ->where('contents.tmdb_score', '>=', $minr)
                            ->where('contents.cinema_release_date', '<=', $maxy)
                            ->where('contents.cinema_release_date', '>=', $miny)
                            ->where('contents.parent_id', '0')
                            ->orderBy($field, $sort_type)
                            ->paginate(28);
                    }
                } elseif ($langs != "" && $genre != "") {
                    if ($minr == 0 && $maxr == 10) {
                        $contents = DB::table('content_genres')
                            ->leftJoin('contents', 'content_genres.content_id', 'contents.content_id')
                            ->where('content_genres.genre_id', $genre)
                            ->where('contents.content_type', $type)
                            ->where('contents.original_language', $langs)
                            ->where('contents.cinema_release_date', '<=', $maxy)
                            ->where('contents.cinema_release_date', '>=', $miny)
                            ->where('contents.parent_id', '0')
                            ->orderBy($field, $sort_type)
                            ->paginate(28);
                    } else {
                        $contents = DB::table('content_genres')
                            ->leftJoin('contents', 'content_genres.content_id', 'contents.content_id')
                            ->where('content_genres.genre_id', $genre)
                            ->where('contents.content_type', $type)
                            ->where('contents.original_language', $langs)
                            ->where('contents.tmdb_score', '<=', $maxr)
                            ->where('contents.tmdb_score', '>=', $minr)
                            ->where('contents.cinema_release_date', '<=', $maxy)
                            ->where('contents.cinema_release_date', '>=', $miny)
                            ->where('contents.parent_id', '0')
                            ->orderBy($field, $sort_type)
                            ->paginate(28);
                    }
                } elseif ($langs != "") {
                    if ($minr == 0 && $maxr == 10) {
                        $contents = DB::table('contents')
                            ->where('content_type', $type)
                            ->where('original_language', $langs)
                            ->where('cinema_release_date', '<=', $maxy)
                            ->where('cinema_release_date', '>=', $miny)
                            ->where('parent_id', '0')
                            ->orderBy($field, $sort_type)
                            ->paginate(28);
                    } else {
                        $contents = DB::table('contents')
                            ->where('content_type', $type)
                            ->where('original_language', $langs)
                            ->where('tmdb_score', '<=', $maxr)
                            ->where('tmdb_score', '>=', $minr)
                            ->where('cinema_release_date', '<=', $maxy)
                            ->where('cinema_release_date', '>=', $miny)
                            ->where('parent_id', '0')
                            ->orderBy($field, $sort_type)
                            ->paginate(28);
                    }
                } elseif ($genre != "") {
                    if ($minr == 0 && $maxr == 10) {
                        $contents = DB::table('content_genres')
                            ->leftJoin('contents', 'content_genres.content_id', 'contents.content_id')
                            ->where('content_genres.genre_id', $genre)
                            ->where('contents.content_type', $type)
                            ->where('contents.cinema_release_date', '<=', $maxy)
                            ->where('contents.cinema_release_date', '>=', $miny)
                            ->where('contents.parent_id', '0')
                            ->orderBy('content_genres.content_id', 'DESC')
                            ->orderBy($field, $sort_type)
                            ->paginate(28);
                    } else {
                        $contents = DB::table('content_genres')
                            ->leftJoin('contents', 'content_genres.content_id', 'contents.content_id')
                            ->where('content_genres.genre_id', $genre)
                            ->where('contents.content_type', $type)
                            ->where('contents.tmdb_score', '<=', $maxr)
                            ->where('contents.tmdb_score', '>=', $minr)
                            ->where('contents.cinema_release_date', '<=', $maxy)
                            ->where('contents.cinema_release_date', '>=', $miny)
                            ->where('contents.parent_id', '0')
                            ->orderBy('content_genres.content_id', 'DESC')
                            ->orderBy($field, $sort_type)
                            ->paginate(28);
                    }
                } elseif ($rated != "") {
                    if ($minr == 0 && $maxr == 10) {
                        $contents = DB::table('contents')
                            ->where('content_type', $type)
                            ->where('age_certification', $rated)
                            ->where('cinema_release_date', '<=', $maxy)
                            ->where('cinema_release_date', '>=', $miny)
                            ->where('parent_id', '0')
                            ->orderBy($field, $sort_type)
                            ->paginate(28);
                    } else {
                        $contents = DB::table('contents')
                            ->where('content_type', $type)
                            ->where('age_certification', $rated)
                            ->where('tmdb_score', '<=', $maxr)
                            ->where('tmdb_score', '>=', $minr)
                            ->where('cinema_release_date', '<=', $maxy)
                            ->where('cinema_release_date', '>=', $miny)
                            ->where('parent_id', '0')
                            ->orderBy($field, $sort_type)
                            ->paginate(28);
                    }
                } else {
                    if ($minr == 0 && $maxr == 10) {
                        $contents = DB::table('contents')
                            ->where('content_type', $type)
                            ->where('cinema_release_date', '<=', $maxy)
                            ->where('cinema_release_date', '>=', $miny)
                            ->where('parent_id', '0')
                            ->orderBy($field, $sort_type)
                            ->paginate(28);
                    } else {
                        $contents = DB::table('contents')
                            ->where('content_type', $type)
                            ->where('tmdb_score', '<=', $maxr)
                            ->where('tmdb_score', '>=', $minr)
                            ->where('cinema_release_date', '<=', $maxy)
                            ->where('cinema_release_date', '>=', $miny)
                            ->where('parent_id', '0')
                            ->orderBy($field, $sort_type)
                            ->paginate(28);
                    }
                }
            }
        } else {

            if ($sort == "") {
                if ($langs != "" && $rated != "" && $genre != "") {
                    if ($minr == 0 && $maxr == 10) {
                        $contents = DB::table('content_providers')
                            ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                            ->leftJoin('content_genres', 'content_genres.content_id', 'contents.content_id')
                            ->where('content_providers.provider_id', $provider)
                            ->where('content_genres.genre_id', $genre)
                            ->where('contents.age_certification', $rated)
                            ->where('contents.content_type', $type)
                            ->where('contents.original_language', $langs)
                            ->where('contents.cinema_release_date', '<=', $maxy)
                            ->where('contents.cinema_release_date', '>=', $miny)
                            ->where('contents.parent_id', '0')
                            ->orderBy('content_genres.content_id', 'DESC')
                            ->select('contents.*')
                            ->distinct('contents.content_id')
                            ->paginate(28);
                    } else {
                        $contents = DB::table('content_providers')
                            ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                            ->leftJoin('content_genres', 'content_genres.content_id', 'contents.content_id')
                            ->where('content_providers.provider_id', $provider)
                            ->where('content_genres.genre_id', $genre)
                            ->where('contents.content_type', $type)
                            ->where('contents.age_certification', $rated)
                            ->where('contents.original_language', $langs)
                            ->where('contents.tmdb_score', '<=', $maxr)
                            ->where('contents.tmdb_score', '>=', $minr)
                            ->where('contents.cinema_release_date', '<=', $maxy)
                            ->where('contents.cinema_release_date', '>=', $miny)
                            ->where('contents.parent_id', '0')
                            ->orderBy('content_genres.content_id', 'DESC')
                            ->select('contents.*')
                            ->distinct('contents.content_id')
                            ->paginate(28);
                    }
                } elseif ($langs != "" && $rated != "") {
                    if ($minr == 0 && $maxr == 10) {
                        $contents = DB::table('content_providers')
                            ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                            ->where('content_providers.provider_id', $provider)
                            ->where('contents.content_type', $type)
                            ->where('contents.original_language', $langs)
                            ->where('contents.age_certification', $rated)
                            ->where('contents.cinema_release_date', '<=', $maxy)
                            ->where('contents.cinema_release_date', '>=', $miny)
                            ->where('contents.parent_id', '0')
                            ->select('contents.*')
                            ->distinct('contents.content_id')
                            ->paginate(28);
                    } else {
                        $contents = DB::table('content_providers')
                            ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                            ->where('content_providers.provider_id', $provider)
                            ->where('contents.content_type', $type)
                            ->where('contents.original_language', $langs)
                            ->where('contents.age_certification', $rated)
                            ->where('contents.tmdb_score', '<=', $maxr)
                            ->where('contents.tmdb_score', '>=', $minr)
                            ->where('contents.cinema_release_date', '<=', $maxy)
                            ->where('contents.cinema_release_date', '>=', $miny)
                            ->where('contents.parent_id', '0')
                            ->select('contents.*')
                            ->distinct('contents.content_id')
                            ->paginate(28);
                    }
                } elseif ($rated != "" && $genre != "") {
                    if ($minr == 0 && $maxr == 10) {
                        $contents = DB::table('content_providers')
                            ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                            ->leftJoin('content_genres', 'content_genres.content_id', 'contents.content_id')
                            ->where('content_providers.provider_id', $provider)
                            ->where('content_genres.genre_id', $genre)
                            ->where('contents.age_certification', $rated)
                            ->where('contents.content_type', $type)
                            ->where('contents.cinema_release_date', '<=', $maxy)
                            ->where('contents.cinema_release_date', '>=', $miny)
                            ->where('contents.parent_id', '0')
                            ->orderBy('content_genres.content_id', 'DESC')
                            ->select('contents.*')
                            ->distinct('contents.content_id')
                            ->paginate(28);
                    } else {
                        $contents = DB::table('content_providers')
                            ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                            ->leftJoin('content_genres', 'content_genres.content_id', 'contents.content_id')
                            ->where('content_providers.provider_id', $provider)
                            ->where('content_genres.genre_id', $genre)
                            ->where('contents.age_certification', $rated)
                            ->where('contents.content_type', $type)
                            ->where('contents.tmdb_score', '<=', $maxr)
                            ->where('contents.tmdb_score', '>=', $minr)
                            ->where('contents.cinema_release_date', '<=', $maxy)
                            ->where('contents.cinema_release_date', '>=', $miny)
                            ->where('contents.parent_id', '0')
                            ->orderBy('content_genres.content_id', 'DESC')
                            ->select('contents.*')
                            ->distinct('contents.content_id')
                            ->paginate(28);
                    }
                } elseif ($langs != "" && $genre != "") {
                    if ($minr == 0 && $maxr == 10) {
                        $contents = DB::table('content_providers')
                            ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                            ->leftJoin('content_genres', 'content_genres.content_id', 'contents.content_id')
                            ->where('content_providers.provider_id', $provider)
                            ->where('content_genres.genre_id', $genre)
                            ->where('contents.content_type', $type)
                            ->where('contents.original_language', $langs)
                            ->where('contents.cinema_release_date', '<=', $maxy)
                            ->where('contents.cinema_release_date', '>=', $miny)
                            ->where('contents.parent_id', '0')
                            ->orderBy('content_genres.content_id', 'DESC')
                            ->select('contents.*')
                            ->distinct('contents.content_id')
                            ->paginate(28);
                    } else {
                        $contents = DB::table('content_providers')
                            ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                            ->leftJoin('content_genres', 'content_genres.content_id', 'contents.content_id')
                            ->where('content_providers.provider_id', $provider)
                            ->where('content_genres.genre_id', $genre)
                            ->where('contents.content_type', $type)
                            ->where('contents.original_language', $langs)
                            ->where('contents.tmdb_score', '<=', $maxr)
                            ->where('contents.tmdb_score', '>=', $minr)
                            ->where('contents.cinema_release_date', '<=', $maxy)
                            ->where('contents.cinema_release_date', '>=', $miny)
                            ->where('contents.parent_id', '0')
                            ->orderBy('content_genres.content_id', 'DESC')
                            ->select('contents.*')
                            ->distinct('contents.content_id')
                            ->paginate(28);
                    }
                } elseif ($langs != "") {
                    if ($minr == 0 && $maxr == 10) {
                        $contents = DB::table('content_providers')
                            ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                            ->where('content_providers.provider_id', $provider)
                            ->where('contents.content_type', $type)
                            ->where('contents.original_language', $langs)
                            ->where('contents.cinema_release_date', '<=', $maxy)
                            ->where('contents.cinema_release_date', '>=', $miny)
                            ->where('contents.parent_id', '0')
                            ->select('contents.*')
                            ->distinct('contents.content_id')
                            ->paginate(28);
                    } else {
                        $contents = DB::table('content_providers')
                            ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                            ->where('content_providers.provider_id', $provider)
                            ->where('contents.content_type', $type)
                            ->where('contents.original_language', $langs)
                            ->where('contents.tmdb_score', '<=', $maxr)
                            ->where('contents.tmdb_score', '>=', $minr)
                            ->where('contents.cinema_release_date', '<=', $maxy)
                            ->where('contents.parent_id', '0')
                            ->where('contents.cinema_release_date', '>=', $miny)
                            ->select('contents.*')
                            ->distinct('contents.content_id')
                            ->paginate(28);
                    }
                } elseif ($genre != "") {
                    if ($minr == 0 && $maxr == 10) {
                        $contents = DB::table('content_providers')
                            ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                            ->leftJoin('content_genres', 'content_genres.content_id', 'contents.content_id')
                            ->where('content_providers.provider_id', $provider)
                            ->where('content_genres.genre_id', $genre)
                            ->where('contents.content_type', $type)
                            ->where('contents.cinema_release_date', '<=', $maxy)
                            ->where('contents.cinema_release_date', '>=', $miny)
                            ->where('contents.parent_id', '0')
                            ->orderBy('content_genres.content_id', 'DESC')
                            ->select('contents.*')
                            ->distinct('contents.content_id')
                            ->paginate(28);
                    } else {
                        $contents = DB::table('content_providers')
                            ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                            ->leftJoin('content_genres', 'content_genres.content_id', 'contents.content_id')
                            ->where('content_providers.provider_id', $provider)
                            ->where('content_genres.genre_id', $genre)
                            ->where('contents.content_type', $type)
                            ->where('contents.tmdb_score', '<=', $maxr)
                            ->where('contents.tmdb_score', '>=', $minr)
                            ->where('contents.cinema_release_date', '<=', $maxy)
                            ->where('contents.cinema_release_date', '>=', $miny)
                            ->where('contents.parent_id', '0')
                            ->orderBy('content_genres.content_id', 'DESC')
                            ->select('contents.*')
                            ->distinct('contents.content_id')
                            ->paginate(28);
                    }
                } elseif ($rated != "") {


                    if ($minr == 0 && $maxr == 10) {
                        $contents = DB::table('content_providers')
                            ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                            ->where('content_providers.provider_id', $provider)
                            ->where('contents.content_type', $type)
                            ->where('contents.age_certification', $rated)
                            ->where('contents.cinema_release_date', '<=', $maxy)
                            ->where('contents.cinema_release_date', '>=', $miny)
                            ->where('contents.parent_id', '0')
                            ->select('contents.*')
                            ->distinct('contents.content_id')
                            ->paginate(28);
                    } else {
                        $contents = DB::table('content_providers')
                            ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                            ->where('content_providers.provider_id', $provider)
                            ->where('contents.content_type', $type)
                            ->where('contents.age_certification', $rated)
                            ->where('contents.tmdb_score', '<=', $maxr)
                            ->where('contents.tmdb_score', '>=', $minr)
                            ->where('contents.cinema_release_date', '<=', $maxy)
                            ->where('contents.cinema_release_date', '>=', $miny)
                            ->where('contents.parent_id', '0')
                            ->select('contents.*')
                            ->distinct('contents.content_id')
                            ->paginate(28);
                    }
                } else {
                    if ($minr == 0 && $maxr == 10) {
                        $contents = DB::table('content_providers')
                            ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                            ->where('content_providers.provider_id', $provider)
                            ->where('contents.content_type', $type)
                            ->where('contents.cinema_release_date', '<=', $maxy)
                            ->where('contents.cinema_release_date', '>=', $miny)
                            ->where('contents.parent_id', '0')
                            ->select('contents.*')
                            ->distinct('contents.content_id')
                            ->paginate(28);
                    } else {
                        $contents = DB::table('content_providers')
                            ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                            ->where('content_providers.provider_id', $provider)
                            ->where('contents.content_type', $type)
                            ->where('contents.tmdb_score', '<=', $maxr)
                            ->where('contents.tmdb_score', '>=', $minr)
                            ->where('contents.cinema_release_date', '<=', $maxy)
                            ->where('contents.cinema_release_date', '>=', $miny)
                            ->where('contents.parent_id', '0')
                            ->select('contents.*')
                            ->distinct('contents.content_id')
                            ->paginate(28);
                    }
                }
            } else {

                if ($langs != "" && $rated != "" && $genre != "") {
                    if ($minr == 0 && $maxr == 10) {
                        $contents = DB::table('content_providers')
                            ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                            ->leftJoin('content_genres', 'content_genres.content_id', 'contents.content_id')
                            ->where('content_providers.provider_id', $provider)
                            ->where('content_genres.genre_id', $genre)
                            ->where('contents.age_certification', $rated)
                            ->where('contents.content_type', $type)
                            ->where('contents.original_language', $langs)
                            ->where('contents.cinema_release_date', '<=', $maxy)
                            ->where('contents.cinema_release_date', '>=', $miny)
                            ->where('contents.parent_id', '0')
                            ->orderBy($field, $sort_type)
                            ->select('contents.*')
                            ->distinct('contents.content_id')
                            ->paginate(28);
                    } else {
                        $contents = DB::table('content_genres')
                            ->leftJoin('contents', 'content_genres.content_id', 'contents.content_id')
                            ->where('content_genres.genre_id', $genre)
                            ->where('contents.content_type', $type)
                            ->where('contents.age_certification', $rated)
                            ->where('contents.original_language', $langs)
                            ->where('contents.tmdb_score', '<=', $maxr)
                            ->where('contents.tmdb_score', '>=', $minr)
                            ->where('contents.cinema_release_date', '<=', $maxy)
                            ->where('contents.cinema_release_date', '>=', $miny)
                            ->where('contents.parent_id', '0')
                            ->orderBy($field, $sort_type)
                            ->paginate(28);
                    }
                } elseif ($langs != "" && $rated != "") {
                    if ($minr == 0 && $maxr == 10) {
                        $contents = DB::table('content_providers')
                            ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                            ->where('content_providers.provider_id', $provider)
                            ->where('contents.content_type', $type)
                            ->where('contents.original_language', $langs)
                            ->where('contents.age_certification', $rated)
                            ->where('contents.cinema_release_date', '<=', $maxy)
                            ->where('contents.cinema_release_date', '>=', $miny)
                            ->where('contents.parent_id', '0')
                            ->orderBy($field, $sort_type)
                            ->select('contents.*')
                            ->distinct('contents.content_id')
                            ->paginate(28);
                    } else {
                        $contents = DB::table('content_providers')
                            ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                            ->where('content_providers.provider_id', $provider)
                            ->where('contents.content_type', $type)
                            ->where('contents.original_language', $langs)
                            ->where('contents.age_certification', $rated)
                            ->where('contents.tmdb_score', '<=', $maxr)
                            ->where('contents.tmdb_score', '>=', $minr)
                            ->where('contents.cinema_release_date', '<=', $maxy)
                            ->where('contents.cinema_release_date', '>=', $miny)
                            ->where('contents.parent_id', '0')
                            ->orderBy($field, $sort_type)
                            ->select('contents.*')
                            ->distinct('contents.content_id')
                            ->paginate(28);
                    }
                } elseif ($rated != "" && $genre != "") {
                    if ($minr == 0 && $maxr == 10) {
                        $contents = DB::table('content_providers')
                            ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                            ->leftJoin('content_genres', 'content_genres.content_id', 'contents.content_id')
                            ->where('content_providers.provider_id', $provider)
                            ->where('content_genres.genre_id', $genre)
                            ->where('contents.age_certification', $rated)
                            ->where('contents.content_type', $type)
                            ->where('contents.cinema_release_date', '<=', $maxy)
                            ->where('contents.cinema_release_date', '>=', $miny)
                            ->where('contents.parent_id', '0')
                            ->orderBy($field, $sort_type)
                            ->select('contents.*')
                            ->distinct('contents.content_id')
                            ->paginate(28);
                    } else {
                        $contents = DB::table('content_providers')
                            ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                            ->leftJoin('content_genres', 'content_genres.content_id', 'contents.content_id')
                            ->where('content_providers.provider_id', $provider)
                            ->where('content_genres.genre_id', $genre)
                            ->where('contents.age_certification', $rated)
                            ->where('contents.content_type', $type)
                            ->where('contents.tmdb_score', '<=', $maxr)
                            ->where('contents.tmdb_score', '>=', $minr)
                            ->where('contents.cinema_release_date', '<=', $maxy)
                            ->where('contents.cinema_release_date', '>=', $miny)
                            ->where('contents.parent_id', '0')
                            ->orderBy($field, $sort_type)
                            ->select('contents.*')
                            ->distinct('contents.content_id')
                            ->paginate(28);
                    }
                } elseif ($langs != "" && $genre != "") {
                    if ($minr == 0 && $maxr == 10) {
                        $contents = DB::table('content_providers')
                            ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                            ->leftJoin('content_genres', 'content_genres.content_id', 'contents.content_id')
                            ->where('content_providers.provider_id', $provider)
                            ->where('content_genres.genre_id', $genre)
                            ->where('contents.content_type', $type)
                            ->where('contents.original_language', $langs)
                            ->where('contents.cinema_release_date', '<=', $maxy)
                            ->where('contents.cinema_release_date', '>=', $miny)
                            ->where('contents.parent_id', '0')
                            ->orderBy($field, $sort_type)
                            ->select('contents.*')
                            ->distinct('contents.content_id')
                            ->paginate(28);
                    } else {
                        $contents = DB::table('content_providers')
                            ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                            ->leftJoin('content_genres', 'content_genres.content_id', 'contents.content_id')
                            ->where('content_providers.provider_id', $provider)
                            ->where('content_genres.genre_id', $genre)
                            ->where('contents.content_type', $type)
                            ->where('contents.original_language', $langs)
                            ->where('contents.tmdb_score', '>=', $minr)
                            ->where('contents.tmdb_score', '<=', $maxr)
                            ->where('contents.cinema_release_date', '<=', $maxy)
                            ->where('contents.cinema_release_date', '>=', $miny)
                            ->where('contents.parent_id', '0')
                            ->orderBy($field, $sort_type)
                            ->select('contents.*')
                            ->distinct('contents.content_id')
                            ->paginate(28);
                    }
                } elseif ($langs != "") {
                    if ($minr == 0 && $maxr == 10) {
                        $contents = DB::table('content_providers')
                            ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                            ->where('content_providers.provider_id', $provider)
                            ->where('contents.content_type', $type)
                            ->where('contents.original_language', $langs)
                            ->where('contents.cinema_release_date', '<=', $maxy)
                            ->where('contents.cinema_release_date', '>=', $miny)
                            ->where('contents.parent_id', '0')
                            ->orderBy($field, $sort_type)
                            ->select('contents.*')
                            ->distinct('contents.content_id')
                            ->paginate(28);
                    } else {
                        $contents = DB::table('content_providers')
                            ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                            ->where('content_providers.provider_id', $provider)
                            ->where('contents.content_type', $type)
                            ->where('contents.original_language', $langs)
                            ->where('contents.tmdb_score', '<=', $maxr)
                            ->where('contents.tmdb_score', '>=', $minr)
                            ->where('contents.cinema_release_date', '<=', $maxy)
                            ->where('contents.cinema_release_date', '>=', $miny)
                            ->where('contents.parent_id', '0')
                            ->orderBy($field, $sort_type)
                            ->select('contents.*')
                            ->distinct('contents.content_id')
                            ->paginate(28);
                    }
                } elseif ($genre != "") {
                    if ($minr == 0 && $maxr == 10) {
                        $contents = DB::table('content_providers')
                            ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                            ->leftJoin('content_genres', 'content_genres.content_id', 'contents.content_id')
                            ->where('content_providers.provider_id', $provider)
                            ->where('content_genres.genre_id', $genre)
                            ->where('contents.content_type', $type)
                            ->where('contents.cinema_release_date', '<=', $maxy)
                            ->where('contents.cinema_release_date', '>=', $miny)
                            ->where('contents.parent_id', '0')
                            ->orderBy($field, $sort_type)
                            ->select('contents.*')
                            ->distinct('contents.content_id')
                            ->paginate(28);
                    } else {
                        $contents = DB::table('content_providers')
                            ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                            ->leftJoin('content_genres', 'content_genres.content_id', 'contents.content_id')
                            ->where('content_providers.provider_id', $provider)
                            ->where('content_genres.genre_id', $genre)
                            ->where('contents.content_type', $type)
                            ->where('contents.tmdb_score', '>=', $minr)
                            ->where('contents.tmdb_score', '<=', $maxr)
                            ->where('contents.cinema_release_date', '<=', $maxy)
                            ->where('contents.cinema_release_date', '>=', $miny)
                            ->where('contents.parent_id', '0')
                            ->orderBy($field, $sort_type)
                            ->select('contents.*')
                            ->distinct('contents.content_id')
                            ->paginate(28);
                    }
                } elseif ($rated != "") {
                    if ($minr == 0 && $maxr == 10) {
                        $contents = DB::table('content_providers')
                            ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                            ->where('content_providers.provider_id', $provider)
                            ->where('contents.content_type', $type)
                            ->where('contents.age_certification', $rated)
                            ->where('contents.cinema_release_date', '<=', $maxy)
                            ->where('contents.cinema_release_date', '>=', $miny)
                            ->where('contents.parent_id', '0')
                            ->orderBy($field, $sort_type)
                            ->select('contents.*')
                            ->distinct('contents.content_id')
                            ->paginate(28);
                    } else {
                        $contents = DB::table('content_providers')
                            ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                            ->where('content_providers.provider_id', $provider)
                            ->where('contents.content_type', $type)
                            ->where('contents.original_language', $langs)
                            ->where('contents.tmdb_score', '<=', $maxr)
                            ->where('contents.tmdb_score', '>=', $minr)
                            ->where('contents.cinema_release_date', '<=', $maxy)
                            ->where('contents.cinema_release_date', '>=', $miny)
                            ->where('contents.parent_id', '0')
                            ->orderBy($field, $sort_type)
                            ->select('contents.*')
                            ->distinct('contents.content_id')
                            ->paginate(28);
                    }
                } else {
                    if ($minr == 0 && $maxr == 10) {
                        $contents = DB::table('content_providers')
                            ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                            ->where('content_providers.provider_id', $provider)
                            ->where('contents.content_type', $type)
                            ->where('contents.cinema_release_date', '<=', $maxy)
                            ->where('contents.cinema_release_date', '>=', $miny)
                            ->where('contents.parent_id', '0')
                            ->orderBy($field, $sort_type)
                            ->select('contents.*')
                            ->distinct('contents.content_id')
                            ->paginate(28);
                    } else {
                        $contents = DB::table('content_providers')
                            ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                            ->where('content_providers.provider_id', $provider)
                            ->where('contents.content_type', $type)
                            ->where('contents.tmdb_score', '<=', $maxr)
                            ->where('contents.tmdb_score', '>=', $minr)
                            ->where('contents.cinema_release_date', '<=', $maxy)
                            ->where('contents.cinema_release_date', '>=', $miny)
                            ->where('contents.parent_id', '0')
                            ->orderBy($field, $sort_type)
                            ->select('contents.*')
                            ->distinct('contents.content_id')
                            ->paginate(28);
                    }
                }
            }
        }
        $miny = explode('-', $miny)[0];
        $maxy = explode('-', $maxy)[0];

        if (isset($_GET['provider'])) {
            $provider = $_GET['provider'];
        }

        $contents = $contents->appends(\Input::except('page'));
        foreach ($contents as $key => $value) {
            $this->resizeImage($value->poster);
        }
        return view('khoj.home', compact('contents', 'page', 'type', 'miny', 'maxy', 'langs', 'genre', 'rated', 'minr', 'maxr', 'sort', 'tab', 'provider'));
    }


    //For Trending Movies
    public function getTrendingMovies(Request $request)
    {
        $date = date('Y-m-d');
        $page = 0;
        $langs = "";
        $rated = "";
        $sort = "";
        $field = "";
        $sort_type = "";
        $provider = "";
        $genre = "";
        $minr = 0;
        $maxr = 10;
        $type = "all";
        $tab = 'Trending';

        // $miny = DB::table('contents')->where('original_release_year','>','1900')->min('original_release_year');
        $miny = '0000-00-00';
        $maxy = $date;

        if (isset($_GET['page'])) {
            $page = $_GET['page'] - 1;
        }

        if (isset($_GET['lang'])) {
            $langs = $_GET['lang'];
        }
        if (isset($_GET['provider'])) {
            $provider = explode('_', $_GET['provider'])[1];
        }

        if (isset($_GET['sort'])) {
            $sort = $_GET['sort'];
            if ($sort == "alpha") {
                $field = 'title';
                $sort_type = 'ASC';
            } elseif ($sort == 'rate') {
                $field = 'tmdb_score';
                $sort_type = 'DESC';
            } elseif ($sort == 'year') {
                $field = 'cinema_release_date';
                $sort_type = 'DESC';
            } elseif ($sort == 'imdb_score') {
                $field = 'imdb_score';
                $sort_type = 'DESC';
            } else {
                $field = 'content_id';
                $sort_type = 'DESC';
            }
        }

        if ((isset($_GET['sort']) && isset($_GET['genre'])) || (isset($_GET['sort']) && isset($_GET['provider']))) {
            $sort = $_GET['sort'];
            if ($sort == "alpha") {
                $field = 'contents.title';
                $sort_type = 'ASC';
            } elseif ($sort == 'rate') {
                $field = 'contents.tmdb_score';
                $sort_type = 'DESC';
            } elseif ($sort == 'year') {
                $field = 'contents.cinema_release_date';
                $sort_type = 'DESC';
            } elseif ($sort == 'imdb_score') {
                $field = 'contents.imdb_score';
                $sort_type = 'DESC';
            } else {
                $field = 'contents.content_id';
                $sort_type = 'DESC';
            }
        }

        if (isset($_GET['rated'])) {
            $rated = $_GET['rated'];
        }

        if (isset($_GET['searchtype'])) {
            $type = $_GET['searchtype'];
        }

        if (isset($_GET['genre'])) {
            $genre = $_GET['genre'];
        }

        if (isset($_GET['minr'])) {
            $minr = $_GET['minr'];
        }
        if (isset($_GET['maxr'])) {
            $maxr = $_GET['maxr'];
        }

        if (isset($_GET['miny'])) {
            $miny = $_GET['miny'] . "-01-01";
        }
        if (isset($_GET['maxy'])) {
            $maxy = $_GET['maxy'] . "-12-31";
        }


        if ($type == 'all') {

            if ($provider == "") {

                if ($sort == "") {


                    if ($langs != "" && $rated != "" && $genre != "") {
                        $contents = DB::table('content_genres')
                            ->leftJoin('contents', 'content_genres.content_id', 'contents.content_id')
                            ->where('content_genres.genre_id', $genre)
                            ->where('contents.tmdb_score', '<=', $maxr)
                            ->where('contents.tmdb_score', '>=', $minr)
                            ->where('contents.cinema_release_date', '<=', $maxy)
                            ->where('contents.cinema_release_date', '>=', $miny)
                            ->where('contents.age_certification', $rated)
                            ->where('contents.original_language', $langs)
                            ->where('contents.content_type', '!=', 'tv-show')
                            ->orderBy('contents.click_count', 'DESC')
                            ->paginate(28);
                    } elseif ($langs != "" && $rated != "") {
                        $contents = DB::table('contents')
                            ->where('original_language', $langs)
                            ->where('age_certification', $rated)
                            ->where('tmdb_score', '<=', $maxr)
                            ->where('tmdb_score', '>=', $minr)
                            ->where('cinema_release_date', '<=', $maxy)
                            ->where('cinema_release_date', '>=', $miny)
                            ->where('content_type', '!=', 'tv-show')
                            ->orderBy('click_count', 'DESC')
                            ->paginate(28);
                    } elseif ($rated != "" && $genre != "") {
                        $contents = DB::table('content_genres')
                            ->leftJoin('contents', 'content_genres.content_id', 'contents.content_id')
                            ->where('content_genres.genre_id', $genre)
                            ->where('contents.age_certification', $rated)
                            ->where('contents.tmdb_score', '<=', $maxr)
                            ->where('contents.tmdb_score', '>=', $minr)
                            ->where('contents.cinema_release_date', '<=', $maxy)
                            ->where('contents.cinema_release_date', '>=', $miny)
                            ->where('contents.content_type', '!=', 'tv-show')
                            ->orderBy('contents.click_count', 'DESC')
                            ->paginate(28);
                    } elseif ($langs != "" && $genre != "") {
                        $contents = DB::table('content_genres')
                            ->leftJoin('contents', 'content_genres.content_id', 'contents.content_id')
                            ->where('content_genres.genre_id', $genre)
                            ->where('contents.original_language', $langs)
                            ->where('contents.tmdb_score', '<=', $maxr)
                            ->where('contents.tmdb_score', '>=', $minr)
                            ->where('contents.cinema_release_date', '<=', $maxy)
                            ->where('contents.cinema_release_date', '>=', $miny)
                            ->where('contents.content_type', '!=', 'tv-show')
                            ->orderBy('contents.click_count', 'DESC')
                            ->paginate(28);
                    } elseif ($langs != "") {
                        $contents = DB::table('contents')
                            ->where('original_language', $langs)
                            ->where('tmdb_score', '<=', $maxr)
                            ->where('tmdb_score', '>=', $minr)
                            ->where('cinema_release_date', '<=', $maxy)
                            ->where('cinema_release_date', '>=', $miny)
                            ->where('content_type', '!=', 'tv-show')
                            ->orderBy('click_count', 'DESC')
                            ->paginate(28);
                    } elseif ($genre != "") {
                        $contents = DB::table('content_genres')
                            ->leftJoin('contents', 'content_genres.content_id', 'contents.content_id')
                            ->where('content_genres.genre_id', $genre)
                            ->where('contents.tmdb_score', '<=', $maxr)
                            ->where('contents.tmdb_score', '>=', $minr)
                            ->where('contents.cinema_release_date', '<=', $maxy)
                            ->where('contents.cinema_release_date', '>=', $miny)
                            ->where('contents.content_type', '!=', 'tv-show')
                            ->orderBy('contents.click_count', 'DESC')
                            ->paginate(28);
                    } elseif ($rated != "") {
                        $contents = DB::table('contents')
                            ->where('age_certification', $rated)
                            ->where('tmdb_score', '<=', $maxr)
                            ->where('tmdb_score', '>=', $minr)
                            ->where('cinema_release_date', '<=', $maxy)
                            ->where('cinema_release_date', '>=', $miny)
                            ->where('content_type', '!=', 'tv-show')
                            ->orderBy('click_count', 'DESC')
                            ->paginate(28);
                    } else {
                        $contents = DB::table('contents')
                            ->where('tmdb_score', '<=', $maxr)
                            ->where('tmdb_score', '>=', $minr)
                            ->where('cinema_release_date', '<=', $maxy)
                            ->where('cinema_release_date', '>=', $miny)
                            ->where('content_type', '!=', 'tv-show')
                            ->orderBy('click_count', 'DESC')
                            ->paginate(28);
                    }
                } else {

                    if ($langs != "" && $rated != "" && $genre != "") {
                        if ($minr == 0 && $maxr == 10) {
                            $contents = DB::table('content_genres')
                                ->leftJoin('contents', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_genres.genre_id', $genre)
                                ->where('contents.age_certification', $rated)
                                ->where('contents.original_language', $langs)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->where('contents.content_type', '!=', 'tv-show')
                                ->orderBy('contents.click_count', 'DESC')
                                ->orderBy($field, $sort_type)
                                ->paginate(28);
                        } else {
                            $contents = DB::table('content_genres')
                                ->leftJoin('contents', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_genres.genre_id', $genre)
                                ->where('contents.age_certification', $rated)
                                ->where('contents.original_language', $langs)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->where('contents.content_type', '!=', 'tv-show')
                                ->orderBy('contents.click_count', 'DESC')
                                ->orderBy($field, $sort_type)
                                ->paginate(28);
                        }
                    } elseif ($langs != "" && $rated != "") {
                        if ($minr == 0 && $maxr == 10) {
                            $contents = DB::table('contents')
                                ->where('original_language', $langs)
                                ->where('age_certification', $rated)
                                ->where('cinema_release_date', '<=', $maxy)
                                ->where('cinema_release_date', '>=', $miny)
                                ->where('content_type', '!=', 'tv-show')
                                ->orderBy('click_count', 'DESC')
                                ->orderBy($field, $sort_type)
                                ->paginate(28);
                        } else {
                            $contents = DB::table('contents')
                                ->where('original_language', $langs)
                                ->where('age_certification', $rated)
                                ->where('tmdb_score', '<=', $maxr)
                                ->where('tmdb_score', '>=', $minr)
                                ->where('cinema_release_date', '<=', $maxy)
                                ->where('cinema_release_date', '>=', $miny)
                                ->where('content_type', '!=', 'tv-show')
                                ->orderBy('click_count', 'DESC')
                                ->orderBy($field, $sort_type)
                                ->paginate(28);
                        }
                    } elseif ($rated != "" && $genre != "") {
                        if ($minr == 0 && $maxr == 10) {
                            $contents = DB::table('content_genres')
                                ->leftJoin('contents', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_genres.genre_id', $genre)
                                ->where('contents.age_certification', $rated)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->where('contents.content_type', '!=', 'tv-show')
                                ->orderBy('contents.click_count', 'DESC')
                                ->orderBy($field, $sort_type)
                                ->paginate(28);
                        } else {
                            $contents = DB::table('content_genres')
                                ->leftJoin('contents', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_genres.genre_id', $genre)
                                ->where('contents.age_certification', $rated)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->where('contents.content_type', '!=', 'tv-show')
                                ->orderBy('contents.click_count', 'DESC')
                                ->orderBy($field, $sort_type)
                                ->paginate(28);
                        }
                    } elseif ($langs != "" && $genre != "") {
                        if ($minr == 0 && $maxr == 10) {
                            $contents = DB::table('content_genres')
                                ->leftJoin('contents', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_genres.genre_id', $genre)
                                ->where('contents.original_language', $langs)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->where('contents.content_type', '!=', 'tv-show')
                                ->orderBy('contents.click_count', 'DESC')
                                ->orderBy($field, $sort_type)
                                ->paginate(28);
                        } else {
                            $contents = DB::table('content_genres')
                                ->leftJoin('contents', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_genres.genre_id', $genre)
                                ->where('contents.original_language', $langs)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->where('contents.content_type', '!=', 'tv-show')
                                ->orderBy('contents.click_count', 'DESC')
                                ->orderBy($field, $sort_type)
                                ->paginate(28);
                        }
                    } elseif ($langs != "") {
                        if ($minr == 0 && $maxr == 10) {
                            $contents = DB::table('contents')
                                ->where('original_language', $langs)
                                ->where('cinema_release_date', '<=', $maxy)
                                ->where('cinema_release_date', '>=', $miny)
                                ->where('content_type', '!=', 'tv-show')
                                ->orderBy('click_count', 'DESC')
                                ->orderBy($field, $sort_type)
                                ->paginate(28);
                        } else {
                            $contents = DB::table('contents')
                                ->where('original_language', $langs)
                                ->where('tmdb_score', '<=', $maxr)
                                ->where('tmdb_score', '>=', $minr)
                                ->where('cinema_release_date', '<=', $maxy)
                                ->where('cinema_release_date', '>=', $miny)
                                ->where('content_type', '!=', 'tv-show')
                                ->orderBy('click_count', 'DESC')
                                ->orderBy($field, $sort_type)
                                ->paginate(28);
                        }
                    } elseif ($genre != "") {
                        if ($minr == 0 && $maxr == 10) {
                            $contents = DB::table('content_genres')
                                ->leftJoin('contents', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_genres.genre_id', $genre)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->where('contents.content_type', '!=', 'tv-show')
                                ->orderBy('contents.click_count', 'DESC')
                                ->orderBy($field, $sort_type)
                                ->paginate(28);
                        } else {
                            $contents = DB::table('content_genres')
                                ->leftJoin('contents', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_genres.genre_id', $genre)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->where('contents.content_type', '!=', 'tv-show')
                                ->orderBy('contents.click_count', 'DESC')
                                ->orderBy($field, $sort_type)
                                ->paginate(28);
                        }
                    } elseif ($rated != "") {
                        if ($minr == 0 && $maxr == 10) {
                            $contents = DB::table('contents')
                                ->where('age_certification', $rated)
                                ->where('cinema_release_date', '<=', $maxy)
                                ->where('cinema_release_date', '>=', $miny)
                                ->where('content_type', '!=', 'tv-show')
                                ->orderBy('click_count', 'DESC')
                                ->orderBy($field, $sort_type)
                                ->paginate(28);
                        } else {
                            $contents = DB::table('contents')
                                ->where('age_certification', $rated)
                                ->where('tmdb_score', '<=', $maxr)
                                ->where('tmdb_score', '>=', $minr)
                                ->where('cinema_release_date', '<=', $maxy)
                                ->where('cinema_release_date', '>=', $miny)
                                ->where('content_type', '!=', 'tv-show')
                                ->orderBy('click_count', 'DESC')
                                ->orderBy($field, $sort_type)
                                ->paginate(28);
                        }
                    } else {
                        if ($minr == 0 && $maxr == 10) {
                            $contents = DB::table('contents')
                                ->where('cinema_release_date', '<=', $maxy)
                                ->where('cinema_release_date', '>=', $miny)
                                ->where('content_type', '!=', 'tv-show')
                                ->orderBy('click_count', 'DESC')
                                ->orderBy($field, $sort_type)
                                ->paginate(28);
                        } else {
                            $contents = DB::table('contents')
                                ->where('tmdb_score', '<=', $maxr)
                                ->where('tmdb_score', '>=', $minr)
                                ->where('cinema_release_date', '<=', $maxy)
                                ->where('cinema_release_date', '>=', $miny)
                                ->where('content_type', '!=', 'tv-show')
                                ->orderBy('click_count', 'DESC')
                                ->orderBy($field, $sort_type)
                                ->paginate(28);
                        }
                    }
                }
            }

            //When Provider Not NULL
            else {
                if ($sort == "") {
                    if ($langs != "" && $rated != "" && $genre != "") {
                        if ($minr == 0 && $maxr == 10) {
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->leftJoin('content_genres', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_providers.provider_id', $provider)
                                ->where('content_genres.genre_id', $genre)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->where('contents.age_certification', $rated)
                                ->where('contents.original_language', $langs)
                                ->where('contents.content_type', '!=', 'tv-show')
                                ->orderBy('contents.click_count', 'DESC')
                                ->orderBy('content_genres.content_id', 'DESC')
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate(28);
                        } else {
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->leftJoin('content_genres', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_providers.provider_id', $provider)
                                ->where('content_genres.genre_id', $genre)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->where('contents.age_certification', $rated)
                                ->where('contents.original_language', $langs)
                                ->where('contents.content_type', '!=', 'tv-show')
                                ->orderBy('contents.click_count', 'DESC')
                                ->orderBy('content_genres.content_id', 'DESC')
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate(28);
                        }
                    } elseif ($langs != "" && $rated != "") {
                        if ($minr == 0 && $maxr == 10) {
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->where('content_providers.provider_id', $provider)
                                ->where('contents.original_language', $langs)
                                ->where('contents.age_certification', $rated)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->where('contents.content_type', '!=', 'tv-show')
                                ->orderBy('contents.click_count', 'DESC')
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate(28);
                        } else {
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->where('content_providers.provider_id', $provider)
                                ->where('contents.original_language', $langs)
                                ->where('contents.age_certification', $rated)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->where('contents.content_type', '!=', 'tv-show')
                                ->orderBy('contents.click_count', 'DESC')
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate(28);
                        }
                    } elseif ($rated != "" && $genre != "") {
                        if ($minr == 0 && $maxr == 10) {
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->leftJoin('content_genres', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_providers.provider_id', $provider)
                                ->where('content_genres.genre_id', $genre)
                                ->where('contents.age_certification', $rated)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->where('contents.content_type', '!=', 'tv-show')
                                ->orderBy('contents.click_count', 'DESC')
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate(28);
                        } else {
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->leftJoin('content_genres', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_providers.provider_id', $provider)
                                ->where('content_genres.genre_id', $genre)
                                ->where('contents.age_certification', $rated)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->where('contents.content_type', '!=', 'tv-show')
                                ->orderBy('contents.click_count', 'DESC')
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate(28);
                        }
                    } elseif ($langs != "" && $genre != "") {
                        if ($minr == 0 && $maxr == 10) {
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->leftJoin('content_genres', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_providers.provider_id', $provider)
                                ->where('content_genres.genre_id', $genre)
                                ->where('contents.original_language', $langs)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->where('contents.content_type', '!=', 'tv-show')
                                ->orderBy('contents.click_count', 'DESC')
                                ->orderBy('content_genres.content_id', 'DESC')
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate(28);
                        } else {
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->leftJoin('content_genres', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_providers.provider_id', $provider)
                                ->where('content_genres.genre_id', $genre)
                                ->where('contents.original_language', $langs)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->where('contents.content_type', '!=', 'tv-show')
                                ->orderBy('contents.click_count', 'DESC')
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate(28);
                        }
                    } elseif ($langs != "") {
                        if ($minr == 0 && $maxr == 10) {
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->where('content_providers.provider_id', $provider)
                                ->where('contents.original_language', $langs)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->where('contents.content_type', '!=', 'tv-show')
                                ->orderBy('contents.click_count', 'DESC')
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate(28);
                        } else {
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->where('content_providers.provider_id', $provider)
                                ->where('contents.original_language', $langs)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->where('contents.content_type', '!=', 'tv-show')
                                ->orderBy('contents.click_count', 'DESC')
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate(28);
                        }
                    } elseif ($genre != "") {
                        if ($minr == 0 && $maxr == 10) {
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->leftJoin('content_genres', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_providers.provider_id', $provider)
                                ->where('content_genres.genre_id', $genre)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->where('contents.content_type', '!=', 'tv-show')
                                ->orderBy('contents.click_count', 'DESC')
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate(28);
                        } else {
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->leftJoin('content_genres', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_providers.provider_id', $provider)
                                ->where('content_genres.genre_id', $genre)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->where('contents.content_type', '!=', 'tv-show')
                                ->orderBy('contents.click_count', 'DESC')
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate(28);
                        }
                    } elseif ($rated != "") {
                        if ($minr == 0 && $maxr == 10) {
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->where('content_providers.provider_id', $provider)
                                ->where('contents.age_certification', $rated)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->where('contents.content_type', '!=', 'tv-show')
                                ->orderBy('contents.click_count', 'DESC')
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate(28);
                        } else {
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->where('content_providers.provider_id', $provider)
                                ->where('contents.age_certification', $rated)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->where('contents.content_type', '!=', 'tv-show')
                                ->orderBy('contents.click_count', 'DESC')
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate(28);
                        }
                    } else {
                        if ($minr == 0 && $maxr == 10) {
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->where('content_providers.provider_id', $provider)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->where('contents.content_type', '!=', 'tv-show')
                                ->orderBy('contents.click_count', 'DESC')
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate(28);
                        } else {
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->where('content_providers.provider_id', $provider)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->where('contents.content_type', '!=', 'tv-show')
                                ->orderBy('contents.click_count', 'DESC')
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate(28);
                        }
                    }
                } else {

                    if ($langs != "" && $rated != "" && $genre != "") {

                        if ($minr == 0 && $maxr == 10) {
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->leftJoin('content_genres', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_providers.provider_id', $provider)
                                ->where('content_genres.genre_id', $genre)
                                ->where('contents.age_certification', $rated)
                                ->where('contents.original_language', $langs)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->where('contents.content_type', '!=', 'tv-show')
                                ->orderBy('contents.click_count', 'DESC')
                                ->orderBy($field, $sort_type)
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate(28);
                        } else {
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->leftJoin('content_genres', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_providers.provider_id', $provider)
                                ->where('content_genres.genre_id', $genre)
                                ->where('contents.age_certification', $rated)
                                ->where('contents.original_language', $langs)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->where('contents.content_type', '!=', 'tv-show')
                                ->orderBy('contents.click_count', 'DESC')
                                ->orderBy($field, $sort_type)
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate(28);
                        }
                    } elseif ($langs != "" && $rated != "") {
                        if ($minr == 0 && $maxr == 10) {
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->where('content_providers.provider_id', $provider)
                                ->where('contents.original_language', $langs)
                                ->where('contents.age_certification', $rated)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->where('contents.content_type', '!=', 'tv-show')
                                ->orderBy('contents.click_count', 'DESC')
                                ->orderBy($field, $sort_type)
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate(28);
                        } else {
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->where('content_providers.provider_id', $provider)
                                ->where('contents.original_language', $langs)
                                ->where('contents.age_certification', $rated)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->where('contents.content_type', '!=', 'tv-show')
                                ->orderBy('contents.click_count', 'DESC')
                                ->orderBy($field, $sort_type)
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate(28);
                        }
                    } elseif ($rated != "" && $genre != "") {
                        if ($minr == 0 && $maxr == 10) {
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->leftJoin('content_genres', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_providers.provider_id', $provider)
                                ->where('content_genres.genre_id', $genre)
                                ->where('contents.age_certification', $rated)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->where('contents.content_type', '!=', 'tv-show')
                                ->orderBy('contents.click_count', 'DESC')
                                ->orderBy($field, $sort_type)
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate(28);
                        } else {
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->leftJoin('content_genres', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_providers.provider_id', $provider)
                                ->where('content_genres.genre_id', $genre)
                                ->where('contents.age_certification', $rated)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->where('contents.content_type', '!=', 'tv-show')
                                ->orderBy('contents.click_count', 'DESC')
                                ->orderBy($field, $sort_type)
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate(28);
                        }
                    } elseif ($langs != "" && $genre != "") {
                        if ($minr == 0 && $maxr == 10) {
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->leftJoin('content_genres', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_providers.provider_id', $provider)
                                ->where('content_genres.genre_id', $genre)
                                ->where('contents.original_language', $langs)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->where('contents.content_type', '!=', 'tv-show')
                                ->orderBy('contents.click_count', 'DESC')
                                ->orderBy($field, $sort_type)
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate(28);
                        } else {
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->leftJoin('content_genres', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_providers.provider_id', $provider)
                                ->where('content_genres.genre_id', $genre)
                                ->where('contents.original_language', $langs)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->where('contents.content_type', '!=', 'tv-show')
                                ->orderBy('contents.click_count', 'DESC')
                                ->orderBy($field, $sort_type)
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate(28);
                        }
                    } elseif ($langs != "") {
                        if ($minr == 0 && $maxr == 10) {
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->where('content_providers.provider_id', $provider)
                                ->where('contents.original_language', $langs)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->where('contents.content_type', '!=', 'tv-show')
                                ->orderBy('contents.click_count', 'DESC')
                                ->orderBy($field, $sort_type)
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate(28);
                        } else {
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->where('content_providers.provider_id', $provider)
                                ->where('contents.original_language', $langs)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->where('contents.content_type', '!=', 'tv-show')
                                ->orderBy('contents.click_count', 'DESC')
                                ->orderBy($field, $sort_type)
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate(28);
                        }
                    } elseif ($genre != "") {
                        if ($minr == 0 && $maxr == 10) {
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->leftJoin('content_genres', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_providers.provider_id', $provider)
                                ->where('content_genres.genre_id', $genre)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->where('contents.content_type', '!=', 'tv-show')
                                ->orderBy('contents.click_count', 'DESC')
                                ->orderBy($field, $sort_type)
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate(28);
                        } else {
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->leftJoin('content_genres', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_providers.provider_id', $provider)
                                ->where('content_genres.genre_id', $genre)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->where('contents.content_type', '!=', 'tv-show')
                                ->orderBy('contents.click_count', 'DESC')
                                ->orderBy($field, $sort_type)
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate(28);
                        }
                    } elseif ($rated != "") {
                        if ($minr == 0 && $maxr == 10) {
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->where('content_providers.provider_id', $provider)
                                ->where('contents.age_certification', $rated)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->where('contents.content_type', '!=', 'tv-show')
                                ->orderBy('contents.click_count', 'DESC')
                                ->orderBy($field, $sort_type)
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate(28);
                        } else {
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->where('content_providers.provider_id', $provider)
                                ->where('contents.age_certification', $rated)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->where('contents.content_type', '!=', 'tv-show')
                                ->orderBy('contents.click_count', 'DESC')
                                ->orderBy($field, $sort_type)
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate(28);
                        }
                    } else {
                        if ($minr == 0 && $maxr == 10) {
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->where('content_providers.provider_id', $provider)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->where('contents.content_type', '!=', 'tv-show')
                                ->orderBy('contents.click_count', 'DESC')
                                ->orderBy($field, $sort_type)
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate(28);
                        } else {
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->where('content_providers.provider_id', $provider)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->where('contents.content_type', '!=', 'tv-show')
                                ->orderBy('contents.click_count', 'DESC')
                                ->orderBy($field, $sort_type)
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate(28);
                        }
                    }
                }
            }
        } else {

            $filter_type = array();
            if ($type == 'movie') {
                $filter_type = ['movie'];
            } else {
                $filter_type = ['shows', 'tv-show', 'show'];
            }

            if ($provider == "") {

                if ($sort == "") {
                    if ($langs != "" && $rated != "" && $genre != "") {
                        if ($minr == 0 && $maxr == 10) {
                            $contents = DB::table('content_genres')
                                ->leftJoin('contents', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_genres.genre_id', $genre)
                                ->whereIn('contents.content_type', $filter_type)
                                ->where('contents.age_certification', $rated)
                                ->where('contents.original_language', $langs)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->orderBy('contents.click_count', 'DESC')
                                ->paginate(28);
                        } else {
                            $contents = DB::table('content_genres')
                                ->leftJoin('contents', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_genres.genre_id', $genre)
                                ->whereIn('contents.content_type', $filter_type)
                                ->where('contents.age_certification', $rated)
                                ->where('contents.original_language', $langs)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->orderBy('contents.click_count', 'DESC')
                                ->paginate(28);
                        }
                    } elseif ($langs != "" && $rated != "") {
                        if ($minr == 0 && $maxr == 10) {
                            $contents = DB::table('contents')
                                ->whereIn('content_type', $filter_type)
                                ->where('original_language', $langs)
                                ->where('age_certification', $rated)
                                ->where('cinema_release_date', '<=', $maxy)
                                ->where('cinema_release_date', '>=', $miny)
                                ->orderBy('click_count', 'DESC')
                                ->paginate(28);
                        } else {
                            $contents = DB::table('contents')
                                ->whereIn('content_type', $filter_type)
                                ->where('original_language', $langs)
                                ->where('age_certification', $rated)
                                ->where('tmdb_score', '<=', $maxr)
                                ->where('tmdb_score', '>=', $minr)
                                ->where('cinema_release_date', '<=', $maxy)
                                ->where('cinema_release_date', '>=', $miny)
                                ->orderBy('click_count', 'DESC')
                                ->paginate(28);
                        }
                    } elseif ($rated != "" && $genre != "") {
                        if ($minr == 0 && $maxr == 10) {
                            $contents = DB::table('content_genres')
                                ->leftJoin('contents', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_genres.genre_id', $genre)
                                ->whereIn('contents.content_type', $filter_type)
                                ->where('contents.age_certification', $rated)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->orderBy('contents.click_count', 'DESC')
                                ->paginate(28);
                        } else {
                            $contents = DB::table('content_genres')
                                ->leftJoin('contents', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_genres.genre_id', $genre)
                                ->whereIn('contents.content_type', $filter_type)
                                ->where('contents.age_certification', $rated)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->orderBy('contents.click_count', 'DESC')
                                ->paginate(28);
                        }
                    } elseif ($langs != "" && $genre != "") {
                        if ($minr == 0 && $maxr == 10) {
                            $contents = DB::table('content_genres')
                                ->leftJoin('contents', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_genres.genre_id', $genre)
                                ->whereIn('contents.content_type', $filter_type)
                                ->where('contents.original_language', $langs)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->orderBy('contents.click_count', 'DESC')
                                ->paginate(28);
                        } else {
                            $contents = DB::table('content_genres')
                                ->leftJoin('contents', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_genres.genre_id', $genre)
                                ->whereIn('contents.content_type', $filter_type)
                                ->where('contents.original_language', $langs)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->orderBy('contents.click_count', 'DESC')
                                ->paginate(28);
                        }
                    } elseif ($langs != "") {
                        if ($minr == 0 && $maxr == 10) {
                            $contents = DB::table('contents')
                                ->whereIn('content_type', $filter_type)
                                ->where('original_language', $langs)
                                ->where('cinema_release_date', '<=', $maxy)
                                ->where('cinema_release_date', '>=', $miny)
                                ->orderBy('click_count', 'DESC')
                                ->paginate(28);
                        } else {
                            $contents = DB::table('contents')
                                ->whereIn('content_type', $filter_type)
                                ->where('original_language', $langs)
                                ->where('tmdb_score', '<=', $maxr)
                                ->where('tmdb_score', '>=', $minr)
                                ->where('cinema_release_date', '<=', $maxy)
                                ->where('cinema_release_date', '>=', $miny)
                                ->orderBy('click_count', 'DESC')
                                ->paginate(28);
                        }
                    } elseif ($genre != "") {
                        if ($minr == 0 && $maxr == 10) {
                            $contents = DB::table('content_genres')
                                ->leftJoin('contents', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_genres.genre_id', $genre)
                                ->whereIn('contents.content_type', $filter_type)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->orderBy('contents.click_count', 'DESC')
                                ->paginate(28);
                        } else {
                            $contents = DB::table('content_genres')
                                ->leftJoin('contents', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_genres.genre_id', $genre)
                                ->whereIn('contents.content_type', $filter_type)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->orderBy('contents.click_count', 'DESC')
                                ->paginate(28);
                        }
                    } elseif ($rated != "") {
                        if ($minr == 0 && $maxr == 10) {
                            $contents = DB::table('contents')
                                ->whereIn('content_type', $filter_type)
                                ->where('age_certification', $rated)
                                ->where('cinema_release_date', '<=', $maxy)
                                ->where('cinema_release_date', '>=', $miny)
                                ->orderBy('click_count', 'DESC')
                                ->paginate(28);
                        } else {
                            $contents = DB::table('contents')
                                ->whereIn('content_type', $filter_type)
                                ->where('age_certification', $rated)
                                ->where('tmdb_score', '<=', $maxr)
                                ->where('tmdb_score', '>=', $minr)
                                ->where('cinema_release_date', '<=', $maxy)
                                ->where('cinema_release_date', '>=', $miny)
                                ->orderBy('click_count', 'DESC')
                                ->paginate(28);
                        }
                    } else {
                        if ($minr == 0 && $maxr == 10) {
                            $contents = DB::table('contents')
                                ->whereIn('content_type', $filter_type)
                                ->where('cinema_release_date', '<=', $maxy)
                                ->where('cinema_release_date', '>=', $miny)
                                ->orderBy('click_count', 'DESC')
                                ->paginate(28);
                        } else {
                            $contents = DB::table('contents')
                                ->whereIn('content_type', $filter_type)
                                ->where('tmdb_score', '<=', $maxr)
                                ->where('tmdb_score', '>=', $minr)
                                ->where('cinema_release_date', '<=', $maxy)
                                ->where('cinema_release_date', '>=', $miny)
                                ->orderBy('click_count', 'DESC')
                                ->paginate(28);
                        }
                    }
                } else {

                    if ($langs != "" && $rated != "" && $genre != "") {
                        if ($minr == 0 && $maxr == 10) {
                            $contents = DB::table('content_genres')
                                ->leftJoin('contents', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_genres.genre_id', $genre)
                                ->whereIn('contents.content_type', $filter_type)
                                ->where('contents.age_certification', $rated)
                                ->where('contents.original_language', $langs)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->orderBy('contents.click_count', 'DESC')
                                ->orderBy($field, $sort_type)
                                ->paginate(28);
                        } else {
                            $contents = DB::table('content_genres')
                                ->leftJoin('contents', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_genres.genre_id', $genre)
                                ->whereIn('contents.content_type', $filter_type)
                                ->where('contents.age_certification', $rated)
                                ->where('contents.original_language', $langs)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->orderBy('contents.click_count', 'DESC')
                                ->orderBy($field, $sort_type)
                                ->paginate(28);
                        }
                    } elseif ($langs != "" && $rated != "") {
                        if ($minr == 0 && $maxr == 10) {
                            $contents = DB::table('contents')
                                ->whereIn('content_type', $filter_type)
                                ->where('original_language', $langs)
                                ->where('age_certification', $rated)
                                ->where('cinema_release_date', '<=', $maxy)
                                ->where('cinema_release_date', '>=', $miny)
                                ->orderBy('click_count', 'DESC')
                                ->orderBy($field, $sort_type)
                                ->paginate(28);
                        } else {
                            $contents = DB::table('contents')
                                ->whereIn('content_type', $filter_type)
                                ->where('original_language', $langs)
                                ->where('age_certification', $rated)
                                ->where('tmdb_score', '<=', $maxr)
                                ->where('tmdb_score', '>=', $minr)
                                ->where('cinema_release_date', '<=', $maxy)
                                ->where('cinema_release_date', '>=', $miny)
                                ->orderBy('click_count', 'DESC')
                                ->orderBy($field, $sort_type)
                                ->paginate(28);
                        }
                    } elseif ($rated != "" && $genre != "") {
                        if ($minr == 0 && $maxr == 10) {
                            $contents = DB::table('content_genres')
                                ->leftJoin('contents', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_genres.genre_id', $genre)
                                ->whereIn('contents.content_type', $filter_type)
                                ->where('contents.age_certification', $rated)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->orderBy('contents.click_count', 'DESC')
                                ->orderBy($field, $sort_type)
                                ->paginate(28);
                        } else {
                            $contents = DB::table('content_genres')
                                ->leftJoin('contents', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_genres.genre_id', $genre)
                                ->whereIn('contents.content_type', $filter_type)
                                ->where('contents.age_certification', $rated)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->orderBy('contents.click_count', 'DESC')
                                ->orderBy($field, $sort_type)
                                ->paginate(28);
                        }
                    } elseif ($langs != "" && $genre != "") {
                        if ($minr == 0 && $maxr == 10) {
                            $contents = DB::table('content_genres')
                                ->leftJoin('contents', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_genres.genre_id', $genre)
                                ->whereIn('contents.content_type', $filter_type)
                                ->where('contents.original_language', $langs)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->orderBy('contents.click_count', 'DESC')
                                ->orderBy($field, $sort_type)
                                ->paginate(28);
                        } else {
                            $contents = DB::table('content_genres')
                                ->leftJoin('contents', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_genres.genre_id', $genre)
                                ->whereIn('contents.content_type', $filter_type)
                                ->where('contents.original_language', $langs)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->orderBy('contents.click_count', 'DESC')
                                ->orderBy($field, $sort_type)
                                ->paginate(28);
                        }
                    } elseif ($langs != "") {
                        if ($minr == 0 && $maxr == 10) {
                            $contents = DB::table('contents')
                                ->whereIn('content_type', $filter_type)
                                ->where('original_language', $langs)
                                ->where('cinema_release_date', '<=', $maxy)
                                ->where('cinema_release_date', '>=', $miny)
                                ->orderBy('click_count', 'DESC')
                                ->orderBy($field, $sort_type)
                                ->paginate(28);
                        } else {
                            $contents = DB::table('contents')
                                ->whereIn('content_type', $filter_type)
                                ->where('original_language', $langs)
                                ->where('tmdb_score', '<=', $maxr)
                                ->where('tmdb_score', '>=', $minr)
                                ->where('cinema_release_date', '<=', $maxy)
                                ->where('cinema_release_date', '>=', $miny)
                                ->orderBy('click_count', 'DESC')
                                ->orderBy($field, $sort_type)
                                ->paginate(28);
                        }
                    } elseif ($genre != "") {
                        if ($minr == 0 && $maxr == 10) {
                            $contents = DB::table('content_genres')
                                ->leftJoin('contents', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_genres.genre_id', $genre)
                                ->whereIn('contents.content_type', $filter_type)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->orderBy('contents.click_count', 'DESC')
                                ->orderBy($field, $sort_type)
                                ->paginate(28);
                        } else {
                            $contents = DB::table('content_genres')
                                ->leftJoin('contents', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_genres.genre_id', $genre)
                                ->whereIn('contents.content_type', $filter_type)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->orderBy('contents.click_count', 'DESC')
                                ->orderBy($field, $sort_type)
                                ->paginate(28);
                        }
                    } elseif ($rated != "") {
                        if ($minr == 0 && $maxr == 10) {
                            $contents = DB::table('contents')
                                ->whereIn('content_type', $filter_type)
                                ->where('age_certification', $rated)
                                ->where('cinema_release_date', '<=', $maxy)
                                ->where('cinema_release_date', '>=', $miny)
                                ->orderBy('click_count', 'DESC')
                                ->orderBy($field, $sort_type)
                                ->paginate(28);
                        } else {
                            $contents = DB::table('contents')
                                ->whereIn('content_type', $filter_type)
                                ->where('age_certification', $rated)
                                ->where('tmdb_score', '<=', $maxr)
                                ->where('tmdb_score', '>=', $minr)
                                ->where('cinema_release_date', '<=', $maxy)
                                ->where('cinema_release_date', '>=', $miny)
                                ->orderBy('click_count', 'DESC')
                                ->orderBy($field, $sort_type)
                                ->paginate(28);
                        }
                    } else {
                        if ($minr == 0 && $maxr == 10) {
                            $contents = DB::table('contents')
                                ->whereIn('content_type', $filter_type)
                                ->where('cinema_release_date', '<=', $maxy)
                                ->where('cinema_release_date', '>=', $miny)
                                ->orderBy('click_count', 'DESC')
                                ->orderBy($field, $sort_type)
                                ->paginate(28);
                        } else {
                            $contents = DB::table('contents')
                                ->whereIn('content_type', $filter_type)
                                ->where('tmdb_score', '<=', $maxr)
                                ->where('tmdb_score', '>=', $minr)
                                ->where('cinema_release_date', '<=', $maxy)
                                ->where('cinema_release_date', '>=', $miny)
                                ->orderBy('click_count', 'DESC')
                                ->orderBy($field, $sort_type)
                                ->paginate(28);
                        }
                    }
                }
            } else {

                if ($sort == "") {
                    if ($langs != "" && $rated != "" && $genre != "") {
                        if ($minr == 0 && $maxr == 10) {
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->leftJoin('content_genres', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_providers.provider_id', $provider)
                                ->where('content_genres.genre_id', $genre)
                                ->where('contents.age_certification', $rated)
                                ->whereIn('contents.content_type', $filter_type)
                                ->where('contents.original_language', $langs)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->orderBy('contents.click_count', 'DESC')
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate(28);
                        } else {
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->leftJoin('content_genres', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_providers.provider_id', $provider)
                                ->where('content_genres.genre_id', $genre)
                                ->whereIn('contents.content_type', $filter_type)
                                ->where('contents.age_certification', $rated)
                                ->where('contents.original_language', $langs)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->orderBy('contents.click_count', 'DESC')
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate(28);
                        }
                    } elseif ($langs != "" && $rated != "") {
                        if ($minr == 0 && $maxr == 10) {
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->where('content_providers.provider_id', $provider)
                                ->whereIn('contents.content_type', $filter_type)
                                ->where('contents.original_language', $langs)
                                ->where('contents.age_certification', $rated)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->orderBy('contents.click_count', 'DESC')
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate(28);
                        } else {
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->where('content_providers.provider_id', $provider)
                                ->whereIn('contents.content_type', $filter_type)
                                ->where('contents.original_language', $langs)
                                ->where('contents.age_certification', $rated)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->orderBy('contents.click_count', 'DESC')
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate(28);
                        }
                    } elseif ($rated != "" && $genre != "") {
                        if ($minr == 0 && $maxr == 10) {
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->leftJoin('content_genres', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_providers.provider_id', $provider)
                                ->where('content_genres.genre_id', $genre)
                                ->where('contents.age_certification', $rated)
                                ->whereIn('contents.content_type', $filter_type)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->orderBy('contents.click_count', 'DESC')
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate(28);
                        } else {
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->leftJoin('content_genres', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_providers.provider_id', $provider)
                                ->where('content_genres.genre_id', $genre)
                                ->where('contents.age_certification', $rated)
                                ->whereIn('contents.content_type', $filter_type)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->orderBy('contents.click_count', 'DESC')
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate(28);
                        }
                    } elseif ($langs != "" && $genre != "") {
                        if ($minr == 0 && $maxr == 10) {
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->leftJoin('content_genres', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_providers.provider_id', $provider)
                                ->where('content_genres.genre_id', $genre)
                                ->whereIn('contents.content_type', $filter_type)
                                ->where('contents.original_language', $langs)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->orderBy('contents.click_count', 'DESC')
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate(28);
                        } else {
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->leftJoin('content_genres', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_providers.provider_id', $provider)
                                ->where('content_genres.genre_id', $genre)
                                ->whereIn('contents.content_type', $filter_type)
                                ->where('contents.original_language', $langs)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->orderBy('contents.click_count', 'DESC')
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate(28);
                        }
                    } elseif ($langs != "") {
                        if ($minr == 0 && $maxr == 10) {
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->where('content_providers.provider_id', $provider)
                                ->whereIn('contents.content_type', $filter_type)
                                ->where('contents.original_language', $langs)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->orderBy('contents.click_count', 'DESC')
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate(28);
                        } else {
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->where('content_providers.provider_id', $provider)
                                ->whereIn('contents.content_type', $filter_type)
                                ->where('contents.original_language', $langs)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->orderBy('contents.click_count', 'DESC')
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate(28);
                        }
                    } elseif ($genre != "") {
                        if ($minr == 0 && $maxr == 10) {
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->leftJoin('content_genres', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_providers.provider_id', $provider)
                                ->where('content_genres.genre_id', $genre)
                                ->whereIn('contents.content_type', $filter_type)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->orderBy('contents.click_count', 'DESC')
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate(28);
                        } else {
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->leftJoin('content_genres', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_providers.provider_id', $provider)
                                ->where('content_genres.genre_id', $genre)
                                ->whereIn('contents.content_type', $filter_type)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->orderBy('contents.click_count', 'DESC')
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate(28);
                        }
                    } elseif ($rated != "") {


                        if ($minr == 0 && $maxr == 10) {
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->where('content_providers.provider_id', $provider)
                                ->whereIn('contents.content_type', $filter_type)
                                ->where('contents.age_certification', $rated)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->orderBy('contents.click_count', 'DESC')
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate(28);
                        } else {
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->where('content_providers.provider_id', $provider)
                                ->whereIn('contents.content_type', $filter_type)
                                ->where('contents.age_certification', $rated)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->orderBy('contents.click_count', 'DESC')
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate(28);
                        }
                    } else {
                        if ($minr == 0 && $maxr == 10) {
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->where('content_providers.provider_id', $provider)
                                ->whereIn('contents.content_type', $filter_type)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->orderBy('contents.click_count', 'DESC')
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate(28);
                        } else {
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->where('content_providers.provider_id', $provider)
                                ->whereIn('contents.content_type', $filter_type)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->orderBy('contents.click_count', 'DESC')
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate(28);
                        }
                    }
                } else {

                    if ($langs != "" && $rated != "" && $genre != "") {
                        if ($minr == 0 && $maxr == 10) {
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->leftJoin('content_genres', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_providers.provider_id', $provider)
                                ->where('content_genres.genre_id', $genre)
                                ->where('contents.age_certification', $rated)
                                ->whereIn('contents.content_type', $filter_type)
                                ->where('contents.original_language', $langs)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->orderBy('contents.click_count', 'DESC')
                                ->orderBy($field, $sort_type)
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate(28);
                        } else {
                            $contents = DB::table('content_genres')
                                ->leftJoin('contents', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_genres.genre_id', $genre)
                                ->whereIn('contents.content_type', $filter_type)
                                ->where('contents.age_certification', $rated)
                                ->where('contents.original_language', $langs)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->orderBy('contents.click_count', 'DESC')
                                ->orderBy($field, $sort_type)
                                ->paginate(28);
                        }
                    } elseif ($langs != "" && $rated != "") {
                        if ($minr == 0 && $maxr == 10) {
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->where('content_providers.provider_id', $provider)
                                ->whereIn('contents.content_type', $filter_type)
                                ->where('contents.original_language', $langs)
                                ->where('contents.age_certification', $rated)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->orderBy('contents.click_count', 'DESC')
                                ->orderBy($field, $sort_type)
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate(28);
                        } else {
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->where('content_providers.provider_id', $provider)
                                ->whereIn('contents.content_type', $filter_type)
                                ->where('contents.original_language', $langs)
                                ->where('contents.age_certification', $rated)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->orderBy('contents.click_count', 'DESC')
                                ->orderBy($field, $sort_type)
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate(28);
                        }
                    } elseif ($rated != "" && $genre != "") {
                        if ($minr == 0 && $maxr == 10) {
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->leftJoin('content_genres', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_providers.provider_id', $provider)
                                ->where('content_genres.genre_id', $genre)
                                ->where('contents.age_certification', $rated)
                                ->whereIn('contents.content_type', $filter_type)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->orderBy('contents.click_count', 'DESC')
                                ->orderBy($field, $sort_type)
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate(28);
                        } else {
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->leftJoin('content_genres', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_providers.provider_id', $provider)
                                ->where('content_genres.genre_id', $genre)
                                ->where('contents.age_certification', $rated)
                                ->whereIn('contents.content_type', $filter_type)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->orderBy('contents.click_count', 'DESC')
                                ->orderBy($field, $sort_type)
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate(28);
                        }
                    } elseif ($langs != "" && $genre != "") {
                        if ($minr == 0 && $maxr == 10) {
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->leftJoin('content_genres', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_providers.provider_id', $provider)
                                ->where('content_genres.genre_id', $genre)
                                ->whereIn('contents.content_type', $filter_type)
                                ->where('contents.original_language', $langs)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->orderBy('contents.click_count', 'DESC')
                                ->orderBy($field, $sort_type)
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate(28);
                        } else {
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->leftJoin('content_genres', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_providers.provider_id', $provider)
                                ->where('content_genres.genre_id', $genre)
                                ->whereIn('contents.content_type', $filter_type)
                                ->where('contents.original_language', $langs)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->orderBy('contents.click_count', 'DESC')
                                ->orderBy($field, $sort_type)
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate(28);
                        }
                    } elseif ($langs != "") {
                        if ($minr == 0 && $maxr == 10) {
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->where('content_providers.provider_id', $provider)
                                ->whereIn('contents.content_type', $filter_type)
                                ->where('contents.original_language', $langs)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->orderBy('contents.click_count', 'DESC')
                                ->orderBy($field, $sort_type)
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate(28);
                        } else {
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->where('content_providers.provider_id', $provider)
                                ->whereIn('contents.content_type', $filter_type)
                                ->where('contents.original_language', $langs)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->orderBy('contents.click_count', 'DESC')
                                ->orderBy($field, $sort_type)
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate(28);
                        }
                    } elseif ($genre != "") {
                        if ($minr == 0 && $maxr == 10) {
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->leftJoin('content_genres', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_providers.provider_id', $provider)
                                ->where('content_genres.genre_id', $genre)
                                ->whereIn('contents.content_type', $filter_type)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->orderBy('contents.click_count', 'DESC')
                                ->orderBy($field, $sort_type)
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate(28);
                        } else {
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->leftJoin('content_genres', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_providers.provider_id', $provider)
                                ->where('content_genres.genre_id', $genre)
                                ->whereIn('contents.content_type', $filter_type)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->orderBy('contents.click_count', 'DESC')
                                ->orderBy($field, $sort_type)
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate(28);
                        }
                    } elseif ($rated != "") {
                        if ($minr == 0 && $maxr == 10) {
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->where('content_providers.provider_id', $provider)
                                ->whereIn('contents.content_type', $filter_type)
                                ->where('contents.age_certification', $rated)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->orderBy('contents.click_count', 'DESC')
                                ->orderBy($field, $sort_type)
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate(28);
                        } else {
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->where('content_providers.provider_id', $provider)
                                ->whereIn('contents.content_type', $filter_type)
                                ->where('contents.original_language', $langs)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->orderBy('contents.click_count', 'DESC')
                                ->orderBy($field, $sort_type)
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate(28);
                        }
                    } else {
                        if ($minr == 0 && $maxr == 10) {
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->where('content_providers.provider_id', $provider)
                                ->whereIn('contents.content_type', $filter_type)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->orderBy('contents.click_count', 'DESC')
                                ->orderBy($field, $sort_type)
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate(28);
                        } else {
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->where('content_providers.provider_id', $provider)
                                ->whereIn('contents.content_type', $filter_type)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->orderBy('contents.click_count', 'DESC')
                                ->orderBy($field, $sort_type)
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate(28);
                        }
                    }
                }
            }
        }
        $miny = explode('-', $miny)[0];
        $maxy = explode('-', $maxy)[0];

        if (isset($_GET['provider'])) {
            $provider = $_GET['provider'];
        }

        $contents = $contents->appends(\Input::except('page'));
        foreach ($contents as $key => $value) {
            $this->resizeImage($value->poster);
        }
        return view('khoj.home', compact('contents', 'page', 'type', 'miny', 'maxy', 'langs', 'genre', 'rated', 'minr', 'maxr', 'sort', 'tab', 'provider'));
    }



    //New Movies
    public function getNewMovies(Request $request)
    {
        $date = date('Y-m-d');
        $page = 0;
        $langs = "";
        $rated = "";
        $sort = "";
        $field = "";
        $sort_type = "";
        $provider = "";
        $genre = "";
        $minr = 0;
        $maxr = 10;
        $type = "all";
        $tab = 'New';

        // $miny = DB::table('contents')->where('original_release_year','>','1900')->min('original_release_year');
        $miny = date("Y-m-d", strtotime("-6 months"));;
        $maxy = $date;

        if (isset($_GET['page'])) {
            $page = $_GET['page'] - 1;
        }

        if (isset($_GET['lang'])) {
            $langs = $_GET['lang'];
        }
        if (isset($_GET['provider'])) {
            $provider = explode('_', $_GET['provider'])[1];
        }

        if (isset($_GET['sort'])) {
            $sort = $_GET['sort'];
            if ($sort == "alpha") {
                $field = 'title';
                $sort_type = 'ASC';
            } elseif ($sort == 'rate') {
                $field = 'tmdb_score';
                $sort_type = 'DESC';
            } elseif ($sort == 'year') {
                $field = 'cinema_release_date';
                $sort_type = 'DESC';
            } elseif ($sort == 'imdb_score') {
                $field = 'imdb_score';
                $sort_type = 'DESC';
            } else {
                $field = 'content_id';
                $sort_type = 'DESC';
            }
        }

        if ((isset($_GET['sort']) && isset($_GET['genre'])) || (isset($_GET['sort']) && isset($_GET['provider']))) {
            $sort = $_GET['sort'];
            if ($sort == "alpha") {
                $field = 'contents.title';
                $sort_type = 'ASC';
            } elseif ($sort == 'rate') {
                $field = 'contents.tmdb_score';
                $sort_type = 'DESC';
            } elseif ($sort == 'year') {
                $field = 'contents.cinema_release_date';
                $sort_type = 'DESC';
            } elseif ($sort == 'imdb_score') {
                $field = 'contents.imdb_score';
                $sort_type = 'DESC';
            } else {
                $field = 'contents.content_id';
                $sort_type = 'DESC';
            }
        }

        if (isset($_GET['rated'])) {
            $rated = $_GET['rated'];
        }

        if (isset($_GET['searchtype'])) {
            $type = $_GET['searchtype'];
        }

        if (isset($_GET['genre'])) {
            $genre = $_GET['genre'];
        }

        if (isset($_GET['minr'])) {
            $minr = $_GET['minr'];
        }
        if (isset($_GET['maxr'])) {
            $maxr = $_GET['maxr'];
        }

        if (isset($_GET['maxy'])) {
            $maxy = $_GET['maxy'] . "-12-31";
        }

        if ($type == 'all') {
            if ($provider == "") {
                if ($sort == "") {

                    if ($langs != "" && $rated != "" && $genre != "") {
                        if ($minr == 0 && $maxr == 10) {
                            $contents = DB::table('content_genres')
                                ->leftJoin('contents', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_genres.genre_id', $genre)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->where('contents.age_certification', $rated)
                                ->where('contents.original_language', $langs)
                                ->where('contents.parent_id', '0')
                                ->orderBy('contents.cinema_release_date', 'DESC')
                                ->orderBy('contents.localized_release_date', 'DESC')
                                ->paginate(28);
                        } else {
                            $contents = DB::table('content_genres')
                                ->leftJoin('contents', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_genres.genre_id', $genre)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->where('contents.age_certification', $rated)
                                ->where('contents.original_language', $langs)
                                ->where('contents.parent_id', '0')
                                ->orderBy('contents.cinema_release_date', 'DESC')
                                ->orderBy('contents.localized_release_date', 'DESC')
                                ->paginate(28);
                        }
                    } elseif ($langs != "" && $rated != "") {
                        if ($minr == 0 && $maxr == 10) {
                            $contents = DB::table('contents')
                                ->where('original_language', $langs)
                                ->where('age_certification', $rated)
                                ->where('cinema_release_date', '<=', $maxy)
                                ->where('cinema_release_date', '>=', $miny)
                                ->where('parent_id', '0')
                                ->orderBy('cinema_release_date', 'DESC')
                                ->orderBy('localized_release_date', 'DESC')
                                ->paginate(28);
                        } else {
                            $contents = DB::table('contents')
                                ->where('original_language', $langs)
                                ->where('age_certification', $rated)
                                ->where('tmdb_score', '<=', $maxr)
                                ->where('tmdb_score', '>=', $minr)
                                ->where('cinema_release_date', '<=', $maxy)
                                ->where('cinema_release_date', '>=', $miny)
                                ->where('parent_id', '0')
                                ->orderBy('cinema_release_date', 'DESC')
                                ->orderBy('localized_release_date', 'DESC')
                                ->paginate(28);
                        }
                    } elseif ($rated != "" && $genre != "") {
                        if ($minr == 0 && $maxr == 10) {
                            $contents = DB::table('content_genres')
                                ->leftJoin('contents', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_genres.genre_id', $genre)
                                ->where('contents.age_certification', $rated)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->where('contents.parent_id', '0')
                                ->orderBy('contents.cinema_release_date', 'DESC')
                                ->orderBy('contents.localized_release_date', 'DESC')
                                ->paginate(28);
                        } else {
                            $contents = DB::table('content_genres')
                                ->leftJoin('contents', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_genres.genre_id', $genre)
                                ->where('contents.age_certification', $rated)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->where('contents.parent_id', '0')
                                ->orderBy('contents.cinema_release_date', 'DESC')
                                ->orderBy('contents.localized_release_date', 'DESC')
                                ->paginate(28);
                        }
                    } elseif ($langs != "" && $genre != "") {
                        if ($minr == 0 && $maxr == 10) {
                            $contents = DB::table('content_genres')
                                ->leftJoin('contents', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_genres.genre_id', $genre)
                                ->where('contents.original_language', $langs)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->where('contents.parent_id', '0')
                                ->orderBy('contents.cinema_release_date', 'DESC')
                                ->orderBy('contents.localized_release_date', 'DESC')
                                ->paginate(28);
                        } else {
                            $contents = DB::table('content_genres')
                                ->leftJoin('contents', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_genres.genre_id', $genre)
                                ->where('contents.original_language', $langs)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->where('contents.parent_id', '0')
                                ->orderBy('contents.cinema_release_date', 'DESC')
                                ->orderBy('contents.localized_release_date', 'DESC')
                                ->paginate(28);
                        }
                    } elseif ($langs != "") {
                        if ($minr == 0 && $maxr == 10) {
                            $contents = DB::table('contents')
                                ->where('original_language', $langs)
                                ->where('cinema_release_date', '<=', $maxy)
                                ->where('cinema_release_date', '>=', $miny)
                                ->where('parent_id', '0')
                                ->orderBy('cinema_release_date', 'DESC')
                                ->orderBy('localized_release_date', 'DESC')
                                ->paginate(28);
                        } else {
                            $contents = DB::table('contents')
                                ->where('original_language', $langs)
                                ->where('tmdb_score', '<=', $maxr)
                                ->where('tmdb_score', '>=', $minr)
                                ->where('cinema_release_date', '<=', $maxy)
                                ->where('cinema_release_date', '>=', $miny)
                                ->where('parent_id', '0')
                                ->orderBy('cinema_release_date', 'DESC')
                                ->orderBy('localized_release_date', 'DESC')
                                ->paginate(28);
                        }
                    } elseif ($genre != "") {
                        if ($minr == 0 && $maxr == 10) {
                            $contents = DB::table('content_genres')
                                ->leftJoin('contents', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_genres.genre_id', $genre)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->where('contents.parent_id', '0')
                                ->orderBy('content_genres.content_id', 'DESC')
                                ->orderBy('contents.cinema_release_date', 'DESC')
                                ->orderBy('contents.localized_release_date', 'DESC')
                                ->paginate(28);
                        } else {
                            $contents = DB::table('content_genres')
                                ->leftJoin('contents', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_genres.genre_id', $genre)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->where('contents.parent_id', '0')
                                ->orderBy('contents.cinema_release_date', 'DESC')
                                ->orderBy('contents.localized_release_date', 'DESC')
                                ->paginate(28);
                        }
                    } elseif ($rated != "") {
                        if ($minr == 0 && $maxr == 10) {
                            $contents = DB::table('contents')
                                ->where('age_certification', $rated)
                                ->where('cinema_release_date', '<=', $maxy)
                                ->where('cinema_release_date', '>=', $miny)
                                ->where('parent_id', '0')
                                ->orderBy('cinema_release_date', 'DESC')
                                ->orderBy('localized_release_date', 'DESC')
                                ->paginate(28);
                        } else {
                            $contents = DB::table('contents')
                                ->where('age_certification', $rated)
                                ->where('tmdb_score', '<=', $maxr)
                                ->where('tmdb_score', '>=', $minr)
                                ->where('cinema_release_date', '<=', $maxy)
                                ->where('cinema_release_date', '>=', $miny)
                                ->where('parent_id', '0')
                                ->orderBy('cinema_release_date', 'DESC')
                                ->orderBy('localized_release_date', 'DESC')
                                ->paginate(28);
                        }
                    } else {
                        if ($minr == 0 && $maxr == 10) {
                            $contents = DB::table('contents')
                                ->where('cinema_release_date', '<=', $maxy)
                                ->where('cinema_release_date', '>=', $miny)
                                ->where('parent_id', '0')
                                ->orderBy('content_id', 'DESC')
                                ->orderBy('cinema_release_date', 'DESC')
                                ->orderBy('localized_release_date', 'DESC')
                                ->paginate(28);
                        } else {
                            $contents = DB::table('contents')
                                ->where('tmdb_score', '<=', $maxr)
                                ->where('tmdb_score', '>=', $minr)
                                ->where('cinema_release_date', '<=', $maxy)
                                ->where('cinema_release_date', '>=', $miny)
                                ->where('parent_id', '0')
                                ->orderBy('content_id', 'DESC')
                                ->orderBy('cinema_release_date', 'DESC')
                                ->orderBy('localized_release_date', 'DESC')
                                ->paginate(28);
                        }
                    }
                } else {

                    if ($langs != "" && $rated != "" && $genre != "") {
                        if ($minr == 0 && $maxr == 10) {
                            $contents = DB::table('content_genres')
                                ->leftJoin('contents', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_genres.genre_id', $genre)
                                ->where('contents.age_certification', $rated)
                                ->where('contents.original_language', $langs)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->where('contents.parent_id', '0')
                                ->orderBy('contents.cinema_release_date', 'DESC')
                                ->orderBy('contents.localized_release_date', 'DESC')
                                ->orderBy($field, $sort_type)
                                ->paginate(28);
                        } else {
                            $contents = DB::table('content_genres')
                                ->leftJoin('contents', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_genres.genre_id', $genre)
                                ->where('contents.age_certification', $rated)
                                ->where('contents.original_language', $langs)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->where('contents.parent_id', '0')
                                ->orderBy('contents.cinema_release_date', 'DESC')
                                ->orderBy('contents.localized_release_date', 'DESC')
                                ->orderBy($field, $sort_type)
                                ->paginate(28);
                        }
                    } elseif ($langs != "" && $rated != "") {
                        if ($minr == 0 && $maxr == 10) {
                            $contents = DB::table('contents')
                                ->where('original_language', $langs)
                                ->where('age_certification', $rated)
                                ->where('cinema_release_date', '<=', $maxy)
                                ->where('cinema_release_date', '>=', $miny)
                                ->where('parent_id', '0')
                                ->orderBy('cinema_release_date', 'DESC')
                                ->orderBy('localized_release_date', 'DESC')
                                ->orderBy($field, $sort_type)
                                ->paginate(28);
                        } else {
                            $contents = DB::table('contents')
                                ->where('original_language', $langs)
                                ->where('age_certification', $rated)
                                ->where('tmdb_score', '<=', $maxr)
                                ->where('tmdb_score', '>=', $minr)
                                ->where('cinema_release_date', '<=', $maxy)
                                ->where('cinema_release_date', '>=', $miny)
                                ->where('parent_id', '0')
                                ->orderBy('cinema_release_date', 'DESC')
                                ->orderBy('localized_release_date', 'DESC')
                                ->orderBy($field, $sort_type)
                                ->paginate(28);
                        }
                    } elseif ($rated != "" && $genre != "") {
                        if ($minr == 0 && $maxr == 10) {
                            $contents = DB::table('content_genres')
                                ->leftJoin('contents', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_genres.genre_id', $genre)
                                ->where('contents.age_certification', $rated)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->where('contents.parent_id', '0')
                                ->orderBy('contents.cinema_release_date', 'DESC')
                                ->orderBy('contents.localized_release_date', 'DESC')
                                ->orderBy($field, $sort_type)
                                ->paginate(28);
                        } else {
                            $contents = DB::table('content_genres')
                                ->leftJoin('contents', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_genres.genre_id', $genre)
                                ->where('contents.age_certification', $rated)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->where('contents.parent_id', '0')
                                ->orderBy('contents.cinema_release_date', 'DESC')
                                ->orderBy('contents.localized_release_date', 'DESC')
                                ->orderBy($field, $sort_type)
                                ->paginate(28);
                        }
                    } elseif ($langs != "" && $genre != "") {
                        if ($minr == 0 && $maxr == 10) {
                            $contents = DB::table('content_genres')
                                ->leftJoin('contents', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_genres.genre_id', $genre)
                                ->where('contents.original_language', $langs)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->where('contents.parent_id', '0')
                                ->orderBy('contents.cinema_release_date', 'DESC')
                                ->orderBy('contents.localized_release_date', 'DESC')
                                ->orderBy($field, $sort_type)
                                ->paginate(28);
                        } else {
                            $contents = DB::table('content_genres')
                                ->leftJoin('contents', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_genres.genre_id', $genre)
                                ->where('contents.original_language', $langs)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->where('contents.parent_id', '0')
                                ->orderBy('contents.cinema_release_date', 'DESC')
                                ->orderBy('contents.localized_release_date', 'DESC')
                                ->orderBy($field, $sort_type)
                                ->paginate(28);
                        }
                    } elseif ($langs != "") {
                        if ($minr == 0 && $maxr == 10) {
                            $contents = DB::table('contents')
                                ->where('original_language', $langs)
                                ->where('cinema_release_date', '<=', $maxy)
                                ->where('cinema_release_date', '>=', $miny)
                                ->where('parent_id', '0')
                                ->orderBy('cinema_release_date', 'DESC')
                                ->orderBy('localized_release_date', 'DESC')
                                ->orderBy($field, $sort_type)
                                ->paginate(28);
                        } else {
                            $contents = DB::table('contents')
                                ->where('original_language', $langs)
                                ->where('tmdb_score', '<=', $maxr)
                                ->where('tmdb_score', '>=', $minr)
                                ->where('cinema_release_date', '<=', $maxy)
                                ->where('cinema_release_date', '>=', $miny)
                                ->where('parent_id', '0')
                                ->orderBy('cinema_release_date', 'DESC')
                                ->orderBy('localized_release_date', 'DESC')
                                ->orderBy($field, $sort_type)
                                ->paginate(28);
                        }
                    } elseif ($genre != "") {
                        if ($minr == 0 && $maxr == 10) {
                            $contents = DB::table('content_genres')
                                ->leftJoin('contents', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_genres.genre_id', $genre)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->where('contents.parent_id', '0')
                                ->orderBy('contents.cinema_release_date', 'DESC')
                                ->orderBy('contents.localized_release_date', 'DESC')
                                ->orderBy($field, $sort_type)
                                ->paginate(28);
                        } else {
                            $contents = DB::table('content_genres')
                                ->leftJoin('contents', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_genres.genre_id', $genre)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->where('contents.parent_id', '0')
                                ->orderBy('contents.cinema_release_date', 'DESC')
                                ->orderBy('contents.localized_release_date', 'DESC')
                                ->orderBy($field, $sort_type)
                                ->paginate(28);
                        }
                    } elseif ($rated != "") {
                        if ($minr == 0 && $maxr == 10) {
                            $contents = DB::table('contents')
                                ->where('age_certification', $rated)
                                ->where('cinema_release_date', '<=', $maxy)
                                ->where('cinema_release_date', '>=', $miny)
                                ->where('parent_id', '0')
                                ->orderBy('cinema_release_date', 'DESC')
                                ->orderBy('localized_release_date', 'DESC')
                                ->orderBy($field, $sort_type)
                                ->paginate(28);
                        } else {
                            $contents = DB::table('contents')
                                ->where('age_certification', $rated)
                                ->where('tmdb_score', '<=', $maxr)
                                ->where('tmdb_score', '>=', $minr)
                                ->where('cinema_release_date', '<=', $maxy)
                                ->where('cinema_release_date', '>=', $miny)
                                ->where('parent_id', '0')
                                ->orderBy('cinema_release_date', 'DESC')
                                ->orderBy('localized_release_date', 'DESC')
                                ->orderBy($field, $sort_type)
                                ->paginate(28);
                        }
                    } else {
                        if ($minr == 0 && $maxr == 10) {
                            $contents = DB::table('contents')
                                ->where('cinema_release_date', '<=', $maxy)
                                ->where('cinema_release_date', '>=', $miny)
                                ->where('parent_id', '0')
                                ->orderBy('cinema_release_date', 'DESC')
                                ->orderBy('localized_release_date', 'DESC')
                                ->orderBy($field, $sort_type)
                                ->paginate(28);
                        } else {
                            $contents = DB::table('contents')
                                ->where('tmdb_score', '<=', $maxr)
                                ->where('tmdb_score', '>=', $minr)
                                ->where('cinema_release_date', '<=', $maxy)
                                ->where('cinema_release_date', '>=', $miny)
                                ->where('parent_id', '0')
                                ->orderBy('cinema_release_date', 'DESC')
                                ->orderBy('localized_release_date', 'DESC')
                                ->orderBy($field, $sort_type)
                                ->paginate(28);
                        }
                    }
                }
            }

            //When Provider Not NULL
            else {
                if ($sort == "") {
                    if ($langs != "" && $rated != "" && $genre != "") {
                        if ($minr == 0 && $maxr == 10) {
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->leftJoin('content_genres', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_providers.provider_id', $provider)
                                ->where('content_genres.genre_id', $genre)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->where('contents.age_certification', $rated)
                                ->where('contents.original_language', $langs)
                                ->where('contents.parent_id', '0')
                                ->orderBy('contents.cinema_release_date', 'DESC')
                                ->orderBy('contents.localized_release_date', 'DESC')
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate(28);
                        } else {
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->leftJoin('content_genres', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_providers.provider_id', $provider)
                                ->where('content_genres.genre_id', $genre)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->where('contents.age_certification', $rated)
                                ->where('contents.original_language', $langs)
                                ->where('contents.parent_id', '0')
                                ->orderBy('contents.cinema_release_date', 'DESC')
                                ->orderBy('contents.localized_release_date', 'DESC')
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate(28);
                        }
                    } elseif ($langs != "" && $rated != "") {
                        if ($minr == 0 && $maxr == 10) {
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->where('content_providers.provider_id', $provider)
                                ->where('contents.original_language', $langs)
                                ->where('contents.age_certification', $rated)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->where('contents.parent_id', '0')
                                ->orderBy('contents.cinema_release_date', 'DESC')
                                ->orderBy('contents.localized_release_date', 'DESC')
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate(28);
                        } else {
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->where('content_providers.provider_id', $provider)
                                ->where('contents.original_language', $langs)
                                ->where('contents.age_certification', $rated)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->where('contents.parent_id', '0')
                                ->orderBy('contents.cinema_release_date', 'DESC')
                                ->orderBy('contents.localized_release_date', 'DESC')
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate(28);
                        }
                    } elseif ($rated != "" && $genre != "") {
                        if ($minr == 0 && $maxr == 10) {
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->leftJoin('content_genres', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_providers.provider_id', $provider)
                                ->where('content_genres.genre_id', $genre)
                                ->where('contents.age_certification', $rated)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->where('contents.parent_id', '0')
                                ->orderBy('contents.cinema_release_date', 'DESC')
                                ->orderBy('contents.localized_release_date', 'DESC')
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate(28);
                        } else {
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->leftJoin('content_genres', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_providers.provider_id', $provider)
                                ->where('content_genres.genre_id', $genre)
                                ->where('contents.age_certification', $rated)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->where('contents.parent_id', '0')
                                ->orderBy('contents.cinema_release_date', 'DESC')
                                ->orderBy('contents.localized_release_date', 'DESC')
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate(28);
                        }
                    } elseif ($langs != "" && $genre != "") {
                        if ($minr == 0 && $maxr == 10) {
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->leftJoin('content_genres', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_providers.provider_id', $provider)
                                ->where('content_genres.genre_id', $genre)
                                ->where('contents.original_language', $langs)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->where('contents.parent_id', '0')
                                ->orderBy('contents.cinema_release_date', 'DESC')
                                ->orderBy('contents.localized_release_date', 'DESC')
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate(28);
                        } else {
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->leftJoin('content_genres', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_providers.provider_id', $provider)
                                ->where('content_genres.genre_id', $genre)
                                ->where('contents.original_language', $langs)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->where('contents.parent_id', '0')
                                ->orderBy('contents.cinema_release_date', 'DESC')
                                ->orderBy('contents.localized_release_date', 'DESC')
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate(28);
                        }
                    } elseif ($langs != "") {
                        if ($minr == 0 && $maxr == 10) {
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->where('content_providers.provider_id', $provider)
                                ->where('contents.original_language', $langs)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->where('contents.parent_id', '0')
                                ->orderBy('contents.cinema_release_date', 'DESC')
                                ->orderBy('contents.localized_release_date', 'DESC')
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate(28);
                        } else {
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->where('content_providers.provider_id', $provider)
                                ->where('contents.original_language', $langs)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->where('contents.parent_id', '0')
                                ->orderBy('contents.cinema_release_date', 'DESC')
                                ->orderBy('contents.localized_release_date', 'DESC')
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate(28);
                        }
                    } elseif ($genre != "") {
                        if ($minr == 0 && $maxr == 10) {
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->leftJoin('content_genres', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_providers.provider_id', $provider)
                                ->where('content_genres.genre_id', $genre)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->where('contents.parent_id', '0')
                                ->orderBy('contents.cinema_release_date', 'DESC')
                                ->orderBy('contents.localized_release_date', 'DESC')
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate(28);
                        } else {
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->leftJoin('content_genres', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_providers.provider_id', $provider)
                                ->where('content_genres.genre_id', $genre)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->where('contents.parent_id', '0')
                                ->orderBy('contents.cinema_release_date', 'DESC')
                                ->orderBy('contents.localized_release_date', 'DESC')
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate(28);
                        }
                    } elseif ($rated != "") {
                        if ($minr == 0 && $maxr == 10) {
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->where('content_providers.provider_id', $provider)
                                ->where('contents.age_certification', $rated)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->where('contents.parent_id', '0')
                                ->orderBy('contents.cinema_release_date', 'DESC')
                                ->orderBy('contents.localized_release_date', 'DESC')
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate(28);
                        } else {
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->where('content_providers.provider_id', $provider)
                                ->where('contents.age_certification', $rated)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->where('contents.parent_id', '0')
                                ->orderBy('contents.cinema_release_date', 'DESC')
                                ->orderBy('contents.localized_release_date', 'DESC')
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate(28);
                        }
                    } else {
                        if ($minr == 0 && $maxr == 10) {
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->where('content_providers.provider_id', $provider)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->where('contents.parent_id', '0')
                                ->orderBy('contents.cinema_release_date', 'DESC')
                                ->orderBy('contents.localized_release_date', 'DESC')
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate(28);
                        } else {
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->where('content_providers.provider_id', $provider)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->where('contents.parent_id', '0')
                                ->orderBy('contents.cinema_release_date', 'DESC')
                                ->orderBy('contents.localized_release_date', 'DESC')
                                ->orderBy($field, $sort_type)
                                ->orderBy($field, $sort_type)
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate(28);
                        }
                    }
                } else {

                    if ($langs != "" && $rated != "" && $genre != "") {

                        if ($minr == 0 && $maxr == 10) {
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->leftJoin('content_genres', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_providers.provider_id', $provider)
                                ->where('content_genres.genre_id', $genre)
                                ->where('contents.age_certification', $rated)
                                ->where('contents.original_language', $langs)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->where('contents.parent_id', '0')
                                ->orderBy('contents.cinema_release_date', 'DESC')
                                ->orderBy('contents.localized_release_date', 'DESC')
                                ->orderBy($field, $sort_type)
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate(28);
                        } else {
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->leftJoin('content_genres', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_providers.provider_id', $provider)
                                ->where('content_genres.genre_id', $genre)
                                ->where('contents.age_certification', $rated)
                                ->where('contents.original_language', $langs)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->where('contents.parent_id', '0')
                                ->orderBy('contents.cinema_release_date', 'DESC')
                                ->orderBy('contents.localized_release_date', 'DESC')
                                ->orderBy($field, $sort_type)
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate(28);
                        }
                    } elseif ($langs != "" && $rated != "") {
                        if ($minr == 0 && $maxr == 10) {
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->where('content_providers.provider_id', $provider)
                                ->where('contents.original_language', $langs)
                                ->where('contents.age_certification', $rated)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->where('contents.parent_id', '0')
                                ->orderBy('contents.cinema_release_date', 'DESC')
                                ->orderBy('contents.localized_release_date', 'DESC')
                                ->orderBy($field, $sort_type)
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate(28);
                        } else {
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->where('content_providers.provider_id', $provider)
                                ->where('contents.original_language', $langs)
                                ->where('contents.age_certification', $rated)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->where('contents.parent_id', '0')
                                ->orderBy('contents.cinema_release_date', 'DESC')
                                ->orderBy('contents.localized_release_date', 'DESC')
                                ->orderBy($field, $sort_type)
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate(28);
                        }
                    } elseif ($rated != "" && $genre != "") {
                        if ($minr == 0 && $maxr == 10) {
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->leftJoin('content_genres', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_providers.provider_id', $provider)
                                ->where('content_genres.genre_id', $genre)
                                ->where('contents.age_certification', $rated)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->where('contents.parent_id', '0')
                                ->orderBy('contents.cinema_release_date', 'DESC')
                                ->orderBy('contents.localized_release_date', 'DESC')
                                ->orderBy($field, $sort_type)
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate(28);
                        } else {
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->leftJoin('content_genres', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_providers.provider_id', $provider)
                                ->where('content_genres.genre_id', $genre)
                                ->where('contents.age_certification', $rated)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->where('contents.parent_id', '0')
                                ->orderBy('contents.cinema_release_date', 'DESC')
                                ->orderBy('contents.localized_release_date', 'DESC')
                                ->orderBy($field, $sort_type)
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate(28);
                        }
                    } elseif ($langs != "" && $genre != "") {
                        if ($minr == 0 && $maxr == 10) {
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->leftJoin('content_genres', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_providers.provider_id', $provider)
                                ->where('content_genres.genre_id', $genre)
                                ->where('contents.original_language', $langs)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->where('contents.parent_id', '0')
                                ->orderBy('contents.cinema_release_date', 'DESC')
                                ->orderBy('contents.localized_release_date', 'DESC')
                                ->orderBy($field, $sort_type)
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate(28);
                        } else {
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->leftJoin('content_genres', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_providers.provider_id', $provider)
                                ->where('content_genres.genre_id', $genre)
                                ->where('contents.original_language', $langs)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->where('contents.parent_id', '0')
                                ->orderBy('contents.cinema_release_date', 'DESC')
                                ->orderBy('contents.localized_release_date', 'DESC')
                                ->orderBy($field, $sort_type)
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate(28);
                        }
                    } elseif ($langs != "") {
                        if ($minr == 0 && $maxr == 10) {
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->where('content_providers.provider_id', $provider)
                                ->where('contents.original_language', $langs)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->where('contents.parent_id', '0')
                                ->orderBy('contents.cinema_release_date', 'DESC')
                                ->orderBy('contents.localized_release_date', 'DESC')
                                ->orderBy($field, $sort_type)
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate(28);
                        } else {
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->where('content_providers.provider_id', $provider)
                                ->where('contents.original_language', $langs)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->where('contents.parent_id', '0')
                                ->orderBy('contents.cinema_release_date', 'DESC')
                                ->orderBy('contents.localized_release_date', 'DESC')
                                ->orderBy($field, $sort_type)
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate(28);
                        }
                    } elseif ($genre != "") {
                        if ($minr == 0 && $maxr == 10) {
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->leftJoin('content_genres', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_providers.provider_id', $provider)
                                ->where('content_genres.genre_id', $genre)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->where('contents.parent_id', '0')
                                ->orderBy('contents.cinema_release_date', 'DESC')
                                ->orderBy('contents.localized_release_date', 'DESC')
                                ->orderBy($field, $sort_type)
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate(28);
                        } else {
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->leftJoin('content_genres', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_providers.provider_id', $provider)
                                ->where('content_genres.genre_id', $genre)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->where('contents.parent_id', '0')
                                ->orderBy('contents.cinema_release_date', 'DESC')
                                ->orderBy('contents.localized_release_date', 'DESC')
                                ->orderBy($field, $sort_type)
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate(28);
                        }
                    } elseif ($rated != "") {
                        if ($minr == 0 && $maxr == 10) {
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->where('content_providers.provider_id', $provider)
                                ->where('contents.age_certification', $rated)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->where('contents.parent_id', '0')
                                ->orderBy('contents.cinema_release_date', 'DESC')
                                ->orderBy('contents.localized_release_date', 'DESC')
                                ->orderBy($field, $sort_type)
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate(28);
                        } else {
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->where('content_providers.provider_id', $provider)
                                ->where('contents.age_certification', $rated)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->where('contents.parent_id', '0')
                                ->orderBy('contents.cinema_release_date', 'DESC')
                                ->orderBy('contents.localized_release_date', 'DESC')
                                ->orderBy($field, $sort_type)
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate(28);
                        }
                    } else {
                        if ($minr == 0 && $maxr == 10) {
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->where('content_providers.provider_id', $provider)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->where('contents.parent_id', '0')
                                ->orderBy('contents.cinema_release_date', 'DESC')
                                ->orderBy('contents.localized_release_date', 'DESC')
                                ->orderBy($field, $sort_type)
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate(28);
                        } else {
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->where('content_providers.provider_id', $provider)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->where('contents.parent_id', '0')
                                ->orderBy('contents.cinema_release_date', 'DESC')
                                ->orderBy('contents.localized_release_date', 'DESC')
                                ->orderBy($field, $sort_type)
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate(28);
                        }
                    }
                }
            }
        } else {

            $filter_type = array();
            if ($type == 'movie') {
                $filter_type = ['movie'];
            } else {
                $filter_type = ['shows', 'tv-show', 'show'];
            }
            if ($provider == "") {

                if ($sort == "") {
                    if ($langs != "" && $rated != "" && $genre != "") {
                        if ($minr == 0 && $maxr == 10) {
                            $contents = DB::table('content_genres')
                                ->leftJoin('contents', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_genres.genre_id', $genre)
                                ->whereIn('contents.content_type', $filter_type)
                                ->where('contents.age_certification', $rated)
                                ->where('contents.original_language', $langs)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->orderBy('contents.cinema_release_date', 'DESC')
                                ->orderBy('contents.localized_release_date', 'DESC')
                                ->paginate(28);
                        } else {
                            $contents = DB::table('content_genres')
                                ->leftJoin('contents', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_genres.genre_id', $genre)
                                ->whereIn('contents.content_type', $filter_type)
                                ->where('contents.age_certification', $rated)
                                ->where('contents.original_language', $langs)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->orderBy('contents.cinema_release_date', 'DESC')
                                ->orderBy('contents.localized_release_date', 'DESC')
                                ->paginate(28);
                        }
                    } elseif ($langs != "" && $rated != "") {
                        if ($minr == 0 && $maxr == 10) {
                            $contents = DB::table('contents')
                                ->whereIn('content_type', $filter_type)
                                ->where('original_language', $langs)
                                ->where('age_certification', $rated)
                                ->where('cinema_release_date', '<=', $maxy)
                                ->where('cinema_release_date', '>=', $miny)
                                ->orderBy('cinema_release_date', 'DESC')
                                ->orderBy('localized_release_date', 'DESC')
                                ->paginate(28);
                        } else {
                            $contents = DB::table('contents')
                                ->whereIn('content_type', $filter_type)
                                ->where('original_language', $langs)
                                ->where('age_certification', $rated)
                                ->where('tmdb_score', '<=', $maxr)
                                ->where('tmdb_score', '>=', $minr)
                                ->where('cinema_release_date', '<=', $maxy)
                                ->where('cinema_release_date', '>=', $miny)
                                ->orderBy('cinema_release_date', 'DESC')
                                ->orderBy('localized_release_date', 'DESC')
                                ->paginate(28);
                        }
                    } elseif ($rated != "" && $genre != "") {
                        if ($minr == 0 && $maxr == 10) {
                            $contents = DB::table('content_genres')
                                ->leftJoin('contents', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_genres.genre_id', $genre)
                                ->whereIn('contents.content_type', $filter_type)
                                ->where('contents.age_certification', $rated)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->orderBy('contents.cinema_release_date', 'DESC')
                                ->orderBy('contents.localized_release_date', 'DESC')
                                ->paginate(28);
                        } else {
                            $contents = DB::table('content_genres')
                                ->leftJoin('contents', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_genres.genre_id', $genre)
                                ->whereIn('contents.content_type', $filter_type)
                                ->where('contents.age_certification', $rated)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->orderBy('contents.cinema_release_date', 'DESC')
                                ->orderBy('contents.localized_release_date', 'DESC')
                                ->paginate(28);
                        }
                    } elseif ($langs != "" && $genre != "") {
                        if ($minr == 0 && $maxr == 10) {
                            $contents = DB::table('content_genres')
                                ->leftJoin('contents', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_genres.genre_id', $genre)
                                ->whereIn('contents.content_type', $filter_type)
                                ->where('contents.original_language', $langs)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->orderBy('contents.cinema_release_date', 'DESC')
                                ->orderBy('contents.localized_release_date', 'DESC')
                                ->paginate(28);
                        } else {
                            $contents = DB::table('content_genres')
                                ->leftJoin('contents', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_genres.genre_id', $genre)
                                ->whereIn('contents.content_type', $filter_type)
                                ->where('contents.original_language', $langs)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->orderBy('contents.cinema_release_date', 'DESC')
                                ->orderBy('contents.localized_release_date', 'DESC')
                                ->paginate(28);
                        }
                    } elseif ($langs != "") {
                        if ($minr == 0 && $maxr == 10) {
                            $contents = DB::table('contents')
                                ->whereIn('content_type', $filter_type)
                                ->where('original_language', $langs)
                                ->where('cinema_release_date', '<=', $maxy)
                                ->where('cinema_release_date', '>=', $miny)
                                ->orderBy('cinema_release_date', 'DESC')
                                ->orderBy('localized_release_date', 'DESC')
                                ->paginate(28);
                        } else {
                            $contents = DB::table('contents')
                                ->whereIn('content_type', $filter_type)
                                ->where('original_language', $langs)
                                ->where('tmdb_score', '<=', $maxr)
                                ->where('tmdb_score', '>=', $minr)
                                ->where('cinema_release_date', '<=', $maxy)
                                ->where('cinema_release_date', '>=', $miny)
                                ->orderBy('cinema_release_date', 'DESC')
                                ->orderBy('localized_release_date', 'DESC')
                                ->paginate(28);
                        }
                    } elseif ($genre != "") {
                        if ($minr == 0 && $maxr == 10) {
                            $contents = DB::table('content_genres')
                                ->leftJoin('contents', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_genres.genre_id', $genre)
                                ->whereIn('contents.content_type', $filter_type)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->orderBy('contents.cinema_release_date', 'DESC')
                                ->orderBy('contents.localized_release_date', 'DESC')
                                ->paginate(28);
                        } else {
                            $contents = DB::table('content_genres')
                                ->leftJoin('contents', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_genres.genre_id', $genre)
                                ->whereIn('contents.content_type', $filter_type)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->orderBy('contents.cinema_release_date', 'DESC')
                                ->orderBy('contents.localized_release_date', 'DESC')
                                ->paginate(28);
                        }
                    } elseif ($rated != "") {
                        if ($minr == 0 && $maxr == 10) {
                            $contents = DB::table('contents')
                                ->whereIn('content_type', $filter_type)
                                ->where('age_certification', $rated)
                                ->where('cinema_release_date', '<=', $maxy)
                                ->where('cinema_release_date', '>=', $miny)
                                ->orderBy('cinema_release_date', 'DESC')
                                ->orderBy('localized_release_date', 'DESC')
                                ->paginate(28);
                        } else {
                            $contents = DB::table('contents')
                                ->whereIn('content_type', $filter_type)
                                ->where('age_certification', $rated)
                                ->where('tmdb_score', '<=', $maxr)
                                ->where('tmdb_score', '>=', $minr)
                                ->where('cinema_release_date', '<=', $maxy)
                                ->where('cinema_release_date', '>=', $miny)
                                ->orderBy('cinema_release_date', 'DESC')
                                ->orderBy('localized_release_date', 'DESC')
                                ->paginate(28);
                        }
                    } else {
                        if ($minr == 0 && $maxr == 10) {
                            $contents = DB::table('contents')
                                ->whereIn('content_type', $filter_type)
                                ->where('cinema_release_date', '<=', $maxy)
                                ->where('cinema_release_date', '>=', $miny)
                                ->orderBy('cinema_release_date', 'DESC')
                                ->orderBy('localized_release_date', 'DESC')
                                ->paginate(28);
                        } else {
                            $contents = DB::table('contents')
                                ->whereIn('content_type', $filter_type)
                                ->where('tmdb_score', '<=', $maxr)
                                ->where('tmdb_score', '>=', $minr)
                                ->where('cinema_release_date', '<=', $maxy)
                                ->where('cinema_release_date', '>=', $miny)
                                ->orderBy('cinema_release_date', 'DESC')
                                ->orderBy('localized_release_date', 'DESC')
                                ->paginate(28);
                        }
                    }
                } else {

                    if ($langs != "" && $rated != "" && $genre != "") {
                        if ($minr == 0 && $maxr == 10) {
                            $contents = DB::table('content_genres')
                                ->leftJoin('contents', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_genres.genre_id', $genre)
                                ->whereIn('contents.content_type', $filter_type)
                                ->where('contents.age_certification', $rated)
                                ->where('contents.original_language', $langs)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->orderBy('contents.cinema_release_date', 'DESC')
                                ->orderBy('contents.localized_release_date', 'DESC')
                                ->orderBy($field, $sort_type)
                                ->paginate(28);
                        } else {
                            $contents = DB::table('content_genres')
                                ->leftJoin('contents', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_genres.genre_id', $genre)
                                ->whereIn('contents.content_type', $filter_type)
                                ->where('contents.age_certification', $rated)
                                ->where('contents.original_language', $langs)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->orderBy('contents.cinema_release_date', 'DESC')
                                ->orderBy('contents.localized_release_date', 'DESC')
                                ->orderBy($field, $sort_type)
                                ->paginate(28);
                        }
                    } elseif ($langs != "" && $rated != "") {
                        if ($minr == 0 && $maxr == 10) {
                            $contents = DB::table('contents')
                                ->whereIn('content_type', $filter_type)
                                ->where('original_language', $langs)
                                ->where('age_certification', $rated)
                                ->where('cinema_release_date', '<=', $maxy)
                                ->where('cinema_release_date', '>=', $miny)
                                ->orderBy('cinema_release_date', 'DESC')
                                ->orderBy('localized_release_date', 'DESC')
                                ->orderBy($field, $sort_type)
                                ->paginate(28);
                        } else {
                            $contents = DB::table('contents')
                                ->whereIn('content_type', $filter_type)
                                ->where('original_language', $langs)
                                ->where('age_certification', $rated)
                                ->where('tmdb_score', '<=', $maxr)
                                ->where('tmdb_score', '>=', $minr)
                                ->where('cinema_release_date', '<=', $maxy)
                                ->where('cinema_release_date', '>=', $miny)
                                ->orderBy('cinema_release_date', 'DESC')
                                ->orderBy('localized_release_date', 'DESC')
                                ->orderBy($field, $sort_type)
                                ->paginate(28);
                        }
                    } elseif ($rated != "" && $genre != "") {
                        if ($minr == 0 && $maxr == 10) {
                            $contents = DB::table('content_genres')
                                ->leftJoin('contents', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_genres.genre_id', $genre)
                                ->whereIn('contents.content_type', $filter_type)
                                ->where('contents.age_certification', $rated)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->orderBy('contents.cinema_release_date', 'DESC')
                                ->orderBy('contents.localized_release_date', 'DESC')
                                ->orderBy($field, $sort_type)
                                ->paginate(28);
                        } else {
                            $contents = DB::table('content_genres')
                                ->leftJoin('contents', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_genres.genre_id', $genre)
                                ->whereIn('contents.content_type', $filter_type)
                                ->where('contents.age_certification', $rated)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->orderBy('contents.cinema_release_date', 'DESC')
                                ->orderBy('contents.localized_release_date', 'DESC')
                                ->orderBy($field, $sort_type)
                                ->paginate(28);
                        }
                    } elseif ($langs != "" && $genre != "") {
                        if ($minr == 0 && $maxr == 10) {
                            $contents = DB::table('content_genres')
                                ->leftJoin('contents', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_genres.genre_id', $genre)
                                ->whereIn('contents.content_type', $filter_type)
                                ->where('contents.original_language', $langs)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->orderBy('contents.cinema_release_date', 'DESC')
                                ->orderBy('contents.localized_release_date', 'DESC')
                                ->orderBy($field, $sort_type)
                                ->paginate(28);
                        } else {
                            $contents = DB::table('content_genres')
                                ->leftJoin('contents', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_genres.genre_id', $genre)
                                ->whereIn('contents.content_type', $filter_type)
                                ->where('contents.original_language', $langs)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->orderBy('contents.cinema_release_date', 'DESC')
                                ->orderBy('contents.localized_release_date', 'DESC')
                                ->orderBy($field, $sort_type)
                                ->paginate(28);
                        }
                    } elseif ($langs != "") {
                        if ($minr == 0 && $maxr == 10) {
                            $contents = DB::table('contents')
                                ->whereIn('content_type', $filter_type)
                                ->where('original_language', $langs)
                                ->where('cinema_release_date', '<=', $maxy)
                                ->where('cinema_release_date', '>=', $miny)
                                ->orderBy('cinema_release_date', 'DESC')
                                ->orderBy('localized_release_date', 'DESC')
                                ->orderBy($field, $sort_type)
                                ->paginate(28);
                        } else {
                            $contents = DB::table('contents')
                                ->whereIn('content_type', $filter_type)
                                ->where('original_language', $langs)
                                ->where('tmdb_score', '<=', $maxr)
                                ->where('tmdb_score', '>=', $minr)
                                ->where('cinema_release_date', '<=', $maxy)
                                ->where('cinema_release_date', '>=', $miny)
                                ->orderBy('cinema_release_date', 'DESC')
                                ->orderBy('localized_release_date', 'DESC')
                                ->orderBy($field, $sort_type)
                                ->paginate(28);
                        }
                    } elseif ($genre != "") {
                        if ($minr == 0 && $maxr == 10) {
                            $contents = DB::table('content_genres')
                                ->leftJoin('contents', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_genres.genre_id', $genre)
                                ->whereIn('contents.content_type', $filter_type)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->orderBy('contents.cinema_release_date', 'DESC')
                                ->orderBy('contents.localized_release_date', 'DESC')
                                ->orderBy($field, $sort_type)
                                ->paginate(28);
                        } else {
                            $contents = DB::table('content_genres')
                                ->leftJoin('contents', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_genres.genre_id', $genre)
                                ->whereIn('contents.content_type', $filter_type)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->orderBy('contents.cinema_release_date', 'DESC')
                                ->orderBy('contents.localized_release_date', 'DESC')
                                ->orderBy($field, $sort_type)
                                ->paginate(28);
                        }
                    } elseif ($rated != "") {
                        if ($minr == 0 && $maxr == 10) {
                            $contents = DB::table('contents')
                                ->whereIn('content_type', $filter_type)
                                ->where('age_certification', $rated)
                                ->where('cinema_release_date', '<=', $maxy)
                                ->where('cinema_release_date', '>=', $miny)
                                ->orderBy('cinema_release_date', 'DESC')
                                ->orderBy('localized_release_date', 'DESC')
                                ->orderBy($field, $sort_type)
                                ->paginate(28);
                        } else {
                            $contents = DB::table('contents')
                                ->whereIn('content_type', $filter_type)
                                ->where('age_certification', $rated)
                                ->where('tmdb_score', '<=', $maxr)
                                ->where('tmdb_score', '>=', $minr)
                                ->where('cinema_release_date', '<=', $maxy)
                                ->where('cinema_release_date', '>=', $miny)
                                ->orderBy('cinema_release_date', 'DESC')
                                ->orderBy('localized_release_date', 'DESC')
                                ->orderBy($field, $sort_type)
                                ->paginate(28);
                        }
                    } else {
                        if ($minr == 0 && $maxr == 10) {
                            $contents = DB::table('contents')
                                ->whereIn('content_type', $filter_type)
                                ->where('cinema_release_date', '<=', $maxy)
                                ->where('cinema_release_date', '>=', $miny)
                                ->orderBy('contents.cinema_release_date', 'DESC')
                                ->orderBy('contents.localized_release_date', 'DESC')
                                ->orderBy($field, $sort_type)
                                ->paginate(28);
                        } else {
                            $contents = DB::table('contents')
                                ->whereIn('content_type', $filter_type)
                                ->where('tmdb_score', '<=', $maxr)
                                ->where('tmdb_score', '>=', $minr)
                                ->where('cinema_release_date', '<=', $maxy)
                                ->where('cinema_release_date', '>=', $miny)
                                ->orderBy('contents.cinema_release_date', 'DESC')
                                ->orderBy('contents.localized_release_date', 'DESC')
                                ->orderBy($field, $sort_type)
                                ->paginate(28);
                        }
                    }
                }
            } else {

                if ($sort == "") {
                    if ($langs != "" && $rated != "" && $genre != "") {
                        if ($minr == 0 && $maxr == 10) {
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->leftJoin('content_genres', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_providers.provider_id', $provider)
                                ->where('content_genres.genre_id', $genre)
                                ->where('contents.age_certification', $rated)
                                ->whereIn('contents.content_type', $filter_type)
                                ->where('contents.original_language', $langs)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->orderBy('contents.cinema_release_date', 'DESC')
                                ->orderBy('contents.localized_release_date', 'DESC')
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate(28);
                        } else {
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->leftJoin('content_genres', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_providers.provider_id', $provider)
                                ->where('content_genres.genre_id', $genre)
                                ->whereIn('contents.content_type', $filter_type)
                                ->where('contents.age_certification', $rated)
                                ->where('contents.original_language', $langs)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->orderBy('contents.cinema_release_date', 'DESC')
                                ->orderBy('contents.localized_release_date', 'DESC')
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate(28);
                        }
                    } elseif ($langs != "" && $rated != "") {
                        if ($minr == 0 && $maxr == 10) {
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->where('content_providers.provider_id', $provider)
                                ->whereIn('contents.content_type', $filter_type)
                                ->where('contents.original_language', $langs)
                                ->where('contents.age_certification', $rated)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->orderBy('contents.cinema_release_date', 'DESC')
                                ->orderBy('contents.localized_release_date', 'DESC')
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate(28);
                        } else {
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->where('content_providers.provider_id', $provider)
                                ->whereIn('contents.content_type', $filter_type)
                                ->where('contents.original_language', $langs)
                                ->where('contents.age_certification', $rated)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->orderBy('contents.cinema_release_date', 'DESC')
                                ->orderBy('contents.localized_release_date', 'DESC')
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate(28);
                        }
                    } elseif ($rated != "" && $genre != "") {
                        if ($minr == 0 && $maxr == 10) {
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->leftJoin('content_genres', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_providers.provider_id', $provider)
                                ->where('content_genres.genre_id', $genre)
                                ->where('contents.age_certification', $rated)
                                ->whereIn('contents.content_type', $filter_type)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->orderBy('contents.cinema_release_date', 'DESC')
                                ->orderBy('contents.localized_release_date', 'DESC')
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate(28);
                        } else {
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->leftJoin('content_genres', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_providers.provider_id', $provider)
                                ->where('content_genres.genre_id', $genre)
                                ->where('contents.age_certification', $rated)
                                ->whereIn('contents.content_type', $filter_type)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->orderBy('contents.cinema_release_date', 'DESC')
                                ->orderBy('contents.localized_release_date', 'DESC')
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate(28);
                        }
                    } elseif ($langs != "" && $genre != "") {
                        if ($minr == 0 && $maxr == 10) {
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->leftJoin('content_genres', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_providers.provider_id', $provider)
                                ->where('content_genres.genre_id', $genre)
                                ->whereIn('contents.content_type', $filter_type)
                                ->where('contents.original_language', $langs)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->orderBy('contents.cinema_release_date', 'DESC')
                                ->orderBy('contents.localized_release_date', 'DESC')
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate(28);
                        } else {
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->leftJoin('content_genres', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_providers.provider_id', $provider)
                                ->where('content_genres.genre_id', $genre)
                                ->whereIn('contents.content_type', $filter_type)
                                ->where('contents.original_language', $langs)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->orderBy('contents.cinema_release_date', 'DESC')
                                ->orderBy('contents.localized_release_date', 'DESC')
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate(28);
                        }
                    } elseif ($langs != "") {
                        if ($minr == 0 && $maxr == 10) {
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->where('content_providers.provider_id', $provider)
                                ->whereIn('contents.content_type', $filter_type)
                                ->where('contents.original_language', $langs)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->orderBy('contents.cinema_release_date', 'DESC')
                                ->orderBy('contents.localized_release_date', 'DESC')
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate(28);
                        } else {
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->where('content_providers.provider_id', $provider)
                                ->whereIn('contents.content_type', $filter_type)
                                ->where('contents.original_language', $langs)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->orderBy('contents.cinema_release_date', 'DESC')
                                ->orderBy('contents.localized_release_date', 'DESC')
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate(28);
                        }
                    } elseif ($genre != "") {
                        if ($minr == 0 && $maxr == 10) {
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->leftJoin('content_genres', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_providers.provider_id', $provider)
                                ->where('content_genres.genre_id', $genre)
                                ->whereIn('contents.content_type', $filter_type)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->orderBy('contents.cinema_release_date', 'DESC')
                                ->orderBy('contents.localized_release_date', 'DESC')
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate(28);
                        } else {
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->leftJoin('content_genres', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_providers.provider_id', $provider)
                                ->where('content_genres.genre_id', $genre)
                                ->whereIn('contents.content_type', $filter_type)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->orderBy('contents.cinema_release_date', 'DESC')
                                ->orderBy('contents.localized_release_date', 'DESC')
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate(28);
                        }
                    } elseif ($rated != "") {
                        if ($minr == 0 && $maxr == 10) {
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->where('content_providers.provider_id', $provider)
                                ->whereIn('contents.content_type', $filter_type)
                                ->where('contents.age_certification', $rated)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->orderBy('contents.cinema_release_date', 'DESC')
                                ->orderBy('contents.localized_release_date', 'DESC')
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate(28);
                        } else {
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->where('content_providers.provider_id', $provider)
                                ->whereIn('contents.content_type', $filter_type)
                                ->where('contents.age_certification', $rated)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->orderBy('contents.cinema_release_date', 'DESC')
                                ->orderBy('contents.localized_release_date', 'DESC')
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate(28);
                        }
                    } else {
                        if ($minr == 0 && $maxr == 10) {
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->where('content_providers.provider_id', $provider)
                                ->whereIn('contents.content_type', $filter_type)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->orderBy('contents.cinema_release_date', 'DESC')
                                ->orderBy('contents.localized_release_date', 'DESC')
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate(28);
                        } else {
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->where('content_providers.provider_id', $provider)
                                ->whereIn('contents.content_type', $filter_type)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->orderBy('contents.cinema_release_date', 'DESC')
                                ->orderBy('contents.localized_release_date', 'DESC')
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate(28);
                        }
                    }
                } else {

                    if ($langs != "" && $rated != "" && $genre != "") {
                        if ($minr == 0 && $maxr == 10) {
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->leftJoin('content_genres', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_providers.provider_id', $provider)
                                ->where('content_genres.genre_id', $genre)
                                ->where('contents.age_certification', $rated)
                                ->whereIn('contents.content_type', $filter_type)
                                ->where('contents.original_language', $langs)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->orderBy('contents.cinema_release_date', 'DESC')
                                ->orderBy('contents.localized_release_date', 'DESC')
                                ->orderBy($field, $sort_type)
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate(28);
                        } else {
                            $contents = DB::table('content_genres')
                                ->leftJoin('contents', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_genres.genre_id', $genre)
                                ->whereIn('contents.content_type', $filter_type)
                                ->where('contents.age_certification', $rated)
                                ->where('contents.original_language', $langs)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->orderBy('contents.cinema_release_date', 'DESC')
                                ->orderBy('contents.localized_release_date', 'DESC')
                                ->orderBy($field, $sort_type)
                                ->paginate(28);
                        }
                    } elseif ($langs != "" && $rated != "") {
                        if ($minr == 0 && $maxr == 10) {
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->where('content_providers.provider_id', $provider)
                                ->whereIn('contents.content_type', $filter_type)
                                ->where('contents.original_language', $langs)
                                ->where('contents.age_certification', $rated)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->orderBy('contents.cinema_release_date', 'DESC')
                                ->orderBy('contents.localized_release_date', 'DESC')
                                ->orderBy($field, $sort_type)
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate(28);
                        } else {
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->where('content_providers.provider_id', $provider)
                                ->whereIn('contents.content_type', $filter_type)
                                ->where('contents.original_language', $langs)
                                ->where('contents.age_certification', $rated)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->orderBy('contents.cinema_release_date', 'DESC')
                                ->orderBy('contents.localized_release_date', 'DESC')
                                ->orderBy($field, $sort_type)
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate(28);
                        }
                    } elseif ($rated != "" && $genre != "") {
                        if ($minr == 0 && $maxr == 10) {
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->leftJoin('content_genres', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_providers.provider_id', $provider)
                                ->where('content_genres.genre_id', $genre)
                                ->where('contents.age_certification', $rated)
                                ->whereIn('contents.content_type', $filter_type)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->orderBy('contents.cinema_release_date', 'DESC')
                                ->orderBy('contents.localized_release_date', 'DESC')
                                ->orderBy($field, $sort_type)
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate(28);
                        } else {
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->leftJoin('content_genres', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_providers.provider_id', $provider)
                                ->where('content_genres.genre_id', $genre)
                                ->where('contents.age_certification', $rated)
                                ->whereIn('contents.content_type', $filter_type)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->orderBy('contents.cinema_release_date', 'DESC')
                                ->orderBy('contents.localized_release_date', 'DESC')
                                ->orderBy($field, $sort_type)
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate(28);
                        }
                    } elseif ($langs != "" && $genre != "") {
                        if ($minr == 0 && $maxr == 10) {
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->leftJoin('content_genres', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_providers.provider_id', $provider)
                                ->where('content_genres.genre_id', $genre)
                                ->whereIn('contents.content_type', $filter_type)
                                ->where('contents.original_language', $langs)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->orderBy('contents.cinema_release_date', 'DESC')
                                ->orderBy('contents.localized_release_date', 'DESC')
                                ->orderBy($field, $sort_type)
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate(28);
                        } else {
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->leftJoin('content_genres', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_providers.provider_id', $provider)
                                ->where('content_genres.genre_id', $genre)
                                ->whereIn('contents.content_type', $filter_type)
                                ->where('contents.original_language', $langs)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->orderBy('contents.cinema_release_date', 'DESC')
                                ->orderBy('contents.localized_release_date', 'DESC')
                                ->orderBy($field, $sort_type)
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate(28);
                        }
                    } elseif ($langs != "") {
                        if ($minr == 0 && $maxr == 10) {
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->where('content_providers.provider_id', $provider)
                                ->whereIn('contents.content_type', $filter_type)
                                ->where('contents.original_language', $langs)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->orderBy('contents.cinema_release_date', 'DESC')
                                ->orderBy('contents.localized_release_date', 'DESC')
                                ->orderBy($field, $sort_type)
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate(28);
                        } else {
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->where('content_providers.provider_id', $provider)
                                ->whereIn('contents.content_type', $filter_type)
                                ->where('contents.original_language', $langs)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->orderBy('contents.cinema_release_date', 'DESC')
                                ->orderBy('contents.localized_release_date', 'DESC')
                                ->orderBy($field, $sort_type)
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate(28);
                        }
                    } elseif ($genre != "") {
                        if ($minr == 0 && $maxr == 10) {
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->leftJoin('content_genres', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_providers.provider_id', $provider)
                                ->where('content_genres.genre_id', $genre)
                                ->whereIn('contents.content_type', $filter_type)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->orderBy('contents.cinema_release_date', 'DESC')
                                ->orderBy('contents.localized_release_date', 'DESC')
                                ->orderBy($field, $sort_type)
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate(28);
                        } else {
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->leftJoin('content_genres', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_providers.provider_id', $provider)
                                ->where('content_genres.genre_id', $genre)
                                ->whereIn('contents.content_type', $filter_type)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->orderBy('contents.cinema_release_date', 'DESC')
                                ->orderBy('contents.localized_release_date', 'DESC')
                                ->orderBy($field, $sort_type)
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate(28);
                        }
                    } elseif ($rated != "") {
                        if ($minr == 0 && $maxr == 10) {
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->where('content_providers.provider_id', $provider)
                                ->whereIn('contents.content_type', $filter_type)
                                ->where('contents.age_certification', $rated)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->orderBy('contents.cinema_release_date', 'DESC')
                                ->orderBy('contents.localized_release_date', 'DESC')
                                ->orderBy($field, $sort_type)
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate(28);
                        } else {
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->where('content_providers.provider_id', $provider)
                                ->whereIn('contents.content_type', $filter_type)
                                ->where('contents.original_language', $langs)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->orderBy('contents.cinema_release_date', 'DESC')->orderBy($field, $sort_type)
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate(28);
                        }
                    } else {
                        if ($minr == 0 && $maxr == 10) {
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->where('content_providers.provider_id', $provider)
                                ->whereIn('contents.content_type', $filter_type)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->orderBy('contents.localized_release_date', 'DESC')->orderBy($field, $sort_type)
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate(28);
                        } else {
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->where('content_providers.provider_id', $provider)
                                ->whereIn('contents.content_type', $filter_type)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->orderBy($field, $sort_type)
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate(28);
                        }
                    }
                }
            }
        }
        $miny = explode('-', $miny)[0];
        $maxy = explode('-', $maxy)[0];

        if (isset($_GET['provider'])) {
            $provider = $_GET['provider'];
        }
        $contents = $contents->appends(\Input::except('page'));
        foreach ($contents as $key => $value) {
            $this->resizeImage($value->poster);
        }
        return view('khoj.home', compact('contents', 'page', 'type', 'miny', 'maxy', 'langs', 'genre', 'rated', 'minr', 'maxr', 'sort', 'tab', 'provider'));
    }


    public function recentlyAdded(Request $request)
    {
        $date = date('Y-m-d');
        $page = 0;
        $langs = "";
        $rated = "";
        $sort = "";
        $field = "";
        $sort_type = "";
        $provider = "";
        $genre = "";
        $minr = 0;
        $maxr = 10;
        $type = "all";
        $tab = 'Recent';

        // $miny = DB::table('contents')->where('original_release_year','>','1900')->min('original_release_year');
        $miny = date("Y-m-d", strtotime("-6 months"));;
        $maxy = $date;

        if (isset($_GET['page'])) {
            $page = $_GET['page'] - 1;
        }

        if (isset($_GET['lang'])) {
            $langs = $_GET['lang'];
        }
        if (isset($_GET['provider'])) {
            $provider = explode('_', $_GET['provider'])[1];
        }

        if (isset($_GET['sort'])) {
            $sort = $_GET['sort'];
            if ($sort == "alpha") {
                $field = 'title';
                $sort_type = 'ASC';
            } elseif ($sort == 'rate') {
                $field = 'tmdb_score';
                $sort_type = 'DESC';
            } elseif ($sort == 'year') {
                $field = 'cinema_release_date';
                $sort_type = 'DESC';
            } elseif ($sort == 'imdb_score') {
                $field = 'imdb_score';
                $sort_type = 'DESC';
            } else {
                $field = 'content_id';
                $sort_type = 'DESC';
            }
        }

        if ((isset($_GET['sort']) && isset($_GET['genre'])) || (isset($_GET['sort']) && isset($_GET['provider']))) {
            $sort = $_GET['sort'];
            if ($sort == "alpha") {
                $field = 'contents.title';
                $sort_type = 'ASC';
            } elseif ($sort == 'rate') {
                $field = 'contents.tmdb_score';
                $sort_type = 'DESC';
            } elseif ($sort == 'year') {
                $field = 'contents.cinema_release_date';
                $sort_type = 'DESC';
            } elseif ($sort == 'imdb_score') {
                $field = 'contents.imdb_score';
                $sort_type = 'DESC';
            } else {
                $field = 'contents.content_id';
                $sort_type = 'DESC';
            }
        }

        if (isset($_GET['rated'])) {
            $rated = $_GET['rated'];
        }

        if (isset($_GET['searchtype'])) {
            $type = $_GET['searchtype'];
        }

        if (isset($_GET['genre'])) {
            $genre = $_GET['genre'];
        }

        if (isset($_GET['minr'])) {
            $minr = $_GET['minr'];
        }
        if (isset($_GET['maxr'])) {
            $maxr = $_GET['maxr'];
        }

        if (isset($_GET['maxy'])) {
            $maxy = $_GET['maxy'] . "-12-31";
        }

        $contents = DB::table('content_providers')
            ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
            ->whereIn('contents.content_type', ['show', 'movie'])
            ->orderBy('contents.created_at', 'desc')
            ->select('contents.*')

            ->distinct('contents.content_id')
            ->paginate(28);
        $miny = explode('-', $miny)[0];
        $maxy = explode('-', $maxy)[0];

        if (isset($_GET['provider'])) {
            $provider = $_GET['provider'];
        }
        $contents = $contents->appends(\Input::except('page'));
        foreach ($contents as $key => $value) {
            $this->resizeImage($value->poster);
        }
        return view('khoj.home', compact('contents', 'page', 'type', 'miny', 'maxy', 'langs', 'genre', 'rated', 'minr', 'maxr', 'sort', 'tab', 'provider'));
    }

    public function formattmdbcontent($movie)
    {
        // dd($movie);
        $db_check = DB::table('contents')->where('content_source_id', $movie->id)->first();

        if (!$db_check) {
            $movie->poster_path = $this->storetmdbimage($movie->poster_path);
            $db_data = DB::table('contents')->insert(
                array(
                    'title' => $movie->title,
                    'original_title' => $movie->original_title,
                    'content_type' => 'movie',
                    'short_description' => $movie->overview,
                    'poster' => $movie->poster_path ?? '',
                    'content_source' => 'tmdb',
                    'content_source_id' => $movie->id,
                    'full_path' => $movie->full_path ?? "",
                    'tmdb_popularity' => $movie->popularity,
                    'tmdb_score' => 0,
                    'original_release_year' => explode('-', $movie->release_date)[0],
                    'cinema_release_date' => $movie->release_date,
                    'original_language' => $movie->original_language,
                    'tmdb_score' => $movie->popularity,
                )
            );
        } else {
        }
    }


    public function storetmdbimage($path)
    {
        if (!empty($path)) {
            // dd($path);
            // $key1 = explode('/', $newdata->poster)[2];
            $url1 = 'https://image.tmdb.org/t/p/w500' . $path;
            $url2 = 'https://image.tmdb.org/t/p/w600_and_h900_bestv2' . $path;

            $base_path = public_path();
            $keyname = $path;
            $img1 = $base_path . '/poster' . $keyname;
            $img2 = $base_path . '/thumbnail' . $keyname;
            // echo $url1;
            // die();
            file_put_contents($img1, file_get_contents($url1));
            file_put_contents($img2, file_get_contents($url2));
            // $updatedata['poster'] = $keyname;
            return $path;
        }
    }

    public function getUpcomingMovies(Request $request)
    {

        $total_pages = 0;
        $predict = "Upcoming";
        $imdb_rating = [];

        $min_date = date('Y-m-d');

        if (isset($request->page) && $request->page > 1) {
            $page = $request->page;
        } else
            $page = 1;

        $url = "https://api.themoviedb.org/3/discover/movie?api_key=f0dc06cd59a97bbddcebd05e902559e0&primary_release_date.gte=$min_date&sort_by=release_date.asc&include_adult=false&page=$page";

        $genre_url = "https://api.themoviedb.org/3/genre/movie/list?api_key=f0dc06cd59a97bbddcebd05e902559e0&language=en-US";
        $c = curl_init();
        curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($c, CURLOPT_URL, $url);
        curl_setopt($c, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($c, CURLOPT_SSL_VERIFYPEER, 0);
        $contents = curl_exec($c);
        $genrerequest = curl_init();
        curl_setopt($genrerequest, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($genrerequest, CURLOPT_URL, $genre_url);
        curl_setopt($genrerequest, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($genrerequest, CURLOPT_SSL_VERIFYPEER, false);
        $genres = curl_exec($genrerequest);

        $contents = json_decode($contents);

        if (isset($contents)) {
            $total_pages = $contents->total_pages;
            // dd($contents->results);
            foreach ($contents->results as $con) {
                $this->formattmdbcontent($con);
            }
        }
        $genres = json_decode($genres);
        $genres = $genres->genres;

        $side = "Upcoming Movies";
        $type = "Movies";
        $date = date('Y-m-d');
        $page = 0;
        $langs = "";
        $rated = "";
        $sort = "";
        $field = "";
        $sort_type = "";
        $provider = "";
        $genre = "";
        $minr = 0;
        $maxr = 10;
        $type = "all";
        $tab = 'Upcoming';

        $miny = $date;
        // $maxy = DB::table('contents')->orderBy('cinema_release_date','DESC')->first();
        $maxy = date('Y-m-d');

        if (isset($_GET['page'])) {
            $page = $_GET['page'] - 1;
        }

        if (isset($_GET['lang'])) {
            $langs = $_GET['lang'];
        }
        if (isset($_GET['provider'])) {
            $provider = explode('_', $_GET['provider'])[1];
        }

        if (isset($_GET['sort'])) {
            $sort = $_GET['sort'];
            if ($sort == "alpha") {
                $field = 'title';
                $sort_type = 'ASC';
            } elseif ($sort == 'rate') {
                $field = 'tmdb_score';
                $sort_type = 'DESC';
            } elseif ($sort == 'year') {
                $field = 'cinema_release_date';
                $sort_type = 'DESC';
            } elseif ($sort == 'imdb_score') {
                $field = 'imdb_score';
                $sort_type = 'DESC';
            } else {
                $field = 'content_id';
                $sort_type = 'DESC';
            }
        }

        if ((isset($_GET['sort']) && isset($_GET['genre'])) || (isset($_GET['sort']) && isset($_GET['provider']))) {
            $sort = $_GET['sort'];
            if ($sort == "alpha") {
                $field = 'contents.title';
                $sort_type = 'ASC';
            } elseif ($sort == 'rate') {
                $field = 'contents.tmdb_score';
                $sort_type = 'DESC';
            } elseif ($sort == 'year') {
                $field = 'contents.cinema_release_date';
                $sort_type = 'DESC';
            } elseif ($sort == 'imdb_score') {
                $field = 'contents.imdb_score';
                $sort_type = 'DESC';
            } else {
                $field = 'contents.content_id';
                $sort_type = 'DESC';
            }
        }

        if (isset($_GET['rated'])) {
            $rated = $_GET['rated'];
        }

        if (isset($_GET['searchtype'])) {
            $type = $_GET['searchtype'];
        }

        if (isset($_GET['genre'])) {
            $genre = $_GET['genre'];
        }

        if (isset($_GET['minr'])) {
            $minr = $_GET['minr'];
        }
        if (isset($_GET['maxr'])) {
            $maxr = $_GET['maxr'];
        }

        $miny = date('Y-m-d');
        $maxy = date("Y-m-d", strtotime("+6 months"));;

        if ($type == 'all') {
            if ($provider == "") {
                if ($sort == "") {

                    if ($langs != "" && $rated != "" && $genre != "") {
                        $contents = DB::table('content_genres')
                            ->leftJoin('contents', 'content_genres.content_id', 'contents.content_id')
                            ->where('content_genres.genre_id', $genre)
                            ->where('contents.tmdb_score', '<=', $maxr)
                            ->where('contents.tmdb_score', '>=', $minr)
                            ->where('contents.cinema_release_date', '<=', $maxy)
                            ->where('contents.cinema_release_date', '>', $miny)
                            ->where('contents.age_certification', $rated)
                            ->where('contents.original_language', $langs)
                            ->where('contents.content_type', '!=', 'tv-show')
                            ->orderBy('content_genres.content_id', 'DESC')
                            ->paginate(28);
                    } elseif ($langs != "" && $rated != "") {
                        $contents = DB::table('contents')
                            ->where('original_language', $langs)
                            ->where('age_certification', $rated)
                            ->where('tmdb_score', '<=', $maxr)
                            ->where('tmdb_score', '>=', $minr)
                            ->where('cinema_release_date', '<=', $maxy)
                            ->where('cinema_release_date', '>', $miny)
                            ->where('content_type', '!=', 'tv-show')
                            ->paginate(28);
                    } elseif ($rated != "" && $genre != "") {

                        $contents = DB::table('content_genres')
                            ->leftJoin('contents', 'content_genres.content_id', 'contents.content_id')
                            ->where('content_genres.genre_id', $genre)
                            ->where('contents.age_certification', $rated)
                            ->where('contents.tmdb_score', '<=', $maxr)
                            ->where('contents.tmdb_score', '>=', $minr)
                            ->where('contents.cinema_release_date', '<=', $maxy)
                            ->where('contents.cinema_release_date', '>', $miny)
                            ->where('contents.content_type', '!=', 'tv-show')
                            ->orderBy('content_genres.content_id', 'DESC')
                            ->paginate(28);
                    } elseif ($langs != "" && $genre != "") {

                        $contents = DB::table('content_genres')
                            ->leftJoin('contents', 'content_genres.content_id', 'contents.content_id')
                            ->where('content_genres.genre_id', $genre)
                            ->where('contents.original_language', $langs)
                            ->where('contents.tmdb_score', '<=', $maxr)
                            ->where('contents.tmdb_score', '>=', $minr)
                            ->where('contents.cinema_release_date', '<=', $maxy)
                            ->where('contents.cinema_release_date', '>', $miny)
                            ->where('contents.content_type', '!=', 'tv-show')
                            ->orderBy('content_genres.content_id', 'DESC')
                            ->paginate(28);
                    } elseif ($langs != "") {

                        $contents = DB::table('contents')
                            ->where('original_language', $langs)
                            ->where('tmdb_score', '<=', $maxr)
                            ->where('tmdb_score', '>=', $minr)
                            ->where('cinema_release_date', '<=', $maxy)
                            ->where('cinema_release_date', '>', $miny)
                            ->where('content_type', '!=', 'tv-show')
                            ->paginate(28);
                    } elseif ($genre != "") {

                        $contents = DB::table('content_genres')
                            ->leftJoin('contents', 'content_genres.content_id', 'contents.content_id')
                            ->where('content_genres.genre_id', $genre)
                            ->where('contents.tmdb_score', '<=', $maxr)
                            ->where('contents.tmdb_score', '>=', $minr)
                            ->where('contents.cinema_release_date', '<=', $maxy)
                            ->where('contents.cinema_release_date', '>', $miny)
                            ->where('contents.content_type', '!=', 'tv-show')
                            ->orderBy('content_genres.content_id', 'DESC')
                            ->paginate(28);
                    } elseif ($rated != "") {

                        $contents = DB::table('contents')
                            ->where('age_certification', $rated)
                            ->where('tmdb_score', '<=', $maxr)
                            ->where('tmdb_score', '>=', $minr)
                            ->where('cinema_release_date', '<=', $maxy)
                            ->where('cinema_release_date', '>', $miny)
                            ->where('content_type', '!=', 'tv-show')
                            ->paginate(28);
                    } else {
                        $contents = DB::table('contents')
                            ->where('tmdb_score', '<=', $maxr)
                            ->where('tmdb_score', '>=', $minr)
                            ->where('cinema_release_date', '<=', $maxy)
                            ->where('cinema_release_date', '>', $miny)
                            ->where('content_type', '!=', 'tv-show')
                            ->orderBy('content_id', 'DESC')
                            ->paginate(28);

                        // dd($contents);
                        // dd($contents);
                    }
                } else {

                    if ($langs != "" && $rated != "" && $genre != "") {

                        $contents = DB::table('content_genres')
                            ->leftJoin('contents', 'content_genres.content_id', 'contents.content_id')
                            ->where('content_genres.genre_id', $genre)
                            ->where('contents.age_certification', $rated)
                            ->where('contents.original_language', $langs)
                            ->where('contents.tmdb_score', '<=', $maxr)
                            ->where('contents.tmdb_score', '>=', $minr)
                            ->where('contents.cinema_release_date', '<=', $maxy)
                            ->where('contents.cinema_release_date', '>', $miny)
                            ->where('contents.content_type', '!=', 'tv-show')
                            ->orderBy($field, $sort_type)
                            ->paginate(28);
                    } elseif ($langs != "" && $rated != "") {

                        $contents = DB::table('contents')
                            ->where('original_language', $langs)
                            ->where('age_certification', $rated)
                            ->where('tmdb_score', '<=', $maxr)
                            ->where('tmdb_score', '>=', $minr)
                            ->where('cinema_release_date', '<=', $maxy)
                            ->where('cinema_release_date', '>', $miny)
                            ->where('content_type', '!=', 'tv-show')
                            ->orderBy($field, $sort_type)
                            ->paginate(28);
                    } elseif ($rated != "" && $genre != "") {

                        $contents = DB::table('content_genres')
                            ->leftJoin('contents', 'content_genres.content_id', 'contents.content_id')
                            ->where('content_genres.genre_id', $genre)
                            ->where('contents.age_certification', $rated)
                            ->where('contents.tmdb_score', '<=', $maxr)
                            ->where('contents.tmdb_score', '>=', $minr)
                            ->where('contents.cinema_release_date', '<=', $maxy)
                            ->where('contents.cinema_release_date', '>', $miny)
                            ->where('contents.content_type', '!=', 'tv-show')
                            ->orderBy($field, $sort_type)
                            ->paginate(28);
                    } elseif ($langs != "" && $genre != "") {

                        $contents = DB::table('content_genres')
                            ->leftJoin('contents', 'content_genres.content_id', 'contents.content_id')
                            ->where('content_genres.genre_id', $genre)
                            ->where('contents.original_language', $langs)
                            ->where('contents.tmdb_score', '<=', $maxr)
                            ->where('contents.tmdb_score', '>=', $minr)
                            ->where('contents.cinema_release_date', '<=', $maxy)
                            ->where('contents.cinema_release_date', '>', $miny)
                            ->where('contents.content_type', '!=', 'tv-show')
                            ->orderBy($field, $sort_type)
                            ->paginate(28);
                    } elseif ($langs != "") {

                        $contents = DB::table('contents')
                            ->where('original_language', $langs)
                            ->where('tmdb_score', '<=', $maxr)
                            ->where('tmdb_score', '>=', $minr)
                            ->where('cinema_release_date', '<=', $maxy)
                            ->where('cinema_release_date', '>', $miny)
                            ->where('content_type', '!=', 'tv-show')
                            ->orderBy($field, $sort_type)
                            ->paginate(28);
                    } elseif ($genre != "") {

                        $contents = DB::table('content_genres')
                            ->leftJoin('contents', 'content_genres.content_id', 'contents.content_id')
                            ->where('content_genres.genre_id', $genre)
                            ->where('contents.tmdb_score', '<=', $maxr)
                            ->where('contents.tmdb_score', '>=', $minr)
                            ->where('contents.cinema_release_date', '<=', $maxy)
                            ->where('contents.cinema_release_date', '>', $miny)
                            ->where('contents.content_type', '!=', 'tv-show')
                            ->orderBy($field, $sort_type)
                            ->paginate(28);
                    } elseif ($rated != "") {

                        $contents = DB::table('contents')
                            ->where('age_certification', $rated)
                            ->where('tmdb_score', '<=', $maxr)
                            ->where('tmdb_score', '>=', $minr)
                            ->where('cinema_release_date', '<=', $maxy)
                            ->where('cinema_release_date', '>', $miny)
                            ->where('content_type', '!=', 'tv-show')
                            ->orderBy($field, $sort_type)
                            ->paginate(28);
                    } else {

                        $contents = DB::table('contents')
                            ->where('tmdb_score', '<=', $maxr)
                            ->where('tmdb_score', '>=', $minr)
                            ->where('cinema_release_date', '<=', $maxy)
                            ->where('cinema_release_date', '>', $miny)
                            ->where('content_type', '!=', 'tv-show')
                            ->orderBy($field, $sort_type)
                            ->paginate(28);
                    }
                }
            }

            //When Provider Not NULL
            else {
                if ($sort == "") {
                    if ($langs != "" && $rated != "" && $genre != "") {

                        $contents = DB::table('content_providers')
                            ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                            ->leftJoin('content_genres', 'content_genres.content_id', 'contents.content_id')
                            ->where('content_providers.provider_id', $provider)
                            ->where('content_genres.genre_id', $genre)
                            ->where('contents.tmdb_score', '<=', $maxr)
                            ->where('contents.tmdb_score', '>=', $minr)
                            ->where('contents.cinema_release_date', '<=', $maxy)
                            ->where('contents.cinema_release_date', '>', $miny)
                            ->where('contents.age_certification', $rated)
                            ->where('contents.original_language', $langs)
                            ->where('contents.content_type', '!=', 'tv-show')
                            ->orderBy('content_genres.content_id', 'DESC')
                            ->select('contents.*')
                            ->distinct('contents.content_id')
                            ->paginate(28);
                    } elseif ($langs != "" && $rated != "") {

                        $contents = DB::table('content_providers')
                            ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                            ->where('content_providers.provider_id', $provider)
                            ->where('contents.original_language', $langs)
                            ->where('contents.age_certification', $rated)
                            ->where('contents.tmdb_score', '<=', $maxr)
                            ->where('contents.tmdb_score', '>=', $minr)
                            ->where('contents.cinema_release_date', '<=', $maxy)
                            ->where('contents.cinema_release_date', '>', $miny)
                            ->where('contents.content_type', '!=', 'tv-show')
                            ->select('contents.*')
                            ->distinct('contents.content_id')
                            ->paginate(28);
                    } elseif ($rated != "" && $genre != "") {

                        $contents = DB::table('content_providers')
                            ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                            ->leftJoin('content_genres', 'content_genres.content_id', 'contents.content_id')
                            ->where('content_providers.provider_id', $provider)
                            ->where('content_genres.genre_id', $genre)
                            ->where('contents.age_certification', $rated)
                            ->where('contents.tmdb_score', '<=', $maxr)
                            ->where('contents.tmdb_score', '>=', $minr)
                            ->where('contents.cinema_release_date', '<=', $maxy)
                            ->where('contents.cinema_release_date', '>', $miny)
                            ->where('contents.content_type', '!=', 'tv-show')
                            ->orderBy('content_genres.content_id', 'DESC')
                            ->select('contents.*')
                            ->distinct('contents.content_id')
                            ->paginate(28);
                    } elseif ($langs != "" && $genre != "") {

                        $contents = DB::table('content_providers')
                            ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                            ->leftJoin('content_genres', 'content_genres.content_id', 'contents.content_id')
                            ->where('content_providers.provider_id', $provider)
                            ->where('content_genres.genre_id', $genre)
                            ->where('contents.original_language', $langs)
                            ->where('contents.tmdb_score', '<=', $maxr)
                            ->where('contents.tmdb_score', '>=', $minr)
                            ->where('contents.cinema_release_date', '<=', $maxy)
                            ->where('contents.cinema_release_date', '>', $miny)
                            ->where('contents.content_type', '!=', 'tv-show')
                            ->orderBy('content_genres.content_id', 'DESC')
                            ->select('contents.*')
                            ->distinct('contents.content_id')
                            ->paginate(28);
                    } elseif ($langs != "") {

                        $contents = DB::table('content_providers')
                            ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                            ->where('content_providers.provider_id', $provider)
                            ->where('contents.original_language', $langs)
                            ->where('contents.tmdb_score', '<=', $maxr)
                            ->where('contents.tmdb_score', '>=', $minr)
                            ->where('contents.cinema_release_date', '<=', $maxy)
                            ->where('contents.cinema_release_date', '>', $miny)
                            ->where('contents.content_type', '!=', 'tv-show')
                            ->select('contents.*')
                            ->distinct('contents.content_id')
                            ->paginate(28);
                    } elseif ($genre != "") {

                        $contents = DB::table('content_providers')
                            ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                            ->leftJoin('content_genres', 'content_genres.content_id', 'contents.content_id')
                            ->where('content_providers.provider_id', $provider)
                            ->where('content_genres.genre_id', $genre)
                            ->where('contents.tmdb_score', '<=', $maxr)
                            ->where('contents.tmdb_score', '>=', $minr)
                            ->where('contents.cinema_release_date', '<=', $maxy)
                            ->where('contents.cinema_release_date', '>', $miny)
                            ->where('contents.content_type', '!=', 'tv-show')
                            ->orderBy('content_genres.content_id', 'DESC')
                            ->select('contents.*')
                            ->distinct('contents.content_id')
                            ->paginate(28);
                    } elseif ($rated != "") {

                        $contents = DB::table('content_providers')
                            ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                            ->where('content_providers.provider_id', $provider)
                            ->where('contents.age_certification', $rated)
                            ->where('contents.tmdb_score', '<=', $maxr)
                            ->where('contents.tmdb_score', '>=', $minr)
                            ->where('contents.cinema_release_date', '<=', $maxy)
                            ->where('contents.cinema_release_date', '>', $miny)
                            ->where('contents.content_type', '!=', 'tv-show')
                            ->select('contents.*')
                            ->distinct('contents.content_id')
                            ->paginate(28);
                    } else {

                        $contents = DB::table('content_providers')
                            ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                            ->where('content_providers.provider_id', $provider)
                            ->where('contents.tmdb_score', '<=', $maxr)
                            ->where('contents.tmdb_score', '>=', $minr)
                            ->where('contents.cinema_release_date', '<=', $maxy)
                            ->where('contents.cinema_release_date', '>', $miny)
                            ->where('contents.content_type', '!=', 'tv-show')
                            ->orderBy('contents.content_id', 'DESC')
                            ->select('contents.*')
                            ->distinct('contents.content_id')
                            ->paginate(28);
                        // dd($contents);
                    }
                } else {

                    if ($langs != "" && $rated != "" && $genre != "") {
                        $contents = DB::table('content_providers')
                            ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                            ->leftJoin('content_genres', 'content_genres.content_id', 'contents.content_id')
                            ->where('content_providers.provider_id', $provider)
                            ->where('content_genres.genre_id', $genre)
                            ->where('contents.age_certification', $rated)
                            ->where('contents.original_language', $langs)
                            ->where('contents.tmdb_score', '<=', $maxr)
                            ->where('contents.tmdb_score', '>=', $minr)
                            ->where('contents.cinema_release_date', '<=', $maxy)
                            ->where('contents.cinema_release_date', '>', $miny)
                            ->where('contents.content_type', '!=', 'tv-show')
                            ->orderBy($field, $sort_type)
                            ->select('contents.*')
                            ->distinct('contents.content_id')
                            ->paginate(28);
                    } elseif ($langs != "" && $rated != "") {

                        $contents = DB::table('content_providers')
                            ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                            ->where('content_providers.provider_id', $provider)
                            ->where('contents.original_language', $langs)
                            ->where('contents.age_certification', $rated)
                            ->where('contents.tmdb_score', '<=', $maxr)
                            ->where('contents.tmdb_score', '>=', $minr)
                            ->where('contents.cinema_release_date', '<=', $maxy)
                            ->where('contents.cinema_release_date', '>', $miny)
                            ->where('contents.content_type', '!=', 'tv-show')
                            ->orderBy('contents.content_id', 'DESC')
                            ->select('contents.*')
                            ->distinct('contents.content_id')
                            ->paginate(28);
                    } elseif ($rated != "" && $genre != "") {

                        $contents = DB::table('content_providers')
                            ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                            ->leftJoin('content_genres', 'content_genres.content_id', 'contents.content_id')
                            ->where('content_providers.provider_id', $provider)
                            ->where('content_genres.genre_id', $genre)
                            ->where('contents.age_certification', $rated)
                            ->where('contents.tmdb_score', '<=', $maxr)
                            ->where('contents.tmdb_score', '>=', $minr)
                            ->where('contents.cinema_release_date', '<=', $maxy)
                            ->where('contents.cinema_release_date', '>', $miny)
                            ->where('contents.content_type', '!=', 'tv-show')
                            ->orderBy($field, $sort_type)
                            ->select('contents.*')
                            ->distinct('contents.content_id')
                            ->paginate(28);
                    } elseif ($langs != "" && $genre != "") {

                        $contents = DB::table('content_providers')
                            ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                            ->leftJoin('content_genres', 'content_genres.content_id', 'contents.content_id')
                            ->where('content_providers.provider_id', $provider)
                            ->where('content_genres.genre_id', $genre)
                            ->where('contents.original_language', $langs)
                            ->where('contents.tmdb_score', '<=', $maxr)
                            ->where('contents.tmdb_score', '>=', $minr)
                            ->where('contents.cinema_release_date', '<=', $maxy)
                            ->where('contents.cinema_release_date', '>', $miny)
                            ->where('contents.content_type', '!=', 'tv-show')
                            ->orderBy($field, $sort_type)
                            ->select('contents.*')
                            ->distinct('contents.content_id')
                            ->paginate(28);
                    } elseif ($langs != "") {

                        $contents = DB::table('content_providers')
                            ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                            ->where('content_providers.provider_id', $provider)
                            ->where('contents.original_language', $langs)
                            ->where('contents.tmdb_score', '<=', $maxr)
                            ->where('contents.tmdb_score', '>=', $minr)
                            ->where('contents.cinema_release_date', '<=', $maxy)
                            ->where('contents.cinema_release_date', '>', $miny)
                            ->where('contents.content_type', '!=', 'tv-show')
                            ->orderBy($field, $sort_type)
                            ->select('contents.*')
                            ->distinct('contents.content_id')
                            ->paginate(28);
                    } elseif ($genre != "") {

                        $contents = DB::table('content_providers')
                            ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                            ->leftJoin('content_genres', 'content_genres.content_id', 'contents.content_id')
                            ->where('content_providers.provider_id', $provider)
                            ->where('content_genres.genre_id', $genre)
                            ->where('contents.tmdb_score', '<=', $maxr)
                            ->where('contents.tmdb_score', '>=', $minr)
                            ->where('contents.cinema_release_date', '<=', $maxy)
                            ->where('contents.cinema_release_date', '>', $miny)
                            ->where('contents.content_type', '!=', 'tv-show')
                            ->orderBy($field, $sort_type)
                            ->select('contents.*')
                            ->distinct('contents.content_id')
                            ->paginate(28);
                    } elseif ($rated != "") {

                        $contents = DB::table('content_providers')
                            ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                            ->where('content_providers.provider_id', $provider)
                            ->where('contents.age_certification', $rated)
                            ->where('contents.tmdb_score', '<=', $maxr)
                            ->where('contents.tmdb_score', '>=', $minr)
                            ->where('contents.cinema_release_date', '<=', $maxy)
                            ->where('contents.cinema_release_date', '>', $miny)
                            ->where('contents.content_type', '!=', 'tv-show')
                            ->orderBy($field, $sort_type)
                            ->select('contents.*')
                            ->distinct('contents.content_id')
                            ->paginate(28);
                    } else {

                        $contents = DB::table('content_providers')
                            ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                            ->where('content_providers.provider_id', $provider)
                            ->where('contents.tmdb_score', '<=', $maxr)
                            ->where('contents.tmdb_score', '>=', $minr)
                            ->where('contents.cinema_release_date', '<=', $maxy)
                            ->where('contents.cinema_release_date', '>', $miny)
                            ->where('contents.content_type', '!=', 'tv-show')
                            ->orderBy($field, $sort_type)
                            ->select('contents.*')
                            ->distinct('contents.content_id')
                            ->paginate(28);
                    }
                }
            }
        } else {

            $filter_type = array();
            if ($type == 'movie') {
                $filter_type = ['movie'];
            } else {
                $filter_type = ['shows', 'tv-show', 'show'];
            }

            if ($provider == "") {

                if ($sort == "") {
                    if ($langs != "" && $rated != "" && $genre != "") {

                        $contents = DB::table('content_genres')
                            ->leftJoin('contents', 'content_genres.content_id', 'contents.content_id')
                            ->where('content_genres.genre_id', $genre)
                            ->whereIn('contents.content_type', $filter_type)
                            ->where('contents.age_certification', $rated)
                            ->where('contents.original_language', $langs)
                            ->where('contents.tmdb_score', '<=', $maxr)
                            ->where('contents.tmdb_score', '>=', $minr)
                            ->where('contents.cinema_release_date', '<=', $maxy)
                            ->where('contents.cinema_release_date', '>', $miny)
                            ->orderBy('content_genres.content_id', 'DESC')
                            ->paginate(28);
                    } elseif ($langs != "" && $rated != "") {
                        $contents = DB::table('contents')
                            ->whereIn('content_type', $filter_type)
                            ->where('original_language', $langs)
                            ->where('age_certification', $rated)
                            ->where('tmdb_score', '<=', $maxr)
                            ->where('tmdb_score', '>=', $minr)
                            ->where('cinema_release_date', '<=', $maxy)
                            ->where('cinema_release_date', '>', $miny)
                            ->paginate(28);
                    } elseif ($rated != "" && $genre != "") {

                        $contents = DB::table('content_genres')
                            ->leftJoin('contents', 'content_genres.content_id', 'contents.content_id')
                            ->where('content_genres.genre_id', $genre)
                            ->whereIn('contents.content_type', $filter_type)
                            ->where('contents.age_certification', $rated)
                            ->where('contents.tmdb_score', '<=', $maxr)
                            ->where('contents.tmdb_score', '>=', $minr)
                            ->where('contents.cinema_release_date', '<=', $maxy)
                            ->where('contents.cinema_release_date', '>', $miny)
                            ->orderBy('content_genres.content_id', 'DESC')
                            ->paginate(28);
                    } elseif ($langs != "" && $genre != "") {

                        $contents = DB::table('content_genres')
                            ->leftJoin('contents', 'content_genres.content_id', 'contents.content_id')
                            ->where('content_genres.genre_id', $genre)
                            ->whereIn('contents.content_type', $filter_type)
                            ->where('contents.original_language', $langs)
                            ->where('contents.tmdb_score', '<=', $maxr)
                            ->where('contents.tmdb_score', '>=', $minr)
                            ->where('contents.cinema_release_date', '<=', $maxy)
                            ->where('contents.cinema_release_date', '>', $miny)
                            ->orderBy('content_genres.content_id', 'DESC')
                            ->paginate(28);
                    } elseif ($langs != "") {

                        $contents = DB::table('contents')
                            ->whereIn('content_type', $filter_type)
                            ->where('original_language', $langs)
                            ->where('tmdb_score', '<=', $maxr)
                            ->where('tmdb_score', '>=', $minr)
                            ->where('cinema_release_date', '<=', $maxy)
                            ->where('cinema_release_date', '>', $miny)
                            ->paginate(28);
                    } elseif ($genre != "") {

                        $contents = DB::table('content_genres')
                            ->leftJoin('contents', 'content_genres.content_id', 'contents.content_id')
                            ->where('content_genres.genre_id', $genre)
                            ->whereIn('contents.content_type', $filter_type)
                            ->where('contents.tmdb_score', '<=', $maxr)
                            ->where('contents.tmdb_score', '>=', $minr)
                            ->where('contents.cinema_release_date', '<=', $maxy)
                            ->where('contents.cinema_release_date', '>', $miny)
                            ->orderBy('content_genres.content_id', 'DESC')
                            ->paginate(28);
                    } elseif ($rated != "") {

                        $contents = DB::table('contents')
                            ->whereIn('content_type', $filter_type)
                            ->where('age_certification', $rated)
                            ->where('tmdb_score', '<=', $maxr)
                            ->where('tmdb_score', '>=', $minr)
                            ->where('cinema_release_date', '<=', $maxy)
                            ->where('cinema_release_date', '>', $miny)
                            ->paginate(28);
                    } else {

                        $contents = DB::table('contents')
                            ->whereIn('content_type', $filter_type)
                            ->where('tmdb_score', '<=', $maxr)
                            ->where('tmdb_score', '>=', $minr)
                            ->where('cinema_release_date', '<=', $maxy)
                            ->where('cinema_release_date', '>', $miny)
                            ->paginate(28);
                        // dd($contents);
                    }
                } else {

                    if ($langs != "" && $rated != "" && $genre != "") {

                        $contents = DB::table('content_genres')
                            ->leftJoin('contents', 'content_genres.content_id', 'contents.content_id')
                            ->where('content_genres.genre_id', $genre)
                            ->whereIn('contents.content_type', $filter_type)
                            ->where('contents.age_certification', $rated)
                            ->where('contents.original_language', $langs)
                            ->where('contents.tmdb_score', '<=', $maxr)
                            ->where('contents.tmdb_score', '>=', $minr)
                            ->where('contents.cinema_release_date', '<=', $maxy)
                            ->where('contents.cinema_release_date', '>', $miny)
                            ->orderBy($field, $sort_type)
                            ->paginate(28);
                    } elseif ($langs != "" && $rated != "") {

                        $contents = DB::table('contents')
                            ->whereIn('content_type', $filter_type)
                            ->where('original_language', $langs)
                            ->where('age_certification', $rated)
                            ->where('tmdb_score', '<=', $maxr)
                            ->where('tmdb_score', '>=', $minr)
                            ->where('cinema_release_date', '<=', $maxy)
                            ->where('cinema_release_date', '>', $miny)
                            ->orderBy($field, $sort_type)
                            ->paginate(28);
                    } elseif ($rated != "" && $genre != "") {

                        $contents = DB::table('content_genres')
                            ->leftJoin('contents', 'content_genres.content_id', 'contents.content_id')
                            ->where('content_genres.genre_id', $genre)
                            ->whereIn('contents.content_type', $filter_type)
                            ->where('contents.age_certification', $rated)
                            ->where('contents.tmdb_score', '<=', $maxr)
                            ->where('contents.tmdb_score', '>=', $minr)
                            ->where('contents.cinema_release_date', '<=', $maxy)
                            ->where('contents.cinema_release_date', '>', $miny)
                            ->orderBy($field, $sort_type)
                            ->paginate(28);
                    } elseif ($langs != "" && $genre != "") {

                        $contents = DB::table('content_genres')
                            ->leftJoin('contents', 'content_genres.content_id', 'contents.content_id')
                            ->where('content_genres.genre_id', $genre)
                            ->whereIn('contents.content_type', $filter_type)
                            ->where('contents.original_language', $langs)
                            ->where('contents.tmdb_score', '<=', $maxr)
                            ->where('contents.tmdb_score', '>=', $minr)
                            ->where('contents.cinema_release_date', '<=', $maxy)
                            ->where('contents.cinema_release_date', '>', $miny)
                            ->orderBy($field, $sort_type)
                            ->paginate(28);
                    } elseif ($langs != "") {

                        $contents = DB::table('contents')
                            ->whereIn('content_type', $filter_type)
                            ->where('original_language', $langs)
                            ->where('tmdb_score', '<=', $maxr)
                            ->where('tmdb_score', '>=', $minr)
                            ->where('cinema_release_date', '<=', $maxy)
                            ->where('cinema_release_date', '>', $miny)
                            ->orderBy($field, $sort_type)
                            ->paginate(28);
                    } elseif ($genre != "") {

                        $contents = DB::table('content_genres')
                            ->leftJoin('contents', 'content_genres.content_id', 'contents.content_id')
                            ->where('content_genres.genre_id', $genre)
                            ->whereIn('contents.content_type', $filter_type)
                            ->where('contents.tmdb_score', '<=', $maxr)
                            ->where('contents.tmdb_score', '>=', $minr)
                            ->where('contents.cinema_release_date', '<=', $maxy)
                            ->where('contents.cinema_release_date', '>', $miny)
                            ->orderBy('content_genres.content_id', 'DESC')
                            ->orderBy($field, $sort_type)
                            ->paginate(28);
                    } elseif ($rated != "") {

                        $contents = DB::table('contents')
                            ->whereIn('content_type', $filter_type)
                            ->where('age_certification', $rated)
                            ->where('tmdb_score', '<=', $maxr)
                            ->where('tmdb_score', '>=', $minr)
                            ->where('cinema_release_date', '<=', $maxy)
                            ->where('cinema_release_date', '>', $miny)
                            ->orderBy($field, $sort_type)
                            ->paginate(28);
                    } else {

                        $contents = DB::table('contents')
                            ->whereIn('content_type', $filter_type)
                            ->where('tmdb_score', '<=', $maxr)
                            ->where('tmdb_score', '>=', $minr)
                            ->where('cinema_release_date', '<=', $maxy)
                            ->where('cinema_release_date', '>', $miny)
                            ->orderBy($field, $sort_type)
                            ->paginate(28);
                    }
                }
            } else {

                if ($sort == "") {
                    if ($langs != "" && $rated != "" && $genre != "") {

                        $contents = DB::table('content_providers')
                            ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                            ->leftJoin('content_genres', 'content_genres.content_id', 'contents.content_id')
                            ->where('content_providers.provider_id', $provider)
                            ->where('content_genres.genre_id', $genre)
                            ->whereIn('contents.content_type', $filter_type)
                            ->where('contents.age_certification', $rated)
                            ->where('contents.original_language', $langs)
                            ->where('contents.tmdb_score', '<=', $maxr)
                            ->where('contents.tmdb_score', '>=', $minr)
                            ->where('contents.cinema_release_date', '<=', $maxy)
                            ->where('contents.cinema_release_date', '>', $miny)
                            ->orderBy('content_genres.content_id', 'DESC')
                            ->select('contents.*')
                            ->distinct('contents.content_id')
                            ->paginate(28);
                    } elseif ($langs != "" && $rated != "") {

                        $contents = DB::table('content_providers')
                            ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                            ->where('content_providers.provider_id', $provider)
                            ->whereIn('contents.content_type', $filter_type)
                            ->where('contents.original_language', $langs)
                            ->where('contents.age_certification', $rated)
                            ->where('contents.tmdb_score', '<=', $maxr)
                            ->where('contents.tmdb_score', '>=', $minr)
                            ->where('contents.cinema_release_date', '<=', $maxy)
                            ->where('contents.cinema_release_date', '>', $miny)
                            ->select('contents.*')
                            ->distinct('contents.content_id')
                            ->paginate(28);
                    } elseif ($rated != "" && $genre != "") {

                        $contents = DB::table('content_providers')
                            ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                            ->leftJoin('content_genres', 'content_genres.content_id', 'contents.content_id')
                            ->where('content_providers.provider_id', $provider)
                            ->where('content_genres.genre_id', $genre)
                            ->where('contents.age_certification', $rated)
                            ->whereIn('contents.content_type', $filter_type)
                            ->where('contents.tmdb_score', '<=', $maxr)
                            ->where('contents.tmdb_score', '>=', $minr)
                            ->where('contents.cinema_release_date', '<=', $maxy)
                            ->where('contents.cinema_release_date', '>', $miny)
                            ->orderBy('content_genres.content_id', 'DESC')
                            ->select('contents.*')
                            ->distinct('contents.content_id')
                            ->paginate(28);
                    } elseif ($langs != "" && $genre != "") {

                        $contents = DB::table('content_providers')
                            ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                            ->leftJoin('content_genres', 'content_genres.content_id', 'contents.content_id')
                            ->where('content_providers.provider_id', $provider)
                            ->where('content_genres.genre_id', $genre)
                            ->whereIn('contents.content_type', $filter_type)
                            ->where('contents.original_language', $langs)
                            ->where('contents.tmdb_score', '<=', $maxr)
                            ->where('contents.tmdb_score', '>=', $minr)
                            ->where('contents.cinema_release_date', '<=', $maxy)
                            ->where('contents.cinema_release_date', '>', $miny)
                            ->orderBy('content_genres.content_id', 'DESC')
                            ->select('contents.*')
                            ->distinct('contents.content_id')
                            ->paginate(28);
                    } elseif ($langs != "") {

                        $contents = DB::table('content_providers')
                            ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                            ->where('content_providers.provider_id', $provider)
                            ->whereIn('contents.content_type', $filter_type)
                            ->where('contents.original_language', $langs)
                            ->where('contents.tmdb_score', '<=', $maxr)
                            ->where('contents.tmdb_score', '>=', $minr)
                            ->where('contents.cinema_release_date', '<=', $maxy)
                            ->where('contents.cinema_release_date', '>', $miny)
                            ->select('contents.*')
                            ->distinct('contents.content_id')
                            ->paginate(28);
                    } elseif ($genre != "") {

                        $contents = DB::table('content_providers')
                            ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                            ->leftJoin('content_genres', 'content_genres.content_id', 'contents.content_id')
                            ->where('content_providers.provider_id', $provider)
                            ->where('content_genres.genre_id', $genre)
                            ->whereIn('contents.content_type', $filter_type)
                            ->where('contents.tmdb_score', '<=', $maxr)
                            ->where('contents.tmdb_score', '>=', $minr)
                            ->where('contents.cinema_release_date', '<=', $maxy)
                            ->where('contents.cinema_release_date', '>', $miny)
                            ->orderBy('content_genres.content_id', 'DESC')
                            ->select('contents.*')
                            ->distinct('contents.content_id')
                            ->paginate(28);
                    } elseif ($rated != "") {
                        $contents = DB::table('content_providers')
                            ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                            ->where('content_providers.provider_id', $provider)
                            ->whereIn('contents.content_type', $filter_type)
                            ->where('contents.age_certification', $rated)
                            ->where('contents.tmdb_score', '<=', $maxr)
                            ->where('contents.tmdb_score', '>=', $minr)
                            ->where('contents.cinema_release_date', '<=', $maxy)
                            ->where('contents.cinema_release_date', '>', $miny)
                            ->select('contents.*')
                            ->distinct('contents.content_id')
                            ->paginate(28);
                    } else {

                        $contents = DB::table('content_providers')
                            ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                            ->where('content_providers.provider_id', $provider)
                            ->whereIn('contents.content_type', $filter_type)
                            ->where('contents.tmdb_score', '<=', $maxr)
                            ->where('contents.tmdb_score', '>=', $minr)
                            ->where('contents.cinema_release_date', '<=', $maxy)
                            ->where('contents.cinema_release_date', '>', $miny)
                            ->select('contents.*')
                            ->distinct('contents.content_id')
                            ->paginate(28);
                    }
                } else {

                    if ($langs != "" && $rated != "" && $genre != "") {

                        $contents = DB::table('content_genres')
                            ->leftJoin('contents', 'content_genres.content_id', 'contents.content_id')
                            ->where('content_genres.genre_id', $genre)
                            ->whereIn('contents.content_type', $filter_type)
                            ->where('contents.age_certification', $rated)
                            ->where('contents.original_language', $langs)
                            ->where('contents.tmdb_score', '<=', $maxr)
                            ->where('contents.tmdb_score', '>=', $minr)
                            ->where('contents.cinema_release_date', '<=', $maxy)
                            ->where('contents.cinema_release_date', '>', $miny)
                            ->orderBy($field, $sort_type)
                            ->paginate(28);
                    } elseif ($langs != "" && $rated != "") {

                        $contents = DB::table('content_providers')
                            ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                            ->where('content_providers.provider_id', $provider)
                            ->whereIn('contents.content_type', $filter_type)
                            ->where('contents.original_language', $langs)
                            ->where('contents.age_certification', $rated)
                            ->where('contents.tmdb_score', '<=', $maxr)
                            ->where('contents.tmdb_score', '>=', $minr)
                            ->where('contents.cinema_release_date', '<=', $maxy)
                            ->where('contents.cinema_release_date', '>', $miny)
                            ->orderBy($field, $sort_type)
                            ->select('contents.*')
                            ->distinct('contents.content_id')
                            ->paginate(28);
                    } elseif ($rated != "" && $genre != "") {

                        $contents = DB::table('content_providers')
                            ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                            ->leftJoin('content_genres', 'content_genres.content_id', 'contents.content_id')
                            ->where('content_providers.provider_id', $provider)
                            ->where('content_genres.genre_id', $genre)
                            ->where('contents.age_certification', $rated)
                            ->whereIn('contents.content_type', $filter_type)
                            ->where('contents.tmdb_score', '<=', $maxr)
                            ->where('contents.tmdb_score', '>=', $minr)
                            ->where('contents.cinema_release_date', '<=', $maxy)
                            ->where('contents.cinema_release_date', '>', $miny)
                            ->orderBy($field, $sort_type)
                            ->select('contents.*')
                            ->distinct('contents.content_id')
                            ->paginate(28);
                    } elseif ($langs != "" && $genre != "") {

                        $contents = DB::table('content_providers')
                            ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                            ->leftJoin('content_genres', 'content_genres.content_id', 'contents.content_id')
                            ->where('content_providers.provider_id', $provider)
                            ->where('content_genres.genre_id', $genre)
                            ->whereIn('contents.content_type', $filter_type)
                            ->where('contents.original_language', $langs)
                            ->where('contents.tmdb_score', '>=', $minr)
                            ->where('contents.tmdb_score', '<=', $maxr)
                            ->where('contents.cinema_release_date', '<=', $maxy)
                            ->where('contents.cinema_release_date', '>', $miny)
                            ->orderBy($field, $sort_type)
                            ->select('contents.*')
                            ->distinct('contents.content_id')
                            ->paginate(28);
                    } elseif ($langs != "") {

                        $contents = DB::table('content_providers')
                            ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                            ->where('content_providers.provider_id', $provider)
                            ->whereIn('contents.content_type', $filter_type)
                            ->where('contents.original_language', $langs)
                            ->where('contents.tmdb_score', '<=', $maxr)
                            ->where('contents.tmdb_score', '>=', $minr)
                            ->where('contents.cinema_release_date', '<=', $maxy)
                            ->where('contents.cinema_release_date', '>', $miny)
                            ->orderBy($field, $sort_type)
                            ->select('contents.*')
                            ->distinct('contents.content_id')
                            ->paginate(28);
                    } elseif ($genre != "") {

                        $contents = DB::table('content_providers')
                            ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                            ->leftJoin('content_genres', 'content_genres.content_id', 'contents.content_id')
                            ->where('content_providers.provider_id', $provider)
                            ->where('content_genres.genre_id', $genre)
                            ->whereIn('contents.content_type', $filter_type)
                            ->where('contents.tmdb_score', '>=', $minr)
                            ->where('contents.tmdb_score', '<=', $maxr)
                            ->where('contents.cinema_release_date', '<=', $maxy)
                            ->where('contents.cinema_release_date', '>', $miny)
                            ->orderBy($field, $sort_type)
                            ->select('contents.*')
                            ->distinct('contents.content_id')
                            ->paginate(28);
                    } elseif ($rated != "") {

                        $contents = DB::table('content_providers')
                            ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                            ->where('content_providers.provider_id', $provider)
                            ->whereIn('contents.content_type', $filter_type)
                            ->where('contents.original_language', $langs)
                            ->where('contents.tmdb_score', '<=', $maxr)
                            ->where('contents.tmdb_score', '>=', $minr)
                            ->where('contents.cinema_release_date', '<=', $maxy)
                            ->where('contents.cinema_release_date', '>', $miny)
                            ->orderBy($field, $sort_type)
                            ->select('contents.*')
                            ->distinct('contents.content_id')
                            ->paginate(28);
                    } else {

                        $contents = DB::table('content_providers')
                            ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                            ->where('content_providers.provider_id', $provider)
                            ->whereIn('contents.content_type', $filter_type)
                            ->where('contents.tmdb_score', '<=', $maxr)
                            ->where('contents.tmdb_score', '>=', $minr)
                            ->where('contents.cinema_release_date', '<=', $maxy)
                            ->where('contents.cinema_release_date', '>', $miny)
                            ->orderBy($field, $sort_type)
                            ->select('contents.*')
                            ->distinct('contents.content_id')
                            ->paginate(28);
                    }
                }
            }
        }
        $miny = explode('-', $miny)[0];
        $maxy = explode('-', $maxy)[0];

        if (isset($_GET['provider'])) {
            $provider = $_GET['provider'];
        }

        $contents = $contents->appends(\Input::except('page'));
        foreach ($contents as $key => $value) {
            $this->resizeImage($value->poster);
        }
        return view('khoj.home', compact('contents', 'page', 'type', 'miny', 'maxy', 'langs', 'genre', 'rated', 'minr', 'maxr', 'sort', 'tab', 'provider'));
    }


    public function getLangSearchFilter(Request $request, $type)
    {
        $page = 0;
        if (isset($_GET['page'])) {
            $page = $_GET['page'] - 1;
        }
        $contents = DB::table('contents')
            ->where('original_language', $data)
            ->orderBy('cinema_release_date', 'ASC')
            ->paginate(28);
        $type = 'movie';

        $miny = explode('_', $miny);
        $maxy = explode('_', $maxy);
        $contents = $contents->appends(\Input::except('page'));
        foreach ($contents as $key => $value) {
            $this->resizeImage($value->poster);
        }
        return view('khoj.home', compact('type', 'contents', 'page', 'miny', 'maxy'));
    }



    public function getFreeMovies(Request $request)
    {
       

        $page = 0;
        $langs = "";
        $rated = "";
        $sort = "";
        $field = "";
        $sort_type = "";
        $provider = "";
        $genre = "";
        $minr = 0;
        $maxr = 10;
        $type = "all";
        $tab = 'Free';

        $miny = '';
        $maxy = date('Y-m-d');

        if (isset($_GET['page'])) {
            $page = $_GET['page'] - 1;
        }

        if (isset($_GET['lang'])) {
            $langs = $_GET['lang'];
        }
        if (isset($_GET['provider'])) {
            $provider = explode('_', $_GET['provider'])[1];
        }

        if (isset($_GET['sort'])) {
            $sort = $_GET['sort'];
            if ($sort == "alpha") {
                $field = 'title';
                $sort_type = 'ASC';
            } elseif ($sort == 'rate') {
                $field = 'tmdb_score';
                $sort_type = 'DESC';
            } elseif ($sort == 'year') {
                $field = 'cinema_release_date';
                $sort_type = 'DESC';
            } elseif ($sort == 'imdb_score') {
                $field = 'imdb_score';
                $sort_type = 'DESC';
            } else {
                $field = 'content_id';
                $sort_type = 'DESC';
            }
        }

        if ((isset($_GET['sort']) && isset($_GET['genre'])) || (isset($_GET['sort']) && isset($_GET['provider']))) {
            $sort = $_GET['sort'];
            if ($sort == "alpha") {
                $field = 'contents.title';
                $sort_type = 'ASC';
            } elseif ($sort == 'rate') {
                $field = 'contents.tmdb_score';
                $sort_type = 'DESC';
            } elseif ($sort == 'year') {
                $field = 'contents.cinema_release_date';
                $sort_type = 'DESC';
            } elseif ($sort == 'imdb_score') {
                $field = 'contents.imdb_score';
                $sort_type = 'DESC';
            } else {
                $field = 'contents.content_id';
                $sort_type = 'DESC';
            }
        }

        if (isset($_GET['rated'])) {
            $rated = $_GET['rated'];
        }

        if (isset($_GET['searchtype'])) {
            $type = $_GET['searchtype'];
        }
        else
            $type = "all";

        if (isset($_GET['genre'])) {
            $genre = $_GET['genre'];
        }

        if (isset($_GET['minr'])) {
            $minr = $_GET['minr'];
        }
        if (isset($_GET['maxr'])) {
            $maxr = $_GET['maxr'];
        }

        if (isset($_GET['miny'])) {
            if ($miny == '0000') {
                $miny = '';
            } else {

                $miny = $_GET['miny'] . "-01-01";
            }
        }
        if (isset($_GET['maxy'])) {
            $maxy = $_GET['maxy'] . "-12-31";
        }

        $showtype = ['show', 'movie'];

        $retail = 0;

        
        if ($type == 'all') {
            if ($provider == "") {
                if ($sort == "") {
                    if ($langs != "" && $rated != "" && $genre != "") {                        
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->leftJoin('content_genres', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_providers.retail_price', $retail)
                                ->where('content_providers.monetization_type', "!=", 'flatrate')
                                ->where('content_genres.genre_id', $genre)
                                ->where('contents.status', 1)
                                ->where('contents.age_certification', $rated)
                                ->where('contents.original_language', $langs)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->whereIn('content_type', $showtype)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->where('contents.content_type', '!=', 'tv-show')
                                ->where('contents.parent_id', '0')
                                ->orderBy('contents.cinema_release_date', 'DESC')
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate('28');
                    
                    } elseif ($langs != "" && $rated != "") {
                        
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->where('content_providers.retail_price', $retail)
                                ->where('content_providers.monetization_type', "!=", 'flatrate')
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.status', 1)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->whereIn('content_type', $showtype)
                                ->where('contents.age_certification', $rated)
                                ->where('contents.original_language', $langs)
                                ->where('contents.content_type', '!=', 'tv-show')
                                ->where('contents.parent_id', '0')
                                ->orderBy('contents.cinema_release_date', 'DESC')
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate('28');
                        
                    } elseif ($rated != "" && $genre != "") {
                        
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->leftJoin('content_genres', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_providers.retail_price', $retail)
                                ->where('content_providers.monetization_type', "!=", 'flatrate')
                                ->where('content_genres.genre_id', $genre)
                                ->where('contents.age_certification', $rated)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.status', 1)
                                ->whereIn('content_type', $showtype)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->where('contents.content_type', '!=', 'tv-show')
                                ->where('contents.parent_id', '0')
                                ->orderBy('contents.cinema_release_date', 'DESC')
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate('28');
                    
                    } elseif ($langs != "" && $genre != "") {
                        
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->leftJoin('content_genres', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_providers.retail_price', $retail)
                                ->where('content_providers.monetization_type', "!=", 'flatrate')
                                ->where('content_genres.genre_id', $genre)
                                ->where('contents.original_language', $langs)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.status', 1)
                                ->whereIn('content_type', $showtype)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->where('contents.content_type', '!=', 'tv-show')
                                ->where('contents.parent_id', '0')
                                ->orderBy('contents.cinema_release_date', 'DESC')
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate('28');
                    
                    } elseif ($langs != "") {
                        
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->where('content_providers.retail_price', $retail)
                                ->where('content_providers.monetization_type', "!=", 'flatrate')
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->whereIn('content_type', $showtype)
                                ->where('contents.original_language', $langs)
                                ->where('contents.status', 1)
                                ->where('contents.content_type', '!=', 'tv-show')
                                ->where('contents.parent_id', '0')
                                ->orderBy('contents.cinema_release_date', 'DESC')
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate('28');
                    
                    } elseif ($genre != "") {
                       
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->leftJoin('content_genres', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_providers.retail_price', $retail)
                                ->where('content_providers.provider_id', '!=', '701')
                                ->where('content_providers.monetization_type', "!=", 'flatrate')
                                ->where('content_genres.genre_id', $genre)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->whereIn('content_type', $showtype)
                                ->where('contents.status', 1)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->where('contents.content_type', '!=', 'tv-show')
                                ->where('contents.parent_id', '0')
                                ->orderBy('contents.cinema_release_date', 'DESC')
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate('28');
                    } elseif ($rated != "") {
                       
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->where('content_providers.retail_price', $retail)
                                ->where('content_providers.provider_id', '!=', '701')
                                ->where('content_providers.monetization_type', "!=", 'flatrate')
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->whereIn('content_type', $showtype)
                                ->where('contents.status', 1)
                                ->where('contents.age_certification', $rated)
                                ->where('contents.content_type', '!=', 'tv-show')
                                ->where('contents.parent_id', '0')
                                ->orderBy('contents.cinema_release_date', 'DESC')
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate('28');
                    
                    } else {
                            // echo "hi";
                            // die;
                        $page = $request->page;

                        $contents = \Cache::rememberForever('first_free'.$page, function () use ($retail, $maxr, $minr, $showtype, $request) {
                            return $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->where('content_providers.retail_price', $retail)
                                ->where('content_providers.provider_id', '!=', '701')
                                ->where('content_providers.monetization_type', "!=", 'flatrate')
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->whereIn('content_type', $showtype)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.status', 1)
                                ->where('contents.content_type', '!=', 'tv-show')
                                ->where('contents.parent_id', '0')
                                ->orderBy('contents.cinema_release_date', 'DESC')
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate('28');

                        });
                    
                    }
                } else {

                    if ($langs != "" && $rated != "" && $genre != "") {
                        
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->leftJoin('content_genres', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_providers.retail_price', $retail)
                                ->where('content_providers.provider_id', '!=', '701')
                                ->where('content_providers.monetization_type', "!=", 'flatrate')
                                ->where('content_genres.genre_id', $genre)
                                ->where('contents.age_certification', $rated)
                                ->where('contents.original_language', $langs)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->whereIn('content_type', $showtype)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->where('contents.status', 1)
                                ->where('contents.content_type', '!=', 'tv-show')
                                ->where('contents.parent_id', '0')
                                ->orderBy($field, $sort_type)
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate('28');
                        
                    } elseif ($langs != "" && $rated != "") {
                        
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->where('content_providers.retail_price', $retail)
                                ->where('content_providers.provider_id', '!=', '701')
                                ->where('content_providers.monetization_type', "!=", 'flatrate')
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->where('contents.age_certification', $rated)
                                ->whereIn('content_type', $showtype)
                                ->where('contents.original_language', $langs)
                                ->where('contents.status', 1)
                                ->where('contents.content_type', '!=', 'tv-show')
                                ->where('contents.parent_id', '0')
                                ->orderBy($field, $sort_type)
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate('28');
                    } elseif ($rated != "" && $genre != "") {
                        
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->leftJoin('content_genres', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_providers.retail_price', $retail)
                                ->where('content_providers.provider_id', '!=', '701')
                                ->where('content_providers.monetization_type', "!=", 'flatrate')
                                ->where('content_genres.genre_id', $genre)
                                ->where('contents.age_certification', $rated)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->whereIn('content_type', $showtype)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->where('contents.status', 1)
                                ->where('contents.content_type', '!=', 'tv-show')
                                ->where('contents.parent_id', '0')
                                ->orderBy($field, $sort_type)
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate('28');
                    
                    } elseif ($langs != "" && $genre != "") {
                        
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->leftJoin('content_genres', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_providers.retail_price', $retail)
                                ->where('content_providers.monetization_type', "!=", 'flatrate')
                                ->where('content_genres.genre_id', $genre)
                                ->where('contents.original_language', $langs)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->whereIn('content_type', $showtype)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->where('contents.status', 1)
                                ->where('contents.content_type', '!=', 'tv-show')
                                ->where('contents.parent_id', '0')
                                ->orderBy($field, $sort_type)
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate('28');
                
                    } elseif ($langs != "") {
                        
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->where('content_providers.retail_price', $retail)
                                ->where('content_providers.monetization_type', "!=", 'flatrate')
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->whereIn('content_type', $showtype)
                                ->where('contents.original_language', $langs)
                                ->where('contents.status', 1)
                                ->where('contents.content_type', '!=', 'tv-show')
                                ->where('contents.parent_id', '0')
                                ->orderBy($field, $sort_type)
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate('28');
                
                    } elseif ($genre != "") {
                        
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->leftJoin('content_genres', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_providers.retail_price', $retail)
                                ->where('content_providers.monetization_type', "!=", 'flatrate')
                                ->where('content_genres.genre_id', $genre)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->whereIn('content_type', $showtype)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->where('contents.status', 1)
                                ->where('contents.content_type', '!=', 'tv-show')
                                ->where('contents.parent_id', '0')
                                ->orderBy($field, $sort_type)
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate('28');
                
                    } elseif ($rated != "") {
                        
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->where('content_providers.retail_price', $retail)
                                ->where('content_providers.monetization_type', "!=", 'flatrate')
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->whereIn('content_type', $showtype)
                                ->where('contents.status', 1)
                                ->where('contents.age_certification', $rated)
                                ->where('contents.content_type', '!=', 'tv-show')
                                ->where('contents.parent_id', '0')
                                ->orderBy($field, $sort_type)
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate('28');
                    
                    } else {
                        
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->where('content_providers.retail_price', $retail)
                                ->where('content_providers.monetization_type', "!=", 'flatrate')
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                // ->where('contents.cinema_release_date','<=',$maxy)
                                ->whereIn('content_type', $showtype)
                                // ->where('contents.cinema_release_date','>=',$miny)
                                ->where('contents.content_type', '!=', 'tv-show')
                                ->where('contents.parent_id', '0')
                                ->where('contents.status', 1)
                                ->orderBy($field, $sort_type)
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate('28');
                        
                    }
                }
            } else {
                if ($sort == "") {
                    if ($langs != "" && $rated != "" && $genre != "") {
                        
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->leftJoin('content_genres', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_providers.retail_price', $retail)
                                ->where('content_providers.monetization_type', "!=", 'flatrate')
                                ->where('content_providers.provider_id', $provider)
                                ->where('content_genres.genre_id', $genre)
                                ->where('contents.age_certification', $rated)
                                ->where('contents.original_language', $langs)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->whereIn('content_type', $showtype)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->where('contents.status', 1)
                                ->where('contents.content_type', '!=', 'tv-show')
                                ->where('contents.parent_id', '0')
                                ->orderBy('contents.cinema_release_date', 'DESC')
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate('28');
                    
                    } elseif ($langs != "" && $rated != "") {
                        
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->where('content_providers.retail_price', $retail)
                                ->where('content_providers.monetization_type', "!=", 'flatrate')
                                ->where('content_providers.provider_id', $provider)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->where('contents.age_certification', $rated)
                                ->whereIn('content_type', $showtype)
                                ->where('contents.original_language', $langs)
                                ->where('contents.content_type', '!=', 'tv-show')
                                ->where('contents.status', 1)
                                ->where('contents.parent_id', '0')
                                ->orderBy('contents.cinema_release_date', 'DESC')
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate('28');
                        
                    } elseif ($rated != "" && $genre != "") {
                        
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->leftJoin('content_genres', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_providers.retail_price', $retail)
                                ->where('content_providers.monetization_type', "!=", 'flatrate')
                                ->where('content_providers.provider_id', $provider)
                                ->where('content_genres.genre_id', $genre)
                                ->where('contents.age_certification', $rated)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->whereIn('content_type', $showtype)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->where('contents.status', 1)
                                ->where('contents.content_type', '!=', 'tv-show')
                                ->where('contents.parent_id', '0')
                                ->orderBy('contents.cinema_release_date', 'DESC')
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate('28');
                    
                    } elseif ($langs != "" && $genre != "") {
                        
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->leftJoin('content_genres', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_providers.retail_price', $retail)
                                ->where('content_providers.monetization_type', "!=", 'flatrate')
                                ->where('content_providers.provider_id', $provider)
                                ->where('content_genres.genre_id', $genre)
                                ->where('contents.original_language', $langs)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->whereIn('content_type', $showtype)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->where('contents.status', 1)
                                ->where('contents.content_type', '!=', 'tv-show')
                                ->where('contents.parent_id', '0')
                                ->orderBy('contents.cinema_release_date', 'DESC')
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate('28');
                    
                    } elseif ($langs != "") {
                        
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->where('content_providers.retail_price', $retail)
                                ->where('content_providers.monetization_type', "!=", 'flatrate')
                                ->where('content_providers.provider_id', $provider)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->whereIn('content_type', $showtype)
                                ->where('contents.status', 1)
                                ->where('contents.original_language', $langs)
                                ->where('contents.content_type', '!=', 'tv-show')
                                ->where('contents.parent_id', '0')
                                ->orderBy('contents.cinema_release_date', 'DESC')
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate('28');
                    } elseif ($genre != "") {
                        
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->leftJoin('content_genres', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_providers.retail_price', $retail)
                                ->where('content_providers.monetization_type', "!=", 'flatrate')
                                ->where('content_providers.provider_id', $provider)
                                ->where('content_genres.genre_id', $genre)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->whereIn('content_type', $showtype)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->where('contents.status', 1)
                                ->where('contents.content_type', '!=', 'tv-show')
                                ->where('contents.parent_id', '0')
                                ->orderBy('contents.cinema_release_date', 'DESC')
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate('28');
                        
                    } elseif ($rated != "") {
                        
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->where('content_providers.retail_price', $retail)
                                ->where('content_providers.monetization_type', "!=", 'flatrate')
                                ->where('content_providers.provider_id', $provider)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->whereIn('content_type', $showtype)
                                ->where('contents.age_certification', $rated)
                                ->where('contents.status', 1)
                                ->where('contents.content_type', '!=', 'tv-show')
                                ->where('contents.parent_id', '0')
                                ->orderBy('contents.cinema_release_date', 'DESC')
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate('28');
                        
                    } else {
                        
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->where('content_providers.retail_price', $retail)
                                ->where('content_providers.monetization_type', "!=", 'flatrate')
                                ->where('content_providers.provider_id', $provider)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                // ->where('contents.cinema_release_date','<=',$maxy)
                                ->whereIn('content_type', $showtype)
                                // ->where('contents.cinema_release_date','>=',$miny)
                                ->where('contents.content_type', '!=', 'tv-show')
                                ->where('contents.parent_id', '0')
                                ->orderBy('contents.cinema_release_date', 'DESC')
                                ->where('contents.status', 1)
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate('28');
                        
                    }
                } else {

                    if ($langs != "" && $rated != "" && $genre != "") {
                        
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->leftJoin('content_genres', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_providers.retail_price', $retail)
                                ->where('content_providers.monetization_type', "!=", 'flatrate')
                                ->where('content_providers.provider_id', $provider)
                                ->where('content_genres.genre_id', $genre)
                                ->where('contents.age_certification', $rated)
                                ->where('contents.original_language', $langs)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->whereIn('content_type', $showtype)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->where('contents.content_type', '!=', 'tv-show')
                                ->where('contents.status', 1)
                                ->where('contents.parent_id', '0')
                                ->orderBy($field, $sort_type)
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate('28');
                    
                    } elseif ($langs != "" && $rated != "") {
                        
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->where('content_providers.retail_price', $retail)
                                ->where('content_providers.monetization_type', "!=", 'flatrate')
                                ->where('content_providers.provider_id', $provider)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->where('contents.age_certification', $rated)
                                ->whereIn('content_type', $showtype)
                                ->where('contents.original_language', $langs)
                                ->where('contents.content_type', '!=', 'tv-show')
                                ->where('contents.status', 1)
                                ->where('contents.parent_id', '0')
                                ->orderBy($field, $sort_type)
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate('28');
                    
                    } elseif ($rated != "" && $genre != "") {
                        
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->leftJoin('content_genres', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_providers.retail_price', $retail)
                                ->where('content_providers.monetization_type', "!=", 'flatrate')
                                ->where('content_providers.provider_id', $provider)
                                ->where('content_genres.genre_id', $genre)
                                ->where('contents.age_certification', $rated)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->whereIn('content_type', $showtype)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->where('contents.content_type', '!=', 'tv-show')
                                ->where('contents.status', 1)
                                ->where('contents.parent_id', '0')
                                ->orderBy($field, $sort_type)
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate('28');
                    
                    } elseif ($langs != "" && $genre != "") {
                        
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->leftJoin('content_genres', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_providers.retail_price', $retail)
                                ->where('content_providers.monetization_type', "!=", 'flatrate')
                                ->where('content_providers.provider_id', $provider)
                                ->where('content_genres.genre_id', $genre)
                                ->where('contents.original_language', $langs)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->whereIn('content_type', $showtype)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->where('contents.content_type', '!=', 'tv-show')
                                ->where('contents.parent_id', '0')
                                ->where('contents.status', 1)
                                ->orderBy($field, $sort_type)
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate('28');
                    
                    } elseif ($langs != "") {
                       
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->where('content_providers.retail_price', $retail)
                                ->where('content_providers.monetization_type', "!=", 'flatrate')
                                ->where('content_providers.provider_id', $provider)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->whereIn('content_type', $showtype)
                                ->where('contents.original_language', $langs)
                                ->where('contents.content_type', '!=', 'tv-show')
                                ->where('contents.parent_id', '0')
                                ->where('contents.status', 1)
                                ->orderBy($field, $sort_type)
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate('28');
                        
                    } elseif ($genre != "") {
                        
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->leftJoin('content_genres', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_providers.retail_price', $retail)
                                ->where('content_providers.monetization_type', "!=", 'flatrate')
                                ->where('content_providers.provider_id', $provider)
                                ->where('content_genres.genre_id', $genre)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->whereIn('content_type', $showtype)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->where('contents.content_type', '!=', 'tv-show')
                                ->where('contents.parent_id', '0')
                                ->where('contents.status', 1)
                                ->orderBy($field, $sort_type)
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate('28');
                    
                    } elseif ($rated != "") {
                        
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->where('content_providers.retail_price', $retail)
                                ->where('content_providers.monetization_type', "!=", 'flatrate')
                                ->where('content_providers.provider_id', $provider)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->whereIn('content_type', $showtype)
                                ->where('contents.age_certification', $rated)
                                ->where('contents.content_type', '!=', 'tv-show')
                                ->where('contents.parent_id', '0')
                                ->where('contents.status', 1)
                                ->orderBy($field, $sort_type)
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate('28');
                    
                    } else {
                        
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->where('content_providers.retail_price', $retail)
                                ->where('content_providers.monetization_type', "!=", 'flatrate')
                                ->where('content_providers.provider_id', $provider)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.status', 1)
                                ->whereIn('content_type', $showtype)
                                // ->where('contents.cinema_release_date','<=',$maxy)
                                // ->where('contents.cinema_release_date','>=',$miny)
                                ->where('contents.content_type', '!=', 'tv-show')
                                ->where('contents.parent_id', '0')
                                ->orderBy($field, $sort_type)
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate('28');
                    
                    }
                }
            }
        } else {

            $filter_type = array();
            if ($type == 'movie') {
                $filter_type = ['movie'];
            } else {
                $filter_type = ['shows', 'tv-show', 'show'];
            }

            if ($provider == "") {

                if ($sort == "") {
                    if ($langs != "" && $rated != "" && $genre != "") {
                        
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->leftJoin('content_genres', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_providers.retail_price', $retail)
                                ->where('content_providers.monetization_type', "!=", 'flatrate')
                                ->where('content_genres.genre_id', $genre)
                                ->where('contents.status', 1)
                                ->whereIn('contents.content_type', $filter_type)
                                ->where('contents.age_certification', $rated)
                                ->where('contents.original_language', $langs)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->whereIn('content_type', $showtype)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->where('contents.parent_id', '0')
                                ->orderBy('contents.cinema_release_date', 'DESC')
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate('28');
                    
                    } elseif ($langs != "" && $rated != "") {
                        
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->where('content_providers.retail_price', $retail)
                                ->where('content_providers.monetization_type', "!=", 'flatrate')
                                ->whereIn('contents.content_type', $filter_type)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->whereIn('content_type', $showtype)
                                ->where('contents.age_certification', $rated)
                                ->where('contents.status', 1)
                                ->where('contents.original_language', $langs)
                                ->where('contents.parent_id', '0')
                                ->orderBy('contents.cinema_release_date', 'DESC')
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate('28');
                        
                    } elseif ($rated != "" && $genre != "") {
                        
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->leftJoin('content_genres', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_providers.retail_price', $retail)
                                ->where('content_providers.monetization_type', "!=", 'flatrate')
                                ->whereIn('contents.content_type', $filter_type)
                                ->where('content_genres.genre_id', $genre)
                                ->where('contents.age_certification', $rated)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->whereIn('content_type', $showtype)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->where('contents.status', 1)
                                ->where('contents.parent_id', '0')
                                ->orderBy('contents.cinema_release_date', 'DESC')
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate('28');
                        
                    } elseif ($langs != "" && $genre != "") {
                        
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->leftJoin('content_genres', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_providers.retail_price', $retail)
                                ->where('content_providers.monetization_type', "!=", 'flatrate')
                                ->whereIn('contents.content_type', $filter_type)
                                ->where('content_genres.genre_id', $genre)
                                ->where('contents.original_language', $langs)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->whereIn('content_type', $showtype)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->where('contents.status', 1)
                                ->where('contents.parent_id', '0')
                                ->orderBy('contents.cinema_release_date', 'DESC')
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate('28');
                        
                    } elseif ($langs != "") {
                        
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->where('content_providers.retail_price', $retail)
                                ->where('content_providers.monetization_type', "!=", 'flatrate')
                                ->whereIn('contents.content_type', $filter_type)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->whereIn('content_type', $showtype)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->where('contents.original_language', $langs)
                                ->where('contents.status', 1)
                                ->where('contents.parent_id', '0')
                                ->orderBy('contents.cinema_release_date', 'DESC')
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate('28');
                    } elseif ($genre != "") {
                        
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->leftJoin('content_genres', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_providers.retail_price', $retail)
                                ->where('content_providers.monetization_type', "!=", 'flatrate')
                                ->whereIn('contents.content_type', $filter_type)
                                ->where('content_genres.genre_id', $genre)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->whereIn('content_type', $showtype)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->where('contents.status', 1)
                                ->where('contents.parent_id', '0')
                                ->orderBy('contents.cinema_release_date', 'DESC')
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate('28');
                
                    } elseif ($rated != "") {
                        
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->where('content_providers.retail_price', $retail)
                                ->where('content_providers.monetization_type', "!=", 'flatrate')
                                ->whereIn('contents.content_type', $filter_type)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->whereIn('content_type', $showtype)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->where('contents.age_certification', $rated)
                                ->where('contents.status', 1)
                                ->where('contents.parent_id', '0')
                                ->orderBy('contents.cinema_release_date', 'DESC')
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate('28');
                    
                    } else {
                        
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->where('content_providers.retail_price', $retail)
                                ->where('content_providers.monetization_type', "!=", 'flatrate')
                                ->whereIn('contents.content_type', $filter_type)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->whereIn('content_type', $showtype)
                                ->where('contents.status', 1)
                                // ->where('contents.cinema_release_date','<=',$maxy)
                                // ->where('contents.cinema_release_date','>=',$miny)
                                ->where('contents.parent_id', '0')
                                ->orderBy('contents.cinema_release_date', 'DESC')
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate('28');
                    
                    }
                } else {

                    if ($langs != "" && $rated != "" && $genre != "") {
                        
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->leftJoin('content_genres', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_providers.retail_price', $retail)
                                ->where('content_providers.monetization_type', "!=", 'flatrate')
                                ->whereIn('contents.content_type', $filter_type)
                                ->where('content_genres.genre_id', $genre)
                                ->where('contents.age_certification', $rated)
                                ->where('contents.original_language', $langs)
                                ->where('contents.status', 1)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->whereIn('content_type', $showtype)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->where('contents.parent_id', '0')
                                ->orderBy($field, $sort_type)
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate('28');
                    
                    } elseif ($langs != "" && $rated != "") {
                        
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->where('content_providers.retail_price', $retail)
                                ->where('content_providers.monetization_type', "!=", 'flatrate')
                                ->whereIn('contents.content_type', $filter_type)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->whereIn('content_type', $showtype)
                                ->where('contents.status', 1)
                                ->where('contents.age_certification', $rated)
                                ->where('contents.original_language', $langs)
                                ->where('contents.parent_id', '0')
                                ->orderBy($field, $sort_type)
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate('28');
                
                    } elseif ($rated != "" && $genre != "") {
                        
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->leftJoin('content_genres', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_providers.retail_price', $retail)
                                ->where('content_providers.monetization_type', "!=", 'flatrate')
                                ->whereIn('contents.content_type', $filter_type)
                                ->where('content_genres.genre_id', $genre)
                                ->where('contents.age_certification', $rated)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->whereIn('content_type', $showtype)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.status', 1)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->where('contents.parent_id', '0')
                                ->orderBy($field, $sort_type)
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate('28');
                    
                    } elseif ($langs != "" && $genre != "") {
                        
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->leftJoin('content_genres', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_providers.retail_price', $retail)
                                ->where('content_providers.monetization_type', "!=", 'flatrate')
                                ->where('content_genres.genre_id', $genre)
                                ->whereIn('contents.content_type', $filter_type)
                                ->where('contents.original_language', $langs)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.status', 1)
                                ->whereIn('content_type', $showtype)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->where('contents.parent_id', '0')
                                ->orderBy($field, $sort_type)
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate('28');
                    
                    } elseif ($langs != "") {
                        
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->where('content_providers.retail_price', $retail)
                                ->where('content_providers.monetization_type', "!=", 'flatrate')
                                ->whereIn('contents.content_type', $filter_type)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->whereIn('content_type', $showtype)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->where('contents.original_language', $langs)
                                ->where('contents.status', 1)
                                ->where('contents.parent_id', '0')
                                ->orderBy($field, $sort_type)
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate('28');
                    
                    } elseif ($genre != "") {
                        
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->leftJoin('content_genres', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_providers.retail_price', $retail)
                                ->where('content_providers.monetization_type', "!=", 'flatrate')
                                ->whereIn('contents.content_type', $filter_type)
                                ->where('content_genres.genre_id', $genre)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->whereIn('content_type', $showtype)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.status', 1)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->where('contents.parent_id', '0')
                                ->orderBy($field, $sort_type)
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate('28');
                    
                    } elseif ($rated != "") {
                        
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->where('content_providers.retail_price', $retail)
                                ->where('content_providers.monetization_type', "!=", 'flatrate')
                                ->whereIn('contents.content_type', $filter_type)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.status', 1)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->whereIn('content_type', $showtype)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->where('contents.age_certification', $rated)
                                ->where('contents.parent_id', '0')
                                ->orderBy($field, $sort_type)
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate('28');
                        
                    } else {
                        
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->where('content_providers.retail_price', $retail)
                                ->where('content_providers.monetization_type', "!=", 'flatrate')
                                ->whereIn('contents.content_type', $filter_type)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->whereIn('content_type', $showtype)
                                ->where('contents.tmdb_score', '>=', $minr)
                                // ->where('contents.cinema_release_date','<=',$maxy)
                                // ->where('contents.cinema_release_date','>=',$miny)
                                ->where('contents.parent_id', '0')
                                ->orderBy($field, $sort_type)
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate('28');
                        
                    }
                }
            } else {
                if ($sort == "") {
                    if ($langs != "" && $rated != "" && $genre != "") {
                        
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->leftJoin('content_genres', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_providers.retail_price', $retail)
                                ->where('content_providers.monetization_type', "!=", 'flatrate')
                                ->where('contents.status', 1)
                                ->where('content_providers.provider_id', $provider)
                                ->where('content_genres.genre_id', $genre)
                                ->whereIn('contents.content_type', $filter_type)
                                ->where('contents.age_certification', $rated)
                                ->where('contents.original_language', $langs)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->whereIn('content_type', $showtype)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->where('contents.parent_id', '0')
                                ->orderBy('contents.cinema_release_date', 'DESC')
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate('28');
                        
                    } elseif ($langs != "" && $rated != "") {
                        
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->where('content_providers.retail_price', $retail)
                                ->where('content_providers.monetization_type', "!=", 'flatrate')
                                ->where('content_providers.provider_id', $provider)
                                ->whereIn('contents.content_type', $filter_type)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->whereIn('content_type', $showtype)
                                ->where('contents.status', 1)
                                ->where('contents.age_certification', $rated)
                                ->where('contents.original_language', $langs)
                                ->where('contents.parent_id', '0')
                                ->orderBy('contents.cinema_release_date', 'DESC')
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate('28');
                    
                    } elseif ($rated != "" && $genre != "") {
                        
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->leftJoin('content_genres', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_providers.retail_price', $retail)
                                ->where('content_providers.monetization_type', "!=", 'flatrate')
                                ->where('content_providers.provider_id', $provider)
                                ->whereIn('contents.content_type', $filter_type)
                                ->where('content_genres.genre_id', $genre)
                                ->where('contents.age_certification', $rated)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->whereIn('content_type', $showtype)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->where('contents.status', 1)
                                ->where('contents.parent_id', '0')
                                ->orderBy('contents.cinema_release_date', 'DESC')
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate('28');
                        
                    } elseif ($langs != "" && $genre != "") {
                        
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->leftJoin('content_genres', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_providers.retail_price', $retail)
                                ->where('content_providers.monetization_type', "!=", 'flatrate')
                                ->where('content_providers.provider_id', $provider)
                                ->whereIn('contents.content_type', $filter_type)
                                ->where('content_genres.genre_id', $genre)
                                ->where('contents.original_language', $langs)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->whereIn('content_type', $showtype)
                                ->where('contents.status', 1)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->where('contents.parent_id', '0')
                                ->orderBy('contents.cinema_release_date', 'DESC')
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate('28');
                    
                    } elseif ($langs != "") {
                        
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->where('content_providers.retail_price', $retail)
                                ->where('content_providers.monetization_type', "!=", 'flatrate')
                                ->where('content_providers.provider_id', $provider)
                                ->whereIn('contents.content_type', $filter_type)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->whereIn('content_type', $showtype)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->where('contents.status', 1)
                                ->where('contents.parent_id', '0')
                                ->where('contents.original_language', $langs)
                                ->orderBy('contents.cinema_release_date', 'DESC')
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate('28');
                        
                    } elseif ($genre != "") {
                        
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->leftJoin('content_genres', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_providers.retail_price', $retail)
                                ->where('content_providers.monetization_type', "!=", 'flatrate')
                                ->where('content_providers.provider_id', $provider)
                                ->whereIn('contents.content_type', $filter_type)
                                ->where('content_genres.genre_id', $genre)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->whereIn('content_type', $showtype)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->where('contents.parent_id', '0')
                                ->where('contents.status', 1)
                                ->orderBy('contents.cinema_release_date', 'DESC')
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate('28');
                    } elseif ($rated != "") {
                        
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->where('content_providers.retail_price', $retail)
                                ->where('content_providers.monetization_type', "!=", 'flatrate')
                                ->where('content_providers.provider_id', $provider)
                                ->whereIn('contents.content_type', $filter_type)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.status', 1)
                                ->whereIn('content_type', $showtype)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->where('contents.age_certification', $rated)
                                ->where('contents.parent_id', '0')
                                ->orderBy('contents.cinema_release_date', 'DESC')
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate('28');
                    
                    } else {
                        
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->where('content_providers.retail_price', $retail)
                                ->where('content_providers.monetization_type', "!=", 'flatrate')
                                ->whereIn('contents.content_type', $filter_type)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->whereIn('content_type', $showtype)
                                ->where('contents.status', 1)
                                // ->where('contents.cinema_release_date','<=',$maxy)
                                // ->where('contents.cinema_release_date','>=',$miny)
                                ->where('contents.parent_id', '0')
                                ->orderBy('contents.cinema_release_date', 'DESC')
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate('28');
                        
                    }
                } else {

                    if ($langs != "" && $rated != "" && $genre != "") {
                        
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->leftJoin('content_genres', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_providers.retail_price', $retail)
                                ->where('content_providers.monetization_type', "!=", 'flatrate')
                                ->where('content_providers.provider_id', $provider)
                                ->whereIn('contents.content_type', $filter_type)
                                ->where('content_genres.genre_id', $genre)
                                ->where('contents.age_certification', $rated)
                                ->where('contents.original_language', $langs)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->whereIn('content_type', $showtype)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->where('contents.status', 1)
                                ->where('contents.parent_id', '0')
                                ->orderBy($field, $sort_type)
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate('28');
                    
                    } elseif ($langs != "" && $rated != "") {
                        
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->where('content_providers.retail_price', $retail)
                                ->where('content_providers.monetization_type', "!=", 'flatrate')
                                ->where('content_providers.provider_id', $provider)
                                ->whereIn('contents.content_type', $filter_type)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->whereIn('content_type', $showtype)
                                ->where('contents.age_certification', $rated)
                                ->where('contents.status', 1)
                                ->where('contents.original_language', $langs)
                                ->where('contents.parent_id', '0')
                                ->orderBy($field, $sort_type)
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate('28');
                        
                    } elseif ($rated != "" && $genre != "") {
                        
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->leftJoin('content_genres', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_providers.retail_price', $retail)
                                ->where('content_providers.monetization_type', "!=", 'flatrate')
                                ->where('content_providers.provider_id', $provider)
                                ->whereIn('contents.content_type', $filter_type)
                                ->where('content_genres.genre_id', $genre)
                                ->where('contents.age_certification', $rated)
                                ->where('contents.status', 1)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->whereIn('content_type', $showtype)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->where('contents.parent_id', '0')
                                ->orderBy($field, $sort_type)
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate('28');
                        
                    } elseif ($langs != "" && $genre != "") {
                        
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->leftJoin('content_genres', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_providers.retail_price', $retail)
                                ->where('content_providers.monetization_type', "!=", 'flatrate')
                                ->where('content_providers.provider_id', $provider)
                                ->where('content_genres.genre_id', $genre)
                                ->whereIn('contents.content_type', $filter_type)
                                ->where('contents.original_language', $langs)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->whereIn('content_type', $showtype)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.status', 1)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->where('contents.parent_id', '0')
                                ->orderBy($field, $sort_type)
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate('28');
                        
                    } elseif ($langs != "") {
                        
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->where('content_providers.retail_price', $retail)
                                ->where('content_providers.monetization_type', "!=", 'flatrate')
                                ->where('content_providers.provider_id', $provider)
                                ->whereIn('contents.content_type', $filter_type)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->whereIn('content_type', $showtype)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->where('contents.status', 1)
                                ->where('contents.original_language', $langs)
                                ->where('contents.parent_id', '0')
                                ->orderBy($field, $sort_type)
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate('28');
                    
                    } elseif ($genre != "") {
                        
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->leftJoin('content_genres', 'content_genres.content_id', 'contents.content_id')
                                ->where('content_providers.retail_price', $retail)
                                ->where('content_providers.monetization_type', "!=", 'flatrate')
                                ->where('content_providers.provider_id', $provider)
                                ->whereIn('contents.content_type', $filter_type)
                                ->where('content_genres.genre_id', $genre)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->whereIn('content_type', $showtype)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->where('contents.status', 1)
                                ->where('contents.parent_id', '0')
                                ->orderBy($field, $sort_type)
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate('28');
                        
                    } elseif ($rated != "") {
                       
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->where('content_providers.retail_price', $retail)
                                ->where('content_providers.monetization_type', "!=", 'flatrate')
                                ->where('content_providers.provider_id', $provider)
                                ->whereIn('contents.content_type', $filter_type)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->where('contents.cinema_release_date', '<=', $maxy)
                                ->whereIn('content_type', $showtype)
                                ->where('contents.cinema_release_date', '>=', $miny)
                                ->where('contents.status', 1)
                                ->where('contents.age_certification', $rated)
                                ->where('contents.parent_id', '0')
                                ->orderBy($field, $sort_type)
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate('28');
                        
                    } else {
                        
                            $contents = DB::table('content_providers')
                                ->leftJoin('contents', 'contents.content_id', 'content_providers.content_id')
                                ->where('content_providers.retail_price', $retail)
                                ->where('content_providers.monetization_type', "!=", 'flatrate')
                                ->where('content_providers.provider_id', $provider)
                                ->whereIn('contents.content_type', $filter_type)
                                ->where('contents.tmdb_score', '<=', $maxr)
                                ->where('contents.tmdb_score', '>=', $minr)
                                ->whereIn('content_type', $showtype)
                                ->where('contents.status', 1)
                                // ->where('contents.cinema_release_date','<=',$maxy)
                                // ->where('contents.cinema_release_date','>=',$miny)
                                ->where('contents.parent_id', '0')
                                ->orderBy($field, $sort_type)
                                ->select('contents.*')
                                ->distinct('contents.content_id')
                                ->paginate('28');
                     
                    }
                }
            }
        }

        


        if ($miny == '') {
            $miny = '0000';
        } else {
            $miny = explode('-', $miny)[0];
        }
        $maxy = explode('-', $maxy)[0];



        if (isset($_GET['provider'])) {
            $provider = $_GET['provider'];
        }
        $contents = $contents->appends(\Input::except('page'));
        foreach ($contents as $key => $value) {
            $this->resizeImage($value->poster);
        }
        return view('khoj.home', compact('contents', 'page', 'type', 'miny', 'maxy', 'langs', 'genre', 'rated', 'minr', 'maxr', 'sort', 'tab', 'provider'));
    }



    public function submitNewsletter(Request $request)
    {
        $check = Newsletter::where('email', $request->email)->first();
        $message = 'Something Went Wrong';
        if (!empty($check)) {
            $message = 'This email is already registered.';
        } else {
            $news = new Newsletter;
            $news->email = $request->email;
            $news->save();
            $message = 'Thank You. We will reply you soon.';
        }
        return response()->json(['message' => $message]);
    }

    

    

    

    //For Single Movie

    function getMovie(Request $request, $content, $id)
    {
        $type = 'movie';
        // return 0;
        $type = $content;
        $name = explode('-', $id);
        $id = $name[count($name) - 1];
        $related_movies = new StdClass;
        $providers = new StdClass;
        $title = "";
        $movie = Content::where('content_id', $id)->first();

        if ($movie) {
            if ($movie->content_source == 'justwatch'){
                
                	$justwatchcheck = $this->verifyfromjustwatch($movie);
            }
            elseif ($movie->content_source == 'mxplayer'){
            	//$id = $justwatchcheck = $this->verifyfrommxplayer($movie);
            	$movie = Content::where('content_id',$id)->first();
            }

            // elseif ($movie->content_source == 'ullu'){
            // 	$justwatchcheck = $this->verifyfromullu($movie);
            // }

            // elseif ($movie->content_source == 'tmdb'){
            // 	//$justwatchcheck = $this->verifyfromtmdb($movie);
            // }
            // =================================justwatch==============================

            if ($movie && isset($movie->title)) {
                $title  = $movie->title;
            } elseif (isset($movie->original_title)) {
                $title = $movie->original_title;
            }
        }
        $casts = "";
        $genres = "";
        $banners = "";
        $clips = "";

        $streams = "";
        $buys = "";
        $seasons = "";
        $episodes = "";
        $season_one = "";
        $episodestotal = 0;


        $getGenres = DB::table('content_genres')->where('content_id', $id)->get();
        $genre_ids = array();
        if (count($getGenres) > 0) {
            foreach ($getGenres as $gen) {
                $genre_ids[] = $gen->genre_id;
            }
        }
        // var_dump($genre_ids); die;


        // var_dump($related_movies); die;

        // $related_movies = DB::table('contents')
        // ->where('title','LIKE','%'.$title.'%')
        // ->where('content_id','!=',$id)
        // ->where('content_type','movie')
        // ->limit(20)
        // ->get();

        $movie = Content::where('content_id', $id)->first();
        $season_id = 0;

        if ($movie) {
            if ($movie->content_type == 'tv-season') {
                $casts = DB::table('content_casting')
                    ->where('content_id', $movie->parent_id)
                    ->distinct('name')
                    ->orderBy('priority', 'ASC')
                    ->get();
                $parent = DB::table('contents')->where('content_id', $movie->parent_id)->first();
                if (isset($parent->short_description))
                    $movie->short_description = $parent->short_description;
                else
                    $movie->short_description = "";
            } else {

                $casts = DB::table('content_casting')
                    ->where('content_id', $id)
                    ->orderBy('role', 'asc')
                    ->orderBy(DB::raw('CAST(IFNULL(priority,99) AS SIGNED)'))
                    ->distinct('name')
                    ->get();
            }
            $genres = DB::table('content_genres')
                ->leftJoin('genres', 'genres.genre_id', 'content_genres.genre_id')
                ->where('content_genres.content_id', $id)
                ->select('genres.genre_id', 'genres.title')
                ->distinct('genre_id')
                ->get();

            $cast_ids = array();
            if (count($casts) > 0) {
                $castcount = 0;
                foreach ($casts as $gen) {
                    if ($castcount> 3){
                        break;
                    }
                    else
                        $castcount++;
                    // if ($gen->role == 'ACTOR')
                        $cast_ids[] = $gen->name;
                }
            }
            // dd($cast_ids);
            $related_movies = DB::table('content_genres')->join('contents', 'contents.content_id', 'content_genres.content_id')->join('content_casting', 'contents.content_id', 'content_casting.content_id')->whereIn('content_genres.genre_id', $genre_ids)->whereIn('content_casting.name', $cast_ids)->where('contents.content_id', '!=', $id)->where('contents.original_language',  $movie->original_language)->where('contents.content_type', $movie->content_type)->orderBy('contents.original_release_year', 'DESC')->orderBy('contents.original_release_year', 'DESC')->select('contents.*')->distinct('contents.content_id')->limit(20)->get();

        
        // die();
            
            $relatedquery = "CALL ret_content_like($id)";
            // $related_movies = DB::select($relatedquery);
            if ($movie->content_type == 'tv-season') {
                $clips = DB::table('content_clips')
                    ->where('content_id', $movie->parent_id)
                    ->get();
            } else {
                $clips = DB::table('content_clips')
                    ->where('content_id', $id)
                    ->get();
            }
            $flatrate = array('cinema', 'flatrate', 'free', 'ads');
            $streams = DB::table('content_providers')
                ->where('content_id', $id)
                ->where('isLive', 1)
                ->whereIn('monetization_type', $flatrate)
                ->select('content_id', 'provider_id', 'video_quality', 'monetization_type', 'retail_price', 'currency', 'web_url')
                ->orderBy('provider_id', 'DESC')
                ->distinct('provider_id')
                ->distinct('content_id')
                ->get();

            // $providers->stream = $streams;
            $buy = array('cinema', 'buy', 'rent');

            $buys = DB::table('content_providers')
                ->where('content_id', $id)
                ->where('isLive', 1)
                ->select('content_id', 'provider_id', 'video_quality', 'monetization_type', 'retail_price', 'currency', 'web_url')
                ->where('retail_price', '>', 0)
                ->whereIn('monetization_type', $buy)
                ->orderBy('provider_id', 'DESC')
                ->distinct('provider_id')
                ->distinct('content_id')
                ->get();
            $providers->stream = $buys;

            // var_dump($providers); die;


            $count = $movie->click_count + 1;
            $change_movie = DB::table('contents')
                ->where('content_id', $id)
                ->update(array(
                    'click_count' => $count,
                ));

            $episodes = array();
            $season_id = 0;
            $season_one = "";
            $seasons = DB::table('contents')->where('parent_id', $movie->content_id)->where('content_type', 'tv-season')->orderBy('cinema_release_date', 'DESC')->orderBy('content_id', 'DESC')->get();
            // dd($seasons);
            if (!empty($seasons) && !empty($seasons[0])) {
                $season_id = $seasons[0]->content_id ?? '';
                $season_one = $seasons[0]->title ?? '';
            } else {
                $season_id = $movie->content_id;
                $season_one = $movie->title ?? '';
            }
            $banners = DB::table('content_banners')->where('content_id', $movie->content_id)->get();
        }




        if ($season_id > 0) {
            $episodes = DB::table('contents')->where('parent_id', $season_id)->where('content_type', 'tv-episode')->orderBy('cinema_release_date', 'DESC')->orderBy('content_id', 'DESC')->limit(5)->get();
            $episodestotal = DB::table('contents')->where('parent_id', $season_id)->where('content_type', 'tv-episode')->orderBy('cinema_release_date', 'DESC')->orderBy('content_id', 'DESC')->count();
        }


        if (isset($movie->content_type))
            $type = $movie->content_type;
        else
            $type = "";
        return view('khoj.movie-detail', compact('movie', 'casts', 'genres', 'banners', 'clips', 'providers', 'related_movies', 'type', 'seasons', 'episodes', 'season_one', 'banners', 'streams', 'buys', 'episodestotal'));
    }

    public function verifyfrommxplayer($movie)
    {
        $id = $movie->content_id;

        $checkoriginal = Content::where('title', $movie->title)
            ->where('content_type', $movie->content_type)
            ->where('original_release_year', $movie->original_release_year)
            ->where('content_source', '!=', 'mxplayer')
            ->first();
        if ($checkoriginal && ($checkoriginal->content_source == 'justwatch' || $checkoriginal->content_source == 'ullu')) {
            // dd($checkoriginal);

            if ($movie->full_path != "") {

                $web_url = "http://mxplayer.in" . $movie->full_path;
            } else
                $web_url = "";
            $mxplayercheck = DB::table('content_providers')->where('content_id', $checkoriginal->content_id)->where('provider_id', 700)->first();
            if (!$mxplayercheck)
                $db_data = DB::table('content_providers')->where('content_id', $id)->where('provider_id', 700)->update(
                    array(
                        'content_id' => $checkoriginal->content_id,
                        'monetization_type' => 'free',
                        'web_url' => $web_url,
                        'video_quality' => 'sd',
                        'provider_id' => 700,
                        'isLive' => 1
                    )
                );
            else
                // $db_data = DB::table('content_providers')->where('content_id', $id)->delete();
                $check3 = DB::table('content_providers')->where('content_id', $checkoriginal->content_id)->get();
            $url = "detail/movie/" . $checkoriginal->title . "-" . $checkoriginal->content_id;
            $delete = DB::table('contents')->where('content_id', $id)->delete();
            return $checkoriginal->content_id;
        } else {
            if ($movie->content_type == 'show') {
                $localtype = 'tvshow';
                $utype = 'collection';
            } else {
                $utype = 'video';
                $localtype = $movie->content_type;
            }

            $url = "https://api.mxplay.com/v1/web/detail/" . $utype . "?type=" . $localtype . "&id=" . $movie->content_source_id . "&platform=com.mxplay.desktop&content-languages=hi,en";
            $c = curl_init();
            curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($c, CURLOPT_URL, $url);
            curl_setopt($c, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($c, CURLOPT_SSL_VERIFYPEER, 0);
            $mxplayer = curl_exec($c);

            $mxplayer = json_decode($mxplayer);
            // dd($mxplayer);

            // dd($data);
            $newdata = array();

            if (isset($mxplayer->description) && $movie->short_description != $mxplayer->description) {
                $newdata['short_description'] = $mxplayer->description;
            }

            if (isset($mxplayer->releaseYear) && $movie->original_release_year != $mxplayer->releaseYear) {
                $newdata['original_release_year'] = $mxplayer->releaseYear;
            }

            if (isset($mxplayer->releaseDate)) {
                $newdata['localized_release_date'] = explode('T', $mxplayer->releaseDate)[0];
                $newdata['cinema_release_date'] = explode('T', $mxplayer->releaseDate)[0];
            }

            if (isset($mxplayer->duration)) {
                $newdata['runtime'] = $mxplayer->duration;
            }

            if (isset($mxplayer->languages)) {
                $newdata['original_language'] = implode(',', $mxplayer->languages);
            } else {
                $newdata['original_language'] = 'hi';
            }


            // dd($mxplayer);
            $newmovie = DB::table('contents')->where('content_id', $id)->update($newdata);
            if ($movie->full_path != "") {
                $web_url = "http://mxplayer.in" . $mxplayer->webUrl;
            } else
                $web_url = "";
            $db_data = DB::table('content_providers')->where('content_id', $movie->content_id)->where('provider_id', 700)->update(
                array(
                    'content_id' => $movie->content_id,
                    'monetization_type' => 'free',
                    'video_quality' => 'sd',
                    'web_url' => $web_url,
                    'isLive' => 1
                )
            );

            // ==================================cast section =========================


            if ($mxplayer->contributors) {
                $casts = $mxplayer->contributors;
                foreach ($casts as $key => $value) {
                    $castingcheck = ContentCasting::where('content_id', $id)->where('name', $value->name)->first();
                    if (!$castingcheck) {
                        $casting = array();
                        $casting['role'] = $value->type;
                        $casting['person_id'] = $value->id;
                        $casting['content_id'] = $id;
                        $casting['name'] = $value->name;
                        if (!empty($casting)) {
                            $addcast = ContentCasting::insert($casting);
                        }
                    }
                }
            }
            return $id;
        }
    }
    

    public function getSearch(Request $request)
    {
        $search = $request->search;
        $request->data = $request->search;
        $keyword = $request->data;
        $keyword = urlencode($search);
        // var_dump($keyword); die;
        // escapecha
        // $this->getmxplayerdata($request);
        // $this->searchjustwatch($request);
        // $this->getulludata($request);



        $page = 0;
        $langs = 'ALL';
        $rated = "ALL";
        $sort = "";
        $field = "title";
        $sort_type = "ASC";
        $genre = 0;
        $minr = 0;
        $maxr = 10;

        $tab = 'search';
        $provider_id = 0;
        if (isset($request->provider)) {
            $provider_id = explode('_', $request->provider)[1];
        }

        $miny = '0000';
        // $maxy = DB::table('contents')->orderBy('cinema_release_date','DESC')->first();

        $maxy = date('Y');
        // $miny = DB::table('contents')->where('original_release_year','>','1900')->min('original_release_year');
        // $maxy = DB::table('contents')->max('original_release_year');

        if (isset($_GET['page'])) {
            $page = ($_GET['page'] - 1) * 28;
        }

        $keyword = $request->search;
        if ($request->searchtype)
            $type = $request->searchtype;
        else
            $type = 'ALL';
        $searchtype = $type;

        // echo $keyword; die;
        if ($keyword != "") {
            // echo 'Same'; die;
            $findKeyword = SearchKeyword::where('keyword', $keyword)->first();

            if (empty($findKeyword)) {
                $search_keyword = new  SearchKeyword;
                if (isset(auth()->user()->id)) {
                    $search_keyword->user_id = auth()->user()->id;
                }
                $search_keyword->keyword = $keyword;
                $search_keyword->save();
            }
        } else {
            // echo 'Home'; die;
            return redirect('/');
        }

        if (isset($_GET['lang'])) {
            $langs = $_GET['lang'];
        }

        if (isset($_GET['sort'])) {
            $sort = $_GET['sort'];
            if ($sort == "alpha") {
                $field = 'contents.title';
                $sort_type = 'ASC';
            } elseif ($sort == 'rate') {
                $field = 'contents.tmdb_score';
                $sort_type = 'DESC';
            } elseif ($sort == 'year') {
                $field = 'contents.cinema_release_date';
                $sort_type = 'DESC';
            } elseif ($sort == 'imdb_score') {
                $field = 'contents.imdb_score';
                $sort_type = 'DESC';
            } else {
                $field = 'contents.title';
                $sort_type = 'ASC';
            }
        }

        if (isset($_GET['sort']) && isset($_GET['genre'])) {
            $sort = $_GET['sort'];
            if ($sort == "alpha") {
                $field = 'contents.title';
                $sort_type = 'ASC';
            } elseif ($sort == 'rate') {
                $field = 'contents.tmdb_score';
                $sort_type = 'DESC';
            } elseif ($sort == 'year') {
                $field = 'contents.cinema_release_date';
                $sort_type = 'DESC';
            } elseif ($sort == 'imdb_score') {
                $field = 'contents.imdb_score';
                $sort_type = 'DESC';
            } else {
                $field = 'contents.title';
                $sort_type = 'ASC';
            }
        }

        if (isset($_GET['rated'])) {
            $rated = $_GET['rated'];
        }

        if (isset($_GET['genre'])) {
            $genre = $_GET['genre'];
        }

        if (isset($_GET['minr'])) {
            $minr = $_GET['minr'];
        }
        if (isset($_GET['maxr'])) {
            $maxr = $_GET['maxr'];
        }

        if (isset($_GET['miny'])) {
            $miny = $_GET['miny'];
        }
        if (isset($_GET['maxy'])) {
            $maxy = $_GET['maxy'];
        }

        // var_dump($keyword);
        // var_dump($genre);
        // var_dump($type);
        // var_dump($rated);
        // var_dump($langs);

        // die();
        $sort_type = "relevance";
        if (isset($_GET['sort_by'])) {
            $sort_type = $_GET['sort_by'];
        }

        $orderBy = "DESC";
        if (isset($_GET['order'])) {
            $orderBy = $_GET['order'];
        }
        $keyword = str_replace('"', "'", $keyword);
        $search23 = 'CALL ret_search("' . $keyword . '", ' . $genre . ', "' . $type . '","' . $miny . '", "' . $maxy . '", "' . $rated . '", "' . $langs . '", ' . $page . ', 28, 0, "' . $provider_id . '", "' . $sort_type . '", "' . $orderBy . '")';
        // die();
        $contents = DB::select($search23);
        // dd($contents);



        $miny = explode('-', $miny)[0];
        $maxy = explode('-', $maxy)[0];


        // $contents = $contents->appends(\Input::except('page'));
        foreach ($contents as $key => $value) {
            if (!empty($value->poster) && strpos($value->poster, 'poster/') == false && file_exists(public_path() . '/poster/' . $value->poster)) {
                $this->resizeImage($value->poster);
            }
        }
        return view('khoj.home', compact('contents', 'page', 'keyword', 'type', 'miny', 'maxy', 'searchtype', 'langs', 'genre', 'rated', 'minr', 'maxr', 'tab'));
    }

    //Without Search Keyword

    public function getSearchFilter(Request $request, $type, $two, $three, $four, $five)
    {
        $page = 0;

        if (isset($_GET['page'])) {
            $page = $_GET['page'] - 1;
        }

        $type = explode("_", $type)[1];
        $miny = explode("_", $miny)[1];
        $maxy = explode("_", $maxy)[1];
        $minr = explode("_", $minr)[1];
        $maxr = explode("_", $maxr)[1];
        $tab = "filter";

        if ($type == 'all') {
            $contents = DB::table('contents')
                ->where('original_release_year', '<=', $maxy)
                ->where('original_release_year', '>=', $miny)
                ->where('tmdb_score', '>=', $minr)
                ->where('tmdb_score', '<=', $maxr)
                ->where('parent_id', 0)
                ->orderBy('content_id', 'DESC')
                ->paginate(28);
        } else {
            $contents = DB::table('contents')
                ->where('content_type', $type)
                ->where('original_release_year', '<=', $maxy)
                ->where('original_release_year', '>=', $miny)
                ->where('tmdb_score', '>=', $minr)
                ->where('tmdb_score', '<=', $maxr)
                ->where('parent_id', 0)
                ->orderBy('content_id', 'DESC')
                ->paginate(28);
        }
        $contents = $contents->appends(\Input::except('page'));
        foreach ($contents as $key => $value) {
            $this->resizeImage($value->poster);
        }
        return view('khoj.home', compact('contents', 'type', 'page', 'miny', 'maxy', 'tab'));
    }

    public function getulludata(Request $request)
    {
        $keyword = $request->data;
        $keyword = str_replace(' ', '%20', $keyword);
        $keyword = str_replace('"', "'", $keyword);
        $data = file_get_contents("https://ullu.app/ulluCore/api/v1/media/v2/client/search?titleText=" . $keyword . "&page=0&size=40");
        $data = json_decode($data);
        foreach ($data->content as $key => $value) {
            $dataadd = $this->addulludata($value, 'show');
        }
    }


    public function addulludata($movies, $contenttype)
    {
        if (isset($movies->portraitPosterId)) {

            $url1 = 'https://d1dfc9w6nzu9oi.cloudfront.net/' . $movies->portraitPosterId;
            $url2 = 'https://d1dfc9w6nzu9oi.cloudfront.net/' . $movies->landscapePosterId;
            $base_path = public_path();
            $keyname = $movies->portraitPosterId;
            $img2 = $base_path . '/thumbnail/' . $movies->portraitPosterId;
            $img1 = $base_path . '/poster/' . $movies->portraitPosterId;
            $updateData['poster'] = $keyname;
            // Save image
            file_put_contents($img1, file_get_contents($url1));
            file_put_contents($img2, file_get_contents($url2));

            // }
        }
        $dbcheck = DB::table('contents')->where('content_source', 'ullu')->where('content_source_id', $movies->id)->first();
        if (!$dbcheck) {
            $db_data1 = DB::table('contents')->insert(
                array(
                    'title' => $movies->title,
                    'original_title' => $movies->title,
                    'content_type' => $contenttype,
                    'poster' => $updateData['poster'],
                    'content_source' => 'ullu',
                    'content_source_id' => $movies->id,
                    'full_path' => $movies->titleYearSlug ?? "",
                    'tmdb_popularity' => 0,
                    'original_release_year' => '',
                    'cinema_release_date' => date('Y-m-d'),
                    'original_language' => "",
                    'tmdb_score' => "",
                    'imdb_score' => '',
                    'parent_id' => 0
                )
            );
            $dbcheck = DB::table('contents')->where('title', $movies->title)->where('content_source', 'ullu')->where('content_type', $contenttype)->where('content_source_id', $movies->id)->first();
            if ($dbcheck)
                $db_data = DB::table('content_providers')->insert(
                    array(
                        'content_id' => $dbcheck->content_id,
                        'provider_id' => 701,
                        'video_quality' => 'SD',
                        'currency' => 'INR',
                        'monetization_type' => 'ads',
                        'retail_price' => '0',
                        'isLive' => '1',
                        'web_url' => ""
                    )
                );
        }
    }
    public function getmxplayerdata(Request $request)
    {
        $keyword = $request->data;
        if ($keyword != '') {
            $keyword = str_replace(' ', '%20', $keyword);
            $keyword = str_replace('"', "'", $keyword);
            $data = file_get_contents("https://api.mxplay.com/v1/web/search/result?query=" . $keyword . "&platform=com.mxplay.desktop&content-languages=hi,en");
            $data = json_decode($data);
            // dd($data);
            foreach ($data->sections as $key => $value) {
                if ($value->id == 'movie') {
                    $dataadd = $this->addmxplayerdata($value, 'movie');
                } elseif ($value->id == 'shows') {
                    $dataadd = $this->addmxplayerdata($value, 'show');
                } else {
                    continue;
                }
            }
        }
        // dd($data);
    }


    public function addmxplayerdata($movies, $contenttype)
    {
        if (count($movies->items) > 0) {
            foreach ($movies->items as $key1 => $value1) {
                // dd($value1);
                $checkoriginal = Content::where('title', $value1->title)
                    ->where('content_type', $contenttype)
                    ->where('content_source', '!=', 'mxplayer')
                    ->first();
                if ($checkoriginal && ($checkoriginal->content_source == 'justwatch' || $checkoriginal->content_source == 'ullu')) {

                    if ($checkoriginal->full_path != "") {
                        if (isset(explode('/in/', $checkoriginal->full_path)[1]))
                            $web_url = "http://mxplayer.in/" . explode('/in/', $checkoriginal->full_path)[1];
                        else
                            $web_url = "http://mxplayer.in/" . $checkoriginal->full_path;
                    } else
                        $web_url = "";
                    $checkprovider =  DB::table('content_providers')->where('content_id', $checkoriginal->content_id)->where('provider_id', 700)->first();
                    if (!$checkprovider)
                        $db_data = DB::table('content_providers')->insert(
                            array(
                                'content_id' => $checkoriginal->content_id,
                                'provider_id' => 700,
                                'video_quality' => 'SD',
                                'currency' => 'INR',
                                'monetization_type' => 'free',
                                'retail_price' => '0',
                                'isLive' => '1',
                                'web_url' => $web_url
                            )
                        );
                } else {
                    // dd($value1);
                    if (isset($value1->image)) {
                        foreach ($value1->image as $key => $image) {
                            if ($key == '2x3') {
                                $image = explode('/', $image);
                                $url1 = 'https://is-1.mxplay.com/media/images/' . $image[3] . '/2x3/2x/' . $image[4];
                                $base_path = public_path();
                                $keyname = time() . $image[4];
                                $img1 = $base_path . '/poster/' . $keyname;
                                $updateData['poster'] = $keyname;
                                // Save image
                                file_put_contents($img1, file_get_contents($url1));
                            }
                        }
                    }
                    $dbcheck = DB::table('contents')->where('title', $value1->title)->where('content_source', 'mxplayer')->where('content_type', $contenttype)->where('content_source_id', $value1->id)->first();
                    $data27 = array(
                        'title' => $value1->title,
                        'original_title' => $value1->title,
                        'content_type' => $contenttype,
                        'poster' => $updateData['poster'],
                        'content_source' => 'mxplayer',
                        'content_source_id' => $value1->id,
                        'full_path' => $value1->webUrl ?? "",
                        'tmdb_popularity' => 0 ?? '',
                        'original_release_year' => explode('-', $value1->releaseDate)[0] ?? '',
                        'cinema_release_date' => $value1->releaseDate ?? '',
                        'original_language' => $value1->language[0] ?? "",
                        'tmdb_score' => $tmdb_score ?? "",
                        'imdb_score' => $imdb_score ?? '',
                        'parent_id' => 0
                    );
                    if (!$dbcheck) {
                        $db_data1 = DB::table('contents')->insert($data27);
                        // dd($data27);
                        $dbcheck = DB::table('contents')->where('title', $value1->title)->where('content_source', 'mxplayer')->where('content_type', $contenttype)->where('content_source_id', $value1->id)->first();
                        if ($dbcheck)
                            $db_data = DB::table('content_providers')->insert(
                                array(
                                    'content_id' => $dbcheck->content_id,
                                    'provider_id' => 700,
                                    'video_quality' => 'SD',
                                    'currency' => 'INR',
                                    'monetization_type' => 'ads',
                                    'retail_price' => '0',
                                    'isLive' => '1',
                                    'web_url' => "http://mxplayer.in/" . $value1->webUrl
                                )
                            );
                    }
                }
                // dd($db_data1);
            }
        }
    }

    public function getSearchV2(Request $request, Content $contents)
    {
        $keyword = $request->data;
        $search = $request->search;
        $request->data = $request->search;
        $this->getmxplayerdata($request);
        // $this->searchjustwatch($request);
        $this->getulludata($request);
        $contents->newQuery();



        $page = 0;
        $langs = "";
        $rated = "";
        $sort = "";
        $field = "title";
        $sort_type = "ASC";
        $genre = "";
        $minr = 0;
        $maxr = 10;

        $tab = 'search';

        $miny = '0000-00-00';
        $maxy = date('Y-m-d');

        // $miny = DB::table('contents')->where('original_release_year','>','1900')->min('original_release_year');
        // $maxy = DB::table('contents')->max('original_release_year');

        if (isset($_GET['page'])) {
            $page = $_GET['page'] - 1;
        }

        $keyword = $request->search;
        $type = $request->searchtype;
        $searchtype = $type;

        $findKeyword = SearchKeyword::where('keyword', $keyword)->first();

        if (empty($findKeyword)) {
            $search_keyword = new  SearchKeyword;
            if (isset(auth()->user()->id)) {
                $search_keyword->user_id = auth()->user()->id;
            }
            $search_keyword->keyword = $keyword;
            $search_keyword->save();
        }

        if (isset($_GET['lang'])) {
            $langs = $_GET['lang'];
        }

        if (isset($_GET['sort'])) {
            $sort = $_GET['sort'];
            if ($sort == "alpha") {
                $field = 'contents.title';
                $sort_type = 'ASC';
            } elseif ($sort == 'rate') {
                $field = 'contents.tmdb_score';
                $sort_type = 'DESC';
            } elseif ($sort == 'year') {
                $field = 'contents.cinema_release_date';
                $sort_type = 'DESC';
            } elseif ($sort == 'imdb_score') {
                $field = 'contents.imdb_score';
                $sort_type = 'DESC';
            } else {
                $field = 'contents.title';
                $sort_type = 'ASC';
            }
        }

        if (isset($_GET['sort']) && isset($_GET['genre'])) {
            $sort = $_GET['sort'];
            if ($sort == "alpha") {
                $field = 'contents.title';
                $sort_type = 'ASC';
            } elseif ($sort == 'rate') {
                $field = 'contents.tmdb_score';
                $sort_type = 'DESC';
            } elseif ($sort == 'year') {
                $field = 'contents.cinema_release_date';
                $sort_type = 'DESC';
            } elseif ($sort == 'imdb_score') {
                $field = 'contents.imdb_score';
                $sort_type = 'DESC';
            } else {
                $field = 'contents.title';
                $sort_type = 'ASC';
            }
        }

        if (isset($_GET['rated'])) {
            $rated = $_GET['rated'];
        }

        if (isset($_GET['genre'])) {
            $genre = $_GET['genre'];
        }

        if (isset($_GET['minr'])) {
            $minr = $_GET['minr'];
        }
        if (isset($_GET['maxr'])) {
            $maxr = $_GET['maxr'];
        }

        if (isset($_GET['miny'])) {
            $miny = $_GET['miny'] . "-01-01";
        }
        if (isset($_GET['maxy'])) {
            $maxy = $_GET['maxy'] . "-12-31";
        }

        $contents->leftJoin('contents', 'content_genres.content_id', 'contents.content_id');
        $contents->leftJoin('content_casting', 'content_casting.content_id', 'contents.content_id');

        if ($type != 'all') {
            $contents->where('contents.content_type', $type);
        }

        if ($sort != "") {
            $contents->orderBy($field, $sort_type);
        }
        if ($langs != "") {
            $contents->where('contents.original_language', $langs);
        }
        if ($rated != "") {
            $contents->where('contents.age_certification', $rated);
        }
        if ($genre != "") {
            $contents->where('content_genres.genre_id', $genre);
        }
        $contents->where('contents.cinema_release_date', '<=', $maxy)
            ->where('contents.cinema_release_date', '>=', $miny)
            ->where('contents.title', 'LIKE', $keyword . '%');
        if ($minr != 0 && $maxr != 10) {
            $contents->where('contents.tmdb_score', '<=', $maxr)
                ->where('contents.tmdb_score', '>=', $minr);
        }
        $contents->orderBy('contents.click_count', 'DESC')
            ->orderBy('contents.imdb_score', 'DESC')
            ->orderBy('contents.original_release_year', 'DESC')
            ->orderBy('contents.title', 'ASC')
            ->select('contents.*')
            ->distinct('contents.content_id')
            ->limit(28)
            ->get();

        // dd($contents);
        $miny = explode('-', $miny)[0];
        $maxy = explode('-', $maxy)[0];


        $contents = $contents->appends(\Input::except('page'));
        foreach ($contents as $key => $value) {
            $this->resizeImage($value->poster);
        }
        return view('khoj.home', compact('contents', 'page', 'keyword', 'type', 'miny', 'maxy', 'searchtype', 'langs', 'genre', 'rated', 'minr', 'maxr', 'tab'));
    }

    public function searchjustwatch($request)
    {
        $keyword = $request->data;
        $keyword = str_replace(' ', '%20', $keyword);
        $keyword = str_replace('"', "'", $keyword);

        $justwatch_url = "https://apis.justwatch.com/content/titles/en_IN/popular?body=%7B%22page_size%22:20,%22page%22:1,%22query%22:%22" . $keyword . "%22,%22content_types%22:[%22show%22,%22movie%22]%7D";

        $c = curl_init();
        curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($c, CURLOPT_URL, $justwatch_url);
        curl_setopt($c, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($c, CURLOPT_SSL_VERIFYPEER, 0);
        $justwatch = curl_exec($c);

        $justwatch = json_decode($justwatch);
        // dd($justwatch);


        if (isset($justwatch->items)) {


            foreach ($justwatch->items as $key => $value) {

                $tmdb_score = "";
                $imdb_score = "";
                $orig_title = " ";
                $title = "";
                $tmdb_popularity = "";
                $object_type = "";
                $cinema_release_date = "";
                $original_release_year = "";
                $original_language = "";

                if (isset($value->scoring)) {
                    foreach ($value->scoring as $score) {
                        if ($score->provider_type == 'tmdb:score')
                            $tmdb_score = $score->value;
                        if ($score->provider_type == 'imdb:score')
                            $imdb_score = $score->value;
                    }
                }

                if (isset($value->original_title))
                    $orig_title = $value->original_title;
                else
                    $orig_title = $value->title;

                if (isset($value->title))
                    $title = $value->title;

                if (isset($value->object_type)) {
                    $object_type = $value->object_type;
                    if ($object_type == 'shows') {
                        $object_type = 'show';
                    }
                }

                if (isset($value->tmdb_popularity))
                    $tmdb_popularity = $value->tmdb_popularity;

                if (isset($value->original_release_year))
                    $original_release_year = $value->original_release_year;

                if (isset($value->cinema_release_date))
                    $cinema_release_date = $value->cinema_release_date;

                if (isset($value->original_language))
                    $original_language = $value->original_language;


                $keyname = "";
                if (isset($value->poster)) {
                    $key1 = explode('/', $value->poster)[2];
                    $url1 = 'https://images.justwatch.com/poster/' . $key1 . '/s592';
                    $url2 = 'https://images.justwatch.com/poster/' . $key1 . '/s166';
                    $base_path = public_path();
                    $keyname = $key1 . ".jpg";
                    $img1 = $base_path . '/poster/' . $keyname;
                    $img2 = $base_path . '/thumbnail/' . $keyname;
                    $newdata['execute_status'] = 1;
                    $newdata['poster'] = $keyname;
                    // echo $movie->content_id." ";
                    try {
                        file_put_contents($img1, file_get_contents($url1));
                    } catch (Exception $ex) {
                        //Process the exception
                    }
                    try {
                        file_put_contents($img2, file_get_contents($url2));
                    } catch (Exception $ex) {
                        //Process the exception
                    }
                }


                $db_content = DB::table('contents')->where('content_type', $value->object_type)->where('content_source_id', $value->id)->first();


                //If Not data available for this movie in contents table
                if (isset($db_content)) {
                    // continue;
                    $content_id = $db_content->content_id;

                    $update_content = DB::table('contents')
                        ->where('content_id', $content_id)
                        ->update(
                            array(
                                'tmdb_score' => $tmdb_score,
                                'imdb_score' => $imdb_score,
                                'title' => $value->title,
                                'poster' => $keyname ?? '',
                                'original_title' => $orig_title,
                                'tmdb_popularity' => $tmdb_popularity,
                                'original_release_year' => $original_release_year,
                                'cinema_release_date' => $cinema_release_date,
                            )
                        );
                } else {


                    $insertdata = array(
                        'title' => $value->title,
                        'original_title' => $orig_title,
                        'content_type' => $object_type,
                        'poster' => $keyname ?? '',
                        'content_source' => 'justwatch',
                        'content_source_id' => $value->id,
                        'full_path' => $value->full_path ?? "",
                        'tmdb_popularity' => $tmdb_popularity,
                        'original_release_year' => $original_release_year,
                        'cinema_release_date' => $cinema_release_date,
                        'original_language' => $original_language,
                        'tmdb_score' => $tmdb_score,
                        'imdb_score' => $imdb_score
                    );


                    $db_data = DB::table('contents')->insert(
                        array(
                            'title' => $value->title,
                            'original_title' => $orig_title,
                            'content_type' => $object_type,
                            'poster' => $keyname ?? '',
                            'content_source' => 'justwatch',
                            'content_source_id' => $value->id,
                            'full_path' => $value->full_path ?? "",
                            'tmdb_popularity' => $tmdb_popularity,
                            'original_release_year' => $original_release_year,
                            'cinema_release_date' => $cinema_release_date,
                            'original_language' => $original_language,
                            'tmdb_score' => $tmdb_score,
                            'imdb_score' => $imdb_score
                        )
                    );

                    if (isset($value->offers)) {
                        $providerlist = $value->offers;
                        foreach ($providerlist as $key2 => $value2) {

                            $dbcheck = DB::table('content_providers')->where('content_id', $value->id)
                                ->where('video_quality', $value2->presentation_type)
                                ->where('provider_id', $value2->provider_id)
                                ->first();
                            if (!$dbcheck) {
                                $db_data = DB::table('content_providers')->insert(
                                    array(
                                        'content_id' => $value->id,
                                        'provider_id' => $value2->provider_id,
                                        'video_quality' => $value2->presentation_type ?? '',
                                        'currency' => $value2->currency ?? '',
                                        'monetization_type' => $value2->monetization_type ?? '',
                                        'retail_price' => $value2->retail_price ?? '',
                                        'isLive' => 1,
                                        'web_url' => $value2->urls->standard_web ?? ""
                                    )
                                );
                            }
                        }
                    }
                }
            }
        }
    }

    //Dynamic Search
    public function getDynamicSearch(Request $request)
    {

        $keyword = $request->data;
        $search = $request->search;
        //$keyword = str_replace(' ', '+', $keyword);
        //$keyword = str_replace('"', ''', $keyword);
        // $search = str_replace(' ', '+', $search);
        // $search = str_replace('"', ''', $search);
        // $this->searchjustwatch($request);
        $content = DB::select('CALL ret_search_help("' . stripcslashes($keyword) . '","ALL" ,5)');
        // dd($content);
        $contents = array();
        $casts = array();
        foreach ($content as $key => $value) {

            $value = explode(',', $value->result);
            $obj = new StdClass;
            if ($value[0] == 'content') {
                $obj->content_id = $value[1];
                $obj->title = $value[2];
                $obj->original_title = $value[3];
                $obj->original_release_year = $value[4];
                $obj->poster = $value[5];
                $obj->content_type = $value[6];
                array_push($contents, $obj);
            } elseif ($value[0] == 'person') {
                // dd($value);
                $obj->person_id = $value[1];
                $obj->name = $value[2];
                array_push($casts, $obj);
            }
        }

        // ==============Justwatch==============

        // if ($search == 'all'){

        //     $contents = DB::table('contents')
        //     ->leftJoin('content_casting','content_casting.content_id','contents.content_id')
        //     ->where('contents.content_type','!=','tv-episode')
        //     ->where('contents.title','LIKE',$keyword.'%')
        //     ->orWhere('content_casting.name','LIKE',$keyword.'%')
        //     ->orderBy('contents.title', 'ASC')
        //     ->select('contents.*')
        //     ->distinct('contents.content_id')
        //     ->limit(5)
        //     ->get();

        //   }
        //   else{

        //     $contents = DB::table('contents')
        //     ->leftJoin('content_casting','content_casting.content_id','contents.content_id')
        //     ->where('contents.content_type',$search)
        //     ->where('contents.title','LIKE',$keyword.'%')
        //     ->orWhere('content_casting.name','LIKE',$keyword.'%')
        //     ->orderBy('contents.title', 'ASC')
        //     ->select('contents.*')
        //     ->distinct('contents.content_id')
        //     ->limit(5)
        //     ->get();

        //   }

        //   $casts = DB::table('content_casting')
        //   ->where('name','LIKE',$keyword.'%')
        //   ->orderBy('name','ASC')
        //   ->select('person_id','name')
        //   ->distinct('person_id')
        //   ->limit(6)
        //   ->get();


        return view('khoj.dynamic-search', compact('contents', 'keyword', 'casts'));
    }


    public function languages()
    {
        $url = "https://api.themoviedb.org/3/configuration/languages?api_key=f0dc06cd59a97bbddcebd05e902559e0";

        $c = curl_init();
        curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($c, CURLOPT_URL, $url);
        curl_setopt($c, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($c, CURLOPT_SSL_VERIFYPEER, false);
        $contents = curl_exec($c);

        $contents = json_decode($contents);

        if (isset($contents)) {
            foreach ($contents as $content) {
                $lang = DB::table('languages')->insert(
                    array(
                        'code' => $content->iso_639_1,
                        'english_name' => $content->english_name,
                    )
                );
            }
        }
    }

    public function updateCertification()
    {
        $results = DB::table('contents')->where('age_certification', NULL)->orWhere('age_certification', '')->update(
            array(
                'age_certification' => 'UA',
            )
        );
        return "success";
    }


    public function getCastDetail($url, Request $request)
    {
        $cast_id = explode('-', $url);
        $cast_id = $cast_id[count($cast_id) - 1];
        $keyword = stripcslashes(explode('-', $url)[0]);
        $keyword = urlencode($keyword);
        if ($cast_id == 0) {
            $find_id = DB::table('content_casting')->where('name', str_replace('_', ' ', $keyword))->where('person_id', '!=', 0)->first();
            if ($find_id) {
                $cast_id = $find_id->person_id;
            }
        }

        // $cast_id;
        // $cast = DB::table('content_casting')->where('person_id',$cast_id)->first();
        // $contents = DB::table('content_casting')->leftJoin('contents','contents.content_id','content_casting.content_id')->where('content_casting.person_id',$cast_id)->orderBy('content_casting.role','ASC')->orderBy('contents.click_count','DESC')->orderBy('contents.imdb_score','DESC')->orderBy('contents.original_release_year','DESC')->select('contents.*')->paginate(28);
        // $type="all";

        $page = 0;
        $langs = 'ALL';
        $rated = "ALL";
        $sort = "";
        $field = "title";
        $sort_type = "ASC";
        $genre = 0;
        $minr = 0;
        $maxr = 10;

        $tab = 'search';

        $miny = '0000';
        // $maxy = DB::table('contents')->orderBy('cinema_release_date','DESC')->first();

        $maxy = date('Y');
        // $miny = DB::table('contents')->where('original_release_year','>','1900')->min('original_release_year');
        // $maxy = DB::table('contents')->max('original_release_year');

        if (isset($_GET['page'])) {
            $page = ($_GET['page'] - 1) * 28;
        }

        $keyword = $request->search;
        if ($request->searchtype)
            $type = $request->searchtype;
        else
            $type = 'ALL';
        $searchtype = $type;


        if (isset($_GET['lang'])) {
            $langs = $_GET['lang'];
        }

        if (isset($_GET['sort'])) {
            $sort = $_GET['sort'];
            if ($sort == "alpha") {
                $field = 'contents.title';
                $sort_type = 'ASC';
            } elseif ($sort == 'rate') {
                $field = 'contents.tmdb_score';
                $sort_type = 'DESC';
            } elseif ($sort == 'year') {
                $field = 'contents.cinema_release_date';
                $sort_type = 'DESC';
            } elseif ($sort == 'imdb_score') {
                $field = 'contents.imdb_score';
                $sort_type = 'DESC';
            } else {
                $field = 'contents.title';
                $sort_type = 'ASC';
            }
        }

        if (isset($_GET['sort']) && isset($_GET['genre'])) {
            $sort = $_GET['sort'];
            if ($sort == "alpha") {
                $field = 'contents.title';
                $sort_type = 'ASC';
            } elseif ($sort == 'rate') {
                $field = 'contents.tmdb_score';
                $sort_type = 'DESC';
            } elseif ($sort == 'year') {
                $field = 'contents.cinema_release_date';
                $sort_type = 'DESC';
            } elseif ($sort == 'imdb_score') {
                $field = 'contents.imdb_score';
                $sort_type = 'DESC';
            } else {
                $field = 'contents.title';
                $sort_type = 'ASC';
            }
        }

        if (isset($_GET['rated'])) {
            $rated = $_GET['rated'];
        }

        if (isset($_GET['genre'])) {
            $genre = $_GET['genre'];
        }

        if (isset($_GET['minr'])) {
            $minr = $_GET['minr'];
        }
        if (isset($_GET['maxr'])) {
            $maxr = $_GET['maxr'];
        }

        if (isset($_GET['miny'])) {
            $miny = $_GET['miny'];
        }
        if (isset($_GET['maxy'])) {
            $maxy = $_GET['maxy'];
        }

        // var_dump($keyword);
        // var_dump($genre);
        // var_dump($type);
        // var_dump($rated);
        // var_dump($langs);
        // die();
        $provider_id = 0;
        if (isset($request->provider)) {
            $provider_id = explode('_', $request->provider)[1];
        }
        $sort_type = "relevance";
        if (isset($_GET['sort_by'])) {
            $sort_type = $_GET['sort_by'];
        }

        $orderBy = "DESC";
        if (isset($_GET['order'])) {
            $orderBy = $_GET['order'];
        }
        $keyword =    str_replace('"', "'", $keyword);
        $cast_id =    str_replace('"', "'", $cast_id);
        $contents = DB::select('CALL ret_search("' . $keyword . '", "' . $genre . '", "' . $type . '","' . $miny . '", "' . $maxy . '", "' . $rated . '", "' . $langs . '",  "' . $page . '", 28, "' . $cast_id . '", "' . $provider_id . '", "' . $sort_type . '", "' . $orderBy . '")');
        // dd($contents);


        $miny = explode('-', $miny)[0];
        $maxy = explode('-', $maxy)[0];

        foreach ($contents as $key => $value) {
            $this->resizeImage($value->poster);
        }
        return view('khoj.home', compact('contents', 'page', 'keyword', 'type', 'miny', 'maxy', 'searchtype', 'langs', 'genre', 'rated', 'minr', 'maxr', 'tab'));
        // return view('khoj.cast_movies',compact('cast','contents','type'));
    }


    public function getTopMovies($score)
    {
        $type = "Top 10 Movies (" . strtoupper($score) . ")";
        $rating = $score . "_score";

        $contents = DB::table('contents')->where('content_type', 'movie')->orderBy($rating, 'DESC')->orderBy('content_id', 'DESC')->limit(10)->get();
        return view('khoj.top_result', compact('contents', 'type'));
    }
    public function getTopShows($score)
    {
        $type = "Top 10 TV Shows (" . strtoupper($score) . ")";
        $rating = $score . "_score";

        $contents = DB::table('contents')->where('content_type', 'show')->orWhere('content_type', 'tv-show')->orderBy($rating, 'DESC')->limit(10)->get();
        return view('khoj.top_result', compact('contents', 'type'));
    }
    public function getTopVideos($score)
    {
        $type = "Top 10 Viral Videos (" . strtoupper($score) . ")";
        $rating = $score . "_score";

        $contents = DB::table('contents')->where('content_type', 'viral')->orderBy($rating, 'DESC')->limit(10)->get();
        return view('khoj.top_result', compact('contents', 'type'));
    }


    public function submitFeedback(Request $request)
    {
        $data = $request->all();
        $newData = new Feedback;

        $newData->name = $data['name'];
        $newData->email = $data['email'];
        $newData->subject = $data['subject'];
        $newData->message = $data['message'];
        $newData->status = 'Active';

        $newData->save();

        return redirect()->back()->with('feedback_success', '1');
    }

    public function showEpisodes(Request $request)
    {
        $data = $request->all();
        $id = $data['id'];
        $season_id = $id;
        $season_one = "";
        $episodes = "";
        $movie = DB::table('contents')->where('content_id', $id)->first();
        $season = DB::table('contents')->where('parent_id', $data['id'])->get()->last();

        if ($movie->content_type == 'season' || $movie->content_type == 'tv-season') {
            $season_id = $movie->content_id ?? '';
            $season_one = $movie->title ?? '';
        } else {
            $season_id = $season->content_id ?? '';
            $season_one = $season->title ?? '';
        }

        if ($data['type'] == 'all') {
            $episodes = DB::table('contents')->where('parent_id', $season_id)->where('content_type', 'tv-episode')->orderBy('cinema_release_date', 'DESC')->orderBy('content_id', 'DESC')->get();
        } else {
            $episodes = DB::table('contents')->where('parent_id', $season_id)->where('content_type', 'tv-episode')->orderBy('cinema_release_date', 'DESC')->orderBy('content_id', 'DESC')->limit(5)->get();
        }

        $episodestotal = DB::table('contents')->where('parent_id', $season_id)->where('content_type', 'tv-episode')->orderBy('cinema_release_date', 'DESC')->orderBy('content_id', 'DESC')->count();

        return view('khoj.dynamic-episodes', compact('movie', 'season', 'season_one', 'episodes', 'season_id', 'episodestotal'));
    }


    public function playlists(Request $request)
    {
        $tab = "Playlist";
        $playlists = PublicPlaylist::orderBy('id', 'desc')->paginate(10);
        return view('khoj.playlist', compact('tab', 'playlists'));
        # code...
    }


    public function playlistDetails($title, Request $request)
    {
        $tab = "Playlist Details";

        $totalname = explode('-', $title);
        $content_id = $totalname[count($totalname) - 1];
        $playlist = PublicPlaylist::where('id', $content_id)->first();
        $movies = [];
        $movie_id = explode(',', $playlist->playlist_content);

        // var_dump($movie_id); die;
        foreach ($movie_id as $key => $value) {
            $movie = Content::where('content_id',  $value)->first();
            if (!empty($movie))
                $movies[] = $movie;
            # code...
        }
        $playlists = $movies;
        return view('khoj.playlistdetails', compact('tab', 'playlists', 'playlist'));
        # code...
    }


    public function checkEmail(Request $request)
    {
        $user = User::where('email', $request->email)->first();

        if (!empty($user)) {
            echo '1';
        } else {
            echo 0;
        }
    }


    public function checkMobile(Request $request)
    {
        $user = User::where('mobile', $request->mobile)->first();

        if (!empty($user)) {
            echo '1';
        } else {
            echo 0;
        }
    }

    public function updateReleaseYear()
    {
        \DB::table('contents')->where('original_release_year', '')->update(array(
            'original_release_year' => '0000'
        ));
    }
}
