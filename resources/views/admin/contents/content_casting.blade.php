@extends('layouts.admin')


@section('content')
<h4 class="header-title m-t-0 m-b-30">Default Example</h4>
<h4 class="header-title m-t-0 m-b-30">All Casts</h4>
@if(Session::get('success'))
<div class="alert alert-success"> {{Session::get('success')}} </div>
@endif
@if (count($errors) > 0)
<div class="alert alert-danger">
	<ul>
		@foreach ($errors->all() as $error)
		<li>{{ $error }}</li>
		@endforeach
	</ul>
</div>
@endif

<form method="POST" action="{{url('admin/store_cast/'.$id)}}" enctype="multipart/form-data">
	@csrf
	
	<div class="row input-control">
		<label class="col-sm-3">Select Cast</label>

		<div class="col-sm-6">
			
			<select name='person_id' id="person_id" class="form-control">

				@if(!empty($casts))
				@foreach($casts as $cast)
				<option value="{{$cast->person_id}}">{{ucfirst($cast->name)}} ( {{$cast->role}} )</option>
				@endforeach
				@endif			
				
			</select>
		</div>
	</div>

<div class="row input-control">
		<label class="col-sm-3">Cast Poster</label>
		<div class="col-sm-6">
			
			<input type="file" name="poster" id="poster" class="form-control">

		</div>
	</div>


	<div class="row form-group">
		<div class="col-sm-6 col-sm-offset-5">
			<button type="submit" name="save" class="btn btn-primary">Save</button> 
		</div>
	</div>

		<h4> Casts of <i>'{{ucfirst($content->title)}}'</i>: </h4>
<div class="row">

	<br><br>

<table class="table">

			<thead>
				<th>ID</th>
				<th>Name</th>
				<th>Role</th>
				<th>Poster</th>
				<th>Action</th>
			</thead>

			<tbody>

				@if(!empty($content_casts))
			@foreach($content_casts as $cast)
				<tr>
					<th>{{$cast->person_id ?? ''}}</th>
					<th>{{$cast->name ?? ''}}</th>
					<th>{{$cast->role ?? ''}}</th>
					<th>
						@if(!empty($cast->poster))
						<img src="{{asset('poster/'.$cast->poster)}}" style="height: 120px; width: auto;">
						@else
						<img src="/images/default.png" style="height: 120px; width: auto;">
						@endif


					</th>
					<th>
						<a href="{{url('admin/delete_cast/'.$cast->content_id.'/'.$cast->person_id)}}" class="btn btn-danger"> <i class="fa fa-trash" style="cursor: pointer;" ></i> </a>
					</th>

				</tr>

				@endforeach
				@endif

			</tbody>
		</table>





	
		
	</div>


</form>


@endsection
