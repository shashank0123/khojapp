<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Crew;
use App\Movie;
use App\Cast;
use App\WatchList;
use App\Playlist;
use App\Favourite;
use App\Genre;
use App\Rating;
use DB;


class ViewController extends Controller
{
	public function __construct()
	{
		$genre = Genre::all();
		session()->put('genre', $genre);
		$featureds = Movie::where('featured', 'yes')->get();
		foreach ($featureds as $key => $value) {
			$id = $value->movie_id;
			$favourite = 0;
			$watchlist = 0;
			$playlist = 0;
			if (isset(auth()->user()->id)){
				$favourite = Favourite::where('movie_id', $id)->where('user_id', auth()->user()->id)->first();
				if ($favourite){
					$featureds[$key]->favourite = 1;
				}
				$watchlist = WatchList::where('movie_id', $id)->where('user_id', auth()->user()->id)->first();
				if ($watchlist){
					$featureds[$key]->watchlist = 1;
				}

				$playlist = Playlist::where('movie_id', $id)->where('user_id', auth()->user()->id)->first();
				if ($playlist){
					$featureds[$key]->playlist = 1;
				}
			}
			$casts = Crew::join('casts', 'casts.id' , '=', 'crews.cast_id')->where('movie_id', $id)->where('job', '!=', 'Director')->get()->take(6);
			$featureds[$key]->crews = $casts;

		}

		
		session()->put('featureds', $featureds);
	}

	public function getmain_home(Request $request)
	{	
		if (isset($request->page_id) && $request->page_id >1){
			$page = $request->page_id;
		}  	
		else
			$page = 1;
		$url = "https://api.themoviedb.org/3/discover/movie?api_key=f0dc06cd59a97bbddcebd05e902559e0&language=en-US&sort_by=popularity.desc&include_adult=false&include_video=false&page=$page";
		$genre_url = "https://api.themoviedb.org/3/genre/movie/list?api_key=f0dc06cd59a97bbddcebd05e902559e0&language=en-US";
		$c = curl_init();
		curl_setopt($c,CURLOPT_RETURNTRANSFER,1);
		curl_setopt($c, CURLOPT_URL, $url);
		curl_setopt($c, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($c, CURLOPT_SSL_VERIFYPEER, false);
		// curl_setopt($c, CURLOPT_SSL_VERIFYHOST, 0);
		// curl_setopt($c, CURLOPT_SSL_VERIFYPEER, 0);
		$contents = curl_exec($c);
		$genrerequest = curl_init();
		curl_setopt($genrerequest,CURLOPT_RETURNTRANSFER,1);
		curl_setopt($genrerequest, CURLOPT_URL, $genre_url);
		curl_setopt($genrerequest, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($genrerequest, CURLOPT_SSL_VERIFYPEER, false);
		$genres = curl_exec($genrerequest);
		
		$contents = json_decode($contents);
		if (isset($contents))
			$genres = json_decode($genres);
		if (isset($contents) && isset($genres))
			$genres = $genres->genres;
		else 
			$genres = array();

		return view('main_home', compact('contents', 'genres'));
	}


	public function getgenre(Request $request, $genre)
	{	
		if (isset($request->page_id) && $request->page_id >1){
			$page = $request->page_id;
		}  	
		else
			$page = 1;
		$genre = ucfirst($genre);
		$genre = Genre::where('name', $genre)->first();
		$genres = $genre->genre_id;
		$url = "https://api.themoviedb.org/3/discover/movie?api_key=f0dc06cd59a97bbddcebd05e902559e0&language=en-US&sort_by=popularity.desc&include_adult=false&include_video=false&page=1&with_genres=$genres";
		$c = curl_init();
		curl_setopt($c,CURLOPT_RETURNTRANSFER,1);
		curl_setopt($c, CURLOPT_URL, $url);
		curl_setopt($c, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($c, CURLOPT_SSL_VERIFYPEER, false);
		$contents = curl_exec($c);		
		$contents = json_decode($contents);
		$genre = $genre->name;
		return view('genre', compact('contents','genre'));

	}

	public function getnew_home(Request $request)
	{ 
		if (isset($request->page_id) && $request->page_id >1){
			$page = $request->page_id;
		}  	
		else
			$page = 1;   	

		echo $url = "https://api.themoviedb.org/3/discover/movie?api_key=f0dc06cd59a97bbddcebd05e902559e0&language=en-US&sort_by=release_date.desc&include_adult=false&include_video=false&page=$page";
		$genre_url = "https://api.themoviedb.org/3/genre/movie/list?api_key=f0dc06cd59a97bbddcebd05e902559e0&language=en-US";
		// die();
		$c = curl_init();
		curl_setopt($c,CURLOPT_RETURNTRANSFER,1);
		curl_setopt($c, CURLOPT_URL, $url);

		curl_setopt($c, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($c, CURLOPT_SSL_VERIFYPEER, 0);
		echo $contents = curl_exec($c);
		// $contents = json_decode($contents);
		if (isset($contents)){
		// print_r($contents);
			$movies = json_decode($contents);
			$movies = $movies->results;
		}
		else
			$movies = [];
		$genrerequest = curl_init();
		curl_setopt($genrerequest,CURLOPT_RETURNTRANSFER,1);
		curl_setopt($genrerequest, CURLOPT_URL, $genre_url);
		$genres = curl_exec($genrerequest);
		$contents = json_decode($genres);
		
		if (isset($contents) && isset($genres))
			$genres = $genres->genres;
		else 
			$genres = array();
		$contents = $movies;
		// die();
		return view('main_home', compact('contents', 'genres'));
	}

	public function getcoming_home(Request $request)
	{
		if (isset($request->page_id) && $request->page_id >1){
			$page = $request->page_id;
		}  	
		else
			$page = 1;    	

		$url = "https://api.themoviedb.org/3/discover/movie?api_key=f0dc06cd59a97bbddcebd05e902559e0&language=en-US&region=US&sort_by=release_date.desc&include_adult=false&include_video=false&page=$page";

		$genre_url = "https://api.themoviedb.org/3/genre/movie/list?api_key=f0dc06cd59a97bbddcebd05e902559e0&language=en-US";
		$c = curl_init();
		curl_setopt($c,CURLOPT_RETURNTRANSFER,1);
		curl_setopt($c, CURLOPT_URL, $url);
		curl_setopt($c, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($c, CURLOPT_SSL_VERIFYPEER, 0);
		$contents = curl_exec($c);
		$genrerequest = curl_init();
		curl_setopt($genrerequest,CURLOPT_RETURNTRANSFER,1);
		curl_setopt($genrerequest, CURLOPT_URL, $genre_url);
		curl_setopt($genrerequest, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($genrerequest, CURLOPT_SSL_VERIFYPEER, false);
		$genres = curl_exec($genrerequest);
		
		$contents = json_decode($contents);
		$genres = json_decode($genres);
		$genres = $genres->genres;
		return view('main_home', compact('contents', 'genres'));
	}

	public function getmain_hometv(Request $request)
	{
		if (isset($request->page_id) && $request->page_id >1){
			$page = $request->page_id;
		}  	
		else
			$page = 1;    	

		$url = "https://api.themoviedb.org/3/discover/tv?api_key=f0dc06cd59a97bbddcebd05e902559e0&language=en-US&sort_by=popularity.desc&include_adult=false&include_video=false&page=$page";
		$genre_url = "https://api.themoviedb.org/3/genre/movie/list?api_key=f0dc06cd59a97bbddcebd05e902559e0&language=en-US";
		$c = curl_init();
		curl_setopt($c,CURLOPT_RETURNTRANSFER,1);
		curl_setopt($c, CURLOPT_URL, $url);
		curl_setopt($c, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($c, CURLOPT_SSL_VERIFYPEER, 0);
		$contents = curl_exec($c);
		$genrerequest = curl_init();
		curl_setopt($genrerequest,CURLOPT_RETURNTRANSFER,1);
		curl_setopt($genrerequest, CURLOPT_URL, $genre_url);
		curl_setopt($genrerequest, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($genrerequest, CURLOPT_SSL_VERIFYPEER, false);
		$genres = curl_exec($genrerequest);
		
		$contents = json_decode($contents);
		$genres = json_decode($genres);
		return view('main_tv', compact('contents'));
	}

	public function getmovie_detail(Request $request, $id)
	{
		$favourite = 0;
		$watchlist = 0;
		$playlist = 0;
		$user_rating = 0;
		if (isset(auth()->user()->id)){
			$favourite = Favourite::where('movie_id', $id)->where('user_id', auth()->user()->id)->first();
			if ($favourite){
				$favourite = 1;
			}
			$watchlist = WatchList::where('movie_id', $id)->where('user_id', auth()->user()->id)->first();
			if ($watchlist){
				$watchlist = 1;
			}

			$playlist = Playlist::where('movie_id', $id)->where('user_id', auth()->user()->id)->first();
			if ($playlist){
				$playlist = 1;
			}
			$user_rating = Rating::where('user_id', auth()->user()->id)->where('movie_id', $id)->first();
		}
		$casts = Crew::join('casts', 'casts.id' , '=', 'crews.cast_id')->where('movie_id', $id)->where(function ($query) {
			$query->where('job', '!=', 'Director')->orWhere('job', '!=' , 'Story');
		})->get();

		$crews = $casts;


		$movie = Movie::where('movie_id', $id)->first();
		if (!isset($movie)){
			$url = "https://api.themoviedb.org/3/movie/$id?api_key=f0dc06cd59a97bbddcebd05e902559e0&language=en-US";
	    	// die();
			$c = curl_init();
			curl_setopt($c,CURLOPT_RETURNTRANSFER,1);
			curl_setopt($c, CURLOPT_URL, $url);
			curl_setopt($c, CURLOPT_SSL_VERIFYHOST, false);
			curl_setopt($c, CURLOPT_SSL_VERIFYPEER, false);
			$contents = curl_exec($c);
			$contents = json_decode($contents);
			$movie = new Movie;
			$movie->movie_id = $id; 
			$movie->name = $contents->original_title; 
			$movie->runtime = $contents->runtime; 
			$movie->release_date = $contents->release_date; 
			$movie->poster_path = $contents->poster_path; 
			$movie->vote_average = $contents->vote_average; 
			$movie->overview = $contents->overview; 
			$movie->type = 'Movie'; 
			
			$movie->save(); 
		}
		if (count($casts)<1){
			$castlist = "https://api.themoviedb.org/3/movie/$id/credits?api_key=f0dc06cd59a97bbddcebd05e902559e0";
			$creditcurl = curl_init();
			curl_setopt($creditcurl,CURLOPT_RETURNTRANSFER,1);
			curl_setopt($creditcurl, CURLOPT_URL, $castlist);
			curl_setopt($creditcurl, CURLOPT_SSL_VERIFYHOST, false);
			curl_setopt($creditcurl, CURLOPT_SSL_VERIFYPEER, false);
			$castlist = curl_exec($creditcurl);
			$castlist = json_decode($castlist);
			$crews = $castlist->crew;
			$casts = $castlist->cast;
			foreach ($casts as $key => $value) {
				// var_dump($value);
				// die();
				$cast = Cast::where('name', $value->name)->first();
				if (!$cast){
					$cast = new Cast;
					$cast->name = $value->name;
					$cast->id = $value->id;
					$cast->profile_path = $value->profile_path;
					$cast->save();
				}


				$crew = Crew::where('cast_id', $cast->id)->first();
				if (!$crew){
					$crew = new Crew;
					$crew->movie_id = $id;
					$crew->cast_id = $cast->id;
					$crew->job = $value->character;
					$crew->save();
				}
			}

		}
		if (isset($crews)){
			$directors = array();
			$writers = array();

			foreach ($crews as $key => $value) {
				if ($value->job == 'Director'){
					$castcheck = Cast::where('name', $value->name)->first();
					if ($castcheck){
						$crewcheck = Crew::where('cast_id', $castcheck->id)->first();
						if (!$crewcheck){
							$crew = new Crew;
							$crew->movie_id = $id;
							$crew->cast_id = $castcheck->id;
							$crew->job = $value->job;
							$crew->save();
						}
					}
					else
					{
						$cast = new Cast;
						$cast->name = $value->name;
						$cast->id = $value->id;
						$cast->profile_path = $value->profile_path;
						$cast->save();
					}
					array_push($directors, $value->name);
				}
				if ($value->job == 'Story'){
					$castcheck = Cast::where('name', $value->name)->first();
					if ($castcheck){
						$crewcheck = Crew::where('cast_id', $castcheck->id)->first();
						if (!$crewcheck){
							$crew = new Crew;
							$crew->movie_id = $id;
							$crew->cast_id = $castcheck->id;
							$crew->job = $value->job;
							$crew->save();
						}
					}
					else
					{
						$cast = new Cast;
						$cast->name = $value->name;
						$cast->id = $value->id;
						$cast->profile_path = $value->profile_path;
						$cast->save();
					}
					array_push($writers, $value->name);
				}
			}
		}
		else{
			$directors = Crew::join('casts', 'casts.id' , '=' ,'crews.cast_id')
			->select('casts.name')
			->where('crews.movie_id', $id)
			->where('crews.job', 'Director')
			->get();
			$writers = Crew::join('casts', 'casts.id' , '=' ,'crews.cast_id')
			->select('casts.name')
			->where('crews.movie_id', $id)
			->where('crews.job', 'Story')
			->get();
		}
		
		$trailorurl = "https://api.themoviedb.org/3/movie/$id/videos?api_key=f0dc06cd59a97bbddcebd05e902559e0&language=en-US";
		$trailorcurl = curl_init();
		curl_setopt($trailorcurl,CURLOPT_RETURNTRANSFER,1);
		curl_setopt($trailorcurl, CURLOPT_URL, $trailorurl);
		curl_setopt($trailorcurl, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($trailorcurl, CURLOPT_SSL_VERIFYPEER, false);
		$trailors = curl_exec($trailorcurl);
		$trailors = json_decode($trailors);
		$trailorslist = array();
		if (isset($trailors->results))
			foreach ($trailors->results as $key => $value) {
				if ($value->type == "Trailer")
					array_push($trailorslist, $value->key);
			}

			$handle = curl_init();
			$urlcani = 'http://www.canistream.it/services/search/?movieName=' . urlencode($movie->name);
		// die();
			curl_setopt( $handle, CURLOPT_URL, $urlcani );
			curl_setopt( $handle, CURLOPT_USERAGENT, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.94 Safari/537.36' );
			curl_setopt($handle, CURLOPT_RETURNTRANSFER, 1 );
			curl_setopt($handle, CURLOPT_SSL_VERIFYHOST, false);
			curl_setopt($handle, CURLOPT_SSL_VERIFYPEER, false);
		// $result = curl_exec( $handle );
		// curl_close( $handle );
		// $streamat = array();
		// $result = json_decode($result);
			if (isset($result[0]->affiliates)){
				foreach ($result[0]->affiliates as $key => $value) {
					if (isset($value->url) ){
						$streamat[$key] = $value;
					}
				}

			}

			$reviewurl = "https://api.themoviedb.org/3/movie/$id/reviews?api_key=f0dc06cd59a97bbddcebd05e902559e0&language=en-US&page=1";
			$reviewcurl = curl_init();
			curl_setopt($reviewcurl,CURLOPT_RETURNTRANSFER,1);
			curl_setopt($reviewcurl, CURLOPT_URL, $reviewurl);
			curl_setopt($reviewcurl, CURLOPT_SSL_VERIFYHOST, false);
			curl_setopt($reviewcurl, CURLOPT_SSL_VERIFYPEER, false);
			$reviews = curl_exec($reviewcurl);
			$reviews = json_decode($reviews);
			$reviewslist = $reviews->results;

			return view('movie_detail', compact('movie','directors','writers','trailorslist','reviewslist', 'casts', 'crews', 'watchlist', 'playlist', 'favourite', 'user_rating'));
		}


		public function gettv_detail(Request $request, $id)
		{
			$favourite = 0;
			$watchlist = 0;
			$playlist = 0;
			if (isset(auth()->user()->id)){
				$favourite = Favourite::where('movie_id', $id)->where('user_id', auth()->user()->id)->first();
				if ($favourite){
					$favourite = 1;
				}
				$watchlist = WatchList::where('movie_id', $id)->where('user_id', auth()->user()->id)->first();
				if ($watchlist){
					$watchlist = 1;
				}

				$playlist = Playlist::where('movie_id', $id)->where('user_id', auth()->user()->id)->first();
				if ($playlist){
					$playlist = 1;
				}
			}
			$casts = Crew::join('casts', 'casts.id' , '=', 'crews.cast_id')->where('movie_id', $id)->where('job', '!=', 'Director')->get();
			$movie = Movie::where('movie_id', $id)->first();
			if (!isset($movie)){
				$url = "https://api.themoviedb.org/3/tv/$id?api_key=f0dc06cd59a97bbddcebd05e902559e0&language=en-US";
				$c = curl_init();
				curl_setopt($c,CURLOPT_RETURNTRANSFER,1);
				curl_setopt($c, CURLOPT_URL, $url);

				$contents = curl_exec($c);

				$contents = json_decode($contents);
				if (!isset($contents->original_name)){
					return redirect('/');
				}
				$movie = new Movie;
				$movie->movie_id = $id; 
				$movie->name = $contents->original_name; 
			// $movie->runtime = $contents->episode_run_time; 
				$movie->release_date = $contents->first_air_date; 
				$movie->poster_path = $contents->poster_path; 
				$movie->vote_average = $contents->vote_average; 
				$movie->overview = $contents->overview; 
				$movie->type = 'Series'; 

				$movie->save(); 
			}
			if (count($casts)<1){
				$castlist = "https://api.themoviedb.org/3/tv/$id/credits?api_key=f0dc06cd59a97bbddcebd05e902559e0";
				$creditcurl = curl_init();
				curl_setopt($creditcurl,CURLOPT_RETURNTRANSFER,1);
				curl_setopt($creditcurl, CURLOPT_URL, $castlist);
				$castlist = curl_exec($creditcurl);
				$castlist = json_decode($castlist);
				$crews = $castlist->crew;
				$casts = $castlist->cast;
				foreach ($casts as $key => $value) {
					$castcheck = Cast::where('name', $value->name)->first();
					if ($castcheck){
						$crewcheck = Crew::where('cast_id', $castcheck->id)->first();
						if (!$crewcheck){
							$crew = new Crew;
							$crew->movie_id = $id;
							$crew->cast_id = $castcheck->id;
							$crew->job = $value->character;
							$crew->save();
						}
					}
					else
					{
						$cast = new Cast;
						$cast->name = $value->name;
						$cast->id = $value->id;
						$cast->profile_path = $value->profile_path;
						$cast->save();
					}
				}

			}
			if (isset($crews)){
				$directors = array();
				$writers = array();

				foreach ($crews as $key => $value) {
					if ($value->job == 'Director'){
						$castcheck = Cast::where('name', $value->name)->first();
						if ($castcheck){
							$crewcheck = Crew::where('cast_id', $castcheck->id)->first();
							if (!$crewcheck){
								$crew = new Crew;
								$crew->movie_id = $id;
								$crew->cast_id = $castcheck->id;
								$crew->job = $value->job;
								$crew->save();
							}
						}
						else
						{
							$cast = new Cast;
							$cast->name = $value->name;
							$cast->id = $value->id;
							$cast->profile_path = $value->profile_path;
							$cast->save();
						}
						array_push($directors, $value->name);
					}
					if ($value->job == 'Story'){
						$castcheck = Cast::where('name', $value->name)->first();
						if ($castcheck){
							$crewcheck = Crew::where('cast_id', $castcheck->id)->first();
							if (!$crewcheck){
								$crew = new Crew;
								$crew->movie_id = $id;
								$crew->cast_id = $castcheck->id;
								$crew->job = $value->job;
								$crew->save();
							}
						}
						else
						{
							$cast = new Cast;
							$cast->id = $value->id;
							$cast->name = $value->name;

							$cast->profile_path = $value->profile_path;
							$cast->save();
						}
						array_push($writers, $value->name);
					}
				}
			}
			else{
				$directors = Crew::join('casts', 'casts.id' , '=' ,'crews.cast_id')
				->select('casts.name')
				->where('crews.movie_id', $id)
				->where('crews.job', 'Director')
				->get();
				$writers = Crew::join('casts', 'casts.id' , '=' ,'crews.cast_id')
				->select('casts.name')
				->where('crews.movie_id', $id)
				->where('crews.job', 'Story')
				->get();
			}

			$trailorurl = "https://api.themoviedb.org/3/tv/$id/videos?api_key=f0dc06cd59a97bbddcebd05e902559e0&language=en-US";
			$trailorcurl = curl_init();
			curl_setopt($trailorcurl,CURLOPT_RETURNTRANSFER,1);
			curl_setopt($trailorcurl, CURLOPT_URL, $trailorurl);
			$trailors = curl_exec($trailorcurl);
			$trailors = json_decode($trailors);
			$trailorslist = array();
			if(isset($trailors)){
				foreach ($trailors->results as $key => $value) {
					if ($value->type == "Trailer")
						array_push($trailorslist, $value->key);
				}
			}

			$handle = curl_init();
			curl_setopt( $handle, CURLOPT_URL, 'http://www.canistream.it/services/search/?movieName=' . urlencode($movie->name) );
			curl_setopt( $handle, CURLOPT_USERAGENT, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.94 Safari/537.36' );
			curl_setopt($handle, CURLOPT_RETURNTRANSFER, 1 );
			// $result = curl_exec( $handle );
			$result = [];
			// curl_close( $handle );
			$streamat = array();
			// $result = json_decode($result);

			if (isset($result[0]->affiliates)){
				foreach ($result[0]->affiliates as $key => $value) {
					if (isset($value->url) ){
						$streamat[$key] = $value;
					}
					if (isset($value->SD->url))
						$streamat[$key] = $value->SD;
					if (isset($value->HD->url))
						$streamat[$key] = $value->SD;
				}

			}

			$reviewurl = "https://api.themoviedb.org/3/movie/$id/reviews?api_key=f0dc06cd59a97bbddcebd05e902559e0&language=en-US&page=1";
			$reviewcurl = curl_init();
			curl_setopt($reviewcurl,CURLOPT_RETURNTRANSFER,1);
			curl_setopt($reviewcurl, CURLOPT_URL, $reviewurl);
			$reviews = curl_exec($reviewcurl);
			$reviews = json_decode($reviews);
			$reviewslist = array();
			if(isset($reviews))
			$reviewslist = $reviews->results;

			return view('movie_detail', compact('movie','directors','writers','trailorslist','reviewslist', 'casts', 'crews', 'watchlist', 'playlist', 'favourite','streamat'));
		}


		public function playlists(Request $request)
		{
			$playlists = Playlist::join('movies', 'movies.movie_id', '=', 'playlists.movie_id')->distinct('playlists.user_id');

			return view('playlists', compact('playlists'));
		}

		public function search(Request $request)
		{
			if (isset($request->page_id) && $request->page_id >1){
				$page = $request->page_id;
			}  	
			else
				$page = 1;
			$country = $request->country;
			if ($country=='ALL')
				$country = null;
			$rating = $request->rating;
			if ($rating)
				$rating = explode(',', $rating);
			else{
				$rating = [0,10];
			}
			$keyword = $request->keyword;
			$genre = $request->genre;
		// echo $url = "https://api.themoviedb.org/3/search ?api_key=f0dc06cd59a97bbddcebd05e902559e0&language=en-US&region=$country&sort_by=popularity.desc&include_adult=false&include_video=false&page=1&vote_average.gte=".$rating[0]."&vote_average.lte=".$rating[1]."&with_genre=$genre&with_keyword=$keyword";
			$url = "https://api.themoviedb.org/3/search/movie?api_key=f0dc06cd59a97bbddcebd05e902559e0&language=en-US&region=$country&sort_by=popularity.desc&include_adult=false&query=$keyword";
		// die;
			$genre_url = "https://api.themoviedb.org/3/genre/movie/list?api_key=f0dc06cd59a97bbddcebd05e902559e0&language=en-US";
			$c = curl_init();
			curl_setopt($c,CURLOPT_RETURNTRANSFER,1);
			curl_setopt($c, CURLOPT_URL, $url);
			curl_setopt($c, CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt($c, CURLOPT_SSL_VERIFYPEER, 0);
			$contents = curl_exec($c);
			$genrerequest = curl_init();
			curl_setopt($genrerequest,CURLOPT_RETURNTRANSFER,1);
			curl_setopt($genrerequest, CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt($genrerequest, CURLOPT_SSL_VERIFYPEER, 0);
			curl_setopt($genrerequest, CURLOPT_URL, $genre_url);
			$genres = curl_exec($genrerequest);

			$contents = json_decode($contents);
			$genres = json_decode($genres);
			$genres = $genres->genres;

			return view('main_home', compact('contents', 'genres'));
		}


		public function savegenre(Request $request)
		{
			$genre_url = "https://api.themoviedb.org/3/genre/movie/list?api_key=f0dc06cd59a97bbddcebd05e902559e0&language=en-US";
		// phpinfo();  


			$genrerequest = curl_init();

			curl_setopt($genrerequest,CURLOPT_RETURNTRANSFER,1);
			curl_setopt($genrerequest,CURLOPT_HTTPGET ,1);
			curl_setopt($genrerequest, CURLOPT_SSL_VERIFYHOST, false);
			curl_setopt($genrerequest, CURLOPT_SSL_VERIFYPEER, false);

			curl_setopt($genrerequest, CURLOPT_URL, $genre_url);

			$genres = curl_exec($genrerequest);

			$genres = json_decode($genres);

			$genres = $genres->genres;

			foreach ($genres as $key => $value) {
				$genre = new Genre;
				$genre->genre_id = $value->id;
				$genre->name =$value->name;
				$genre->save();
			}
			echo 'Done';

		}

  //   public function getnew_home(Request $request)
  //   { 
  //   if (isset($request->page_id) && $request->page_id >1){
  //   		$page = $request->page_id;
  //   	}  	
  //   	else
  //   		$page = 1;   	

		// $url = "https://api.themoviedb.org/3/discover/movie?api_key=f0dc06cd59a97bbddcebd05e902559e0&language=en-US&sort_by=release_date.desc&include_adult=false&include_video=false&page=$page";
		
		// $genre_url = "https://api.themoviedb.org/3/genre/movie/list?api_key=f0dc06cd59a97bbddcebd05e902559e0&language=en-US";
		// $c = curl_init();
	 //    curl_setopt($c,CURLOPT_RETURNTRANSFER,1);
	 //    curl_setopt($c, CURLOPT_URL, $url);
		// // curl_setopt($c, CURLOPT_SSL_VERIFYHOST, 0);
		// // curl_setopt($c, CURLOPT_SSL_VERIFYPEER, 0);
	 //    $contents = curl_exec($c);
		// $genrerequest = curl_init();
	 //    curl_setopt($genrerequest,CURLOPT_RETURNTRANSFER,1);
	 //    curl_setopt($genrerequest, CURLOPT_URL, $genre_url);
	 //    $genres = curl_exec($genrerequest);
		
		// $contents = json_decode($contents);
		// $genres = json_decode($genres);
		// $genres = $genres->genres;
		// return view('main_home', compact('contents', 'genres'));
  //   }


  //   public function gettv_detailbackup(Request $request, $id)
  //   {
  //   	$url = "https://api.themoviedb.org/3/tv/$id?api_key=f0dc06cd59a97bbddcebd05e902559e0&language=en-US";
  //   	$trailorurl = "https://api.themoviedb.org/3/tv/$id/videos?api_key=f0dc06cd59a97bbddcebd05e902559e0&language=en-US";
  //   	$castlist = "https://api.themoviedb.org/3/tv/$id/credits?api_key=f0dc06cd59a97bbddcebd05e902559e0";
  //   	$reviewurl = "https://api.themoviedb.org/3/tv/$id/reviews?api_key=f0dc06cd59a97bbddcebd05e902559e0&language=en-US&page=1";
		// $c = curl_init();
	 //    curl_setopt($c,CURLOPT_RETURNTRANSFER,1);
	 //    curl_setopt($c, CURLOPT_URL, $url);
		// $contents = curl_exec($c);
		// $creditcurl = curl_init();
	 //    curl_setopt($creditcurl,CURLOPT_RETURNTRANSFER,1);
	 //    curl_setopt($creditcurl, CURLOPT_URL, $castlist);
		// $castlist = curl_exec($creditcurl);
		// $contents = json_decode($contents);
		// $castlist = json_decode($castlist);
		// $crew = $castlist->crew;
		// $casts = $castlist->cast;
		// $directors = array();
		// $writers = array();
		// // foreach ($crew as $key => $value) {
		// // 	if ($value->job == 'Director')
		// // 		array_push($directors, $value->name);
		// // 	if ($value->job == 'Story')
		// // 		array_push($writers, $value->name);
		// // }

		// $trailorcurl = curl_init();
	 //    curl_setopt($trailorcurl,CURLOPT_RETURNTRANSFER,1);
	 //    curl_setopt($trailorcurl, CURLOPT_URL, $trailorurl);
		// $trailors = curl_exec($trailorcurl);
		// $trailors = json_decode($trailors);
		// $trailorslist = array();
		// foreach ($trailors->results as $key => $value) {
		// 	if ($value->type == "Trailer")
		// 		array_push($trailorslist, $value->key);
		// }

		// $reviewcurl = curl_init();
	 //    curl_setopt($reviewcurl,CURLOPT_RETURNTRANSFER,1);
	 //    curl_setopt($reviewcurl, CURLOPT_URL, $reviewurl);
		// $reviews = curl_exec($reviewcurl);
		// $reviews = json_decode($reviews);
		// $reviewslist = $reviews->results;
		
		// return view('tv_detail', compact('contents','directors','writers','trailorslist','reviewslist', 'casts', 'crew'));
  //   }
	}
