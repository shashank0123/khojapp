<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Notification;
use App\User;
use App\Profile;
use App\Recommendation;
use App\WatchList;
use App\Playlist;
use App\Favourite;
use App\Rating;
use App\Seo;
use Input;


class UserController extends Controller
{
		 public function showhome(Request $request)
    {
        return view('admin.home');
    }

    public function showusers(Request $request)
    {
        $users = User::where('user_type', 'user')->get();
        return view('admin.userlist', compact('users'));
    }
    
    public function getSeo(Request $request){
    	$seo = Seo::first();

    	return view('admin.seo',compact('seo'));
    }

public function postSeo(Request $request){
    	$seo = Seo::first();

    	if(!empty($seo)){
    		$seo->page_tags = $request->page_tags ?? '';  
    		$seo->page_title = $request->page_title ?? '';  
    		$seo->page_description = $request->page_description ?? '';  

    		$seo->update();
    	}
    	else{
    		$newData = new Seo;
    		$newData->page_tags = $request->page_tags ?? '';  
    		$newData->page_title = $request->page_title ?? '';  
    		$newData->page_description = $request->page_description ?? '';  

    		$newData->save();
    	}

    	return view('admin.seo',compact('seo'));
    }


}