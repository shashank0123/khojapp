<?php
use App\Seo;
use App\Genre;
use App\FeaturedImage;
$flag = 0;
$type = "";

$seo = Seo::first();
?>

@extends('layouts.khoj_new') 

@section('tags')
{{$seo->page_tags ?? 'khojaap online movie search, watch online '}}
@endsection

@section('title')
{{$seo->page_title ?? 'Khojapp - Stream online movies on khoj'}}
@endsection

@section('description')
{{$seo->meta_description ?? 'We show you where you can legally watch movies and TV shows that you love. You are kept up to date with what is new on Netflix, Amazon Prime, iTunes and many other streaming platforms.'}}
@endsection

@section('banner')

<style>

.inner {
  padding: 6px;
}

.inner img {
  width: 100%;
  border-radius: 10px;
  height: auto;
}
img:hover {
  opacity: 0.9;
}
</style>
@endsection
@section('content')
<div class="col-sm-2 left-side-menu">
  </div>
  <div class="col-sm-8 col-lg-8 col-xs-12 right-filter">
    <div class="row" style="width: 100%; margin-top: 35px">
      <div class="col-sm-6 col-xs-12">
        
      <h2>Top Playlists for you &nbsp;&nbsp;
        
      </h2>
     
    </div>
    
    </div>

    <div class="movie-content" id="dynamic-search" style="height: 100%; padding-top: 60px; margin-top: 30px;">
      
      @if (isset($playlists))
      <?php $space = 0; $rat=0;?>
      @foreach ($playlists as $element)
      <?php 
      $parent_title = "";

      $movies = explode(',', $element->playlist_content);
      if (count($movies)> 0){
        $first = $movies[0];
      $movie = \DB::table('contents')->where('content_id', $first)->first();
      $poster = "";
      if ($movie){
        $poster = $movie->poster;
      }
      }

      $flag = 1; 
      $space++;
      
      ?>

      <div class="row">
        <?php $urlen = preg_replace('/[^a-zA-Z0-9_.]/', '-', (strtolower($element->playlist_name))).'-'.$element->id; ?>
        <a href="/playlists/details/{{$urlen}}">
          
        <!-- <div class="col-md-3"> @if(isset($poster) && strpos($poster, 'poster/') == false && file_exists(public_path().'/poster/'.$poster))
          <img src="/poster/{{$poster}}" alt="{{$first->playlist_name ?? ''}}" class="poster-image web-image">
          @else
          <img src="/images/default.png" alt="{{$first->playlist_name ?? ''}}" class="web-image">
          @endif
        </div> -->


<div class="col-md-3"> 

  @if(!empty($element->poster)  && $element->poster != '')
          <img src="/poster/{{$element->poster}}" alt="{{$first->playlist_name ?? ''}}" class="poster-image web-image">
          @else
          <img src="/images/default.png" alt="{{$first->playlist_name ?? ''}}" class="web-image">
          @endif
        </div>

        <div class="col-md-9">
          <h3>{{$element->playlist_name}}</h3>
          <br>
          <span>{{$element->description}}</span>
          <br>
          <span>Created By : Khojapp</span>
        </div>
      </div>
        </a>
<?php $flag++; ?>
@endforeach

@endif

@if($flag == 0)
<h4 class="result-sec">No result found</h4>
@endif

</div>
</div>

@endsection 

@section('paginate')


<div class="container" style="text-align: center;">
  <div class="row">
    @if (!is_array($playlists) && $tab != "Select Your favourite" && $playlists->links() != null)
    {{$playlists->links()}}
  @endif
    
  </div>
</div>


@endsection

@section('script')



@endsection
