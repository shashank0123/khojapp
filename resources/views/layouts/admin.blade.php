<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Admin panel for Applications">
    <meta name="author" content="backstage">

    <link rel="shortcut icon" href="/asset/images/logo1.png">

    <title>KHOJ - Admin Dashboard</title>

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Khoj') }}</title>

    <!-- Styles -->
    {{-- <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" /> --}}
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <!--Morris Chart CSS -->
    <link rel="stylesheet" href="{{  asset('plugins/morris/morris.css') }}">

    <!-- App css -->
    <link href="{{  asset('css/bootstrap.min.css')  }}" rel="stylesheet" type="text/css" />
    <link href="{{  asset('css/core.css')  }}" rel="stylesheet" type="text/css" />
    <link href="{{  asset('css/components.css')  }}" rel="stylesheet" type="text/css" />
    <link href="{{  asset('css/icons.css')  }}" rel="stylesheet" type="text/css" />
    <link href="{{  asset('css/pages.css')  }}" rel="stylesheet" type="text/css" />
    <link href="{{  asset('css/menu.css')  }}" rel="stylesheet" type="text/css" />
    <link href="{{  asset('css/responsive.css')  }}" rel="stylesheet" type="text/css" />

    <link href="{{asset('css/chosen.css')}}" rel="stylesheet"> 

    <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->
<style type="text/css">
    .slimScrollBar{
        background: rgb(70, 70, 70) !important;
        width: 8px !important;
    }
</style>

<script src="{{  asset('js/modernizr.min.js') }}"></script>
</head>
<body class="fixed-left">

    <!-- Begin page -->
    <div id="wrapper">

        <!-- Top Bar Start -->
        <div class="topbar">

            <!-- LOGO -->
            <div class="topbar-left">
                <a href="{{url('admin/dashboard')}}" class="logo"><span>KHOJ</span><i class="zmdi zmdi-layers"></i></a>
            </div>

            <!-- Button mobile view to collapse sidebar menu -->
            <div class="navbar navbar-default" role="navigation">
                <div class="container">

                    <!-- Page title -->
                    <ul class="nav navbar-nav navbar-left">
                        <li>
                            <button class="button-menu-mobile open-left">
                                <i class="zmdi zmdi-menu"></i>
                            </button>
                        </li>
                        <li>
                            <h4 class="page-title">Dashboard</h4>
                        </li>
                    </ul>

                    <!-- Right(Notification and Searchbox -->
                    <ul class="nav navbar-nav navbar-right">
                        <li>
                            <!-- Notification -->
                            <div class="notification-box">
                                <ul class="list-inline m-b-0">
                                    <li>
                                        <a href="javascript:void(0);" class="right-bar-toggle">
                                            <i class="zmdi zmdi-notifications-none"></i>
                                        </a>
                                        <div class="noti-dot">
                                            <span class="dot"></span>
                                            <span class="pulse"></span>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <!-- End Notification bar -->
                        </li>
                        <li class="hidden-xs">
                            <form role="search" class="app-search" action="{{url('admin/global_search')}}" method="GET" id="global_search">
                                <input type="text" placeholder="Search..."
                                class="form-control" value="@if(!empty(session()->get('search'))){{session()->get('search')}}@endif" name="keyword">
                                <a onclick="submitForm()"><i class="fa fa-search"></i></a>
                                <button type="submit" name="submit" id="get_search" style="display: none;">Search</button>
                            </form>
                        </li>
                    </ul>

                </div><!-- end container -->
            </div><!-- end navbar -->
        </div>
        <!-- Top Bar End -->


        <!-- ========== Left Sidebar Start ========== -->
        <div class="left side-menu">
            <div class="sidebar-inner slimscrollleft">

                <!-- User -->
                <div class="user-box">
                    <div class="user-img">
                        <img src="{{  asset('images/default_user.png') }}" alt="user-img" title="Mat Helme" class="img-circle img-thumbnail img-responsive">
                        <div class="user-status offline"><i class="zmdi zmdi-dot-circle"></i></div>
                    </div>
                    <h5><a href="#">{{ucfirst(Auth::user()->name)}}</a> </h5>
                    <ul class="list-inline">
                        <li>
                            <a href="#" >
                                <i class="zmdi zmdi-settings"></i>
                            </a>
                        </li>

                        <li>
                            <a href="{{ route('logout') }}"
                            onclick="event.preventDefault();
                            document.getElementById('logout-form1').submit();" class="text-custom">
                            <i class="zmdi zmdi-power"></i>
                        </a>

                        <form id="logout-form1" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form></li> 
                    </li>
                </ul>
            </div>
            <!-- End User -->

            <!--- Sidemenu -->
            <div id="sidebar-menu">
                <ul>
                    <li class="text-muted menu-title">Navigation</li>

                    <li>
                        <a href="{{url('admin/dashboard')}}" class="waves-effect"><i class="zmdi zmdi-view-dashboard"></i> <span> Dashboard </span> </a>
                    </li>

                    <li>
                        <a href="{{url('admin/seo')}}" class="waves-effect"><i class="zmdi zmdi-invert-colors"></i> <span> SEO </span> </a>
                    </li>

                    <li class="has_sub">
                        <a href="javascript:void(0);" class="waves-effect"><i class="zmdi zmdi-invert-colors"></i> <span> Users</span> <span class="menu-arrow"></span></a>
                        <ul class="list-unstyled">
                            <li><a href="{{ url('admin/users')}}">User List</a></li>
                            <!-- <li><a href="{{ url('admin/movies')}}">Movie List</a></li> -->
                            <!-- <li><a href="{{ url('admin/genres')}}">Genres List</a></li> -->
                        </ul>
                    </li>

                    <li class="has_sub">
                        <a href="javascript:void(0);" class="waves-effect"><i class="zmdi zmdi-invert-colors"></i> <span> Movies </span> <span class="menu-arrow"></span></a>
                        <ul class="list-unstyled">
                            <li><a href="{{ url('admin/movies')}}">Movies List</a></li>
                            <li><a href="{{ url('admin/add_content/movie')}}">Add Movie</a></li>
                            
                        </ul>
                    </li>

                    <li class="has_sub">
                        <a href="javascript:void(0);" class="waves-effect"><i class="zmdi zmdi-invert-colors"></i> <span> Playlist </span> <span class="menu-arrow"></span></a>
                        <ul class="list-unstyled">
                            <li><a href="{{ url('admin/public_playlist')}}">Play List</a></li>
                            <li><a href="{{ url('admin/public_playlist/add')}}">Add Playlist</a></li>
                            
                        </ul>
                    </li>

                    <li class="has_sub">
                        <a href="javascript:void(0);" class="waves-effect"><i class="zmdi zmdi-invert-colors"></i> <span> TV Shows </span> <span class="menu-arrow"></span></a>
                        <ul class="list-unstyled">
                            <li><a href="{{ url('admin/tv-shows')}}">Tv Shows List</a></li>
                            <li><a href="{{ url('admin/add_content/show')}}">Add TV Show</a></li>
                            
                        </ul>
                    </li>

                    <li class="has_sub">
                        <a href="javascript:void(0);" class="waves-effect"><i class="zmdi zmdi-invert-colors"></i> <span> Viral Videos </span> <span class="menu-arrow"></span></a>
                        <ul class="list-unstyled">
                            <li><a href="{{ url('admin/viral-videos')}}">Viral Videos List</a></li>
                            <li><a href="{{ url('admin/add_content/viral')}}">Add Viral Video</a></li>
                            
                        </ul>
                    </li>

                    <li class="has_sub">
                        <a href="javascript:void(0);" class="waves-effect"><i class="zmdi zmdi-invert-colors"></i> <span> Free Movies </span> <span class="menu-arrow"></span></a>
                        <ul class="list-unstyled">
                            <li><a href="{{ url('admin/free-movies')}}">Free Movies List</a></li>
                            <li><a href="{{ url('admin/movie/free')}}">Add Free Movie</a></li>
                            
                        </ul>
                    </li>


                    <li class="has_sub">
                        <a href="javascript:void(0);" class="waves-effect"><i class="zmdi zmdi-invert-colors"></i> <span> Slider </span> <span class="menu-arrow"></span></a>
                        <ul class="list-unstyled">
                            <li><a href="{{ url('admin/sliders')}}">Sliders List</a></li>
                            <li><a href="{{ url('admin/add_slider')}}">Add Slider</a></li>
                            
                        </ul>
                    </li>

                    <li>
                        <a href="{{url('admin/ott')}}" class="waves-effect"><i class="zmdi zmdi-invert-colors"></i> <span> OTT </span> </a>
                    </li>

                    <li>
                        <a href="{{url('admin/languages')}}" class="waves-effect"><i class="zmdi zmdi-invert-colors"></i> <span> Language </span> </a>
                    </li>


                    <li class="has_sub">
                        <a href="javascript:void(0);" class="waves-effect"><i class="zmdi zmdi-invert-colors"></i> <span> General Setting </span> <span class="menu-arrow"></span></a>
                        <ul class="list-unstyled">
                            <li><a href="{{ url('admin/setting/website')}}">Website</a></li>
                            <li><a href="{{ url('admin/setting/google')}}">Google</a></li>
                            <li><a href="{{ url('admin/setting/facebook')}}">Facebook</a></li>
                        </ul>
                    </li>

                        <!-- <li class="has_sub">
                            <a href="javascript:void(0);" class="waves-effect"><i class="zmdi zmdi-collection-text"></i><span class="label label-warning pull-right"></span><span> Country </span> </a>
                            <ul class="list-unstyled">
                                <li><a href="{{ url('admin/country')}}">Country List</a></li>
                                <li><a href="{{ url('admin/country/create')}}">Add Country</a></li>
                            </ul>
                        </li>   --> 
                        
                        <li><a href="{{ route('logout') }}"
                            onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">
                            Logout
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form></li>                       
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <!-- Sidebar -->
                <div class="clearfix"></div>

            </div>

        </div>
        <!-- Left Sidebar End -->



        <!-- ============================================================== -->
        <!-- Start right Content here -->
        <!-- ============================================================== -->
        <div class="content-page">
            <!-- Start content -->
            <div class="content">
                <div class="container">
                   @yield('content')

               </div> <!-- container -->

           </div> <!-- content -->

           <footer class="footer text-right">
            2018 - 2019 © Backstage.
        </footer>

    </div>
    <!-- ============================================================== -->
    <!-- End Right content here -->
    <!-- ============================================================== -->


    <!-- Right Sidebar -->
    <div class="side-bar right-bar">
        <a href="javascript:void(0);" class="right-bar-toggle">
            <i class="zmdi zmdi-close-circle-o"></i>
        </a>
        <h4 class="">Notifications</h4>
        <div class="notification-list nicescroll">
            <ul class="list-group list-no-border user-list">
                <li class="list-group-item">
                    <a href="#" class="user-list-item">
                        <div class="avatar">
                            <img src="{{  asset('images/users/avatar-2.jpg') }}" alt="">
                        </div>
                        <div class="user-desc">
                            <span class="name">Michael Zenaty</span>
                            <span class="desc">There are new settings available</span>
                            <span class="time">2 hours ago</span>
                        </div>
                    </a>
                </li>
                <li class="list-group-item">
                    <a href="#" class="user-list-item">
                        <div class="icon bg-info">
                            <i class="zmdi zmdi-account"></i>
                        </div>
                        <div class="user-desc">
                            <span class="name">New Signup</span>
                            <span class="desc">There are new settings available</span>
                            <span class="time">5 hours ago</span>
                        </div>
                    </a>
                </li>
                <li class="list-group-item">
                    <a href="#" class="user-list-item">
                        <div class="icon bg-pink">
                            <i class="zmdi zmdi-comment"></i>
                        </div>
                        <div class="user-desc">
                            <span class="name">New Message received</span>
                            <span class="desc">There are new settings available</span>
                            <span class="time">1 day ago</span>
                        </div>
                    </a>
                </li>
                <li class="list-group-item active">
                    <a href="#" class="user-list-item">
                        <div class="avatar">
                            <img src="{{  asset('images/users/avatar-3.jpg') }}" alt="">
                        </div>
                        <div class="user-desc">
                            <span class="name">James Anderson</span>
                            <span class="desc">There are new settings available</span>
                            <span class="time">2 days ago</span>
                        </div>
                    </a>
                </li>
                <li class="list-group-item active">
                    <a href="#" class="user-list-item">
                        <div class="icon bg-warning">
                            <i class="zmdi zmdi-settings"></i>
                        </div>
                        <div class="user-desc">
                            <span class="name">Settings</span>
                            <span class="desc">There are new settings available</span>
                            <span class="time">1 day ago</span>
                        </div>
                    </a>
                </li>

            </ul>
        </div>
    </div>
    <!-- /Right-bar -->

</div>
<!-- END wrapper -->



<script>
    var resizefunc = [];
</script>

<!-- jQuery  -->
<script src="{{ asset('js/jquery.min.js') }}"></script>
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/detect.js') }}"></script>
<script src="{{ asset('js/fastclick.js') }}"></script>
<script src="{{ asset('js/jquery.slimscroll.js') }}"></script>
<script src="{{ asset('js/jquery.blockUI.js') }}"></script>
<script src="{{ asset('js/waves.js') }}"></script>
<script src="{{ asset('js/jquery.nicescroll.js') }}"></script>
<script src="{{ asset('js/jquery.slimscroll.js') }}"></script>
<script src="{{ asset('js/jquery.scrollTo.min.js') }}"></script>

<!-- KNOB JS -->
        <!--[if IE]>
        <script type="text/javascript" src="assets/plugins/jquery-knob/excanvas.js"></script>
    <![endif]-->
    <script src="{{ asset('plugins/jquery-knob/jquery.knob.js')}}"></script>

    <!--Morris Chart-->
    <script src="{{ asset('plugins/morris/morris.min.js') }}"></script>
    <script src="{{ asset('plugins/raphael/raphael-min.js') }}"></script>

    <!-- Dashboard init -->
    <script src="{{ asset('pages/jquery.dashboard.js') }}"></script>

    <!-- App js -->
    <script src="{{ asset('js/jquery.core.js') }}"></script>
    <script src="{{ asset('js/jquery.app.js') }}"></script>

    <script type="text/javascript">
        function getPriority(id){
            var priority = $('#priority'+id).val()

            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

            $.ajax({
               url: '/admin/set_priority',
               type: 'POST',
               data: {_token: CSRF_TOKEN, provider_id: id, display_priority: priority },
               success: function (data) {
                 window.location.href = '/admin/ott';
             },
             failure: function (data) {
                 alert('Something Went Wrong');
             }
         });
        }

        function getLanguagePriority(id){
            var priority = $('#language'+id).val()

            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

            $.ajax({
               url: '/admin/language_priority',
               type: 'POST',
               data: {_token: CSRF_TOKEN, id: id, display_priority: priority },
               success: function (data) {
                 window.location.href = '/admin/languages';
             },
             failure: function (data) {
                 alert('Something Went Wrong');
             }
         });
        }






        function submitForm(){
            $('#get_search').click();
        }
    </script>

</body>

</html>
