<!DOCTYPE html>

<html lang="en" class="no-js">

<head>
  <!-- Basic need -->
  <title>KHOJ</title>
  <meta charset="UTF-8">
  <meta name="description" content="">
  <meta name="keywords" content="">
  <meta name="author" content="">
  <link rel="profile" href="#">
  <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" media="all">
  <link rel="icon" href="/asset/images/logo1.png" type="image/x-icon"/>

  <meta name="csrf-token" content="{{ csrf_token() }}">

  <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" media="all">
  <link href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css" rel="stylesheet" media="all">
  <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">

  <!-- Bootstrap bootstrap-touch-slider Slider Main Style Sheet -->
  <link href="http://bootstrapthemes.co/demo/resource/BootstrapCarouselTouchSlider/bootstrap-touch-slider.css" rel="stylesheet" media="all">
  <!--Google Font-->
  <link rel="stylesheet" href='http://fonts.googleapis.com/css?family=Dosis:400,700,500|Nunito:300,400,600' />
  <!-- Mobile specific meta -->
  <meta name=viewport content="width=device-width, initial-scale=1">
  <meta name="format-detection" content="telephone-no">

  <!-- CSS files -->
  <link rel="stylesheet" href="/asset/css/plugins.css">
  <link rel="stylesheet" href="/asset/css/style.css">

  <link rel="stylesheet" href="https://unpkg.com/swiper/css/swiper.css">
  <link rel="stylesheet" href="https://unpkg.com/swiper/css/swiper.min.css">

  <style type="text/css">
    .row { margin-left: 0 !important; margin-right: 0!important }
  </style>

</head>
<body onload="hideSearch()">
  <!--preloading-->
  <div id="preloader">
    <img class="logo" src="/asset/images/logo1.png" alt="" width="119" height="58">
    <div id="status">
      <span></span>
      <span></span>
    </div>
  </div>
  <!--end of preloading-->
  <!--login form popup-->
  {{-- <div class="login-wrapper" id="login-content">
    <div class="login-content">
      <a href="#" class="close">x</a>
      <h3>Login</h3>
      <form method="post" >
        <div class="row">
         <label for="username">
          Username:
          <input type="text" name="username" id="username" placeholder="Username"  required="required" />
        </label>
      </div>

      <div class="row">
        <label for="password">
          Password:
          <input type="password" name="password" id="password" placeholder="******"  required="required" />
        </label>
      </div>
      <div class="row">
        <div class="remember">
          <div>
            <input type="checkbox" name="remember" value="Remember me"><span>Remember me</span>
          </div>
          <a href="#">Forget password ?</a>
        </div>
      </div>
      <div class="row">
       <button type="submit">Login</button>
     </div>
   </form>
   <div class="row">
    <p>Or via social</p>
    <div class="social-btn-2">
      <a class="fb" href="#"><i class="ion-social-facebook"></i>Facebook</a>
      <a class="tw" href="#"><i class="ion-social-googleplus"></i>Google</a>
    </div>
  </div>
</div>
</div> --}}
<!--end of login form popup-->
<!--signup form popup-->
{{-- <div class="login-wrapper"  id="signup-content">
  <div class="login-content">
    <a href="#" class="close">x</a>
    <h3>sign up</h3>
    <form method="post" >
      <div class="row">
       <label for="username-2">
        Username:
        <input type="text" name="username" id="username-2" placeholder="Enter  Username:"  required="required" />
      </label>
    </div>

    <div class="row">
      <label for="email-2">
        your email:
        <input type="password" name="email" id="email-2" placeholder="your email:" required="required" />
      </label>
    </div>
    <div class="row">
      <label for="password-2">
        Password:
        <input type="password" name="password" id="password-2" placeholder=""  required="required" />
      </label>
    </div>
    <div class="row">
      <label for="repassword-2">
        re-type Password:
        <input type="password" name="password" id="repassword-2" placeholder=""  required="required" />
      </label>
    </div>
    <div class="row">
     <button type="submit">sign up</button>
   </div>
 </form>
</div>
</div> --}}
<!--end of signup form popup-->

<!-- Modal -->
<div class="modal fade" id="LoginModal" tabindex="-1" role="dialog" aria-labelledby="LoginModal" aria-hidden="true">
 <button type="button" class="close float-close-pro" data-dismiss="modal" aria-label="Close">
   <span aria-hidden="true">&times;</span>
 </button>
 <div class="modal-dialog modal-dialog-centered modal-md" role="document">
  <div class="modal-content">
   <div class="modal-header-pro">
     <h2>Welcome Back</h2>
     <h6>Sign in to your account to continue using SKRN</h6>
   </div>
   <div class="modal-body-pro social-login-modal-body-pro">
     <div class="row">
       <div class="col-sm-8">
        <div class="registration-social-login-container">
         <form method="POST" id="login-form" action="{{ route('login') }}">
          @csrf
          <div class="form-group">
           <input type="text" name="email" class="form-control" id="username" placeholder="Email">
           <h6 id="username-result"></h6>
         </div>
         <div class="form-group">
           <input type="password" name="password" class="form-control" id="password" placeholder="Password">
           <h6 id="password-result"></h6>
         </div>
         <div class="form-group">
           <input type="button" name="submit" value="Sign In" class="btn btn-success" id="login-submit" onclick="checkLogin()">
           <input type="submit" name="submit" value="Sign In" class="btn btn-success" id="login-submit2" style="display: none;">
         </div>
         <div class="container-fluid">
           <div class="row no-gutters">
            <div class="col checkbox-remember-pro"><input type="checkbox" id="checkbox-remember"><label for="checkbox-remember" class="col-form-label">Remember me</label></div>
            <div class="col forgot-your-password"><a href="{{ route('password.request') }}">Forgot your password?</a></div>
          </div>
        </div><!-- close .container-fluid -->

      </form>

      <div class="registration-social-login-or">or</div>

    </div><!-- close .registration-social-login-container -->
  </div>
  <div class="col-sm-4">
   <div class="registration-social-login-options">
    <h6>Sign in with your social account</h6>
    <div class="social-icon-login facebook-color"><a href="{{ url('/auth/facebook') }}" style="color: #fff"><i class="fab fa-facebook-f"></i> Facebook</a></div>
    {{-- <div class="social-icon-login twitter-color"><a href="#"><i class="fab fa-twitter"></i> Twitter</a></div> --}}
    <div class="social-icon-login google-color"><a href="{{ url('/auth/google') }}" style="color: #fff"><i class="fab fa-google-plus-g"></i> Google</a></div>
  </div><!-- close .registration-social-login-options -->
</div>
</div> 

<div class="clearfix"></div>

</div><!-- close .modal-body -->

<a class="not-a-member-pro" href="register">Not a member? <span>Join Today!</span></a>
</div><!-- close .modal-content -->
</div><!-- close .modal-dialog -->
</div><!-- close .modal --> 

<!-- BEGIN | Header -->
<!-- BEGIN | Header -->
<header class="ht-header full-width-hd">
  <div class="row">
    <nav id="mainNav" class="navbar navbar-default navbar-custom">
      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header logo">
        <div class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
          <span class="sr-only">Toggle navigation</span>
          <div id="nav-icon1" onclick="showNav()">
            <span></span>
            <span></span>
            <span></span>
          </div>
        </div>
        
        <a href="/"><img class="logo" src="/asset/images/logo1.png" alt="" width="119" height="58"></a>
      </div>
      <!-- Collect the nav links, forms, and other content for toggling -->
      <div class="collapse navbar-collapse flex-parent" id="bs-example-navbar-collapse-1">
        <div class="show-nav">
          <ul>
            @if(isset(auth()->user()->id))
            <li><a href="/profile"><span class="icon-User"></span>My Profile</a></li>
            <li><a href="/favourites/{{Auth::user()->id}}"><span class="icon-Favorite-Window"></span>My Favorites</a></li>
            <li><a href="{{ route('logout') }}"
              onclick="event.preventDefault();
              document.getElementById('logout-form').submit();">
              Logout
            </a>

            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
              {{ csrf_field() }}
            </form></li>
            @else
            <li> <a href="{{url('about-us')}}">About</a></li>
            <li> <a href="{{url('contact-us')}}">Contact</a></li>
            <li> <a href="{{url('privacy-policy')}}">Privacy Policy</a></li>
            <li> <a href="{{url('frequently-asked-questions')}}">FAQ</a></li>
            <li> <a href="/login">Sign In</a></li>
            <li> <a href="/register">Register</a></li>
            <li class="no-show"> <a href="{{url('free')}}">Free</a></li>
            @endif
          </ul>
        </div>

        <ul class="nav navbar-nav flex-child-menu menu-left" id="full-div">
          <li class="hidden">
            <a href="#page-top"></a>
          </li>

          <form action="/search" autocomplete="on" method="GET" style="width: 80%;" id="window-form">
            <div class="row">
              <div class="col-sm-2">
                <select class="form-control" name="searchtype" id="searchtype" style="width: 98%">
                  <option value="movie" @if($type == 'movie'){{'selected'}}@endif>Movies</option>
                  <option value="tv" @if($type == 'tv'){{'selected'}}@endif>Tv Serials</option>
                  <option value="web" @if($type == 'web'){{'selected'}}@endif>Web Series</option>
                </select>
              </div>
              <div class="col-sm-8">
                <input type="search" name="search" id="search1" placeholder="Search for movies, web series, TV series etc" class="form-control" style="width: 100%" onkeyup="getResult()">

                <div style="position: absolute; z-index: 999; width: 100%; margin:0%; background-color: #000;border: 1px solid #000; box-shadow: 0 15px 20px rgba(0, 0, 0, 1); max-height: 400px; overflow-y: auto" id="dynamic-search-result">

                </div>
              </div>
              <div class="col-sm-1">
                <img src="/asset/images/search.png" class="search-dialog form-control" onclick="callSubmit()">
              </div>
              <div class="col-sm-1">
                <!-- <img src="/asset/images/reset1.png" class="reset-dialog form-control" onclick="getReset()"> -->
              </div>
            </div>            
            <input id="search_submit" value="Rechercher" type="submit" style="display: none;" >
          </form>
        </ul>
        
        <ul class="nav navbar-nav flex-child-menu menu-right" id="full-div">

           <?php if (isset(auth()->user()->profile_pic))
           $profile_pic = '/images/user/'.auth()->user()->profile_pic;
           else {
            $profile_pic = '/images/default_user.png';
          }
          ?>                
          @guest
          <a href="/register" class="registerbutton">Register</a>

          <a class="loginbutton" style="color: #fff" data-toggle="modal" data-target="#LoginModal" role="button">Sign In</a>
          @else

          <div class="dropdown">
            <span>  <div class="row" style="text-align: right;">
              <div class="col-sm-4">
               <img src="{{$profile_pic}}" alt="{{ Auth::user()->name }}" id="profile-image">
             </div>
             <div class="col-sm-8">
              <div style="padding-top: 7px">
               <span id="username-header">
                {{ Auth::user()->name }} <i class="fa fa-angle-down" aria-hidden="true"></i></span>
              </div>
            </div>
          </div>


          <!-- close #header-user-profile-click --></span>
          <div class="dropdown-content" style="z-index: 999 !important">
            <ul class="dropdown" id="profile-option">  
              <li><a href="/profile"><span class="icon-User"></span>My Profile</a></li>
              <li><a href="/favourites/{{Auth::user()->id}}"><span class="icon-Favorite-Window"></span>My Favorites</a></li>
              {{-- <li><a href="/account"><span class="icon-Gears"></span>Account Details</a></li> --}}
              {{-- <li><a href="#!"><span class="icon-Life-Safer"></span>Help/Support</a></li> --}}
              <li><a href="{{ route('logout') }}"
                onclick="event.preventDefault();
                document.getElementById('logout-form').submit();">
                Logout
              </a>

              <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
              </form></li>
            </ul>
          </div>
        </div>

      </ul>
    </div>
    <!-- /.navbar-collapse -->
  </nav>

</div><!-- close #header-user-profile-menu -->
</div><!-- close #header-user-profile -->

@endguest
<!-- search form -->

</div>

</header>

<div class="container-fluid response-search">
  <form action="/search" autocomplete="on" method="GET" style="width: 90%; margin: auto" id="mobile-form">
    <div class="row">
      <div class="col-xs-3">
        <select class="form-control" name="searchtype" id="searchtype2" style="width: 98%">
          <option value="movie" @if($type == 'Movie'){{'selected'}}@endif>Movies</option>
          <option value="tv" @if($type == 'TV Serials'){{'selected'}}@endif>Tv Serials</option>
          <option value="web" @if($type == 'Web Series'){{'selected'}}@endif>Web Series</option>
        </select>
      </div>
      <div class="col-xs-6">
       <input type="search" name="search" id="search2" placeholder="Search for movies, web series, TV series etc" class="form-control" class="search-bar" onkeyup="getResult2()">

       <div style="position: absolute; z-index: 999; width: 180%; left: -80px; margin:0%; background-color: #000;border: 1px solid #000; box-shadow: 0 15px 20px rgba(0, 0, 0, 1); height: auto" id="dynamic-search-result2">

       </div>

     </div>
     <div class="col-xs-3">
      <img src="/asset/images/search.png" class="reset-dialog form-group" onclick="callSubmit2()">
      <!-- <img src="/asset/images/reset1.png" class="reset-dialog form-group" onclick="getReset()"> -->
    </div>
    
  </div>


  <input id="search_submit2" value="Rechercher" type="submit" style="display: none;" >
</form>
</div>
<div class="container-fluid bottom-nav" >
  <div class="row">
    <div class="col-sm-8">
      <ul>
        <li> <a href="/">Home</li>
          <li> <a href="/trending">Trending</a></li>
          <li> <a href="/upcoming">Upcoming</a></li>
          <li> <a href="@if(isset(auth()->user()->id)){{'/watchlists/'}}{{auth()->user()->id}}@else{{url('/login')}} @endif">Watchlist</a></li>
          <li class="no-show"> <a href="/new-release">New Release</a></li>
          <li class="no-show"> <a href="{{url('free')}}">Free</a></li>
          <li class="to-show" onclick="showFilter2()"> <a style="cursor: pointer;"><img src="{{asset('asset/images/filter.png')}}" style="width: 20px ; height: 20px" title="filter"/> </a></li>
        </ul>
      </div>
      <div class="col-sm-2">
        <select name="lang" id="lang" class="form-sort" onchange="getLang()">
          <option value="Holywood">Holywood</option>
          <option value="Bollywood">Bollywood</option>          
        </select>
      </div>

      <div class="col-sm-2">
        <select name="sort" id="sort" class="form-sort" onchange="getSort()">
          <!-- <option value="popularity.desc">Popularity</option> -->
          <option value="random">Random</option>
          <option value="original_title.asc">Alphabetical</option>
          <option value="vote_average.desc">Rating (DESC)</option>
          <option value="primary_release_date.desc">Release Year (DESC)</option>
          {{-- <option value="year">IMDB Score</option> --}}
        </select>
      </div>
    </div>    
  </div>


  @yield('banner')

  <!-- End  bootstrap-touch-slider Slider -->
  <!-- END | Header -->

  <div class="movie-items full-width main-movie container-fluid">
    <div class="row" id="main-content" style="margin-left: 0!important; margin-right: 0!important">
      <div class="col-md-12 left-filters">


        @yield('content')


    @yield('paginate')
      </div>


     
     

    </div>


  </div>
  
  <!-- footer v2 section-->
  <footer class="ht-footer full-width-ft">

    <div class="row">
      <div class="flex-parent-ft">
        <div class="flex-child-ft item1">
         <a href="/"><img class="logo" src="/asset/images/logo1.png" alt="" style="margin-bottom: 20px"></a>
         <h4><u>Address</u></h4>
         <p>E-399,<br>
           Swami Vivekanand Marg,<br>
           East Babarpur,<br>
         New Delhi-110032</p>
         {{-- <h4><u>Call us</u></h4> --}}
         <p><u style="font-size: 16px; font-weight: bold;">Call us</u>: <a href="tel:9322854558" style="font-size: 14px">(+91) 9322854558</a></p>
         <p><u style="font-size: 16px; font-weight: bold;">Email us</u>: <a href="mailto:pankaj@digichoice.in" style="font-size: 14px">pankaj@digichoice.in</a></p>
       </div>
       <div class="flex-child-ft item2">
        <h4>Resources</h4>
        <ul>
          <li><a href="{{url('/')}}">Home</a></li> 
          <li><a href="{{url('about-us')}}">About</a></li> 
          <li><a href="{{url('contact-us')}}">Contact Us</a></li>
          
        </ul>
      </div>
      <div class="flex-child-ft item3">
        <h4>Legal</h4>
        <ul>
          {{-- <li><a href="#">Terms of Use</a></li>  --}}
          <li><a href="{{url('privacy-policy')}}">Privacy Policy</a></li> 
          <li><a href="{{url('frequently-asked-questions')}}">FAQ</a></li>
        </ul>
      </div>
      <div class="flex-child-ft item4">
        <h4>Account</h4>
        <ul>
          <li><a href="@if(isset(auth()->user()->id)){{url('profile')}}@else{{url('login')}}@endif">My Account</a></li> 
          <li><a href="@if(isset(auth()->user()->id)){{url('watchlists')}}/{{auth()->user()->id}}@else{{url('login')}}@endif">Watchlist</a></li>  
          {{-- <li><a href="#">Collections</a></li>
          <li><a href="#">User Guide</a></li> --}}
        </ul>
      </div>
      <div class="flex-child-ft item5">
        <h4>Newsletter</h4>
        <p>Subscribe to our newsletter system now <br> to get latest news from us.</p>
        
        <input type="email" placeholder="Enter your email" name="newsletter" id="newsletter" required style="border-radius: 3px">
        
        <a style="cursor: pointer;" class="btn" onclick="clickNewsSubmit()">Subscribe now <i class="ion-ios-arrow-forward"></i></a>
        <p id="result" style="text-align: center;color: #fff; margin-top: 10px;"></p>
      </div>
    </div>
    <div class="ft-copyright">
      <div class="ft-left">
        <p>© 2019 Blockbuster. All Rights Reserved. Designed by Backstage Supporters.</p>
      </div>
      <div class="backtotop">
        <p><a href="#" id="back-to-top">Back to top  <i class="ion-ios-arrow-thin-up"></i></a></p>
      </div>
    </div>
  </div>
</footer>
<!-- end of footer v2 section-->


<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.touchswipe/1.6.18/jquery.touchSwipe.min.js"></script>
<!-- Bootstrap bootstrap-touch-slider Slider Main JS File -->
<script src="http://bootstrapthemes.co/demo/resource/BootstrapCarouselTouchSlider/bootstrap-touch-slider.js"></script>
<script type="text/javascript">
  $('#bootstrap-touch-slider').bsTouchSlider();
</script>
<script src="https://unpkg.com/swiper/js/swiper.js"></script>
<script src="https://unpkg.com/swiper/js/swiper.min.js"></script>
<script src="/asset/js/jquery.js"></script>
<script src="/asset/js/plugins.js"></script>
<script src="/asset/js/plugins2.js"></script>
<script src="/asset/js/custom.js"></script>
<style>


</style>
<script type="text/javascript">

  function getSort()
  {
    var data = $('#sort').val();
    $('#sort_filter').val(data);
    search();
  }

function getLang()
  {
    var data = $('#lang').val();
    $('#lang_filter').val(data);
    search();
  }

  function viewInSearch(id){
    data = $('#cast'+id).text(); 
    $('#search1').val(data);
    $('#search2').val(data);
    $('#search_submit').click();    
  }

  function getSearchPage(){
    $('#search_submit').click();    
  }

  function getSearchPage2(){
    $('#search_submit2').click();    
  }

  function getResult(){
    var data = $('#search1').val();
    var search = $('#searchtype').val();
    var CSRF_Token = $('meta[name="csrf-token"]').attr('content'); 
    if (data != "")
    $.ajax({
      type: "POST",
      url: "/dynamic-search",
      data:{ _token: CSRF_Token, data: data, search: search},
      success:function(msg){
        $('#dynamic-search-result').show();
        $('#dynamic-search-result').html(msg);  
      }
    });

  }

 

  function getResult2(){
    var data = $('#search2').val();
    var search = $('#searchtype2').val();
    var CSRF_Token = $('meta[name="csrf-token"]').attr('content'); 

    $.ajax({
      type: "POST",
      url: "/dynamic-search",
      data:{ _token: CSRF_Token, data: data, search: search},
      success:function(msg){
        $('#dynamic-search-result2').html(msg);  
      }
    });
  }

  function callSubmit(){
    var search = $('#search1').val();
    if(search == ""){
      alert('First enter your search');
    }
    else{
      $('#search_submit').click();
    }
  }

  function callSubmit2(){
    var search = $('#search2').val();
    if(search == ""){
      alert('First enter your search');
    }
    else{
      $('#search_submit2').click();
    }
  }

  function showNav(){
    $('.navbar-collapse').toggle();
  }

  function clickNewsSubmit(){
    var news = $('#newsletter').val();    
    if(news == ""){
      $('#result').text('First enter your email');
    }
    else{
      var CSRF_Token = $('meta[name="csrf-token"]').attr('content');
      $('#result').empty();
      $.ajax({              
        url: '/submit-newsletter',
        type: 'POST',             
        data: { _token: CSRF_Token, email : news},
        success: function (data) { 
          $('#result').text(data.message);            
        },
      });
    }
  }

  function showFilter2(){
    $('.left-side-menu').toggle();
  }

  function checkLogin(){
    var uname = $('#username').val();
    var pass = $('#password').val();
    if(uname == ''){
      $('#username-result').text('*First Enter user email');
    }
    else{
      $('#username-result').empty();

      if(pass == ''){
        $('#password-result').text('*Enter your password');
      }
      else{
        $('#password-result').empty();
        $('#login-submit2').click();
      }
    }
  }



function getReset(){
document.getElementById("window-form").reset();
document.getElementById("mobile-form").reset();
  // $('#search1').reset();
  // $('#search2').reset();
  $('#dynamic-search-result').hide();
  $('#dynamic-search-result2').hide();
 
}


</script>

@yield('script')
</body>

</html>