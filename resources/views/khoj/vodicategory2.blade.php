<html lang="en-US">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<link rel="pingback" href="https://demo3.madrasthemes.com/vodi-demos/main/xmlrpc.php">
	<link rel="stylesheet" id="wp-block-library-css" href="https://demo3.madrasthemes.com/vodi-demos/main/wp-includes/css/dist/block-library/style.min.css?ver=5.4.4" type="text/css" media="all">
	<style id="wp-block-library-inline-css" type="text/css">
		.has-text-align-justify{text-align:justify;}
	</style>
	<link rel="stylesheet" id="wp-block-library-theme-css" href="https://demo3.madrasthemes.com/vodi-demos/main/wp-includes/css/dist/block-library/theme.min.css?ver=5.4.4" type="text/css" media="all">
	<link rel="stylesheet" id="vodi-comingsoon-launch-section-css" href="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/plugins/vodi-extensions/assets/css/gutenberg-blocks/comingsoon-launch-section/style.min.css?ver=1570784057" type="text/css" media="all">
	<link rel="stylesheet" id="vodi-landing-viewcounts-section-css" href="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/plugins/vodi-extensions/assets/css/gutenberg-blocks/landing-viewcounts-section/style.min.css?ver=1570784057" type="text/css" media="all">
	<link rel="stylesheet" id="vodi-faq-section-css" href="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/plugins/vodi-extensions/assets/css/gutenberg-blocks/faq-section/style.min.css?ver=1570784057" type="text/css" media="all">
	<link rel="stylesheet" id="vodi-landing-features-list-css" href="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/plugins/vodi-extensions/assets/css/gutenberg-blocks/landing-features-list/style.min.css?ver=1570784057" type="text/css" media="all">
	<link rel="stylesheet" id="vodi-landing-featured-video-css" href="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/plugins/vodi-extensions/assets/css/gutenberg-blocks/landing-featured-video/style.min.css?ver=1570784057" type="text/css" media="all">
	<link rel="stylesheet" id="vodi-landing-tabs-features-css" href="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/plugins/vodi-extensions/assets/css/gutenberg-blocks/landing-tabs-features/style.min.css?ver=1570784057" type="text/css" media="all">
	<link rel="stylesheet" id="vodi-landing-movies-carousel-css" href="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/plugins/vodi-extensions/assets/css/gutenberg-blocks/landing-movies-carousel/style.min.css?ver=1570784057" type="text/css" media="all">
	<link rel="stylesheet" id="vodi-landing-featured-section-css" href="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/plugins/vodi-extensions/assets/css/gutenberg-blocks/landing-featured-section/style.min.css?ver=1570784057" type="text/css" media="all">
	<link rel="stylesheet" id="vodi-landing-hero-banner-css" href="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/plugins/vodi-extensions/assets/css/gutenberg-blocks/landing-hero-banner/style.min.css?ver=1570784057" type="text/css" media="all">
	<link rel="stylesheet" id="vodi-section-full-width-banner-css" href="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/plugins/vodi-extensions/assets/css/gutenberg-blocks/section-full-width-banner/style.min.css?ver=1570784057" type="text/css" media="all">
	<link rel="stylesheet" id="vodi-video-section-css" href="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/plugins/vodi-extensions/assets/css/gutenberg-blocks/video-section/style.min.css?ver=1570784057" type="text/css" media="all">
	<link rel="stylesheet" id="vodi-tv-show-section-css" href="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/plugins/vodi-extensions/assets/css/gutenberg-blocks/tv-show-section/style.min.css?ver=1570784057" type="text/css" media="all">
	<link rel="stylesheet" id="vodi-hot-premieres-block-css" href="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/plugins/vodi-extensions/assets/css/gutenberg-blocks/hot-premieres-block/style.min.css?ver=1570784057" type="text/css" media="all">
	<link rel="stylesheet" id="vodi-movie-section-aside-header-css" href="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/plugins/vodi-extensions/assets/css/gutenberg-blocks/movie-section-aside-header/style.min.css?ver=1570784057" type="text/css" media="all">
	<link rel="stylesheet" id="vodi-video-section-aside-header-css" href="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/plugins/vodi-extensions/assets/css/gutenberg-blocks/video-section-aside-header/style.min.css?ver=1570784057" type="text/css" media="all">
	<link rel="stylesheet" id="vodi-section-movies-carousel-aside-header-css" href="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/plugins/vodi-extensions/assets/css/gutenberg-blocks/section-movies-carousel-aside-header/style.min.css?ver=1570784057" type="text/css" media="all">
	<link rel="stylesheet" id="vodi-section-videos-carousel-aside-header-css" href="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/plugins/vodi-extensions/assets/css/gutenberg-blocks/section-videos-carousel-aside-header/style.min.css?ver=1570784057" type="text/css" media="all">
	<link rel="stylesheet" id="vodi-section-movies-carousel-nav-header-css" href="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/plugins/vodi-extensions/assets/css/gutenberg-blocks/section-movies-carousel-nav-header/style.min.css?ver=1570784057" type="text/css" media="all">
	<link rel="stylesheet" id="vodi-section-videos-carousel-nav-header-css" href="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/plugins/vodi-extensions/assets/css/gutenberg-blocks/section-videos-carousel-nav-header/style.min.css?ver=1570784057" type="text/css" media="all">
	<link rel="stylesheet" id="vodi-section-movies-carousel-flex-header-css" href="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/plugins/vodi-extensions/assets/css/gutenberg-blocks/section-movies-carousel-flex-header/style.min.css?ver=1570784057" type="text/css" media="all">
	<link rel="stylesheet" id="vodi-section-videos-carousel-flex-header-css" href="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/plugins/vodi-extensions/assets/css/gutenberg-blocks/section-videos-carousel-flex-header/style.min.css?ver=1570784057" type="text/css" media="all">
	<link rel="stylesheet" id="section-featured-post-css" href="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/plugins/vodi-extensions/assets/css/gutenberg-blocks/section-featured-post/style.min.css?ver=1570784057" type="text/css" media="all">
	<link rel="stylesheet" id="videos-with-featured-video-css" href="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/plugins/vodi-extensions/assets/css/gutenberg-blocks/videos-with-featured-video/style.min.css?ver=1570784057" type="text/css" media="all">
	<link rel="stylesheet" id="vodi-featured-movies-carousel-css" href="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/plugins/vodi-extensions/assets/css/gutenberg-blocks/featured-movies-carousel/style.min.css?ver=1570784057" type="text/css" media="all">
	<link rel="stylesheet" id="vodi-section-featured-movie-css" href="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/plugins/vodi-extensions/assets/css/gutenberg-blocks/section-featured-movie/style.min.css?ver=1570784057" type="text/css" media="all">
	<link rel="stylesheet" id="vodi-section-featured-tv-show-css" href="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/plugins/vodi-extensions/assets/css/gutenberg-blocks/section-featured-tv-show/style.min.css?ver=1570784057" type="text/css" media="all">
	<link rel="stylesheet" id="vodi-banner-with-section-tv-shows-css" href="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/plugins/vodi-extensions/assets/css/gutenberg-blocks/banner-with-section-tv-shows/style.min.css?ver=1570784057" type="text/css" media="all">
	<link rel="stylesheet" id="vodi-blog-list-section-css" href="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/plugins/vodi-extensions/assets/css/gutenberg-blocks/blog-list-section/style.min.css?ver=1570784057" type="text/css" media="all">
	<link rel="stylesheet" id="vodi-blog-grid-section-css" href="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/plugins/vodi-extensions/assets/css/gutenberg-blocks/blog-grid-section/style.min.css?ver=1570784057" type="text/css" media="all">
	<link rel="stylesheet" id="vodi-blog-tab-section-css" href="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/plugins/vodi-extensions/assets/css/gutenberg-blocks/blog-tab-section/style.min.css?ver=1570784057" type="text/css" media="all">
	<link rel="stylesheet" id="vodi-slider-movies-css" href="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/plugins/vodi-extensions/assets/css/gutenberg-blocks/slider-movies/style.min.css?ver=1570784057" type="text/css" media="all">
	<link rel="stylesheet" id="vodi-section-live-videos-css" href="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/plugins/vodi-extensions/assets/css/gutenberg-blocks/section-live-videos/style.min.css?ver=1570784057" type="text/css" media="all">
	<link rel="stylesheet" id="vodi-videos-slider-css" href="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/plugins/vodi-extensions/assets/css/gutenberg-blocks/videos-slider/style.min.css?ver=1570784057" type="text/css" media="all">
	<link rel="stylesheet" id="vodi-movies-slider-css" href="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/plugins/vodi-extensions/assets/css/gutenberg-blocks/movies-slider/style.min.css?ver=1570784057" type="text/css" media="all">
	<link rel="stylesheet" id="vodi-tv-shows-slider-css" href="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/plugins/vodi-extensions/assets/css/gutenberg-blocks/tv-shows-slider/style.min.css?ver=1570784057" type="text/css" media="all">
	<link rel="stylesheet" id="vodi-section-coming-soon-videos-css" href="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/plugins/vodi-extensions/assets/css/gutenberg-blocks/section-coming-soon-videos/style.min.css?ver=1570784057" type="text/css" media="all">
	<link rel="stylesheet" id="vodi-movies-list-css" href="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/plugins/vodi-extensions/assets/css/gutenberg-blocks/movies-list/style.min.css?ver=1570784057" type="text/css" media="all">
	<link rel="stylesheet" id="vodi-blog-grid-with-list-section-css" href="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/plugins/vodi-extensions/assets/css/gutenberg-blocks/blog-grid-with-list-section/style.min.css?ver=1570784057" type="text/css" media="all">
	<link rel="stylesheet" id="vodi-recent-comments-css" href="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/plugins/vodi-extensions/assets/css/gutenberg-blocks/recent-comments/style.min.css?ver=1570784057" type="text/css" media="all">
	<link rel="stylesheet" id="vodi-section-event-category-list-css" href="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/plugins/vodi-extensions/assets/css/gutenberg-blocks/section-event-category-list/style.min.css?ver=1570784057" type="text/css" media="all">
	<link rel="stylesheet" id="vodi-featured-blog-with-blog-grid-section-css" href="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/plugins/vodi-extensions/assets/css/gutenberg-blocks/featured-blog-with-blog-grid-section/style.min.css?ver=1570784057" type="text/css" media="all">
	<link rel="stylesheet" id="vodi-active-videos-css" href="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/plugins/vodi-extensions/assets/css/gutenberg-blocks/active-videos/style.min.css?ver=1570784057" type="text/css" media="all">
	<link rel="stylesheet" id="vodi-section-playlist-carousel-css" href="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/plugins/vodi-extensions/assets/css/gutenberg-blocks/section-playlist-carousel/style.min.css?ver=1570784057" type="text/css" media="all">
	<link rel="stylesheet" id="vodi-section-tv-episodes-carousel-aside-header-css" href="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/plugins/vodi-extensions/assets/css/gutenberg-blocks/section-tv-episodes-carousel-aside-header/style.min.css?ver=1570784057" type="text/css" media="all">
	<link rel="stylesheet" id="vodi-section-tv-episodes-carousel-flex-header-css" href="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/plugins/vodi-extensions/assets/css/gutenberg-blocks/section-tv-episodes-carousel-flex-header/style.min.css?ver=1570784057" type="text/css" media="all">
	<link rel="stylesheet" id="vodi-tv-show-section-aside-header-css" href="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/plugins/vodi-extensions/assets/css/gutenberg-blocks/tv-show-section-aside-header/style.min.css?ver=1570784057" type="text/css" media="all">
	<link rel="stylesheet" id="vodi-section-tv-shows-carousel-nav-header-css" href="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/plugins/vodi-extensions/assets/css/gutenberg-blocks/section-tv-shows-carousel-nav-header/style.min.css?ver=1570784057" type="text/css" media="all">
	<link rel="stylesheet" id="vodi-episodes-with-featured-episode-css" href="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/plugins/vodi-extensions/assets/css/gutenberg-blocks/episodes-with-featured-episode/style.min.css?ver=1570784057" type="text/css" media="all">
	<link rel="stylesheet" id="vodi-block-styles-css" href="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/themes/vodi/assets/css/gutenberg-blocks.min.css?ver=1.2.0" type="text/css" media="all">
	<link rel="stylesheet" id="contact-form-7-css" href="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=5.1.7" type="text/css" media="all">
	<link rel="stylesheet" id="wpcdt-public-css-css" href="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/plugins/countdown-timer-ultimate/assets/css/wpcdt-timecircles.css?ver=1.2.4" type="text/css" media="all">
	<link rel="stylesheet" id="photoswipe-css" href="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/plugins/masvideos/assets/css/photoswipe/photoswipe.css?ver=1.1.9" type="text/css" media="all">
	<link rel="stylesheet" id="photoswipe-default-skin-css" href="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/plugins/masvideos/assets/css/photoswipe/default-skin/default-skin.css?ver=1.1.9" type="text/css" media="all">
	<link rel="stylesheet" id="vodi-style-css" href="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/themes/vodi/style.css?ver=1.2.0" type="text/css" media="all">
	<link rel="stylesheet" id="fontawesome-css" href="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/themes/vodi/assets/css/fontawesome.css?ver=1.2.0" type="text/css" media="all">
	<link rel="stylesheet" id="jquery-fancybox-css" href="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/themes/vodi/assets/css/jquery.fancybox.css?ver=1.2.0" type="text/css" media="all">
	<link rel="stylesheet" id="vodi-theme-css" href="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/themes/vodi/assets/css/theme.css?ver=1.2.0" type="text/css" media="all">
	<link rel="stylesheet" id="animate-css" href="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/themes/vodi/assets/css/animate.min.css?ver=1.2.0" type="text/css" media="all">
	<link rel="stylesheet" id="vodi-masvideos-css" href="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/themes/vodi/assets/css/masvideos.css?ver=1.2.0" type="text/css" media="all">
	<link rel="stylesheet" id="vodi-color-css" href="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/themes/vodi/assets/css/colors/blue.css?ver=1.2.0" type="text/css" media="all">
	<link rel="stylesheet" id="vodi-fonts-css" href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700,800%7cOpen+Sans:400,600,700&amp;subset=latin%2Clatin-ext" type="text/css" media="all">
	<link rel="stylesheet" id="jetpack_css-css" href="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/plugins/jetpack/css/jetpack.css?ver=8.5" type="text/css" media="all">
	<script type="text/javascript" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-includes/js/jquery/jquery.js?ver=1.12.4-wp">
	</script>
	<script type="text/javascript" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1">
	</script>
	<link rel="https://api.w.org/" href="https://demo3.madrasthemes.com/vodi-demos/main/wp-json/">
	<link rel="EditURI" type="application/rsd+xml" title="RSD" href="https://demo3.madrasthemes.com/vodi-demos/main/xmlrpc.php?rsd">
	<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="https://demo3.madrasthemes.com/vodi-demos/main/wp-includes/wlwmanifest.xml">
	<meta name="generator" content="WordPress 5.4.4">
	<style type="text/css">img#wpstats{display:none}</style>
	<style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style>
	<style type="text/css"> .single-episode-v1 .episode__description > div, .single-episode-v2 .episode__description > div, .single-episode-v2 .episode__description > div, .single-episode-v4 .episode__short-description > p + [data-readmore-toggle], .single-episode-v1 .episode__description > div, .single-episode-v2 .episode__description > div, .single-episode-v2 .episode__description > div, .single-episode-v4 .episode__short-description > p[data-readmore] {
		display: block; width: 100%;
		}.single-episode-v1 .episode__description > div, .single-episode-v2 .episode__description > div, .single-episode-v2 .episode__description > div, .single-episode-v4 .episode__short-description > p[data-readmore] {
			transition: height 600ms;
			overflow: hidden;
		}</style>
		<style type="text/css"> .single-movie-v1 .movie__description > div, .single-movie-v2 .movie__description > div, .single-movie-v3 .movie__description > div, .single-movie-v4 .movie__info--head .movie__short-description > p + [data-readmore-toggle], .single-movie-v1 .movie__description > div, .single-movie-v2 .movie__description > div, .single-movie-v3 .movie__description > div, .single-movie-v4 .movie__info--head .movie__short-description > p[data-readmore] {
			display: block; width: 100%;
			}.single-movie-v1 .movie__description > div, .single-movie-v2 .movie__description > div, .single-movie-v3 .movie__description > div, .single-movie-v4 .movie__info--head .movie__short-description > p[data-readmore] {
				transition: height 600ms;
				overflow: hidden;
			}</style>
			<style type="text/css"> .single-video .single-video__description > div + [data-readmore-toggle], .single-video .single-video__description > div[data-readmore] {
				display: block; width: 100%;
				}.single-video .single-video__description > div[data-readmore] {
					transition: height 600ms;
					overflow: hidden;
				}</style>
			</head>
			<script type="text/javascript">
				window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/demo3.madrasthemes.com\/vodi-demos\/main\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.4.4"}};
				/*! This file is auto-generated */
				!function(e,a,t){var r,n,o,i,p=a.createElement("canvas"),s=p.getContext&&p.getContext("2d");function c(e,t){var a=String.fromCharCode;s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,e),0,0);var r=p.toDataURL();return s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,t),0,0),r===p.toDataURL()}function l(e){if(!s||!s.fillText)return!1;switch(s.textBaseline="top",s.font="600 32px Arial",e){case"flag":return!c([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])&&(!c([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!c([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]));case"emoji":return!c([55357,56424,55356,57342,8205,55358,56605,8205,55357,56424,55356,57340],[55357,56424,55356,57342,8203,55358,56605,8203,55357,56424,55356,57340])}return!1}function d(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(i=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},o=0;o<i.length;o++)t.supports[i[o]]=l(i[o]),t.supports.everything=t.supports.everything&&t.supports[i[o]],"flag"!==i[o]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[i[o]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(r=t.source||{}).concatemoji?d(r.concatemoji):r.wpemoji&&r.twemoji&&(d(r.twemoji),d(r.wpemoji)))}(window,document,window._wpemojiSettings);
			</script>
			<script src="https://demo3.madrasthemes.com/vodi-demos/main/wp-includes/js/wp-emoji-release.min.js?ver=5.4.4" type="text/javascript" defer="">
			</script>
			<style type="text/css">
				img.wp-smiley,
				img.emoji {
					display: inline !important;
					border: none !important;
					box-shadow: none !important;
					height: 1em !important;
					width: 1em !important;
					margin: 0 .07em !important;
					vertical-align: -0.1em !important;
					background: none !important;
					padding: 0 !important;
				}
			</style>

			<body class="archive post-type-archive post-type-archive-movie wp-embed-responsive masvideos masvideos-page masvideos-archive masvideos-js  sidebar-left dark">
				<div id="page" class="hfeed site">
					<header id="site-header" class="site-header header-v1 desktop-header stick-this light" role="banner" style="">
						<div class="container-fluid">
							<div class="site-header__inner">
								<div class="site-header__right">
									<div class="site-header__offcanvas">
										<button class="site-header__offcanvas--toggler navbar-toggler" data-toggle="offcanvas">
											<svg xmlns="http://www.w3.org/2000/svg" width="16" height="13">
												<path d="M0 13L0 11.4 16 11.4 16 13 0 13ZM0 5.7L16 5.7 16 7.3 0 7.3 0 5.7ZM0 0L16 0 16 1.6 0 1.6 0 0Z" />
											</svg>
										</button>
										<div class="offcanvas-drawer">
											<div class="offcanvas-collapse" data-simplebar>
												<div class="site_header__offcanvas-nav">
													<ul id="menu-off-canvas-menu" class="offcanvas-nav yamm">
														<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-5565" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-ancestor current-menu-parent menu-item-has-children dropdown active menu-item-5565 nav-item">
															<a title="Home" href="/" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle nav-link">Home</a>

														</li>
														<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-5751" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children dropdown menu-item-5751 nav-item">
															<a title="Movies" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle nav-link">Movies</a>
															<ul class="dropdown-menu" role="menu">
																<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-5579" class="menu-item menu-item-type-taxonomy menu-item-object-movie_genre menu-item-5579 nav-item">
																	<a title="Action" href="/react2/movies/action/" class="dropdown-item">Action</a>
																</li>
																<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-5580" class="menu-item menu-item-type-taxonomy menu-item-object-movie_genre menu-item-5580 nav-item">
																	<a title="Adventure" href="/react2/movies/adventure/" class="dropdown-item">Adventure</a>
																</li>
																<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-5581" class="menu-item menu-item-type-taxonomy menu-item-object-movie_genre menu-item-5581 nav-item">
																	<a title="Comedy" href="/react2/movies/comedy/" class="dropdown-item">Comedy</a>
																</li>
																<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-5582" class="menu-item menu-item-type-taxonomy menu-item-object-movie_genre menu-item-5582 nav-item">
																	<a title="Drama" href="/react2/movies/drama/" class="dropdown-item">Drama</a>
																</li>
																<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-5583" class="menu-item menu-item-type-taxonomy menu-item-object-movie_genre menu-item-5583 nav-item">
																	<a title="Family" href="/react2/movies/family/" class="dropdown-item">Family</a>
																</li>
																<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-5584" class="menu-item menu-item-type-taxonomy menu-item-object-movie_genre menu-item-5584 nav-item">
																	<a title="Romance" href="/react2/movies/romance/" class="dropdown-item">Romance</a>
																</li>
															</ul>
														</li>
														<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-5559" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children dropdown menu-item-5559 nav-item">
															<a title="TV Shows" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle nav-link">TV Shows</a>
															<ul class="dropdown-menu" role="menu">
																<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-5572" class="menu-item menu-item-type-taxonomy menu-item-object-tv_show_genre menu-item-5572 nav-item">
																	<a title="Drama" href="/tv-show-genre/drama/" class="dropdown-item">Drama</a>
																</li>
																<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-5573" class="menu-item menu-item-type-taxonomy menu-item-object-tv_show_genre menu-item-5573 nav-item">
																	<a title="Comedy" href="/tv-show-genre/comedy/" class="dropdown-item">Comedy</a>
																</li>
																<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-5574" class="menu-item menu-item-type-taxonomy menu-item-object-tv_show_genre menu-item-5574 nav-item">
																	<a title="Action" href="/tv-show-genre/action/" class="dropdown-item">Action</a>
																</li>
																<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-5575" class="menu-item menu-item-type-taxonomy menu-item-object-tv_show_genre menu-item-5575 nav-item">
																	<a title="Crime" href="/tv-show-genre/crime/" class="dropdown-item">Crime</a>
																</li>
																<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-5576" class="menu-item menu-item-type-taxonomy menu-item-object-tv_show_genre menu-item-5576 nav-item">
																	<a title="Romance" href="/tv-show-genre/romance/" class="dropdown-item">Romance</a>
																</li>
																<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-5577" class="menu-item menu-item-type-taxonomy menu-item-object-tv_show_genre menu-item-5577 nav-item">
																	<a title="Adventure" href="/tv-show-genre/adventure/" class="dropdown-item">Adventure</a>
																</li>
																<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-5578" class="menu-item menu-item-type-taxonomy menu-item-object-tv_show_genre menu-item-5578 nav-item">
																	<a title="Sci-Fi" href="/tv-show-genre/sci-fi/" class="dropdown-item">Sci-Fi</a>
																</li>
															</ul>
														</li>
														<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-5558" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children dropdown menu-item-5558 nav-item">
															<a title="Videos" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle nav-link">Videos</a>
															<ul class="dropdown-menu" role="menu">
																<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-5566" class="menu-item menu-item-type-taxonomy menu-item-object-video_cat menu-item-5566 nav-item">
																	<a title="Games" href="https://demo3.madrasthemes.com/vodi-demos/main/video-category/games/" class="dropdown-item">Games</a>
																</li>
																<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-5567" class="menu-item menu-item-type-taxonomy menu-item-object-video_cat menu-item-5567 nav-item">
																	<a title="Football" href="https://demo3.madrasthemes.com/vodi-demos/main/video-category/football/" class="dropdown-item">Football</a>
																</li>
																<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-5568" class="menu-item menu-item-type-taxonomy menu-item-object-video_cat menu-item-5568 nav-item">
																	<a title="Adventure" href="https://demo3.madrasthemes.com/vodi-demos/main/video-category/adventure/" class="dropdown-item">Adventure</a>
																</li>
																<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-5569" class="menu-item menu-item-type-taxonomy menu-item-object-video_cat menu-item-5569 nav-item">
																	<a title="Cricket" href="https://demo3.madrasthemes.com/vodi-demos/main/video-category/cricket/" class="dropdown-item">Cricket</a>
																</li>
																<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-5570" class="menu-item menu-item-type-taxonomy menu-item-object-video_cat menu-item-5570 nav-item">
																	<a title="Tennis" href="https://demo3.madrasthemes.com/vodi-demos/main/video-category/tennis/" class="dropdown-item">Tennis</a>
																</li>
																<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-5571" class="menu-item menu-item-type-taxonomy menu-item-object-video_cat menu-item-5571 nav-item">
																	<a title="Action" href="https://demo3.madrasthemes.com/vodi-demos/main/video-category/action/" class="dropdown-item">Action</a>
																</li>
															</ul>
														</li>
													</ul>
												</div>
											</div>

										</div>
									</div>
									<div class="site-header__logo">
										<a href="/" rel="home" class="navbar-brand"> <img style="max-width: 16%;" src="https://khojapp.com/asset/images/logo1.png">
										</a>
									</div>
									<div class="site_header__primary-nav">
										<ul id="menu-primary-menu" class="nav yamm">
						{{--<li id="menu-item-5554" class="yamm-tfw menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-has-children menu-item-5554">
<a href="/" aria-current="page">Browse</a>
							 <ul class="sub-menu">
								<li id="menu-item-5213" class="menu-item menu-item-type-post_type menu-item-object-mas_static_content menu-item-5213">
<a title="" href="https://demo3.madrasthemes.com/vodi-demos/main/?mas_static_content=pages-megamenu">Pages Megamenu</a>
									<div class="yamm-content">
										<div class="wp-block-columns has-3-columns">
											<div class="wp-block-column">
												<ul>
													<li class="nav-title">
<a href="#">Archive Pages</a>
</li>
													<li>
<a href="/movies/">Movies</a>
</li>
													<li>
<a href="/tv-shows/">TV Shows</a>
</li>
													<li>
<a href="/videos/">Videos</a>
</li>
													<li class="nav-title">
<a href="#">Single Movies</a>
</li>
													<li>
<a href="/react2/movie/black-mirror/">Movie v1</a>
</li>
													<li>
<a href="/react2/movie/renegades/">Movie v2</a>
</li>
													<li>
<a href="/react2/movie/my-generation/">Movie v3</a>
</li>
													<li>
<a href="/react2/movie/breath/">Movie v4</a>
</li>
													<li>
<a href="/react2/movie/a-kid-like-jake/">Movie v5</a>
</li>
													<li>
<a href="/react2/movie/paradigm-lost/">Movie v6</a>
</li>
													<li>
<a href="/react2/movie/the-tale/">Movie v7</a>
</li>
												</ul>
												<p>
</p>
											</div>
											<div class="wp-block-column">
												<ul>
													<li class="nav-title">
<a href="#">Single Videos</a>
</li>
													<li>
<a href="/video/marin-retires-hurt-saina-wins-2/">Video v1</a>
</li>
													<li>
<a href="/video/anderson-fights-his-way-to-glory/">Video v2</a>
</li>
													<li>
<a href="/video/unstoppable-vihari-cruises-with-151/">Video v3</a>
</li>
													<li>
<a href="/video/spain-vs-netherlands/">Video v4</a>
</li>
													<li>
<a href="/video/steyn-betters-kapil-arsenal-lose/">Video v5</a>
</li>
													<li>
<a href="/video/super-bowl-liii-patriots-vs-rams-micd-up-nfl-2018-season/">Video v6</a>
</li>
													<li class="nav-title">
<a href="#">Other Pages</a>
</li>
													<li>
<a href="/landing-v1/">Landing v1</a>
</li>
													<li>
<a href="/landing-v2/">Landing v2</a>
</li>
													<li>
<a href="/comingsoon/">Coming Soon</a>
</li>
													<li class="nav-title">
<a href="/react2/tv_show/vikings/">Single TV Show</a>
</li>
												</ul>
											</div>
											<div class="wp-block-column">
												<ul>
													<li class="nav-title">
<a href="#">Single Episodes</a>
</li>
													<li>
<a href="/react2/episode/tape-2-side-a/">Episode v1</a>
</li>
													<li>
<a href="/react2/episode/tape-2-side-b/">Episode v2</a>
</li>
													<li>
<a href="/react2/episode/tape-3-side-a/">Episode v3</a>
</li>
													<li>
<a href="/react2/episode/the-first-polaroid/">Episode v4</a>
</li>
													 <li class="nav-title">
<a href="https://demo3.madrasthemes.com/vodi-demos/main/blog/">Blog Pages</a>
</li>
													 <li>
<a href="https://demo3.madrasthemes.com/vodi-demos/main/blog/">Blog</a>
</li> 
													 <li>
<a href="https://demo3.madrasthemes.com/vodi-demos/main/2019/02/05/wizard-actor-has-another-digital-comics-role-featured-slideshow-post/">Single Blog</a>
</li> 
													 <li class="nav-title">
<a href="#">Static Pages</a>
</li>
													<li>
<a href="/contact/">Contact Us</a>
</li>
													<li>
<a href="/404page">404</a>
</li>
												</ul>
											</div>
										</div>
									</div>
								</li>
								</ul>  
							</li>--}}
							<li id="menu-item-5553" class="yamm-fw menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home  menu-item-5553">
								<a href="/" aria-current="page">Home</a>
								{{-- <ul class="sub-menu">
									<li id="menu-item-5215" class="menu-item menu-item-type-post_type menu-item-object-mas_static_content menu-item-5215">
<a title="" href="https://demo3.madrasthemes.com/vodi-demos/main/?mas_static_content=home">Home</a>
										<div class="yamm-content">
											<div class="wp-block-columns has-5-columns">
												<div class="wp-block-column">
													<figure class="wp-block-image">
<a href="/" target="_blank" rel="noreferrer noopener">
<img src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/menu-1.jpg" alt="" class="wp-image-5794" srcset="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/menu-1.jpg 260w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/menu-1-150x150.jpg 150w" sizes="(max-width: 260px) 100vw, 260px" />
</a>
<figcaption>
<strong>Home v1</strong>
</figcaption>
</figure>
												</div>
												<div class="wp-block-column">
													<figure class="wp-block-image">
<a href="https://demo3.madrasthemes.com/vodi-demos/main/home-v2/" target="_blank" rel="noreferrer noopener">
<img src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/menu-14.jpg" alt="" class="wp-image-5726" srcset="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/menu-14.jpg 260w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/menu-14-150x150.jpg 150w" sizes="(max-width: 260px) 100vw, 260px" />
</a>
<figcaption>
<strong>Home v2</strong>
</figcaption>
</figure>
												</div>
												<div class="wp-block-column">
													<figure class="wp-block-image">
<a href="https://demo3.madrasthemes.com/vodi-demos/main/home-v3/" target="_blank" rel="noreferrer noopener">
<img src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/menu-15.jpg" alt="" class="wp-image-5749" srcset="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/menu-15.jpg 260w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/menu-15-150x150.jpg 150w" sizes="(max-width: 260px) 100vw, 260px" />
</a>
<figcaption>
<strong>Home v3</strong>
</figcaption>
</figure>
												</div>
												<div class="wp-block-column">
													<figure class="wp-block-image">
<a href="https://demo3.madrasthemes.com/vodi-demos/vodi-sports/">
<img src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/05/menu-4.jpg" alt="" class="wp-image-6036" srcset="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/05/menu-4.jpg 260w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/05/menu-4-150x150.jpg 150w" sizes="(max-width: 260px) 100vw, 260px" />
</a>
<figcaption>
<strong>Home v4</strong>
</figcaption>
</figure>
												</div>
												<div class="wp-block-column">
													<figure class="wp-block-image">
<a href="https://demo3.madrasthemes.com/vodi-demos/main/home-v5/" target="_blank" rel="noreferrer noopener">
<img src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/menu-16.jpg" alt="" class="wp-image-5756" srcset="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/menu-16.jpg 260w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/menu-16-150x150.jpg 150w" sizes="(max-width: 260px) 100vw, 260px" />
</a>
<figcaption>
<strong>Home v5</strong>
</figcaption>
</figure>
												</div>
											</div>
											<div class="wp-block-columns has-5-columns">
												<div class="wp-block-column">
													<figure class="wp-block-image">
<a href="https://demo3.madrasthemes.com/vodi-demos/vodi-prime/" target="_blank" rel="noreferrer noopener">
<img src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/menu-18.jpg" alt="" class="wp-image-5782" srcset="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/menu-18.jpg 260w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/menu-18-150x150.jpg 150w" sizes="(max-width: 260px) 100vw, 260px" />
</a>
<figcaption>
<strong>Vodi Prime﻿</strong>
<br>
<strong>Home v6 ( Light )</strong>
</figcaption>
</figure>
												</div>
												<div class="wp-block-column">
													<figure class="wp-block-image">
<a href="https://demo3.madrasthemes.com/vodi-demos/vodi-prime-dark/" target="_blank" rel="noreferrer noopener">
<img src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/menu-19.jpg" alt="" class="wp-image-5783" srcset="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/menu-19.jpg 260w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/menu-19-150x150.jpg 150w" sizes="(max-width: 260px) 100vw, 260px" />
</a>
<figcaption>
<strong>﻿Vodi Prime</strong>
<br>
<strong>Home v7 ( Dark )</strong>
</figcaption>
</figure>
													<p>
</p>
												</div>
												<div class="wp-block-column">
													<figure class="wp-block-image">
<a href="https://demo3.madrasthemes.com/vodi-demos/vodi-stream/" target="_blank" rel="noreferrer noopener">
<img src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/menu-17.jpg" alt="" class="wp-image-5768" srcset="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/menu-17.jpg 260w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/menu-17-150x150.jpg 150w" sizes="(max-width: 260px) 100vw, 260px" />
</a>
<figcaption>
<strong>Vodi Stream </strong>
<br>
<strong>Home v8</strong>
</figcaption>
</figure>
												</div>
												<div class="wp-block-column">
													<figure class="wp-block-image">
<a href="https://demo3.madrasthemes.com/vodi-demos/vodi-tube/" target="_blank" rel="noreferrer noopener">
<img src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/menu-12.jpg" alt="" class="wp-image-5612" srcset="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/menu-12.jpg 260w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/menu-12-150x150.jpg 150w" sizes="(max-width: 260px) 100vw, 260px" />
</a>
<figcaption>
<strong>Vodi Tube</strong>
<br>
<strong>Home v9 ( Light )</strong>
</figcaption>
</figure>
												</div>
												<div class="wp-block-column">
													<figure class="wp-block-image">
<a href="https://demo3.madrasthemes.com/vodi-demos/vodi-tube-dark/" target="_blank" rel="noreferrer noopener">
<img src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/menu-13.jpg" alt="" class="wp-image-5613" srcset="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/menu-13.jpg 260w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/menu-13-150x150.jpg 150w" sizes="(max-width: 260px) 100vw, 260px" />
</a>
<figcaption>
<strong>Vodi Tube</strong>
<br>
<strong>Home v10 ( Dark )</strong>
</figcaption>
</figure>
													<p>
</p>
												</div>
											</div>
										</div>
</li>
</ul> --}}
</li>
<li id="menu-item-5551" class="yamm-fw menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-5551">
	<a href="https://demo3.madrasthemes.com">Movies</a>
	<ul class="sub-menu">
		<li id="menu-item-5204" class="menu-item menu-item-type-post_type menu-item-object-mas_static_content menu-item-5204">
			<a title="" href="https://demo3.madrasthemes.com/vodi-demos/main/?mas_static_content=movies-megamenu">Movies Megamenu</a>
			<div class="yamm-content">
				<div class="wp-block-columns has-3-columns">
					<div class="wp-block-column">
						<h2>Movie of the Day</h2>
						<div class="masvideos masvideos-movies vodi-megamenu-movie">
							<div class="movies columns-1">
								<div class="movies__inner">
									<div class="post-322 movie type-movie status-publish has-post-thumbnail hentry movie_genre-adventure movie_genre-comedy movie_genre-romance movie_tag-4k-ultra movie_tag-king movie_tag-premieres movie_tag-viking">
										<div class="movie__poster">
											<a href="/react2/movie/the-big-sick/" class="masvideos-LoopMovie-link masvideos-loop-movie__link movie__link">
												<img width="300" height="450" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/36-cztery-lwy-300x450.jpg" class="movie__poster--image" alt="" srcset="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/36-cztery-lwy-300x450.jpg 300w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/36-cztery-lwy-66x98.jpg 66w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/36-cztery-lwy-600x900.jpg 600w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/36-cztery-lwy-150x225.jpg 150w" sizes="(max-width: 300px) 100vw, 300px" />
											</a>
										</div>
										<div class="movie__body">
											<div class="movie__info">
												<div class="movie__info--head">
													<div class="movie__meta">
														<span class="movie__meta--release-year">2017</span>
														<span class="movie__meta--genre">
															<a href="/react2/movies/adventure/" rel="tag">Adventure</a>, <a href="/react2/movies/comedy/" rel="tag">Comedy</a>, <a href="/react2/movies/romance/" rel="tag">Romance</a>
														</span>
													</div>
													<a href="/react2/movie/the-big-sick/" class="masvideos-LoopMovie-link masvideos-loop-movie__link movie__link">
														<h3 class="masvideos-loop-movie__title  movie__title">The Big Sick</h3>
													</a>
												</div> 
												<div class="movie__short-description">
													<div>
														<p>Pakistan-born comedian Kumail Nanjiani and grad student Emily Gardner fall in love but struggle as their cultures clash. When Emily contracts a mysterious illness, Kumail finds himself forced to face her feisty parents, his family&#8217;s expectations, and his true feelings</p> 
													</div> 
												</div>
												<div class="movie__actions">
													<a href="/react2/movie/the-big-sick/" class="movie-actions--link_watch">Watch Now</a> 
													<div class="movie-actions--link_add-to-playlist dropdown">
														<a class="dropdown-toggle" href="/react2/movie/the-big-sick/" data-toggle="dropdown">+ Playlist</a>
														<div class="dropdown-menu">
															<a class="login-link" href="/login/movie-playlists/">Sign in to add this movie to a playlist.</a> </div>
														</div>
													</div>
												</div>
												<div class="movie__review-info"> <a href="/react2/movie/the-big-sick/#reviews" class="avg-rating">
													<span class="rating-with-count">
														<svg class="vodi-svg" width="40px" height="39px" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 39">
															<title>play</title>
															<path fill-rule="evenodd" d="M19.633,-0.000 C21.509,0.035 21.530,1.174 22.167,2.414 C23.329,4.679 24.406,7.067 25.572,9.338 C25.853,9.886 26.431,11.640 26.918,11.834 C27.486,12.203 29.345,12.109 30.165,12.316 C32.170,12.825 34.489,12.860 36.500,13.364 C37.516,13.618 38.689,13.413 39.430,13.927 C39.689,14.107 39.770,14.504 39.984,14.732 C40.047,16.499 39.096,16.843 38.163,17.792 C36.473,19.509 34.784,21.227 33.095,22.944 C32.585,23.462 31.092,24.543 31.036,25.359 C31.423,25.951 31.307,27.455 31.511,28.258 C32.138,30.727 32.213,33.522 32.857,35.987 C33.142,37.078 33.016,38.241 32.303,38.724 C31.108,39.533 29.632,38.193 28.819,37.758 C26.695,36.623 24.601,35.624 22.483,34.457 C21.979,34.179 20.607,33.178 20.108,33.088 C19.748,33.023 18.163,34.107 17.812,34.296 C15.557,35.505 13.340,36.640 11.080,37.839 C10.548,38.120 9.180,39.226 8.309,38.966 C6.955,38.558 6.874,36.993 7.280,35.423 C7.716,33.733 7.697,31.880 8.151,30.109 C8.527,28.642 8.907,26.529 9.022,24.957 C8.092,24.344 7.202,23.107 6.408,22.300 C4.760,20.625 3.059,18.990 1.340,17.389 C0.646,16.742 -0.578,15.515 0.311,14.249 C0.915,13.388 2.364,13.656 3.557,13.364 C6.678,12.599 10.114,12.468 13.298,11.834 C14.186,9.747 15.306,7.711 16.307,5.716 C16.954,4.426 17.496,3.163 18.128,1.931 C18.334,1.531 18.358,1.093 18.603,0.724 C18.845,0.362 19.299,0.273 19.633,-0.000 Z" />
														</svg> <span class="avg-rating-number"> 5.0</span>
													</span>
													<span class="rating-number-with-text">
														<span class="avg-rating-number"> 5.0</span>
														<span class="avg-rating-text">
															<span>1</span> Vote </span>
														</span>
													</a>
													<div class="viewers-count">
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="wp-block-column">
							<h2>Movies Genre</h2>
							<ul class="menu">
								<li>
									<a href="https://demo3.madrasthemes.com/vodi-demos/main/movies">Action</a>
								</li>
								<li>
									<a href="https://demo3.madrasthemes.com/vodi-demos/main/movies">Adventure</a>
								</li>
								<li>
									<a href="https://demo3.madrasthemes.com/vodi-demos/main/movies">Comedy</a>
								</li>
								<li>
									<a href="https://demo3.madrasthemes.com/vodi-demos/main/movies">Drama</a>
								</li>
								<li>
									<a href="https://demo3.madrasthemes.com/vodi-demos/main/movies">Sci-Fi</a>
								</li>
								<li>
									<a href="https://demo3.madrasthemes.com/vodi-demos/main/movies">Thriller</a>
								</li>
								<li>
									<a href="https://demo3.madrasthemes.com/vodi-demos/main/movies">Biography</a>
								</li>
								<li>
									<a href="https://demo3.madrasthemes.com/vodi-demos/main/movies">Family</a>
								</li>
								<li>
									<a href="https://demo3.madrasthemes.com/vodi-demos/main/movies">Romance</a>
								</li>
								<li>
									<a href="https://demo3.madrasthemes.com/vodi-demos/main/movies">Animation</a>
								</li>
								<li>
									<a href="https://demo3.madrasthemes.com/vodi-demos/main/movies">Thriller</a>
								</li>
								<li>
									<a href="https://demo3.madrasthemes.com/vodi-demos/main/movies">Adventure</a>
								</li>
								<li>
									<a href="https://demo3.madrasthemes.com/vodi-demos/main/movies">Biography</a>
								</li>
								<li>
									<a href="https://demo3.madrasthemes.com/vodi-demos/main/movies">Action</a>
								</li>
								<li>
									<a href="https://demo3.madrasthemes.com/vodi-demos/main/movies">Family</a>
								</li>
								<li>
									<a href="https://demo3.madrasthemes.com/vodi-demos/main/movies">Sci-Fi</a>
								</li>
								<li>
									<a href="https://demo3.madrasthemes.com/vodi-demos/main/movies">Romance</a>
								</li>
								<li>
									<a href="https://demo3.madrasthemes.com/vodi-demos/main/movies">Drama</a>
								</li>
								<li>
									<a href="https://demo3.madrasthemes.com/vodi-demos/main/movies">Comedy</a>
								</li>
							</ul>
						</div>
						<div class="wp-block-column">
							<h2>Movie Trailer</h2>
							<section id="section-videos-carousel-flex-header-5f87f9fbec843" class="home-section section-videos-carousel-flex-header style-2" style="padding-left: 40px; padding-right: 40px; ">
								<div class="container">
									<div class="home-section__inner">
										<div class="section-videos-carousel__carousel">
											<div class="videos-carousel__inner" data-ride="vodi-slick-carousel" data-wrap=".videos__inner" data-slick="{&quot;slidesToShow&quot;:1,&quot;slidesToScroll&quot;:1,&quot;dots&quot;:false,&quot;arrows&quot;:true,&quot;autoplay&quot;:false,&quot;infinite&quot;:false,&quot;responsive&quot;:[{&quot;breakpoint&quot;:768,&quot;settings&quot;:{&quot;slidesToShow&quot;:1,&quot;slidesToScroll&quot;:1}},{&quot;breakpoint&quot;:992,&quot;settings&quot;:{&quot;slidesToShow&quot;:3,&quot;slidesToScroll&quot;:1}},{&quot;breakpoint&quot;:1200,&quot;settings&quot;:{&quot;slidesToShow&quot;:4,&quot;slidesToScroll&quot;:1}}]}">
												<div class="masvideos masvideos-videos ">
													<div class="videos columns-1">
														<div class="videos__inner">
															<div class="post-1478 video type-video status-publish has-post-thumbnail hentry video_cat-tennis">
																<div class="video__container">
																	<div class="video__poster">
																		<a href="https://demo3.madrasthemes.com/vodi-demos/main/video/anderson-fights-his-way-to-glory/" class="masvideos-LoopVideo-link masvideos-loop-video__link video__link">
																			<img width="480" height="270" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/video-thumbnail-K5h2cd4QtKk-480x270.jpg" class="video__poster--image" alt="" data-gallery-images="[]" srcset="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/video-thumbnail-K5h2cd4QtKk-480x270.jpg 480w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/video-thumbnail-K5h2cd4QtKk-270x153.jpg 270w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/video-thumbnail-K5h2cd4QtKk-220x125.jpg 220w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/video-thumbnail-K5h2cd4QtKk-244x138.jpg 244w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/video-thumbnail-K5h2cd4QtKk-339x192.jpg 339w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/video-thumbnail-K5h2cd4QtKk-327x185.jpg 327w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/video-thumbnail-K5h2cd4QtKk-300x169.jpg 300w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/video-thumbnail-K5h2cd4QtKk-768x432.jpg 768w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/video-thumbnail-K5h2cd4QtKk-1024x576.jpg 1024w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/video-thumbnail-K5h2cd4QtKk-640x360.jpg 640w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/video-thumbnail-K5h2cd4QtKk-120x67.jpg 120w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/video-thumbnail-K5h2cd4QtKk.jpg 1280w" sizes="(max-width: 480px) 100vw, 480px" />
																		</a>
																	</div>
																</div>
																<div class="video__body">
																	<div class="video__info">
																		<div class="video__info--head">
																			<a href="https://demo3.madrasthemes.com/vodi-demos/main/video/anderson-fights-his-way-to-glory/" class="masvideos-LoopVideo-link masvideos-loop-video__link video__link">
																				<h3 class="masvideos-loop-video__title  video__title">Anderson Fights His Way to Glory</h3>
																			</a>
																			<div class="video__views-meta">
																				<div class="vodi-jetpack-views">
																					<span>1.9K views</span>
																				</div>
																				<div class="video__meta">
																					<span class="video__meta--last-update">2 years ago</span>
																				</div>
																			</div>
																		</div> 
																		<div class="video__short-description">
																			<div>Kevin Anderson is a South African professional tennis player who is ranked world No. 5 in men&#8217;s singles by the Association of Tennis Professionals as of 28 January 2019.</div> </div>
																			<div class="video__actions">
																				<a href="https://demo3.madrasthemes.com/vodi-demos/main/video/anderson-fights-his-way-to-glory/" class="video-actions--link_watch">Watch Now</a> 
																				<div class="video-actions--link_add-to-playlist dropdown">
																					<a class="dropdown-toggle" href="https://demo3.madrasthemes.com/vodi-demos/main/video/anderson-fights-his-way-to-glory/" data-toggle="dropdown">+ Playlist</a>
																					<div class="dropdown-menu">
																						<a class="login-link" href="/login/video-playlists/">Sign in to add this video to a playlist.</a> </div>
																					</div>
																				</div>
																			</div>
																			<div class="video__review-info">
																				<div class="viewers-count">
																				</div>
																			</div>
																		</div>
																	</div>
																	<div class="post-1475 video type-video status-publish has-post-thumbnail hentry video_cat-cricket">
																		<div class="video__container">
																			<div class="video__poster">
																				<a href="https://demo3.madrasthemes.com/vodi-demos/main/video/unstoppable-vihari-cruises-with-151/" class="masvideos-LoopVideo-link masvideos-loop-video__link video__link">
																					<img width="480" height="270" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/video-thumbnail-KXVlVCix6NI-480x270.jpg" class="video__poster--image" alt="" data-gallery-images="[]" srcset="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/video-thumbnail-KXVlVCix6NI-480x270.jpg 480w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/video-thumbnail-KXVlVCix6NI-270x153.jpg 270w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/video-thumbnail-KXVlVCix6NI-220x125.jpg 220w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/video-thumbnail-KXVlVCix6NI-244x138.jpg 244w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/video-thumbnail-KXVlVCix6NI-339x192.jpg 339w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/video-thumbnail-KXVlVCix6NI-327x185.jpg 327w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/video-thumbnail-KXVlVCix6NI-120x67.jpg 120w" sizes="(max-width: 480px) 100vw, 480px" />
																				</a>
																			</div>
																		</div>
																		<div class="video__body">
																			<div class="video__info">
																				<div class="video__info--head">
																					<a href="https://demo3.madrasthemes.com/vodi-demos/main/video/unstoppable-vihari-cruises-with-151/" class="masvideos-LoopVideo-link masvideos-loop-video__link video__link">
																						<h3 class="masvideos-loop-video__title  video__title">Unstoppable Vihari Cruises with 151*</h3>
																					</a>
																					<div class="video__views-meta">
																						<div class="vodi-jetpack-views">
																							<span>1.4K views</span>
																						</div>
																						<div class="video__meta">
																							<span class="video__meta--last-update">2 years ago</span>
																						</div>
																					</div>
																				</div> 
																				<div class="video__short-description">
																					<div>Hanuma Vihari is an Indian cricketer who plays for Andhra in Indian domestic cricket. A right-handed batsman and occasional right arm off break bowler, he was a member of the India Under-19 cricket team that won the 2012 ICC Under-19 Cricket World Cup in Australia. He made his Test debut for India in September 2018.</div> </div>
																					<div class="video__actions">
																						<a href="https://demo3.madrasthemes.com/vodi-demos/main/video/unstoppable-vihari-cruises-with-151/" class="video-actions--link_watch">Watch Now</a> 
																						<div class="video-actions--link_add-to-playlist dropdown">
																							<a class="dropdown-toggle" href="https://demo3.madrasthemes.com/vodi-demos/main/video/unstoppable-vihari-cruises-with-151/" data-toggle="dropdown">+ Playlist</a>
																							<div class="dropdown-menu">
																								<a class="login-link" href="/login/video-playlists/">Sign in to add this video to a playlist.</a> </div>
																							</div>
																						</div>
																					</div>
																					<div class="video__review-info">
																						<div class="viewers-count">
																						</div>
																					</div>
																				</div>
																			</div>
																			<div class="post-1473 video type-video status-publish has-post-thumbnail hentry video_cat-football">
																				<div class="video__container">
																					<div class="video__poster">
																						<a href="https://demo3.madrasthemes.com/vodi-demos/main/video/spain-vs-netherlands/" class="masvideos-LoopVideo-link masvideos-loop-video__link video__link">
																							<img width="480" height="270" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/video-thumbnail-agm8CNZEvyo-480x270.jpg" class="video__poster--image" alt="" data-gallery-images="[&quot;https:\/\/demo3.madrasthemes.com\/vodi-demos\/main\/wp-content\/uploads\/sites\/2\/2019\/02\/hi-res-a032b247f1362a4640fe20a86c536c8b_crop_north.jpg&quot;,&quot;https:\/\/demo3.madrasthemes.com\/vodi-demos\/main\/wp-content\/uploads\/sites\/2\/2019\/02\/iniesta_650x360_061414020515.jpg&quot;,&quot;https:\/\/demo3.madrasthemes.com\/vodi-demos\/main\/wp-content\/uploads\/sites\/2\/2019\/02\/netherlands-vs-spain_650x360_061314103658.jpg&quot;,&quot;https:\/\/demo3.madrasthemes.com\/vodi-demos\/main\/wp-content\/uploads\/sites\/2\/2019\/02\/spain-vs-netherlands-world-cup-final.jpg&quot;]" srcset="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/video-thumbnail-agm8CNZEvyo-480x270.jpg 480w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/video-thumbnail-agm8CNZEvyo-270x153.jpg 270w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/video-thumbnail-agm8CNZEvyo-220x125.jpg 220w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/video-thumbnail-agm8CNZEvyo-244x138.jpg 244w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/video-thumbnail-agm8CNZEvyo-339x192.jpg 339w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/video-thumbnail-agm8CNZEvyo-327x185.jpg 327w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/video-thumbnail-agm8CNZEvyo-300x169.jpg 300w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/video-thumbnail-agm8CNZEvyo-768x432.jpg 768w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/video-thumbnail-agm8CNZEvyo-1024x576.jpg 1024w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/video-thumbnail-agm8CNZEvyo-640x360.jpg 640w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/video-thumbnail-agm8CNZEvyo-120x67.jpg 120w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/video-thumbnail-agm8CNZEvyo.jpg 1280w" sizes="(max-width: 480px) 100vw, 480px" />
																						</a>
																					</div>
																				</div>
																				<div class="video__body">
																					<div class="video__info">
																						<div class="video__info--head">
																							<a href="https://demo3.madrasthemes.com/vodi-demos/main/video/spain-vs-netherlands/" class="masvideos-LoopVideo-link masvideos-loop-video__link video__link">
																								<h3 class="masvideos-loop-video__title  video__title">Spain vs Netherlands</h3>
																							</a>
																							<div class="video__views-meta">
																								<div class="vodi-jetpack-views">
																									<span>1.3K views</span>
																								</div>
																								<div class="video__meta">
																									<span class="video__meta--last-update">2 years ago</span>
																								</div>
																							</div>
																						</div> 
																						<div class="video__short-description">
																							<div>
																								<p>Netherlands produced a sensational second-half performance to annihilate reigning champions Spain in a stunned Arena Fonte Nova.</p>
																							</div> </div>
																							<div class="video__actions">
																								<a href="https://demo3.madrasthemes.com/vodi-demos/main/video/spain-vs-netherlands/" class="video-actions--link_watch">Watch Now</a> 
																								<div class="video-actions--link_add-to-playlist dropdown">
																									<a class="dropdown-toggle" href="https://demo3.madrasthemes.com/vodi-demos/main/video/spain-vs-netherlands/" data-toggle="dropdown">+ Playlist</a>
																									<div class="dropdown-menu">
																										<a class="login-link" href="/login/video-playlists/">Sign in to add this video to a playlist.</a> </div>
																									</div>
																								</div>
																							</div>
																							<div class="video__review-info">
																								<div class="viewers-count">
																								</div>
																							</div>
																						</div>
																					</div>
																					<div class="post-1471 video type-video status-publish has-post-thumbnail hentry video_cat-hockey">
																						<div class="video__container">
																							<div class="video__poster">
																								<a href="https://demo3.madrasthemes.com/vodi-demos/main/video/steyn-betters-kapil-arsenal-lose/" class="masvideos-LoopVideo-link masvideos-loop-video__link video__link">
																									<img width="480" height="270" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/video-thumbnail-8qWC_-4zqWI-480x270.jpg" class="video__poster--image" alt="" data-gallery-images="[]" srcset="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/video-thumbnail-8qWC_-4zqWI-480x270.jpg 480w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/video-thumbnail-8qWC_-4zqWI-270x153.jpg 270w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/video-thumbnail-8qWC_-4zqWI-220x125.jpg 220w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/video-thumbnail-8qWC_-4zqWI-244x138.jpg 244w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/video-thumbnail-8qWC_-4zqWI-339x192.jpg 339w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/video-thumbnail-8qWC_-4zqWI-327x185.jpg 327w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/video-thumbnail-8qWC_-4zqWI-300x169.jpg 300w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/video-thumbnail-8qWC_-4zqWI-768x432.jpg 768w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/video-thumbnail-8qWC_-4zqWI-1024x576.jpg 1024w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/video-thumbnail-8qWC_-4zqWI-640x360.jpg 640w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/video-thumbnail-8qWC_-4zqWI-120x67.jpg 120w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/video-thumbnail-8qWC_-4zqWI.jpg 1280w" sizes="(max-width: 480px) 100vw, 480px" />
																								</a>
																							</div>
																						</div>
																						<div class="video__body">
																							<div class="video__info">
																								<div class="video__info--head">
																									<a href="https://demo3.madrasthemes.com/vodi-demos/main/video/steyn-betters-kapil-arsenal-lose/" class="masvideos-LoopVideo-link masvideos-loop-video__link video__link">
																										<h3 class="masvideos-loop-video__title  video__title">Steyn Betters Kapil, Arsenal Lose</h3>
																									</a>
																									<div class="video__views-meta">
																										<div class="vodi-jetpack-views">
																											<span>1.1K views</span>
																										</div>
																										<div class="video__meta">
																											<span class="video__meta--last-update">2 years ago</span>
																										</div>
																									</div>
																								</div> 
																								<div class="video__short-description">
																									<div>Arsenal suffered a shock 1-0 defeat at BATE Borisov on a night forward Alexandre Lacazette will want to forget after being sent off late on, while Chelsea, Inter Milan, and Napoli all won away in Europa League last 32 first-leg ties on Thursday.</div> </div>
																									<div class="video__actions">
																										<a href="https://demo3.madrasthemes.com/vodi-demos/main/video/steyn-betters-kapil-arsenal-lose/" class="video-actions--link_watch">Watch Now</a> 
																										<div class="video-actions--link_add-to-playlist dropdown">
																											<a class="dropdown-toggle" href="https://demo3.madrasthemes.com/vodi-demos/main/video/steyn-betters-kapil-arsenal-lose/" data-toggle="dropdown">+ Playlist</a>
																											<div class="dropdown-menu">
																												<a class="login-link" href="/login/video-playlists/">Sign in to add this video to a playlist.</a> </div>
																											</div>
																										</div>
																									</div>
																									<div class="video__review-info">
																										<div class="viewers-count">
																										</div>
																									</div>
																								</div>
																							</div>
																							<div class="post-1469 video type-video status-publish has-post-thumbnail hentry video_cat-cricket">
																								<div class="video__container">
																									<div class="video__poster">
																										<a href="https://demo3.madrasthemes.com/vodi-demos/main/video/all-india-selection-committee-meet/" class="masvideos-LoopVideo-link masvideos-loop-video__link video__link">
																											<img width="480" height="270" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/video-thumbnail-N_PBcj9LLs-480x270.jpg" class="video__poster--image" alt="" data-gallery-images="[]" srcset="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/video-thumbnail-N_PBcj9LLs-480x270.jpg 480w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/video-thumbnail-N_PBcj9LLs-270x153.jpg 270w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/video-thumbnail-N_PBcj9LLs-220x125.jpg 220w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/video-thumbnail-N_PBcj9LLs-244x138.jpg 244w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/video-thumbnail-N_PBcj9LLs-339x192.jpg 339w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/video-thumbnail-N_PBcj9LLs-327x185.jpg 327w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/video-thumbnail-N_PBcj9LLs-640x360.jpg 640w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/video-thumbnail-N_PBcj9LLs-120x67.jpg 120w" sizes="(max-width: 480px) 100vw, 480px" />
																										</a>
																									</div>
																								</div>
																								<div class="video__body">
																									<div class="video__info">
																										<div class="video__info--head">
																											<a href="https://demo3.madrasthemes.com/vodi-demos/main/video/all-india-selection-committee-meet/" class="masvideos-LoopVideo-link masvideos-loop-video__link video__link">
																												<h3 class="masvideos-loop-video__title  video__title">All-India Selection Committee Meet</h3>
																											</a>
																											<div class="video__views-meta">
																												<div class="vodi-jetpack-views">
																													<span>45 views</span>
																												</div>
																												<div class="video__meta">
																													<span class="video__meta--last-update">2 years ago</span>
																												</div>
																											</div>
																										</div> 
																										<div class="video__short-description">
																											<div>Indian National Cricket Selectors is a committee of cricket administrators (usually ex-cricket players) whose responsibility is the selection of cricket team to represent India at various levels. The term for the selectors was increased from 1-year to 2 years in 2006[1] with a provision for an additional year based on performance. Technically, there are 2 groups of selector</div> </div>
																											<div class="video__actions">
																												<a href="https://demo3.madrasthemes.com/vodi-demos/main/video/all-india-selection-committee-meet/" class="video-actions--link_watch">Watch Now</a> 
																												<div class="video-actions--link_add-to-playlist dropdown">
																													<a class="dropdown-toggle" href="https://demo3.madrasthemes.com/vodi-demos/main/video/all-india-selection-committee-meet/" data-toggle="dropdown">+ Playlist</a>
																													<div class="dropdown-menu">
																														<a class="login-link" href="/login/video-playlists/">Sign in to add this video to a playlist.</a> </div>
																													</div>
																												</div>
																											</div>
																											<div class="video__review-info">
																												<div class="viewers-count">
																												</div>
																											</div>
																										</div>
																									</div>
																								</div>
																							</div>
																						</div> </div>
																					</div>
																				</div>
																			</div>
																		</section>
																	</div>
																</div>
															</div>
														</li>
													</ul>
												</li>
												<li id="menu-item-5552" class="yamm-fw menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-5552">
													<a href="https://demo3.madrasthemes.com/vodi-demos/main/tv-shows/">TV Shows</a>
													<ul class="sub-menu">
														<li id="menu-item-5206" class="menu-item menu-item-type-post_type menu-item-object-mas_static_content menu-item-5206">
															<a title="" href="https://demo3.madrasthemes.com/vodi-demos/main/?mas_static_content=tv-shows-megamenu">TV Shows Megamenu</a>
															<div class="yamm-content">
																<div class="wp-block-columns has-2-columns">
																	<div class="wp-block-column">
																		<ul>
																			<li class="highlight">
																				<a href="https://demo3.madrasthemes.com/vodi-demos/main/tv-shows/">Trending TV Shows</a>
																			</li>
																			<li class="highlight">
																				<a href="https://demo3.madrasthemes.com/vodi-demos/main/tv-shows/">Popular Now</a>
																			</li>
																			<li class="highlight">
																				<a href="https://demo3.madrasthemes.com/vodi-demos/main/tv-shows/">New This Month</a>
																			</li>
																			<li class="highlight">
																				<a href="https://demo3.madrasthemes.com/vodi-demos/main/tv-shows/">Vodi Exclusives</a>
																			</li>
																			<li class="highlight">
																				<a href="https://demo3.madrasthemes.com/vodi-demos/main/tv-shows/">Just for Kids</a>
																			</li>
																			<li class="highlight">
																				<a href="https://demo3.madrasthemes.com/vodi-demos/main/tv-shows/">Featured Shows</a>
																			</li>
																			<li class="highlight">
																				<a href="https://demo3.madrasthemes.com/vodi-demos/main/tv-shows/">TV Show Genres</a>
																			</li>
																			<li class="highlight">
																				<a href="https://demo3.madrasthemes.com/vodi-demos/main/tv-shows/">TV Series: Drama</a>
																			</li>
																			<li class="highlight">
																				<a href="https://demo3.madrasthemes.com/vodi-demos/main/tv-shows/">TV Series: Comedy</a>
																			</li>
																		</ul>
																		<p>
																		</p>
																	</div>
																	<div class="wp-block-column">
																		<h2>Picked for You: Madras Themes</h2>
																		<section id="section-tv-show-carousel-5f87f9fc02fb7" class="home-section section-tv-show-carousel">
																			<div class="container">
																				<div class="section-tv-show-carousel__inner">
																					<div class="tv-show-carousel">
																						<div class="tv-show-carousel__inner" data-ride="vodi-slick-carousel" data-wrap=".tv-shows__inner" data-slick="{&quot;slidesToShow&quot;:5,&quot;slidesToScroll&quot;:5,&quot;dots&quot;:true,&quot;arrows&quot;:false,&quot;autoplay&quot;:false,&quot;infinite&quot;:false,&quot;responsive&quot;:[{&quot;breakpoint&quot;:768,&quot;settings&quot;:{&quot;slidesToShow&quot;:2,&quot;slidesToScroll&quot;:1}},{&quot;breakpoint&quot;:992,&quot;settings&quot;:{&quot;slidesToShow&quot;:3,&quot;slidesToScroll&quot;:1}},{&quot;breakpoint&quot;:1200,&quot;settings&quot;:{&quot;slidesToShow&quot;:4,&quot;slidesToScroll&quot;:1}}]}">
																							<div class="masvideos masvideos-tv-shows ">
																								<div class="tv-shows columns-5">
																									<div class="tv-shows__inner">
																										<div class="tv-show post-2571 tv_show type-tv_show status-publish has-post-thumbnail hentry tv_show_genre-action tv_show_genre-drama tv_show_tag-brother tv_show_tag-brother-relationship tv_show_tag-kings tv_show_tag-vikings">
																											<div class="tv-show__poster">
																												<a href="/react2/tv_show/chicago-med-2/" class="masvideos-LoopTvShow-link masvideos-loop-tv-show__link tv-show__link">
																													<img width="970" height="550" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/42-limit.jpg" class="tv-show__poster--image tv_show__poster--image" alt="" srcset="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/42-limit.jpg 970w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/42-limit-270x153.jpg 270w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/42-limit-220x125.jpg 220w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/42-limit-324x184.jpg 324w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/42-limit-272x155.jpg 272w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/42-limit-214x122.jpg 214w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/42-limit-244x138.jpg 244w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/42-limit-339x192.jpg 339w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/42-limit-327x185.jpg 327w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/42-limit-300x170.jpg 300w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/42-limit-768x435.jpg 768w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/42-limit-120x67.jpg 120w" sizes="(max-width: 970px) 100vw, 970px" />
																												</a>
																											</div>
																											<div class="tv-show__body">
																												<div class="tv-show__info">
																													<div class="tv-show__info--head">
																														<div class="tv-show__meta">
																															<span class="tv-show__meta--genre">
																																<a href="/tv-show-genre/action/" rel="tag">Action</a>, <a href="/tv-show-genre/drama/" rel="tag">Drama</a>
																															</span>
																															<span class="tv-show__meta--release-year">2015 to 2016 &#8211; 2016</span>
																														</div>
																														<a href="/react2/tv_show/chicago-med-2/" class="masvideos-LoopTvShow-link masvideos-loop-tv-show__link tv-show__link">
																															<h3 class="masvideos-loop-tv-show__title  tv-show__title">Chicago Med</h3>
																														</a>
																														<div class="tv-show__episode">Newest Episode: <a href="/react2/episode/brothers-keeper-2/" class="tv-show__episode--link">S01E04</a>
																														</div>
																													</div> 
																													<div class="tv-show__short-description">
																														<div>
																															<p>An emotional thrill ride through the day-to-day chaos of the city&#8217;s most explosive hospital and the courageous team of doctors who hold it together.</p> 
																														</div> 
																													</div>
																													<div class="tv-show__actions">
																														<a href="/react2/tv_show/chicago-med-2/" class="tv-show-actions--link_watch">Watch Now</a> 
																														<div class="tv-show-actions--link_add-to-playlist dropdown">
																															<a class="dropdown-toggle" href="/react2/tv_show/chicago-med-2/" data-toggle="dropdown">+ Playlist</a>
																															<div class="dropdown-menu">
																																<a class="login-link" href="/login/tv-show-playlists/">Sign in to add this tv show to a playlist.</a> </div>
																															</div>
																														</div>
																													</div>
																													<div class="tv-show__review-info">
																														<div class="viewers-count">
																														</div>
																													</div>
																												</div> 
																												<div class="tv-show__hover-area">
																													<div class="tv-show__hover-area--inner">
																														<div class="tv-show__hover-area--poster">
																															<div class="tv-show__poster">
																																<a href="/react2/tv_show/chicago-med-2/" class="masvideos-LoopTvShow-link masvideos-loop-tv-show__link tv-show__link">
																																	<img width="970" height="550" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/42-limit.jpg" class="tv-show__poster--image tv_show__poster--image" alt="" srcset="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/42-limit.jpg 970w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/42-limit-270x153.jpg 270w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/42-limit-220x125.jpg 220w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/42-limit-324x184.jpg 324w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/42-limit-272x155.jpg 272w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/42-limit-214x122.jpg 214w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/42-limit-244x138.jpg 244w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/42-limit-339x192.jpg 339w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/42-limit-327x185.jpg 327w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/42-limit-300x170.jpg 300w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/42-limit-768x435.jpg 768w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/42-limit-120x67.jpg 120w" sizes="(max-width: 970px) 100vw, 970px" />
																																</a>
																															</div>
																															<div class="tv-show__info--head">
																																<div class="tv-show__meta">
																																	<span class="tv-show__meta--genre">
																																		<a href="/tv-show-genre/action/" rel="tag">Action</a>, <a href="/tv-show-genre/drama/" rel="tag">Drama</a>
																																	</span>
																																	<span class="tv-show__meta--release-year">2015 to 2016 &#8211; 2016</span>
																																</div>
																																<a href="/react2/tv_show/chicago-med-2/" class="masvideos-LoopTvShow-link masvideos-loop-tv-show__link tv-show__link">
																																	<h3 class="masvideos-loop-tv-show__title  tv-show__title">Chicago Med</h3>
																																</a>
																															</div>
																														</div>
																														<div class="tv-show__hover-area--body">
																															<div class="tv-show__season-info">
																																<div class="tv-show__seasons">Seasons #: <a href="/react2/tv_show/chicago-med-2/" class="tv-show__episode--link">Season 1</a>
																																	<a href="/react2/tv_show/chicago-med-2/" class="tv-show__episode--link">Season 2</a>
																																</div>
																																<div class="tv-show__episode">Newest Episode: <a href="/react2/episode/brothers-keeper-2/" class="tv-show__episode--link">S01E04</a>
																																</div>
																															</div>
																															<div class="tv-show__review-info"> 
																																<div class="tv-show__avg-rating">
																																</div>
																																<div class="viewers-count">
																																</div>
																															</div>
																															<div class="tv-show__actions">
																																<a href="/react2/tv_show/chicago-med-2/" class="tv-show-actions--link_watch">Watch Now</a> 
																																<div class="tv-show-actions--link_add-to-playlist dropdown">
																																	<a class="dropdown-toggle" href="/react2/tv_show/chicago-med-2/" data-toggle="dropdown">+ Playlist</a>
																																	<div class="dropdown-menu">
																																		<a class="login-link" href="/login/tv-show-playlists/">Sign in to add this tv show to a playlist.</a> </div>
																																	</div>
																																</div>
																															</div> </div>
																														</div>
																													</div>
																													<div class="tv-show post-2560 tv_show type-tv_show status-publish has-post-thumbnail hentry tv_show_genre-action tv_show_genre-comedy tv_show_genre-drama tv_show_tag-4k-ultra tv_show_tag-brother tv_show_tag-brother-relationship tv_show_tag-kings tv_show_tag-vikings">
																														<div class="tv-show__poster">
																															<a href="/react2/tv_show/the-last-man-on-the-earth/" class="masvideos-LoopTvShow-link masvideos-loop-tv-show__link tv-show__link">
																																<img width="970" height="550" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/41-light.jpg" class="tv-show__poster--image tv_show__poster--image" alt="" srcset="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/41-light.jpg 970w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/41-light-270x153.jpg 270w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/41-light-220x125.jpg 220w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/41-light-324x184.jpg 324w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/41-light-272x155.jpg 272w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/41-light-214x122.jpg 214w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/41-light-244x138.jpg 244w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/41-light-339x192.jpg 339w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/41-light-327x185.jpg 327w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/41-light-300x170.jpg 300w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/41-light-768x435.jpg 768w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/41-light-120x67.jpg 120w" sizes="(max-width: 970px) 100vw, 970px" />
																															</a>
																														</div>
																														<div class="tv-show__body">
																															<div class="tv-show__info">
																																<div class="tv-show__info--head">
																																	<div class="tv-show__meta">
																																		<span class="tv-show__meta--genre">
																																			<a href="/tv-show-genre/action/" rel="tag">Action</a>, <a href="/tv-show-genre/comedy/" rel="tag">Comedy</a>, <a href="/tv-show-genre/drama/" rel="tag">Drama</a>
																																		</span>
																																		<span class="tv-show__meta--release-year">2015 &#8211; 2015</span>
																																	</div>
																																	<a href="/react2/tv_show/the-last-man-on-the-earth/" class="masvideos-LoopTvShow-link masvideos-loop-tv-show__link tv-show__link">
																																		<h3 class="masvideos-loop-tv-show__title  tv-show__title">The Last Man on the earth</h3>
																																	</a>
																																	<div class="tv-show__episode">Newest Episode: <a href="/react2/episode/crickets/" class="tv-show__episode--link">S02E05</a>
																																	</div>
																																</div> 
																																<div class="tv-show__short-description">
																																	<div>
																																		<p>Almost two years after a virus wiped out most of the human race, Phil Miller only wishes for some company, but soon gets more than he bargained for when that company shows up in the form of other survivors.</p> 
																																	</div> 
																																</div>
																																<div class="tv-show__actions">
																																	<a href="/react2/tv_show/the-last-man-on-the-earth/" class="tv-show-actions--link_watch">Watch Now</a> 
																																	<div class="tv-show-actions--link_add-to-playlist dropdown">
																																		<a class="dropdown-toggle" href="/react2/tv_show/the-last-man-on-the-earth/" data-toggle="dropdown">+ Playlist</a>
																																		<div class="dropdown-menu">
																																			<a class="login-link" href="/login/tv-show-playlists/">Sign in to add this tv show to a playlist.</a> </div>
																																		</div>
																																	</div>
																																</div>
																																<div class="tv-show__review-info"> <a href="/react2/tv_show/the-last-man-on-the-earth/#reviews" class="avg-rating">
																																	<span class="rating-with-count">
																																		<svg class="vodi-svg" width="40px" height="39px" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 39">
																																			<title>play</title>
																																			<path fill-rule="evenodd" d="M19.633,-0.000 C21.509,0.035 21.530,1.174 22.167,2.414 C23.329,4.679 24.406,7.067 25.572,9.338 C25.853,9.886 26.431,11.640 26.918,11.834 C27.486,12.203 29.345,12.109 30.165,12.316 C32.170,12.825 34.489,12.860 36.500,13.364 C37.516,13.618 38.689,13.413 39.430,13.927 C39.689,14.107 39.770,14.504 39.984,14.732 C40.047,16.499 39.096,16.843 38.163,17.792 C36.473,19.509 34.784,21.227 33.095,22.944 C32.585,23.462 31.092,24.543 31.036,25.359 C31.423,25.951 31.307,27.455 31.511,28.258 C32.138,30.727 32.213,33.522 32.857,35.987 C33.142,37.078 33.016,38.241 32.303,38.724 C31.108,39.533 29.632,38.193 28.819,37.758 C26.695,36.623 24.601,35.624 22.483,34.457 C21.979,34.179 20.607,33.178 20.108,33.088 C19.748,33.023 18.163,34.107 17.812,34.296 C15.557,35.505 13.340,36.640 11.080,37.839 C10.548,38.120 9.180,39.226 8.309,38.966 C6.955,38.558 6.874,36.993 7.280,35.423 C7.716,33.733 7.697,31.880 8.151,30.109 C8.527,28.642 8.907,26.529 9.022,24.957 C8.092,24.344 7.202,23.107 6.408,22.300 C4.760,20.625 3.059,18.990 1.340,17.389 C0.646,16.742 -0.578,15.515 0.311,14.249 C0.915,13.388 2.364,13.656 3.557,13.364 C6.678,12.599 10.114,12.468 13.298,11.834 C14.186,9.747 15.306,7.711 16.307,5.716 C16.954,4.426 17.496,3.163 18.128,1.931 C18.334,1.531 18.358,1.093 18.603,0.724 C18.845,0.362 19.299,0.273 19.633,-0.000 Z" />
																																		</svg> <span class="avg-rating-number"> 6.0</span>
																																	</span>
																																	<span class="rating-number-with-text">
																																		<span class="avg-rating-number"> 6.0</span>
																																		<span class="avg-rating-text">
																																			<span>1</span> Vote </span>
																																		</span>
																																	</a>
																																	<div class="viewers-count">
																																	</div>
																																</div>
																															</div> 
																															<div class="tv-show__hover-area">
																																<div class="tv-show__hover-area--inner">
																																	<div class="tv-show__hover-area--poster">
																																		<div class="tv-show__poster">
																																			<a href="/react2/tv_show/the-last-man-on-the-earth/" class="masvideos-LoopTvShow-link masvideos-loop-tv-show__link tv-show__link">
																																				<img width="970" height="550" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/41-light.jpg" class="tv-show__poster--image tv_show__poster--image" alt="" srcset="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/41-light.jpg 970w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/41-light-270x153.jpg 270w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/41-light-220x125.jpg 220w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/41-light-324x184.jpg 324w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/41-light-272x155.jpg 272w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/41-light-214x122.jpg 214w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/41-light-244x138.jpg 244w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/41-light-339x192.jpg 339w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/41-light-327x185.jpg 327w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/41-light-300x170.jpg 300w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/41-light-768x435.jpg 768w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/41-light-120x67.jpg 120w" sizes="(max-width: 970px) 100vw, 970px" />
																																			</a>
																																		</div>
																																		<div class="tv-show__info--head">
																																			<div class="tv-show__meta">
																																				<span class="tv-show__meta--genre">
																																					<a href="/tv-show-genre/action/" rel="tag">Action</a>, <a href="/tv-show-genre/comedy/" rel="tag">Comedy</a>, <a href="/tv-show-genre/drama/" rel="tag">Drama</a>
																																				</span>
																																				<span class="tv-show__meta--release-year">2015 &#8211; 2015</span>
																																			</div>
																																			<a href="/react2/tv_show/the-last-man-on-the-earth/" class="masvideos-LoopTvShow-link masvideos-loop-tv-show__link tv-show__link">
																																				<h3 class="masvideos-loop-tv-show__title  tv-show__title">The Last Man on the earth</h3>
																																			</a>
																																		</div>
																																	</div>
																																	<div class="tv-show__hover-area--body">
																																		<div class="tv-show__season-info">
																																			<div class="tv-show__seasons">Seasons #: <a href="/react2/tv_show/the-last-man-on-the-earth/" class="tv-show__episode--link">Season 1</a>
																																				<a href="/react2/tv_show/the-last-man-on-the-earth/" class="tv-show__episode--link">Season 2</a>
																																			</div>
																																			<div class="tv-show__episode">Newest Episode: <a href="/react2/episode/crickets/" class="tv-show__episode--link">S02E05</a>
																																			</div>
																																		</div>
																																		<div class="tv-show__review-info"> 
																																			<div class="tv-show__avg-rating">
																																				<a href="/react2/tv_show/the-last-man-on-the-earth/#reviews" class="avg-rating">
																																					<div class="avg-rating__inner">
																																						<span class="avg-rating__number"> 6.0</span>
																																						<span class="avg-rating__text">
																																							<span>1</span> Vote </span>
																																						</div>
																																					</a>
																																				</div>
																																				<div class="viewers-count">
																																				</div>
																																			</div>
																																			<div class="tv-show__actions">
																																				<a href="/react2/tv_show/the-last-man-on-the-earth/" class="tv-show-actions--link_watch">Watch Now</a> 
																																				<div class="tv-show-actions--link_add-to-playlist dropdown">
																																					<a class="dropdown-toggle" href="/react2/tv_show/the-last-man-on-the-earth/" data-toggle="dropdown">+ Playlist</a>
																																					<div class="dropdown-menu">
																																						<a class="login-link" href="/login/tv-show-playlists/">Sign in to add this tv show to a playlist.</a> </div>
																																					</div>
																																				</div>
																																			</div> </div>
																																		</div>
																																	</div>
																																	<div class="tv-show post-2547 tv_show type-tv_show status-publish has-post-thumbnail hentry tv_show_genre-comedy tv_show_genre-drama tv_show_tag-4k-ultra tv_show_tag-brother tv_show_tag-brother-relationship tv_show_tag-king tv_show_tag-vikings">
																																		<div class="tv-show__poster">
																																			<a href="/react2/tv_show/unbreakable-kimmy-schmidt/" class="masvideos-LoopTvShow-link masvideos-loop-tv-show__link tv-show__link">
																																				<img width="970" height="550" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/40-keep-going.jpg" class="tv-show__poster--image tv_show__poster--image" alt="" srcset="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/40-keep-going.jpg 970w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/40-keep-going-270x153.jpg 270w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/40-keep-going-220x125.jpg 220w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/40-keep-going-324x184.jpg 324w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/40-keep-going-272x155.jpg 272w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/40-keep-going-214x122.jpg 214w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/40-keep-going-244x138.jpg 244w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/40-keep-going-339x192.jpg 339w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/40-keep-going-327x185.jpg 327w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/40-keep-going-300x170.jpg 300w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/40-keep-going-768x435.jpg 768w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/40-keep-going-120x67.jpg 120w" sizes="(max-width: 970px) 100vw, 970px" />
																																			</a>
																																		</div>
																																		<div class="tv-show__body">
																																			<div class="tv-show__info">
																																				<div class="tv-show__info--head">
																																					<div class="tv-show__meta">
																																						<span class="tv-show__meta--genre">
																																							<a href="/tv-show-genre/comedy/" rel="tag">Comedy</a>, <a href="/tv-show-genre/drama/" rel="tag">Drama</a>
																																						</span>
																																						<span class="tv-show__meta--release-year">2015 &#8211; 2016</span>
																																					</div>
																																					<a href="/react2/tv_show/unbreakable-kimmy-schmidt/" class="masvideos-LoopTvShow-link masvideos-loop-tv-show__link tv-show__link">
																																						<h3 class="masvideos-loop-tv-show__title  tv-show__title">Unbreakable Kimmy Schmidt</h3>
																																					</a>
																																					<div class="tv-show__episode">Newest Episode: <a href="/react2/episode/kimmy-gives-up/" class="tv-show__episode--link">S02E05</a>
																																					</div>
																																				</div> 
																																				<div class="tv-show__short-description">
																																					<div>
																																						<p>Praesent iaculis, purus ac vehicula mattis, arcu lorem blandit nisl, non laoreet dui mi eget elit. Donec porttitor ex vel augue maximus luctus. Vivamus finibus nibh eu nunc volutpat suscipit. Nam vulputate libero quis nisi euismod rhoncus. Sed eu euismod felis. Aenean ullamcorper dapibus odio ac tempor. Aliquam iaculis, quam vitae imperdiet consectetur, mi ante semper metus, ac efficitur nisi justo ut eros. Maecenas suscipit turpis fermentum elementum scelerisque. </p>
																																						<p>Sed leo elit, volutpat quis aliquet eu, elementum eget arcu. Aenean ligula tellus, malesuada eu ultrices vel, vulputate sit amet metus. Donec tincidunt sapien ut enim feugiat, sed egestas dolor ornare.</p> 
																																					</div> 
																																				</div>
																																				<div class="tv-show__actions">
																																					<a href="/react2/tv_show/unbreakable-kimmy-schmidt/" class="tv-show-actions--link_watch">Watch Now</a> 
																																					<div class="tv-show-actions--link_add-to-playlist dropdown">
																																						<a class="dropdown-toggle" href="/react2/tv_show/unbreakable-kimmy-schmidt/" data-toggle="dropdown">+ Playlist</a>
																																						<div class="dropdown-menu">
																																							<a class="login-link" href="/login/tv-show-playlists/">Sign in to add this tv show to a playlist.</a> </div>
																																						</div>
																																					</div>
																																				</div>
																																				<div class="tv-show__review-info">
																																					<div class="viewers-count">
																																					</div>
																																				</div>
																																			</div> 
																																			<div class="tv-show__hover-area">
																																				<div class="tv-show__hover-area--inner">
																																					<div class="tv-show__hover-area--poster">
																																						<div class="tv-show__poster">
																																							<a href="/react2/tv_show/unbreakable-kimmy-schmidt/" class="masvideos-LoopTvShow-link masvideos-loop-tv-show__link tv-show__link">
																																								<img width="970" height="550" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/40-keep-going.jpg" class="tv-show__poster--image tv_show__poster--image" alt="" srcset="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/40-keep-going.jpg 970w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/40-keep-going-270x153.jpg 270w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/40-keep-going-220x125.jpg 220w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/40-keep-going-324x184.jpg 324w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/40-keep-going-272x155.jpg 272w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/40-keep-going-214x122.jpg 214w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/40-keep-going-244x138.jpg 244w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/40-keep-going-339x192.jpg 339w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/40-keep-going-327x185.jpg 327w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/40-keep-going-300x170.jpg 300w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/40-keep-going-768x435.jpg 768w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/40-keep-going-120x67.jpg 120w" sizes="(max-width: 970px) 100vw, 970px" />
																																							</a>
																																						</div>
																																						<div class="tv-show__info--head">
																																							<div class="tv-show__meta">
																																								<span class="tv-show__meta--genre">
																																									<a href="/tv-show-genre/comedy/" rel="tag">Comedy</a>, <a href="/tv-show-genre/drama/" rel="tag">Drama</a>
																																								</span>
																																								<span class="tv-show__meta--release-year">2015 &#8211; 2016</span>
																																							</div>
																																							<a href="/react2/tv_show/unbreakable-kimmy-schmidt/" class="masvideos-LoopTvShow-link masvideos-loop-tv-show__link tv-show__link">
																																								<h3 class="masvideos-loop-tv-show__title  tv-show__title">Unbreakable Kimmy Schmidt</h3>
																																							</a>
																																						</div>
																																					</div>
																																					<div class="tv-show__hover-area--body">
																																						<div class="tv-show__season-info">
																																							<div class="tv-show__seasons">Seasons #: <a href="/react2/tv_show/unbreakable-kimmy-schmidt/" class="tv-show__episode--link">Season 1</a>
																																								<a href="/react2/tv_show/unbreakable-kimmy-schmidt/" class="tv-show__episode--link">Season 2</a>
																																							</div>
																																							<div class="tv-show__episode">Newest Episode: <a href="/react2/episode/kimmy-gives-up/" class="tv-show__episode--link">S02E05</a>
																																							</div>
																																						</div>
																																						<div class="tv-show__review-info"> 
																																							<div class="tv-show__avg-rating">
																																							</div>
																																							<div class="viewers-count">
																																							</div>
																																						</div>
																																						<div class="tv-show__actions">
																																							<a href="/react2/tv_show/unbreakable-kimmy-schmidt/" class="tv-show-actions--link_watch">Watch Now</a> 
																																							<div class="tv-show-actions--link_add-to-playlist dropdown">
																																								<a class="dropdown-toggle" href="/react2/tv_show/unbreakable-kimmy-schmidt/" data-toggle="dropdown">+ Playlist</a>
																																								<div class="dropdown-menu">
																																									<a class="login-link" href="/login/tv-show-playlists/">Sign in to add this tv show to a playlist.</a> </div>
																																								</div>
																																							</div>
																																						</div> </div>
																																					</div>
																																				</div>
																																				<div class="tv-show post-2534 tv_show type-tv_show status-publish has-post-thumbnail hentry tv_show_genre-action tv_show_genre-drama tv_show_tag-brother tv_show_tag-brother-relationship tv_show_tag-king tv_show_tag-premieres tv_show_tag-vikings">
																																					<div class="tv-show__poster">
																																						<a href="/react2/tv_show/house-of-cards/" class="masvideos-LoopTvShow-link masvideos-loop-tv-show__link tv-show__link">
																																							<img width="970" height="550" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/39-journey.jpg" class="tv-show__poster--image tv_show__poster--image" alt="" srcset="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/39-journey.jpg 970w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/39-journey-270x153.jpg 270w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/39-journey-220x125.jpg 220w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/39-journey-324x184.jpg 324w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/39-journey-272x155.jpg 272w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/39-journey-214x122.jpg 214w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/39-journey-244x138.jpg 244w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/39-journey-339x192.jpg 339w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/39-journey-327x185.jpg 327w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/39-journey-300x170.jpg 300w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/39-journey-768x435.jpg 768w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/39-journey-120x67.jpg 120w" sizes="(max-width: 970px) 100vw, 970px" />
																																						</a>
																																					</div>
																																					<div class="tv-show__body">
																																						<div class="tv-show__info">
																																							<div class="tv-show__info--head">
																																								<div class="tv-show__meta">
																																									<span class="tv-show__meta--genre">
																																										<a href="/tv-show-genre/action/" rel="tag">Action</a>, <a href="/tv-show-genre/drama/" rel="tag">Drama</a>
																																									</span>
																																									<span class="tv-show__meta--release-year">2013 &#8211; 2015</span>
																																								</div>
																																								<a href="/react2/tv_show/house-of-cards/" class="masvideos-LoopTvShow-link masvideos-loop-tv-show__link tv-show__link">
																																									<h3 class="masvideos-loop-tv-show__title  tv-show__title">House of cards</h3>
																																								</a>
																																								<div class="tv-show__episode">Newest Episode: <a href="/react2/episode/chapter-18/" class="tv-show__episode--link">S02E05</a>
																																								</div>
																																							</div> 
																																							<div class="tv-show__short-description">
																																								<div>
																																									<p>A Congressman works with his equally conniving wife to exact revenge on the people who betrayed him.</p> 
																																								</div> 
																																							</div>
																																							<div class="tv-show__actions">
																																								<a href="/react2/tv_show/house-of-cards/" class="tv-show-actions--link_watch">Watch Now</a> 
																																								<div class="tv-show-actions--link_add-to-playlist dropdown">
																																									<a class="dropdown-toggle" href="/react2/tv_show/house-of-cards/" data-toggle="dropdown">+ Playlist</a>
																																									<div class="dropdown-menu">
																																										<a class="login-link" href="/login/tv-show-playlists/">Sign in to add this tv show to a playlist.</a> </div>
																																									</div>
																																								</div>
																																							</div>
																																							<div class="tv-show__review-info"> <a href="/react2/tv_show/house-of-cards/#reviews" class="avg-rating">
																																								<span class="rating-with-count">
																																									<svg class="vodi-svg" width="40px" height="39px" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 39">
																																										<title>play</title>
																																										<path fill-rule="evenodd" d="M19.633,-0.000 C21.509,0.035 21.530,1.174 22.167,2.414 C23.329,4.679 24.406,7.067 25.572,9.338 C25.853,9.886 26.431,11.640 26.918,11.834 C27.486,12.203 29.345,12.109 30.165,12.316 C32.170,12.825 34.489,12.860 36.500,13.364 C37.516,13.618 38.689,13.413 39.430,13.927 C39.689,14.107 39.770,14.504 39.984,14.732 C40.047,16.499 39.096,16.843 38.163,17.792 C36.473,19.509 34.784,21.227 33.095,22.944 C32.585,23.462 31.092,24.543 31.036,25.359 C31.423,25.951 31.307,27.455 31.511,28.258 C32.138,30.727 32.213,33.522 32.857,35.987 C33.142,37.078 33.016,38.241 32.303,38.724 C31.108,39.533 29.632,38.193 28.819,37.758 C26.695,36.623 24.601,35.624 22.483,34.457 C21.979,34.179 20.607,33.178 20.108,33.088 C19.748,33.023 18.163,34.107 17.812,34.296 C15.557,35.505 13.340,36.640 11.080,37.839 C10.548,38.120 9.180,39.226 8.309,38.966 C6.955,38.558 6.874,36.993 7.280,35.423 C7.716,33.733 7.697,31.880 8.151,30.109 C8.527,28.642 8.907,26.529 9.022,24.957 C8.092,24.344 7.202,23.107 6.408,22.300 C4.760,20.625 3.059,18.990 1.340,17.389 C0.646,16.742 -0.578,15.515 0.311,14.249 C0.915,13.388 2.364,13.656 3.557,13.364 C6.678,12.599 10.114,12.468 13.298,11.834 C14.186,9.747 15.306,7.711 16.307,5.716 C16.954,4.426 17.496,3.163 18.128,1.931 C18.334,1.531 18.358,1.093 18.603,0.724 C18.845,0.362 19.299,0.273 19.633,-0.000 Z" />
																																									</svg> <span class="avg-rating-number"> 8.0</span>
																																								</span>
																																								<span class="rating-number-with-text">
																																									<span class="avg-rating-number"> 8.0</span>
																																									<span class="avg-rating-text">
																																										<span>1</span> Vote </span>
																																									</span>
																																								</a>
																																								<div class="viewers-count">
																																								</div>
																																							</div>
																																						</div> 
																																						<div class="tv-show__hover-area">
																																							<div class="tv-show__hover-area--inner">
																																								<div class="tv-show__hover-area--poster">
																																									<div class="tv-show__poster">
																																										<a href="/react2/tv_show/house-of-cards/" class="masvideos-LoopTvShow-link masvideos-loop-tv-show__link tv-show__link">
																																											<img width="970" height="550" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/39-journey.jpg" class="tv-show__poster--image tv_show__poster--image" alt="" srcset="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/39-journey.jpg 970w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/39-journey-270x153.jpg 270w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/39-journey-220x125.jpg 220w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/39-journey-324x184.jpg 324w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/39-journey-272x155.jpg 272w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/39-journey-214x122.jpg 214w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/39-journey-244x138.jpg 244w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/39-journey-339x192.jpg 339w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/39-journey-327x185.jpg 327w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/39-journey-300x170.jpg 300w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/39-journey-768x435.jpg 768w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/39-journey-120x67.jpg 120w" sizes="(max-width: 970px) 100vw, 970px" />
																																										</a>
																																									</div>
																																									<div class="tv-show__info--head">
																																										<div class="tv-show__meta">
																																											<span class="tv-show__meta--genre">
																																												<a href="/tv-show-genre/action/" rel="tag">Action</a>, <a href="/tv-show-genre/drama/" rel="tag">Drama</a>
																																											</span>
																																											<span class="tv-show__meta--release-year">2013 &#8211; 2015</span>
																																										</div>
																																										<a href="/react2/tv_show/house-of-cards/" class="masvideos-LoopTvShow-link masvideos-loop-tv-show__link tv-show__link">
																																											<h3 class="masvideos-loop-tv-show__title  tv-show__title">House of cards</h3>
																																										</a>
																																									</div>
																																								</div>
																																								<div class="tv-show__hover-area--body">
																																									<div class="tv-show__season-info">
																																										<div class="tv-show__seasons">Seasons #: <a href="/react2/tv_show/house-of-cards/" class="tv-show__episode--link">Season 1</a>
																																											<a href="/react2/tv_show/house-of-cards/" class="tv-show__episode--link">Season 2</a>
																																										</div>
																																										<div class="tv-show__episode">Newest Episode: <a href="/react2/episode/chapter-18/" class="tv-show__episode--link">S02E05</a>
																																										</div>
																																									</div>
																																									<div class="tv-show__review-info"> 
																																										<div class="tv-show__avg-rating">
																																											<a href="/react2/tv_show/house-of-cards/#reviews" class="avg-rating">
																																												<div class="avg-rating__inner">
																																													<span class="avg-rating__number"> 8.0</span>
																																													<span class="avg-rating__text">
																																														<span>1</span> Vote </span>
																																													</div>
																																												</a>
																																											</div>
																																											<div class="viewers-count">
																																											</div>
																																										</div>
																																										<div class="tv-show__actions">
																																											<a href="/react2/tv_show/house-of-cards/" class="tv-show-actions--link_watch">Watch Now</a> 
																																											<div class="tv-show-actions--link_add-to-playlist dropdown">
																																												<a class="dropdown-toggle" href="/react2/tv_show/house-of-cards/" data-toggle="dropdown">+ Playlist</a>
																																												<div class="dropdown-menu">
																																													<a class="login-link" href="/login/tv-show-playlists/">Sign in to add this tv show to a playlist.</a> </div>
																																												</div>
																																											</div>
																																										</div> </div>
																																									</div>
																																								</div>
																																								<div class="tv-show post-2521 tv_show type-tv_show status-publish has-post-thumbnail hentry tv_show_genre-drama tv_show_genre-romance tv_show_tag-brother tv_show_tag-brother-relationship tv_show_tag-kings tv_show_tag-vikings">
																																									<div class="tv-show__poster">
																																										<a href="/react2/tv_show/greys-anatomy/" class="masvideos-LoopTvShow-link masvideos-loop-tv-show__link tv-show__link">
																																											<img width="970" height="550" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/38-holidays.jpg" class="tv-show__poster--image tv_show__poster--image" alt="" srcset="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/38-holidays.jpg 970w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/38-holidays-270x153.jpg 270w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/38-holidays-220x125.jpg 220w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/38-holidays-324x184.jpg 324w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/38-holidays-272x155.jpg 272w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/38-holidays-214x122.jpg 214w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/38-holidays-244x138.jpg 244w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/38-holidays-339x192.jpg 339w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/38-holidays-327x185.jpg 327w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/38-holidays-300x170.jpg 300w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/38-holidays-768x435.jpg 768w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/38-holidays-120x67.jpg 120w" sizes="(max-width: 970px) 100vw, 970px" />
																																										</a>
																																									</div>
																																									<div class="tv-show__body">
																																										<div class="tv-show__info">
																																											<div class="tv-show__info--head">
																																												<div class="tv-show__meta">
																																													<span class="tv-show__meta--genre">
																																														<a href="/tv-show-genre/drama/" rel="tag">Drama</a>, <a href="/tv-show-genre/romance/" rel="tag">Romance</a>
																																													</span>
																																													<span class="tv-show__meta--release-year">2005 &#8211; 2005</span>
																																												</div>
																																												<a href="/react2/tv_show/greys-anatomy/" class="masvideos-LoopTvShow-link masvideos-loop-tv-show__link tv-show__link">
																																													<h3 class="masvideos-loop-tv-show__title  tv-show__title">Grey&#039;s anatomy</h3>
																																												</a>
																																												<div class="tv-show__episode">Newest Episode: <a href="/react2/episode/bring-the-pain/" class="tv-show__episode--link">S02E05</a>
																																												</div>
																																											</div> 
																																											<div class="tv-show__short-description">
																																												<div>
																																													<p>A drama centered on the personal and professional lives of five surgical interns and their supervisors.</p> 
																																												</div> 
																																											</div>
																																											<div class="tv-show__actions">
																																												<a href="/react2/tv_show/greys-anatomy/" class="tv-show-actions--link_watch">Watch Now</a> 
																																												<div class="tv-show-actions--link_add-to-playlist dropdown">
																																													<a class="dropdown-toggle" href="/react2/tv_show/greys-anatomy/" data-toggle="dropdown">+ Playlist</a>
																																													<div class="dropdown-menu">
																																														<a class="login-link" href="/login/tv-show-playlists/">Sign in to add this tv show to a playlist.</a> </div>
																																													</div>
																																												</div>
																																											</div>
																																											<div class="tv-show__review-info"> <a href="/react2/tv_show/greys-anatomy/#reviews" class="avg-rating">
																																												<span class="rating-with-count">
																																													<svg class="vodi-svg" width="40px" height="39px" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 39">
																																														<title>play</title>
																																														<path fill-rule="evenodd" d="M19.633,-0.000 C21.509,0.035 21.530,1.174 22.167,2.414 C23.329,4.679 24.406,7.067 25.572,9.338 C25.853,9.886 26.431,11.640 26.918,11.834 C27.486,12.203 29.345,12.109 30.165,12.316 C32.170,12.825 34.489,12.860 36.500,13.364 C37.516,13.618 38.689,13.413 39.430,13.927 C39.689,14.107 39.770,14.504 39.984,14.732 C40.047,16.499 39.096,16.843 38.163,17.792 C36.473,19.509 34.784,21.227 33.095,22.944 C32.585,23.462 31.092,24.543 31.036,25.359 C31.423,25.951 31.307,27.455 31.511,28.258 C32.138,30.727 32.213,33.522 32.857,35.987 C33.142,37.078 33.016,38.241 32.303,38.724 C31.108,39.533 29.632,38.193 28.819,37.758 C26.695,36.623 24.601,35.624 22.483,34.457 C21.979,34.179 20.607,33.178 20.108,33.088 C19.748,33.023 18.163,34.107 17.812,34.296 C15.557,35.505 13.340,36.640 11.080,37.839 C10.548,38.120 9.180,39.226 8.309,38.966 C6.955,38.558 6.874,36.993 7.280,35.423 C7.716,33.733 7.697,31.880 8.151,30.109 C8.527,28.642 8.907,26.529 9.022,24.957 C8.092,24.344 7.202,23.107 6.408,22.300 C4.760,20.625 3.059,18.990 1.340,17.389 C0.646,16.742 -0.578,15.515 0.311,14.249 C0.915,13.388 2.364,13.656 3.557,13.364 C6.678,12.599 10.114,12.468 13.298,11.834 C14.186,9.747 15.306,7.711 16.307,5.716 C16.954,4.426 17.496,3.163 18.128,1.931 C18.334,1.531 18.358,1.093 18.603,0.724 C18.845,0.362 19.299,0.273 19.633,-0.000 Z" />
																																													</svg> <span class="avg-rating-number"> 8.0</span>
																																												</span>
																																												<span class="rating-number-with-text">
																																													<span class="avg-rating-number"> 8.0</span>
																																													<span class="avg-rating-text">
																																														<span>1</span> Vote </span>
																																													</span>
																																												</a>
																																												<div class="viewers-count">
																																												</div>
																																											</div>
																																										</div> 
																																										<div class="tv-show__hover-area">
																																											<div class="tv-show__hover-area--inner">
																																												<div class="tv-show__hover-area--poster">
																																													<div class="tv-show__poster">
																																														<a href="/react2/tv_show/greys-anatomy/" class="masvideos-LoopTvShow-link masvideos-loop-tv-show__link tv-show__link">
																																															<img width="970" height="550" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/38-holidays.jpg" class="tv-show__poster--image tv_show__poster--image" alt="" srcset="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/38-holidays.jpg 970w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/38-holidays-270x153.jpg 270w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/38-holidays-220x125.jpg 220w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/38-holidays-324x184.jpg 324w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/38-holidays-272x155.jpg 272w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/38-holidays-214x122.jpg 214w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/38-holidays-244x138.jpg 244w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/38-holidays-339x192.jpg 339w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/38-holidays-327x185.jpg 327w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/38-holidays-300x170.jpg 300w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/38-holidays-768x435.jpg 768w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/38-holidays-120x67.jpg 120w" sizes="(max-width: 970px) 100vw, 970px" />
																																														</a>
																																													</div>
																																													<div class="tv-show__info--head">
																																														<div class="tv-show__meta">
																																															<span class="tv-show__meta--genre">
																																																<a href="/tv-show-genre/drama/" rel="tag">Drama</a>, <a href="/tv-show-genre/romance/" rel="tag">Romance</a>
																																															</span>
																																															<span class="tv-show__meta--release-year">2005 &#8211; 2005</span>
																																														</div>
																																														<a href="/react2/tv_show/greys-anatomy/" class="masvideos-LoopTvShow-link masvideos-loop-tv-show__link tv-show__link">
																																															<h3 class="masvideos-loop-tv-show__title  tv-show__title">Grey&#039;s anatomy</h3>
																																														</a>
																																													</div>
																																												</div>
																																												<div class="tv-show__hover-area--body">
																																													<div class="tv-show__season-info">
																																														<div class="tv-show__seasons">Seasons #: <a href="/react2/tv_show/greys-anatomy/" class="tv-show__episode--link">Season 1</a>
																																															<a href="/react2/tv_show/greys-anatomy/" class="tv-show__episode--link">Season 2</a>
																																														</div>
																																														<div class="tv-show__episode">Newest Episode: <a href="/react2/episode/bring-the-pain/" class="tv-show__episode--link">S02E05</a>
																																														</div>
																																													</div>
																																													<div class="tv-show__review-info"> 
																																														<div class="tv-show__avg-rating">
																																															<a href="/react2/tv_show/greys-anatomy/#reviews" class="avg-rating">
																																																<div class="avg-rating__inner">
																																																	<span class="avg-rating__number"> 8.0</span>
																																																	<span class="avg-rating__text">
																																																		<span>1</span> Vote </span>
																																																	</div>
																																																</a>
																																															</div>
																																															<div class="viewers-count">
																																															</div>
																																														</div>
																																														<div class="tv-show__actions">
																																															<a href="/react2/tv_show/greys-anatomy/" class="tv-show-actions--link_watch">Watch Now</a> 
																																															<div class="tv-show-actions--link_add-to-playlist dropdown">
																																																<a class="dropdown-toggle" href="/react2/tv_show/greys-anatomy/" data-toggle="dropdown">+ Playlist</a>
																																																<div class="dropdown-menu">
																																																	<a class="login-link" href="/login/tv-show-playlists/">Sign in to add this tv show to a playlist.</a> </div>
																																																</div>
																																															</div>
																																														</div> </div>
																																													</div>
																																												</div>
																																												<div class="tv-show post-2508 tv_show type-tv_show status-publish has-post-thumbnail hentry tv_show_genre-drama tv_show_genre-fantasy tv_show_tag-brother tv_show_tag-brother-relationship tv_show_tag-kings tv_show_tag-vikings">
																																													<div class="tv-show__poster">
																																														<a href="/react2/tv_show/dom-grozypeeny-dreadful/" class="masvideos-LoopTvShow-link masvideos-loop-tv-show__link tv-show__link">
																																															<img width="970" height="550" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/37-hater.jpg" class="tv-show__poster--image tv_show__poster--image" alt="" srcset="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/37-hater.jpg 970w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/37-hater-270x153.jpg 270w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/37-hater-220x125.jpg 220w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/37-hater-324x184.jpg 324w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/37-hater-272x155.jpg 272w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/37-hater-214x122.jpg 214w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/37-hater-244x138.jpg 244w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/37-hater-339x192.jpg 339w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/37-hater-327x185.jpg 327w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/37-hater-300x170.jpg 300w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/37-hater-768x435.jpg 768w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/37-hater-120x67.jpg 120w" sizes="(max-width: 970px) 100vw, 970px" />
																																														</a>
																																													</div>
																																													<div class="tv-show__body">
																																														<div class="tv-show__info">
																																															<div class="tv-show__info--head">
																																																<div class="tv-show__meta">
																																																	<span class="tv-show__meta--genre">
																																																		<a href="/tv-show-genre/drama/" rel="tag">Drama</a>, <a href="/tv-show-genre/fantasy/" rel="tag">Fantasy</a>
																																																	</span>
																																																	<span class="tv-show__meta--release-year">2014 &#8211; 2015</span>
																																																</div>
																																																<a href="/react2/tv_show/dom-grozypeeny-dreadful/" class="masvideos-LoopTvShow-link masvideos-loop-tv-show__link tv-show__link">
																																																	<h3 class="masvideos-loop-tv-show__title  tv-show__title">Dom grozy(Peeny Dreadful)</h3>
																																																</a>
																																																<div class="tv-show__episode">Newest Episode: <a href="/react2/episode/little-scorpion/" class="tv-show__episode--link">S02E05</a>
																																																</div>
																																															</div> 
																																															<div class="tv-show__short-description">
																																																<div>
																																																	<p>Explorer Sir Malcolm Murray, American gunslinger Ethan Chandler, scientist Victor Frankenstein, and medium Vanessa Ives unite to combat supernatural threats in Victorian London.</p> 
																																																</div> 
																																															</div>
																																															<div class="tv-show__actions">
																																																<a href="/react2/tv_show/dom-grozypeeny-dreadful/" class="tv-show-actions--link_watch">Watch Now</a> 
																																																<div class="tv-show-actions--link_add-to-playlist dropdown">
																																																	<a class="dropdown-toggle" href="/react2/tv_show/dom-grozypeeny-dreadful/" data-toggle="dropdown">+ Playlist</a>
																																																	<div class="dropdown-menu">
																																																		<a class="login-link" href="/login/tv-show-playlists/">Sign in to add this tv show to a playlist.</a> </div>
																																																	</div>
																																																</div>
																																															</div>
																																															<div class="tv-show__review-info">
																																																<div class="viewers-count">
																																																</div>
																																															</div>
																																														</div> 
																																														<div class="tv-show__hover-area">
																																															<div class="tv-show__hover-area--inner">
																																																<div class="tv-show__hover-area--poster">
																																																	<div class="tv-show__poster">
																																																		<a href="/react2/tv_show/dom-grozypeeny-dreadful/" class="masvideos-LoopTvShow-link masvideos-loop-tv-show__link tv-show__link">
																																																			<img width="970" height="550" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/37-hater.jpg" class="tv-show__poster--image tv_show__poster--image" alt="" srcset="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/37-hater.jpg 970w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/37-hater-270x153.jpg 270w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/37-hater-220x125.jpg 220w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/37-hater-324x184.jpg 324w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/37-hater-272x155.jpg 272w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/37-hater-214x122.jpg 214w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/37-hater-244x138.jpg 244w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/37-hater-339x192.jpg 339w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/37-hater-327x185.jpg 327w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/37-hater-300x170.jpg 300w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/37-hater-768x435.jpg 768w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/37-hater-120x67.jpg 120w" sizes="(max-width: 970px) 100vw, 970px" />
																																																		</a>
																																																	</div>
																																																	<div class="tv-show__info--head">
																																																		<div class="tv-show__meta">
																																																			<span class="tv-show__meta--genre">
																																																				<a href="/tv-show-genre/drama/" rel="tag">Drama</a>, <a href="/tv-show-genre/fantasy/" rel="tag">Fantasy</a>
																																																			</span>
																																																			<span class="tv-show__meta--release-year">2014 &#8211; 2015</span>
																																																		</div>
																																																		<a href="/react2/tv_show/dom-grozypeeny-dreadful/" class="masvideos-LoopTvShow-link masvideos-loop-tv-show__link tv-show__link">
																																																			<h3 class="masvideos-loop-tv-show__title  tv-show__title">Dom grozy(Peeny Dreadful)</h3>
																																																		</a>
																																																	</div>
																																																</div>
																																																<div class="tv-show__hover-area--body">
																																																	<div class="tv-show__season-info">
																																																		<div class="tv-show__seasons">Seasons #: <a href="/react2/tv_show/dom-grozypeeny-dreadful/" class="tv-show__episode--link">Season 1</a>
																																																			<a href="/react2/tv_show/dom-grozypeeny-dreadful/" class="tv-show__episode--link">season 2</a>
																																																		</div>
																																																		<div class="tv-show__episode">Newest Episode: <a href="/react2/episode/little-scorpion/" class="tv-show__episode--link">S02E05</a>
																																																		</div>
																																																	</div>
																																																	<div class="tv-show__review-info"> 
																																																		<div class="tv-show__avg-rating">
																																																		</div>
																																																		<div class="viewers-count">
																																																		</div>
																																																	</div>
																																																	<div class="tv-show__actions">
																																																		<a href="/react2/tv_show/dom-grozypeeny-dreadful/" class="tv-show-actions--link_watch">Watch Now</a> 
																																																		<div class="tv-show-actions--link_add-to-playlist dropdown">
																																																			<a class="dropdown-toggle" href="/react2/tv_show/dom-grozypeeny-dreadful/" data-toggle="dropdown">+ Playlist</a>
																																																			<div class="dropdown-menu">
																																																				<a class="login-link" href="/login/tv-show-playlists/">Sign in to add this tv show to a playlist.</a> </div>
																																																			</div>
																																																		</div>
																																																	</div> </div>
																																																</div>
																																															</div>
																																															<div class="tv-show post-2495 tv_show type-tv_show status-publish has-post-thumbnail hentry tv_show_genre-crime tv_show_genre-drama tv_show_tag-brother tv_show_tag-brother-relationship tv_show_tag-kings tv_show_tag-vikings">
																																																<div class="tv-show__poster">
																																																	<a href="/react2/tv_show/cardinal/" class="masvideos-LoopTvShow-link masvideos-loop-tv-show__link tv-show__link">
																																																		<img width="970" height="550" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/36-garden.jpg" class="tv-show__poster--image tv_show__poster--image" alt="" srcset="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/36-garden.jpg 970w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/36-garden-270x153.jpg 270w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/36-garden-220x125.jpg 220w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/36-garden-324x184.jpg 324w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/36-garden-272x155.jpg 272w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/36-garden-214x122.jpg 214w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/36-garden-244x138.jpg 244w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/36-garden-339x192.jpg 339w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/36-garden-327x185.jpg 327w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/36-garden-300x170.jpg 300w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/36-garden-768x435.jpg 768w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/36-garden-120x67.jpg 120w" sizes="(max-width: 970px) 100vw, 970px" />
																																																	</a>
																																																</div>
																																																<div class="tv-show__body">
																																																	<div class="tv-show__info">
																																																		<div class="tv-show__info--head">
																																																			<div class="tv-show__meta">
																																																				<span class="tv-show__meta--genre">
																																																					<a href="/tv-show-genre/crime/" rel="tag">Crime</a>, <a href="/tv-show-genre/drama/" rel="tag">Drama</a>
																																																				</span>
																																																				<span class="tv-show__meta--release-year">2017 &#8211; 2018</span>
																																																			</div>
																																																			<a href="/react2/tv_show/cardinal/" class="masvideos-LoopTvShow-link masvideos-loop-tv-show__link tv-show__link">
																																																				<h3 class="masvideos-loop-tv-show__title  tv-show__title">Cardinal</h3>
																																																			</a>
																																																			<div class="tv-show__episode">Newest Episode: <a href="/react2/episode/northwind/" class="tv-show__episode--link">S02E05</a>
																																																			</div>
																																																		</div> 
																																																		<div class="tv-show__short-description">
																																																			<div>
																																																				<p>Cardinal struggles to right past wrongs that could derail his investigation and end his career, as the case grows more violent and twisted, and the clock ticks down on the killer&#8217;s next victim.</p> 
																																																			</div> 
																																																		</div>
																																																		<div class="tv-show__actions">
																																																			<a href="/react2/tv_show/cardinal/" class="tv-show-actions--link_watch">Watch Now</a> 
																																																			<div class="tv-show-actions--link_add-to-playlist dropdown">
																																																				<a class="dropdown-toggle" href="/react2/tv_show/cardinal/" data-toggle="dropdown">+ Playlist</a>
																																																				<div class="dropdown-menu">
																																																					<a class="login-link" href="/login/tv-show-playlists/">Sign in to add this tv show to a playlist.</a> </div>
																																																				</div>
																																																			</div>
																																																		</div>
																																																		<div class="tv-show__review-info"> <a href="/react2/tv_show/cardinal/#reviews" class="avg-rating">
																																																			<span class="rating-with-count">
																																																				<svg class="vodi-svg" width="40px" height="39px" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 39">
																																																					<title>play</title>
																																																					<path fill-rule="evenodd" d="M19.633,-0.000 C21.509,0.035 21.530,1.174 22.167,2.414 C23.329,4.679 24.406,7.067 25.572,9.338 C25.853,9.886 26.431,11.640 26.918,11.834 C27.486,12.203 29.345,12.109 30.165,12.316 C32.170,12.825 34.489,12.860 36.500,13.364 C37.516,13.618 38.689,13.413 39.430,13.927 C39.689,14.107 39.770,14.504 39.984,14.732 C40.047,16.499 39.096,16.843 38.163,17.792 C36.473,19.509 34.784,21.227 33.095,22.944 C32.585,23.462 31.092,24.543 31.036,25.359 C31.423,25.951 31.307,27.455 31.511,28.258 C32.138,30.727 32.213,33.522 32.857,35.987 C33.142,37.078 33.016,38.241 32.303,38.724 C31.108,39.533 29.632,38.193 28.819,37.758 C26.695,36.623 24.601,35.624 22.483,34.457 C21.979,34.179 20.607,33.178 20.108,33.088 C19.748,33.023 18.163,34.107 17.812,34.296 C15.557,35.505 13.340,36.640 11.080,37.839 C10.548,38.120 9.180,39.226 8.309,38.966 C6.955,38.558 6.874,36.993 7.280,35.423 C7.716,33.733 7.697,31.880 8.151,30.109 C8.527,28.642 8.907,26.529 9.022,24.957 C8.092,24.344 7.202,23.107 6.408,22.300 C4.760,20.625 3.059,18.990 1.340,17.389 C0.646,16.742 -0.578,15.515 0.311,14.249 C0.915,13.388 2.364,13.656 3.557,13.364 C6.678,12.599 10.114,12.468 13.298,11.834 C14.186,9.747 15.306,7.711 16.307,5.716 C16.954,4.426 17.496,3.163 18.128,1.931 C18.334,1.531 18.358,1.093 18.603,0.724 C18.845,0.362 19.299,0.273 19.633,-0.000 Z" />
																																																				</svg> <span class="avg-rating-number"> 8.0</span>
																																																			</span>
																																																			<span class="rating-number-with-text">
																																																				<span class="avg-rating-number"> 8.0</span>
																																																				<span class="avg-rating-text">
																																																					<span>1</span> Vote </span>
																																																				</span>
																																																			</a>
																																																			<div class="viewers-count">
																																																			</div>
																																																		</div>
																																																	</div> 
																																																	<div class="tv-show__hover-area">
																																																		<div class="tv-show__hover-area--inner">
																																																			<div class="tv-show__hover-area--poster">
																																																				<div class="tv-show__poster">
																																																					<a href="/react2/tv_show/cardinal/" class="masvideos-LoopTvShow-link masvideos-loop-tv-show__link tv-show__link">
																																																						<img width="970" height="550" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/36-garden.jpg" class="tv-show__poster--image tv_show__poster--image" alt="" srcset="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/36-garden.jpg 970w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/36-garden-270x153.jpg 270w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/36-garden-220x125.jpg 220w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/36-garden-324x184.jpg 324w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/36-garden-272x155.jpg 272w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/36-garden-214x122.jpg 214w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/36-garden-244x138.jpg 244w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/36-garden-339x192.jpg 339w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/36-garden-327x185.jpg 327w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/36-garden-300x170.jpg 300w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/36-garden-768x435.jpg 768w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/36-garden-120x67.jpg 120w" sizes="(max-width: 970px) 100vw, 970px" />
																																																					</a>
																																																				</div>
																																																				<div class="tv-show__info--head">
																																																					<div class="tv-show__meta">
																																																						<span class="tv-show__meta--genre">
																																																							<a href="/tv-show-genre/crime/" rel="tag">Crime</a>, <a href="/tv-show-genre/drama/" rel="tag">Drama</a>
																																																						</span>
																																																						<span class="tv-show__meta--release-year">2017 &#8211; 2018</span>
																																																					</div>
																																																					<a href="/react2/tv_show/cardinal/" class="masvideos-LoopTvShow-link masvideos-loop-tv-show__link tv-show__link">
																																																						<h3 class="masvideos-loop-tv-show__title  tv-show__title">Cardinal</h3>
																																																					</a>
																																																				</div>
																																																			</div>
																																																			<div class="tv-show__hover-area--body">
																																																				<div class="tv-show__season-info">
																																																					<div class="tv-show__seasons">Seasons #: <a href="/react2/tv_show/cardinal/" class="tv-show__episode--link">Season 1</a>
																																																						<a href="/react2/tv_show/cardinal/" class="tv-show__episode--link">Season 2</a>
																																																					</div>
																																																					<div class="tv-show__episode">Newest Episode: <a href="/react2/episode/northwind/" class="tv-show__episode--link">S02E05</a>
																																																					</div>
																																																				</div>
																																																				<div class="tv-show__review-info"> 
																																																					<div class="tv-show__avg-rating">
																																																						<a href="/react2/tv_show/cardinal/#reviews" class="avg-rating">
																																																							<div class="avg-rating__inner">
																																																								<span class="avg-rating__number"> 8.0</span>
																																																								<span class="avg-rating__text">
																																																									<span>1</span> Vote </span>
																																																								</div>
																																																							</a>
																																																						</div>
																																																						<div class="viewers-count">
																																																						</div>
																																																					</div>
																																																					<div class="tv-show__actions">
																																																						<a href="/react2/tv_show/cardinal/" class="tv-show-actions--link_watch">Watch Now</a> 
																																																						<div class="tv-show-actions--link_add-to-playlist dropdown">
																																																							<a class="dropdown-toggle" href="/react2/tv_show/cardinal/" data-toggle="dropdown">+ Playlist</a>
																																																							<div class="dropdown-menu">
																																																								<a class="login-link" href="/login/tv-show-playlists/">Sign in to add this tv show to a playlist.</a> </div>
																																																							</div>
																																																						</div>
																																																					</div> </div>
																																																				</div>
																																																			</div>
																																																			<div class="tv-show post-2482 tv_show type-tv_show status-publish has-post-thumbnail hentry tv_show_genre-comedy tv_show_genre-crime tv_show_genre-drama tv_show_tag-brother tv_show_tag-brother-relationship tv_show_tag-kings tv_show_tag-original tv_show_tag-vikings">
																																																				<div class="tv-show__poster">
																																																					<a href="/react2/tv_show/orange-is-the-new-black/" class="masvideos-LoopTvShow-link masvideos-loop-tv-show__link tv-show__link">
																																																						<img width="970" height="550" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/35-fine-line.jpg" class="tv-show__poster--image tv_show__poster--image" alt="" srcset="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/35-fine-line.jpg 970w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/35-fine-line-270x153.jpg 270w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/35-fine-line-220x125.jpg 220w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/35-fine-line-324x184.jpg 324w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/35-fine-line-272x155.jpg 272w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/35-fine-line-214x122.jpg 214w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/35-fine-line-244x138.jpg 244w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/35-fine-line-339x192.jpg 339w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/35-fine-line-327x185.jpg 327w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/35-fine-line-300x170.jpg 300w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/35-fine-line-768x435.jpg 768w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/35-fine-line-120x67.jpg 120w" sizes="(max-width: 970px) 100vw, 970px" />
																																																					</a>
																																																				</div>
																																																				<div class="tv-show__body">
																																																					<div class="tv-show__info">
																																																						<div class="tv-show__info--head">
																																																							<div class="tv-show__meta">
																																																								<span class="tv-show__meta--genre">
																																																									<a href="/tv-show-genre/comedy/" rel="tag">Comedy</a>, <a href="/tv-show-genre/crime/" rel="tag">Crime</a>, <a href="/tv-show-genre/drama/" rel="tag">Drama</a>
																																																								</span>
																																																								<span class="tv-show__meta--release-year">2013 &#8211; 2014</span>
																																																							</div>
																																																							<a href="/react2/tv_show/orange-is-the-new-black/" class="masvideos-LoopTvShow-link masvideos-loop-tv-show__link tv-show__link">
																																																								<h3 class="masvideos-loop-tv-show__title  tv-show__title">Orange is the New black</h3>
																																																							</a>
																																																							<div class="tv-show__episode">Newest Episode: <a href="/react2/episode/low-self-esteem-city/" class="tv-show__episode--link">S01E05</a>
																																																							</div>
																																																						</div> 
																																																						<div class="tv-show__short-description">
																																																							<div>
																																																								<p>Convicted of a decade old crime of transporting drug money to an ex-girlfriend, normally law-abiding Piper Chapman is sentenced to a year and a half behind bars to face the reality of how life-changing prison can really be.</p> 
																																																							</div> 
																																																						</div>
																																																						<div class="tv-show__actions">
																																																							<a href="/react2/tv_show/orange-is-the-new-black/" class="tv-show-actions--link_watch">Watch Now</a> 
																																																							<div class="tv-show-actions--link_add-to-playlist dropdown">
																																																								<a class="dropdown-toggle" href="/react2/tv_show/orange-is-the-new-black/" data-toggle="dropdown">+ Playlist</a>
																																																								<div class="dropdown-menu">
																																																									<a class="login-link" href="/login/tv-show-playlists/">Sign in to add this tv show to a playlist.</a> </div>
																																																								</div>
																																																							</div>
																																																						</div>
																																																						<div class="tv-show__review-info"> <a href="/react2/tv_show/orange-is-the-new-black/#reviews" class="avg-rating">
																																																							<span class="rating-with-count">
																																																								<svg class="vodi-svg" width="40px" height="39px" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 39">
																																																									<title>play</title>
																																																									<path fill-rule="evenodd" d="M19.633,-0.000 C21.509,0.035 21.530,1.174 22.167,2.414 C23.329,4.679 24.406,7.067 25.572,9.338 C25.853,9.886 26.431,11.640 26.918,11.834 C27.486,12.203 29.345,12.109 30.165,12.316 C32.170,12.825 34.489,12.860 36.500,13.364 C37.516,13.618 38.689,13.413 39.430,13.927 C39.689,14.107 39.770,14.504 39.984,14.732 C40.047,16.499 39.096,16.843 38.163,17.792 C36.473,19.509 34.784,21.227 33.095,22.944 C32.585,23.462 31.092,24.543 31.036,25.359 C31.423,25.951 31.307,27.455 31.511,28.258 C32.138,30.727 32.213,33.522 32.857,35.987 C33.142,37.078 33.016,38.241 32.303,38.724 C31.108,39.533 29.632,38.193 28.819,37.758 C26.695,36.623 24.601,35.624 22.483,34.457 C21.979,34.179 20.607,33.178 20.108,33.088 C19.748,33.023 18.163,34.107 17.812,34.296 C15.557,35.505 13.340,36.640 11.080,37.839 C10.548,38.120 9.180,39.226 8.309,38.966 C6.955,38.558 6.874,36.993 7.280,35.423 C7.716,33.733 7.697,31.880 8.151,30.109 C8.527,28.642 8.907,26.529 9.022,24.957 C8.092,24.344 7.202,23.107 6.408,22.300 C4.760,20.625 3.059,18.990 1.340,17.389 C0.646,16.742 -0.578,15.515 0.311,14.249 C0.915,13.388 2.364,13.656 3.557,13.364 C6.678,12.599 10.114,12.468 13.298,11.834 C14.186,9.747 15.306,7.711 16.307,5.716 C16.954,4.426 17.496,3.163 18.128,1.931 C18.334,1.531 18.358,1.093 18.603,0.724 C18.845,0.362 19.299,0.273 19.633,-0.000 Z" />
																																																								</svg> <span class="avg-rating-number"> 8.0</span>
																																																							</span>
																																																							<span class="rating-number-with-text">
																																																								<span class="avg-rating-number"> 8.0</span>
																																																								<span class="avg-rating-text">
																																																									<span>1</span> Vote </span>
																																																								</span>
																																																							</a>
																																																							<div class="viewers-count">
																																																							</div>
																																																						</div>
																																																					</div> 
																																																					<div class="tv-show__hover-area">
																																																						<div class="tv-show__hover-area--inner">
																																																							<div class="tv-show__hover-area--poster">
																																																								<div class="tv-show__poster">
																																																									<a href="/react2/tv_show/orange-is-the-new-black/" class="masvideos-LoopTvShow-link masvideos-loop-tv-show__link tv-show__link">
																																																										<img width="970" height="550" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/35-fine-line.jpg" class="tv-show__poster--image tv_show__poster--image" alt="" srcset="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/35-fine-line.jpg 970w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/35-fine-line-270x153.jpg 270w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/35-fine-line-220x125.jpg 220w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/35-fine-line-324x184.jpg 324w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/35-fine-line-272x155.jpg 272w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/35-fine-line-214x122.jpg 214w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/35-fine-line-244x138.jpg 244w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/35-fine-line-339x192.jpg 339w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/35-fine-line-327x185.jpg 327w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/35-fine-line-300x170.jpg 300w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/35-fine-line-768x435.jpg 768w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/35-fine-line-120x67.jpg 120w" sizes="(max-width: 970px) 100vw, 970px" />
																																																									</a>
																																																								</div>
																																																								<div class="tv-show__info--head">
																																																									<div class="tv-show__meta">
																																																										<span class="tv-show__meta--genre">
																																																											<a href="/tv-show-genre/comedy/" rel="tag">Comedy</a>, <a href="/tv-show-genre/crime/" rel="tag">Crime</a>, <a href="/tv-show-genre/drama/" rel="tag">Drama</a>
																																																										</span>
																																																										<span class="tv-show__meta--release-year">2013 &#8211; 2014</span>
																																																									</div>
																																																									<a href="/react2/tv_show/orange-is-the-new-black/" class="masvideos-LoopTvShow-link masvideos-loop-tv-show__link tv-show__link">
																																																										<h3 class="masvideos-loop-tv-show__title  tv-show__title">Orange is the New black</h3>
																																																									</a>
																																																								</div>
																																																							</div>
																																																							<div class="tv-show__hover-area--body">
																																																								<div class="tv-show__season-info">
																																																									<div class="tv-show__seasons">Seasons #: <a href="/react2/tv_show/orange-is-the-new-black/" class="tv-show__episode--link">Season 1</a>
																																																										<a href="/react2/tv_show/orange-is-the-new-black/" class="tv-show__episode--link">Season 2</a>
																																																									</div>
																																																									<div class="tv-show__episode">Newest Episode: <a href="/react2/episode/low-self-esteem-city/" class="tv-show__episode--link">S01E05</a>
																																																									</div>
																																																								</div>
																																																								<div class="tv-show__review-info"> 
																																																									<div class="tv-show__avg-rating">
																																																										<a href="/react2/tv_show/orange-is-the-new-black/#reviews" class="avg-rating">
																																																											<div class="avg-rating__inner">
																																																												<span class="avg-rating__number"> 8.0</span>
																																																												<span class="avg-rating__text">
																																																													<span>1</span> Vote </span>
																																																												</div>
																																																											</a>
																																																										</div>
																																																										<div class="viewers-count">
																																																										</div>
																																																									</div>
																																																									<div class="tv-show__actions">
																																																										<a href="/react2/tv_show/orange-is-the-new-black/" class="tv-show-actions--link_watch">Watch Now</a> 
																																																										<div class="tv-show-actions--link_add-to-playlist dropdown">
																																																											<a class="dropdown-toggle" href="/react2/tv_show/orange-is-the-new-black/" data-toggle="dropdown">+ Playlist</a>
																																																											<div class="dropdown-menu">
																																																												<a class="login-link" href="/login/tv-show-playlists/">Sign in to add this tv show to a playlist.</a> </div>
																																																											</div>
																																																										</div>
																																																									</div> </div>
																																																								</div>
																																																							</div>
																																																						</div>
																																																					</div>
																																																				</div> </div>
																																																			</div>
																																																		</div>
																																																	</div>
																																																</section>
																																															</div>
																																														</div>
																																													</div>
																																												</li>
																																											</ul>
																																										</li>
																																																				{{-- <li id="menu-item-5555" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5555">
<a href="https://demo3.madrasthemes.com/vodi-demos/main/blog/">Blog</a>
</li> --}}
</ul>
</div>
</div>
<div class="site-header__left">

	<div class="site-header__user-account dropdown">
		<a href="/login" class="site-header__user-account--link" data-toggle="dropdown">
			<svg width="32px" height="32px">
				<image x="0px" y="0px" width="32px" height="32px" xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAMAAABEpIrGAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAACB1BMVEW7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu3t7eys7KztLO4uLi6u7q0tbSxsrG2t7awsrC7vLu8vLy1trW8vby1tbWysrK6urq3uLe5urm9vr24ubitrq2wsbCsrazV1dXs7Oz29vb39/fw8PDe3t6vsK/v7+/////5+vnLy8urrKuxsbHGx8aur67k5OT4+Pizs7P9/f3R0tGurq7Oz87j4+OsrqzU1NTo6OiusK7+/v7k5eT4+fjGyMa7u7v6+vrMzMyrq6u+vr7Ky8ri4uLt7e3u7u7m5+bT1NOwsLC+v77Q0NDe397f39/T09O/v7/FxsX5+fn09PS9vb37+/v09fTNzs3g4ODh4uHX2Nf8/PzY2djIyMjIycjU1dTl5uXx8fHm5ubV1tVi3+TsAAAAUnRSTlMADleWxOHwxphZESftmiuDiBvN0iAq8TAW9xzX340dI5ymCPj9VV+lydDl5/P05srRnqdjCvr+EKCqipTd5Pb7IfI31NmLkAQvojIVn+PLoWIYYNJBTgAAAAFiS0dEca8HXOIAAAAHdElNRQfjBQECDwMxTbKCAAACG0lEQVQ4y21T90PTQBQ+RKpY3FisExcuEHEvFPdeXLikJLnakjbVgjESSEutqIiKIo66R90L/SNtbzTp+H66e9/37r17A4ACqqZUT63xeGqmTa+dAcrhra7rhEIXQl2iFPDMnFXKz0bdoqzIBIosdqM5RfTceYKKZRdwUJhf7/AL6iD35lAU6Gvg/ELfJe4eCgd62FGT/IuYYDGkPI5E9RASYiK9anAJyw8yFx1evhLv7buKYwoJqMCleX4ZUskVG+iaSXC936ICFS3PCRoHyItKUOw1OQZtYsMDKwBYuUqgEaxEgTeTQxFiE1dXgTWdlFdxyhGYN9LM2gQaJVoB46aLN4ctWg19LVjH/nDrtltwJ42IFa4HG8JUMFLpBTm8ETSHeA53K+SAmkELE8jWqMPfY7/ICzYFWHfcdbhv40KIVok3Z+RBH6XHHj7ivYWbQZvOL+N6+vHEk6fPnoejCh8dfQuo5SHkjB158fLV6zdvg3aGj4e6FWzbTkqtida74SQN8T4xGBM0UuodOwHYlW+WZosT7jok1HROgfXduW7WI1HRoviDWYT4eEzLtdubH4g98GOmZ8wsQVY2NLiXjtQ+Q/pkliE+ZLTzod/fb1bA5wMdfKwbDn4p57/6DzmL0XH42/diOvXjiLdot47+TLgkqdFfx46XLOeJtpbffyaT2Wxy8u8//8lTFfb7dNOZ1rMeX/u58xcuOtb/keQ/CDzeyUsAAAAASUVORK5CYII=" />
			</svg> </a>
			<ul class="dropdown-menu sub-menu">
				<li>
					<a href="/login/" data-toggle="modal" data-target="#modal-register-login">Sign in</a>
				</li>
				<li>
					<a href="/register/" data-toggle="modal" data-target="#modal-register-login">Register</a>
				</li>
			</ul>
		</div>
	</div> </div>
</div>
</header>
<header class="handheld-header site-header handheld-stick-this light">
	<div class="container-fluid">
		<div class="site-header__inner">
			<div class="site-header__left">
				<div class="site-header__offcanvas">
					<button class="site-header__offcanvas--toggler navbar-toggler" data-toggle="offcanvas">
						<svg xmlns="http://www.w3.org/2000/svg" width="16" height="13">
							<path d="M0 13L0 11.4 16 11.4 16 13 0 13ZM0 5.7L16 5.7 16 7.3 0 7.3 0 5.7ZM0 0L16 0 16 1.6 0 1.6 0 0Z" />
						</svg>
					</button>
					<div class="offcanvas-drawer">
						<div class="offcanvas-collapse" data-simplebar>
							<div class="site_header__offcanvas-nav">
								<ul id="menu-off-canvas-menu-1" class="offcanvas-nav yamm">
									<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-ancestor current-menu-parent menu-item-has-children dropdown active menu-item-5565 nav-item">
										<a title="Home" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle nav-link">Home</a>
										<ul class="dropdown-menu" role="menu">
											<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-343 current_page_item active menu-item-5561 nav-item">
												<a title="Home v1" href="/" class="dropdown-item">Home v1</a>
											</li>
											<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5562 nav-item">
												<a title="Home v2" href="https://demo3.madrasthemes.com/vodi-demos/main/home-v2/" class="dropdown-item">Home v2</a>
											</li>
											<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5563 nav-item">
												<a title="Home v3" href="https://demo3.madrasthemes.com/vodi-demos/main/home-v3/" class="dropdown-item">Home v3</a>
											</li>
											<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5564 nav-item">
												<a title="Home v5" href="https://demo3.madrasthemes.com/vodi-demos/main/home-v5/" class="dropdown-item">Home v5</a>
											</li>
										</ul>
									</li>
									<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children dropdown menu-item-5751 nav-item">
										<a title="Movies" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle nav-link">Movies</a>
										<ul class="dropdown-menu" role="menu">
											<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" class="menu-item menu-item-type-taxonomy menu-item-object-movie_genre menu-item-5579 nav-item">
												<a title="Action" href="/react2/movies/action/" class="dropdown-item">Action</a>
											</li>
											<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" class="menu-item menu-item-type-taxonomy menu-item-object-movie_genre menu-item-5580 nav-item">
												<a title="Adventure" href="/react2/movies/adventure/" class="dropdown-item">Adventure</a>
											</li>
											<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" class="menu-item menu-item-type-taxonomy menu-item-object-movie_genre menu-item-5581 nav-item">
												<a title="Comedy" href="/react2/movies/comedy/" class="dropdown-item">Comedy</a>
											</li>
											<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" class="menu-item menu-item-type-taxonomy menu-item-object-movie_genre menu-item-5582 nav-item">
												<a title="Drama" href="/react2/movies/drama/" class="dropdown-item">Drama</a>
											</li>
											<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" class="menu-item menu-item-type-taxonomy menu-item-object-movie_genre menu-item-5583 nav-item">
												<a title="Family" href="/react2/movies/family/" class="dropdown-item">Family</a>
											</li>
											<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" class="menu-item menu-item-type-taxonomy menu-item-object-movie_genre menu-item-5584 nav-item">
												<a title="Romance" href="/react2/movies/romance/" class="dropdown-item">Romance</a>
											</li>
										</ul>
									</li>
									<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children dropdown menu-item-5559 nav-item">
										<a title="TV Shows" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle nav-link">TV Shows</a>
										<ul class="dropdown-menu" role="menu">
											<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" class="menu-item menu-item-type-taxonomy menu-item-object-tv_show_genre menu-item-5572 nav-item">
												<a title="Drama" href="/tv-show-genre/drama/" class="dropdown-item">Drama</a>
											</li>
											<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" class="menu-item menu-item-type-taxonomy menu-item-object-tv_show_genre menu-item-5573 nav-item">
												<a title="Comedy" href="/tv-show-genre/comedy/" class="dropdown-item">Comedy</a>
											</li>
											<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" class="menu-item menu-item-type-taxonomy menu-item-object-tv_show_genre menu-item-5574 nav-item">
												<a title="Action" href="/tv-show-genre/action/" class="dropdown-item">Action</a>
											</li>
											<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" class="menu-item menu-item-type-taxonomy menu-item-object-tv_show_genre menu-item-5575 nav-item">
												<a title="Crime" href="/tv-show-genre/crime/" class="dropdown-item">Crime</a>
											</li>
											<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" class="menu-item menu-item-type-taxonomy menu-item-object-tv_show_genre menu-item-5576 nav-item">
												<a title="Romance" href="/tv-show-genre/romance/" class="dropdown-item">Romance</a>
											</li>
											<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" class="menu-item menu-item-type-taxonomy menu-item-object-tv_show_genre menu-item-5577 nav-item">
												<a title="Adventure" href="/tv-show-genre/adventure/" class="dropdown-item">Adventure</a>
											</li>
											<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" class="menu-item menu-item-type-taxonomy menu-item-object-tv_show_genre menu-item-5578 nav-item">
												<a title="Sci-Fi" href="/tv-show-genre/sci-fi/" class="dropdown-item">Sci-Fi</a>
											</li>
										</ul>
									</li>
									<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children dropdown menu-item-5558 nav-item">
										<a title="Videos" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle nav-link">Videos</a>
										<ul class="dropdown-menu" role="menu">
											<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" class="menu-item menu-item-type-taxonomy menu-item-object-video_cat menu-item-5566 nav-item">
												<a title="Games" href="https://demo3.madrasthemes.com/vodi-demos/main/video-category/games/" class="dropdown-item">Games</a>
											</li>
											<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" class="menu-item menu-item-type-taxonomy menu-item-object-video_cat menu-item-5567 nav-item">
												<a title="Football" href="https://demo3.madrasthemes.com/vodi-demos/main/video-category/football/" class="dropdown-item">Football</a>
											</li>
											<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" class="menu-item menu-item-type-taxonomy menu-item-object-video_cat menu-item-5568 nav-item">
												<a title="Adventure" href="https://demo3.madrasthemes.com/vodi-demos/main/video-category/adventure/" class="dropdown-item">Adventure</a>
											</li>
											<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" class="menu-item menu-item-type-taxonomy menu-item-object-video_cat menu-item-5569 nav-item">
												<a title="Cricket" href="https://demo3.madrasthemes.com/vodi-demos/main/video-category/cricket/" class="dropdown-item">Cricket</a>
											</li>
											<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" class="menu-item menu-item-type-taxonomy menu-item-object-video_cat menu-item-5570 nav-item">
												<a title="Tennis" href="https://demo3.madrasthemes.com/vodi-demos/main/video-category/tennis/" class="dropdown-item">Tennis</a>
											</li>
											<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" class="menu-item menu-item-type-taxonomy menu-item-object-video_cat menu-item-5571 nav-item">
												<a title="Action" href="https://demo3.madrasthemes.com/vodi-demos/main/video-category/action/" class="dropdown-item">Action</a>
											</li>
										</ul>
									</li>
								</ul>
							</div>
						</div>

					</div>
				</div>
				<div class="site-header__logo">
					<a href="/" rel="home" class="navbar-brand"> 
						<img style="max-width: 45%" src="https://khojapp.com/images/logo2.png">
					</a>
				</div>
			</div>
			<div class="site-header__right">
				<div class="site-header__search">
					<div class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							<svg xmlns="http://www.w3.org/2000/svg" width="18" height="18">
								<path d="M7 0C11-0.1 13.4 2.1 14.6 4.9 15.5 7.1 14.9 9.8 13.9 11.4 13.7 11.7 13.6 12 13.3 12.2 13.4 12.5 14.2 13.1 14.4 13.4 15.4 14.3 16.3 15.2 17.2 16.1 17.5 16.4 18.2 16.9 18 17.5 17.9 17.6 17.9 17.7 17.8 17.8 17.2 18.3 16.7 17.8 16.4 17.4 15.4 16.4 14.3 15.4 13.3 14.3 13 14.1 12.8 13.8 12.5 13.6 12.4 13.5 12.3 13.3 12.2 13.3 12 13.4 11.5 13.8 11.3 14 10.7 14.4 9.9 14.6 9.2 14.8 8.9 14.9 8.6 14.9 8.3 14.9 8 15 7.4 15.1 7.1 15 6.3 14.8 5.6 14.8 4.9 14.5 2.7 13.6 1.1 12.1 0.4 9.7 0 8.7-0.2 7.1 0.2 6 0.3 5.3 0.5 4.6 0.9 4 1.8 2.4 3 1.3 4.7 0.5 5.2 0.3 5.7 0.2 6.3 0.1 6.5 0 6.8 0.1 7 0ZM7.3 1.5C7.1 1.6 6.8 1.5 6.7 1.5 6.2 1.6 5.8 1.7 5.4 1.9 3.7 2.5 2.6 3.7 1.9 5.4 1.7 5.8 1.7 6.2 1.6 6.6 1.4 7.4 1.6 8.5 1.8 9.1 2.4 11.1 3.5 12.3 5.3 13 5.9 13.3 6.6 13.5 7.5 13.5 7.7 13.5 7.9 13.5 8.1 13.5 8.6 13.4 9.1 13.3 9.6 13.1 11.2 12.5 12.4 11.4 13.1 9.8 13.6 8.5 13.6 6.6 13.1 5.3 12.2 3.1 10.4 1.5 7.3 1.5Z" />
							</svg>
						</a>

					</div>
				</div> 
				<div class="site-header__user-account dropdown">
					<a href="/login/" class="site-header__user-account--link" data-toggle="dropdown">
						<svg width="32px" height="32px">
							<image x="0px" y="0px" width="32px" height="32px" xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAMAAABEpIrGAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAACB1BMVEW7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu7vLu3t7eys7KztLO4uLi6u7q0tbSxsrG2t7awsrC7vLu8vLy1trW8vby1tbWysrK6urq3uLe5urm9vr24ubitrq2wsbCsrazV1dXs7Oz29vb39/fw8PDe3t6vsK/v7+/////5+vnLy8urrKuxsbHGx8aur67k5OT4+Pizs7P9/f3R0tGurq7Oz87j4+OsrqzU1NTo6OiusK7+/v7k5eT4+fjGyMa7u7v6+vrMzMyrq6u+vr7Ky8ri4uLt7e3u7u7m5+bT1NOwsLC+v77Q0NDe397f39/T09O/v7/FxsX5+fn09PS9vb37+/v09fTNzs3g4ODh4uHX2Nf8/PzY2djIyMjIycjU1dTl5uXx8fHm5ubV1tVi3+TsAAAAUnRSTlMADleWxOHwxphZESftmiuDiBvN0iAq8TAW9xzX340dI5ymCPj9VV+lydDl5/P05srRnqdjCvr+EKCqipTd5Pb7IfI31NmLkAQvojIVn+PLoWIYYNJBTgAAAAFiS0dEca8HXOIAAAAHdElNRQfjBQECDwMxTbKCAAACG0lEQVQ4y21T90PTQBQ+RKpY3FisExcuEHEvFPdeXLikJLnakjbVgjESSEutqIiKIo66R90L/SNtbzTp+H66e9/37r17A4ACqqZUT63xeGqmTa+dAcrhra7rhEIXQl2iFPDMnFXKz0bdoqzIBIosdqM5RfTceYKKZRdwUJhf7/AL6iD35lAU6Gvg/ELfJe4eCgd62FGT/IuYYDGkPI5E9RASYiK9anAJyw8yFx1evhLv7buKYwoJqMCleX4ZUskVG+iaSXC936ICFS3PCRoHyItKUOw1OQZtYsMDKwBYuUqgEaxEgTeTQxFiE1dXgTWdlFdxyhGYN9LM2gQaJVoB46aLN4ctWg19LVjH/nDrtltwJ42IFa4HG8JUMFLpBTm8ETSHeA53K+SAmkELE8jWqMPfY7/ICzYFWHfcdbhv40KIVok3Z+RBH6XHHj7ivYWbQZvOL+N6+vHEk6fPnoejCh8dfQuo5SHkjB158fLV6zdvg3aGj4e6FWzbTkqtida74SQN8T4xGBM0UuodOwHYlW+WZosT7jok1HROgfXduW7WI1HRoviDWYT4eEzLtdubH4g98GOmZ8wsQVY2NLiXjtQ+Q/pkliE+ZLTzod/fb1bA5wMdfKwbDn4p57/6DzmL0XH42/diOvXjiLdot47+TLgkqdFfx46XLOeJtpbffyaT2Wxy8u8//8lTFfb7dNOZ1rMeX/u58xcuOtb/keQ/CDzeyUsAAAAASUVORK5CYII=" />
						</svg> </a>
						<ul class="dropdown-menu sub-menu">
							<li>
								<a href="/login/" data-toggle="modal" data-target="#modal-register-login">Sign in</a>
							</li>
							<li>
								<a href="/login/" data-toggle="modal" data-target="#modal-register-login">Register</a>
							</li>
						</ul>
					</div>
				</div> 
			</div>
		</div>
	</header>
	<div id="content" class="site-content" tabindex="-1">
		<div class="container">
			<nav class="masvideos-breadcrumb">
				<a href="https://demo3.madrasthemes.com/vodi-demos/main">Home</a>
				<span class="delimiter">
					<svg width="4px" height="7px">
						<path fill-rule="evenodd" d="M3.978,3.702 C3.986,3.785 3.966,3.868 3.903,3.934 L1.038,6.901 C0.920,7.022 0.724,7.029 0.598,6.916 L0.143,6.506 C0.017,6.393 0.010,6.203 0.127,6.082 L2.190,3.945 C2.276,3.829 2.355,3.690 2.355,3.548 C2.355,3.214 1.947,2.884 1.947,2.884 L1.963,2.877 L0.080,0.905 C-0.037,0.783 -0.029,0.593 0.095,0.479 L0.547,0.068 C0.671,-0.045 0.866,-0.039 0.983,0.083 L3.823,3.056 C3.866,3.102 3.875,3.161 3.885,3.218 C3.945,3.267 3.988,3.333 3.988,3.415 L3.988,3.681 C3.988,3.689 3.979,3.694 3.978,3.702 Z">
						</path>
					</svg>
				</span>Movies</nav>
				<div class="site-content__inner">
					<div id="primary" class="content-area"> 
						<div class="vodi-control-bar">
							<div class="vodi-control-bar__left">
								<div class="widget masvideos widget_layered_nav masvideos-movies-tags-filter-widget">
									<ul class="masvideos-widget-movies-layered-nav-list">
										<li class="masvideos-widget-movies-layered-nav-list__item masvideos-layered-nav-term ">
											<i class="far fa-window-maximize">
											</i>
											<a rel="nofollow" href="https://demo3.madrasthemes.com?filter_tag=4k-ultra">4K Ultra</a>
										</li>
										<li class="masvideos-widget-movies-layered-nav-list__item masvideos-layered-nav-term ">
											<i class="fas fa-chess-knight">
											</i>
											<a rel="nofollow" href="https://demo3.madrasthemes.com?filter_tag=brother">Brother</a>
										</li>
									</ul>
								</div>
							</div>
							<div class="vodi-control-bar__right"> <ul class="archive-view-switcher nav nav-tabs">
								<li class="nav-item">
									<a id="vodi-archive-view-switcher-grid" class="nav-link active" data-archive-columns="6" data-toggle="tab" data-archive-class="grid" title="Grid View" href="#vodi-archive-view-content">
										<svg xmlns="http://www.w3.org/2000/svg" width="18px" height="15px">
											<path fill-rule="evenodd" fill="rgb(176, 183, 188)" d="M16.500,10.999 C15.671,10.999 15.000,10.327 15.000,9.500 C15.000,8.671 15.671,7.999 16.500,7.999 C17.328,7.999 18.000,8.671 18.000,9.500 C18.000,10.327 17.328,10.999 16.500,10.999 ZM16.500,6.999 C15.671,6.999 15.000,6.328 15.000,5.499 C15.000,4.671 15.671,3.999 16.500,3.999 C17.328,3.999 18.000,4.671 18.000,5.499 C18.000,6.328 17.328,6.999 16.500,6.999 ZM16.500,3.000 C15.671,3.000 15.000,2.328 15.000,1.499 C15.000,0.671 15.671,-0.001 16.500,-0.001 C17.328,-0.001 18.000,0.671 18.000,1.499 C18.000,2.328 17.328,3.000 16.500,3.000 ZM11.500,14.999 C10.672,14.999 10.000,14.328 10.000,13.499 C10.000,12.671 10.672,11.999 11.500,11.999 C12.328,11.999 13.000,12.671 13.000,13.499 C13.000,14.328 12.328,14.999 11.500,14.999 ZM11.500,10.999 C10.672,10.999 10.000,10.327 10.000,9.500 C10.000,8.671 10.672,7.999 11.500,7.999 C12.328,7.999 13.000,8.671 13.000,9.500 C13.000,10.327 12.328,10.999 11.500,10.999 ZM11.500,6.999 C10.672,6.999 10.000,6.328 10.000,5.499 C10.000,4.671 10.672,3.999 11.500,3.999 C12.328,3.999 13.000,4.671 13.000,5.499 C13.000,6.328 12.328,6.999 11.500,6.999 ZM11.500,3.000 C10.672,3.000 10.000,2.328 10.000,1.499 C10.000,0.671 10.672,-0.001 11.500,-0.001 C12.328,-0.001 13.000,0.671 13.000,1.499 C13.000,2.328 12.328,3.000 11.500,3.000 ZM6.500,14.999 C5.671,14.999 5.000,14.328 5.000,13.499 C5.000,12.671 5.671,11.999 6.500,11.999 C7.328,11.999 8.000,12.671 8.000,13.499 C8.000,14.328 7.328,14.999 6.500,14.999 ZM6.500,10.999 C5.671,10.999 5.000,10.327 5.000,9.500 C5.000,8.671 5.671,7.999 6.500,7.999 C7.328,7.999 8.000,8.671 8.000,9.500 C8.000,10.327 7.328,10.999 6.500,10.999 ZM6.500,6.999 C5.671,6.999 5.000,6.328 5.000,5.499 C5.000,4.671 5.671,3.999 6.500,3.999 C7.328,3.999 8.000,4.671 8.000,5.499 C8.000,6.328 7.328,6.999 6.500,6.999 ZM6.500,3.000 C5.671,3.000 5.000,2.328 5.000,1.499 C5.000,0.671 5.671,-0.001 6.500,-0.001 C7.328,-0.001 8.000,0.671 8.000,1.499 C8.000,2.328 7.328,3.000 6.500,3.000 ZM1.500,14.999 C0.671,14.999 -0.000,14.328 -0.000,13.499 C-0.000,12.671 0.671,11.999 1.500,11.999 C2.328,11.999 3.000,12.671 3.000,13.499 C3.000,14.328 2.328,14.999 1.500,14.999 ZM1.500,10.999 C0.671,10.999 -0.000,10.327 -0.000,9.500 C-0.000,8.671 0.671,7.999 1.500,7.999 C2.328,7.999 3.000,8.671 3.000,9.500 C3.000,10.327 2.328,10.999 1.500,10.999 ZM1.500,6.999 C0.671,6.999 -0.000,6.328 -0.000,5.499 C-0.000,4.671 0.671,3.999 1.500,3.999 C2.328,3.999 3.000,4.671 3.000,5.499 C3.000,6.328 2.328,6.999 1.500,6.999 ZM1.500,3.000 C0.671,3.000 -0.000,2.328 -0.000,1.499 C-0.000,0.671 0.671,-0.001 1.500,-0.001 C2.328,-0.001 3.000,0.671 3.000,1.499 C3.000,2.328 2.328,3.000 1.500,3.000 ZM16.500,11.999 C17.328,11.999 18.000,12.671 18.000,13.499 C18.000,14.328 17.328,14.999 16.500,14.999 C15.671,14.999 15.000,14.328 15.000,13.499 C15.000,12.671 15.671,11.999 16.500,11.999 Z">
											</path>
										</svg>
									</a>
								</li>
								<li class="nav-item">
									<a id="vodi-archive-view-switcher-grid-extended" class="nav-link " data-archive-columns="6" data-toggle="tab" data-archive-class="grid-extended" title="Grid View Spacious" href="#vodi-archive-view-content">
										<svg xmlns="http://www.w3.org/2000/svg" width="17px" height="15px">
											<path fill-rule="evenodd" fill="rgb(180, 187, 192)" d="M15.500,8.999 C14.671,8.999 14.000,8.328 14.000,7.499 C14.000,6.671 14.671,5.999 15.500,5.999 C16.328,5.999 17.000,6.671 17.000,7.499 C17.000,8.328 16.328,8.999 15.500,8.999 ZM15.500,2.999 C14.671,2.999 14.000,2.328 14.000,1.499 C14.000,0.671 14.671,-0.000 15.500,-0.000 C16.328,-0.000 17.000,0.671 17.000,1.499 C17.000,2.328 16.328,2.999 15.500,2.999 ZM8.500,14.999 C7.671,14.999 7.000,14.328 7.000,13.499 C7.000,12.671 7.671,11.999 8.500,11.999 C9.328,11.999 10.000,12.671 10.000,13.499 C10.000,14.328 9.328,14.999 8.500,14.999 ZM8.500,8.999 C7.671,8.999 7.000,8.328 7.000,7.499 C7.000,6.671 7.671,5.999 8.500,5.999 C9.328,5.999 10.000,6.671 10.000,7.499 C10.000,8.328 9.328,8.999 8.500,8.999 ZM8.500,2.999 C7.671,2.999 7.000,2.328 7.000,1.499 C7.000,0.671 7.671,-0.000 8.500,-0.000 C9.328,-0.000 10.000,0.671 10.000,1.499 C10.000,2.328 9.328,2.999 8.500,2.999 ZM1.500,14.999 C0.671,14.999 -0.000,14.328 -0.000,13.499 C-0.000,12.671 0.671,11.999 1.500,11.999 C2.328,11.999 3.000,12.671 3.000,13.499 C3.000,14.328 2.328,14.999 1.500,14.999 ZM1.500,8.999 C0.671,8.999 -0.000,8.328 -0.000,7.499 C-0.000,6.671 0.671,5.999 1.500,5.999 C2.328,5.999 3.000,6.671 3.000,7.499 C3.000,8.328 2.328,8.999 1.500,8.999 ZM1.500,2.999 C0.671,2.999 -0.000,2.328 -0.000,1.499 C-0.000,0.671 0.671,-0.000 1.500,-0.000 C2.328,-0.000 3.000,0.671 3.000,1.499 C3.000,2.328 2.328,2.999 1.500,2.999 ZM15.500,11.999 C16.328,11.999 17.000,12.671 17.000,13.499 C17.000,14.328 16.328,14.999 15.500,14.999 C14.671,14.999 14.000,14.328 14.000,13.499 C14.000,12.671 14.671,11.999 15.500,11.999 Z">
											</path>
										</svg>
									</a>
								</li>
								<li class="nav-item">
									<a id="vodi-archive-view-switcher-list-large" class="nav-link " data-archive-columns="6" data-toggle="tab" data-archive-class="list-large" title="List Large View" href="#vodi-archive-view-content">
										<svg xmlns="http://www.w3.org/2000/svg" width="18px" height="15px">
											<path fill-rule="evenodd" fill="rgb(112, 112, 112)" d="M5.000,13.999 L5.000,12.999 L18.000,12.999 L18.000,13.999 L5.000,13.999 ZM5.000,6.999 L18.000,6.999 L18.000,7.999 L5.000,7.999 L5.000,6.999 ZM5.000,0.999 L18.000,0.999 L18.000,1.999 L5.000,1.999 L5.000,0.999 ZM1.500,14.999 C0.671,14.999 -0.000,14.327 -0.000,13.499 C-0.000,12.671 0.671,11.999 1.500,11.999 C2.328,11.999 3.000,12.671 3.000,13.499 C3.000,14.327 2.328,14.999 1.500,14.999 ZM1.500,8.999 C0.671,8.999 -0.000,8.328 -0.000,7.499 C-0.000,6.671 0.671,5.999 1.500,5.999 C2.328,5.999 3.000,6.671 3.000,7.499 C3.000,8.328 2.328,8.999 1.500,8.999 ZM1.500,2.999 C0.671,2.999 -0.000,2.328 -0.000,1.499 C-0.000,0.671 0.671,-0.001 1.500,-0.001 C2.328,-0.001 3.000,0.671 3.000,1.499 C3.000,2.328 2.328,2.999 1.500,2.999 Z">
											</path>
										</svg>
									</a>
								</li>
								<li class="nav-item">
									<a id="vodi-archive-view-switcher-list-small" class="nav-link " data-archive-columns="6" data-toggle="tab" data-archive-class="list-small" title="List View" href="#vodi-archive-view-content">
										<svg xmlns="http://www.w3.org/2000/svg" width="18px" height="15px">
											<path fill-rule="evenodd" fill="rgb(112, 112, 112)" d="M5.000,13.999 L5.000,12.999 L18.000,12.999 L18.000,13.999 L5.000,13.999 ZM5.000,8.999 L18.000,8.999 L18.000,10.000 L5.000,10.000 L5.000,8.999 ZM5.000,4.999 L18.000,4.999 L18.000,5.999 L5.000,5.999 L5.000,4.999 ZM5.000,0.999 L18.000,0.999 L18.000,1.999 L5.000,1.999 L5.000,0.999 ZM1.500,14.999 C0.671,14.999 -0.000,14.327 -0.000,13.499 C-0.000,12.671 0.671,11.999 1.500,11.999 C2.328,11.999 3.000,12.671 3.000,13.499 C3.000,14.327 2.328,14.999 1.500,14.999 ZM1.500,10.999 C0.671,10.999 -0.000,10.328 -0.000,9.499 C-0.000,8.671 0.671,7.999 1.500,7.999 C2.328,7.999 3.000,8.671 3.000,9.499 C3.000,10.328 2.328,10.999 1.500,10.999 ZM1.500,6.999 C0.671,6.999 -0.000,6.328 -0.000,5.499 C-0.000,4.671 0.671,3.999 1.500,3.999 C2.328,3.999 3.000,4.671 3.000,5.499 C3.000,6.328 2.328,6.999 1.500,6.999 ZM1.500,2.999 C0.671,2.999 -0.000,2.328 -0.000,1.499 C-0.000,0.671 0.671,-0.001 1.500,-0.001 C2.328,-0.001 3.000,0.671 3.000,1.499 C3.000,2.328 2.328,2.999 1.500,2.999 Z">
											</path>
										</svg>
									</a>
								</li>
								<li class="nav-item">
									<a id="vodi-archive-view-switcher-list" class="nav-link " data-archive-columns="6" data-toggle="tab" data-archive-class="list" title="List Small View" href="#vodi-archive-view-content">
										<svg xmlns="http://www.w3.org/2000/svg" width="17px" height="13px">
											<path fill-rule="evenodd" fill="rgb(180, 187, 192)" d="M-0.000,13.000 L-0.000,11.999 L17.000,11.999 L17.000,13.000 L-0.000,13.000 ZM-0.000,7.999 L17.000,7.999 L17.000,8.999 L-0.000,8.999 L-0.000,7.999 ZM-0.000,3.999 L17.000,3.999 L17.000,4.999 L-0.000,4.999 L-0.000,3.999 ZM-0.000,-0.001 L17.000,-0.001 L17.000,0.999 L-0.000,0.999 L-0.000,-0.001 Z">
											</path>
										</svg>
									</a>
								</li>
							</ul>
							<div class="movies-ordering">
								<div class="handheld-sidebar-toggle">
									<button class="btn sidebar-toggler" type="button">
										<i class="fas fa-sliders-h">
										</i>
										<span>Filters</span>
									</button>
								</div>
								<svg class="svg-icon svg-icon__sort" aria-hidden="true" role="img" focusable="false" width="17px" height="14px">
									<path fill-rule="evenodd" d="M4.034,-0.001 C4.248,0.009 4.401,0.071 4.578,0.113 C4.699,0.294 4.899,0.408 4.967,0.644 C4.967,1.606 4.967,2.568 4.967,3.529 C4.967,5.972 4.967,8.414 4.967,10.856 C4.980,10.856 4.993,10.856 5.006,10.856 C5.641,10.224 6.276,9.591 6.911,8.958 C7.041,8.873 7.329,8.745 7.572,8.806 C7.930,8.896 8.016,9.121 8.233,9.337 C8.293,10.165 7.817,10.389 7.377,10.818 C6.639,11.539 5.900,12.260 5.161,12.982 C4.928,13.209 4.395,13.909 4.073,13.969 C3.952,13.787 3.760,13.663 3.606,13.513 C3.270,13.184 2.933,12.855 2.596,12.526 C2.052,11.982 1.507,11.438 0.963,10.894 C0.717,10.666 0.471,10.438 0.224,10.211 C0.148,10.110 0.119,9.993 0.030,9.907 C0.015,9.698 -0.048,9.491 0.069,9.337 C0.171,8.957 0.746,8.634 1.235,8.882 C1.922,9.540 2.609,10.198 3.296,10.856 C3.296,7.465 3.296,4.073 3.296,0.682 C3.358,0.600 3.351,0.467 3.412,0.379 C3.511,0.235 3.714,0.158 3.840,0.037 C3.938,0.035 3.984,0.034 4.034,-0.001 ZM12.781,0.037 C12.820,0.037 12.859,0.037 12.898,0.037 C13.999,1.125 15.101,2.214 16.202,3.302 C16.427,3.522 17.287,4.153 16.902,4.668 C16.828,4.945 16.613,4.994 16.435,5.162 C16.280,5.174 16.124,5.187 15.969,5.200 C15.631,5.108 15.447,4.842 15.230,4.630 C14.712,4.137 14.193,3.643 13.675,3.150 C13.675,6.553 13.675,9.958 13.675,13.362 C13.514,13.560 13.485,13.804 13.209,13.893 C13.076,14.007 12.700,14.044 12.548,13.931 C11.760,13.719 12.004,12.233 12.004,11.273 C12.004,8.566 12.004,5.858 12.004,3.150 C11.991,3.150 11.978,3.150 11.965,3.150 C11.676,3.589 10.996,4.095 10.604,4.479 C10.404,4.673 10.198,4.996 9.943,5.124 C9.784,5.204 9.589,5.200 9.360,5.200 C9.238,5.102 9.043,5.080 8.932,4.972 C8.848,4.890 8.822,4.751 8.738,4.668 C8.699,3.730 9.312,3.462 9.827,2.960 C10.811,1.986 11.796,1.011 12.781,0.037 Z">
									</path>
								</svg> <form method="get">
									<select name="orderby" class="orderby" onchange="this.form.submit();">
										<option value="title-asc">From A to Z</option>
										<option value="title-desc">From Z to A</option>
										<option value="release_date" selected="selected">Latest</option>
										<option value="menu_order">Menu Order</option>
										<option value="rating">Rating</option>
										<option value="views">Views</option>
										<option value="likes">Likes</option>
									</select>
									<input type="hidden" name="paged" value="1">
								</form>
							</div>
						</div>
					</div>
					<div class="vodi-archive-wrapper" data-view="grid">
						<div class="movies columns-6">
							<div class="movies__inner">
								<div class="post-718 movie type-movie status-publish has-post-thumbnail hentry movie_genre-action movie_genre-mystery movie_tag-brother movie_tag-hero movie_tag-premieres movie_tag-viking">
									<div class="movie__poster">
										<a href="https://demo3.madrasthemes.com/vodi-demos/main/movie/black-mirror/" class="masvideos-LoopMovie-link masvideos-loop-movie__link movie__link">
											<img width="300" height="450" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/13-300x450.jpg" class="movie__poster--image" alt="" srcset="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/13-300x450.jpg 300w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/13-66x98.jpg 66w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/13-200x300.jpg 200w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/13-600x900.jpg 600w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/13-150x225.jpg 150w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/13.jpg 668w" sizes="(max-width: 300px) 100vw, 300px">
										</a>
									</div>
									<div class="movie__body">
										<div class="movie__info">
											<div class="movie__info--head">
												<div class="movie__meta">
													<span class="movie__meta--release-year">2018</span>
													<span class="movie__meta--genre">
														<a href="https://demo3.madrasthemes.com/vodi-demos/main/movie-genre/action/" rel="tag">Action</a>, <a href="https://demo3.madrasthemes.com/vodi-demos/main/movie-genre/mystery/" rel="tag">Mystery</a>
													</span>
												</div>
												<a href="https://demo3.madrasthemes.com/vodi-demos/main/movie/black-mirror/" class="masvideos-LoopMovie-link masvideos-loop-movie__link movie__link">
													<h3 class="masvideos-loop-movie__title  movie__title">Black Mirror</h3>
												</a>
											</div> <div class="movie__short-description">
												<div>
													<p>In 1984, a young programmer begins to question reality as he works to adapt a fantasy novel into a video game.</p> </div> </div>
													<div class="movie__actions">
														<a href="https://demo3.madrasthemes.com/vodi-demos/main/movie/black-mirror/" class="movie-actions--link_watch">Watch Now</a> <div class="movie-actions--link_add-to-playlist dropdown">
															<a class="dropdown-toggle" href="https://demo3.madrasthemes.com/vodi-demos/main/movie/black-mirror/" data-toggle="dropdown">+ Playlist</a>
															<div class="dropdown-menu">
																<a class="login-link" href="https://demo3.madrasthemes.com/vodi-demos/main/my-account/movie-playlists/">Sign in to add this movie to a playlist.</a> </div>
															</div>
														</div>
													</div>
													<div class="movie__review-info"> <a href="https://demo3.madrasthemes.com/vodi-demos/main/movie/black-mirror/#reviews" class="avg-rating">
														<span class="rating-with-count">
															<svg class="vodi-svg" width="40px" height="39px" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 39">
																<title>play</title>
																<path fill-rule="evenodd" d="M19.633,-0.000 C21.509,0.035 21.530,1.174 22.167,2.414 C23.329,4.679 24.406,7.067 25.572,9.338 C25.853,9.886 26.431,11.640 26.918,11.834 C27.486,12.203 29.345,12.109 30.165,12.316 C32.170,12.825 34.489,12.860 36.500,13.364 C37.516,13.618 38.689,13.413 39.430,13.927 C39.689,14.107 39.770,14.504 39.984,14.732 C40.047,16.499 39.096,16.843 38.163,17.792 C36.473,19.509 34.784,21.227 33.095,22.944 C32.585,23.462 31.092,24.543 31.036,25.359 C31.423,25.951 31.307,27.455 31.511,28.258 C32.138,30.727 32.213,33.522 32.857,35.987 C33.142,37.078 33.016,38.241 32.303,38.724 C31.108,39.533 29.632,38.193 28.819,37.758 C26.695,36.623 24.601,35.624 22.483,34.457 C21.979,34.179 20.607,33.178 20.108,33.088 C19.748,33.023 18.163,34.107 17.812,34.296 C15.557,35.505 13.340,36.640 11.080,37.839 C10.548,38.120 9.180,39.226 8.309,38.966 C6.955,38.558 6.874,36.993 7.280,35.423 C7.716,33.733 7.697,31.880 8.151,30.109 C8.527,28.642 8.907,26.529 9.022,24.957 C8.092,24.344 7.202,23.107 6.408,22.300 C4.760,20.625 3.059,18.990 1.340,17.389 C0.646,16.742 -0.578,15.515 0.311,14.249 C0.915,13.388 2.364,13.656 3.557,13.364 C6.678,12.599 10.114,12.468 13.298,11.834 C14.186,9.747 15.306,7.711 16.307,5.716 C16.954,4.426 17.496,3.163 18.128,1.931 C18.334,1.531 18.358,1.093 18.603,0.724 C18.845,0.362 19.299,0.273 19.633,-0.000 Z">
																</path>
															</svg> <span class="avg-rating-number"> 8.0</span>
														</span>
														<span class="rating-number-with-text">
															<span class="avg-rating-number"> 8.0</span>
															<span class="avg-rating-text">
																<span>3</span> Votes </span>
															</span>
														</a>
														<div class="viewers-count">
														</div>
													</div>
												</div>
											</div>
											<div class="post-320 movie type-movie status-publish has-post-thumbnail hentry movie_genre-action movie_genre-adventure movie_tag-4k-ultra movie_tag-king movie_tag-premieres movie_tag-viking">
												<div class="movie__poster">
													<a href="https://demo3.madrasthemes.com/vodi-demos/main/movie/renegades/" class="masvideos-LoopMovie-link masvideos-loop-movie__link movie__link">
														<img width="300" height="450" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/35-brothers-bloom-300x450.jpg" class="movie__poster--image" alt="" srcset="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/35-brothers-bloom-300x450.jpg 300w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/35-brothers-bloom-66x98.jpg 66w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/35-brothers-bloom-600x900.jpg 600w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/35-brothers-bloom-150x225.jpg 150w" sizes="(max-width: 300px) 100vw, 300px">
													</a>
												</div>
												<div class="movie__body">
													<div class="movie__info">
														<div class="movie__info--head">
															<div class="movie__meta">
																<span class="movie__meta--release-year">2018</span>
																<span class="movie__meta--genre">
																	<a href="https://demo3.madrasthemes.com/vodi-demos/main/movie-genre/action/" rel="tag">Action</a>, <a href="https://demo3.madrasthemes.com/vodi-demos/main/movie-genre/adventure/" rel="tag">Adventure</a>
																</span>
															</div>
															<a href="https://demo3.madrasthemes.com/vodi-demos/main/movie/renegades/" class="masvideos-LoopMovie-link masvideos-loop-movie__link movie__link">
																<h3 class="masvideos-loop-movie__title  movie__title">Renegades</h3>
															</a>
														</div> <div class="movie__short-description">
															<div>
																<p>A team of Navy SEALs discover an underwater treasure in a Bosnian lake.</p> </div> </div>
																<div class="movie__actions">
																	<a href="https://demo3.madrasthemes.com/vodi-demos/main/movie/renegades/" class="movie-actions--link_watch">Watch Now</a> <div class="movie-actions--link_add-to-playlist dropdown">
																		<a class="dropdown-toggle" href="https://demo3.madrasthemes.com/vodi-demos/main/movie/renegades/" data-toggle="dropdown">+ Playlist</a>
																		<div class="dropdown-menu">
																			<a class="login-link" href="https://demo3.madrasthemes.com/vodi-demos/main/my-account/movie-playlists/">Sign in to add this movie to a playlist.</a> </div>
																		</div>
																	</div>
																</div>
																<div class="movie__review-info"> <a href="https://demo3.madrasthemes.com/vodi-demos/main/movie/renegades/#reviews" class="avg-rating">
																	<span class="rating-with-count">
																		<svg class="vodi-svg" width="40px" height="39px" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 39">
																			<title>play</title>
																			<path fill-rule="evenodd" d="M19.633,-0.000 C21.509,0.035 21.530,1.174 22.167,2.414 C23.329,4.679 24.406,7.067 25.572,9.338 C25.853,9.886 26.431,11.640 26.918,11.834 C27.486,12.203 29.345,12.109 30.165,12.316 C32.170,12.825 34.489,12.860 36.500,13.364 C37.516,13.618 38.689,13.413 39.430,13.927 C39.689,14.107 39.770,14.504 39.984,14.732 C40.047,16.499 39.096,16.843 38.163,17.792 C36.473,19.509 34.784,21.227 33.095,22.944 C32.585,23.462 31.092,24.543 31.036,25.359 C31.423,25.951 31.307,27.455 31.511,28.258 C32.138,30.727 32.213,33.522 32.857,35.987 C33.142,37.078 33.016,38.241 32.303,38.724 C31.108,39.533 29.632,38.193 28.819,37.758 C26.695,36.623 24.601,35.624 22.483,34.457 C21.979,34.179 20.607,33.178 20.108,33.088 C19.748,33.023 18.163,34.107 17.812,34.296 C15.557,35.505 13.340,36.640 11.080,37.839 C10.548,38.120 9.180,39.226 8.309,38.966 C6.955,38.558 6.874,36.993 7.280,35.423 C7.716,33.733 7.697,31.880 8.151,30.109 C8.527,28.642 8.907,26.529 9.022,24.957 C8.092,24.344 7.202,23.107 6.408,22.300 C4.760,20.625 3.059,18.990 1.340,17.389 C0.646,16.742 -0.578,15.515 0.311,14.249 C0.915,13.388 2.364,13.656 3.557,13.364 C6.678,12.599 10.114,12.468 13.298,11.834 C14.186,9.747 15.306,7.711 16.307,5.716 C16.954,4.426 17.496,3.163 18.128,1.931 C18.334,1.531 18.358,1.093 18.603,0.724 C18.845,0.362 19.299,0.273 19.633,-0.000 Z">
																			</path>
																		</svg> <span class="avg-rating-number"> 8.0</span>
																	</span>
																	<span class="rating-number-with-text">
																		<span class="avg-rating-number"> 8.0</span>
																		<span class="avg-rating-text">
																			<span>1</span> Vote </span>
																		</span>
																	</a>
																	<div class="viewers-count">
																	</div>
																</div>
															</div>
														</div>
														<div class="post-314 movie type-movie status-publish has-post-thumbnail hentry movie_genre-action movie_genre-documentary movie_tag-4k-ultra movie_tag-king movie_tag-premieres movie_tag-viking">
															<div class="movie__poster">
																<a href="https://demo3.madrasthemes.com/vodi-demos/main/movie/my-generation/" class="masvideos-LoopMovie-link masvideos-loop-movie__link movie__link">
																	<img width="300" height="450" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/32-a-long-way-down-300x450.jpg" class="movie__poster--image" alt="" srcset="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/32-a-long-way-down-300x450.jpg 300w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/32-a-long-way-down-66x98.jpg 66w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/32-a-long-way-down-600x900.jpg 600w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/32-a-long-way-down-150x225.jpg 150w" sizes="(max-width: 300px) 100vw, 300px">
																</a>
															</div>
															<div class="movie__body">
																<div class="movie__info">
																	<div class="movie__info--head">
																		<div class="movie__meta">
																			<span class="movie__meta--release-year">2018</span>
																			<span class="movie__meta--genre">
																				<a href="https://demo3.madrasthemes.com/vodi-demos/main/movie-genre/action/" rel="tag">Action</a>, <a href="https://demo3.madrasthemes.com/vodi-demos/main/movie-genre/documentary/" rel="tag">Documentary</a>
																			</span>
																		</div>
																		<a href="https://demo3.madrasthemes.com/vodi-demos/main/movie/my-generation/" class="masvideos-LoopMovie-link masvideos-loop-movie__link movie__link">
																			<h3 class="masvideos-loop-movie__title  movie__title">My Generation</h3>
																		</a>
																	</div> <div class="movie__short-description">
																		<div>
																			<p>The cultural revolution that occurred in the 1960s England is explored in this documentary.</p>
																		</div> </div>
																		<div class="movie__actions">
																			<a href="https://demo3.madrasthemes.com/vodi-demos/main/movie/my-generation/" class="movie-actions--link_watch">Watch Now</a> <div class="movie-actions--link_add-to-playlist dropdown">
																				<a class="dropdown-toggle" href="https://demo3.madrasthemes.com/vodi-demos/main/movie/my-generation/" data-toggle="dropdown">+ Playlist</a>
																				<div class="dropdown-menu">
																					<a class="login-link" href="https://demo3.madrasthemes.com/vodi-demos/main/my-account/movie-playlists/">Sign in to add this movie to a playlist.</a> </div>
																				</div>
																			</div>
																		</div>
																		<div class="movie__review-info"> <a href="https://demo3.madrasthemes.com/vodi-demos/main/movie/my-generation/#reviews" class="avg-rating">
																			<span class="rating-with-count">
																				<svg class="vodi-svg" width="40px" height="39px" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 39">
																					<title>play</title>
																					<path fill-rule="evenodd" d="M19.633,-0.000 C21.509,0.035 21.530,1.174 22.167,2.414 C23.329,4.679 24.406,7.067 25.572,9.338 C25.853,9.886 26.431,11.640 26.918,11.834 C27.486,12.203 29.345,12.109 30.165,12.316 C32.170,12.825 34.489,12.860 36.500,13.364 C37.516,13.618 38.689,13.413 39.430,13.927 C39.689,14.107 39.770,14.504 39.984,14.732 C40.047,16.499 39.096,16.843 38.163,17.792 C36.473,19.509 34.784,21.227 33.095,22.944 C32.585,23.462 31.092,24.543 31.036,25.359 C31.423,25.951 31.307,27.455 31.511,28.258 C32.138,30.727 32.213,33.522 32.857,35.987 C33.142,37.078 33.016,38.241 32.303,38.724 C31.108,39.533 29.632,38.193 28.819,37.758 C26.695,36.623 24.601,35.624 22.483,34.457 C21.979,34.179 20.607,33.178 20.108,33.088 C19.748,33.023 18.163,34.107 17.812,34.296 C15.557,35.505 13.340,36.640 11.080,37.839 C10.548,38.120 9.180,39.226 8.309,38.966 C6.955,38.558 6.874,36.993 7.280,35.423 C7.716,33.733 7.697,31.880 8.151,30.109 C8.527,28.642 8.907,26.529 9.022,24.957 C8.092,24.344 7.202,23.107 6.408,22.300 C4.760,20.625 3.059,18.990 1.340,17.389 C0.646,16.742 -0.578,15.515 0.311,14.249 C0.915,13.388 2.364,13.656 3.557,13.364 C6.678,12.599 10.114,12.468 13.298,11.834 C14.186,9.747 15.306,7.711 16.307,5.716 C16.954,4.426 17.496,3.163 18.128,1.931 C18.334,1.531 18.358,1.093 18.603,0.724 C18.845,0.362 19.299,0.273 19.633,-0.000 Z">
																					</path>
																				</svg> <span class="avg-rating-number"> 10.0</span>
																			</span>
																			<span class="rating-number-with-text">
																				<span class="avg-rating-number"> 10.0</span>
																				<span class="avg-rating-text">
																					<span>1</span> Vote </span>
																				</span>
																			</a>
																			<div class="viewers-count">
																			</div>
																		</div>
																	</div>
																</div>
																<div class="post-266 movie type-movie status-publish has-post-thumbnail hentry movie_genre-action movie_genre-family movie_tag-4k-ultra movie_tag-brother movie_tag-premieres movie_tag-viking">
																	<div class="movie__poster">
																		<a href="https://demo3.madrasthemes.com/vodi-demos/main/movie/breath/" class="masvideos-LoopMovie-link masvideos-loop-movie__link movie__link">
																			<img width="300" height="450" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/8-300x450.jpg" class="movie__poster--image" alt="" srcset="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/8-300x450.jpg 300w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/8-66x98.jpg 66w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/8-200x300.jpg 200w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/8-600x900.jpg 600w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/8-150x225.jpg 150w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/8.jpg 668w" sizes="(max-width: 300px) 100vw, 300px">
																		</a>
																	</div>
																	<div class="movie__body">
																		<div class="movie__info">
																			<div class="movie__info--head">
																				<div class="movie__meta">
																					<span class="movie__meta--release-year">2018</span>
																					<span class="movie__meta--genre">
																						<a href="https://demo3.madrasthemes.com/vodi-demos/main/movie-genre/action/" rel="tag">Action</a>, <a href="https://demo3.madrasthemes.com/vodi-demos/main/movie-genre/family/" rel="tag">Family</a>
																					</span>
																				</div>
																				<a href="https://demo3.madrasthemes.com/vodi-demos/main/movie/breath/" class="masvideos-LoopMovie-link masvideos-loop-movie__link movie__link">
																					<h3 class="masvideos-loop-movie__title  movie__title">Breath</h3>
																				</a>
																			</div> <div class="movie__short-description">
																				<div>
																					<p>he inspiring and unknown true story behind MercyMe's beloved, chart topping song that brings ultimate hope to so many is a gripping reminder of the power of true forgiveness.</p> </div> </div>
																					<div class="movie__actions">
																						<a href="https://demo3.madrasthemes.com/vodi-demos/main/movie/breath/" class="movie-actions--link_watch">Watch Now</a> <div class="movie-actions--link_add-to-playlist dropdown">
																							<a class="dropdown-toggle" href="https://demo3.madrasthemes.com/vodi-demos/main/movie/breath/" data-toggle="dropdown">+ Playlist</a>
																							<div class="dropdown-menu">
																								<a class="login-link" href="https://demo3.madrasthemes.com/vodi-demos/main/my-account/movie-playlists/">Sign in to add this movie to a playlist.</a> </div>
																							</div>
																						</div>
																					</div>
																					<div class="movie__review-info"> <a href="https://demo3.madrasthemes.com/vodi-demos/main/movie/breath/#reviews" class="avg-rating">
																						<span class="rating-with-count">
																							<svg class="vodi-svg" width="40px" height="39px" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 39">
																								<title>play</title>
																								<path fill-rule="evenodd" d="M19.633,-0.000 C21.509,0.035 21.530,1.174 22.167,2.414 C23.329,4.679 24.406,7.067 25.572,9.338 C25.853,9.886 26.431,11.640 26.918,11.834 C27.486,12.203 29.345,12.109 30.165,12.316 C32.170,12.825 34.489,12.860 36.500,13.364 C37.516,13.618 38.689,13.413 39.430,13.927 C39.689,14.107 39.770,14.504 39.984,14.732 C40.047,16.499 39.096,16.843 38.163,17.792 C36.473,19.509 34.784,21.227 33.095,22.944 C32.585,23.462 31.092,24.543 31.036,25.359 C31.423,25.951 31.307,27.455 31.511,28.258 C32.138,30.727 32.213,33.522 32.857,35.987 C33.142,37.078 33.016,38.241 32.303,38.724 C31.108,39.533 29.632,38.193 28.819,37.758 C26.695,36.623 24.601,35.624 22.483,34.457 C21.979,34.179 20.607,33.178 20.108,33.088 C19.748,33.023 18.163,34.107 17.812,34.296 C15.557,35.505 13.340,36.640 11.080,37.839 C10.548,38.120 9.180,39.226 8.309,38.966 C6.955,38.558 6.874,36.993 7.280,35.423 C7.716,33.733 7.697,31.880 8.151,30.109 C8.527,28.642 8.907,26.529 9.022,24.957 C8.092,24.344 7.202,23.107 6.408,22.300 C4.760,20.625 3.059,18.990 1.340,17.389 C0.646,16.742 -0.578,15.515 0.311,14.249 C0.915,13.388 2.364,13.656 3.557,13.364 C6.678,12.599 10.114,12.468 13.298,11.834 C14.186,9.747 15.306,7.711 16.307,5.716 C16.954,4.426 17.496,3.163 18.128,1.931 C18.334,1.531 18.358,1.093 18.603,0.724 C18.845,0.362 19.299,0.273 19.633,-0.000 Z">
																								</path>
																							</svg> <span class="avg-rating-number"> 7.0</span>
																						</span>
																						<span class="rating-number-with-text">
																							<span class="avg-rating-number"> 7.0</span>
																							<span class="avg-rating-text">
																								<span>1</span> Vote </span>
																							</span>
																						</a>
																						<div class="viewers-count">
																						</div>
																					</div>
																				</div>
																			</div>
																			<div class="post-290 movie type-movie status-publish has-post-thumbnail hentry movie_genre-action movie_genre-adventure movie_tag-4k-ultra movie_tag-brother movie_tag-king movie_tag-viking">
																				<div class="movie__poster">
																					<a href="https://demo3.madrasthemes.com/vodi-demos/main/movie/oceans-8/" class="masvideos-LoopMovie-link masvideos-loop-movie__link movie__link">
																						<img width="300" height="450" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/20-300x450.jpg" class="movie__poster--image" alt="" srcset="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/20-300x450.jpg 300w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/20-66x98.jpg 66w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/20-200x300.jpg 200w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/20-600x900.jpg 600w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/20-150x225.jpg 150w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/20.jpg 668w" sizes="(max-width: 300px) 100vw, 300px">
																					</a>
																				</div>
																				<div class="movie__body">
																					<div class="movie__info">
																						<div class="movie__info--head">
																							<div class="movie__meta">
																								<span class="movie__meta--release-year">2018</span>
																								<span class="movie__meta--genre">
																									<a href="https://demo3.madrasthemes.com/vodi-demos/main/movie-genre/action/" rel="tag">Action</a>, <a href="https://demo3.madrasthemes.com/vodi-demos/main/movie-genre/adventure/" rel="tag">Adventure</a>
																								</span>
																							</div>
																							<a href="https://demo3.madrasthemes.com/vodi-demos/main/movie/oceans-8/" class="masvideos-LoopMovie-link masvideos-loop-movie__link movie__link">
																								<h3 class="masvideos-loop-movie__title  movie__title">Ocean's 8</h3>
																							</a>
																						</div> <div class="movie__short-description">
																							<div>
																								<p>Debbie Ocean gathers an all-female crew to attempt an impossible heist at New York City's yearly Met Gala.</p> </div> </div>
																								<div class="movie__actions">
																									<a href="https://demo3.madrasthemes.com/vodi-demos/main/movie/oceans-8/" class="movie-actions--link_watch">Watch Now</a> <div class="movie-actions--link_add-to-playlist dropdown">
																										<a class="dropdown-toggle" href="https://demo3.madrasthemes.com/vodi-demos/main/movie/oceans-8/" data-toggle="dropdown">+ Playlist</a>
																										<div class="dropdown-menu">
																											<a class="login-link" href="https://demo3.madrasthemes.com/vodi-demos/main/my-account/movie-playlists/">Sign in to add this movie to a playlist.</a> </div>
																										</div>
																									</div>
																								</div>
																								<div class="movie__review-info"> <a href="https://demo3.madrasthemes.com/vodi-demos/main/movie/oceans-8/#reviews" class="avg-rating">
																									<span class="rating-with-count">
																										<svg class="vodi-svg" width="40px" height="39px" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 39">
																											<title>play</title>
																											<path fill-rule="evenodd" d="M19.633,-0.000 C21.509,0.035 21.530,1.174 22.167,2.414 C23.329,4.679 24.406,7.067 25.572,9.338 C25.853,9.886 26.431,11.640 26.918,11.834 C27.486,12.203 29.345,12.109 30.165,12.316 C32.170,12.825 34.489,12.860 36.500,13.364 C37.516,13.618 38.689,13.413 39.430,13.927 C39.689,14.107 39.770,14.504 39.984,14.732 C40.047,16.499 39.096,16.843 38.163,17.792 C36.473,19.509 34.784,21.227 33.095,22.944 C32.585,23.462 31.092,24.543 31.036,25.359 C31.423,25.951 31.307,27.455 31.511,28.258 C32.138,30.727 32.213,33.522 32.857,35.987 C33.142,37.078 33.016,38.241 32.303,38.724 C31.108,39.533 29.632,38.193 28.819,37.758 C26.695,36.623 24.601,35.624 22.483,34.457 C21.979,34.179 20.607,33.178 20.108,33.088 C19.748,33.023 18.163,34.107 17.812,34.296 C15.557,35.505 13.340,36.640 11.080,37.839 C10.548,38.120 9.180,39.226 8.309,38.966 C6.955,38.558 6.874,36.993 7.280,35.423 C7.716,33.733 7.697,31.880 8.151,30.109 C8.527,28.642 8.907,26.529 9.022,24.957 C8.092,24.344 7.202,23.107 6.408,22.300 C4.760,20.625 3.059,18.990 1.340,17.389 C0.646,16.742 -0.578,15.515 0.311,14.249 C0.915,13.388 2.364,13.656 3.557,13.364 C6.678,12.599 10.114,12.468 13.298,11.834 C14.186,9.747 15.306,7.711 16.307,5.716 C16.954,4.426 17.496,3.163 18.128,1.931 C18.334,1.531 18.358,1.093 18.603,0.724 C18.845,0.362 19.299,0.273 19.633,-0.000 Z">
																											</path>
																										</svg> <span class="avg-rating-number"> 9.0</span>
																									</span>
																									<span class="rating-number-with-text">
																										<span class="avg-rating-number"> 9.0</span>
																										<span class="avg-rating-text">
																											<span>1</span> Vote </span>
																										</span>
																									</a>
																									<div class="viewers-count">
																									</div>
																								</div>
																							</div>
																						</div>
																						<div class="post-702 movie type-movie status-publish has-post-thumbnail hentry movie_genre-action movie_genre-family movie_tag-4k-ultra movie_tag-brother movie_tag-premieres movie_tag-viking">
																							<div class="movie__poster">
																								<a href="https://demo3.madrasthemes.com/vodi-demos/main/movie/a-kid-like-jake/" class="masvideos-LoopMovie-link masvideos-loop-movie__link movie__link">
																									<img width="300" height="450" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/45-rybka-zwana-wanda-300x450.jpg" class="movie__poster--image" alt="" srcset="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/45-rybka-zwana-wanda-300x450.jpg 300w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/45-rybka-zwana-wanda-66x98.jpg 66w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/45-rybka-zwana-wanda-600x900.jpg 600w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/45-rybka-zwana-wanda-150x225.jpg 150w" sizes="(max-width: 300px) 100vw, 300px">
																								</a>
																							</div>
																							<div class="movie__body">
																								<div class="movie__info">
																									<div class="movie__info--head">
																										<div class="movie__meta">
																											<span class="movie__meta--release-year">2018</span>
																											<span class="movie__meta--genre">
																												<a href="https://demo3.madrasthemes.com/vodi-demos/main/movie-genre/action/" rel="tag">Action</a>, <a href="https://demo3.madrasthemes.com/vodi-demos/main/movie-genre/family/" rel="tag">Family</a>
																											</span>
																										</div>
																										<a href="https://demo3.madrasthemes.com/vodi-demos/main/movie/a-kid-like-jake/" class="masvideos-LoopMovie-link masvideos-loop-movie__link movie__link">
																											<h3 class="masvideos-loop-movie__title  movie__title">A kid LIke Jake</h3>
																										</a>
																									</div> <div class="movie__short-description">
																										<div>
																											<p>Loving parents of a four-year-old must come to terms with their child being transgender.</p>
																										</div> </div>
																										<div class="movie__actions">
																											<a href="https://demo3.madrasthemes.com/vodi-demos/main/movie/a-kid-like-jake/" class="movie-actions--link_watch">Watch Now</a> <div class="movie-actions--link_add-to-playlist dropdown">
																												<a class="dropdown-toggle" href="https://demo3.madrasthemes.com/vodi-demos/main/movie/a-kid-like-jake/" data-toggle="dropdown">+ Playlist</a>
																												<div class="dropdown-menu">
																													<a class="login-link" href="https://demo3.madrasthemes.com/vodi-demos/main/my-account/movie-playlists/">Sign in to add this movie to a playlist.</a> </div>
																												</div>
																											</div>
																										</div>
																										<div class="movie__review-info"> <a href="https://demo3.madrasthemes.com/vodi-demos/main/movie/a-kid-like-jake/#reviews" class="avg-rating">
																											<span class="rating-with-count">
																												<svg class="vodi-svg" width="40px" height="39px" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 39">
																													<title>play</title>
																													<path fill-rule="evenodd" d="M19.633,-0.000 C21.509,0.035 21.530,1.174 22.167,2.414 C23.329,4.679 24.406,7.067 25.572,9.338 C25.853,9.886 26.431,11.640 26.918,11.834 C27.486,12.203 29.345,12.109 30.165,12.316 C32.170,12.825 34.489,12.860 36.500,13.364 C37.516,13.618 38.689,13.413 39.430,13.927 C39.689,14.107 39.770,14.504 39.984,14.732 C40.047,16.499 39.096,16.843 38.163,17.792 C36.473,19.509 34.784,21.227 33.095,22.944 C32.585,23.462 31.092,24.543 31.036,25.359 C31.423,25.951 31.307,27.455 31.511,28.258 C32.138,30.727 32.213,33.522 32.857,35.987 C33.142,37.078 33.016,38.241 32.303,38.724 C31.108,39.533 29.632,38.193 28.819,37.758 C26.695,36.623 24.601,35.624 22.483,34.457 C21.979,34.179 20.607,33.178 20.108,33.088 C19.748,33.023 18.163,34.107 17.812,34.296 C15.557,35.505 13.340,36.640 11.080,37.839 C10.548,38.120 9.180,39.226 8.309,38.966 C6.955,38.558 6.874,36.993 7.280,35.423 C7.716,33.733 7.697,31.880 8.151,30.109 C8.527,28.642 8.907,26.529 9.022,24.957 C8.092,24.344 7.202,23.107 6.408,22.300 C4.760,20.625 3.059,18.990 1.340,17.389 C0.646,16.742 -0.578,15.515 0.311,14.249 C0.915,13.388 2.364,13.656 3.557,13.364 C6.678,12.599 10.114,12.468 13.298,11.834 C14.186,9.747 15.306,7.711 16.307,5.716 C16.954,4.426 17.496,3.163 18.128,1.931 C18.334,1.531 18.358,1.093 18.603,0.724 C18.845,0.362 19.299,0.273 19.633,-0.000 Z">
																													</path>
																												</svg> <span class="avg-rating-number"> 8.0</span>
																											</span>
																											<span class="rating-number-with-text">
																												<span class="avg-rating-number"> 8.0</span>
																												<span class="avg-rating-text">
																													<span>1</span> Vote </span>
																												</span>
																											</a>
																											<div class="viewers-count">
																											</div>
																										</div>
																									</div>
																								</div>
																								<div class="post-312 movie type-movie status-publish has-post-thumbnail hentry movie_genre-action movie_genre-thriller movie_tag-4k-ultra movie_tag-brother movie_tag-premieres movie_tag-viking">
																									<span class="movie__badge">
																										<span class="movie__badge--featured">Featured</span>
																									</span>
																									<div class="movie__poster">
																										<a href="https://demo3.madrasthemes.com/vodi-demos/main/movie/the-last-witness/" class="masvideos-LoopMovie-link masvideos-loop-movie__link movie__link">
																											<img width="300" height="450" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/31-300x450.jpg" class="movie__poster--image" alt="" srcset="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/31-300x450.jpg 300w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/31-66x98.jpg 66w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/31-200x300.jpg 200w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/31-600x900.jpg 600w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/31-150x225.jpg 150w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/31.jpg 668w" sizes="(max-width: 300px) 100vw, 300px">
																										</a>
																									</div>
																									<div class="movie__body">
																										<div class="movie__info">
																											<div class="movie__info--head">
																												<div class="movie__meta">
																													<span class="movie__meta--release-year">2018</span>
																													<span class="movie__meta--genre">
																														<a href="https://demo3.madrasthemes.com/vodi-demos/main/movie-genre/action/" rel="tag">Action</a>, <a href="https://demo3.madrasthemes.com/vodi-demos/main/movie-genre/thriller/" rel="tag">Thriller</a>
																													</span>
																												</div>
																												<a href="https://demo3.madrasthemes.com/vodi-demos/main/movie/the-last-witness/" class="masvideos-LoopMovie-link masvideos-loop-movie__link movie__link">
																													<h3 class="masvideos-loop-movie__title  movie__title">The Last Witness</h3>
																												</a>
																											</div> <div class="movie__short-description">
																												<div>In 1892, a legendary Army captain reluctantly agrees to escort a Cheyenne chief and his family through dangerous territory.</div> </div>
																												<div class="movie__actions">
																													<a href="https://demo3.madrasthemes.com/vodi-demos/main/movie/the-last-witness/" class="movie-actions--link_watch">Watch Now</a> <div class="movie-actions--link_add-to-playlist dropdown">
																														<a class="dropdown-toggle" href="https://demo3.madrasthemes.com/vodi-demos/main/movie/the-last-witness/" data-toggle="dropdown">+ Playlist</a>
																														<div class="dropdown-menu">
																															<a class="login-link" href="https://demo3.madrasthemes.com/vodi-demos/main/my-account/movie-playlists/">Sign in to add this movie to a playlist.</a> </div>
																														</div>
																													</div>
																												</div>
																												<div class="movie__review-info"> <a href="https://demo3.madrasthemes.com/vodi-demos/main/movie/the-last-witness/#reviews" class="avg-rating">
																													<span class="rating-with-count">
																														<svg class="vodi-svg" width="40px" height="39px" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 39">
																															<title>play</title>
																															<path fill-rule="evenodd" d="M19.633,-0.000 C21.509,0.035 21.530,1.174 22.167,2.414 C23.329,4.679 24.406,7.067 25.572,9.338 C25.853,9.886 26.431,11.640 26.918,11.834 C27.486,12.203 29.345,12.109 30.165,12.316 C32.170,12.825 34.489,12.860 36.500,13.364 C37.516,13.618 38.689,13.413 39.430,13.927 C39.689,14.107 39.770,14.504 39.984,14.732 C40.047,16.499 39.096,16.843 38.163,17.792 C36.473,19.509 34.784,21.227 33.095,22.944 C32.585,23.462 31.092,24.543 31.036,25.359 C31.423,25.951 31.307,27.455 31.511,28.258 C32.138,30.727 32.213,33.522 32.857,35.987 C33.142,37.078 33.016,38.241 32.303,38.724 C31.108,39.533 29.632,38.193 28.819,37.758 C26.695,36.623 24.601,35.624 22.483,34.457 C21.979,34.179 20.607,33.178 20.108,33.088 C19.748,33.023 18.163,34.107 17.812,34.296 C15.557,35.505 13.340,36.640 11.080,37.839 C10.548,38.120 9.180,39.226 8.309,38.966 C6.955,38.558 6.874,36.993 7.280,35.423 C7.716,33.733 7.697,31.880 8.151,30.109 C8.527,28.642 8.907,26.529 9.022,24.957 C8.092,24.344 7.202,23.107 6.408,22.300 C4.760,20.625 3.059,18.990 1.340,17.389 C0.646,16.742 -0.578,15.515 0.311,14.249 C0.915,13.388 2.364,13.656 3.557,13.364 C6.678,12.599 10.114,12.468 13.298,11.834 C14.186,9.747 15.306,7.711 16.307,5.716 C16.954,4.426 17.496,3.163 18.128,1.931 C18.334,1.531 18.358,1.093 18.603,0.724 C18.845,0.362 19.299,0.273 19.633,-0.000 Z">
																															</path>
																														</svg> <span class="avg-rating-number"> 8.0</span>
																													</span>
																													<span class="rating-number-with-text">
																														<span class="avg-rating-number"> 8.0</span>
																														<span class="avg-rating-text">
																															<span>1</span> Vote </span>
																														</span>
																													</a>
																													<div class="viewers-count">
																													</div>
																												</div>
																											</div>
																										</div>
																										<div class="post-330 movie type-movie status-publish has-post-thumbnail hentry movie_genre-action movie_genre-thriller movie_tag-4k-ultra movie_tag-brother movie_tag-premieres movie_tag-viking movie_music-alan-silvestri movie_photos-jack-kirby movie_photos-jim-starlin movie_photos-trent-opaloch">
																											<div class="movie__poster">
																												<a href="https://demo3.madrasthemes.com/vodi-demos/main/movie/the-tale/" class="masvideos-LoopMovie-link masvideos-loop-movie__link movie__link">
																													<img width="300" height="450" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/40-murder-by-death-300x450.jpg" class="movie__poster--image" alt="" srcset="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/40-murder-by-death-300x450.jpg 300w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/40-murder-by-death-66x98.jpg 66w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/40-murder-by-death-600x900.jpg 600w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/40-murder-by-death-150x225.jpg 150w" sizes="(max-width: 300px) 100vw, 300px">
																												</a>
																											</div>
																											<div class="movie__body">
																												<div class="movie__info">
																													<div class="movie__info--head">
																														<div class="movie__meta">
																															<span class="movie__meta--release-year">2018</span>
																															<span class="movie__meta--genre">
																																<a href="https://demo3.madrasthemes.com/vodi-demos/main/movie-genre/action/" rel="tag">Action</a>, <a href="https://demo3.madrasthemes.com/vodi-demos/main/movie-genre/thriller/" rel="tag">Thriller</a>
																															</span>
																														</div>
																														<a href="https://demo3.madrasthemes.com/vodi-demos/main/movie/the-tale/" class="masvideos-LoopMovie-link masvideos-loop-movie__link movie__link">
																															<h3 class="masvideos-loop-movie__title  movie__title">The Tale</h3>
																														</a>
																													</div> <div class="movie__short-description">
																														<div>
																															<p>A woman filming a documentary on childhood rape victims starts to question the nature of her childhood relationship with her riding instructor and running coach</p>
																														</div> </div>
																														<div class="movie__actions">
																															<a href="https://demo3.madrasthemes.com/vodi-demos/main/movie/the-tale/" class="movie-actions--link_watch">Watch Now</a> <div class="movie-actions--link_add-to-playlist dropdown">
																																<a class="dropdown-toggle" href="https://demo3.madrasthemes.com/vodi-demos/main/movie/the-tale/" data-toggle="dropdown">+ Playlist</a>
																																<div class="dropdown-menu">
																																	<a class="login-link" href="https://demo3.madrasthemes.com/vodi-demos/main/my-account/movie-playlists/">Sign in to add this movie to a playlist.</a> </div>
																																</div>
																															</div>
																														</div>
																														<div class="movie__review-info"> <a href="https://demo3.madrasthemes.com/vodi-demos/main/movie/the-tale/#reviews" class="avg-rating">
																															<span class="rating-with-count">
																																<svg class="vodi-svg" width="40px" height="39px" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 39">
																																	<title>play</title>
																																	<path fill-rule="evenodd" d="M19.633,-0.000 C21.509,0.035 21.530,1.174 22.167,2.414 C23.329,4.679 24.406,7.067 25.572,9.338 C25.853,9.886 26.431,11.640 26.918,11.834 C27.486,12.203 29.345,12.109 30.165,12.316 C32.170,12.825 34.489,12.860 36.500,13.364 C37.516,13.618 38.689,13.413 39.430,13.927 C39.689,14.107 39.770,14.504 39.984,14.732 C40.047,16.499 39.096,16.843 38.163,17.792 C36.473,19.509 34.784,21.227 33.095,22.944 C32.585,23.462 31.092,24.543 31.036,25.359 C31.423,25.951 31.307,27.455 31.511,28.258 C32.138,30.727 32.213,33.522 32.857,35.987 C33.142,37.078 33.016,38.241 32.303,38.724 C31.108,39.533 29.632,38.193 28.819,37.758 C26.695,36.623 24.601,35.624 22.483,34.457 C21.979,34.179 20.607,33.178 20.108,33.088 C19.748,33.023 18.163,34.107 17.812,34.296 C15.557,35.505 13.340,36.640 11.080,37.839 C10.548,38.120 9.180,39.226 8.309,38.966 C6.955,38.558 6.874,36.993 7.280,35.423 C7.716,33.733 7.697,31.880 8.151,30.109 C8.527,28.642 8.907,26.529 9.022,24.957 C8.092,24.344 7.202,23.107 6.408,22.300 C4.760,20.625 3.059,18.990 1.340,17.389 C0.646,16.742 -0.578,15.515 0.311,14.249 C0.915,13.388 2.364,13.656 3.557,13.364 C6.678,12.599 10.114,12.468 13.298,11.834 C14.186,9.747 15.306,7.711 16.307,5.716 C16.954,4.426 17.496,3.163 18.128,1.931 C18.334,1.531 18.358,1.093 18.603,0.724 C18.845,0.362 19.299,0.273 19.633,-0.000 Z">
																																	</path>
																																</svg> <span class="avg-rating-number"> 8.5</span>
																															</span>
																															<span class="rating-number-with-text">
																																<span class="avg-rating-number"> 8.5</span>
																																<span class="avg-rating-text">
																																	<span>2</span> Votes </span>
																																</span>
																															</a>
																															<div class="viewers-count">
																															</div>
																														</div>
																													</div>
																												</div>
																												<div class="post-306 movie type-movie status-publish has-post-thumbnail hentry movie_genre-action movie_genre-thriller movie_tag-4k-ultra movie_tag-brother movie_tag-premieres movie_tag-viking">
																													<div class="movie__poster">
																														<a href="https://demo3.madrasthemes.com/vodi-demos/main/movie/black-water/" class="masvideos-LoopMovie-link masvideos-loop-movie__link movie__link">
																															<img width="300" height="450" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/28-300x450.jpg" class="movie__poster--image" alt="" srcset="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/28-300x450.jpg 300w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/28-66x98.jpg 66w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/28-200x300.jpg 200w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/28-600x900.jpg 600w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/28-150x225.jpg 150w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/28.jpg 668w" sizes="(max-width: 300px) 100vw, 300px">
																														</a>
																													</div>
																													<div class="movie__body">
																														<div class="movie__info">
																															<div class="movie__info--head">
																																<div class="movie__meta">
																																	<span class="movie__meta--release-year">2018</span>
																																	<span class="movie__meta--genre">
																																		<a href="https://demo3.madrasthemes.com/vodi-demos/main/movie-genre/action/" rel="tag">Action</a>, <a href="https://demo3.madrasthemes.com/vodi-demos/main/movie-genre/thriller/" rel="tag">Thriller</a>
																																	</span>
																																</div>
																																<a href="https://demo3.madrasthemes.com/vodi-demos/main/movie/black-water/" class="masvideos-LoopMovie-link masvideos-loop-movie__link movie__link">
																																	<h3 class="masvideos-loop-movie__title  movie__title">Black Water</h3>
																																</a>
																															</div> <div class="movie__short-description">
																																<div>
																																	<p>A deep cover operative awakens to find himself imprisoned in a CIA black site on a submarine.</p> </div> </div>
																																	<div class="movie__actions">
																																		<a href="https://demo3.madrasthemes.com/vodi-demos/main/movie/black-water/" class="movie-actions--link_watch">Watch Now</a> <div class="movie-actions--link_add-to-playlist dropdown">
																																			<a class="dropdown-toggle" href="https://demo3.madrasthemes.com/vodi-demos/main/movie/black-water/" data-toggle="dropdown">+ Playlist</a>
																																			<div class="dropdown-menu">
																																				<a class="login-link" href="https://demo3.madrasthemes.com/vodi-demos/main/my-account/movie-playlists/">Sign in to add this movie to a playlist.</a> </div>
																																			</div>
																																		</div>
																																	</div>
																																	<div class="movie__review-info"> <a href="https://demo3.madrasthemes.com/vodi-demos/main/movie/black-water/#reviews" class="avg-rating">
																																		<span class="rating-with-count">
																																			<svg class="vodi-svg" width="40px" height="39px" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 39">
																																				<title>play</title>
																																				<path fill-rule="evenodd" d="M19.633,-0.000 C21.509,0.035 21.530,1.174 22.167,2.414 C23.329,4.679 24.406,7.067 25.572,9.338 C25.853,9.886 26.431,11.640 26.918,11.834 C27.486,12.203 29.345,12.109 30.165,12.316 C32.170,12.825 34.489,12.860 36.500,13.364 C37.516,13.618 38.689,13.413 39.430,13.927 C39.689,14.107 39.770,14.504 39.984,14.732 C40.047,16.499 39.096,16.843 38.163,17.792 C36.473,19.509 34.784,21.227 33.095,22.944 C32.585,23.462 31.092,24.543 31.036,25.359 C31.423,25.951 31.307,27.455 31.511,28.258 C32.138,30.727 32.213,33.522 32.857,35.987 C33.142,37.078 33.016,38.241 32.303,38.724 C31.108,39.533 29.632,38.193 28.819,37.758 C26.695,36.623 24.601,35.624 22.483,34.457 C21.979,34.179 20.607,33.178 20.108,33.088 C19.748,33.023 18.163,34.107 17.812,34.296 C15.557,35.505 13.340,36.640 11.080,37.839 C10.548,38.120 9.180,39.226 8.309,38.966 C6.955,38.558 6.874,36.993 7.280,35.423 C7.716,33.733 7.697,31.880 8.151,30.109 C8.527,28.642 8.907,26.529 9.022,24.957 C8.092,24.344 7.202,23.107 6.408,22.300 C4.760,20.625 3.059,18.990 1.340,17.389 C0.646,16.742 -0.578,15.515 0.311,14.249 C0.915,13.388 2.364,13.656 3.557,13.364 C6.678,12.599 10.114,12.468 13.298,11.834 C14.186,9.747 15.306,7.711 16.307,5.716 C16.954,4.426 17.496,3.163 18.128,1.931 C18.334,1.531 18.358,1.093 18.603,0.724 C18.845,0.362 19.299,0.273 19.633,-0.000 Z">
																																				</path>
																																			</svg> <span class="avg-rating-number"> 10.0</span>
																																		</span>
																																		<span class="rating-number-with-text">
																																			<span class="avg-rating-number"> 10.0</span>
																																			<span class="avg-rating-text">
																																				<span>1</span> Vote </span>
																																			</span>
																																		</a>
																																		<div class="viewers-count">
																																		</div>
																																	</div>
																																</div>
																															</div>
																															<div class="post-270 movie type-movie status-publish has-post-thumbnail hentry movie_genre-action movie_genre-horror movie_tag-4k-ultra movie_tag-brother movie_tag-premieres movie_tag-viking">
																																<div class="movie__poster">
																																	<a href="https://demo3.madrasthemes.com/vodi-demos/main/movie/the-strangers-prey-at-night/" class="masvideos-LoopMovie-link masvideos-loop-movie__link movie__link">
																																		<img width="300" height="450" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/10-300x450.jpg" class="movie__poster--image" alt="" srcset="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/10-300x450.jpg 300w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/10-66x98.jpg 66w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/10-200x300.jpg 200w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/10-600x900.jpg 600w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/10-150x225.jpg 150w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/10.jpg 668w" sizes="(max-width: 300px) 100vw, 300px">
																																	</a>
																																</div>
																																<div class="movie__body">
																																	<div class="movie__info">
																																		<div class="movie__info--head">
																																			<div class="movie__meta">
																																				<span class="movie__meta--release-year">2018</span>
																																				<span class="movie__meta--genre">
																																					<a href="https://demo3.madrasthemes.com/vodi-demos/main/movie-genre/action/" rel="tag">Action</a>, <a href="https://demo3.madrasthemes.com/vodi-demos/main/movie-genre/horror/" rel="tag">Horror</a>
																																				</span>
																																			</div>
																																			<a href="https://demo3.madrasthemes.com/vodi-demos/main/movie/the-strangers-prey-at-night/" class="masvideos-LoopMovie-link masvideos-loop-movie__link movie__link">
																																				<h3 class="masvideos-loop-movie__title  movie__title">The Strangers: Prey At Night</h3>
																																			</a>
																																		</div> <div class="movie__short-description">
																																			<div>
																																				<p>A family of four staying at a secluded mobile home park for the night are stalked and then hunted by three masked psychopaths.</p> </div> </div>
																																				<div class="movie__actions">
																																					<a href="https://demo3.madrasthemes.com/vodi-demos/main/movie/the-strangers-prey-at-night/" class="movie-actions--link_watch">Watch Now</a> <div class="movie-actions--link_add-to-playlist dropdown">
																																						<a class="dropdown-toggle" href="https://demo3.madrasthemes.com/vodi-demos/main/movie/the-strangers-prey-at-night/" data-toggle="dropdown">+ Playlist</a>
																																						<div class="dropdown-menu">
																																							<a class="login-link" href="https://demo3.madrasthemes.com/vodi-demos/main/my-account/movie-playlists/">Sign in to add this movie to a playlist.</a> </div>
																																						</div>
																																					</div>
																																				</div>
																																				<div class="movie__review-info"> <a href="https://demo3.madrasthemes.com/vodi-demos/main/movie/the-strangers-prey-at-night/#reviews" class="avg-rating">
																																					<span class="rating-with-count">
																																						<svg class="vodi-svg" width="40px" height="39px" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 39">
																																							<title>play</title>
																																							<path fill-rule="evenodd" d="M19.633,-0.000 C21.509,0.035 21.530,1.174 22.167,2.414 C23.329,4.679 24.406,7.067 25.572,9.338 C25.853,9.886 26.431,11.640 26.918,11.834 C27.486,12.203 29.345,12.109 30.165,12.316 C32.170,12.825 34.489,12.860 36.500,13.364 C37.516,13.618 38.689,13.413 39.430,13.927 C39.689,14.107 39.770,14.504 39.984,14.732 C40.047,16.499 39.096,16.843 38.163,17.792 C36.473,19.509 34.784,21.227 33.095,22.944 C32.585,23.462 31.092,24.543 31.036,25.359 C31.423,25.951 31.307,27.455 31.511,28.258 C32.138,30.727 32.213,33.522 32.857,35.987 C33.142,37.078 33.016,38.241 32.303,38.724 C31.108,39.533 29.632,38.193 28.819,37.758 C26.695,36.623 24.601,35.624 22.483,34.457 C21.979,34.179 20.607,33.178 20.108,33.088 C19.748,33.023 18.163,34.107 17.812,34.296 C15.557,35.505 13.340,36.640 11.080,37.839 C10.548,38.120 9.180,39.226 8.309,38.966 C6.955,38.558 6.874,36.993 7.280,35.423 C7.716,33.733 7.697,31.880 8.151,30.109 C8.527,28.642 8.907,26.529 9.022,24.957 C8.092,24.344 7.202,23.107 6.408,22.300 C4.760,20.625 3.059,18.990 1.340,17.389 C0.646,16.742 -0.578,15.515 0.311,14.249 C0.915,13.388 2.364,13.656 3.557,13.364 C6.678,12.599 10.114,12.468 13.298,11.834 C14.186,9.747 15.306,7.711 16.307,5.716 C16.954,4.426 17.496,3.163 18.128,1.931 C18.334,1.531 18.358,1.093 18.603,0.724 C18.845,0.362 19.299,0.273 19.633,-0.000 Z">
																																							</path>
																																						</svg> <span class="avg-rating-number"> 9.0</span>
																																					</span>
																																					<span class="rating-number-with-text">
																																						<span class="avg-rating-number"> 9.0</span>
																																						<span class="avg-rating-text">
																																							<span>1</span> Vote </span>
																																						</span>
																																					</a>
																																					<div class="viewers-count">
																																					</div>
																																				</div>
																																			</div>
																																		</div>
																																		<div class="post-722 movie type-movie status-publish has-post-thumbnail hentry movie_genre-action movie_genre-comedy movie_tag-4k-ultra movie_tag-brother movie_tag-premieres movie_tag-viking">
																																			<div class="movie__poster">
																																				<a href="https://demo3.madrasthemes.com/vodi-demos/main/movie/oh-lucy/" class="masvideos-LoopMovie-link masvideos-loop-movie__link movie__link">
																																					<img width="300" height="450" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/33-a-woman-under-the-influence-300x450.jpg" class="movie__poster--image" alt="" srcset="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/33-a-woman-under-the-influence-300x450.jpg 300w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/33-a-woman-under-the-influence-66x98.jpg 66w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/33-a-woman-under-the-influence-600x900.jpg 600w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/33-a-woman-under-the-influence-150x225.jpg 150w" sizes="(max-width: 300px) 100vw, 300px">
																																				</a>
																																			</div>
																																			<div class="movie__body">
																																				<div class="movie__info">
																																					<div class="movie__info--head">
																																						<div class="movie__meta">
																																							<span class="movie__meta--release-year">2018</span>
																																							<span class="movie__meta--genre">
																																								<a href="https://demo3.madrasthemes.com/vodi-demos/main/movie-genre/action/" rel="tag">Action</a>, <a href="https://demo3.madrasthemes.com/vodi-demos/main/movie-genre/comedy/" rel="tag">Comedy</a>
																																							</span>
																																						</div>
																																						<a href="https://demo3.madrasthemes.com/vodi-demos/main/movie/oh-lucy/" class="masvideos-LoopMovie-link masvideos-loop-movie__link movie__link">
																																							<h3 class="masvideos-loop-movie__title  movie__title">Oh Lucy</h3>
																																						</a>
																																					</div> <div class="movie__short-description">
																																						<div>
																																							<p>A lonely woman living in Tokyo decides to take an English class where she discovers her alter ego, Lucy.</p> </div> </div>
																																							<div class="movie__actions">
																																								<a href="https://demo3.madrasthemes.com/vodi-demos/main/movie/oh-lucy/" class="movie-actions--link_watch">Watch Now</a> <div class="movie-actions--link_add-to-playlist dropdown">
																																									<a class="dropdown-toggle" href="https://demo3.madrasthemes.com/vodi-demos/main/movie/oh-lucy/" data-toggle="dropdown">+ Playlist</a>
																																									<div class="dropdown-menu">
																																										<a class="login-link" href="https://demo3.madrasthemes.com/vodi-demos/main/my-account/movie-playlists/">Sign in to add this movie to a playlist.</a> </div>
																																									</div>
																																								</div>
																																							</div>
																																							<div class="movie__review-info"> <a href="https://demo3.madrasthemes.com/vodi-demos/main/movie/oh-lucy/#reviews" class="avg-rating">
																																								<span class="rating-with-count">
																																									<svg class="vodi-svg" width="40px" height="39px" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 39">
																																										<title>play</title>
																																										<path fill-rule="evenodd" d="M19.633,-0.000 C21.509,0.035 21.530,1.174 22.167,2.414 C23.329,4.679 24.406,7.067 25.572,9.338 C25.853,9.886 26.431,11.640 26.918,11.834 C27.486,12.203 29.345,12.109 30.165,12.316 C32.170,12.825 34.489,12.860 36.500,13.364 C37.516,13.618 38.689,13.413 39.430,13.927 C39.689,14.107 39.770,14.504 39.984,14.732 C40.047,16.499 39.096,16.843 38.163,17.792 C36.473,19.509 34.784,21.227 33.095,22.944 C32.585,23.462 31.092,24.543 31.036,25.359 C31.423,25.951 31.307,27.455 31.511,28.258 C32.138,30.727 32.213,33.522 32.857,35.987 C33.142,37.078 33.016,38.241 32.303,38.724 C31.108,39.533 29.632,38.193 28.819,37.758 C26.695,36.623 24.601,35.624 22.483,34.457 C21.979,34.179 20.607,33.178 20.108,33.088 C19.748,33.023 18.163,34.107 17.812,34.296 C15.557,35.505 13.340,36.640 11.080,37.839 C10.548,38.120 9.180,39.226 8.309,38.966 C6.955,38.558 6.874,36.993 7.280,35.423 C7.716,33.733 7.697,31.880 8.151,30.109 C8.527,28.642 8.907,26.529 9.022,24.957 C8.092,24.344 7.202,23.107 6.408,22.300 C4.760,20.625 3.059,18.990 1.340,17.389 C0.646,16.742 -0.578,15.515 0.311,14.249 C0.915,13.388 2.364,13.656 3.557,13.364 C6.678,12.599 10.114,12.468 13.298,11.834 C14.186,9.747 15.306,7.711 16.307,5.716 C16.954,4.426 17.496,3.163 18.128,1.931 C18.334,1.531 18.358,1.093 18.603,0.724 C18.845,0.362 19.299,0.273 19.633,-0.000 Z">
																																										</path>
																																									</svg> <span class="avg-rating-number"> 8.0</span>
																																								</span>
																																								<span class="rating-number-with-text">
																																									<span class="avg-rating-number"> 8.0</span>
																																									<span class="avg-rating-text">
																																										<span>1</span> Vote </span>
																																									</span>
																																								</a>
																																								<div class="viewers-count">
																																								</div>
																																							</div>
																																						</div>
																																					</div>
																																					<div class="post-704 movie type-movie status-publish has-post-thumbnail hentry movie_genre-action movie_genre-drama movie_tag-4k-ultra movie_tag-brother movie_tag-premieres movie_tag-viking">
																																						<div class="movie__poster">
																																							<a href="https://demo3.madrasthemes.com/vodi-demos/main/movie/bpm/" class="masvideos-LoopMovie-link masvideos-loop-movie__link movie__link">
																																								<img width="300" height="450" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/46-she-is-funny-that-way-300x450.jpg" class="movie__poster--image" alt="" srcset="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/46-she-is-funny-that-way-300x450.jpg 300w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/46-she-is-funny-that-way-66x98.jpg 66w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/46-she-is-funny-that-way-600x900.jpg 600w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/46-she-is-funny-that-way-150x225.jpg 150w" sizes="(max-width: 300px) 100vw, 300px">
																																							</a>
																																						</div>
																																						<div class="movie__body">
																																							<div class="movie__info">
																																								<div class="movie__info--head">
																																									<div class="movie__meta">
																																										<span class="movie__meta--release-year">2018</span>
																																										<span class="movie__meta--genre">
																																											<a href="https://demo3.madrasthemes.com/vodi-demos/main/movie-genre/action/" rel="tag">Action</a>, <a href="https://demo3.madrasthemes.com/vodi-demos/main/movie-genre/drama/" rel="tag">Drama</a>
																																										</span>
																																									</div>
																																									<a href="https://demo3.madrasthemes.com/vodi-demos/main/movie/bpm/" class="masvideos-LoopMovie-link masvideos-loop-movie__link movie__link">
																																										<h3 class="masvideos-loop-movie__title  movie__title">Bpm</h3>
																																									</a>
																																								</div> <div class="movie__short-description">
																																									<div>
																																										<p>Members of the advocacy group ACT UP Paris demand action by the government and pharmaceutical companies to combat the AIDS epidemic in the early 1990s.</p> </div> </div>
																																										<div class="movie__actions">
																																											<a href="https://demo3.madrasthemes.com/vodi-demos/main/movie/bpm/" class="movie-actions--link_watch">Watch Now</a> <div class="movie-actions--link_add-to-playlist dropdown">
																																												<a class="dropdown-toggle" href="https://demo3.madrasthemes.com/vodi-demos/main/movie/bpm/" data-toggle="dropdown">+ Playlist</a>
																																												<div class="dropdown-menu">
																																													<a class="login-link" href="https://demo3.madrasthemes.com/vodi-demos/main/my-account/movie-playlists/">Sign in to add this movie to a playlist.</a> </div>
																																												</div>
																																											</div>
																																										</div>
																																										<div class="movie__review-info"> <a href="https://demo3.madrasthemes.com/vodi-demos/main/movie/bpm/#reviews" class="avg-rating">
																																											<span class="rating-with-count">
																																												<svg class="vodi-svg" width="40px" height="39px" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 39">
																																													<title>play</title>
																																													<path fill-rule="evenodd" d="M19.633,-0.000 C21.509,0.035 21.530,1.174 22.167,2.414 C23.329,4.679 24.406,7.067 25.572,9.338 C25.853,9.886 26.431,11.640 26.918,11.834 C27.486,12.203 29.345,12.109 30.165,12.316 C32.170,12.825 34.489,12.860 36.500,13.364 C37.516,13.618 38.689,13.413 39.430,13.927 C39.689,14.107 39.770,14.504 39.984,14.732 C40.047,16.499 39.096,16.843 38.163,17.792 C36.473,19.509 34.784,21.227 33.095,22.944 C32.585,23.462 31.092,24.543 31.036,25.359 C31.423,25.951 31.307,27.455 31.511,28.258 C32.138,30.727 32.213,33.522 32.857,35.987 C33.142,37.078 33.016,38.241 32.303,38.724 C31.108,39.533 29.632,38.193 28.819,37.758 C26.695,36.623 24.601,35.624 22.483,34.457 C21.979,34.179 20.607,33.178 20.108,33.088 C19.748,33.023 18.163,34.107 17.812,34.296 C15.557,35.505 13.340,36.640 11.080,37.839 C10.548,38.120 9.180,39.226 8.309,38.966 C6.955,38.558 6.874,36.993 7.280,35.423 C7.716,33.733 7.697,31.880 8.151,30.109 C8.527,28.642 8.907,26.529 9.022,24.957 C8.092,24.344 7.202,23.107 6.408,22.300 C4.760,20.625 3.059,18.990 1.340,17.389 C0.646,16.742 -0.578,15.515 0.311,14.249 C0.915,13.388 2.364,13.656 3.557,13.364 C6.678,12.599 10.114,12.468 13.298,11.834 C14.186,9.747 15.306,7.711 16.307,5.716 C16.954,4.426 17.496,3.163 18.128,1.931 C18.334,1.531 18.358,1.093 18.603,0.724 C18.845,0.362 19.299,0.273 19.633,-0.000 Z">
																																													</path>
																																												</svg> <span class="avg-rating-number"> 7.0</span>
																																											</span>
																																											<span class="rating-number-with-text">
																																												<span class="avg-rating-number"> 7.0</span>
																																												<span class="avg-rating-text">
																																													<span>1</span> Vote </span>
																																												</span>
																																											</a>
																																											<div class="viewers-count">
																																											</div>
																																										</div>
																																									</div>
																																								</div>
																																								<div class="post-294 movie type-movie status-publish has-post-thumbnail hentry movie_genre-action movie_genre-drama movie_tag-4k-ultra movie_tag-brother movie_tag-king movie_tag-viking">
																																									<div class="movie__poster">
																																										<a href="https://demo3.madrasthemes.com/vodi-demos/main/movie/midnight-sun/" class="masvideos-LoopMovie-link masvideos-loop-movie__link movie__link">
																																											<img width="300" height="450" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/22-300x450.jpg" class="movie__poster--image" alt="" srcset="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/22-300x450.jpg 300w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/22-66x98.jpg 66w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/22-200x300.jpg 200w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/22-600x900.jpg 600w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/22-150x225.jpg 150w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/22.jpg 668w" sizes="(max-width: 300px) 100vw, 300px">
																																										</a>
																																									</div>
																																									<div class="movie__body">
																																										<div class="movie__info">
																																											<div class="movie__info--head">
																																												<div class="movie__meta">
																																													<span class="movie__meta--release-year">2018</span>
																																													<span class="movie__meta--genre">
																																														<a href="https://demo3.madrasthemes.com/vodi-demos/main/movie-genre/action/" rel="tag">Action</a>, <a href="https://demo3.madrasthemes.com/vodi-demos/main/movie-genre/drama/" rel="tag">Drama</a>
																																													</span>
																																												</div>
																																												<a href="https://demo3.madrasthemes.com/vodi-demos/main/movie/midnight-sun/" class="masvideos-LoopMovie-link masvideos-loop-movie__link movie__link">
																																													<h3 class="masvideos-loop-movie__title  movie__title">Midnight Sun</h3>
																																												</a>
																																											</div> <div class="movie__short-description">
																																												<div>
																																													<p>A 17-year-old girl suffers from a condition that prevents her from being out in the sunlight.</p> </div> </div>
																																													<div class="movie__actions">
																																														<a href="https://demo3.madrasthemes.com/vodi-demos/main/movie/midnight-sun/" class="movie-actions--link_watch">Watch Now</a> <div class="movie-actions--link_add-to-playlist dropdown">
																																															<a class="dropdown-toggle" href="https://demo3.madrasthemes.com/vodi-demos/main/movie/midnight-sun/" data-toggle="dropdown">+ Playlist</a>
																																															<div class="dropdown-menu">
																																																<a class="login-link" href="https://demo3.madrasthemes.com/vodi-demos/main/my-account/movie-playlists/">Sign in to add this movie to a playlist.</a> </div>
																																															</div>
																																														</div>
																																													</div>
																																													<div class="movie__review-info"> <a href="https://demo3.madrasthemes.com/vodi-demos/main/movie/midnight-sun/#reviews" class="avg-rating">
																																														<span class="rating-with-count">
																																															<svg class="vodi-svg" width="40px" height="39px" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 39">
																																																<title>play</title>
																																																<path fill-rule="evenodd" d="M19.633,-0.000 C21.509,0.035 21.530,1.174 22.167,2.414 C23.329,4.679 24.406,7.067 25.572,9.338 C25.853,9.886 26.431,11.640 26.918,11.834 C27.486,12.203 29.345,12.109 30.165,12.316 C32.170,12.825 34.489,12.860 36.500,13.364 C37.516,13.618 38.689,13.413 39.430,13.927 C39.689,14.107 39.770,14.504 39.984,14.732 C40.047,16.499 39.096,16.843 38.163,17.792 C36.473,19.509 34.784,21.227 33.095,22.944 C32.585,23.462 31.092,24.543 31.036,25.359 C31.423,25.951 31.307,27.455 31.511,28.258 C32.138,30.727 32.213,33.522 32.857,35.987 C33.142,37.078 33.016,38.241 32.303,38.724 C31.108,39.533 29.632,38.193 28.819,37.758 C26.695,36.623 24.601,35.624 22.483,34.457 C21.979,34.179 20.607,33.178 20.108,33.088 C19.748,33.023 18.163,34.107 17.812,34.296 C15.557,35.505 13.340,36.640 11.080,37.839 C10.548,38.120 9.180,39.226 8.309,38.966 C6.955,38.558 6.874,36.993 7.280,35.423 C7.716,33.733 7.697,31.880 8.151,30.109 C8.527,28.642 8.907,26.529 9.022,24.957 C8.092,24.344 7.202,23.107 6.408,22.300 C4.760,20.625 3.059,18.990 1.340,17.389 C0.646,16.742 -0.578,15.515 0.311,14.249 C0.915,13.388 2.364,13.656 3.557,13.364 C6.678,12.599 10.114,12.468 13.298,11.834 C14.186,9.747 15.306,7.711 16.307,5.716 C16.954,4.426 17.496,3.163 18.128,1.931 C18.334,1.531 18.358,1.093 18.603,0.724 C18.845,0.362 19.299,0.273 19.633,-0.000 Z">
																																																</path>
																																															</svg> <span class="avg-rating-number"> 8.0</span>
																																														</span>
																																														<span class="rating-number-with-text">
																																															<span class="avg-rating-number"> 8.0</span>
																																															<span class="avg-rating-text">
																																																<span>1</span> Vote </span>
																																															</span>
																																														</a>
																																														<div class="viewers-count">
																																														</div>
																																													</div>
																																												</div>
																																											</div>
																																											<div class="post-712 movie type-movie status-publish has-post-thumbnail hentry movie_genre-action movie_genre-sci-fi movie_tag-brother movie_tag-king movie_tag-premieres movie_tag-viking">
																																												<div class="movie__poster">
																																													<a href="https://demo3.madrasthemes.com/vodi-demos/main/movie/pacific-rim-uprising/" class="masvideos-LoopMovie-link masvideos-loop-movie__link movie__link">
																																														<img width="300" height="450" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/50-the-300x450.jpg" class="movie__poster--image" alt="" srcset="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/50-the-300x450.jpg 300w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/50-the-66x98.jpg 66w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/50-the-600x900.jpg 600w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/50-the-150x225.jpg 150w" sizes="(max-width: 300px) 100vw, 300px">
																																													</a>
																																												</div>
																																												<div class="movie__body">
																																													<div class="movie__info">
																																														<div class="movie__info--head">
																																															<div class="movie__meta">
																																																<span class="movie__meta--release-year">2018</span>
																																																<span class="movie__meta--genre">
																																																	<a href="https://demo3.madrasthemes.com/vodi-demos/main/movie-genre/action/" rel="tag">Action</a>, <a href="https://demo3.madrasthemes.com/vodi-demos/main/movie-genre/sci-fi/" rel="tag">Sci-Fi</a>
																																																</span>
																																															</div>
																																															<a href="https://demo3.madrasthemes.com/vodi-demos/main/movie/pacific-rim-uprising/" class="masvideos-LoopMovie-link masvideos-loop-movie__link movie__link">
																																																<h3 class="masvideos-loop-movie__title  movie__title">Pacific Rim: Uprising</h3>
																																															</a>
																																														</div> <div class="movie__short-description">
																																															<div>
																																																<p>Jake Pentecost, son of Stacker Pentecost, reunites with Mako Mori to lead a new generation of Jaeger pilots, including rival Lambert and 15-year-old hacker Amara, against a new Kaiju threat.</p> </div> </div>
																																																<div class="movie__actions">
																																																	<a href="https://demo3.madrasthemes.com/vodi-demos/main/movie/pacific-rim-uprising/" class="movie-actions--link_watch">Watch Now</a> <div class="movie-actions--link_add-to-playlist dropdown">
																																																		<a class="dropdown-toggle" href="https://demo3.madrasthemes.com/vodi-demos/main/movie/pacific-rim-uprising/" data-toggle="dropdown">+ Playlist</a>
																																																		<div class="dropdown-menu">
																																																			<a class="login-link" href="https://demo3.madrasthemes.com/vodi-demos/main/my-account/movie-playlists/">Sign in to add this movie to a playlist.</a> </div>
																																																		</div>
																																																	</div>
																																																</div>
																																																<div class="movie__review-info"> <a href="https://demo3.madrasthemes.com/vodi-demos/main/movie/pacific-rim-uprising/#reviews" class="avg-rating">
																																																	<span class="rating-with-count">
																																																		<svg class="vodi-svg" width="40px" height="39px" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 39">
																																																			<title>play</title>
																																																			<path fill-rule="evenodd" d="M19.633,-0.000 C21.509,0.035 21.530,1.174 22.167,2.414 C23.329,4.679 24.406,7.067 25.572,9.338 C25.853,9.886 26.431,11.640 26.918,11.834 C27.486,12.203 29.345,12.109 30.165,12.316 C32.170,12.825 34.489,12.860 36.500,13.364 C37.516,13.618 38.689,13.413 39.430,13.927 C39.689,14.107 39.770,14.504 39.984,14.732 C40.047,16.499 39.096,16.843 38.163,17.792 C36.473,19.509 34.784,21.227 33.095,22.944 C32.585,23.462 31.092,24.543 31.036,25.359 C31.423,25.951 31.307,27.455 31.511,28.258 C32.138,30.727 32.213,33.522 32.857,35.987 C33.142,37.078 33.016,38.241 32.303,38.724 C31.108,39.533 29.632,38.193 28.819,37.758 C26.695,36.623 24.601,35.624 22.483,34.457 C21.979,34.179 20.607,33.178 20.108,33.088 C19.748,33.023 18.163,34.107 17.812,34.296 C15.557,35.505 13.340,36.640 11.080,37.839 C10.548,38.120 9.180,39.226 8.309,38.966 C6.955,38.558 6.874,36.993 7.280,35.423 C7.716,33.733 7.697,31.880 8.151,30.109 C8.527,28.642 8.907,26.529 9.022,24.957 C8.092,24.344 7.202,23.107 6.408,22.300 C4.760,20.625 3.059,18.990 1.340,17.389 C0.646,16.742 -0.578,15.515 0.311,14.249 C0.915,13.388 2.364,13.656 3.557,13.364 C6.678,12.599 10.114,12.468 13.298,11.834 C14.186,9.747 15.306,7.711 16.307,5.716 C16.954,4.426 17.496,3.163 18.128,1.931 C18.334,1.531 18.358,1.093 18.603,0.724 C18.845,0.362 19.299,0.273 19.633,-0.000 Z">
																																																			</path>
																																																		</svg> <span class="avg-rating-number"> 7.0</span>
																																																	</span>
																																																	<span class="rating-number-with-text">
																																																		<span class="avg-rating-number"> 7.0</span>
																																																		<span class="avg-rating-text">
																																																			<span>1</span> Vote </span>
																																																		</span>
																																																	</a>
																																																	<div class="viewers-count">
																																																	</div>
																																																</div>
																																															</div>
																																														</div>
																																														<div class="post-308 movie type-movie status-publish has-post-thumbnail hentry movie_genre-action movie_genre-romance movie_tag-4k-ultra movie_tag-brother movie_tag-king movie_tag-viking">
																																															<span class="movie__badge">
																																																<span class="movie__badge--featured">Featured</span>
																																															</span>
																																															<div class="movie__poster">
																																																<a href="https://demo3.madrasthemes.com/vodi-demos/main/movie/love-simon/" class="masvideos-LoopMovie-link masvideos-loop-movie__link movie__link">
																																																	<img width="300" height="450" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/29-300x450.jpg" class="movie__poster--image" alt="" srcset="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/29-300x450.jpg 300w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/29-66x98.jpg 66w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/29-200x300.jpg 200w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/29-600x900.jpg 600w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/29-150x225.jpg 150w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/29.jpg 668w" sizes="(max-width: 300px) 100vw, 300px">
																																																</a>
																																															</div>
																																															<div class="movie__body">
																																																<div class="movie__info">
																																																	<div class="movie__info--head">
																																																		<div class="movie__meta">
																																																			<span class="movie__meta--release-year">2018</span>
																																																			<span class="movie__meta--genre">
																																																				<a href="https://demo3.madrasthemes.com/vodi-demos/main/movie-genre/action/" rel="tag">Action</a>, <a href="https://demo3.madrasthemes.com/vodi-demos/main/movie-genre/romance/" rel="tag">Romance</a>
																																																			</span>
																																																		</div>
																																																		<a href="https://demo3.madrasthemes.com/vodi-demos/main/movie/love-simon/" class="masvideos-LoopMovie-link masvideos-loop-movie__link movie__link">
																																																			<h3 class="masvideos-loop-movie__title  movie__title">Love, Simon</h3>
																																																		</a>
																																																	</div> <div class="movie__short-description">
																																																		<div>
																																																			<p>Simon Spier keeps a huge secret from his family, his friends and all of his classmates: he's gay. When that secret is threatened, Simon must face everyone and come to terms with his identity.</p> </div> </div>
																																																			<div class="movie__actions">
																																																				<a href="https://demo3.madrasthemes.com/vodi-demos/main/movie/love-simon/" class="movie-actions--link_watch">Watch Now</a> <div class="movie-actions--link_add-to-playlist dropdown">
																																																					<a class="dropdown-toggle" href="https://demo3.madrasthemes.com/vodi-demos/main/movie/love-simon/" data-toggle="dropdown">+ Playlist</a>
																																																					<div class="dropdown-menu">
																																																						<a class="login-link" href="https://demo3.madrasthemes.com/vodi-demos/main/my-account/movie-playlists/">Sign in to add this movie to a playlist.</a> </div>
																																																					</div>
																																																				</div>
																																																			</div>
																																																			<div class="movie__review-info"> <a href="https://demo3.madrasthemes.com/vodi-demos/main/movie/love-simon/#reviews" class="avg-rating">
																																																				<span class="rating-with-count">
																																																					<svg class="vodi-svg" width="40px" height="39px" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 39">
																																																						<title>play</title>
																																																						<path fill-rule="evenodd" d="M19.633,-0.000 C21.509,0.035 21.530,1.174 22.167,2.414 C23.329,4.679 24.406,7.067 25.572,9.338 C25.853,9.886 26.431,11.640 26.918,11.834 C27.486,12.203 29.345,12.109 30.165,12.316 C32.170,12.825 34.489,12.860 36.500,13.364 C37.516,13.618 38.689,13.413 39.430,13.927 C39.689,14.107 39.770,14.504 39.984,14.732 C40.047,16.499 39.096,16.843 38.163,17.792 C36.473,19.509 34.784,21.227 33.095,22.944 C32.585,23.462 31.092,24.543 31.036,25.359 C31.423,25.951 31.307,27.455 31.511,28.258 C32.138,30.727 32.213,33.522 32.857,35.987 C33.142,37.078 33.016,38.241 32.303,38.724 C31.108,39.533 29.632,38.193 28.819,37.758 C26.695,36.623 24.601,35.624 22.483,34.457 C21.979,34.179 20.607,33.178 20.108,33.088 C19.748,33.023 18.163,34.107 17.812,34.296 C15.557,35.505 13.340,36.640 11.080,37.839 C10.548,38.120 9.180,39.226 8.309,38.966 C6.955,38.558 6.874,36.993 7.280,35.423 C7.716,33.733 7.697,31.880 8.151,30.109 C8.527,28.642 8.907,26.529 9.022,24.957 C8.092,24.344 7.202,23.107 6.408,22.300 C4.760,20.625 3.059,18.990 1.340,17.389 C0.646,16.742 -0.578,15.515 0.311,14.249 C0.915,13.388 2.364,13.656 3.557,13.364 C6.678,12.599 10.114,12.468 13.298,11.834 C14.186,9.747 15.306,7.711 16.307,5.716 C16.954,4.426 17.496,3.163 18.128,1.931 C18.334,1.531 18.358,1.093 18.603,0.724 C18.845,0.362 19.299,0.273 19.633,-0.000 Z">
																																																						</path>
																																																					</svg> <span class="avg-rating-number"> 8.0</span>
																																																				</span>
																																																				<span class="rating-number-with-text">
																																																					<span class="avg-rating-number"> 8.0</span>
																																																					<span class="avg-rating-text">
																																																						<span>1</span> Vote </span>
																																																					</span>
																																																				</a>
																																																				<div class="viewers-count">
																																																				</div>
																																																			</div>
																																																		</div>
																																																	</div>
																																																	<div class="post-316 movie type-movie status-publish has-post-thumbnail hentry movie_genre-action movie_genre-family movie_tag-4k-ultra movie_tag-brother movie_tag-premieres movie_tag-viking">
																																																		<div class="movie__poster">
																																																			<a href="https://demo3.madrasthemes.com/vodi-demos/main/movie/i-can-only-imagine/" class="masvideos-LoopMovie-link masvideos-loop-movie__link movie__link">
																																																				<img width="300" height="450" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/33-a-woman-under-the-influence-300x450.jpg" class="movie__poster--image" alt="" srcset="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/33-a-woman-under-the-influence-300x450.jpg 300w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/33-a-woman-under-the-influence-66x98.jpg 66w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/33-a-woman-under-the-influence-600x900.jpg 600w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/33-a-woman-under-the-influence-150x225.jpg 150w" sizes="(max-width: 300px) 100vw, 300px">
																																																			</a>
																																																		</div>
																																																		<div class="movie__body">
																																																			<div class="movie__info">
																																																				<div class="movie__info--head">
																																																					<div class="movie__meta">
																																																						<span class="movie__meta--release-year">2018</span>
																																																						<span class="movie__meta--genre">
																																																							<a href="https://demo3.madrasthemes.com/vodi-demos/main/movie-genre/action/" rel="tag">Action</a>, <a href="https://demo3.madrasthemes.com/vodi-demos/main/movie-genre/family/" rel="tag">Family</a>
																																																						</span>
																																																					</div>
																																																					<a href="https://demo3.madrasthemes.com/vodi-demos/main/movie/i-can-only-imagine/" class="masvideos-LoopMovie-link masvideos-loop-movie__link movie__link">
																																																						<h3 class="masvideos-loop-movie__title  movie__title">I Can Only Imagine</h3>
																																																					</a>
																																																				</div> <div class="movie__short-description">
																																																					<div>
																																																						<p>The inspiring and unknown true story behind MercyMe's beloved, chart topping song that brings ultimate hope to so many is a gripping reminder of the power of true forgiveness.</p> </div> </div>
																																																						<div class="movie__actions">
																																																							<a href="https://demo3.madrasthemes.com/vodi-demos/main/movie/i-can-only-imagine/" class="movie-actions--link_watch">Watch Now</a> <div class="movie-actions--link_add-to-playlist dropdown">
																																																								<a class="dropdown-toggle" href="https://demo3.madrasthemes.com/vodi-demos/main/movie/i-can-only-imagine/" data-toggle="dropdown">+ Playlist</a>
																																																								<div class="dropdown-menu">
																																																									<a class="login-link" href="https://demo3.madrasthemes.com/vodi-demos/main/my-account/movie-playlists/">Sign in to add this movie to a playlist.</a> </div>
																																																								</div>
																																																							</div>
																																																						</div>
																																																						<div class="movie__review-info"> <a href="https://demo3.madrasthemes.com/vodi-demos/main/movie/i-can-only-imagine/#reviews" class="avg-rating">
																																																							<span class="rating-with-count">
																																																								<svg class="vodi-svg" width="40px" height="39px" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 39">
																																																									<title>play</title>
																																																									<path fill-rule="evenodd" d="M19.633,-0.000 C21.509,0.035 21.530,1.174 22.167,2.414 C23.329,4.679 24.406,7.067 25.572,9.338 C25.853,9.886 26.431,11.640 26.918,11.834 C27.486,12.203 29.345,12.109 30.165,12.316 C32.170,12.825 34.489,12.860 36.500,13.364 C37.516,13.618 38.689,13.413 39.430,13.927 C39.689,14.107 39.770,14.504 39.984,14.732 C40.047,16.499 39.096,16.843 38.163,17.792 C36.473,19.509 34.784,21.227 33.095,22.944 C32.585,23.462 31.092,24.543 31.036,25.359 C31.423,25.951 31.307,27.455 31.511,28.258 C32.138,30.727 32.213,33.522 32.857,35.987 C33.142,37.078 33.016,38.241 32.303,38.724 C31.108,39.533 29.632,38.193 28.819,37.758 C26.695,36.623 24.601,35.624 22.483,34.457 C21.979,34.179 20.607,33.178 20.108,33.088 C19.748,33.023 18.163,34.107 17.812,34.296 C15.557,35.505 13.340,36.640 11.080,37.839 C10.548,38.120 9.180,39.226 8.309,38.966 C6.955,38.558 6.874,36.993 7.280,35.423 C7.716,33.733 7.697,31.880 8.151,30.109 C8.527,28.642 8.907,26.529 9.022,24.957 C8.092,24.344 7.202,23.107 6.408,22.300 C4.760,20.625 3.059,18.990 1.340,17.389 C0.646,16.742 -0.578,15.515 0.311,14.249 C0.915,13.388 2.364,13.656 3.557,13.364 C6.678,12.599 10.114,12.468 13.298,11.834 C14.186,9.747 15.306,7.711 16.307,5.716 C16.954,4.426 17.496,3.163 18.128,1.931 C18.334,1.531 18.358,1.093 18.603,0.724 C18.845,0.362 19.299,0.273 19.633,-0.000 Z">
																																																									</path>
																																																								</svg> <span class="avg-rating-number"> 9.0</span>
																																																							</span>
																																																							<span class="rating-number-with-text">
																																																								<span class="avg-rating-number"> 9.0</span>
																																																								<span class="avg-rating-text">
																																																									<span>1</span> Vote </span>
																																																								</span>
																																																							</a>
																																																							<div class="viewers-count">
																																																							</div>
																																																						</div>
																																																					</div>
																																																				</div>
																																																				<div class="post-710 movie type-movie status-publish has-post-thumbnail hentry movie_genre-action movie_genre-sport movie_tag-4k-ultra movie_tag-brother movie_tag-premieres movie_tag-viking">
																																																					<div class="movie__poster">
																																																						<a href="https://demo3.madrasthemes.com/vodi-demos/main/movie/dirt/" class="masvideos-LoopMovie-link masvideos-loop-movie__link movie__link">
																																																							<img width="300" height="450" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/49-the-sure-thing-300x450.jpg" class="movie__poster--image" alt="" srcset="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/49-the-sure-thing-300x450.jpg 300w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/49-the-sure-thing-66x98.jpg 66w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/49-the-sure-thing-600x900.jpg 600w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/49-the-sure-thing-150x225.jpg 150w" sizes="(max-width: 300px) 100vw, 300px">
																																																						</a>
																																																					</div>
																																																					<div class="movie__body">
																																																						<div class="movie__info">
																																																							<div class="movie__info--head">
																																																								<div class="movie__meta">
																																																									<span class="movie__meta--release-year">2018</span>
																																																									<span class="movie__meta--genre">
																																																										<a href="https://demo3.madrasthemes.com/vodi-demos/main/movie-genre/action/" rel="tag">Action</a>, <a href="https://demo3.madrasthemes.com/vodi-demos/main/movie-genre/sport/" rel="tag">Sport</a>
																																																									</span>
																																																								</div>
																																																								<a href="https://demo3.madrasthemes.com/vodi-demos/main/movie/dirt/" class="masvideos-LoopMovie-link masvideos-loop-movie__link movie__link">
																																																									<h3 class="masvideos-loop-movie__title  movie__title">Dirt</h3>
																																																								</a>
																																																							</div> <div class="movie__short-description">
																																																								<div>
																																																									<p>In search of a lifeline for his struggling off road racing team, a man takes on a young car thief looking for a second chance, but as their worlds collide, they must struggle to forge a successful alliance.</p> </div> </div>
																																																									<div class="movie__actions">
																																																										<a href="https://demo3.madrasthemes.com/vodi-demos/main/movie/dirt/" class="movie-actions--link_watch">Watch Now</a> <div class="movie-actions--link_add-to-playlist dropdown">
																																																											<a class="dropdown-toggle" href="https://demo3.madrasthemes.com/vodi-demos/main/movie/dirt/" data-toggle="dropdown">+ Playlist</a>
																																																											<div class="dropdown-menu">
																																																												<a class="login-link" href="https://demo3.madrasthemes.com/vodi-demos/main/my-account/movie-playlists/">Sign in to add this movie to a playlist.</a> </div>
																																																											</div>
																																																										</div>
																																																									</div>
																																																									<div class="movie__review-info"> <a href="https://demo3.madrasthemes.com/vodi-demos/main/movie/dirt/#reviews" class="avg-rating">
																																																										<span class="rating-with-count">
																																																											<svg class="vodi-svg" width="40px" height="39px" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 39">
																																																												<title>play</title>
																																																												<path fill-rule="evenodd" d="M19.633,-0.000 C21.509,0.035 21.530,1.174 22.167,2.414 C23.329,4.679 24.406,7.067 25.572,9.338 C25.853,9.886 26.431,11.640 26.918,11.834 C27.486,12.203 29.345,12.109 30.165,12.316 C32.170,12.825 34.489,12.860 36.500,13.364 C37.516,13.618 38.689,13.413 39.430,13.927 C39.689,14.107 39.770,14.504 39.984,14.732 C40.047,16.499 39.096,16.843 38.163,17.792 C36.473,19.509 34.784,21.227 33.095,22.944 C32.585,23.462 31.092,24.543 31.036,25.359 C31.423,25.951 31.307,27.455 31.511,28.258 C32.138,30.727 32.213,33.522 32.857,35.987 C33.142,37.078 33.016,38.241 32.303,38.724 C31.108,39.533 29.632,38.193 28.819,37.758 C26.695,36.623 24.601,35.624 22.483,34.457 C21.979,34.179 20.607,33.178 20.108,33.088 C19.748,33.023 18.163,34.107 17.812,34.296 C15.557,35.505 13.340,36.640 11.080,37.839 C10.548,38.120 9.180,39.226 8.309,38.966 C6.955,38.558 6.874,36.993 7.280,35.423 C7.716,33.733 7.697,31.880 8.151,30.109 C8.527,28.642 8.907,26.529 9.022,24.957 C8.092,24.344 7.202,23.107 6.408,22.300 C4.760,20.625 3.059,18.990 1.340,17.389 C0.646,16.742 -0.578,15.515 0.311,14.249 C0.915,13.388 2.364,13.656 3.557,13.364 C6.678,12.599 10.114,12.468 13.298,11.834 C14.186,9.747 15.306,7.711 16.307,5.716 C16.954,4.426 17.496,3.163 18.128,1.931 C18.334,1.531 18.358,1.093 18.603,0.724 C18.845,0.362 19.299,0.273 19.633,-0.000 Z">
																																																												</path>
																																																											</svg> <span class="avg-rating-number"> 8.0</span>
																																																										</span>
																																																										<span class="rating-number-with-text">
																																																											<span class="avg-rating-number"> 8.0</span>
																																																											<span class="avg-rating-text">
																																																												<span>1</span> Vote </span>
																																																											</span>
																																																										</a>
																																																										<div class="viewers-count">
																																																										</div>
																																																									</div>
																																																								</div>
																																																							</div>
																																																							<div class="post-280 movie type-movie status-publish has-post-thumbnail hentry movie_genre-action movie_genre-adventure movie_tag-brother movie_tag-king movie_tag-premieres movie_tag-viking">
																																																								<span class="movie__badge">
																																																									<span class="movie__badge--featured">Featured</span>
																																																								</span>
																																																								<div class="movie__poster">
																																																									<a href="https://demo3.madrasthemes.com/vodi-demos/main/movie/game-night/" class="masvideos-LoopMovie-link masvideos-loop-movie__link movie__link">
																																																										<img width="300" height="450" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/15-300x450.jpg" class="movie__poster--image" alt="" srcset="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/15-300x450.jpg 300w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/15-66x98.jpg 66w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/15-200x300.jpg 200w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/15-600x900.jpg 600w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/15-150x225.jpg 150w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/15.jpg 668w" sizes="(max-width: 300px) 100vw, 300px">
																																																									</a>
																																																								</div>
																																																								<div class="movie__body">
																																																									<div class="movie__info">
																																																										<div class="movie__info--head">
																																																											<div class="movie__meta">
																																																												<span class="movie__meta--release-year">2018</span>
																																																												<span class="movie__meta--genre">
																																																													<a href="https://demo3.madrasthemes.com/vodi-demos/main/movie-genre/action/" rel="tag">Action</a>, <a href="https://demo3.madrasthemes.com/vodi-demos/main/movie-genre/adventure/" rel="tag">Adventure</a>
																																																												</span>
																																																											</div>
																																																											<a href="https://demo3.madrasthemes.com/vodi-demos/main/movie/game-night/" class="masvideos-LoopMovie-link masvideos-loop-movie__link movie__link">
																																																												<h3 class="masvideos-loop-movie__title  movie__title">Game Night</h3>
																																																											</a>
																																																										</div> <div class="movie__short-description">
																																																											<div>
																																																												<p>A group of friends who meet regularly for game nights find themselves entangled in a real-life mystery when the shady brother of one of them is seemingly kidnapped by dangerous gangsters.</p> </div> </div>
																																																												<div class="movie__actions">
																																																													<a href="https://demo3.madrasthemes.com/vodi-demos/main/movie/game-night/" class="movie-actions--link_watch">Watch Now</a> <div class="movie-actions--link_add-to-playlist dropdown">
																																																														<a class="dropdown-toggle" href="https://demo3.madrasthemes.com/vodi-demos/main/movie/game-night/" data-toggle="dropdown">+ Playlist</a>
																																																														<div class="dropdown-menu">
																																																															<a class="login-link" href="https://demo3.madrasthemes.com/vodi-demos/main/my-account/movie-playlists/">Sign in to add this movie to a playlist.</a> </div>
																																																														</div>
																																																													</div>
																																																												</div>
																																																												<div class="movie__review-info"> <a href="https://demo3.madrasthemes.com/vodi-demos/main/movie/game-night/#reviews" class="avg-rating">
																																																													<span class="rating-with-count">
																																																														<svg class="vodi-svg" width="40px" height="39px" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 39">
																																																															<title>play</title>
																																																															<path fill-rule="evenodd" d="M19.633,-0.000 C21.509,0.035 21.530,1.174 22.167,2.414 C23.329,4.679 24.406,7.067 25.572,9.338 C25.853,9.886 26.431,11.640 26.918,11.834 C27.486,12.203 29.345,12.109 30.165,12.316 C32.170,12.825 34.489,12.860 36.500,13.364 C37.516,13.618 38.689,13.413 39.430,13.927 C39.689,14.107 39.770,14.504 39.984,14.732 C40.047,16.499 39.096,16.843 38.163,17.792 C36.473,19.509 34.784,21.227 33.095,22.944 C32.585,23.462 31.092,24.543 31.036,25.359 C31.423,25.951 31.307,27.455 31.511,28.258 C32.138,30.727 32.213,33.522 32.857,35.987 C33.142,37.078 33.016,38.241 32.303,38.724 C31.108,39.533 29.632,38.193 28.819,37.758 C26.695,36.623 24.601,35.624 22.483,34.457 C21.979,34.179 20.607,33.178 20.108,33.088 C19.748,33.023 18.163,34.107 17.812,34.296 C15.557,35.505 13.340,36.640 11.080,37.839 C10.548,38.120 9.180,39.226 8.309,38.966 C6.955,38.558 6.874,36.993 7.280,35.423 C7.716,33.733 7.697,31.880 8.151,30.109 C8.527,28.642 8.907,26.529 9.022,24.957 C8.092,24.344 7.202,23.107 6.408,22.300 C4.760,20.625 3.059,18.990 1.340,17.389 C0.646,16.742 -0.578,15.515 0.311,14.249 C0.915,13.388 2.364,13.656 3.557,13.364 C6.678,12.599 10.114,12.468 13.298,11.834 C14.186,9.747 15.306,7.711 16.307,5.716 C16.954,4.426 17.496,3.163 18.128,1.931 C18.334,1.531 18.358,1.093 18.603,0.724 C18.845,0.362 19.299,0.273 19.633,-0.000 Z">
																																																															</path>
																																																														</svg> <span class="avg-rating-number"> 8.0</span>
																																																													</span>
																																																													<span class="rating-number-with-text">
																																																														<span class="avg-rating-number"> 8.0</span>
																																																														<span class="avg-rating-text">
																																																															<span>1</span> Vote </span>
																																																														</span>
																																																													</a>
																																																													<div class="viewers-count">
																																																													</div>
																																																												</div>
																																																											</div>
																																																										</div>
																																																										<div class="post-334 movie type-movie status-publish has-post-thumbnail hentry movie_genre-action movie_genre-fantacy movie_tag-4k-ultra movie_tag-brother movie_tag-premieres movie_tag-viking">
																																																											<div class="movie__poster">
																																																												<a href="https://demo3.madrasthemes.com/vodi-demos/main/movie/every-day/" class="masvideos-LoopMovie-link masvideos-loop-movie__link movie__link">
																																																													<img width="300" height="450" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/42-nim-project-300x450.jpg" class="movie__poster--image" alt="" srcset="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/42-nim-project-300x450.jpg 300w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/42-nim-project-66x98.jpg 66w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/42-nim-project-600x900.jpg 600w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/42-nim-project-150x225.jpg 150w" sizes="(max-width: 300px) 100vw, 300px">
																																																												</a>
																																																											</div>
																																																											<div class="movie__body">
																																																												<div class="movie__info">
																																																													<div class="movie__info--head">
																																																														<div class="movie__meta">
																																																															<span class="movie__meta--release-year">2018</span>
																																																															<span class="movie__meta--genre">
																																																																<a href="https://demo3.madrasthemes.com/vodi-demos/main/movie-genre/action/" rel="tag">Action</a>, <a href="https://demo3.madrasthemes.com/vodi-demos/main/movie-genre/fantacy/" rel="tag">Fantacy</a>
																																																															</span>
																																																														</div>
																																																														<a href="https://demo3.madrasthemes.com/vodi-demos/main/movie/every-day/" class="masvideos-LoopMovie-link masvideos-loop-movie__link movie__link">
																																																															<h3 class="masvideos-loop-movie__title  movie__title">Every Day</h3>
																																																														</a>
																																																													</div> <div class="movie__short-description">
																																																														<div>
																																																															<p>A shy teenager falls for someone who transforms into another person every day</p> </div> </div>
																																																															<div class="movie__actions">
																																																																<a href="https://demo3.madrasthemes.com/vodi-demos/main/movie/every-day/" class="movie-actions--link_watch">Watch Now</a> <div class="movie-actions--link_add-to-playlist dropdown">
																																																																	<a class="dropdown-toggle" href="https://demo3.madrasthemes.com/vodi-demos/main/movie/every-day/" data-toggle="dropdown">+ Playlist</a>
																																																																	<div class="dropdown-menu">
																																																																		<a class="login-link" href="https://demo3.madrasthemes.com/vodi-demos/main/my-account/movie-playlists/">Sign in to add this movie to a playlist.</a> </div>
																																																																	</div>
																																																																</div>
																																																															</div>
																																																															<div class="movie__review-info"> <a href="https://demo3.madrasthemes.com/vodi-demos/main/movie/every-day/#reviews" class="avg-rating">
																																																																<span class="rating-with-count">
																																																																	<svg class="vodi-svg" width="40px" height="39px" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 39">
																																																																		<title>play</title>
																																																																		<path fill-rule="evenodd" d="M19.633,-0.000 C21.509,0.035 21.530,1.174 22.167,2.414 C23.329,4.679 24.406,7.067 25.572,9.338 C25.853,9.886 26.431,11.640 26.918,11.834 C27.486,12.203 29.345,12.109 30.165,12.316 C32.170,12.825 34.489,12.860 36.500,13.364 C37.516,13.618 38.689,13.413 39.430,13.927 C39.689,14.107 39.770,14.504 39.984,14.732 C40.047,16.499 39.096,16.843 38.163,17.792 C36.473,19.509 34.784,21.227 33.095,22.944 C32.585,23.462 31.092,24.543 31.036,25.359 C31.423,25.951 31.307,27.455 31.511,28.258 C32.138,30.727 32.213,33.522 32.857,35.987 C33.142,37.078 33.016,38.241 32.303,38.724 C31.108,39.533 29.632,38.193 28.819,37.758 C26.695,36.623 24.601,35.624 22.483,34.457 C21.979,34.179 20.607,33.178 20.108,33.088 C19.748,33.023 18.163,34.107 17.812,34.296 C15.557,35.505 13.340,36.640 11.080,37.839 C10.548,38.120 9.180,39.226 8.309,38.966 C6.955,38.558 6.874,36.993 7.280,35.423 C7.716,33.733 7.697,31.880 8.151,30.109 C8.527,28.642 8.907,26.529 9.022,24.957 C8.092,24.344 7.202,23.107 6.408,22.300 C4.760,20.625 3.059,18.990 1.340,17.389 C0.646,16.742 -0.578,15.515 0.311,14.249 C0.915,13.388 2.364,13.656 3.557,13.364 C6.678,12.599 10.114,12.468 13.298,11.834 C14.186,9.747 15.306,7.711 16.307,5.716 C16.954,4.426 17.496,3.163 18.128,1.931 C18.334,1.531 18.358,1.093 18.603,0.724 C18.845,0.362 19.299,0.273 19.633,-0.000 Z">
																																																																		</path>
																																																																	</svg> <span class="avg-rating-number"> 9.0</span>
																																																																</span>
																																																																<span class="rating-number-with-text">
																																																																	<span class="avg-rating-number"> 9.0</span>
																																																																	<span class="avg-rating-text">
																																																																		<span>1</span> Vote </span>
																																																																	</span>
																																																																</a>
																																																																<div class="viewers-count">
																																																																</div>
																																																															</div>
																																																														</div>
																																																													</div>
																																																													<div class="post-278 movie type-movie status-publish has-post-thumbnail hentry movie_genre-action movie_genre-biography movie_tag-4k-ultra movie_tag-brother movie_tag-king movie_tag-viking">
																																																														<div class="movie__poster">
																																																															<a href="https://demo3.madrasthemes.com/vodi-demos/main/movie/the-mercy/" class="masvideos-LoopMovie-link masvideos-loop-movie__link movie__link">
																																																																<img width="300" height="450" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/14-300x450.jpg" class="movie__poster--image" alt="" srcset="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/14-300x450.jpg 300w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/14-66x98.jpg 66w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/14-200x300.jpg 200w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/14-600x900.jpg 600w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/14-150x225.jpg 150w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/14.jpg 668w" sizes="(max-width: 300px) 100vw, 300px">
																																																															</a>
																																																														</div>
																																																														<div class="movie__body">
																																																															<div class="movie__info">
																																																																<div class="movie__info--head">
																																																																	<div class="movie__meta">
																																																																		<span class="movie__meta--release-year">2018</span>
																																																																		<span class="movie__meta--genre">
																																																																			<a href="https://demo3.madrasthemes.com/vodi-demos/main/movie-genre/action/" rel="tag">Action</a>, <a href="https://demo3.madrasthemes.com/vodi-demos/main/movie-genre/biography/" rel="tag">Biography</a>
																																																																		</span>
																																																																	</div>
																																																																	<a href="https://demo3.madrasthemes.com/vodi-demos/main/movie/the-mercy/" class="masvideos-LoopMovie-link masvideos-loop-movie__link movie__link">
																																																																		<h3 class="masvideos-loop-movie__title  movie__title">The Mercy</h3>
																																																																	</a>
																																																																</div> <div class="movie__short-description">
																																																																	<div>
																																																																		<p>The incredible story of amateur sailor Donald Crowhurst and his solo attempt to circumnavigate the globe. The struggles he confronted on the journey while his family awaited his return is one of the most enduring mysteries of recent times.</p> </div> </div>
																																																																		<div class="movie__actions">
																																																																			<a href="https://demo3.madrasthemes.com/vodi-demos/main/movie/the-mercy/" class="movie-actions--link_watch">Watch Now</a> <div class="movie-actions--link_add-to-playlist dropdown">
																																																																				<a class="dropdown-toggle" href="https://demo3.madrasthemes.com/vodi-demos/main/movie/the-mercy/" data-toggle="dropdown">+ Playlist</a>
																																																																				<div class="dropdown-menu">
																																																																					<a class="login-link" href="https://demo3.madrasthemes.com/vodi-demos/main/my-account/movie-playlists/">Sign in to add this movie to a playlist.</a> </div>
																																																																				</div>
																																																																			</div>
																																																																		</div>
																																																																		<div class="movie__review-info"> <a href="https://demo3.madrasthemes.com/vodi-demos/main/movie/the-mercy/#reviews" class="avg-rating">
																																																																			<span class="rating-with-count">
																																																																				<svg class="vodi-svg" width="40px" height="39px" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 39">
																																																																					<title>play</title>
																																																																					<path fill-rule="evenodd" d="M19.633,-0.000 C21.509,0.035 21.530,1.174 22.167,2.414 C23.329,4.679 24.406,7.067 25.572,9.338 C25.853,9.886 26.431,11.640 26.918,11.834 C27.486,12.203 29.345,12.109 30.165,12.316 C32.170,12.825 34.489,12.860 36.500,13.364 C37.516,13.618 38.689,13.413 39.430,13.927 C39.689,14.107 39.770,14.504 39.984,14.732 C40.047,16.499 39.096,16.843 38.163,17.792 C36.473,19.509 34.784,21.227 33.095,22.944 C32.585,23.462 31.092,24.543 31.036,25.359 C31.423,25.951 31.307,27.455 31.511,28.258 C32.138,30.727 32.213,33.522 32.857,35.987 C33.142,37.078 33.016,38.241 32.303,38.724 C31.108,39.533 29.632,38.193 28.819,37.758 C26.695,36.623 24.601,35.624 22.483,34.457 C21.979,34.179 20.607,33.178 20.108,33.088 C19.748,33.023 18.163,34.107 17.812,34.296 C15.557,35.505 13.340,36.640 11.080,37.839 C10.548,38.120 9.180,39.226 8.309,38.966 C6.955,38.558 6.874,36.993 7.280,35.423 C7.716,33.733 7.697,31.880 8.151,30.109 C8.527,28.642 8.907,26.529 9.022,24.957 C8.092,24.344 7.202,23.107 6.408,22.300 C4.760,20.625 3.059,18.990 1.340,17.389 C0.646,16.742 -0.578,15.515 0.311,14.249 C0.915,13.388 2.364,13.656 3.557,13.364 C6.678,12.599 10.114,12.468 13.298,11.834 C14.186,9.747 15.306,7.711 16.307,5.716 C16.954,4.426 17.496,3.163 18.128,1.931 C18.334,1.531 18.358,1.093 18.603,0.724 C18.845,0.362 19.299,0.273 19.633,-0.000 Z">
																																																																					</path>
																																																																				</svg> <span class="avg-rating-number"> 8.0</span>
																																																																			</span>
																																																																			<span class="rating-number-with-text">
																																																																				<span class="avg-rating-number"> 8.0</span>
																																																																				<span class="avg-rating-text">
																																																																					<span>1</span> Vote </span>
																																																																				</span>
																																																																			</a>
																																																																			<div class="viewers-count">
																																																																			</div>
																																																																		</div>
																																																																	</div>
																																																																</div>
																																																																<div class="post-302 movie type-movie status-publish has-post-thumbnail hentry movie_genre-action movie_genre-animation movie_tag-4k-ultra movie_tag-brother movie_tag-king movie_tag-premieres">
																																																																	<div class="movie__poster">
																																																																		<a href="https://demo3.madrasthemes.com/vodi-demos/main/movie/have-a-nice-day/" class="masvideos-LoopMovie-link masvideos-loop-movie__link movie__link">
																																																																			<img width="300" height="450" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/26-300x450.jpg" class="movie__poster--image" alt="" srcset="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/26-300x450.jpg 300w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/26-66x98.jpg 66w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/26-200x300.jpg 200w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/26-600x900.jpg 600w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/26-150x225.jpg 150w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/26.jpg 668w" sizes="(max-width: 300px) 100vw, 300px">
																																																																		</a>
																																																																	</div>
																																																																	<div class="movie__body">
																																																																		<div class="movie__info">
																																																																			<div class="movie__info--head">
																																																																				<div class="movie__meta">
																																																																					<span class="movie__meta--release-year">2018</span>
																																																																					<span class="movie__meta--genre">
																																																																						<a href="https://demo3.madrasthemes.com/vodi-demos/main/movie-genre/action/" rel="tag">Action</a>, <a href="https://demo3.madrasthemes.com/vodi-demos/main/movie-genre/animation/" rel="tag">Animation</a>
																																																																					</span>
																																																																				</div>
																																																																				<a href="https://demo3.madrasthemes.com/vodi-demos/main/movie/have-a-nice-day/" class="masvideos-LoopMovie-link masvideos-loop-movie__link movie__link">
																																																																					<h3 class="masvideos-loop-movie__title  movie__title">Have a Nice Day</h3>
																																																																				</a>
																																																																			</div> <div class="movie__short-description">
																																																																				<div>
																																																																					<p>A city in southern China and a bag containing a million yuan draw several people from diverse backgrounds with different personal motives into a bloody conflict</p> </div> </div>
																																																																					<div class="movie__actions">
																																																																						<a href="https://demo3.madrasthemes.com/vodi-demos/main/movie/have-a-nice-day/" class="movie-actions--link_watch">Watch Now</a> <div class="movie-actions--link_add-to-playlist dropdown">
																																																																							<a class="dropdown-toggle" href="https://demo3.madrasthemes.com/vodi-demos/main/movie/have-a-nice-day/" data-toggle="dropdown">+ Playlist</a>
																																																																							<div class="dropdown-menu">
																																																																								<a class="login-link" href="https://demo3.madrasthemes.com/vodi-demos/main/my-account/movie-playlists/">Sign in to add this movie to a playlist.</a> </div>
																																																																							</div>
																																																																						</div>
																																																																					</div>
																																																																					<div class="movie__review-info"> <a href="https://demo3.madrasthemes.com/vodi-demos/main/movie/have-a-nice-day/#reviews" class="avg-rating">
																																																																						<span class="rating-with-count">
																																																																							<svg class="vodi-svg" width="40px" height="39px" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 39">
																																																																								<title>play</title>
																																																																								<path fill-rule="evenodd" d="M19.633,-0.000 C21.509,0.035 21.530,1.174 22.167,2.414 C23.329,4.679 24.406,7.067 25.572,9.338 C25.853,9.886 26.431,11.640 26.918,11.834 C27.486,12.203 29.345,12.109 30.165,12.316 C32.170,12.825 34.489,12.860 36.500,13.364 C37.516,13.618 38.689,13.413 39.430,13.927 C39.689,14.107 39.770,14.504 39.984,14.732 C40.047,16.499 39.096,16.843 38.163,17.792 C36.473,19.509 34.784,21.227 33.095,22.944 C32.585,23.462 31.092,24.543 31.036,25.359 C31.423,25.951 31.307,27.455 31.511,28.258 C32.138,30.727 32.213,33.522 32.857,35.987 C33.142,37.078 33.016,38.241 32.303,38.724 C31.108,39.533 29.632,38.193 28.819,37.758 C26.695,36.623 24.601,35.624 22.483,34.457 C21.979,34.179 20.607,33.178 20.108,33.088 C19.748,33.023 18.163,34.107 17.812,34.296 C15.557,35.505 13.340,36.640 11.080,37.839 C10.548,38.120 9.180,39.226 8.309,38.966 C6.955,38.558 6.874,36.993 7.280,35.423 C7.716,33.733 7.697,31.880 8.151,30.109 C8.527,28.642 8.907,26.529 9.022,24.957 C8.092,24.344 7.202,23.107 6.408,22.300 C4.760,20.625 3.059,18.990 1.340,17.389 C0.646,16.742 -0.578,15.515 0.311,14.249 C0.915,13.388 2.364,13.656 3.557,13.364 C6.678,12.599 10.114,12.468 13.298,11.834 C14.186,9.747 15.306,7.711 16.307,5.716 C16.954,4.426 17.496,3.163 18.128,1.931 C18.334,1.531 18.358,1.093 18.603,0.724 C18.845,0.362 19.299,0.273 19.633,-0.000 Z">
																																																																								</path>
																																																																							</svg> <span class="avg-rating-number"> 8.0</span>
																																																																						</span>
																																																																						<span class="rating-number-with-text">
																																																																							<span class="avg-rating-number"> 8.0</span>
																																																																							<span class="avg-rating-text">
																																																																								<span>1</span> Vote </span>
																																																																							</span>
																																																																						</a>
																																																																						<div class="viewers-count">
																																																																						</div>
																																																																					</div>
																																																																				</div>
																																																																			</div>
																																																																			<div class="post-272 movie type-movie status-publish has-post-thumbnail hentry movie_genre-action movie_genre-drama movie_tag-brother movie_tag-king movie_tag-premieres movie_tag-viking">
																																																																				<span class="movie__badge">
																																																																					<span class="movie__badge--featured">Featured</span>
																																																																				</span>
																																																																				<div class="movie__poster">
																																																																					<a href="https://demo3.madrasthemes.com/vodi-demos/main/movie/phantom-thread/" class="masvideos-LoopMovie-link masvideos-loop-movie__link movie__link">
																																																																						<img width="300" height="450" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/11-300x450.jpg" class="movie__poster--image" alt="" srcset="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/11-300x450.jpg 300w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/11-66x98.jpg 66w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/11-200x300.jpg 200w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/11-600x900.jpg 600w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/11-150x225.jpg 150w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/11.jpg 668w" sizes="(max-width: 300px) 100vw, 300px">
																																																																					</a>
																																																																				</div>
																																																																				<div class="movie__body">
																																																																					<div class="movie__info">
																																																																						<div class="movie__info--head">
																																																																							<div class="movie__meta">
																																																																								<span class="movie__meta--release-year">2018</span>
																																																																								<span class="movie__meta--genre">
																																																																									<a href="https://demo3.madrasthemes.com/vodi-demos/main/movie-genre/action/" rel="tag">Action</a>, <a href="https://demo3.madrasthemes.com/vodi-demos/main/movie-genre/drama/" rel="tag">Drama</a>
																																																																								</span>
																																																																							</div>
																																																																							<a href="https://demo3.madrasthemes.com/vodi-demos/main/movie/phantom-thread/" class="masvideos-LoopMovie-link masvideos-loop-movie__link movie__link">
																																																																								<h3 class="masvideos-loop-movie__title  movie__title">Phantom Thread</h3>
																																																																							</a>
																																																																						</div> <div class="movie__short-description">
																																																																							<div>
																																																																								<p>Set in 1950s London, Reynolds Woodcock is a renowned dressmaker whose fastidious life is disrupted by a young, strong-willed woman, Alma, who becomes his muse and lover.</p> </div> </div>
																																																																								<div class="movie__actions">
																																																																									<a href="https://demo3.madrasthemes.com/vodi-demos/main/movie/phantom-thread/" class="movie-actions--link_watch">Watch Now</a> <div class="movie-actions--link_add-to-playlist dropdown">
																																																																										<a class="dropdown-toggle" href="https://demo3.madrasthemes.com/vodi-demos/main/movie/phantom-thread/" data-toggle="dropdown">+ Playlist</a>
																																																																										<div class="dropdown-menu">
																																																																											<a class="login-link" href="https://demo3.madrasthemes.com/vodi-demos/main/my-account/movie-playlists/">Sign in to add this movie to a playlist.</a> </div>
																																																																										</div>
																																																																									</div>
																																																																								</div>
																																																																								<div class="movie__review-info"> <a href="https://demo3.madrasthemes.com/vodi-demos/main/movie/phantom-thread/#reviews" class="avg-rating">
																																																																									<span class="rating-with-count">
																																																																										<svg class="vodi-svg" width="40px" height="39px" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 39">
																																																																											<title>play</title>
																																																																											<path fill-rule="evenodd" d="M19.633,-0.000 C21.509,0.035 21.530,1.174 22.167,2.414 C23.329,4.679 24.406,7.067 25.572,9.338 C25.853,9.886 26.431,11.640 26.918,11.834 C27.486,12.203 29.345,12.109 30.165,12.316 C32.170,12.825 34.489,12.860 36.500,13.364 C37.516,13.618 38.689,13.413 39.430,13.927 C39.689,14.107 39.770,14.504 39.984,14.732 C40.047,16.499 39.096,16.843 38.163,17.792 C36.473,19.509 34.784,21.227 33.095,22.944 C32.585,23.462 31.092,24.543 31.036,25.359 C31.423,25.951 31.307,27.455 31.511,28.258 C32.138,30.727 32.213,33.522 32.857,35.987 C33.142,37.078 33.016,38.241 32.303,38.724 C31.108,39.533 29.632,38.193 28.819,37.758 C26.695,36.623 24.601,35.624 22.483,34.457 C21.979,34.179 20.607,33.178 20.108,33.088 C19.748,33.023 18.163,34.107 17.812,34.296 C15.557,35.505 13.340,36.640 11.080,37.839 C10.548,38.120 9.180,39.226 8.309,38.966 C6.955,38.558 6.874,36.993 7.280,35.423 C7.716,33.733 7.697,31.880 8.151,30.109 C8.527,28.642 8.907,26.529 9.022,24.957 C8.092,24.344 7.202,23.107 6.408,22.300 C4.760,20.625 3.059,18.990 1.340,17.389 C0.646,16.742 -0.578,15.515 0.311,14.249 C0.915,13.388 2.364,13.656 3.557,13.364 C6.678,12.599 10.114,12.468 13.298,11.834 C14.186,9.747 15.306,7.711 16.307,5.716 C16.954,4.426 17.496,3.163 18.128,1.931 C18.334,1.531 18.358,1.093 18.603,0.724 C18.845,0.362 19.299,0.273 19.633,-0.000 Z">
																																																																											</path>
																																																																										</svg> <span class="avg-rating-number"> 8.0</span>
																																																																									</span>
																																																																									<span class="rating-number-with-text">
																																																																										<span class="avg-rating-number"> 8.0</span>
																																																																										<span class="avg-rating-text">
																																																																											<span>1</span> Vote </span>
																																																																										</span>
																																																																									</a>
																																																																									<div class="viewers-count">
																																																																									</div>
																																																																								</div>
																																																																							</div>
																																																																						</div>
																																																																						<div class="post-720 movie type-movie status-publish has-post-thumbnail hentry movie_genre-action movie_genre-adventure movie_tag-brother movie_tag-king movie_tag-premieres movie_tag-viking">
																																																																							<div class="movie__poster">
																																																																								<a href="https://demo3.madrasthemes.com/vodi-demos/main/movie/euphoria/" class="masvideos-LoopMovie-link masvideos-loop-movie__link movie__link">
																																																																									<img width="300" height="450" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/23-300x450.jpg" class="movie__poster--image" alt="" srcset="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/23-300x450.jpg 300w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/23-66x98.jpg 66w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/23-200x300.jpg 200w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/23-600x900.jpg 600w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/23-150x225.jpg 150w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/23.jpg 668w" sizes="(max-width: 300px) 100vw, 300px">
																																																																								</a>
																																																																							</div>
																																																																							<div class="movie__body">
																																																																								<div class="movie__info">
																																																																									<div class="movie__info--head">
																																																																										<div class="movie__meta">
																																																																											<span class="movie__meta--release-year">2018</span>
																																																																											<span class="movie__meta--genre">
																																																																												<a href="https://demo3.madrasthemes.com/vodi-demos/main/movie-genre/action/" rel="tag">Action</a>, <a href="https://demo3.madrasthemes.com/vodi-demos/main/movie-genre/adventure/" rel="tag">Adventure</a>
																																																																											</span>
																																																																										</div>
																																																																										<a href="https://demo3.madrasthemes.com/vodi-demos/main/movie/euphoria/" class="masvideos-LoopMovie-link masvideos-loop-movie__link movie__link">
																																																																											<h3 class="masvideos-loop-movie__title  movie__title">Euphoria</h3>
																																																																										</a>
																																																																									</div> <div class="movie__short-description">
																																																																										<div>
																																																																											<p>Sisters in conflict travelling through Europe toward a mystery destination</p> </div> </div>
																																																																											<div class="movie__actions">
																																																																												<a href="https://demo3.madrasthemes.com/vodi-demos/main/movie/euphoria/" class="movie-actions--link_watch">Watch Now</a> <div class="movie-actions--link_add-to-playlist dropdown">
																																																																													<a class="dropdown-toggle" href="https://demo3.madrasthemes.com/vodi-demos/main/movie/euphoria/" data-toggle="dropdown">+ Playlist</a>
																																																																													<div class="dropdown-menu">
																																																																														<a class="login-link" href="https://demo3.madrasthemes.com/vodi-demos/main/my-account/movie-playlists/">Sign in to add this movie to a playlist.</a> </div>
																																																																													</div>
																																																																												</div>
																																																																											</div>
																																																																											<div class="movie__review-info"> <a href="https://demo3.madrasthemes.com/vodi-demos/main/movie/euphoria/#reviews" class="avg-rating">
																																																																												<span class="rating-with-count">
																																																																													<svg class="vodi-svg" width="40px" height="39px" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 39">
																																																																														<title>play</title>
																																																																														<path fill-rule="evenodd" d="M19.633,-0.000 C21.509,0.035 21.530,1.174 22.167,2.414 C23.329,4.679 24.406,7.067 25.572,9.338 C25.853,9.886 26.431,11.640 26.918,11.834 C27.486,12.203 29.345,12.109 30.165,12.316 C32.170,12.825 34.489,12.860 36.500,13.364 C37.516,13.618 38.689,13.413 39.430,13.927 C39.689,14.107 39.770,14.504 39.984,14.732 C40.047,16.499 39.096,16.843 38.163,17.792 C36.473,19.509 34.784,21.227 33.095,22.944 C32.585,23.462 31.092,24.543 31.036,25.359 C31.423,25.951 31.307,27.455 31.511,28.258 C32.138,30.727 32.213,33.522 32.857,35.987 C33.142,37.078 33.016,38.241 32.303,38.724 C31.108,39.533 29.632,38.193 28.819,37.758 C26.695,36.623 24.601,35.624 22.483,34.457 C21.979,34.179 20.607,33.178 20.108,33.088 C19.748,33.023 18.163,34.107 17.812,34.296 C15.557,35.505 13.340,36.640 11.080,37.839 C10.548,38.120 9.180,39.226 8.309,38.966 C6.955,38.558 6.874,36.993 7.280,35.423 C7.716,33.733 7.697,31.880 8.151,30.109 C8.527,28.642 8.907,26.529 9.022,24.957 C8.092,24.344 7.202,23.107 6.408,22.300 C4.760,20.625 3.059,18.990 1.340,17.389 C0.646,16.742 -0.578,15.515 0.311,14.249 C0.915,13.388 2.364,13.656 3.557,13.364 C6.678,12.599 10.114,12.468 13.298,11.834 C14.186,9.747 15.306,7.711 16.307,5.716 C16.954,4.426 17.496,3.163 18.128,1.931 C18.334,1.531 18.358,1.093 18.603,0.724 C18.845,0.362 19.299,0.273 19.633,-0.000 Z">
																																																																														</path>
																																																																													</svg> <span class="avg-rating-number"> 9.0</span>
																																																																												</span>
																																																																												<span class="rating-number-with-text">
																																																																													<span class="avg-rating-number"> 9.0</span>
																																																																													<span class="avg-rating-text">
																																																																														<span>1</span> Vote </span>
																																																																													</span>
																																																																												</a>
																																																																												<div class="viewers-count">
																																																																												</div>
																																																																											</div>
																																																																										</div>
																																																																									</div>
																																																																									<div class="post-286 movie type-movie status-publish has-post-thumbnail hentry movie_genre-action movie_genre-sci-fi movie_tag-4k-ultra movie_tag-brother movie_tag-king movie_tag-viking">
																																																																										<div class="movie__poster">
																																																																											<a href="https://demo3.madrasthemes.com/vodi-demos/main/movie/downsizing/" class="masvideos-LoopMovie-link masvideos-loop-movie__link movie__link">
																																																																												<img width="300" height="450" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/18-300x450.jpg" class="movie__poster--image" alt="" srcset="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/18-300x450.jpg 300w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/18-66x98.jpg 66w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/18-200x300.jpg 200w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/18-600x900.jpg 600w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/18-150x225.jpg 150w, https://demo3.madrasthemes.com/vodi-demos/main/wp-content/uploads/sites/2/2019/04/18.jpg 668w" sizes="(max-width: 300px) 100vw, 300px">
																																																																											</a>
																																																																										</div>
																																																																										<div class="movie__body">
																																																																											<div class="movie__info">
																																																																												<div class="movie__info--head">
																																																																													<div class="movie__meta">
																																																																														<span class="movie__meta--release-year">2018</span>
																																																																														<span class="movie__meta--genre">
																																																																															<a href="https://demo3.madrasthemes.com/vodi-demos/main/movie-genre/action/" rel="tag">Action</a>, <a href="https://demo3.madrasthemes.com/vodi-demos/main/movie-genre/sci-fi/" rel="tag">Sci-Fi</a>
																																																																														</span>
																																																																													</div>
																																																																													<a href="https://demo3.madrasthemes.com/vodi-demos/main/movie/downsizing/" class="masvideos-LoopMovie-link masvideos-loop-movie__link movie__link">
																																																																														<h3 class="masvideos-loop-movie__title  movie__title">Downsizing</h3>
																																																																													</a>
																																																																												</div> <div class="movie__short-description">
																																																																													<div>
																																																																														<p>A social satire in which a man realizes he would have a better life if he were to shrink himself to five inches tall, allowing him to live in wealth and splendor.</p> </div> </div>
																																																																														<div class="movie__actions">
																																																																															<a href="https://demo3.madrasthemes.com/vodi-demos/main/movie/downsizing/" class="movie-actions--link_watch">Watch Now</a> <div class="movie-actions--link_add-to-playlist dropdown">
																																																																																<a class="dropdown-toggle" href="https://demo3.madrasthemes.com/vodi-demos/main/movie/downsizing/" data-toggle="dropdown">+ Playlist</a>
																																																																																<div class="dropdown-menu">
																																																																																	<a class="login-link" href="https://demo3.madrasthemes.com/vodi-demos/main/my-account/movie-playlists/">Sign in to add this movie to a playlist.</a> </div>
																																																																																</div>
																																																																															</div>
																																																																														</div>
																																																																														<div class="movie__review-info"> <a href="https://demo3.madrasthemes.com/vodi-demos/main/movie/downsizing/#reviews" class="avg-rating">
																																																																															<span class="rating-with-count">
																																																																																<svg class="vodi-svg" width="40px" height="39px" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 39">
																																																																																	<title>play</title>
																																																																																	<path fill-rule="evenodd" d="M19.633,-0.000 C21.509,0.035 21.530,1.174 22.167,2.414 C23.329,4.679 24.406,7.067 25.572,9.338 C25.853,9.886 26.431,11.640 26.918,11.834 C27.486,12.203 29.345,12.109 30.165,12.316 C32.170,12.825 34.489,12.860 36.500,13.364 C37.516,13.618 38.689,13.413 39.430,13.927 C39.689,14.107 39.770,14.504 39.984,14.732 C40.047,16.499 39.096,16.843 38.163,17.792 C36.473,19.509 34.784,21.227 33.095,22.944 C32.585,23.462 31.092,24.543 31.036,25.359 C31.423,25.951 31.307,27.455 31.511,28.258 C32.138,30.727 32.213,33.522 32.857,35.987 C33.142,37.078 33.016,38.241 32.303,38.724 C31.108,39.533 29.632,38.193 28.819,37.758 C26.695,36.623 24.601,35.624 22.483,34.457 C21.979,34.179 20.607,33.178 20.108,33.088 C19.748,33.023 18.163,34.107 17.812,34.296 C15.557,35.505 13.340,36.640 11.080,37.839 C10.548,38.120 9.180,39.226 8.309,38.966 C6.955,38.558 6.874,36.993 7.280,35.423 C7.716,33.733 7.697,31.880 8.151,30.109 C8.527,28.642 8.907,26.529 9.022,24.957 C8.092,24.344 7.202,23.107 6.408,22.300 C4.760,20.625 3.059,18.990 1.340,17.389 C0.646,16.742 -0.578,15.515 0.311,14.249 C0.915,13.388 2.364,13.656 3.557,13.364 C6.678,12.599 10.114,12.468 13.298,11.834 C14.186,9.747 15.306,7.711 16.307,5.716 C16.954,4.426 17.496,3.163 18.128,1.931 C18.334,1.531 18.358,1.093 18.603,0.724 C18.845,0.362 19.299,0.273 19.633,-0.000 Z">
																																																																																	</path>
																																																																																</svg> <span class="avg-rating-number"> 8.0</span>
																																																																															</span>
																																																																															<span class="rating-number-with-text">
																																																																																<span class="avg-rating-number"> 8.0</span>
																																																																																<span class="avg-rating-text">
																																																																																	<span>1</span> Vote </span>
																																																																																</span>
																																																																															</a>
																																																																															<div class="viewers-count">
																																																																															</div>
																																																																														</div>
																																																																													</div>
																																																																												</div>
																																																																											</div>
																																																																										</div>
																																																																									</div>
																																																																									<div class="page-control-bar-bottom"> <p class="masvideos-result-count masvideos-movies-result-count">
																																																																									Showing 1–24 of 55 results </p>
																																																																									<nav class="masvideos-pagination masvideos-movies-pagination">
																																																																										<ul class="page-numbers">
																																																																											<li>
																																																																												<span aria-current="page" class="page-numbers current">1</span>
																																																																											</li>
																																																																											<li>
																																																																												<a class="page-numbers" href="https://demo3.madrasthemes.compage/2/">2</a>
																																																																											</li>
																																																																											<li>
																																																																												<a class="page-numbers" href="https://demo3.madrasthemes.compage/3/">3</a>
																																																																											</li>
																																																																											<li>
																																																																												<a class="next page-numbers" href="https://demo3.madrasthemes.compage/2/">Next Page &nbsp;&nbsp;&nbsp;→</a>
																																																																											</li>
																																																																										</ul>
																																																																									</nav>
																																																																								</div>
																																																																							</div>
																																																																							<div id="secondary" class="widget-area sidebar-area movie-sidebar" role="">
																																																																								<div class="widget-area-inner">
																																																																									<div class="widget widget_vodi_movies_filter">
																																																																										<div id="masvideos_movies_filter_widget-2" class="widget masvideos widget_layered_nav masvideos-movies-filter-widget">
																																																																											<div class="widget-header">
																																																																												<span class="widget-title">Categories</span>
																																																																											</div>
																																																																											<ul class="masvideos-widget-movies-layered-nav-list">
																																																																												<li class="masvideos-widget-movies-layered-nav-list__item masvideos-layered-nav-term ">
																																																																													<a rel="nofollow" href="https://demo3.madrasthemes.com?filter_genre=action&amp;query_type_genre=or">Action</a>
																																																																												</li>
																																																																												<li class="masvideos-widget-movies-layered-nav-list__item masvideos-layered-nav-term ">
																																																																													<a rel="nofollow" href="https://demo3.madrasthemes.com?filter_genre=adventure&amp;query_type_genre=or">Adventure</a>
																																																																												</li>
																																																																												<li class="masvideos-widget-movies-layered-nav-list__item masvideos-layered-nav-term ">
																																																																													<a rel="nofollow" href="https://demo3.madrasthemes.com?filter_genre=animation&amp;query_type_genre=or">Animation</a>
																																																																												</li>
																																																																												<li class="masvideos-widget-movies-layered-nav-list__item masvideos-layered-nav-term ">
																																																																													<a rel="nofollow" href="https://demo3.madrasthemes.com?filter_genre=annimation&amp;query_type_genre=or">Annimation</a>
																																																																												</li>
																																																																												<li class="masvideos-widget-movies-layered-nav-list__item masvideos-layered-nav-term ">
																																																																													<a rel="nofollow" href="https://demo3.madrasthemes.com?filter_genre=biography&amp;query_type_genre=or">Biography</a>
																																																																												</li>
																																																																												<li class="masvideos-widget-movies-layered-nav-list__item masvideos-layered-nav-term ">
																																																																													<a rel="nofollow" href="https://demo3.madrasthemes.com?filter_genre=comedy&amp;query_type_genre=or">Comedy</a>
																																																																												</li>
																																																																												<li class="masvideos-widget-movies-layered-nav-list__item masvideos-layered-nav-term ">
																																																																													<a rel="nofollow" href="https://demo3.madrasthemes.com?filter_genre=crime&amp;query_type_genre=or">Crime</a>
																																																																												</li>
																																																																												<li class="masvideos-widget-movies-layered-nav-list__item masvideos-layered-nav-term ">
																																																																													<a rel="nofollow" href="https://demo3.madrasthemes.com?filter_genre=documentary&amp;query_type_genre=or">Documentary</a>
																																																																												</li>
																																																																												<li class="masvideos-widget-movies-layered-nav-list__item masvideos-layered-nav-term ">
																																																																													<a rel="nofollow" href="https://demo3.madrasthemes.com?filter_genre=drama&amp;query_type_genre=or">Drama</a>
																																																																												</li>
																																																																												<li class="masvideos-widget-movies-layered-nav-list__item masvideos-layered-nav-term ">
																																																																													<a rel="nofollow" href="https://demo3.madrasthemes.com?filter_genre=family&amp;query_type_genre=or">Family</a>
																																																																												</li>
																																																																												<li class="masvideos-widget-movies-layered-nav-list__item masvideos-layered-nav-term ">
																																																																													<a rel="nofollow" href="https://demo3.madrasthemes.com?filter_genre=fantacy&amp;query_type_genre=or">Fantacy</a>
																																																																												</li>
																																																																												<li class="masvideos-widget-movies-layered-nav-list__item masvideos-layered-nav-term ">
																																																																													<a rel="nofollow" href="https://demo3.madrasthemes.com?filter_genre=history&amp;query_type_genre=or">History</a>
																																																																												</li>
																																																																												<li class="masvideos-widget-movies-layered-nav-list__item masvideos-layered-nav-term ">
																																																																													<a rel="nofollow" href="https://demo3.madrasthemes.com?filter_genre=horror&amp;query_type_genre=or">Horror</a>
																																																																												</li>
																																																																												<li class="masvideos-widget-movies-layered-nav-list__item masvideos-layered-nav-term ">
																																																																													<a rel="nofollow" href="https://demo3.madrasthemes.com?filter_genre=music&amp;query_type_genre=or">Music</a>
																																																																												</li>
																																																																												<li class="masvideos-widget-movies-layered-nav-list__item masvideos-layered-nav-term ">
																																																																													<a rel="nofollow" href="https://demo3.madrasthemes.com?filter_genre=mystery&amp;query_type_genre=or">Mystery</a>
																																																																												</li>
																																																																												<li class="masvideos-widget-movies-layered-nav-list__item masvideos-layered-nav-term ">
																																																																													<a rel="nofollow" href="https://demo3.madrasthemes.com?filter_genre=mystrey&amp;query_type_genre=or">Mystrey</a>
																																																																												</li>
																																																																												<li class="masvideos-widget-movies-layered-nav-list__item masvideos-layered-nav-term ">
																																																																													<a rel="nofollow" href="https://demo3.madrasthemes.com?filter_genre=romance&amp;query_type_genre=or">Romance</a>
																																																																												</li>
																																																																												<li class="masvideos-widget-movies-layered-nav-list__item masvideos-layered-nav-term ">
																																																																													<a rel="nofollow" href="https://demo3.madrasthemes.com?filter_genre=sci-fi&amp;query_type_genre=or">Sci-Fi</a>
																																																																												</li>
																																																																												<li class="masvideos-widget-movies-layered-nav-list__item masvideos-layered-nav-term ">
																																																																													<a rel="nofollow" href="https://demo3.madrasthemes.com?filter_genre=sport&amp;query_type_genre=or">Sport</a>
																																																																												</li>
																																																																												<li class="masvideos-widget-movies-layered-nav-list__item masvideos-layered-nav-term ">
																																																																													<a rel="nofollow" href="https://demo3.madrasthemes.com?filter_genre=thiller&amp;query_type_genre=or">Thiller</a>
																																																																												</li>
																																																																												<li class="masvideos-widget-movies-layered-nav-list__item masvideos-layered-nav-term ">
																																																																													<a rel="nofollow" href="https://demo3.madrasthemes.com?filter_genre=thriller&amp;query_type_genre=or">Thriller</a>
																																																																												</li>
																																																																											</ul>
																																																																										</div>
																																																																										<div id="masvideos_movies_tags_filter_widget-2" class="widget masvideos widget_layered_nav masvideos-movies-tags-filter-widget">
																																																																											<ul class="masvideos-widget-movies-layered-nav-list">
																																																																												<li class="masvideos-widget-movies-layered-nav-list__item masvideos-layered-nav-term ">
																																																																													<i class="far fa-window-maximize">
																																																																													</i>
																																																																													<a rel="nofollow" href="https://demo3.madrasthemes.com?filter_tag=4k-ultra&amp;query_type_tag=or">4K Ultra</a>
																																																																												</li>
																																																																												<li class="masvideos-widget-movies-layered-nav-list__item masvideos-layered-nav-term ">
																																																																													<i class="fas fa-chess-knight">
																																																																													</i>
																																																																													<a rel="nofollow" href="https://demo3.madrasthemes.com?filter_tag=brother&amp;query_type_tag=or">Brother</a>
																																																																												</li>
																																																																												<li class="masvideos-widget-movies-layered-nav-list__item masvideos-layered-nav-term ">
																																																																													<i class="fab fa-teamspeak">
																																																																													</i>
																																																																													<a rel="nofollow" href="https://demo3.madrasthemes.com?filter_tag=dubbing&amp;query_type_tag=or">Dubbing</a>
																																																																												</li>
																																																																												<li class="masvideos-widget-movies-layered-nav-list__item masvideos-layered-nav-term ">
																																																																													<i class="fas fa-chess-knight">
																																																																													</i>
																																																																													<a rel="nofollow" href="https://demo3.madrasthemes.com?filter_tag=hero&amp;query_type_tag=or">Hero</a>
																																																																												</li>
																																																																											</ul>
																																																																										</div>
																																																																										<div id="masvideos_movies_year_filter-2" class="widget masvideos masvideos-widget_movies_year_filter">
																																																																											<div class="widget-header">
																																																																												<span class="widget-title">Movies by Year</span>
																																																																											</div>
																																																																											<ul>
																																																																												<li class="masvideos-layered-nav-movies-year">
																																																																													<a href="?year_filter=2021">
																																																																														<span>2021</span> </a>
																																																																													</li>
																																																																													<li class="masvideos-layered-nav-movies-year">
																																																																														<a href="?year_filter=2020">
																																																																															<span>2020</span> </a>
																																																																														</li>
																																																																														<li class="masvideos-layered-nav-movies-year">
																																																																															<a href="?year_filter=2019">
																																																																																<span>2019</span> </a>
																																																																															</li>
																																																																															<li class="masvideos-layered-nav-movies-year">
																																																																																<a href="?year_filter=2018">
																																																																																	<span>2018</span> </a>
																																																																																</li>
																																																																																<li class="masvideos-layered-nav-movies-year">
																																																																																	<a href="?year_filter=2017">
																																																																																		<span>2017</span> </a>
																																																																																	</li>
																																																																																	<li class="masvideos-layered-nav-movies-year">
																																																																																		<a href="?year_filter=2016">
																																																																																			<span>2016</span> </a>
																																																																																		</li>
																																																																																		<li class="masvideos-layered-nav-movies-year">
																																																																																			<a href="?year_filter=2015">
																																																																																				<span>2015</span> </a>
																																																																																			</li>
																																																																																			<li class="masvideos-layered-nav-movies-year">
																																																																																				<a href="?year_filter=2014">
																																																																																					<span>2014</span> </a>
																																																																																				</li>
																																																																																				<li class="masvideos-layered-nav-movies-year">
																																																																																					<a href="?year_filter=2013">
																																																																																						<span>2013</span> </a>
																																																																																					</li>
																																																																																					<li class="masvideos-layered-nav-movies-year">
																																																																																						<a href="?year_filter=2012">
																																																																																							<span>2012</span> </a>
																																																																																						</li>
																																																																																						<li class="masvideos-layered-nav-movies-year">
																																																																																							<a href="?year_filter=2011">
																																																																																								<span>2011</span> </a>
																																																																																							</li>
																																																																																							<li class="masvideos-layered-nav-movies-year">
																																																																																								<a href="?year_filter=2010">
																																																																																									<span>2010</span> </a>
																																																																																								</li>
																																																																																							</ul>
																																																																																						</div>
																																																																																						<div id="masvideos_movies_rating_filter-2" class="widget masvideos movies_widget_rating_filter">
																																																																																							<div class="widget-header">
																																																																																								<span class="widget-title">Filter by Rating</span>
																																																																																							</div>
																																																																																							<ul>
																																																																																								<li class="masvideos-layered-nav-rating">
																																																																																									<a href="?rating_filter=10">
																																																																																										<div class="star-rating">
																																																																																											<div class="star-rating">
																																																																																												<span class="screen-reader-text">10.0 rating</span>
																																																																																												<div class="star star-full" aria-hidden="true">
																																																																																												</div>
																																																																																												<div class="star star-full" aria-hidden="true">
																																																																																												</div>
																																																																																												<div class="star star-full" aria-hidden="true">
																																																																																												</div>
																																																																																												<div class="star star-full" aria-hidden="true">
																																																																																												</div>
																																																																																												<div class="star star-full" aria-hidden="true">
																																																																																												</div>
																																																																																												<div class="star star-full" aria-hidden="true">
																																																																																												</div>
																																																																																												<div class="star star-full" aria-hidden="true">
																																																																																												</div>
																																																																																												<div class="star star-full" aria-hidden="true">
																																																																																												</div>
																																																																																												<div class="star star-full" aria-hidden="true">
																																																																																												</div>
																																																																																												<div class="star star-full" aria-hidden="true">
																																																																																												</div>
																																																																																											</div>
																																																																																										</div> (2)</a>
																																																																																									</li>
																																																																																									<li class="masvideos-layered-nav-rating">
																																																																																										<a href="?rating_filter=9">
																																																																																											<div class="star-rating">
																																																																																												<div class="star-rating">
																																																																																													<span class="screen-reader-text">9.0 rating</span>
																																																																																													<div class="star star-full" aria-hidden="true">
																																																																																													</div>
																																																																																													<div class="star star-full" aria-hidden="true">
																																																																																													</div>
																																																																																													<div class="star star-full" aria-hidden="true">
																																																																																													</div>
																																																																																													<div class="star star-full" aria-hidden="true">
																																																																																													</div>
																																																																																													<div class="star star-full" aria-hidden="true">
																																																																																													</div>
																																																																																													<div class="star star-full" aria-hidden="true">
																																																																																													</div>
																																																																																													<div class="star star-full" aria-hidden="true">
																																																																																													</div>
																																																																																													<div class="star star-full" aria-hidden="true">
																																																																																													</div>
																																																																																													<div class="star star-full" aria-hidden="true">
																																																																																													</div>
																																																																																													<div class="star star-empty" aria-hidden="true">
																																																																																													</div>
																																																																																												</div>
																																																																																											</div> (10)</a>
																																																																																										</li>
																																																																																										<li class="masvideos-layered-nav-rating">
																																																																																											<a href="?rating_filter=8">
																																																																																												<div class="star-rating">
																																																																																													<div class="star-rating">
																																																																																														<span class="screen-reader-text">8.0 rating</span>
																																																																																														<div class="star star-full" aria-hidden="true">
																																																																																														</div>
																																																																																														<div class="star star-full" aria-hidden="true">
																																																																																														</div>
																																																																																														<div class="star star-full" aria-hidden="true">
																																																																																														</div>
																																																																																														<div class="star star-full" aria-hidden="true">
																																																																																														</div>
																																																																																														<div class="star star-full" aria-hidden="true">
																																																																																														</div>
																																																																																														<div class="star star-full" aria-hidden="true">
																																																																																														</div>
																																																																																														<div class="star star-full" aria-hidden="true">
																																																																																														</div>
																																																																																														<div class="star star-full" aria-hidden="true">
																																																																																														</div>
																																																																																														<div class="star star-empty" aria-hidden="true">
																																																																																														</div>
																																																																																														<div class="star star-empty" aria-hidden="true">
																																																																																														</div>
																																																																																													</div>
																																																																																												</div> (27)</a>
																																																																																											</li>
																																																																																											<li class="masvideos-layered-nav-rating">
																																																																																												<a href="?rating_filter=7">
																																																																																													<div class="star-rating">
																																																																																														<div class="star-rating">
																																																																																															<span class="screen-reader-text">7.0 rating</span>
																																																																																															<div class="star star-full" aria-hidden="true">
																																																																																															</div>
																																																																																															<div class="star star-full" aria-hidden="true">
																																																																																															</div>
																																																																																															<div class="star star-full" aria-hidden="true">
																																																																																															</div>
																																																																																															<div class="star star-full" aria-hidden="true">
																																																																																															</div>
																																																																																															<div class="star star-full" aria-hidden="true">
																																																																																															</div>
																																																																																															<div class="star star-full" aria-hidden="true">
																																																																																															</div>
																																																																																															<div class="star star-full" aria-hidden="true">
																																																																																															</div>
																																																																																															<div class="star star-empty" aria-hidden="true">
																																																																																															</div>
																																																																																															<div class="star star-empty" aria-hidden="true">
																																																																																															</div>
																																																																																															<div class="star star-empty" aria-hidden="true">
																																																																																															</div>
																																																																																														</div>
																																																																																													</div> (13)</a>
																																																																																												</li>
																																																																																												<li class="masvideos-layered-nav-rating">
																																																																																													<a href="?rating_filter=5">
																																																																																														<div class="star-rating">
																																																																																															<div class="star-rating">
																																																																																																<span class="screen-reader-text">5.0 rating</span>
																																																																																																<div class="star star-full" aria-hidden="true">
																																																																																																</div>
																																																																																																<div class="star star-full" aria-hidden="true">
																																																																																																</div>
																																																																																																<div class="star star-full" aria-hidden="true">
																																																																																																</div>
																																																																																																<div class="star star-full" aria-hidden="true">
																																																																																																</div>
																																																																																																<div class="star star-full" aria-hidden="true">
																																																																																																</div>
																																																																																																<div class="star star-empty" aria-hidden="true">
																																																																																																</div>
																																																																																																<div class="star star-empty" aria-hidden="true">
																																																																																																</div>
																																																																																																<div class="star star-empty" aria-hidden="true">
																																																																																																</div>
																																																																																																<div class="star star-empty" aria-hidden="true">
																																																																																																</div>
																																																																																																<div class="star star-empty" aria-hidden="true">
																																																																																																</div>
																																																																																															</div>
																																																																																														</div> (1)</a>
																																																																																													</li>
																																																																																												</ul>
																																																																																											</div> </div>
																																																																																										</div>
																																																																																										</div>
																																																																																									</div>
																																																																																								</div>
																																																																																							</div>
																																																																																							<footer id="colophon" class="site-footer site__footer--v1 desktop-footer dark" role="contentinfo">
																																																																																								<div class="container">
																																																																																									<div class="footer-top-bar">
																																																																																										<div class="site-footer__logo footer-logo">
																																																																																											<a href="/" rel="home"> 
																																																																																												<img style="max-width: 16%" src="https://khojapp.com/asset/images/logo1.png">
																																																																																											</a>
																																																																																										</div>
																																																																																										<div class="footer-social-icons social-label">
																																																																																											<ul id="menu-footer-social-menu" class="social-icons">
																																																																																												<li class="menu-item menu-item-type-custom menu-item-object-custom social-media-item social-media-item-5164">
																																																																																													<a title="
																																																																																													" href="#" class="footer-social-icon">
																																																																																													<span class="fa-stack">
																																																																																														<i class="fas fa-circle fa-stack-2x">
																																																																																														</i>
																																																																																														<i class="fab fa-facebook-f social-media-item__icon fa-stack-1x fa-inverse" aria-hidden="true">
																																																																																														</i> </span>
																																																																																														<span class="social-media-item__title">Facebook</span>
																																																																																													</a>
																																																																																												</li>
																																																																																												<li class="menu-item menu-item-type-custom menu-item-object-custom social-media-item social-media-item-5165">
																																																																																													<a title="
																																																																																													" href="#" class="footer-social-icon">
																																																																																													<span class="fa-stack">
																																																																																														<i class="fas fa-circle fa-stack-2x">
																																																																																														</i>
																																																																																														<i class="fab fa-twitter social-media-item__icon fa-stack-1x fa-inverse" aria-hidden="true">
																																																																																														</i> </span>
																																																																																														<span class="social-media-item__title">Twitter</span>
																																																																																													</a>
																																																																																												</li>
																																																																																												<li class="menu-item menu-item-type-custom menu-item-object-custom social-media-item social-media-item-5166">
																																																																																													<a title="
																																																																																													" href="#" class="footer-social-icon">
																																																																																													<span class="fa-stack">
																																																																																														<i class="fas fa-circle fa-stack-2x">
																																																																																														</i>
																																																																																														<i class="fab fa-google-plus-g social-media-item__icon fa-stack-1x fa-inverse" aria-hidden="true">
																																																																																														</i> </span>
																																																																																														<span class="social-media-item__title">Google+</span>
																																																																																													</a>
																																																																																												</li>
																																																																																												<li class="menu-item menu-item-type-custom menu-item-object-custom social-media-item social-media-item-5167">
																																																																																													<a title="
																																																																																													" href="#" class="footer-social-icon">
																																																																																													<span class="fa-stack">
																																																																																														<i class="fas fa-circle fa-stack-2x">
																																																																																														</i>
																																																																																														<i class="fas fa-globe social-media-item__icon fa-stack-1x fa-inverse" aria-hidden="true">
																																																																																														</i> </span>
																																																																																														<span class="social-media-item__title">Vimeo</span>
																																																																																													</a>
																																																																																												</li>
																																																																																												<li class="menu-item menu-item-type-custom menu-item-object-custom social-media-item social-media-item-5168">
																																																																																													<a title="
																																																																																													" href="#" class="footer-social-icon">
																																																																																													<span class="fa-stack">
																																																																																														<i class="fas fa-circle fa-stack-2x">
																																																																																														</i>
																																																																																														<i class="fas fa-rss social-media-item__icon fa-stack-1x fa-inverse" aria-hidden="true">
																																																																																														</i> </span>
																																																																																														<span class="social-media-item__title">RSS</span>
																																																																																													</a>
																																																																																												</li>
																																																																																											</ul>
																																																																																										</div>
																																																																																									</div>
																																																																																									<div class="footer-widgets">
																																																																																										<div class="footer-widgets-inner row-1 col-3 fix"> 
																																																																																											<div class="block footer-widget-1">
																																																																																												<div id="nav_menu-1" class="widget widget_nav_menu">
																																																																																													<div class="widget-header">
																																																																																														<span class="widget-title">Movie Categories</span>
																																																																																													</div>
																																																																																													<div class="menu-footer-movie-genres-container">
																																																																																														<ul id="menu-footer-movie-genres" class="menu">
																																																																																															<li id="menu-item-5169" class="menu-item menu-item-type-taxonomy menu-item-object-movie_genre menu-item-5169">
																																																																																																<a title="" href="/react2/movies/action/">Action</a>
																																																																																															</li>
																																																																																															<li id="menu-item-5170" class="menu-item menu-item-type-taxonomy menu-item-object-movie_genre menu-item-5170">
																																																																																																<a title="" href="/react2/movies/adventure/">Adventure</a>
																																																																																															</li>
																																																																																															<li id="menu-item-5171" class="menu-item menu-item-type-taxonomy menu-item-object-movie_genre menu-item-5171">
																																																																																																<a title="" href="/react2/movies/animation/">Animation</a>
																																																																																															</li>
																																																																																															<li id="menu-item-5172" class="menu-item menu-item-type-taxonomy menu-item-object-movie_genre menu-item-5172">
																																																																																																<a title="" href="/react2/movies/comedy/">Comedy</a>
																																																																																															</li>
																																																																																															<li id="menu-item-5173" class="menu-item menu-item-type-taxonomy menu-item-object-movie_genre menu-item-5173">
																																																																																																<a title="" href="/react2/movies/crime/">Crime</a>
																																																																																															</li>
																																																																																															<li id="menu-item-5174" class="menu-item menu-item-type-taxonomy menu-item-object-movie_genre menu-item-5174">
																																																																																																<a title="" href="/react2/movies/drama/">Drama</a>
																																																																																															</li>
																																																																																															<li id="menu-item-5175" class="menu-item menu-item-type-taxonomy menu-item-object-movie_genre menu-item-5175">
																																																																																																<a title="" href="/react2/movies/fantacy/">Fantacy</a>
																																																																																															</li>
																																																																																															<li id="menu-item-5176" class="menu-item menu-item-type-taxonomy menu-item-object-movie_genre menu-item-5176">
																																																																																																<a title="" href="/react2/movies/horror/">Horror</a>
																																																																																															</li>
																																																																																															<li id="menu-item-5177" class="menu-item menu-item-type-taxonomy menu-item-object-movie_genre menu-item-5177">
																																																																																																<a title="" href="/react2/movies/mystrey/">Mystrey</a>
																																																																																															</li>
																																																																																															<li id="menu-item-5178" class="menu-item menu-item-type-taxonomy menu-item-object-movie_genre menu-item-5178">
																																																																																																<a title="" href="/react2/movies/romance/">Romance</a>
																																																																																															</li>
																																																																																														</ul>
																																																																																													</div>
																																																																																												</div>
																																																																																											</div> 
																																																																																											<div class="block footer-widget-2">
																																																																																												<div id="nav_menu-2" class="widget widget_nav_menu">
																																																																																													<div class="widget-header">
																																																																																														<span class="widget-title">TV Series</span>
																																																																																													</div>
																																																																																													<div class="menu-footer-tv-show-genres-container">
																																																																																														<ul id="menu-footer-tv-show-genres" class="menu">
																																																																																															<li id="menu-item-5179" class="menu-item menu-item-type-taxonomy menu-item-object-tv_show_genre menu-item-5179">
																																																																																																<a title="" href="/tv-show-genre/action/">Valentine Day</a>
																																																																																															</li>
																																																																																															<li id="menu-item-5180" class="menu-item menu-item-type-taxonomy menu-item-object-tv_show_genre menu-item-5180">
																																																																																																<a title="" href="/tv-show-genre/adventure/">Underrated Comedies</a>
																																																																																															</li>
																																																																																															<li id="menu-item-5181" class="menu-item menu-item-type-taxonomy menu-item-object-tv_show_genre menu-item-5181">
																																																																																																<a title="" href="/tv-show-genre/animation/">Scary TV Series</a>
																																																																																															</li>
																																																																																															<li id="menu-item-5183" class="menu-item menu-item-type-taxonomy menu-item-object-tv_show_genre menu-item-5183">
																																																																																																<a title="" href="/tv-show-genre/comedy/">Best 2018 Documentaries</a>
																																																																																															</li>
																																																																																															<li id="menu-item-5182" class="menu-item menu-item-type-taxonomy menu-item-object-tv_show_genre menu-item-5182">
																																																																																																<a title="" href="/tv-show-genre/biography/">Classic Shows</a>
																																																																																															</li>
																																																																																															<li id="menu-item-5184" class="menu-item menu-item-type-taxonomy menu-item-object-tv_show_genre menu-item-5184">
																																																																																																<a title="" href="/tv-show-genre/crime/">Big TV Premieres</a>
																																																																																															</li>
																																																																																															<li id="menu-item-5185" class="menu-item menu-item-type-taxonomy menu-item-object-tv_show_genre menu-item-5185">
																																																																																																<a title="" href="/tv-show-genre/crime-drama/">Reality TV Shows</a>
																																																																																															</li>
																																																																																															<li id="menu-item-5186" class="menu-item menu-item-type-taxonomy menu-item-object-tv_show_genre menu-item-5186">
																																																																																																<a title="" href="/tv-show-genre/drama/">Original Shows</a>
																																																																																															</li>
																																																																																															<li id="menu-item-5187" class="menu-item menu-item-type-taxonomy menu-item-object-tv_show_genre menu-item-5187">
																																																																																																<a title="" href="/tv-show-genre/fantacy/">Suprise of the Year Shows</a>
																																																																																															</li>
																																																																																														</ul>
																																																																																													</div>
																																																																																												</div>
																																																																																											</div> 
																																																																																											<div class="block footer-widget-3">
																																																																																												<div id="nav_menu-3" class="widget widget_nav_menu">
																																																																																													<div class="widget-header">
																																																																																														<span class="widget-title">Support</span>
																																																																																													</div>
																																																																																													<div class="menu-footer-support-links-container">
																																																																																														<ul id="menu-footer-support-links" class="menu">
																																																																																															<li id="menu-item-5220" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5220">
																																																																																																<a title="" href="/react2/blogs/">My Account</a>
																																																																																															</li>
																																																																																															<li id="menu-item-5221" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-343 current_page_item menu-item-5221">
																																																																																																<a title="" href="/" aria-current="page">FAQ</a>
																																																																																															</li>
																																																										{{-- <li id="menu-item-5222" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5222">
<a title="" href="/react2/home-v2/">Watch on TV</a>
</li> --}}
<li id="menu-item-5223" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5223">
	<a title="" href="/faq/">Help Center</a>
</li>
<li id="menu-item-5224" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5224">
	<a title="" href="/contact">Contact</a>
</li>
</ul>
</div>
</div>
</div> </div>
</div>
</div> 
<div class="footer-bottom-bar">
	<div class="container">
		<div class="footer-bottom-bar-inner"> 
			<div class="site-footer__info site-info">
			Copyright &copy; 2020, Khoj. All Rights Reserved </div>
			<ul id="menu-footer-quick-links" class="footer-quick-links nav">
				<li id="menu-item-5251" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5251">
					<a title="
					" href="/privacy-policy/">Privacy Policy</a>
				</li>
			</ul>
		</div>
	</div>
</div>
</footer>
<footer class="site-footer handheld-footer dark">
	<div class="container"> 
		<div class="site-footer__info site-info">
		Copyright &copy; 2020, Khoj. All Rights Reserved </div>
	</div> </footer>
</div>
<div class="modal-register-login-wrapper">
	<div class="modal fade modal-register-login" id="modal-register-login" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body">
					<div class="masvideos">
						<div class="masvideos-notices-wrapper">
						</div>
						<div class="masvideos-register-login">
							<div class="masvideos-register">
								<div class="masvideos-register__inner">
									<h2>Register</h2>
									<form method="post" class="masvideos-form masvideos-form-register register">
										<p class="masvideos-form-row masvideos-form-row--wide form-row form-row-wide">
											<label for="reg_email">Email address&nbsp;<span class="required">*</span>
											</label>
											<input type="email" class="masvideos-Input masvideos-Input--text input-text" name="email" id="reg_email" autocomplete="email" value=""> </p>
											<p class="masvideos-FormRow form-row">
												<input type="hidden" id="masvideos-register-nonce" name="masvideos-register-nonce" value="1dff4a2169">
												<input type="hidden" name="_wp_http_referer" value=""> <button type="submit" class="masvideos-Button button" name="register" value="Register">Register</button>
											</p>
										</form>
									</div>
								</div>
								<div class="masvideos-login">
									<div class="masvideos-login__inner">
										<h2>Login</h2>
										<form class="masvideos-form masvideos-form-login login" method="post">
											<p class="masvideos-form-row masvideos-form-row--wide form-row form-row-wide">
												<label for="username">Username or email address&nbsp;<span class="required">*</span>
												</label>
												<input type="text" class="masvideos-Input masvideos-Input--text input-text" name="username" id="username" autocomplete="username" value=""> </p>
												<p class="masvideos-form-row masvideos-form-row--wide form-row form-row-wide">
													<label for="password">Password&nbsp;<span class="required">*</span>
													</label>
													<input class="masvideos-Input masvideos-Input--text input-text" type="password" name="password" id="password" autocomplete="current-password">
												</p>
												<p class="form-row">
													<input type="hidden" id="masvideos-login-nonce" name="masvideos-login-nonce" value="415d7bbc0f">
													<input type="hidden" name="_wp_http_referer" value=""> <button type="submit" class="masvideos-Button button" name="login" value="Log in">Log in</button>
													<label class="masvideos-form__label masvideos-form__label-for-checkbox inline">
														<input class="masvideos-form__input masvideos-form__input-checkbox" name="rememberme" type="checkbox" id="rememberme" value="forever"> <span>Remember me</span>
													</label>
												</p>
												<p class="masvideos-LostPassword lost_password">
													<a href="https://demo3.madrasthemes.com/vodi-demos/wp-login.php?action=lostpassword">Lost your password?</a>
												</p>
											</form>
										</div>
									</div>
								</div>
							</div> <a class="close-button" data-dismiss="modal" aria-label="Close">
								<i class="la la-close">
								</i>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">
			<div class="pswp__bg">
			</div>
			<div class="pswp__scroll-wrap">
				<div class="pswp__container">
					<div class="pswp__item">
					</div>
					<div class="pswp__item">
					</div>
					<div class="pswp__item">
					</div>
				</div>
				<div class="pswp__ui pswp__ui--hidden">
					<div class="pswp__top-bar">
						<div class="pswp__counter">
						</div>
						<button class="pswp__button pswp__button--close" aria-label="Close (Esc)">
						</button>
						<button class="pswp__button pswp__button--share" aria-label="Share">
						</button>
						<button class="pswp__button pswp__button--fs" aria-label="Toggle fullscreen">
						</button>
						<button class="pswp__button pswp__button--zoom" aria-label="Zoom in/out">
						</button>
						<div class="pswp__preloader">
							<div class="pswp__preloader__icn">
								<div class="pswp__preloader__cut">
									<div class="pswp__preloader__donut">
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
						<div class="pswp__share-tooltip">
						</div>
					</div>
					<button class="pswp__button pswp__button--arrow--left" aria-label="Previous (arrow left)">
					</button>
					<button class="pswp__button pswp__button--arrow--right" aria-label="Next (arrow right)">
					</button>
					<div class="pswp__caption">
						<div class="pswp__caption__center">
						</div>
					</div>
				</div>
			</div>
		</div>
		<script type="text/javascript">
			var c = document.body.className;
			c = c.replace(/masvideos-no-js/, 'masvideos-js');
			document.body.className = c;
		</script>
		<script type="text/javascript">
			/* <![CDATA[ */
			var wpcf7 = {"apiSettings":{"root":"https:\/\/demo3.madrasthemes.com\/vodi-demos\/main\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"}};
			/* ]]> */
		</script>
		<script type="text/javascript" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=5.1.7">
		</script>
		<script type="text/javascript">
			/* <![CDATA[ */
			var masvideos_playlist_tv_show_params = {"ajax_url":"\/vodi-demos\/main\/wp-admin\/admin-ajax.php","masvideos_ajax_url":"\/vodi-demos\/main\/?masvideos-ajax=%%endpoint%%"};
			/* ]]> */
		</script>
		<script type="text/javascript" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/plugins/masvideos/assets/js/frontend/playlist-tv-show.min.js?ver=1.1.9">
		</script>
		<script type="text/javascript">
			/* <![CDATA[ */
			var masvideos_playlist_video_params = {"ajax_url":"\/vodi-demos\/main\/wp-admin\/admin-ajax.php","masvideos_ajax_url":"\/vodi-demos\/main\/?masvideos-ajax=%%endpoint%%"};
			/* ]]> */
		</script>
		<script type="text/javascript" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/plugins/masvideos/assets/js/frontend/playlist-video.min.js?ver=1.1.9">
		</script>
		<script type="text/javascript">
			/* <![CDATA[ */
			var masvideos_playlist_movie_params = {"ajax_url":"\/vodi-demos\/main\/wp-admin\/admin-ajax.php","masvideos_ajax_url":"\/vodi-demos\/main\/?masvideos-ajax=%%endpoint%%"};
			/* ]]> */
		</script>
		<script type="text/javascript" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/plugins/masvideos/assets/js/frontend/playlist-movie.min.js?ver=1.1.9">
		</script>
		<script type="text/javascript" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/plugins/masvideos/assets/js/frontend/gallery-flip.min.js?ver=1.1.9">
		</script>
		<script type="text/javascript" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/plugins/masvideos/assets/js/photoswipe/photoswipe.min.js?ver=4.1.1">
		</script>
		<script type="text/javascript" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/plugins/masvideos/assets/js/photoswipe/photoswipe-ui-default.min.js?ver=4.1.1">
		</script>
		<script type="text/javascript">
			/* <![CDATA[ */
			var wp_ulike_params = {"ajax_url":"https:\/\/demo3.madrasthemes.com\/vodi-demos\/main\/wp-admin\/admin-ajax.php","notifications":"1"};
			/* ]]> */
		</script>
		<script type="text/javascript" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/plugins/wp-ulike/assets/js/wp-ulike.min.js?ver=4.2.2">
		</script>
		<script type="text/javascript" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/themes/vodi/assets/js/bootstrap.bundle.min.js?ver=1.2.0">
		</script>
		<script type="text/javascript" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/themes/vodi/assets/js/jquery.waypoints.min.js?ver=1.2.0">
		</script>
		<script type="text/javascript" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/themes/vodi/assets/js/jquery.easing.min.js?ver=1.2.0">
		</script>
		<script type="text/javascript" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/themes/vodi/assets/js/scrollup.min.js?ver=1.2.0">
		</script>
		<script type="text/javascript" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/themes/vodi/assets/js/jquery.fancybox.min.js?ver=1.2.0">
		</script>
		<script type="text/javascript" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/themes/vodi/assets/js/readmore.min.js?ver=3.0.0">
		</script>
		<script type="text/javascript" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/themes/vodi/assets/js/simplebar.min.js?ver=1.2.0">
		</script>
		<script type="text/javascript" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/themes/vodi/assets/js/slick.min.js?ver=1.2.0">
		</script>
		<script type="text/javascript" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/themes/vodi/assets/js/vodi-slick.min.js?ver=1.2.0">
		</script>
		<script type="text/javascript">
			/* <![CDATA[ */
			var vodi_options = {"rtl":"0","enable_sticky_header":"","enable_hh_sticky_header":"","ajax_url":"https:\/\/demo3.madrasthemes.com\/vodi-demos\/main\/wp-admin\/admin-ajax.php","deal_countdown_text":{"days_text":"Days","hours_text":"Hours","mins_text":"Mins","secs_text":"Secs"},"wp_local_time_offset":"5.5","enable_vodi_readmore":"1","vodi_readmore_data":[{"selectors":".single-episode-v1 .episode__description > div, .single-episode-v2 .episode__description > div, .single-episode-v2 .episode__description > div, .single-episode-v4 .episode__short-description > p","options":{"moreLink":"<a class=\"maxlist-more\" href=\"#\">Read More<\/a>","lessLink":"<a class=\"maxlist-less\" href=\"#\">Show Less<\/a>","collapsedHeight":47,"speed":600}},{"selectors":".single-movie-v1 .movie__description > div, .single-movie-v2 .movie__description > div, .single-movie-v3 .movie__description > div, .single-movie-v4 .movie__info--head .movie__short-description > p","options":{"moreLink":"<a class=\"maxlist-more\" href=\"#\">Read More<\/a>","lessLink":"<a class=\"maxlist-less\" href=\"#\">Show Less<\/a>","collapsedHeight":47,"speed":600}},{"selectors":".single-video .single-video__description > div","options":{"moreLink":"<a class=\"maxlist-more\" href=\"#\">Show more<\/a>","lessLink":"<a class=\"maxlist-less\" href=\"#\">Show less<\/a>","collapsedHeight":0,"speed":600}}],"vodi_fancybox_options":{"speedIn":300,"speedOut":300}};
			/* ]]> */
		</script>
		<script type="text/javascript" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-content/themes/vodi/assets/js/vodi.min.js?ver=1.2.0">
		</script>
		<script type="text/javascript" src="https://demo3.madrasthemes.com/vodi-demos/main/wp-includes/js/wp-embed.min.js?ver=5.4.4">
		</script>
		<script type="text/javascript" src="https://stats.wp.com/e-202107.js" async="async" defer="defer">
		</script>
		<script type="text/javascript">
			_stq = window._stq || [];
			_stq.push([ 'view', {v:'ext',j:'1:8.5',blog:'162040229',post:'0',tz:'5.5',srv:'demo3.madrasthemes.com'} ]);
			_stq.push([ 'clickTrackerInit', '162040229', '0' ]);
		</script>

		<a id="scrollUp" href="#top" style="position: fixed; z-index: 1001; display: none;">
			<i class="fas fa-angle-up">
			</i>
		</a>
		<img src="https://pixel.wp.com/g.gif?v=ext&amp;j=1%3A8.5&amp;blog=162040229&amp;post=0&amp;tz=5.5&amp;srv=demo3.madrasthemes.com&amp;host=demo3.madrasthemes.com&amp;ref=https%3A%2F%2Fdemo3.madrasthemes.com%2Fvodi-demos%2Fmain%2F&amp;fcp=1838&amp;rand=0.4661225022727489" alt=":)" width="6" height="5" id="wpstats">
	</body>
	</html>