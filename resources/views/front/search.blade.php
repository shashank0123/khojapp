@extends('layouts.new_front')
<style>
	#filter label{ color: #fff; }
	#filter #type, #filter #genre, #filter #country { border-radius: 20px }
	.filter-price {
		width: 220px;
		border: 0;
		padding: 0;
		margin: 0; 
	}

	.price-title {
		position: relative;
		color: #fff;
		font-size: 14px;
		line-height: 1.2em;
		font-weight: 400;
		background: #d58e32;
		padding:10px;
	}

	.price-container {
		display: flex;
		border: 1px solid #ccc;
		padding: 5px;
		margin-left: 57px;
		width:100px;
	}

	.price-field {
		position: relative;
		width: 100%;
		height: 36px;
		box-sizing: border-box;
		padding-top: 15px;
		padding-left: 0px;
	}

	.price-field input[type=range] {
		position: absolute;
	}

	/* Reset style for input range */

	.price-field input[type=range] {
		width: 100%;
		height: 7px;
		border: 1px solid #000;
		outline: 0;
		box-sizing: border-box;
		border-radius: 5px;
		pointer-events: none;
		-webkit-appearance: none;
	}

	.price-field input[type=range]::-webkit-slider-thumb {
		-webkit-appearance: none;
	}

	.price-field input[type=range]:active,
	.price-field input[type=range]:focus {
		outline: 0;
	}

	.price-field input[type=range]::-ms-track {
		width: 188px;
		height: 2px; 
		border: 0;
		outline: 0;
		box-sizing: border-box;
		border-radius: 5px;
		pointer-events: none;
		background: transparent;
		border-color: transparent;
		color: red;
		border-radius: 5px;
	}

	/* Style toddler input range */

	.price-field input[type=range]::-webkit-slider-thumb { 
		/* WebKit/Blink */
		position: relative;
		-webkit-appearance: none;
		margin: 0;
		border: 0;
		outline: 0;
		border-radius: 50%;
		height: 10px;
		width: 10px;
		margin-top: -4px;
		background-color: #fff;
		cursor: pointer;
		cursor: pointer;
		pointer-events: all;
		z-index: 100;
	}

	.price-field input[type=range]::-moz-range-thumb { 
		/* Firefox */
		position: relative;
		appearance: none;
		margin: 0;
		border: 0;
		outline: 0;
		border-radius: 50%;
		height: 10px;
		width: 10px;
		margin-top: -5px;
		background-color: #fff;
		cursor: pointer;
		cursor: pointer;
		pointer-events: all;
		z-index: 100;
	}

	.price-field input[type=range]::-ms-thumb  { 
		/* IE */
		position: relative;
		appearance: none;
		margin: 0;
		border: 0;
		outline: 0;
		border-radius: 50%;
		height: 10px;
		width: 10px;
		margin-top: -5px;
		background-color: #242424;
		cursor: pointer;
		cursor: pointer;
		pointer-events: all;
		z-index: 100;
	}

	/* Style track input range */

	.price-field input[type=range]::-webkit-slider-runnable-track { 
		/* WebKit/Blink */
		width: 188px;
		height: 2px;
		cursor: pointer;
		background: #555;
		border-radius: 5px;
	}

	.price-field input[type=range]::-moz-range-track { 
		/* Firefox */
		width: 188px;
		height: 2px;
		cursor: pointer;
		background: #242424;
		border-radius: 5px;
	}

	.price-field input[type=range]::-ms-track { 
		/* IE */
		width: 188px;
		height: 2px;
		cursor: pointer;
		background: #242424;
		border-radius: 5px;
	}

	/* Style for input value block */

	.price-wrap {
		display: flex;
		color: #242424;
		font-size: 14px;
		line-height: 1.2em;
		font-weight: 400;
		margin-bottom: 0px;
	}

	.price-wrap-1, 
	.price-wrap-2 {
		display: flex;
		margin-left: 0px;
	}

	.price-title {
		margin-right: 5px;
	}

	.price-wrap_line {
		margin: 6px 0px 5px 5px;
	}

	.price-wrap #one, 
	.price-wrap #two {
		width: 30px;
		text-align: right;
		margin: 0;
		padding: 0;
		margin-right: 2px;
		background:  0;
		border: 0;
		outline: 0;
		color: #242424;
		font-family: 'Karla', 'Arial', sans-serif;
		font-size: 14px;
		line-height: 1.2em;
		font-weight: 400;
	}

	.price-wrap #one-rate, 
	.price-wrap #two-rate {
		width: 30px;
		text-align: right;
		margin: 0;
		padding: 0;
		margin-right: 2px;
		background:  0;
		border: 0;
		outline: 0;
		color: #242424;
		font-family: 'Karla', 'Arial', sans-serif;
		font-size: 14px;
		line-height: 1.2em;
		font-weight: 400;
	}

	.price-wrap label {
		text-align: right;
		margin-top: 6px;
		padding-left: 5px;
	}

	/* Style for active state input */

	.price-field input[type=range]:hover::-webkit-slider-thumb {
		box-shadow: 0 0 0 0.5px #242424;
		transition-duration: 0.3s;
	}

	.price-field input[type=range]:active::-webkit-slider-thumb {
		box-shadow: 0 0 0 0.5px #242424;
		transition-duration: 0.3s;
	}
</style>

@section('content')

<div class="movie-items  full-width">
	<input type="hidden" name="keyword" id="keyword" value="{{$keyword}}">
	<input type="hidden" name="country_name" id="country_name">
	<input type="hidden" name="genre_name" id="genre_name">
	<input type="hidden" name="select_type" id="select_type">
	<input type="hidden" name="rate_min" id="rate_min">
	<input type="hidden" name="rate_max" id="rate_max">
	<div class="row">
		<div class="col-md-12">
			<div class="container" id="filter">
				<div class="col-sm-3"> 					
					<label>Type</label>
					<br>
					<select name="type" id="type" class="form-control" onchange="getType()">
						
						<option>--SELECT TYPE--</option>
						<option value="movie">Movies</option>
						<option value="tv">T-Series</option>
						<option value="new">New Arrival</option>
						
					</select>
					
				</div>
				<div class="col-sm-3"> <label>Genre</label>
					<br>
					<select name="genre" id="genre" class="form-control" onchange="getGenre()">
						<option>--SELECT GENRE--</option>
						@if(isset($genres))
						@foreach($genres as $genre)
						<option value="{{$genre->id}}">{{$genre->name}}</option>
						@endforeach
						@endif
					</select>
					<br>
				</div>
				<div class="col-sm-3"> <label>Release Year</label>
					<br>
					<div class="wrapper">
						<fieldset class="filter-price">

							<div class="price-field">
								<input type="range" min="1900" max="2019" value="1920" id="lower" onchange="getYearFilter1()">
								<input type="range" min="1900" max="2019" value="2019" id="upper" onchange="getYearFilter2()">
							</div>
							<div class="price-wrap">
								{{-- <span class="price-title">FILTER</span> --}}
								<div class="price-container">
									<div class="price-wrap-1">

										<label for="one"></label>
										<input id="one" style="color: #fff!important">
									</div>
									<div class="price-wrap_line" style="color: #fff!important">-</div>
									<div class="price-wrap-2">
										<label for="two"></label>
										<input id="two" style="color: #fff!important">

									</div>
								</div>
							</div>
						</fieldset>
					</div>
					<br>
				</div>
				<div class="col-sm-3">
					<label>Rating</label> 
					<div class="wrapper">
						<fieldset class="filter-price">

							<div class="price-field">
								<input type="range" min="0" max="10" value="5" id="lower-rate" onchange="getRateFilter1()">
								<input type="range" min="0" max="10" value="10" id="upper-rate" onchange="getRateFilter2()">
							</div>
							<div class="price-wrap">
								{{-- <span class="price-title">FILTER</span> --}}
								<div class="price-container">
									<div class="price-wrap-1">

										<label for="one-rate"></label>
										<input id="one-rate" style="color: #fff!important">
									</div>
									<div class="price-wrap_line" style="color: #fff!important">-</div>
									<div class="price-wrap-2">
										<label for="two-rate"></label>
										<input id="two-rate" style="color: #fff!important">

									</div>
								</div>
							</div>
						</fieldset>
					</div>
				</div>


			</div>
			<div class="title-hd">
				<h4 style="color:#fff">Showing results for <i style="color: #dcf836">{{ucfirst($keyword)}}</i></h4>
				<br><br>
				{{-- <a href="#" class="nowshow">Now Showing <i class="ion-ios-arrow-right"></i></a>
				<a href="#" class="viewall">View all <i class="ion-ios-arrow-right"></i></a> --}}

			</div>
			
			<ul class="tab-links">
				{{-- <li class="subtitle">Showing all results for keyword</li> --}}

			</ul>        

			<div class="row" id="dynamic-search">
				@if (isset($contents->results))
				@foreach ($contents->results as $element)
				<div class="col-sm-3 search-response">
					<div class="movie-item">
						<div class="mv-img">
							@if(isset($element->poster_path))
							<img src="https://image.tmdb.org/t/p/w300{{$element->poster_path}}" alt="" style="width: 240px; height: 340px">
							@else
							<img src="/images/default.png" alt="" style="width: 240px; height: 340px">
							@endif
						</div>
						<div class="hvr-inner">
							<a  href="/movie-detail/{{$element->id}}">
								<i class="fa fa-play" aria-hidden="true"></i> </a>
							</div>
							<div class="title-in">
								<h6><a href="/movie-detail/{{$element->id}}">{{$element->title}}</a></h6>
								<p><i class="ion-android-star"></i><span>{{$element->vote_average}}</span> /10</p>
							</div>
						</div>
					</div>
					@endforeach
					@endif



				</div>






				<hr class="hrstyle">

			</div>
		</div>

	</div>

	<script>
		function getType(){
			var type = $('#type').val();
			$('#select_type').val(type);
			search();
		}

		function getCountry(){
			var type = $('#country').val();
			$('#country_name').val(type);
			search();
		}

		function getGenre(){
			var type = $('#genre').val();
			$('#genre_name').val(type);
			search();

		}

		function getRate(){
			var type = $('#type').val();
			$('#select_type').val(type);
			search();

		}

		function getYearFilter1(){
	 		search();

	 }

	 function getYearFilter2(){
	 		search();

	 }

	 function getRateFilter1(){
	 		search();

	 }

	 function getRateFilter2(){
	 		search();

	 }

	 function search(){
	 	var CSRF_Token = $('meta[name="csrf-token"]').attr('content');

	 	var type = $('#select_type').val();
	 	var country = $('#country_name').val();
	 	var genre = $('#genre_name').val();
	 	var search = $('#keyword').val();
	 	var min_year = $('#one').val();
	 	var max_year = $('#two').val();
	 	var min_rate = $('#one-rate').val();
	 	var max_rate = $('#two-rate').val();

		$.ajax({
			type: "POST",
			url: "/search-filter",
			data:{ _token: CSRF_Token, type: type, country: country,genre: genre, search: search, min_y : min_year, max_y : max_year, min_r : min_rate, max_r: max_rate},
			success:function(msg){
          $('#dynamic-search').html(msg);   
      }
  });

	}
</script>

<script type="text/javascript">var lowerSlider = document.querySelector('#lower');
var  upperSlider = document.querySelector('#upper');

document.querySelector('#two').value=upperSlider.value;
document.querySelector('#one').value=lowerSlider.value;

var  lowerVal = parseInt(lowerSlider.value);
var upperVal = parseInt(upperSlider.value);

upperSlider.oninput = function () {
	lowerVal = parseInt(lowerSlider.value);
	upperVal = parseInt(upperSlider.value);

	if (upperVal < lowerVal + 4) {
		lowerSlider.value = upperVal - 4;
		if (lowerVal == lowerSlider.min) {
			upperSlider.value = 4;
		}
	}
	document.querySelector('#two').value=this.value
};

lowerSlider.oninput = function () {
	lowerVal = parseInt(lowerSlider.value);
	upperVal = parseInt(upperSlider.value);
	if (lowerVal > upperVal - 4) {
		upperSlider.value = lowerVal + 4;
		if (upperVal == upperSlider.max) {
			lowerSlider.value = parseInt(upperSlider.max) - 4;
		}
	}
	document.querySelector('#one').value=this.value
}; </script>

<script type="text/javascript">var lowerSlider = document.querySelector('#lower-rate');
var  upperSlider = document.querySelector('#upper-rate');

document.querySelector('#two-rate').value=upperSlider.value;
document.querySelector('#one-rate').value=lowerSlider.value;

var  lowerVal = parseInt(lowerSlider.value);
var upperVal = parseInt(upperSlider.value);

upperSlider.oninput = function () {
	lowerVal = parseInt(lowerSlider.value);
	upperVal = parseInt(upperSlider.value);

	if (upperVal < lowerVal + 2) {
		lowerSlider.value = upperVal - 2;
		if (lowerVal == lowerSlider.min) {
			upperSlider.value = 2;
		}
	}
	document.querySelector('#two-rate').value=this.value
};

lowerSlider.oninput = function () {
	lowerVal = parseInt(lowerSlider.value);
	upperVal = parseInt(upperSlider.value);
	if (lowerVal > upperVal - 2) {
		upperSlider.value = lowerVal + 2;
		if (upperVal == upperSlider.max) {
			lowerSlider.value = parseInt(upperSlider.max) - 2;
		}
	}
	document.querySelector('#one-rate').value=this.value
}; </script>

@endsection 